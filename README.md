# M3U PlayList manager service

### Windows 10 compatible service - M3U PlayList manager

This program does not store video files. The base of links to videos and the video fragments themselves is created by the user himself.
As a rule, links to public URLs of video streams are deliberately distributed by copyright holders and operators providing access to video streams, the latter doing this for advertising purposes hoping to increase the paid audience.
Most of the operators providing access services officially distribute their own playlists in MM format among their paid subscribers.

The creation of links is not a direct violation of copyright, as it is a description of the resource and not the author's material itself.

If you think that any links in your playlists violate the rights of the copyright holder or access operator - remove them! :)

### Dependency

* cscore-master
* embedio-master
* taglib-sharp-main
* XamarinMediaManager-develop

### Tools

* Minify-master
* BundlerMinifier-master

