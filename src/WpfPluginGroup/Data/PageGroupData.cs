﻿/* Copyright (c) 2021 WpfPluginGroup, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using PlayListServiceData.Base;
using PlayListServiceData.Utils;
using PlayListServiceData.Container;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class PageGroupData : INotifyPropertyChanged
    {
        public enum TypeActions : int
        {
            LOAD_All = 0,
            LOAD_ALIAS,
            SAVE_ALIAS,
            SAVE_ALL,
            COLLAPSE_ALL,
            EDIT_GROUP,
            FIND_M3u,
            ADD_ToGroup,
            MOVE_M3uToGroup,
            MOVE_GroupToM3u,
            COPY_M3uToGroup,
            COPY_GroupToM3u,
            COPY_UnknownToGroup,
            DELETE_FromM3u,
            DELETE_FromGroup,
            NONE
        };

        private int _ListViewIndex = -1;
        private bool _IsListChanged = false,
                     _IsTreeChanged = false,
                     _IsTreeActions = false,
                     _IsLoadingOrSavingActions = false;
        private CancellationTokenSource tokenSourceCancel = default;
        private readonly DataContainer<BaseSerializableChannelData> XmlDataContainer;

        public ObservableCollection<string> M3uItems { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<TreeViewDataItem> GroupsItems { get; set; } = new ObservableCollection<TreeViewDataItem>();
        public TreeViewDataItem TreeSelectedItem { get; set; } = default;

        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageGroupData()
        {
            tokenSourceCancel = new CancellationTokenSource();
            XmlDataContainer = new DataContainer<BaseSerializableChannelData>(tokenSourceCancel.Token);
        }
        ~PageGroupData()
        {
            TokenDispose();
        }

        public async void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
            ServiceCtrl.PropertyChanged += local_PropertyChanged;
            if (ServiceCtrl.IsControlConfig)
            {
                await loadFileListAsync_(tokenSourceCancel.Token);
                XmlDataContainer.OpenAndRead(
                    BasePath.GetGroupAliasPath(),
                    eventGroupLoadCb_,
                    ConstContainer.Actions.LOAD_GROUP_ALIAS);
            }
        }

        #region Get/Set

        public bool IsListChanged
        {
            get { return _IsListChanged; }
            private set
            {
                if (_IsListChanged == value)
                    return;
                _IsListChanged = value;
                OnPropertyChanged(nameof(IsListChanged));
            }
        }

        public bool IsTreeChanged
        {
            get { return _IsTreeChanged; }
            private set
            {
                if (_IsTreeChanged == value)
                    return;
                _IsTreeChanged = value;
                OnPropertyChanged(nameof(IsTreeChanged));
            }
        }

        public bool IsListActions
        {
            get { return _ListViewIndex >= 0; }
        }

        public bool IsTreeActions
        {
            get { return _IsTreeActions; }
            set
            {
                if (_IsTreeActions == value)
                    return;
                _IsTreeActions = value;
                OnPropertyChanged(nameof(IsTreeActions));
            }
        }

        public Visibility GuiProgressVisible
        {
            get { return _IsLoadingOrSavingActions ? Visibility.Visible : Visibility.Hidden; }
        }

        public Visibility GuiInfoVisible
        {
            get { return _IsLoadingOrSavingActions ? Visibility.Hidden : Visibility.Visible; }
        }

        public int ListViewIndex
        {
            get { return _ListViewIndex; }
            set
            {
                if (_ListViewIndex == value)
                    return;
                _ListViewIndex = value;
                OnPropertyChanged(nameof(ListViewIndex));
                OnPropertyChanged(nameof(IsListActions));
            }
        }

        #endregion

        #region Find Item

        public async void FindItem(string s, ModernWpf.Controls.ListView lv, System.Windows.Controls.TreeView tv)
        {
            await FindItemAsync(s, lv, tv);
        }
        public async Task FindItemAsync(string s, ModernWpf.Controls.ListView lv, System.Windows.Controls.TreeView tv)
        {
            if (string.IsNullOrWhiteSpace(s))
                return;

            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        for (int i = 0; i < M3uItems.Count; i++)
                            if (M3uItems[i].StartsWith(s, StringComparison.OrdinalIgnoreCase))
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    ListViewIndex = i;
                                    lv.ScrollIntoView(M3uItems[i]);
                                    lv.Focus();
                                });
                                return;
                            }
                        foreach (TreeViewDataItem i in GroupsItems)
                            foreach (TreeViewDataItem t in i.IfListItems)
                                if (t.IfTag.StartsWith(s, StringComparison.OrdinalIgnoreCase))
                                {
                                    Application.Current.Dispatcher.Invoke(() =>
                                    {
                                        i.IsExpanded = true;
                                        t.IsSelected = true;
                                        tv.Focus();
                                    });
                                    return;
                                }
                        foreach (TreeViewDataItem i in tv.Items)
                            if (i.IfTag.Contains(s))
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    i.IsExpanded = true;
                                    i.IsSelected = true;
                                    tv.Focus();
                                });
                                return;
                            }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
#else
                    catch { }
#endif
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
#else
            catch { }
#endif
        }

        #endregion

        #region Item Action

        public void ItemAction<T1, T2>(TypeActions a, T1 source, T2 target) where T1 : class where T2 : class
        {
            if (source == null)
                return;

            switch (a)
            {
                case TypeActions.MOVE_M3uToGroup:
                    {
                        if (source is string s)
                        {
                            if (M3uItems.Contains(s))
                            {
                                bool b = false;
                                if (target != null)
                                {
                                    if (target is TreeViewDataItem tvi)
                                    {
                                        if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT)
                                            b = tvi.SubAdd(s);
                                        else if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_CHILD)
                                        {
                                            TreeViewDataItem tvd = GroupsItems.Find(tvi.IfTag);
                                            if (tvd != null)
                                                b = tvd.SubAdd(s);
                                        }
                                    }
                                    else if (target is string str)
                                    {
                                        if (!string.IsNullOrWhiteSpace(str))
                                            b = GroupsItems.Add(str);
                                    }
                                }
                                else
                                    b = GroupsItems.Add(s);

                                M3uItems.Remove(s);
                                ListViewIndex = -1;
                                IsListChanged = true;
                                OnPropertyChanged(nameof(M3uItems));

                                if (b)
                                {
                                    IsTreeChanged = true;
                                    OnPropertyChanged(nameof(GroupsItems));
                                }
                            }
                        }
                        break;
                    }
                case TypeActions.DELETE_FromGroup: /* ? the same ? */
                case TypeActions.MOVE_GroupToM3u:
                    {
                        if (source is TreeViewDataItem tvi)
                        {
                            if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT)
                            {
                                foreach (TreeViewDataItem i in tvi.IfListItems)
                                {
                                    if ((!string.IsNullOrWhiteSpace(i.IfTag)) && (!M3uItems.Contains(i.IfTag)))
                                    {
                                        M3uItems.Insert(0, i.IfTag);
                                        IsListChanged = true;
                                    }
                                }
                                if ((a == TypeActions.MOVE_GroupToM3u) && (!string.IsNullOrWhiteSpace(tvi.IfTag)))
                                {
                                    M3uItems.Insert(0, tvi.IfTag);
                                    IsListChanged = true;
                                }
                                GroupsItems.Remove(tvi);
                            }
                            else if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_CHILD)
                            {
                                if (string.IsNullOrWhiteSpace(tvi.IfTag))
                                    return;
                                if (!M3uItems.Contains(tvi.IfTag))
                                {
                                    M3uItems.Insert(0, tvi.IfTag);
                                    IsListChanged = true;
                                }
                                TreeViewDataItem tvd = GroupsItems.Find(tvi.IfTag);
                                if (tvd != null)
                                    tvd.SubRemove(tvi);
                            }
                            IsTreeChanged = true;
                            OnPropertyChanged(nameof(M3uItems));
                            OnPropertyChanged(nameof(GroupsItems));
                        }
                        break;
                    }
                case TypeActions.COPY_UnknownToGroup:
                    {
                        if (source is string s)
                        {
                            if ((target != null) && (target is string str) && (!string.IsNullOrWhiteSpace(str)))
                                _ = GroupsItems.Add(s, str);
                            else
                                _ = GroupsItems.Add(s);
                            IsTreeChanged = true;
                            OnPropertyChanged(nameof(GroupsItems));
                        }
                        break;
                    }
                case TypeActions.COPY_M3uToGroup:
                    {
                        break;
                    }
                case TypeActions.COPY_GroupToM3u:
                    {
                        break;
                    }
                case TypeActions.DELETE_FromM3u:
                    {
                        if (source is string s)
                        {
                            if (M3uItems.Contains(s))
                            {
                                M3uItems.Remove(s);
                                ListViewIndex = -1;
                            }
                        }
                        break;
                    }
                case TypeActions.ADD_ToGroup:
                    {
                        if (source is string s)
                            GroupsItems.Add(s);
                        OnPropertyChanged(nameof(GroupsItems));
                        break;
                    }
                case TypeActions.EDIT_GROUP:
                    {
                        if (source is TreeViewDataItem tvi)
                        {
                            if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT)
                            {
                                if (tvi.IsEditable)
                                    tvi.IsEditable = false;
                                else
                                {
                                    foreach (var i in from i in GroupsItems
                                                      where i.IsEditable && i.IsRoot
                                                      select i)
                                        i.IsEditable = false;
                                    tvi.IsEditable = true;
                                }
                            }
                            else if (tvi.ItemType == TreeViewDataItem.ItemTypes.TVD_CHILD)
                            {
                                if (string.IsNullOrWhiteSpace(tvi.IfTag))
                                    return;

                                TreeViewDataItem tvd = GroupsItems.Find(tvi.IfTag);
                                if (tvd != null)
                                {
                                    if (!tvd.IsEditable)
                                    {
                                        tvd.IsEditable = true;
                                        tvd.IsSelected = true;
                                        tvd.IsExpanded = false;
                                    }
                                    else
                                        tvd.IsEditable = false;
                                }
                            }
                        }
                        break;
                    }
            }
        }

        #endregion

        /* IO */

        #region init IO CallBack
        private readonly RunOnce onceGroup = new RunOnce();

        #region - Load all data = LoadAllGroup
        public async void LoadAllGroup()
        {
            if (_IsLoadingOrSavingActions || string.IsNullOrEmpty(ServiceCtrl.ControlConfig.ConfigM3uPath))
                return;

            Application.Current.Dispatcher.Invoke(() => _IsLoadingOrSavingActions = true);
            onceGroup.Reset();
            await loadFileListAsync_(tokenSourceCancel.Token);
            XmlDataContainer.ReadData(ConstContainer.Actions.LOAD_GROUP_ALIAS);
        }

        #endregion

        /* - LOAD Group tree - */
        #region - Group tree = eventGroupLoadCb_ (call loadedGroupAsync_)
        private async void eventGroupLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceGroup.GetOnce())
                            return;
                        await loadGroupAsync_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                    {
                        break;
                    }
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        _IsLoadingOrSavingActions = false;
                        Application.Current.Dispatcher.Invoke(() => { _IsLoadingOrSavingActions = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { _IsLoadingOrSavingActions = false; });
                        break;
                    }
            }
        }

        #endregion

        /* - SAVE Groups - */
        #region - SAVE Groups callback = eventEpgFavSaveCb_
        private void eventGroupSaveCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.SaveBegin:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { _IsLoadingOrSavingActions = true; });
                        break;
                    }
                case ConstContainer.State.SaveOk:
                    {
                        break;
                    }
                case ConstContainer.State.DataReady:
                case ConstContainer.State.SaveEnd:
                case ConstContainer.State.SaveError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { _IsLoadingOrSavingActions = false; });
                        break;
                    }
            }
            switch (s)
            {
                case ConstContainer.State.SaveBegin:
                case ConstContainer.State.Saving:
                case ConstContainer.State.Loading:
                case ConstContainer.State.DataReady:
                case ConstContainer.State.SaveEnd:
                case ConstContainer.State.SaveError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            OnPropertyChanged(nameof(GuiInfoVisible));
                            OnPropertyChanged(nameof(GuiProgressVisible));
                        });
                        break;
                    }
            }
        }

        #endregion
        #endregion

        #region LOAD Group Async
        private async Task loadGroupAsync_(CancellationToken tokenCancel)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                GroupsItems.Clear();
                _IsLoadingOrSavingActions = true;
                OnPropertyChanged(nameof(GroupsItems));
                OnPropertyChanged(nameof(GuiInfoVisible));
                OnPropertyChanged(nameof(GuiProgressVisible));
            });

            try
            {
                await Task.Run(() =>
                {
                    DataContainerItem<BaseSerializableChannelData> dc = default;
                    int cnt = M3uItems.Count;
                    try
                    {
                        dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.LOAD_GROUP_ALIAS);
                        if ((dc == null) || (dc.Data == null))
                            return;

                        dc.IsComplette = false;
                        foreach (var n in dc.Data.channel.Distinct())
                        {
                            if (tokenCancel.IsCancellationRequested)
                                break;
                            if (string.IsNullOrWhiteSpace(n.id))
                                continue;

                            if ((n.displayname == default) || (n.displayname.Length == 0))
                                continue;

                            var list = n.displayname.Distinct();
                            Application.Current.Dispatcher.Invoke(() => GroupsItems.Add(n.id, list.ToArray(), true));
                            if (M3uItems.Contains(n.id))
                                Application.Current.Dispatcher.Invoke(() => M3uItems.Remove(n.id));

                            foreach (var i in list)
                            {
                                if (tokenCancel.IsCancellationRequested)
                                    break;
                                if (M3uItems.Contains(i))
                                    Application.Current.Dispatcher.Invoke(() => M3uItems.Remove(i));
                            }
                        }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                    finally
                    {
                        if (dc != default)
                        {
                            dc.IsComplette = true;
                            dc.Data = null;
                            dc.Event.EventStatus.BaseEventReset();
                        }
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            _IsLoadingOrSavingActions = false;
                            OnPropertyChanged(nameof(GuiInfoVisible));
                            OnPropertyChanged(nameof(GuiProgressVisible));
                            if (M3uItems.Count != cnt)
                                OnPropertyChanged(nameof(M3uItems));
                            if (GroupsItems.Count > 0)
                                OnPropertyChanged(nameof(GroupsItems));
                        });
                    }
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
        }
        #endregion

        #region LOAD FileList Async
        private async Task loadFileListAsync_(CancellationToken tokenCancel)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                M3uItems.Clear();
                _IsLoadingOrSavingActions = true;
                OnPropertyChanged(nameof(M3uItems));
                OnPropertyChanged(nameof(GuiInfoVisible));
                OnPropertyChanged(nameof(GuiProgressVisible));
            });
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        foreach (var f in Directory.EnumerateFiles(ServiceCtrl.ControlConfig.ConfigM3uPath, "*.m3u", SearchOption.TopDirectoryOnly))
                        {
                            if (tokenCancel.IsCancellationRequested)
                                break;
                            FileInfo file = new FileInfo(f);
                            if (file.Exists && (!file.IsReadOnly))
                                Application.Current.Dispatcher.Invoke(() => { M3uItems.Add(Path.GetFileNameWithoutExtension(file.FullName)); });
                        }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                    finally
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            _IsLoadingOrSavingActions = false;
                            OnPropertyChanged(nameof(GuiInfoVisible));
                            OnPropertyChanged(nameof(GuiProgressVisible));
                            if (M3uItems.Count > 0)
                                OnPropertyChanged(nameof(M3uItems));
                        });
                    }
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
        }

        #endregion

        #region Save Data

        public void SaveData()
        {
            if (_IsLoadingOrSavingActions || string.IsNullOrEmpty(ServiceCtrl.ControlConfig.ConfigM3uPath))
                return;
            Application.Current.Dispatcher.Invoke(() =>
            {
                _IsLoadingOrSavingActions = true;
                OnPropertyChanged(nameof(GuiInfoVisible));
                OnPropertyChanged(nameof(GuiProgressVisible));
            });
            onceGroup.Reset();
            var dc = XmlDataContainer.OpenAndWrite(
                BasePath.GetGroupAliasPath(),
                buildGroup_,
                eventGroupSaveCb_,
                ConstContainer.Actions.SAVE_GROUP_ALIAS);
            if (dc != default)
                dc.Event.EventStatus.BaseEventReset();
        }

        #region Save Group builder = buildGroup_
        private BaseSerializableChannelData buildGroup_()
        {
            try
            {
                List<BaseSerializableGroup> list = new List<BaseSerializableGroup>();
                foreach (var i in GroupsItems.Distinct())
                {
                    try
                    {
                        BaseSerializableGroup g = new BaseSerializableGroup(true)
                        {
                            id = i.IfTag
                        };
                        if (g.icon == null)
                            g.icon = new BaseSerializableIcon(true);
                        g.icon.src = default;
                        if (i.IfListItems.Count == 0)
                            continue;
                        g.displayname = new string[i.IfListItems.Count];
                        for (int n = 0; n < i.IfListItems.Count; n++)
                            g.displayname[n] = i.IfListItems[n].IfTag;
                        list.Add(g);
                    }
                    catch { }
                }
                if (list.Count > 0)
                {
                    BaseSerializableChannelData data = new BaseSerializableChannelData(true, list.Count)
                    {
                        channel = list.ToArray()
                    };
                    return data;
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return default;
        }
        #endregion

        #endregion

        #region private Token Dispose

        private void TokenDispose()
        {
            if (tokenSourceCancel != null)
            {
                tokenSourceCancel.Cancel();
                tokenSourceCancel.Dispose();
                tokenSourceCancel = default;
            }
        }

        #endregion

        #region event control
        private void local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ControlConfig":
                    {
                        if ((ServiceCtrl.ControlConfig == null) || (!ServiceCtrl.ControlConfig.IsLoad))
                            return;

                        if ((tokenSourceCancel != null) && tokenSourceCancel.IsCancellationRequested)
                            return;

                        LoadAllGroup();
                        break;
                    }
            }
        }
        #endregion
    }
}
