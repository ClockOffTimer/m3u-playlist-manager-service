﻿/* Copyright (c) 2021 WpfPluginGroup, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using M3U.Model;
using PlayListServiceData.Container.Items;

namespace WpfPlugin.Data
{
    public class TreeViewDataItem : IViewItem<ObservableCollection<TreeViewDataItem>>, INotifyPropertyChanged
    {
        public enum ItemTypes : int
        {
            TVD_NONE = -1,
            TVD_ROOT = 0,
            TVD_CHILD
        }
        public ItemTypes ItemType { get; private set; } = ItemTypes.TVD_NONE;
        public ObservableCollection<TreeViewDataItem> IfListItems { get; set; } = new ObservableCollection<TreeViewDataItem>();

        private bool _IsSelected = default,
                     _IsExpanded = default,
                     _IsEditable = default,
                     _IsFavorite = default;

        private string _IfTag = default,
                       _Icon = default,
                       _ChannelExample = default;

        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                if (value != _IsSelected)
                {
                    _IsSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }

        public bool IsExpanded
        {
            get { return (ItemType == ItemTypes.TVD_ROOT) && _IsExpanded; }
            set
            {
                if (value != _IsExpanded)
                {
                    _IsExpanded = (ItemType == ItemTypes.TVD_ROOT) && value;
                    OnPropertyChanged(nameof(IsExpanded));
                }
            }
        }

        public bool IsEditable
        {
            get { return (ItemType == ItemTypes.TVD_ROOT) && _IsEditable; }
            set
            {
                if (value != _IsEditable)
                {
                    _IsEditable = (ItemType == ItemTypes.TVD_ROOT) && value;
                    OnPropertyChanged(nameof(IsEditable));
                }
            }
        }

        public bool IsRoot
        {
            get { return ItemType == ItemTypes.TVD_ROOT; }
        }
        public bool IsChild
        {
            get { return ItemType == ItemTypes.TVD_CHILD; }
        }

        /* interface IViewItem */

        public Media IfMedia { get; set; } = default;

        public string IfTag
        {
            get { return _IfTag; }
            set { _IfTag = value; OnPropertyChanged(nameof(IfTag)); }
        }
        public string IfIcon
        {
            get { return _Icon; }
            set { _Icon = value; OnPropertyChanged(nameof(IfIcon)); }
        }
        public string IfChannelName
        {
            get
            {
                if (_ChannelExample == default)
                    _ChannelExample = (IfListItems.Count() > 0) ? IfListItems[0].IfTag : default;
                return _ChannelExample;
            }
            set { _ChannelExample = value; OnPropertyChanged(nameof(IfChannelName)); }
        }
        public bool IfFavorite
        {
            get { return _IsFavorite; }
            set { _IsFavorite = value; OnPropertyChanged(nameof(IfFavorite)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public TreeViewDataItem() { }
        public TreeViewDataItem(ItemTypes t, string s)
        {
            IfTag = s;
            ItemType = t;
        }
        public TreeViewDataItem(ItemTypes t, string s, string[] child)
        {
            IfTag = s;
            ItemType = t;
            _SubAdd(child);
        }

        public bool SubAdd(string s)
        {
            return _SubAdd(new string[] { s });
        }

        public bool SubAdd(string[] childs)
        {
            return _SubAdd(childs);
        }

        private bool _SubAdd(string[] childs)
        {
            try
            {
                if (childs.Length == 0)
                    return false;

                bool b = default;
                foreach (var s in childs)
                {
                    var d = GetTreeViewSubItem(s);
                    if (d == null)
                    {
                        /// ? Items.Insert(0, new TreeViewDataItem(ItemTypes.TVD_CHILD, s, new string [] { s }));
                        IfListItems.Insert(0, new TreeViewDataItem(ItemTypes.TVD_CHILD, s));
                        b = true;
                    }
                }
                if (b)
                {
                    if (IfListItems.Count > 1)
                    {
                        foreach (var t in from TreeViewDataItem t in IfListItems
                                          where string.IsNullOrWhiteSpace(t.IfTag)
                                          select t)
                            IfListItems.Remove(t);
                    }
                    OnPropertyChanged(nameof(IfListItems));
                }
                else
                    return false;
                return true;
            }
            catch { return false; }
        }

        public void SubRemove(string s)
        {
            foreach (var t in from TreeViewDataItem t in IfListItems
                              where t.IfTag.Equals(s)
                              select t)
            {
                IfListItems.Remove(t);
                OnPropertyChanged(nameof(IfListItems));
                break;
            }
        }

        public void SubRemove(TreeViewDataItem tvd)
        {
            IfListItems.Remove(tvd);
            OnPropertyChanged(nameof(IfListItems));
        }

        public bool TagEquals(string s)
        {
            return IfTag.Equals(s, System.StringComparison.OrdinalIgnoreCase);
        }

        public bool GroupsContains(string s)
        {
            return GetTreeViewSubItem(s) != null;
        }

        public void Clear()
        {
            IfTag = default(string);
            IfListItems.Clear();
            OnPropertyChanged(nameof(IfTag));
            OnPropertyChanged(nameof(IfListItems));
        }

        private TreeViewDataItem GetTreeViewSubItem(string grp)
        {
            try
            {
                return (from TreeViewDataItem t in IfListItems
                        where (t != null) && t.IfTag.Equals(grp)
                        select t).FirstOrDefault();
            }
            catch { return default; }
        }
    }

    /// ///////////////////////////////

    public static class DataUtil
    {
        public static ObservableCollection<TreeViewDataItem> Create<T>()
        {
            return new ObservableCollection<TreeViewDataItem>();
        }

        public static bool Add<T>(this ObservableCollection<TreeViewDataItem> data, TreeViewDataItem item)
        {
            if (item == null)
                return false;

            data.Add(item);
            return true;
        }

        public static bool Add(this ObservableCollection<TreeViewDataItem> data, string tag)
        {
            return Add(data, tag, new string[] { "" }, true);
        }

        public static bool Add(this ObservableCollection<TreeViewDataItem> data, TreeViewDataItem item, string child)
        {
            if (item == null)
                return false;
            return Add(data, item.IfTag, new string[] { child }, false);
        }

        public static bool Add(this ObservableCollection<TreeViewDataItem> data, string header, string child, bool IsNotFoundCreate = false)
        {
            return Add(data, header, new string[] { child }, IsNotFoundCreate);
        }

        public static bool Add(this ObservableCollection<TreeViewDataItem> data, string header, string[] childs, bool IsNotFoundCreate)
        {
            try
            {
                if (string.IsNullOrEmpty(header))
                    return false;

                TreeViewDataItem item = GetTreeViewItem(ref data, header);
                if (item == null)
                {
                    if (IsNotFoundCreate)
                    {
                        data.Insert(0, new TreeViewDataItem(TreeViewDataItem.ItemTypes.TVD_ROOT, header, childs));
                        return true;
                    }
                    return false;
                }

                if (!item.SubAdd(childs))
                    return false;
                return true;
            }
            catch { return false; }
        }

        public static TreeViewDataItem Find(this ObservableCollection<TreeViewDataItem> data, string child)
        {
            foreach (var d in data.SelectMany(d => d.IfListItems.Where(n => n.IfTag.Equals(child)).Select(n => d)))
                return d;
            return default;
        }

        private static TreeViewDataItem GetTreeViewItem(ref ObservableCollection<TreeViewDataItem> data, string header)
        {
            try
            {
                return (from d in data
                        where (d != null) && d.IfTag.Equals(header)
                        select d).FirstOrDefault();
            }
            catch { return default; }
        }
    }
}
