﻿/* Copyright (c) 2021 WpfPluginGroup, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 4),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageGroup : Page, IPlugin
    {
        private PageGroupData pageGroupData { get; set; } = default;
        private readonly string[] controlTag;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageGroup(IServiceControls icontrols)
        {
            InitializeComponent();
            pageGroupData = (PageGroupData)DataContext;
            pageGroupData.SetServiceCtrl(icontrols);
            controlTag = Enum.GetNames(typeof(PageGroupData.TypeActions)).ToArray<string>();
            TreeViewGroups.SelectedItemChanged += (s, a) =>
            {
                pageGroupData.IsTreeActions = a.NewValue != null;
                pageGroupData.TreeSelectedItem = a.NewValue as TreeViewDataItem;
            };
        }

        #region left panel

        #region ListView

        private void ListViewM3u_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((e.Source != null) && (sender is ModernWpf.Controls.ListView lv) &&
                (lv.SelectedItem != null))
                pageGroupData.ItemAction(
                    PageGroupData.TypeActions.MOVE_M3uToGroup,
                    lv.SelectedItem as string,
                    pageGroupData.TreeSelectedItem);
        }
        private void ListViewM3u_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.LeftButton == MouseButtonState.Pressed) && (e.Source != null) && (sender is ModernWpf.Controls.ListView lv))
                if (lv.SelectedItem != null)
                {
                    if (e.GetPosition(lv).X > (lv.ActualHeight - 70))
                        return;
                    DragDrop.DoDragDrop(lv, lv.SelectedItem, DragDropEffects.Move);
                }
        }
        private void ListViewM3u_DragEnter(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
                e.Effects = DragDropEffects.Move;
        }
        private void ListViewM3u_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    e.Handled = false;
                else if (e.Data.GetDataPresent(typeof(TreeViewDataItem)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.MOVE_GroupToM3u,
                        e.Data.GetData(typeof(TreeViewDataItem)) as TreeViewDataItem,
                        default(string));
                e.Handled = true;
            }
        }

        #endregion

        #region button/event button

        #region M3uLoad
        private void ButtonM3uLoad_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton tb)
                if (tb.Uid == controlTag[(int)PageGroupData.TypeActions.LOAD_All])
                    pageGroupData.LoadAllGroup();
        }

        private void ButtonM3uMove_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.MOVE_M3uToGroup]))
                pageGroupData.ItemAction(
                    PageGroupData.TypeActions.MOVE_M3uToGroup,
                    ListViewM3u.SelectedItem as string,
                    default(string));
        }
        private void EventM3uMove_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton tb)
            {
                if (!e.Data.GetDataPresent(typeof(string)) || (tb.Uid != controlTag[(int)PageGroupData.TypeActions.MOVE_M3uToGroup]))
                    return;
                pageGroupData.ItemAction(
                    PageGroupData.TypeActions.MOVE_M3uToGroup,
                    e.Data.GetData(typeof(string)) as string,
                    default(string));
            }
        }

        #endregion

        #region M3uDelete
        private void ButtonM3uDelete_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.DELETE_FromM3u]))
                pageGroupData.ItemAction(
                    PageGroupData.TypeActions.DELETE_FromM3u,
                    ListViewM3u.SelectedItem as string,
                    default(string));
        }
        private void EventM3uDelete_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton tb)
            {
                if (!e.Data.GetDataPresent(typeof(string)) || (tb.Uid != controlTag[(int)PageGroupData.TypeActions.DELETE_FromM3u]))
                    return;
                pageGroupData.ItemAction(PageGroupData.TypeActions.DELETE_FromM3u, e.Data.GetData(typeof(string)) as string, default(string));
            }
        }

        #endregion

        #region M3uFind
        private void ButtonM3uFind_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton)
            {
                if (string.IsNullOrWhiteSpace(TextBoxFindGroup.Text))
                    TextBoxFindGroup.Focus();
                else
                    pageGroupData.FindItem(
                        TextBoxFindGroup.Text,
                        ListViewM3u,
                        TreeViewGroups);
            }
        }
        private void EventM3uFind_Drop(object sender, DragEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton) &&
                 e.Data.GetDataPresent(typeof(string)))
                pageGroupData.FindItem(
                    e.Data.GetData(typeof(string)) as string,
                    ListViewM3u,
                    TreeViewGroups);

        }

        #endregion

        #endregion

        #endregion

        #region right panel

        #region TreeView

        private void TreeViewGroups_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.LeftButton == MouseButtonState.Pressed) &&
                (e.Source != null) &&
                (sender is TreeView tv) &&
                (tv.SelectedItem is TreeViewDataItem item))
            {
                if (e.GetPosition(tv).X > (tv.ActualHeight - 70))
                    return;

                if (item.IsEditable)
                    return;
                DragDrop.DoDragDrop(tv, tv.SelectedItem, DragDropEffects.Move);
                e.Handled = true;
            }
        }
        private void TreeViewGroups_DragEnter(object sender, DragEventArgs e)
        {
            if (sender is TreeView)
                e.Effects = DragDropEffects.Move;
        }
        private void TreeViewGroups_Drop(object sender, DragEventArgs e)
        {
            if ((sender is TreeView tv) && e.Data.GetDataPresent(typeof(string)))
                pageGroupData.ItemAction(
                    PageGroupData.TypeActions.MOVE_M3uToGroup,
                    e.Data.GetData(typeof(string)) as string, tv.SelectedItem);
        }

        #endregion

        #region Editable Header

        private void TreeViewGroups_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if ((e.ChangedButton == MouseButton.Middle) && (e.ButtonState == MouseButtonState.Pressed) &&
                (sender is TreeView tv) && (tv.SelectedItem is TreeViewDataItem tvd) && (tvd.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT))
                pageGroupData.ItemAction(PageGroupData.TypeActions.EDIT_GROUP, tvd, default(string));
        }

        private void TreeViewGroups_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((sender is TreeView tv) && (tv.SelectedItem is TreeViewDataItem))
            {
                try
                {
                    object source = e.OriginalSource;
                    while (source != null && !(source is TreeViewItem))
                        source = VisualTreeHelper.GetParent(source as DependencyObject);

                    if ((source != null) && (source is TreeViewItem tvi))
                    {
                        if (tvi.DataContext is TreeViewDataItem tvd)
                        {
                            if (tvd.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT)
                            {
                                if (!tvd.IsSelected)
                                    tvd.IsSelected = true;
                                if (!tvd.IsEditable)
                                    pageGroupData.ItemAction(PageGroupData.TypeActions.EDIT_GROUP, tvd, default(string));
                                else
                                    tvd.IsEditable = false;
                                e.Handled = true;
                            }
                            else if (tvd.ItemType == TreeViewDataItem.ItemTypes.TVD_CHILD)
                            {
                                if (!tvd.IsSelected)
                                    tvd.IsSelected = true;
                            }
                        }
                        else
                            tvi.Focus();
                        return;
                    }
                }
                catch { }
            }
        }
        private void EditableTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Return) &&
                (sender is TextBox tb) &&
                (TreeViewGroups.SelectedItem is TreeViewDataItem item) &&
                (item.ItemType == TreeViewDataItem.ItemTypes.TVD_ROOT))
                if (item.IsEditable)
                {
                    item.IfTag = tb.Text;
                    item.IsEditable = false;
                }
        }

        #endregion

        #region button/event button

        #region GroupSave
        private void ButtonGroupSave_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.SAVE_ALL]))
                pageGroupData.SaveData();
        }

        #endregion

        #region GroupMove
        private void ButtonGroupMove_Click(object sender, RoutedEventArgs e)
        {
            if (((sender is MenuItem mi) &&
                (mi.Uid == controlTag[(int)PageGroupData.TypeActions.MOVE_GroupToM3u])) ||
                ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.MOVE_GroupToM3u])))
                if (TreeViewGroups.SelectedItem is TreeViewDataItem tvi)
                    pageGroupData.ItemAction(PageGroupData.TypeActions.MOVE_GroupToM3u, tvi, default(string));
        }
        private void EventGroupMove_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton)
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.MOVE_M3uToGroup,
                        e.Data.GetData(typeof(string)) as string,
                        default(string));
                else if (e.Data.GetDataPresent(typeof(TreeViewDataItem)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.MOVE_M3uToGroup,
                        e.Data.GetData(typeof(TreeViewDataItem)) as TreeViewDataItem,
                        default(string));
            }
        }

        #endregion

        #region GroupAdd
        private void ButtonGroupAdd_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton)
            {
                if (string.IsNullOrWhiteSpace(TextBoxAddGroup.Text))
                    TextBoxAddGroup.Focus();
                else
                    pageGroupData.ItemAction(PageGroupData.TypeActions.ADD_ToGroup, TextBoxAddGroup.Text, default(string));
            }
        }
        private void EventGroupAdd_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton)
                if (e.Data.GetDataPresent(typeof(string)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.ADD_ToGroup,
                        e.Data.GetData(typeof(string)) as string,
                        default(string));
        }
        private void TextBoxAddGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Return) && (sender is TextBox tb))
            {
                if (!string.IsNullOrWhiteSpace(tb.Text))
                    pageGroupData.ItemAction(PageGroupData.TypeActions.ADD_ToGroup, tb.Text, default(string));
                tb.Text = default;
                e.Handled = true;
            }
        }

        #endregion

        #region GroupCollapse
        private void ButtonGroupCollapse_Click(object sender, RoutedEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton tb)
                if (tb.Uid == controlTag[(int)PageGroupData.TypeActions.COLLAPSE_ALL])
                    foreach (TreeViewDataItem i in TreeViewGroups.Items)
                        i.IsExpanded = false;
        }
        #endregion

        #region Group edit Header
        private void ButtonGroupEdit_Click(object sender, RoutedEventArgs e)
        {
            if (((sender is MenuItem mi) &&
                (mi.Uid == controlTag[(int)PageGroupData.TypeActions.EDIT_GROUP])) ||
                ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.EDIT_GROUP])))
                if (TreeViewGroups.SelectedItem is TreeViewDataItem tvi)
                    pageGroupData.ItemAction(PageGroupData.TypeActions.EDIT_GROUP, tvi, default(string));
        }
        #endregion

        #region GroupDelete
        private void ButtonGroupDelete_Click(object sender, RoutedEventArgs e)
        {
            if (((sender is MenuItem mi) &&
                (mi.Uid == controlTag[(int)PageGroupData.TypeActions.DELETE_FromGroup])) ||
                ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.DELETE_FromGroup])))
                if (TreeViewGroups.SelectedItem is TreeViewDataItem tvi)
                    pageGroupData.ItemAction(PageGroupData.TypeActions.DELETE_FromGroup, tvi, default(string));
        }
        private void EventGroupDelete_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.Primitives.TitleBarButton)
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.DELETE_FromGroup,
                        e.Data.GetData(typeof(string)) as string,
                        default(string));
                else if (e.Data.GetDataPresent(typeof(TreeViewDataItem)))
                    pageGroupData.ItemAction(
                        PageGroupData.TypeActions.DELETE_FromGroup,
                        e.Data.GetData(typeof(TreeViewDataItem)) as TreeViewDataItem,
                        default(string));
            }
        }

        #endregion

        #region GroupFind
        private void TextBoxFindGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Return) && (sender is TextBox tb))
            {
                if (!string.IsNullOrWhiteSpace(tb.Text))
                    pageGroupData.FindItem(
                        tb.Text,
                        ListViewM3u,
                        TreeViewGroups);
                e.Handled = true;
            }
        }
        private void TextBoxFindGroup_DragEnter(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.FIND_M3u]))
                e.Effects = DragDropEffects.Copy;
        }
        private void TextBoxFindGroup_DragOver(object sender, DragEventArgs e)
        {
            if ((e.Data.GetDataPresent(typeof(string))) ||
                (e.Data.GetDataPresent(typeof(TreeViewDataItem))))
                e.Handled = true;
            else
                e.Handled = false;
        }
        private void TextBoxFindGroup_Drop(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageGroupData.TypeActions.FIND_M3u]))
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    tb.Text = e.Data.GetData(typeof(string)) as string;
                else if (e.Data.GetDataPresent(typeof(TreeViewDataItem)))
                {
                    if (e.Data.GetData(typeof(TreeViewDataItem)) is TreeViewDataItem lvd)
                        try
                        {
                            tb.Text = lvd.IsSelected ? lvd.IfTag :
                                (from i in lvd.IfListItems
                                 where i.IsSelected
                                 select i.IfTag).FirstOrDefault<string>();
                        }
                        catch { return; }
                }
                else
                    return;

                if (!string.IsNullOrWhiteSpace(tb.Text))
                    pageGroupData.FindItem(
                        tb.Text,
                        ListViewM3u,
                        TreeViewGroups);
                e.Handled = true;
            }
        }

        #endregion

        #endregion

        #endregion

    }
}
