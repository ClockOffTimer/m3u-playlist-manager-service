﻿/* Copyright (c) 2021 WpfPluginControls, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Windows.Controls;
using PlayListServiceData.Base;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 2),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_BTN)]
    public partial class PageControls : Page, IPlugin
    {
        private IServiceControls ServiceCtrl { get; set; } = default;
        private PageControlsData pageControlData { get; set; } = default;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageControls(IServiceControls icontrols)
        {
            ServiceCtrl = icontrols;
            InitializeComponent();
            pageControlData = (PageControlsData)DataContext;
            pageControlData.SetServiceCtrl(ServiceCtrl);
        }

        private void CustomServiceCommand(int cmd)
        {
            try
            {
                pageControlData.ControlTextError = default;
                ServiceCtrl.CheckServiceStatus();
                if (ServiceCtrl.ControlServiceState != ServiceState.Running)
                    return;

                var state = ServiceCtrl.ControlCommand.SetUserCommand(cmd);
                pageControlData.ControlServiceError = state == ServiceState.Unknown;
            }
            catch (Exception ex) { pageControlData.ControlTextError = ex.Message; }
        }

        #region Multi StateControl
        private void StateControl_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            pageControlData.ControlTextError = default;
            ServiceCtrl.CheckServiceStatus();
            switch (ServiceCtrl.ControlServiceState)
            {
                case ServiceState.Running:
                    {
                        try { ServiceCtrl.ControlCommand.StopService(); }
                        catch (Exception ex) { pageControlData.ControlTextError = ex.Message; }
                        break;
                    }
                case ServiceState.Paused:
                case ServiceState.Stopped:
                    {
                        try { ServiceCtrl.ControlCommand.StartService(); }
                        catch (Exception ex) { pageControlData.ControlTextError = ex.Message; }
                        break;
                    }
                case ServiceState.NotFound:
                    {
                        try
                        {
                            string path = System.IO.Path.GetDirectoryName(
                                Assembly.GetAssembly(typeof(PageControls)).Location);

                            if (string.IsNullOrEmpty(path))
                                break;

                            ServiceCtrl.ControlCommand.InstallAndStartService(
                                ServiceCtrl.ControlServiceTags.Display,
                                ServiceCtrl.ControlServiceTags.Description,
                                $"{path}{System.IO.Path.DirectorySeparatorChar}{ServiceCtrl.ControlServiceTags.Name}.exe",
                                true
                            );
                        }
                        catch (Exception ex) { pageControlData.ControlTextError = ex.Message; }
                        break;
                    }
                case ServiceState.None:
                case ServiceState.Unknown:
                    {
                        break;
                    }
            }
            ServiceCtrl.CheckServiceStatus();
        }
        private void StateControl_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlMultiBtnEnable;
        }
        #endregion

        #region Reload Config
        private void ReloadConfig_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(2);
        }
        private void ReloadConfig_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Rescan EPG
        private void RescanEPG_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(7);
        }
        private void RescanEPG_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Rescan M3U
        private void RescanM3U_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(3);
        }
        private void RescanM3U_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Rescan Video Local
        private void RescanVideoLocal_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(4);
        }
        private void RescanVideoLocal_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Rescan Audio Local
        private void RescanAudioLocal_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(5);
        }
        private void RescanAudioLocal_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Check M3U
        private void CheckM3U_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            CustomServiceCommand(6);
        }
        private void CheckM3U_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Uninstall
        private void Uninstall_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            pageControlData.ControlTextError = default;
            try { ServiceCtrl.ControlCommand.UninstallService(); }
            catch (Exception ex) { pageControlData.ControlTextError = ex.Message; }
            ServiceCtrl.CheckServiceStatus();
        }
        private void Uninstall_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlUninstallBtnEnable;
        }
        #endregion

        #region Copy Log
        private void CopyLog_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            pageControlData.CopyLogEntry();
        }
        private void CopyLog_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

        #region Fix Play Lists
        private async void FixPlayLists_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
        {
            try
            {
                ControlsDialogContent cdc = new ControlsDialogContent();
                ModernWpf.Controls.ContentDialog dialog = new ModernWpf.Controls.ContentDialog
                {
                    Title = $"{cdc.Tag}?",
                    PrimaryButtonText = ServiceCtrl.ControlLanguage.DialogMessage1,
                    CloseButtonText = ServiceCtrl.ControlLanguage.DialogMessage2,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Primary,
                    Content = cdc
                };

                ModernWpf.Controls.ContentDialogResult result = await dialog.ShowAsync();
                if (result == ModernWpf.Controls.ContentDialogResult.Primary)
                    CustomServiceCommand(7);
            } catch { }
        }
        private void FixPlayLists_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = pageControlData != default && (bool)pageControlData?.IsControlServiceRun;
        }
        #endregion

        #region Log Reader
        private void LogBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (pageControlData.LogBoxIndex >= 0)
                pageControlData.ReadLog();
        }
        #endregion
    }
}
