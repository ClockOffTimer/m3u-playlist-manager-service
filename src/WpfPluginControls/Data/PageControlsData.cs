﻿/* Copyright (c) 2021 WpfPluginControls, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define TRACE

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using PlayListServiceData.Base;
using PlayListServiceData.ServiceConfig;
using Swan;

namespace WpfPlugin.Data
{
    public class PageControlsData : INotifyPropertyChanged
    {
        private System.Timers.Timer timerLog;
        private System.Timers.Timer timerBusy;
        private string _ControlTextLogName = default,
                       _ControlTextLog = default,
                       _ControlTextError = default;
        private bool _ControlServiceError = default;
        private int _ControlServiceWaitCount = 0;

        private readonly List<Tuple<string, string>> _LogBase = new() {
            new Tuple<string, string>(BasePath.GetMainLogPath(), Path.GetFileNameWithoutExtension(BasePath.MainLog)),
            new Tuple<string, string>(BasePath.GetScanLogPath(), Path.GetFileNameWithoutExtension(BasePath.ScanLog)),
            new Tuple<string, string>(BasePath.GetScanLocalVideoLogPath(), Path.GetFileNameWithoutExtension(BasePath.ScanLocalVideoLog)),
            new Tuple<string, string>(BasePath.GetScanLocalAudioLogPath(), Path.GetFileNameWithoutExtension(BasePath.ScanLocalAudioLog)),
            new Tuple<string, string>(BasePath.GetScanTranslateAudioLogPath(), Path.GetFileNameWithoutExtension(BasePath.ScanTranslateAudioLog)),
            new Tuple<string, string>(BasePath.GetCheckLogPath(), Path.GetFileNameWithoutExtension(BasePath.CheckLog)),
            new Tuple<string, string>(BasePath.GetAudioEngineLogPath(), Path.GetFileNameWithoutExtension(BasePath.AudioEngineLog)),
            new Tuple<string, string>(BasePath.GetHttpLogPath(), Path.GetFileNameWithoutExtension(BasePath.HttpLog)),
            new Tuple<string, string>(BasePath.GetServiceInstallLogPath(), Path.GetFileNameWithoutExtension(BasePath.ServiceInstallLog)),
            new Tuple<string, string>(BasePath.GetUtilInstallLogPath(), Path.GetFileNameWithoutExtension(BasePath.UtilInstallLog))
        };

        public int SelectedLogIndex { get; set; } = -1;
        private ObservableCollection<Tuple<int, DateTime, string>> _LogItems = new();
        public ObservableCollection<Tuple<int, DateTime, string>> LogItems
        {
            get { return _LogItems; }
            private set { _LogItems = value; OnPropertyChanged(nameof(LogItems)); }
        }

        public int LogBoxIndex { get; set; } = -1;
        private ObservableCollection<string> _LogBoxItems = new();
        public ObservableCollection<string> LogBoxItems
        {
            get { return _LogBoxItems; }
            private set { _LogBoxItems = value; OnPropertyChanged(nameof(LogBoxItems)); }
        }

        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set
            {
                _ServiceCtrl = value;
                OnPropertyChanged(nameof(ServiceCtrl));
                OnPropertyChanged(nameof(IsControlServiceRun));
                OnPropertyChanged(nameof(IsControlMultiBtnEnable));
                OnPropertyChanged(nameof(IsControlUninstallBtnEnable));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageControlsData()
        {
            timerBusy = new System.Timers.Timer(1000)
            {
                Enabled = false
            };
            timerLog = new System.Timers.Timer(1000)
            {
                Enabled = false
            };
            foreach (var a in _LogBase)
                LogBoxItems.Add(a.Item2.Humanize());
        }
        ~PageControlsData()
        {
            if (ServiceCtrl != null)
                ServiceCtrl.PropertyChanged -= local_PropertyChanged;
            try
            {
                if (timerBusy != null)
                {
                    if (timerBusy.Enabled)
                        timerBusy.Stop();
                    timerBusy.Dispose();
                    timerBusy = null;
                }
            }
            catch { }
            try
            {
                if (timerLog != null)
                {
                    if (timerLog.Enabled)
                        timerLog.Stop();
                    timerLog.Dispose();
                    timerLog = null;
                }
            }
            catch { }
        }

        public void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
            ServiceCtrl.PropertyChanged += local_PropertyChanged;
            timerBusy.Enabled = true;
            timerBusy.AutoReset = true;
            timerBusy.Elapsed += (s, a) =>
            {
                int x = ControlServiceWaitCount + 10;
                ControlServiceWaitCount = (x > 60) ? 1 : x;
            };
            timerLog.Enabled = true;
            timerLog.AutoReset = true;
            timerLog.Elapsed += (s, a) =>
            {
                timerLog.Interval = 10000;
                getLog_();
            };
        }

        public void CopyLogEntry()
        {
            if ((SelectedLogIndex < 0) || (SelectedLogIndex >= LogItems.Count))
                return;
            Clipboard.SetText(LogItems[SelectedLogIndex].Item3);
        }

        #region get/set

        private bool _IsControlLogViewExpand = false;
        public bool IsControlLogViewExpand
        {
            get { return _IsControlLogViewExpand; }
            set
            {
                if (_IsControlLogViewExpand == value)
                    return;
                _IsControlLogViewExpand = value;
                OnPropertyChanged(nameof(IsControlLogViewExpand));
                OnPropertyChanged(nameof(IsControlBtnViewExpandCollapse));
            }
        }
        public bool IsControlBtnViewExpandCollapse
        {
            get { return !_IsControlLogViewExpand; }
        }

        public bool IsControlUninstallBtnEnable
        {
            get
            {
                return ServiceCtrl != null && ServiceCtrl.IsControlAdminRun && (ServiceCtrl.ControlServiceState == ServiceState.Stopped);
            }
        }

        public bool IsControlMultiBtnEnable
        {
            get
            {
                if (ServiceCtrl == null)
                    return false;

                switch (ServiceCtrl.ControlServiceState)
                {
                    case ServiceState.ContinuePending:
                    case ServiceState.PausePending:
                    case ServiceState.StartPending:
                    case ServiceState.StopPending:
                        {
                            return false;
                        }
                    default: return ServiceCtrl.IsControlAdminRun;
                }
            }
        }

        public bool IsControlServiceRun
        {
            get
            {
                return ServiceCtrl != null && (ServiceCtrl.IsControlAdminRun && (ServiceCtrl.ControlServiceState == ServiceState.Running));
            }
        }

        public string ControlTextLogName
        {
            get { return _ControlTextLogName; }
            set
            {
                if (_ControlTextLogName == value)
                    return;
                _ControlTextLogName = value;
                OnPropertyChanged(nameof(ControlTextLogName));
            }
        }

        public string ControlTextLog
        {
            get { return _ControlTextLog; }
            set
            {
                if (_ControlTextLog == value)
                    return;
                _ControlTextLog = value;
                OnPropertyChanged(nameof(ControlTextLog));
            }
        }

        public string ControlTextError
        {
            get { return _ControlTextError; }
            set
            {
                if (_ControlTextError == value)
                    return;
                _ControlTextError = value;
                _ControlServiceError = !string.IsNullOrWhiteSpace(ControlTextError);
                OnPropertyChanged(nameof(ControlTextError));
                OnPropertyChanged(nameof(ControlServiceErrorVisibility));
            }
        }

        public int ControlServiceWaitCount
        {
            get { return _ControlServiceWaitCount; }
            set
            {
                if (_ControlServiceWaitCount == value)
                    return;
                _ControlServiceWaitCount = value;
                OnPropertyChanged(nameof(ControlServiceWaitCount));
            }
        }

        public bool ControlServiceError
        {
            get { return _ControlServiceError; }
            set
            {
                if (_ControlServiceError == value)
                    return;
                _ControlServiceError = value;
                OnPropertyChanged(nameof(ControlServiceError));
                OnPropertyChanged(nameof(ControlServiceErrorVisibility));
            }
        }
        public Visibility ControlServiceErrorVisibility
        {
            get { return _ControlServiceError ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility ControlServiceWaitBtnVisibility
        {
            get { return (ControlServiceWaitCount == 0) ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility ControlServiceWaitBarVisibility
        {
            get { return (ControlServiceWaitCount > 0) ? Visibility.Visible : Visibility.Collapsed; }
        }

        #endregion

        #region public

        public async void ReadLog()
        {
            int idx = LogBoxIndex;
            if (idx < 0 || idx > LogBoxItems.Count)
                return;
            try
            {
                using FileStream r = File.Open(_LogBase[idx].Item1, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using StreamReader sr = new(r, new UTF8Encoding(false));
                ControlTextLog = await sr.ReadToEndAsync();
                ControlTextLogName = LogBoxItems[idx];
            }
            catch { ControlTextLogName = default; ControlTextLog = ""; }
        }
        #endregion

        #region private

        private async void getLog_()
        {
            await getLog();
        }
        private async Task getLog()
        {
            await Task.Run(() =>
            {
                try
                {
                    EventLog eventLog = new()
                    {
                        Log = ServiceCtrl.ControlServiceTags.SysLogName,
                        Source = ServiceCtrl.ControlServiceTags.Name
                    };
                    foreach (var i in from EventLogEntry i in eventLog.Entries
                                      where i.Source.Equals(ServiceCtrl.ControlServiceTags.Name)
                                      select i)
                    {
                        var t = (from n in LogItems
                                 where n.Item1 == i.Index
                                 select n).FirstOrDefault();
                        if (t != null)
                            continue;

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            LogItems.Insert(0, new Tuple<int, DateTime, string>(i.Index, i.TimeGenerated, i.Message));
                            OnPropertyChanged(nameof(LogItems));
                        });
                    }

                    eventLog.Close();
                    eventLog.Dispose();
                }
                catch (Exception e) { Debug.WriteLine($"{e}"); }
            });
        }

        private void local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsControlAdminRun":
                    {
                        OnPropertyChanged(nameof(IsControlServiceRun));
                        OnPropertyChanged(nameof(IsControlUninstallBtnEnable));
                        break;
                    }
                case "ControlServiceState":
                    {
                        if (ServiceCtrl == default)
                            return;
                        if (ServiceCtrl.ControlServiceState == ServiceState.Stopped)
                            OnPropertyChanged(nameof(IsControlUninstallBtnEnable));
                        else if (ServiceCtrl.ControlServiceState == ServiceState.Running)
                            OnPropertyChanged(nameof(IsControlServiceRun));
                        else if (ServiceCtrl.ControlServiceState == ServiceState.NotFound)
                        {
                            OnPropertyChanged(nameof(IsControlServiceRun));
                            OnPropertyChanged(nameof(IsControlMultiBtnEnable));
                            OnPropertyChanged(nameof(IsControlUninstallBtnEnable));
                        }

                        if ((ServiceCtrl.ControlServiceState == ServiceState.StartPending) ||
                            (ServiceCtrl.ControlServiceState == ServiceState.StopPending) ||
                            (ServiceCtrl.ControlServiceState == ServiceState.PausePending) ||
                            (ServiceCtrl.ControlServiceState == ServiceState.ContinuePending))
                        {
                            try
                            {
                                if (!timerBusy.Enabled)
                                {
                                    ControlServiceWaitCount = 1;
                                    OnPropertyChanged(nameof(ControlServiceWaitBtnVisibility));
                                    OnPropertyChanged(nameof(ControlServiceWaitBarVisibility));
                                    OnPropertyChanged(nameof(ControlServiceWaitCount));

                                    timerBusy.Enabled = true;
                                    timerBusy.Start();
                                }
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                if (timerBusy.Enabled)
                                {
                                    ControlServiceWaitCount = 0;
                                    OnPropertyChanged(nameof(ControlServiceWaitBtnVisibility));
                                    OnPropertyChanged(nameof(ControlServiceWaitBarVisibility));
                                    OnPropertyChanged(nameof(ControlServiceWaitCount));

                                    timerBusy.Enabled = false;
                                    timerBusy.Stop();
                                }
                            }
                            catch { }
                        }
                        break;
                    }
            }
        }
        #endregion
    }
}
