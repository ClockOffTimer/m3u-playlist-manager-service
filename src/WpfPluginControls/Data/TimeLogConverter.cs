﻿/* Copyright (c) 2021 WpfPluginControls, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfPlugin.Data
{
    [ValueConversion(typeof(string), typeof(string))]
    public class TimeLogConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is DateTime date)
                return date.ToString("MM/dd HH:mm");
            return "?";
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
