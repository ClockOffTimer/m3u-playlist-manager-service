﻿/* Copyright (c) 2021 WpfPluginControls, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfPlugin.Data
{
    [ValueConversion(typeof(double), typeof(double))]
    public class DimensionsConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object param, CultureInfo culture)
        {
            try
            {
                if ((value is double d0) && (param is string s))
                {
                    if (d0 <= 0)
                        return (double)0;
                    if (double.TryParse(s, out double d1))
                        return (d0 > d1) ? (d0 - d1) : d0;
                }
            } catch { }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
