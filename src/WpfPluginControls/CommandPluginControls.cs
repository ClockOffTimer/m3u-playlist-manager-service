﻿/* Copyright (c) 2021 WpfPluginControls, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */
using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginControls
	{
		public static readonly RoutedUICommand StateControl = new RoutedUICommand
			(
				"",
				"StateControl",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand ReloadConfig = new RoutedUICommand
			(
				"",
				"ReloadConfig",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand RescanM3U = new RoutedUICommand
			(
				"",
				"RescanM3U",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand RescanVideoLocal = new RoutedUICommand
			(
				"",
				"RescanVideoLocal",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand RescanAudioLocal = new RoutedUICommand
			(
				"",
				"RescanAudioLocal",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand RescanEPG = new RoutedUICommand
			(
				"",
				"RescanEPG",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand CheckM3U = new RoutedUICommand
			(
				"",
				"CheckM3U",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand Uninstall = new RoutedUICommand
			(
				"",
				"Uninstall",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand CopyLog = new RoutedUICommand
			(
				"",
				"CopyLog",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand FixPlayLists = new RoutedUICommand
			(
				"",
				"FixPlayLists",
				typeof(CommandPluginControls),
				new InputGestureCollection()
			);
	}
}
