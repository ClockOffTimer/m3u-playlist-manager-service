﻿using System.Windows.Controls;

namespace WpfPlugin
{
    public partial class ControlsDialogContent : UserControl
    {
        public ControlsDialogContent()
        {
            InitializeComponent();
            DataContext = this;
        }
    }
}
