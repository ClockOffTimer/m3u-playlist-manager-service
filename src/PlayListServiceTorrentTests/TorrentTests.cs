﻿
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PlayListServiceTorrent.Tests
{
    [TestClass()]
    public class TorrentTests
    {
        readonly string localPathTopDirectory = @"..\..\..\PlayListService\config";
        readonly string torrentPathTopDirectory = "config.torrent";

        readonly string localPathAllDirectory = @"..\..\..\packages";
        readonly string torrentPathAllDirectory = "packages.torrent";

        readonly List<string> trackers;

        public TorrentTests()
        {
            trackers = File.ReadAllLines($@"{localPathTopDirectory}\TTrackers.list").ToList();
            Assert.IsNotNull(trackers);
            Assert.IsTrue(trackers.Count > 0);
            System.Diagnostics.Debug.WriteLine($"- Root dir: {Directory.GetCurrentDirectory()}");
        }

        [TestMethod()]
        public void TorrentCreate_AllDirectory()
        {
            Torrent torrent = Torrent.Create(
                localPathAllDirectory,
                trackers.ToList(),
                "My comment" /*comment*/,
                "*.*" /*dirmask*/,
                SearchOption.AllDirectories);

            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
            torrent.Save(@"..\Debug", Path.GetFileNameWithoutExtension(torrentPathAllDirectory));

            _ = TorrentFileTest(torrentPathAllDirectory);
        }

        [TestMethod()]
        public void TorrentCreate_TopDirectory()
        {
            Torrent torrent = Torrent.Create(
                localPathTopDirectory,
                trackers.ToList(),
                default /*comment*/,
                "*.xml" /*dirmask*/,
                SearchOption.TopDirectoryOnly);

            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
            torrent.Save(@"..\Debug");

            _ = TorrentFileTest(torrentPathTopDirectory);
        }

        [TestMethod()]
        public void TorrentLoad_AllDirectory()
        {
            FileInfo f = TorrentFileTest(torrentPathAllDirectory);

            Torrent torrent = Torrent.LoadTorrent(f.FullName, localPathAllDirectory);
            Assert.IsNotNull(torrent);
            Assert.IsTrue(torrent.Verify());

            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
        }

        [TestMethod()]
        public void TorrentLoad_TopDirectory()
        {
            FileInfo f = TorrentFileTest(torrentPathTopDirectory);

            Torrent torrent = Torrent.LoadTorrent(f.FullName, localPathTopDirectory);
            Assert.IsNotNull(torrent);
            Assert.IsTrue(torrent.Verify());
            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
        }

        [TestMethod()]
        public void TorrentLoad_AllDirectory_NoVerify()
        {
            FileInfo f = TorrentFileTest(torrentPathAllDirectory);

            Torrent torrent = Torrent.LoadTorrent(f.FullName, string.Empty);
            Assert.IsNotNull(torrent);
            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
        }

        [TestMethod()]
        public void TorrentLoad_TopDirectory_NoVerify()
        {
            FileInfo f = TorrentFileTest(torrentPathTopDirectory);

            Torrent torrent = Torrent.LoadTorrent(f.FullName, string.Empty);
            Assert.IsNotNull(torrent);
            System.Diagnostics.Debug.WriteLine(torrent.ToDetailedString());
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
        }

        [TestMethod()]
        public void TorrentMagnetLinkTest()
        {
            FileInfo f = TorrentFileTest(torrentPathTopDirectory);

            Torrent torrent = Torrent.LoadTorrent(f.FullName, string.Empty);
            Assert.IsNotNull(torrent);
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(false, false));
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, false));
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(false, true));
            System.Diagnostics.Debug.WriteLine(torrent.GetMagnetLink(true, true));
        }

        private FileInfo TorrentFileTest(string file)
        {
            System.Diagnostics.Debug.WriteLine($"- Open Torrent file: {file}");
            FileInfo f = new FileInfo(file);
            Assert.IsNotNull(f);
            if (!f.Exists)
                Assert.Fail();
            Assert.IsTrue(f.Length > 0L);
            System.Diagnostics.Debug.WriteLine($"- Torrent file size: {Torrent.BytesToString(f.Length)}");
            return f;
        }
    }
}