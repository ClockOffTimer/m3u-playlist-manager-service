﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace PlayListServiceTorrent.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class TorrentWebResponse
    {
        [XmlElement("torrentname")]
        public string TorrentName = string.Empty;
        [XmlElement("torrenturl")]
        public string TorrentUrl = string.Empty;
        [XmlElement("magnetlink")]
        public string MagnetLink = string.Empty;
        [XmlElement("torrentpath")]
        public string TorrentPath = string.Empty;
        [XmlElement("magnetpath")]
        public string MagnetPath = string.Empty;
        [XmlElement("error")]
        public string Errors = string.Empty;
        [XmlIgnore]
        public bool IsErrors => !string.IsNullOrWhiteSpace(Errors);

        public TorrentWebResponse Copy(Torrent tr, string url = default, bool ismlname = false)
        {
            if (tr == default)
            {
                Errors = Properties.Resources.E8;
                return this;
            }
            TorrentUrl = (url == default) ? string.Empty : url;
            MagnetPath = tr.SavedMagnetPath;
            MagnetLink = tr.GetMagnetLink(true, ismlname);
            TorrentPath = tr.SavedTorrentPath;
            TorrentName = string.IsNullOrWhiteSpace(TorrentPath) ? string.Empty : Path.GetFileName(TorrentPath);
            return this;
        }
    }
}
