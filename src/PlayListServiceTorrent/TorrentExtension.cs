﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */


using System.Collections.Generic;
using System.IO;

namespace PlayListServiceTorrent
{
    public static class TorrentExtension
    {
        #region File Import/Export extension
        public static Torrent Load(this string filePath) => Torrent.LoadTorrent(filePath, Path.GetDirectoryName(filePath));
        public static Torrent Load(this string filePath, string srcPath) => Torrent.LoadTorrent(filePath, srcPath);

        public static void Save(this Torrent torrent) => torrent.SaveTorrent();
        public static void Save(this Torrent torrent, string filePath) => torrent.SaveTorrent(filePath);
        public static void Save(this Torrent torrent, string filePath, string fileName) => torrent.SaveTorrent(filePath, fileName);

        public static Torrent CreateFromDirectory(this string srcPath, List<string> trackers) =>
            Torrent.Create(srcPath, trackers, default, "*.*", SearchOption.TopDirectoryOnly);
        public static Torrent CreateFromDirectory(this string srcPath, List<string> trackers, string dirMask, SearchOption opt) =>
            Torrent.Create(srcPath, trackers, default, dirMask, opt);
        public static Torrent CreateFromDirectory(this string srcPath, List<string> trackers, string dirMask, SearchOption opt, string comment) =>
            Torrent.Create(srcPath, trackers, comment, dirMask, opt);

        public static Torrent CreateFromFile(this string srcPath, List<string> trackers, string comment) =>
            Torrent.Create(srcPath, trackers, comment, default, SearchOption.TopDirectoryOnly);
        public static Torrent CreateFromFile(this string srcPath, List<string> trackers) =>
            Torrent.Create(srcPath, trackers, default, default, SearchOption.TopDirectoryOnly);

        #endregion

        #region Internal extension
        public static byte [] GetBytes(this Torrent t, string s) => t.UtfEncoding.GetBytes(s);
        #endregion
    }
}
