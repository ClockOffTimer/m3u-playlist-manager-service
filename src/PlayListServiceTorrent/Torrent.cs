﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define MAGNET_LINK

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace PlayListServiceTorrent
{
    public class Torrent
    {
        #region Properties
        public static string CreatorName { get; set; } = "Torrent Creater";
        public static string SavePath { get; set; } = string.Empty;

        public string Name { get; private set; }
        public bool? IsPrivate { get; private set; }
        public List<TorrentFileItem> SourceFiles { get; private set; } = new List<TorrentFileItem>();
        public string SourceDirectory { get; private set; }
        public string SourceRootDirectory {
            get => string.IsNullOrWhiteSpace(SourceDirectory) ?
                string.Empty : Path.GetDirectoryName(SourceDirectory);
        }
        public string SavedTorrentPath { get; private set; } = string.Empty;
        public string SavedMagnetPath {
            get => string.IsNullOrWhiteSpace(SavedTorrentPath) ?
                string.Empty : SavedTorrentPath.Replace(TorrentExt, MagnetExt);
        }

        public List<string> Trackers { get; } = new List<string>();
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        public Encoding UtfEncoding { get; set; } = new UTF8Encoding(false);

        public int  BlockSize  { get; private set; } = 16384;
        public int  PieceSize  { get; private set; } = 32768;
        public int  PieceCount { get => PieceHashes.Length; }
        public long TotalSize  { get => SourceFiles.Sum(x => x.Size); }


        public byte[][] PieceHashes { get; private set; }
        public bool[]   IsPieceVerified { get; private set; }
        public bool[][] IsBlockAcquired { get; private set; }

#       if DEBUG
        public string FormattedPieceSize { get => BytesToString(PieceSize); }
        public string FormattedTotalSize { get => BytesToString(TotalSize); }

        public string VerifiedPiecesString { get => string.Join("", IsPieceVerified.Select(x => x ? 1 : 0)); }
        public int VerifiedPieceCount { get => IsPieceVerified.Count(x => x); }
        public double VerifiedRatio { get => VerifiedPieceCount / (double)PieceCount; }
#       endif
        #endregion

        #region Magnet Link
#       if MAGNET_LINK || DEBUG
        public byte[] Infohash { get; private set; } = new byte[20];
        public string HexStringInfohash { get => string.Join("", Infohash.Select(x => x.ToString("x2"))); }
        public string UrlSafeStringInfohash { get => UtfEncoding.GetString(WebUtility.UrlEncodeToBytes(Infohash, 0, 20)); }
#       endif
        #endregion

        #region private fields
        private const string TorrentExt = ".torrent";
        private const string MagnetExt  = ".magnet";
        private readonly SHA1 sha1 = SHA1.Create();
        #endregion

        #region Constructor
        public Torrent(
            string name,
            string location,
            List<TorrentFileItem> files,
            List<string> trackers,
            int pieceSize = 32768,
            int blockSize = 16384,
            byte[] pieceHashes = null,
            bool? isPrivate = false)
        {
            Name = name;
            SourceFiles = files;
            SourceDirectory = location;
            PieceSize = (pieceSize > 0) ? pieceSize : PieceSize;
            BlockSize = (blockSize > 0) ? blockSize : BlockSize;
            IsPrivate = isPrivate;

            if ((trackers != null) && (trackers.Count > 0))
            {
                Trackers = (from i in trackers
                            where !string.IsNullOrWhiteSpace(i)
                            select i).ToList();
            }

            int count = Convert.ToInt32(Math.Ceiling(TotalSize / Convert.ToDouble(PieceSize)));
            if (count <= 0)
                throw new TorrentException(nameof(count));

            PieceHashes = new byte[count][];
            IsPieceVerified = new bool[count];
            IsBlockAcquired = new bool[count][];

            for (int i = 0; i < PieceCount; i++)
                IsBlockAcquired[i] = new bool[GetBlockCount(i)];

            if (pieceHashes == null)
                for (int i = 0; i < PieceCount; i++) {
                    PieceHashes[i] = GetHash(i);
                    if (PieceHashes[i] == null) PieceHashes[i] = new byte[20];
                }
            else
                for (int i = 0; i < PieceCount; i++) {
                    PieceHashes[i] = new byte[20];
                    Buffer.BlockCopy(pieceHashes, i * 20, PieceHashes[i], 0, 20);
                }

#           if MAGNET_LINK || DEBUG
            {
                object info = TorrentInfoToBEncodingObject(this);
                byte[] bytes = TorrentEncoding.Encode(info, UtfEncoding);
                Infohash = SHA1.Create().ComputeHash(bytes);
            }
#           endif

            if (!string.IsNullOrWhiteSpace(SourceDirectory))
                for (int i = 0; i < PieceCount; i++)
                    VerifyPiece(i);
        }
        #endregion

        #region Creation
        public static Torrent Create(string srcpath, List<string> trackers, string comment, string dirmask, SearchOption diropt)
        {
            if (string.IsNullOrWhiteSpace(srcpath))
                throw new TorrentSourceEmptyException(nameof(srcpath));

            DateTime date = DateTime.Now;
            string name,
                   fullpath,
                   path = srcpath.Normalize(NormalizationForm.FormC);
            List<TorrentFileItem> files;

            do
            {
                DirectoryInfo dir = new(path);
                if ((dir != default) && dir.Exists)
                {
                    if (string.IsNullOrWhiteSpace(dirmask))
                        dirmask = "*.*";

                    name = dir.Name;
                    fullpath = dir.FullName;

                    if (diropt == SearchOption.TopDirectoryOnly)
                        files = CreateList_TopDirectory(dir, dirmask);
                    else
                        files = CreateList_AllDirectories(dir, dirmask);
                    break;
                }
                FileInfo file = new(path);
                if ((file == default) || !file.Exists)
                    throw new TorrentSourceNotFoundException(nameof(path));

                files = new();
                files.Add(new TorrentFileItem()
                {
                    Path = Path.GetFileName(path),
                    Size = file.Length
                });
                name = Path.GetFileNameWithoutExtension(path);
                fullpath = file.FullName;

            } while (false);

            if (string.IsNullOrWhiteSpace(name))
                throw new TorrentSourceEmptyException(nameof(name));
            if (string.IsNullOrWhiteSpace(fullpath))
                throw new TorrentSourceEmptyException(nameof(fullpath));
            if (files == default)
                throw new TorrentSourceEmptyException(nameof(files));
            if (string.IsNullOrWhiteSpace(comment))
                comment = $"{Properties.Resources.S1} {date.ToString("dddd, dd MMMM yyyy")}, {name}";

            Torrent torrent = new(name, fullpath, files, trackers)
            {
                Comment = comment,
                CreatedBy = Torrent.CreatorName,
                CreationDate = date
            };
            return torrent;
        }

        private static List<TorrentFileItem> CreateList_TopDirectory(DirectoryInfo dir, string dirmask)
        {
            long running = 0;
            List<TorrentFileItem> files = new();
            foreach (FileInfo fi in dir.EnumerateFiles(dirmask, SearchOption.TopDirectoryOnly))
            {
                try
                {
                    files.Add(new TorrentFileItem(fi.Name, fi.Length, running));
                    running += fi.Length;
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
            }
            return files;
        }
        private static List<TorrentFileItem> CreateList_AllDirectories(DirectoryInfo dir, string dirmask)
        {
            long running = 0;
            List<TorrentFileItem> files = new();
            foreach (FileInfo fi in dir.EnumerateFiles(dirmask, SearchOption.AllDirectories))
            {
                try
                {
                    files.Add(new TorrentFileItem(
                        fi.FullName.Replace($"{dir.FullName}{Path.DirectorySeparatorChar}", ""),
                        fi.Length,
                        running));
                    running += fi.Length;
                }
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
            }
            return files;
        }
        #endregion

        #region File Import/Export
        public static Torrent LoadTorrent(string filePath, string sourcePath)
        {
            if (!string.IsNullOrWhiteSpace(sourcePath)) {
                DirectoryInfo dir = new(sourcePath);
                if ((dir == null) || !dir.Exists)
                    throw new TorrentBadDirectoryException(sourcePath);
                sourcePath = dir.FullName;
            }

            FileInfo file = new(filePath);
            if ((file == null) || !file.Exists)
                throw new TorrentBadFileException(filePath);

            Encoding utf = new UTF8Encoding(false);
            return BEncodingObjectToTorrent(
                TorrentEncoding.DecodeFile(file.FullName, utf),
                Path.GetFileNameWithoutExtension(file.FullName),
                sourcePath,
                utf);
        }
        public void SaveTorrent(string savePath = default, string saveName = default)
        {
            string torrentName = string.IsNullOrWhiteSpace(saveName) ?
                        $"{Name}{TorrentExt}" :
                        (saveName.EndsWith(TorrentExt) ? saveName : $"{saveName}{TorrentExt}"),
                   fileName = (!string.IsNullOrWhiteSpace(savePath)) ?
                    Path.Combine(savePath, torrentName) :
                    (!string.IsNullOrWhiteSpace(SavePath) ?
                        Path.Combine(SavePath, torrentName) :
                        Path.Combine(SourceRootDirectory, torrentName));

            TorrentEncoding.EncodeToFile(
                TorrentToBEncodingObject(this),
                fileName,
                UtfEncoding);

            SavedTorrentPath = fileName;
        }
        #endregion

        #region File Read
        public byte[] Read(long start, int length)
        {
            long end = start + length;
            byte[] buffer = new byte[length];

            for (int i = 0; i < SourceFiles.Count; i++)
            {
                string path = Path.Combine(SourceDirectory, SourceFiles[i].Path);
                if (!File.Exists(path))
                    continue;

                if ((start < SourceFiles[i].Offset && end < SourceFiles[i].Offset) ||
                    (start > SourceFiles[i].Offset + SourceFiles[i].Size && end > SourceFiles[i].Offset + SourceFiles[i].Size))
                    continue;

                long fstart = Math.Max(0, start - SourceFiles[i].Offset);
                long fend = Math.Min(end - SourceFiles[i].Offset, SourceFiles[i].Size);
                int flength = Convert.ToInt32(fend - fstart);
                int bstart = Math.Max(0, Convert.ToInt32(SourceFiles[i].Offset - start));

                using Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                stream.Seek(fstart, SeekOrigin.Begin);
                stream.Read(buffer, bstart, flength);
            }
            return buffer;
        }
        #endregion

        #region Verification
        public bool Verify()
        {
            if ((PieceCount <= 0) ||
                (PieceHashes == null) ||
                (IsPieceVerified == null) ||
                (IsBlockAcquired == null))
                return false;

            bool isVerified = false;
            for (int i = 0; i < PieceCount; i++)
                isVerified = VerifyPiece(i);
            return isVerified;
        }
        private bool VerifyPiece(int piece)
        {
            byte[] hash;
            hash = GetHash(piece);

            bool isVerified = (hash != null) && hash.SequenceEqual(PieceHashes[piece]);
            if (isVerified)
            {
                IsPieceVerified[piece] = true;

                for (int j = 0; j < IsBlockAcquired[piece].Length; j++)
                    IsBlockAcquired[piece][j] = true;
                return isVerified;
            }

            IsPieceVerified[piece] = false;
            if (IsBlockAcquired[piece].All(x => x))
            {
                for (int j = 0; j < IsBlockAcquired[piece].Length; j++)
                    IsBlockAcquired[piece][j] = false;
            }
            return isVerified;
        }

        public byte[] GetHash(int piece)
        {
            byte[] data = Read(piece * PieceSize, GetPieceSize(piece));

            if (data == null)
                return null;

            return sha1.ComputeHash(data);
        }
        #endregion

        #region Get Magnet Link
        public string GetMagnetLink(bool isTrackers = true, bool isName = true)
        {
            try {
                if (Infohash == null)
                    return string.Empty;

                StringBuilder sb = new();
                sb.Append($"magnet:?xt=urn:btih:{HexStringInfohash}");
                if (isName)
                    sb.Append($@"&dn={WebUtility.UrlEncode(Name)}");
                if (isTrackers && (Trackers.Count > 0))
                {
                    foreach (string s in Trackers)
                        if (!string.IsNullOrWhiteSpace(s))
                            sb.Append($@"&tr={WebUtility.UrlEncode(s)}");
                }
                return sb.ToString();
            }
            catch { return string.Empty; }
        }
        #endregion

        #region BEncoding Internal
        private static object TorrentToBEncodingObject(Torrent torrent)
        {
            Dictionary<string, object> dict = new();

            if (torrent.Trackers.Count > 0)
                dict["announce"] = torrent.GetBytes(torrent.Trackers[0]);
            if (torrent.Trackers.Count >= 1)
            {
                List<object> trackersList = new();
                for (int i = 1; i < torrent.Trackers.Count; i++)
                    if (!string.IsNullOrWhiteSpace(torrent.Trackers[i]))
                        trackersList.Add(new string[] { torrent.Trackers[i] });
                dict["announce-list"] = trackersList;
            }
            dict["comment"] = torrent.GetBytes(torrent.Comment);
            dict["created by"] = torrent.GetBytes(torrent.CreatedBy);
            dict["creation date"] = DateTimeToUnixTimestamp(torrent.CreationDate);
            dict["encoding"] = torrent.GetBytes(torrent.UtfEncoding.WebName.ToUpper());
            dict["info"] = TorrentInfoToBEncodingObject(torrent);
            return dict;
        }

        private static object TorrentInfoToBEncodingObject(Torrent torrent)
        {
            Dictionary<string, object> dict = new();

            dict["piece length"] = (long)torrent.PieceSize;
            byte[] pieces = new byte[20 * torrent.PieceCount];
            for (int i = 0; i < torrent.PieceCount; i++)
                Buffer.BlockCopy(torrent.PieceHashes[i], 0, pieces, i * 20, 20);
            dict["pieces"] = pieces;

            if (torrent.IsPrivate.HasValue)
                dict["private"] = torrent.IsPrivate.Value ? 1L : 0L;

            if (torrent.SourceFiles.Count == 1)
            {
                TorrentFileItem item = torrent.SourceFiles[0];
                if (item.Path.IndexOf(Path.DirectorySeparatorChar) != -1)
                {
                    List<object> files = new();
                    files.Add(item.EncodeDictionaryFilePath());
                    dict["files"] = files;
                }
                else
                {
                    dict["name"] = torrent.GetBytes(item.Path);
                    dict["length"] = item.Size;
                }
            }
            else
            {
                List<object> files = new();
                foreach (var f in torrent.SourceFiles)
                    files.Add(f.EncodeDictionaryFilePath());

                dict["files"] = files;
                dict["name"] = torrent.GetBytes(torrent.Name);
            }
            return dict;
        }

        private static Torrent BEncodingObjectToTorrent(object bencoding, string name, string downloadPath, Encoding utf)
        {
            Dictionary<string, object> obj = (Dictionary<string, object>)bencoding;

            if (obj == null)
                throw new TorrentBadFileSpecificationException("dictionary");

            if (!obj.ContainsKey("info"))
                throw new TorrentBadFileSpecificationException("info section");

            List<string> trackers = new();
            if (obj.ContainsKey("announce"))
                trackers.Add(DecodeUTF8String(obj["announce"], utf));

            if (obj.ContainsKey("announce-list"))
                foreach (object item in (List<object>)obj["announce-list"])
                {
                    if (item is not List<object> list)
                        throw new TorrentBadFileSpecificationException("announce-list");
                    if (list.Count == 0)
                        continue;
                    trackers.Add(DecodeUTF8String(list[0], utf));
                }

            Dictionary<string, object> info = (Dictionary<string, object>)obj["info"];
            if (info == null)
                throw new TorrentBadFileSpecificationException("object info");

            List<TorrentFileItem> files = new();
            if (info.ContainsKey("name") && info.ContainsKey("length"))
            {
                files.Add(new TorrentFileItem()
                {
                    Path = DecodeUTF8String(info["name"], utf),
                    Size = (long)info["length"]
                });
            }
            else if (info.ContainsKey("files"))
            {
                long running = 0;

                foreach (object item in (List<object>)info["files"])
                {
                    if ((item is not Dictionary<string, object> dict) || !dict.ContainsKey("path") || !dict.ContainsKey("length"))
                        throw new TorrentBadFileSpecificationException("path or length");

                    string path = string.Join(
                        Path.DirectorySeparatorChar.ToString(),
                        ((List<object>)dict["path"]).Select(x => DecodeUTF8String(x, utf)));

                    long size = (long)dict["length"];

                    files.Add(new TorrentFileItem(path, size, running));
                    running += size;
                }
            }
            else
                throw new TorrentBadFileSpecificationException("files");

            if (!info.ContainsKey("piece length"))
                throw new TorrentBadFileSpecificationException("piece length");
            if (!info.ContainsKey("pieces"))
                throw new TorrentBadFileSpecificationException("pieces");

            int pieceSize = Convert.ToInt32(info["piece length"]);
            byte[] pieceHashes = (byte[])info["pieces"];
            bool? isPrivate = null;
            if (info.ContainsKey("private"))
                isPrivate = ((long)info["private"]) == 1L;

            Torrent torrent = new(name, downloadPath, files, trackers, pieceSize, 16384, pieceHashes, isPrivate);

            if (obj.ContainsKey("comment"))
                torrent.Comment = DecodeUTF8String(obj["comment"], utf);

            if (obj.ContainsKey("created by"))
                torrent.CreatedBy = DecodeUTF8String(obj["created by"], utf);

            if (obj.ContainsKey("creation date"))
                torrent.CreationDate = UnixTimeStampToDateTime(Convert.ToDouble(obj["creation date"]));

            if (obj.ContainsKey("encoding"))
                torrent.UtfEncoding = Encoding.GetEncoding(DecodeUTF8String(obj["encoding"], utf));

            return torrent;
        }
        #endregion

        #region Size Helpers
        public int GetPieceSize(int piece)
        {
            if (piece == PieceCount - 1)
            {
                int remainder = Convert.ToInt32(TotalSize % PieceSize);
                if (remainder != 0)
                    return remainder;
            }
            return PieceSize;
        }

        public int GetBlockSize(int piece, int block)
        {
            if (block == GetBlockCount(piece) - 1)
            {
                int remainder = Convert.ToInt32(GetPieceSize(piece) % BlockSize);
                if (remainder != 0)
                    return remainder;
            }
            return BlockSize;
        }

        public int GetBlockCount(int piece)
        {
            return Convert.ToInt32(Math.Ceiling(GetPieceSize(piece) / (double)BlockSize));
        }
        #endregion

        #region Helpers
        public static string DecodeUTF8String(object obj, Encoding utf)
        {
            if (obj is byte[] b)
                return utf.GetString(b);
            throw new TorrentException(Properties.Resources.E1);
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            DateTime dtDateTime = new(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        public static long DateTimeToUnixTimestamp(DateTime time)
        {
            return Convert.ToInt64((time == default) ?
                    DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds :
                    time.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds);
        }

        public static string BytesToString(long bytes)
        {
            string[] units = {
                Properties.Resources.B0,
                Properties.Resources.B1,
                Properties.Resources.B2,
                Properties.Resources.B3,
                Properties.Resources.B4,
                Properties.Resources.B5,
                Properties.Resources.B6
            };
            if (bytes == 0)
                return "0" + units[0];
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, 1024)));
            double num = Math.Round(bytes / Math.Pow(1024, place), 1);
            return $"{num} {units[place]}";
        }
        #endregion

        #region To String
#if DEBUG
        public override string ToString()
        {
            return string.Format("[Torrent: {0} {1} ({2}x{3}) Verified {4}/{5} ({6:P1}) {7}]",
                Name,
                FormattedTotalSize,
                PieceCount,
                FormattedPieceSize,
                VerifiedPieceCount,
                PieceCount,
                VerifiedRatio,
                HexStringInfohash);
        }

        public string ToDetailedString()
        {
            StringBuilder sb = new();
            sb.AppendLine("Torrent: ".PadRight(15) + Name);
            sb.AppendLine("Size: ".PadRight(15) + FormattedTotalSize);
            sb.AppendLine("Pieces: ".PadRight(15) + $"{PieceCount}x{FormattedPieceSize}");
            sb.AppendLine("Verified: ".PadRight(15) + $"{VerifiedPieceCount}/{PieceCount} ({VerifiedRatio.ToString("P1")})");
            sb.AppendLine("Hash: ".PadRight(15) + HexStringInfohash);
            sb.AppendLine("Created By: ".PadRight(15) + CreatedBy);
            sb.AppendLine("Creation Date: ".PadRight(15) + CreationDate);
            sb.AppendLine("Comment: ".PadRight(15) + Comment);
            if (Trackers.Count  > 0)
            {
                string [] trackers = (from i in Trackers
                                      where !string.IsNullOrWhiteSpace(i)
                                      select i).ToArray();
                sb.AppendLine("Trackers: ".PadRight(15) + trackers.Length);
                for (int i = 0; i < trackers.Length; i++)
                    sb.AppendLine(("- " + (i + 1) + ":").PadRight(15) + $"{trackers[i]}");
            }
            if (SourceFiles.Count == 1)
                sb.AppendLine("File: ".PadRight(15) + $"{SourceFiles[0].Path} ({SourceFiles[0].FormattedSize})");
            else
            {
                sb.AppendLine("Files: ".PadRight(15) + SourceFiles.Count);
                string displayPath = string.IsNullOrWhiteSpace(SourceDirectory) ? Name : SourceDirectory;
                for (int i = 0; i < SourceFiles.Count; i++)
                    sb.AppendLine(("- " + (i + 1) + ":").PadRight(15) + $"{displayPath}\\{SourceFiles[i].Path} ({SourceFiles[i].FormattedSize})");
            }
            return sb.ToString();
        }
#       else
        public string ToDetailedString() => ToString();
#       endif
        #endregion
    }
}
