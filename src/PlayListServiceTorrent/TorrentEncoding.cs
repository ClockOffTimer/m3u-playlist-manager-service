﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PlayListServiceTorrent
{

    public static class TorrentEncoding
    {
        private const byte DictionaryStart = (byte)'d';     // 100
        private const byte DictionaryEnd = (byte)'e';       // 101
        private const byte ListStart = (byte)'l';           // 108
        private const byte ListEnd = (byte)'e';             // 101
        private const byte NumberStart = (byte)'i';         // 105
        private const byte NumberEnd = (byte)'e';           // 101
        private const byte ByteArrayDivider = (byte)':';    //  58

        #region Decode
        public static object Decode(byte[] bytes, Encoding utf)
        {
            IEnumerator<byte> enumerator = ((IEnumerable<byte>)bytes).GetEnumerator();
            enumerator.MoveNext();

            return DecodeNextObject(enumerator, utf);
        }

        public static object DecodeFile(string path, Encoding utf)
        {
            byte[] bytes = File.ReadAllBytes(path);
            return TorrentEncoding.Decode(bytes, utf);
        }

        private static object DecodeNextObject(IEnumerator<byte> enumerator, Encoding utf)
        {
            if (enumerator.Current == DictionaryStart)
                return DecodeDictionary(enumerator, utf);

            if (enumerator.Current == ListStart)
                return DecodeList(enumerator, utf);

            if (enumerator.Current == NumberStart)
                return DecodeNumber(enumerator, utf);

            return DecodeByteArray(enumerator, utf);
        }

        private static Dictionary<string, object> DecodeDictionary(IEnumerator<byte> enumerator, Encoding utf)
        {
            Dictionary<string, object> dict = new();
            List<string> keys = new();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current == DictionaryEnd)
                    break;

                string key = utf.GetString(DecodeByteArray(enumerator, utf));
                enumerator.MoveNext();
                object val = DecodeNextObject(enumerator, utf);

                keys.Add(key);
                dict.Add(key, val);
            }

            var sortedKeys = keys.OrderBy(x => BitConverter.ToString(utf.GetBytes(x)));
            if (!keys.SequenceEqual(sortedKeys))
                throw new TorrentEncodingException("error loading dictionary: keys not sorted");

            return dict;
        }

        private static List<object> DecodeList(IEnumerator<byte> enumerator, Encoding utf)
        {
            List<object> list = new();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current == ListEnd)
                    break;

                list.Add(DecodeNextObject(enumerator, utf));
            }

            return list;
        }

        private static byte[] DecodeByteArray(IEnumerator<byte> enumerator, Encoding utf)
        {
            List<byte> lengthBytes = new();

            do
            {
                if (enumerator.Current == ByteArrayDivider)
                    break;

                lengthBytes.Add(enumerator.Current);
            }
            while (enumerator.MoveNext());

            string lengthString = utf.GetString(lengthBytes.ToArray());

            if (!int.TryParse(lengthString, out int length))
                throw new TorrentEncodingException(Properties.Resources.E10);

            byte[] bytes = new byte[length];

            for (int i = 0; i < length; i++)
            {
                enumerator.MoveNext();
                bytes[i] = enumerator.Current;
            }

            return bytes;
        }

        private static long DecodeNumber(IEnumerator<byte> enumerator, Encoding utf)
        {
            List<byte> bytes = new();

            while (enumerator.MoveNext())
            {
                if (enumerator.Current == NumberEnd)
                    break;

                bytes.Add(enumerator.Current);
            }

            string numAsString = utf.GetString(bytes.ToArray());

            return Int64.Parse(numAsString);
        }
        #endregion

        #region Encode
        public static byte[] Encode(object obj, Encoding utf) =>
            EncodeNextObject(new MemoryStream(), obj, utf).ToArray();

        public static void EncodeToFile(object obj, string path, Encoding utf) =>
            File.WriteAllBytes(path, Encode(obj, utf));

        private static MemoryStream EncodeNextObject(MemoryStream ms, object obj, Encoding utf)
        {
            if (obj is byte[] o0)
                EncodeByteArray(ms, o0, utf);
            else if (obj is string [] s0)
                EncodeStringArray(ms, s0, utf);
            else if (obj is string s1)
                EncodeString(ms, s1, utf);
            else if (obj is long l0)
                EncodeNumber(ms, l0, utf);
            else if (obj.GetType() == typeof(List<object>))
                EncodeList(ms, (List<object>)obj, utf);
            else if (obj.GetType() == typeof(List<string>))
                EncodeList(ms, (List<string>)obj, utf);
            else if (obj.GetType() == typeof(Dictionary<string, object>))
                EncodeDictionary(ms, (Dictionary<string, object>)obj, utf);
            else
                throw new TorrentEncodingException($"{Properties.Resources.E9}: {obj.GetType()}");
            return ms;
        }

        private static void EncodeByteArray(MemoryStream ms, byte[] body, Encoding utf)
        {
            ms.Append(utf.GetBytes(Convert.ToString(body.Length)));
            ms.Append(ByteArrayDivider);
            ms.Append(body);
        }

        private static void EncodeString(MemoryStream ms, string input, Encoding utf)
        {
            EncodeByteArray(ms, utf.GetBytes(input), utf);
        }

        private static void EncodeNumber(MemoryStream ms, long input, Encoding utf)
        {
            ms.Append(NumberStart);
            ms.Append(utf.GetBytes(Convert.ToString(input)));
            ms.Append(NumberEnd);
        }

        private static void EncodeStringArray(MemoryStream ms, string [] input, Encoding utf)
        {
            ms.Append(ListStart);
            foreach (string item in input)
                _ = EncodeNextObject(ms, item, utf);
            ms.Append(ListEnd);
        }

        private static void EncodeList(MemoryStream ms, List<object> input, Encoding utf)
        {
            ms.Append(ListStart);
            foreach (var item in input)
                _ = EncodeNextObject(ms, item, utf);
            ms.Append(ListEnd);
        }

        private static void EncodeList(MemoryStream ms, List<string> input, Encoding utf)
        {
            ms.Append(ListStart);
            foreach (string item in input)
                _ = EncodeNextObject(ms, item, utf);
            ms.Append(ListEnd);
        }

        private static void EncodeDictionary(MemoryStream ms, Dictionary<string, object> input, Encoding utf)
        {
            ms.Append(DictionaryStart);

            var sortedKeys = input.Keys.ToList().OrderBy(x => BitConverter.ToString(utf.GetBytes(x)));

            foreach (var key in sortedKeys)
            {
                EncodeString(ms, key, utf);
                _ = EncodeNextObject(ms, input[key], utf);
            }
            ms.Append(DictionaryEnd);
        }

        #endregion

        #region Encode Utils
        public static Dictionary<string, object> EncodeDictionaryFilePath(this TorrentFileItem item)
        {
            string[] ss = item.Path.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
            if ((ss == default) || (ss.Length == 0))
                ss = new string[] { item.Path };
            Dictionary<string, object> dict = new();
            dict["path"] = ss;
            dict["length"] = item.Size;
            return dict;
        }
        #endregion
    }

    // source: Fredrik Mörk (http://stackoverflow.com/a/4015634)
    public static class MemoryStreamExtensions
    {
        public static void Append(this MemoryStream stream, byte value)
        {
            stream.WriteByte(value);
        }

        public static void Append(this MemoryStream stream, byte[] values)
        {
            stream.Write(values, 0, values.Length);
        }
    }
}
