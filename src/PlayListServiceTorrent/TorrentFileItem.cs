﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceTorrent
{
    #region FileItem
    public class TorrentFileItem
    {
        public string Path;
        public long Size;
        public long Offset;
        public string FormattedSize { get { return Torrent.BytesToString(Size); } }

        public TorrentFileItem() { }
        public TorrentFileItem(string p, long s, long o)
        {
            Path = p;
            Size = s;
            Offset = o;
        }
    }
    #endregion
}
