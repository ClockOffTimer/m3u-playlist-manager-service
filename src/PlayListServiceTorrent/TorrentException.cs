﻿/* Copyright (c) 2022 PlayListServiceTorrent, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceTorrent
{
    [Serializable]
    public class TorrentException : Exception
    {
        public TorrentException() { }
        public TorrentException(string message) : base(message) { }
        public TorrentException(string message, Exception inner) : base(message, inner) { }
        protected TorrentException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    [Serializable]
    public class TorrentSourceEmptyException : TorrentException
    {
        public TorrentSourceEmptyException(string message) : base($"{Properties.Resources.E2}: {message}") { }
    }
    [Serializable]
    public class TorrentEncodingException : TorrentException
    {
        public TorrentEncodingException(string message) : base($"{Properties.Resources.E3}: {message}") { }
    }
    [Serializable]
    public class TorrentSourceNotFoundException : TorrentException
    {
        public TorrentSourceNotFoundException(string message) : base($"{Properties.Resources.E4}: {message}") { }
    }
    [Serializable]
    public class TorrentNameNotFoundException : TorrentException
    {
        public TorrentNameNotFoundException(string message) : base($"{Properties.Resources.E5}: {message}") { }
    }
    [Serializable]
    public class TorrentBadFileException : TorrentException
    {
        public TorrentBadFileException(string message) : base($"{Properties.Resources.E6}: {message}") { }
    }
    [Serializable]
    public class TorrentBadDirectoryException : TorrentException
    {
        public TorrentBadDirectoryException(string message) : base($"{Properties.Resources.E11}: {message}") { }
    }
    [Serializable]
    public class TorrentBadFileSpecificationException : TorrentException
    {
        public TorrentBadFileSpecificationException(string message) : base($"{Properties.Resources.E7}: {message}") { }
    }
}
