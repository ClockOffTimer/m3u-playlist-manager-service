﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Xaml.Behaviors;
using WpfPlugin.EditBox;

namespace WpfPlugin
{
    public class LogViewBehaviors : Behavior<RichTextBox>
    {
        private Timer timer;
        private readonly LogEventTextFormater LogTextFormater_ = new();
        public LogEventTextFormater LogTextFormater
        {
            get { return LogTextFormater_; }
        }

        public LogViewBehaviors() : base()
        {
            #region Init this errors handle self
            LogTextFormater_.EventErrors += (a) =>
            {
                LogTextFormater_.LogWriteLine(AssociatedObject, a);
            };
            #endregion
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.TextChanged += TextChanged;
            timer = new Timer(
                (a) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        LogTextFormater_.BeginMessage(AssociatedObject);
                    });
                    try { timer.Dispose(); } catch { }
                    timer = default;
                },
                null,
                System.TimeSpan.FromSeconds(2),
                Timeout.InfiniteTimeSpan);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.TextChanged -= TextChanged;
        }

        #region EditBox Event
        private async void TextChanged(object sender, TextChangedEventArgs e)
        {
            AssociatedObject.TextChanged -= TextChanged;
            await LogTextFormater_.TextChangedAsync(AssociatedObject, e);
            AssociatedObject.TextChanged += TextChanged;
        }
        #endregion
    }
}
