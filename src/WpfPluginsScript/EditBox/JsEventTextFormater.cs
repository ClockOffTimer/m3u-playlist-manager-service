﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Text.RegularExpressions;
using System.Windows.Documents;
using System.Windows.Media;
#if !UICOMPATIBLE
using ModernWpf;
#endif

namespace WpfPlugin.EditBox
{
    public class JsEventTextFormater : EventTextFormater, IEventTextFormater
    {
        #region CONSTANT
        private readonly uint[] jsSymbolsMap = new uint[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
            1, 2, 0, 0, 1, 0, 1, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 1, 0, 2, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0,
        };

        private readonly Regex[] regex = new Regex[] {
            new Regex("[\"\'](?<FIELD>[^\"\']*)[\"\']?|(:?<FIELD>\\S+)", RegexOptions.Compiled),
            new Regex("[/\\*]{2}(?:(?!/\\*|\\*/).)*\\*/", RegexOptions.Compiled),
            new Regex("(\\/\\*.+$)", RegexOptions.Compiled),
            new Regex("(^.+\\*\\/)", RegexOptions.Compiled),
            new Regex("(\\/\\/.+$)", RegexOptions.Compiled)
        };
        private readonly Color[] TColorsLight = new Color[]
        {
            Colors.DarkOliveGreen,
            Colors.DarkOrange,
            Colors.Blue,
            Colors.DarkRed,
            Color.FromRgb(222, 68, 0)
        };
#if !UICOMPATIBLE
        private readonly Color[] TColorsDark = new Color[]
        {
            Colors.YellowGreen,
            Colors.DarkOrange,
            Colors.DarkOrchid,
            Colors.DeepPink,
            Color.FromRgb(222, 68, 0)
        };
#endif
        private enum TColor : int
        {
            Comment = 0,
            Quoted = 1,
            Generic = 2,
            Function = 3,
            Internal = 4
        }
#if !UICOMPATIBLE
        private Color GetColor(TColor id) => (currentTheme == ApplicationTheme.Dark) ?
            TColorsDark[(int)id] : TColorsLight[(int)id];
#else
        private Color GetColor(TColor id) => TColorsLight[(int)id];
#endif
        #endregion

        public JsEventTextFormater() : base()
        {
            ParseLine = JsParseLine;
            CharSymbolsMap = jsSymbolsMap;
        }
        ~JsEventTextFormater()
        {
            base.EventFileWatchClose();
        }

        #region Js ParseLine
        private LineDirection JsParseLine(TextRange doc, TextPointer pos, string txt)
        {
            do
            {
                bool b;
                do
                {
                    if (ModeComment != MultilineComment.StartLine)
                    {
                        b = SetStyleFromRegex(doc, pos, regex[1], txt, GetColor(TColor.Comment), false);
                        if (b)
                        {
                            ModeComment = MultilineComment.FullLine;
                            break;
                        }
                        b = SetStyleFromRegex(doc, pos, regex[2], txt, GetColor(TColor.Comment), false);
                        if (b)
                        {
                            ModeComment = MultilineComment.StartLine;
                            break;
                        }
                        b = SetStyleFromRegex(doc, pos, regex[4], txt, GetColor(TColor.Comment), false);
                        if (b)
                        {
                            ModeComment = MultilineComment.FullLine;
                            break;
                        }
                        b = SetStyleFromRegex(doc, pos, regex[0], txt, GetColor(TColor.Quoted));
                        if (b)
                            txt = GetStringFromCurentLine(pos);
                    }
                    b = SetStyleFromRegex(doc, pos, regex[3], txt, GetColor(TColor.Comment), false);
                    if (b)
                    {
                        ModeComment = MultilineComment.EndLine;
                        break;
                    }

                } while (false);

                if (ModeComment != MultilineComment.None)
                {
                    switch (ModeComment)
                    {
                        case MultilineComment.FullLine:
                        case MultilineComment.EndLine: ModeComment = MultilineComment.None; break;
                        case MultilineComment.StartLine: ModeComment = MultilineComment.TransitLine; break;
                        case MultilineComment.TransitLine:
                            {
                                SetColorString(doc, pos, GetColor(TColor.Comment), txt, txt, false);
                                break;
                            }
                    };
                    break;
                }
                foreach (var s in JsKeywords.tagsGeneric)
                    b = SetColorString(doc, pos, GetColor(TColor.Generic), txt, s, true) || b;
                if (b)
                    txt = GetStringFromCurentLine(pos);
                foreach (var s in JsKeywords.tagsFunctions)
                    b = SetColorString(doc, pos, GetColor(TColor.Function), txt, s, true) || b;
                if (b)
                    txt = GetStringFromCurentLine(pos);
                foreach (var s in JsKeywords.tagsInternals)
                    b = SetColorString(doc, pos, GetColor(TColor.Internal), txt, s, true) || b;

            } while (false);
            return LineDirection.TOKENLINE;
        }
        #endregion
    }
}
