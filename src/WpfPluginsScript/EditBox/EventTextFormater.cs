﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
#if !UICOMPATIBLE
using ModernWpf;
#endif

namespace WpfPlugin.EditBox
{
    public abstract class EventTextFormater : IEventTextFormater
    {
        #region CONSTANT
        protected int COLORSPACE = 2;
        protected uint[] CharSymbolsMap;
        protected enum MultilineComment : int
        {
            None = -1,
            FullLine = 0,
            StartLine = 1,
            EndLine = 2,
            TransitLine = 3
        };
        protected enum LineDirection : int
        {
            PREVLINE = -1,
            CURRENTLINE = 0,
            NEXTLINE = 1,
            TOKENLINE = 2
        };
        #endregion

        #region PRIVATE/PROTECTED SETTER
        private bool IsShift_ = false;
        private bool IsRun_ = false;
        private bool IsWatch_ = false;

#if !UICOMPATIBLE
        protected ApplicationTheme currentTheme = ApplicationTheme.Light;
#endif
        protected FileSystemWatcher Watcher_ = default;
        protected MultilineComment ModeComment = MultilineComment.None;
        protected Func<TextRange, TextPointer, string, LineDirection> ParseLine = (a, b, s) => { return LineDirection.NEXTLINE; };
        public Action<FileSystemEventArgs> WatcherCb = (a) => { };
        public EditorState State { get; set; } = EditorState.None;
        public string OpenFilePath { get; set; } = default;
        #endregion

        #region ERROR EVENT SETTER
        public delegate void ErrorEvent(LogEventArgs arg);
        public event ErrorEvent  EventErrors = delegate { };
        private void ErrorEventSend(EditorState s, Exception e)
        {
            EventErrors?.Invoke(new LogEventArgs(s, e));
        }
        #endregion

        #region PUBLIC OpenDocument
        public void OpenDocument(RichTextBox box, string file)
        {
            try
            {
                if (string.IsNullOrEmpty(file))
                {
                    State = EditorState.FileEmpty;
                    ErrorEventSend(State, default);
                    return;
                }

                EventFileWatchDisable();

                using (Stream s = new FileStream(file, FileMode.Open))
                {
                    TextRange r = new(box.Document.ContentStart, box.Document.ContentEnd);
                    r.Load(s, DataFormats.Text);
                }
                State = file.Equals(OpenFilePath) ? EditorState.ReLoad : EditorState.Load;
                ErrorEventSend(State, new LogFormaterException(file));
                OpenFilePath = file;
                EventFileWatchInit(file);
            }
            catch (Exception e) { State = EditorState.Error; ErrorEventSend(State, e); }
        }
        #endregion

        #region PUBLIC SaveDocument
        public void SaveDocument(RichTextBox box, string file = default)
        {
            try
            {
                if (!string.IsNullOrEmpty(file))
                    OpenFilePath = file;
                if (string.IsNullOrEmpty(OpenFilePath))
                {
                    State = EditorState.FileEmpty;
                    ErrorEventSend(State, default);
                    return;
                }

                EventFileWatchDisable();

                if (OpenFilePath.EndsWith(".js.example", StringComparison.InvariantCultureIgnoreCase))
                    OpenFilePath = OpenFilePath.Substring(0, OpenFilePath.Length - 8);

                using (Stream s = new FileStream(OpenFilePath, FileMode.Create))
                {
                    TextRange r = new(box.Document.ContentStart, box.Document.ContentEnd);
                    r.Save(s, DataFormats.Text);
                }
                State = EditorState.Save;
                ErrorEventSend(State, new LogFormaterException(OpenFilePath));
                EventFileWatchDisable(true);
            }
            catch (Exception e) { State = EditorState.Error; ErrorEventSend(State, e); }
        }
        public bool CanSaveDocument()
        {
            return !string.IsNullOrEmpty(OpenFilePath);
        }
        #endregion

        #region PUBLIC KeyUp
        public void KeyUp(RichTextBox box, KeyEventArgs e)
        {
            string s = default;
            switch (e.Key)
            {
                case Key.D9:
                    {
                        if (IsShift_)
                            s = ");";
                        break;
                    }
                case Key.OemOpenBrackets:
                    {
                        if (IsShift_)
                            s = " }";
                        else
                            s = "]";
                        break;
                    }
                case Key.OemQuotes:
                    {
                        if (IsShift_)
                            s = "\"";
                        else
                            s = "'";
                        break;
                    }
                    /*
                case Key.Tab:
                    {
                        s = new string(' ', 4);
                        break;
                    }
                    */
            }
            if (IsShift_)
                IsShift_ = false;
            if (s != default)
            {
                TextRange doc = new(box.Document.ContentStart, box.Document.ContentEnd);
                if (doc.IsEmpty)
                    return;

                TextPointer tpp = box.CaretPosition;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    tpp.InsertTextInRun(s);
                    doc.Select(tpp.DocumentStart, tpp.DocumentEnd);
                });
                e.Handled = true;
            }
        }
        #endregion

        #region PUBLIC KeyDown
        public void KeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.LeftShift:
                case Key.RightShift:
                    {
                        IsShift_ = true;
                        break;
                    }
                    /*
                case Key.Tab:
                    {
                        e.Handled = true;
                        break;
                    }
                    */
            }
        }
        #endregion

        #region PUBLIC TextChangedAsync
        public async Task TextChangedAsync(RichTextBox box, TextChangedEventArgs e)
        {
            if (IsRun_)
                return;

#if !UICOMPATIBLE
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (ThemeManager.Current.ApplicationTheme.HasValue)
                    currentTheme = ThemeManager.Current.ApplicationTheme.Value;
            });
#endif
            try
            {
                await Task.Run(() =>
                {
                    if (IsRun_)
                        return;
                    try
                    {
                        IsRun_ = true;
                        Task.Delay(1200);
                        TextChanged(box, e);
                    }
                    finally
                    {
                        IsRun_ = false;
                    }
                });
            }
            catch (Exception ex) { ErrorEventSend(State, ex); }
        }
        #endregion

        #region PUBLIC TextChanged
        public void TextChanged(RichTextBox box, TextChangedEventArgs e)
        {
            try
            {
                ICollection<TextChange> changes = e.Changes;
                if ((changes == default) || (changes.Count == 0))
                    return;

                foreach (TextChange change in changes)
                {
                    TextRange doc = default;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        TextPointer tp0 = box.Document.ContentStart.GetPositionAtOffset(change.Offset),
                                    tp1 = box.Document.ContentStart.GetPositionAtOffset(change.Offset + change.AddedLength);

                        if ((tp0 != default) && (tp1 != default))
                        {
                            doc = new TextRange(tp0, tp1);
                            if (doc.IsEmpty)
                                doc = default;
                        }
                    });
                    if (doc == default)
                        continue;

                    ParseLines(doc);
                }
            }
            catch (Exception ex) { ErrorEventSend(State, ex); }
        }
        #endregion

        #region PUBLIC ClearDocument
        public void ClearDocument(RichTextBox box)
        {
            EventFileWatchClose();

            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                box.Document.Blocks.Clear();
                box.Document.Blocks.Add(new Paragraph(new Run("")));
            }));
        }
        #endregion

        #region PUBLIC GetText
        public string GetText(RichTextBox box)
        {
            return new TextRange(box.Document.ContentStart, box.Document.ContentEnd).Text;
        }
        #endregion

        #region PUBLIC AddTextLine
        public void AddTextLine(RichTextBox box, string s, Color clr = default)
        {
            try
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    TextRange tr = new(box.Document.ContentEnd, box.Document.ContentEnd);
                    if (tr == default)
                        return;
                    tr.Text = s;
                    box.Document.ContentEnd.InsertLineBreak();
                    if (CheckColor(clr))
                        tr.ApplyPropertyValue(TextElement.ForegroundProperty, GetDefaultColor(clr));
                });
            }
            catch (Exception ex) { ErrorEventSend(State, ex); }
        }
        #endregion

        #region PUBLIC AddTextParagraph
        public void AddTextParagraph(RichTextBox box, string s, bool isins = false, Color clr = default)
        {
            try
            {
                Paragraph p = new();
                p.Inlines.Add(new Run(s)
                {
                    Foreground = GetDefaultColor(clr)
                });
                if (isins)
                    box.Document.Blocks.InsertBefore(box.Document.Blocks.FirstBlock, p);
                else
                    box.Document.Blocks.Add(p);
            }
            catch (Exception ex) { ErrorEventSend(State, ex); }
        }
        #endregion

        #region PROTECTED

        protected void EventFileWatchClose()
        {
            FileSystemWatcher fw = Watcher_;
            Watcher_ = default;
            if (fw != default)
            {
                fw.EnableRaisingEvents = false;
                try { fw.Dispose(); } catch (Exception e) { ErrorEventSend(State, e); }
            }
        }

        protected void EventFileWatchDisable(bool state = false)
        {
            if (Watcher_ != default)
                Watcher_.EnableRaisingEvents = state;
        }

        protected void EventFileWatchInit(string file)
        {
            try
            {
                if (Watcher_ == default)
                {
                    Watcher_ = new FileSystemWatcher
                    {
                        Path = Path.GetDirectoryName(file),
                        IncludeSubdirectories = false,
                        NotifyFilter = NotifyFilters.LastWrite,
                        Filter = Path.GetFileName(file)
                    };
                    Watcher_.Changed += (s, a) =>
                    {
                        try
                        {
                            if (IsWatch_)
                                return;
                            IsWatch_ = true;

                            Application.Current.Dispatcher.Invoke(() => {
                                WatcherCb.Invoke(a);
                            });
                        }
                        catch (Exception e) { ErrorEventSend(State, e); }
                        finally
                        {
                            IsWatch_ = false;
                        }
                    };
                    Watcher_.Error += (s, a) =>
                    {
                        ErrorEventSend(State, a.GetException());
                    };
                }
                else
                {
                    Watcher_.Path = Path.GetDirectoryName(file);
                    Watcher_.Filter = Path.GetFileName(file);
                }
                Watcher_.EnableRaisingEvents = true;

            }
            catch (Exception e) { State = EditorState.Error; ErrorEventSend(State, e); Watcher_ = default; }
        }

        protected SolidColorBrush GetDefaultColor(Color clr) =>
            CheckColor(clr) ? new SolidColorBrush(clr) : SystemColors.WindowTextBrush;
        protected bool CheckColor(Color clr) => ((clr != default) && (clr != SystemColors.WindowTextColor));

        protected void ParseLines(TextRange doc)
        {
            TextPointer pos = default;
            Application.Current.Dispatcher.Invoke(() =>
                { pos = doc.Start.DocumentStart; });

            while (pos != default)
            {
                LineDirection continueType = LineDirection.TOKENLINE;
                TextPointerContext context = TextPointerContext.None;
                Application.Current.Dispatcher.Invoke(() =>
                    { context = pos.GetPointerContext(LogicalDirection.Forward); });

                if (context == TextPointerContext.Text)
                {
                    string txt = default;
                    Application.Current.Dispatcher.Invoke(() =>
                        { txt = pos.GetTextInRun(LogicalDirection.Forward); });

                    if (!string.IsNullOrWhiteSpace(txt))
                        continueType = ParseLine.Invoke(doc, pos, txt);
                }

                Application.Current.Dispatcher.Invoke(() =>
                    {
                        if (continueType == LineDirection.NEXTLINE)
                            pos = pos?.GetLineStartPosition(1);
                        pos = pos?.GetNextContextPosition(LogicalDirection.Forward);
                    });
            }
        }

        protected string GetStringFromCurentLine(TextPointer pos, LineDirection line = LineDirection.CURRENTLINE)
        {
            string txt = string.Empty;
            Application.Current.Dispatcher.Invoke(() =>
            {
                txt = pos.GetLineStartPosition((int)line)?
                         .GetTextInRun(LogicalDirection.Forward);
            });
            return txt;
        }

        protected string GetStringFromCurentLine2(TextPointer pos)
        {
            string txt = string.Empty;
            Application.Current.Dispatcher.Invoke(() =>
            {
                txt = new TextRange(
                         pos.GetLineStartPosition(0), pos.GetLineStartPosition(1)).Text.TrimEnd(new char[] { '\r', '\n' });
            });
            return txt;
        }

        protected bool SetStyleFromRegex(TextRange doc, TextPointer pos, Regex regex, string txt, Color clr, bool ischeck = true, int offset = 0)
        {
            MatchCollection mc = regex.Matches(txt);
            if (mc.Count > 0)
            {
                bool b = false;
                foreach (Match m in mc)
                {
                    b = SetColorString(doc, pos, clr, txt, m.Value, ischeck, offset) || b;
                    txt = GetStringFromCurentLine(pos, LineDirection.CURRENTLINE);
                }
                return b;
            }
            return false;
        }

        protected TextRange GetStringPosition(TextPointer pos, string txt, string word, bool ischeck, int offset = 0)
        {
            int idx = txt.IndexOf(word, StringComparison.InvariantCulture);
            if (idx >= 0)
            {
                do
                {
                    if ((idx = txt.IndexOf(word, StringComparison.InvariantCulture)) < 0)
                        break;
                    if (word.Length > txt.Length)
                        break;

                    if (ischeck)
                    {
                        int p = idx + word.Length;
                        if ((p >= txt.Length) || (!(CharSymbolsMap[txt.ElementAt(p)] >= 1)))
                            break;
                        if ((idx > 0) && (CharSymbolsMap[txt.ElementAt(idx - 1)] != 1))
                            break;
                    }

                    TextRange tr = default;
                    int selbegin = idx + ((offset < 0) ? 0 : offset),
                        selend = selbegin + word.Length,
                        strorig,
                        strtrim;

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        TextPointer begin = pos.GetPositionAtOffset(selbegin),
                                    end;
                        selend += begin.IsAtLineStartPosition ? 1 : 0;
                        end = pos.GetPositionAtOffset(selend);

                        if ((begin != default) && (end != default))
                            tr = new TextRange(begin, end);
                    });

                    if (tr == default)
                        break;

                    string s = tr.Text;
                    if (string.IsNullOrWhiteSpace(s))
                        break;

                    strorig = s.Length;
                    strtrim = s.Trim().Length;

                    if (strorig != strtrim)
                    {
                        int trimbegin = s.TrimStart().Length,
                            trimend = s.TrimEnd().Length;

                        if (strtrim == trimbegin)
                            selbegin += (strorig - trimbegin);
                        else if (strtrim == trimend)
                            selend -= (strorig - trimend);
                        else
                        {
                            selbegin += (strorig - trimbegin);
                            selend -= (strorig - trimend);
                        }
                        if ((selend - selbegin) != word.Length)
                            break;

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            TextPointer begin = pos.GetPositionAtOffset(selbegin),
                                        end = pos.GetPositionAtOffset(selend);

                            if ((begin != null) && (end != null))
                                tr = new TextRange(begin, end);
                            else
                                tr = default;
                        });
                    }
                    return tr;

                } while (false);
            }
            return default;
        }

        protected bool SetColorString(TextRange doc, TextPointer pos, Color clr, string txt, string word, bool ischeck = true, int offset = 0)
        {
            if (!CheckColor(clr))
                return false;

            TextRange tr = GetStringPosition(pos, txt, word, ischeck, offset);
            if ((tr == default) || tr.IsEmpty)
                return false;

            Application.Current.Dispatcher.Invoke(() =>
            {
                doc.Select(tr.Start, tr.End);
                doc.ApplyPropertyValue(TextElement.ForegroundProperty, GetDefaultColor(clr));
            });
            return true;
        }
        #endregion
    }
}
