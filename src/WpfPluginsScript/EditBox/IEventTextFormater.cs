﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace WpfPlugin.EditBox
{
    public enum EditorState : int
    {
        None = -1,
        Load = 0,
        Save = 1,
        FileEmpty = 2,
        Error = 3,
        Info = 4,
        Script = 5,
        ReLoad = 6
    };
    public interface IEventTextFormater
    {
        string OpenFilePath { get; set; }
        EditorState State { get; set; }

        event EventTextFormater.ErrorEvent EventErrors;

        string GetText(RichTextBox box);
        void AddTextLine(RichTextBox box, string s, Color clr = default);
        void AddTextParagraph(RichTextBox box, string s, bool isins = false, Color clr = default);
        bool CanSaveDocument();
        void ClearDocument(RichTextBox box);
        void KeyDown(KeyEventArgs e);
        void KeyUp(RichTextBox box, KeyEventArgs e);
        void OpenDocument(RichTextBox box, string file);
        void SaveDocument(RichTextBox box, string file = null);
        void TextChanged(RichTextBox box, TextChangedEventArgs e);
        Task TextChangedAsync(RichTextBox box, TextChangedEventArgs e);
    }
}