﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using ModernWpf;

namespace WpfPlugin.EditBox
{
    public class LogEventTextFormater : EventTextFormater, IEventTextFormater
    {
        #region CONSTANT
        private readonly uint[] logSymbolsMap = new uint[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 2, 0, 2, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 2, 2,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0,
        };

        private readonly Regex[] regex = new Regex[] {
            new Regex("^(\\d{1,2}:\\d{1,2}:\\d{1,2})|\\[(\\S*)\\]|(\\S.+)$", RegexOptions.Compiled | RegexOptions.Singleline)
        };
        private readonly string TimeFmt = "H:mm:ss";
        private readonly object lock_ = new();
        #endregion

        public LogEventTextFormater() : base()
        {
            ParseLine = LogParseLine;
            CharSymbolsMap = logSymbolsMap;
        }

        public void BeginMessage(RichTextBox box)
        {
            AddTextLine(box, Properties.ResourcesScript.S13);
            /* x:Key = logxMessage1 */
        }
        public void LogWriteLine(RichTextBox box, LogEventArgs a)
        {
            LogWriteLine(box, a.State, a.Error);
        }
        public void LogWriteLine(RichTextBox box, EditorState state, Exception e)
        {
            EditorState s = state;
            if ((s != EditorState.Script) && (e.GetType() == typeof(LogFormaterException)))
                s = EditorState.Info;

            string tag;
            switch (s)
            {
                case EditorState.Load:
                case EditorState.Save:
                case EditorState.ReLoad:
                case EditorState.FileEmpty: tag = "EDIT"; break;
                case EditorState.Script: tag = "SCRIPT"; break;
                case EditorState.Error: tag = "ERROR"; break;
                case EditorState.Info: tag = "INFO"; break;
                default: return;
            }
            lock (lock_)
            {
                if (e != default)
                    AddTextLine(box, $"{DateTime.Now.ToString(TimeFmt)} [{tag}] {state}: {e.Message}".Trim());
                else
                    AddTextLine(box, $"{DateTime.Now.ToString(TimeFmt)} [{tag}] {state}".Trim());
            }
        }

        #region Log ParseLine
        private LineDirection LogParseLine(TextRange doc, TextPointer pos, string txt)
        {
#if !UICOMPATIBLE
            bool b = currentTheme == ApplicationTheme.Dark;
#else
            bool b = false;
#endif
            MatchCollection mc = regex[0].Matches(txt);
            if (mc.Count != 3)
                return LineDirection.NEXTLINE;

            for (int i = 0; i < mc.Count; i++)
            {
                string val = mc[i].Value;
                if (string.IsNullOrWhiteSpace(val))
                    continue;

                Color clr;
                switch (i)
                {
                    case 0: clr = b ? Colors.YellowGreen : Colors.DarkOliveGreen; break;
                    case 1: clr = b ? Colors.DeepPink    : Colors.DarkRed; break;
                    case 2: clr = SystemColors.WindowTextColor; break;
                    default: continue;
                }
                _ = SetColorString(doc, pos, clr, txt, val, true, i * COLORSPACE);
            }
            return LineDirection.NEXTLINE;
        }
#endregion
    }
}
