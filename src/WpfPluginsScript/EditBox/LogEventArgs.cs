﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace WpfPlugin.EditBox
{
    [Serializable]
    public class LogEventArgs : EventArgs
    {
        public EditorState State { get; private set; } = default;
        public Exception   Error { get; private set; } = default;
        public string      ErrorString
        {
            get { return Error.Message; }
            private set { Error = new LogFormaterException(value); }
        }

        public LogEventArgs(EditorState s, Exception e) : base()
        {
            State = s;
            Error = e;
        }
        public LogEventArgs(EditorState s, string msg) : base()
        {
            State = s;
            ErrorString = msg;
        }
    }
}
