﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace WpfPlugin.EditBox
{
    public class LogFormaterException : Exception
    {
        public LogFormaterException() { }
        public LogFormaterException(string message) : base(message) { }
    }
}
