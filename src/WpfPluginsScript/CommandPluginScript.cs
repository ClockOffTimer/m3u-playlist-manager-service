﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginScript
	{
		public static readonly RoutedUICommand Open = new RoutedUICommand
			(
				"",
				"Open",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F2, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand Save = new RoutedUICommand
			(
				"",
				"Save",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F3, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand Zoom = new RoutedUICommand
			(
				"",
				"Zoom",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F9, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand JsClear = new RoutedUICommand
			(
				"",
				"JsClear",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F7, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand LogClear = new RoutedUICommand
			(
				"",
				"LogClear",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F8, ModifierKeys.Alt)
				}
			);
		public static readonly RoutedUICommand DebugRun = new RoutedUICommand
			(
				"",
				"DebugRun",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F5, ModifierKeys.None, "F5 - Start")
				}
			);
		public static readonly RoutedUICommand DebugExit = new RoutedUICommand
			(
				"",
				"DebugExit",
				typeof(CommandPluginScript),
				new InputGestureCollection()
				{
					new KeyGesture(Key.F6, ModifierKeys.None, "F6 - Stop")
				}
			);
	}
}
