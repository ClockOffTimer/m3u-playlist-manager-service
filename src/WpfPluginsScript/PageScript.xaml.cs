﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using M3U.Model;
using Microsoft.Win32;
using ModernWpf.Controls;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using PlayListServiceScript;
using WpfPlugin.Data;
using WpfPlugin.EditBox;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 7),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageScript : System.Windows.Controls.Page, IPlugin
    {
        private bool scriptRun_ = false;
        private IServiceControls ServiceCtrl { get; set; } = default;
        private ExecDebugger execDebugger { get; set; } = default;
        private PageScriptData pageScriptData { get; set; } = default;
        private IEventTextFormater jsTextFormater { get; set; } = default;
        private LogEventTextFormater logTextFormater { get; set; } = default;
        private ScriptLogManager scriptLogManager { get; set; } = default;
        private ScriptsManager<Media> scriptsManager { get; set; } = default;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;
        public string ScriptRoot { get; private set; } = default;

        [ImportingConstructor]
        public PageScript(IServiceControls icontrols)
        {
            ServiceCtrl = icontrols;
            ScriptRoot = Path.Combine(
                            Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                            "scripts"
                            );
            InitializeComponent();
            logTextFormater = LogFormater.LogTextFormater;
            jsTextFormater = CodeFormater.JsTextFormater;
            jsTextFormater.EventErrors += ErrorsEventCb;
            execDebugger = new ExecDebugger(ScriptRoot, ErrorsEventCb);
            pageScriptData = (PageScriptData)DataContext;
            pageScriptData.SetServiceCtrl(icontrols, jsTextFormater, execDebugger);
            scriptLogManager = new ScriptLogManager(ErrorsEventCb);
            try { _ = ScriptData.LoadScriptLib("scripts.jslib.DefaultInsertLib.js"); } catch { }
        }

        private void ErrorsEventCb(LogEventArgs a)
        {
            logTextFormater?.LogWriteLine(LogBox, a);
        }

        #region Script engine
        private async Task RunScriptEngine(bool isRun = false)
        {
            try
            {
                if (isRun && !scriptRun_)
                    scriptRun_ = true;
                else if (!isRun && scriptRun_)
                {
                    if (scriptsManager == default)
                        throw new Exception(Properties.ResourcesScript.S1);
                    scriptsManager.SetSingleInterrupt();
                    throw new Exception(Properties.ResourcesScript.S2);
                }
                else
                    throw new Exception($"{Properties.ResourcesScript.S3}: {isRun}/{scriptRun_}");

                if (string.IsNullOrWhiteSpace(jsTextFormater?.OpenFilePath))
                    throw new Exception(Properties.ResourcesScript.S4);

                string context = jsTextFormater.GetText(CodeBox);
                if (string.IsNullOrWhiteSpace(context))
                    throw new Exception(Properties.ResourcesScript.S5);

                Application.Current.Dispatcher.Invoke(new Action(() => {
                    pageScriptData.BtnDebugStart = true;
                    pageScriptData.OutMedia = default;
                }));

                await Task.Run(() =>
                {
                    try
                    {
                        if (scriptsManager == default)
                        {
                            scriptsManager = BuildScriptEngine<Media>(jsTextFormater.OpenFilePath, context);
                            if ((scriptsManager == default) || (scriptsManager.Count == 0))
                                throw new Exception(Properties.ResourcesScript.S1);
                        }
                        if (scriptsManager.Count == 0)
                            scriptsManager.ReInit(context);

                        ScriptData sd = scriptsManager.GetSingleEngine();
                        if (sd == default)
                            throw new Exception(Properties.ResourcesScript.S6);

                        logTextFormater.LogWriteLine(LogBox, new LogEventArgs(EditorState.Info, Properties.ResourcesScript.S7));
                        logTextFormater.LogWriteLine(LogBox, new LogEventArgs(EditorState.Info, Properties.ResourcesScript.S8));

                        CONTINUESTATE state = CONTINUESTATE.NONE;
                        switch (sd.ScriptType)
                        {
                            case CALLSTATE.NONE: break;
                            case CALLSTATE.BEGIN_SCAN:
                            case CALLSTATE.BEGIN_CHECK:
                            case CALLSTATE.BEGIN_LOCALSCAN:
                            case CALLSTATE.BEGIN_LOCALCHECK:
                            case CALLSTATE.END_CHECK:
                                {
                                    state = scriptsManager.RunDebug<Media>(sd, out Media media);
                                    pageScriptData.OutMedia = media;
                                    if (media != default)
                                    {
                                        logTextFormater.LogWriteLine(
                                            LogBox,
                                            new LogEventArgs(
                                                EditorState.Script,
                                                $@"{Properties.ResourcesScript.S9} = ""{media.MediaFile}"""));

                                        if (state != CONTINUESTATE.CONTINUE_CHANGE)
                                            logTextFormater.LogWriteLine(
                                                LogBox,
                                                new LogEventArgs(
                                                    EditorState.Script,
                                                    Properties.ResourcesScript.S10));
                                    }
                                    break;
                                }
                            case CALLSTATE.BEGIN_DOWNLOAD:
                                {
                                    state = scriptsManager.RunDebug<List<Media>>(sd, out List<Media> medias);
                                    if ((medias != default) && (medias.GetType() == typeof(List<Media>)))
                                    {
                                        logTextFormater.LogWriteLine(
                                            LogBox,
                                            new LogEventArgs(
                                                EditorState.Script,
                                                $@"{Properties.ResourcesScript.S11} = {medias.Count}"));

                                        medias.Clear();
                                    }
                                    break;
                                }
                            default:
                                {
                                    state = scriptsManager.RunDebug<object>(sd, out object obj);
                                    break;
                                }
                        }
                        logTextFormater.LogWriteLine(
                            LogBox,
                            new LogEventArgs(
                                EditorState.Script,
                                $"return = {state}"));
                    }
                    catch (Exception e)
                    {
                        logTextFormater.LogWriteLine(LogBox, new LogEventArgs(EditorState.Script, e));
                    }
                    finally
                    {
                        HandleScriptException();
                        scriptsManager.ClearSingleEngine();
                    }
                }).ConfigureAwait(false);
            }
            catch (ScriptAllreadyCanceledException e)
            {
                HandleScriptException(e);
                scriptsManager.ClearSingleEngine();
            }
            catch (Exception e)
            {
                HandleScriptException(e);
            }
        }

        private void HandleScriptException(Exception e = default)
        {
            scriptRun_ = false;
            Application.Current.Dispatcher.Invoke(new Action(() => pageScriptData.BtnDebugStart = false));
            if (e != default)
                logTextFormater.LogWriteLine(LogBox, new LogEventArgs(EditorState.Script, e));
        }

        private ScriptsManager<T> BuildScriptEngine<T>(string file, string context)
        {
            try
            {
                return new ScriptsManager<T>(
                    file,
                    new ScriptRunnerDebug<T>()
                    {
                        OptExtendedAdd = (a) =>
                        {
                            a.AddHostObject("xSettings", Microsoft.ClearScript.HostItemFlags.GlobalMembers, ServiceCtrl.ControlConfig);
                            a.AddHostObject("xConsole", Microsoft.ClearScript.HostItemFlags.GlobalMembers, new ScriptExportConsole(scriptLogManager));
                            a.AddHostObject("MediaSource", new Media("http://test.test", "Test Name", 4433221));
                        }
                    },
                    scriptLogManager,
                    true,
                    context
                ) {
                    IsConfigJsHost = pageScriptData.IsConfigJsHost,
                    IsConfigJsLib = pageScriptData.IsConfigJsLib,
                    IsConfigJsXMLHttpRequest = pageScriptData.IsConfigJsXMLHttpRequest
                };
            } catch (Exception ex) { logTextFormater.LogWriteLine(LogBox, EditorState.Error, ex); }
            return default;
        }

        private async Task ClearScriptEngine()
        {
            ScriptsManager<Media> sm = scriptsManager;
            scriptsManager = default;
            if (sm == default)
                return;
            await Task.Run(() =>
            {
                try
                {
                    sm.Clear();
                    scriptRun_ = false;
                }
                catch (Exception ex) { logTextFormater.LogWriteLine(LogBox, EditorState.Error, ex); }
            }).ConfigureAwait(false);
        }
        #endregion

        #region CodeBox command

        #region - Open command
                    private void Open_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(ScriptRoot))
                    return;
                DirectoryInfo dir = new(ScriptRoot);
                if (dir == default)
                    return;
                if (!dir.Exists)
                    dir.Create();

                OpenFileDialog ofd = new()
                {
                    Filter = $@"{Properties.ResourcesScript.S12} (ts,js,js.example)|*.js;*.ts;*.js.example",
                    Multiselect = false,
                    InitialDirectory = ScriptRoot
                };
                if (ofd.ShowDialog() == true)
                {
                    jsTextFormater?.OpenDocument(CodeBox, ofd.FileName);
                    pageScriptData.SetNotify();
                }
            } catch (Exception ex) { logTextFormater.LogWriteLine(LogBox, EditorState.Error, ex); }
        }
        private void Open_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion
        #region - Save command
        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            jsTextFormater?.SaveDocument(CodeBox);
        }
        private void Save_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = jsTextFormater != default && (bool)jsTextFormater?.CanSaveDocument();
        }
        #endregion
        #region - Zoom command
        private void Zoom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.OriginalSource is AppBarToggleButton btn)
            {
                if ((bool)btn.IsChecked)
                {
                    CodeBox.FontSize = 16;
                    CodeBox.FontWeight = FontWeights.Regular;
                    CodeBox.FontStretch = FontStretch.FromOpenTypeStretch(5);
                }
                else
                {
                    CodeBox.FontSize = 14;
                    CodeBox.FontWeight = FontWeights.Regular;
                    CodeBox.FontStretch = FontStretch.FromOpenTypeStretch(5);
                }
            }
        }
        private void Zoom_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion
        #region - Script clear command
        private void JsClear_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            jsTextFormater?.ClearDocument(CodeBox);
        }
        private void JsClear_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion
        #region - Log clear command
        private void LogClear_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            logTextFormater?.ClearDocument(LogBox);
        }
        private void LogClear_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion
        #region - Debug Run (Start) command
        private async void DebugRun_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageScriptData == default)
                return;
            logTextFormater?.ClearDocument(LogBox);
            await RunScriptEngine(!pageScriptData.BtnDebugStart);
        }
        private void DebugRun_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (pageScriptData == default)
                e.CanExecute = false;
            else
                e.CanExecute = (bool)pageScriptData?.CanBtnDebugStart;
        }
        #endregion
        #region - Debug Exit (Stop) command
        private async void DebugExit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageScriptData == default)
                return;
            if (scriptRun_)
                await RunScriptEngine();
            await ClearScriptEngine();
            pageScriptData.BtnDebugStart = false;
            pageScriptData.BtnDebugStop = true;
        }
        private void DebugExit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (pageScriptData == default)
                e.CanExecute = false;
            else
                e.CanExecute = (bool)pageScriptData?.CanBtnDebugStop;
        }
        #endregion

        #endregion

    }
}
