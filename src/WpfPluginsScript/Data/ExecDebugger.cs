﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Process.Run;
using WpfPlugin.EditBox;

namespace WpfPlugin.Data
{
    public class ExecDebugger
    {
        public enum DebuggerId : int
        {
            None = 0,
            ChromeId = 1,
            VsCodeId = 2
        }
        private static readonly string[] pathExe = new string[]
        {
            @"Google\Chrome\Application\chrome.exe",
            @"Microsoft VS Code\Code.exe"
        };
        private static readonly string[] descExe = new string[]
        {
            "UNKNOWN",
            "Chrome browser",
            "Microsoft Visual Code"
        };
        private CancellationTokenSource cancellation = new();
        private readonly Action<LogEventArgs> toLog = (a) => {};
        private readonly bool [] IsRuning = new bool[3] { true, false, false };
        public readonly string ChromeUrl; /* URL: "chrome://inspect/#devices chrome://inspect" */
        public readonly string VsCodeUrl;
        public string ChromeBin { get; private set; } = string.Empty;
        public string VsCodeBin { get; private set; } = string.Empty;

        public bool IsRun(DebuggerId id)
        {
            return IsRuning[(int)id];
        }

        public ExecDebugger(string path, Action<LogEventArgs> log_)
        {
            toLog = log_;
            string chromeTmp = Path.Combine(path, "chrome-debug");
            ChromeUrl = $"--disable-web-security --user-data-dir=\"{chromeTmp}\" \"{Path.Combine(path, "v8-debug.html")}\""; /* --remote-debugging-port=9229  */
            VsCodeUrl = Path.Combine(path, "v8-debug-vc.code-workspace");
            DebuggerInit();
            DirectoryInfo dir = new(chromeTmp);
            if ((dir != default) && !dir.Exists)
                dir.Create();
        }
        ~ExecDebugger()
        {
            try { cancellation.Cancel(); } catch { }
            try { cancellation.Dispose(); } catch { }
        }

        public async void DebuggerInit()
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        string[] rootPath = new string[]
                        {
                            Environment.ExpandEnvironmentVariables("%ProgramFiles(x86)%"),
                            Environment.ExpandEnvironmentVariables("%ProgramW6432%"),
                            $@"{Environment.ExpandEnvironmentVariables("%HOMEPATH%")}\AppData\Local\Programs",
                            @"E:\__Bin64"
                        };
                        for (int i = 0; i < rootPath.Length; i++)
                            for (int n = 0; n < pathExe.Length; n++)
                            {
                                FileInfo f = new(
                                    Path.Combine(rootPath[i], pathExe[n])
                                );
                                if ((f == default) || !f.Exists)
                                    continue;
                                bool[] b = new bool[]
                                {
                        string.IsNullOrWhiteSpace(ChromeBin),
                        string.IsNullOrWhiteSpace(VsCodeBin)
                                };
                                if (!b[0] && !b[1])
                                    break;
                                if ((n == 0) && b[n])
                                    ChromeBin = f.FullName;
                                else if ((n == 1) && b[n])
                                    VsCodeBin = f.FullName;
                            }
                    }
                    catch (Exception e) { toLog.Invoke(new LogEventArgs(EditorState.Error, e)); }

                }).ConfigureAwait(false);
            } catch (Exception e) { toLog.Invoke(new LogEventArgs(EditorState.Error, e)); }
        }

        public async void Close()
        {
            try
            {
                if (cancellation.IsCancellationRequested)
                    return;

                int idx = IsRuning[1] ? 1 : (IsRuning[2] ? 2 : 0);
                if (idx == 0)
                    return;

                IsRuning[0] = true;
                try { cancellation.Cancel(); } catch { }
                await Task.Delay(1000);
                try { cancellation.Dispose(); } catch { }
                cancellation = new CancellationTokenSource();
                toLog.Invoke(new LogEventArgs(EditorState.Info, new Exception(string.Format(Properties.ResourcesScript.F1, descExe[idx]))));
            }
            catch (Exception e) { toLog.Invoke(new LogEventArgs(EditorState.Error, e)); }
        }

        public async Task Open(DebuggerId id)
        {
            try
            {
                if (IsRun(id))
                    return;

                switch (id)
                {
                    case DebuggerId.ChromeId:
                    case DebuggerId.VsCodeId:
                        {
                            toLog.Invoke(new LogEventArgs(EditorState.Info, new Exception(string.Format(Properties.ResourcesScript.F1, descExe[(int)id]))));
                            break;
                        }
                    default: return;
                }

                string [] exec;
                switch (id)
                {
                    case DebuggerId.ChromeId:
                        {
                            exec = new string[] { ChromeBin, ChromeUrl };
                            break;
                        }
                    case DebuggerId.VsCodeId:
                        {
                            exec = new string[] { VsCodeBin, VsCodeUrl };
                            break;
                        }
                    default: return;
                }
                IsRuning[0] = false;
                IsRuning[(int)id] = true;
                using ProcessResults results = await ProcessEx.RunAsync(exec[0], exec[1], cancellation.Token);
                if (results.ExitCode != 0)
                    toLog.Invoke(new LogEventArgs(EditorState.Error, new ArgumentException(exec[0])));
                if (results.StandardError.Length > 0)
                    foreach (var s in results.StandardError)
                        toLog.Invoke(new LogEventArgs(EditorState.Error, s));
            } catch (Exception e) { toLog.Invoke(new LogEventArgs(EditorState.Error, e)); }
            finally { IsRuning[(int)id] = false; }
        }
    }
}
