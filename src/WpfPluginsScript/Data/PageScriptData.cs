﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows;
using System.ComponentModel;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.EditBox;
using M3U.Model;

namespace WpfPlugin.Data
{
    public class PageScriptData : INotifyPropertyChanged
    {
        private IEventTextFormater jsTextFormater_ = default;
        private ExecDebugger execDebugger_ = default;
        private readonly bool[] debugerButton_ = new bool[4];
        private readonly bool[] jsLoadOpt_ = new bool[4];

        private IServiceControls ServiceCtrl_ = default;
        public IServiceControls ServiceCtrl
        {
            get { return  ServiceCtrl_; }
            private set { ServiceCtrl_ = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }
        private Media outMedia_ = default;
        public  Media OutMedia
        {
            get { return outMedia_; }
            set { outMedia_ = value; OnPropertyChanged(nameof(OutMedia)); }
        }
        public bool BtnDebug1
        {
            get { return debugerButton_[0]; }
            set { SetDebugState(0, value); }
        }
        public bool BtnDebug2
        {
            get { return debugerButton_[1]; }
            set { SetDebugState(1, value); }
        }
        public bool BtnDebugStop
        {
            get { return debugerButton_[2]; }
            set { SetDebugState(2, value); }
        }
        public bool BtnDebugStart
        {
            get { return debugerButton_[3]; }
            set {
                if (debugerButton_[3] != value)
                {
                    debugerButton_[3] = value;
                    OnPropertyChanged(nameof(BtnDebugStart));
                    OnPropertyChanged(nameof(CanBtnDebugStart));
                    OnPropertyChanged(nameof(StateBtnDebugStart));
                }
            }
        }
        public bool CanBtnDebug1
        {
            get { return !debugerButton_[0] && !debugerButton_[1]; }
        }
        public bool CanBtnDebug2
        {
            get { return !debugerButton_[1] && !debugerButton_[0]; }
        }
        public bool CanBtnDebugStop
        {
            get { return debugerButton_[2]; }
        }
        public bool CanBtnDebugStart
        {
            get { return debugerButton_[0] || debugerButton_[1]; }
        }
        public int StateBtnDebugStart
        {
            get { return debugerButton_[3] ? 3 : ((debugerButton_[0] || debugerButton_[1]) ? 2 : 1); }
        }
        public Visibility ProgressVisible
        {
            get { return debugerButton_[0] || debugerButton_[1] ? Visibility.Visible : Visibility.Hidden; }
        }
        public Visibility LoadOtionsVisible
        {
            get { return IsExpanderOpen ? Visibility.Collapsed : Visibility.Visible; }
        }

        public bool IsConfigJsLib
        {
            get { return jsLoadOpt_[0]; }
            set { jsLoadOpt_[0] = value; OnPropertyChanged(nameof(IsConfigJsLib)); }
        }
        public bool IsConfigJsHost
        {
            get { return jsLoadOpt_[1]; }
            set { jsLoadOpt_[1] = value; OnPropertyChanged(nameof(IsConfigJsHost)); }
        }
        public bool IsConfigJsXMLHttpRequest
        {
            get { return jsLoadOpt_[2]; }
            set { jsLoadOpt_[2] = value; OnPropertyChanged(nameof(IsConfigJsXMLHttpRequest)); }
        }
        public bool IsExpanderOpen
        {
            get { return jsLoadOpt_[3]; }
            set
            {
                if (jsLoadOpt_[3] == value)
                    return;
                jsLoadOpt_[3] = value;
                OnPropertyChanged(nameof(IsExpanderOpen));
                OnPropertyChanged(nameof(LoadOtionsVisible));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public string ScriptPath
        {
            get { return jsTextFormater_?.OpenFilePath; }
        }

        private bool GetStopValue() => debugerButton_[0] || debugerButton_[1];
        private bool GetParentValue(int id, bool val) => !val && debugerButton_[id];

        private async void SetDebugState(int idx, bool val)
        {
            switch (idx)
            {
                case 0:
                    {
                        if (debugerButton_[0] == val)
                            break;
                        if (val && debugerButton_[1])
                            break;
                        debugerButton_[0] = val;
                        debugerButton_[1] = GetParentValue(1, val);
                        debugerButton_[2] = GetStopValue();
                        SetNotify(0);
                        if (val)
                            await execDebugger_?.Open(ExecDebugger.DebuggerId.ChromeId);
                        break;
                    }
                case 1:
                    {
                        if (debugerButton_[1] == val)
                            break;
                        if (val && debugerButton_[0])
                            break;
                        debugerButton_[1] = val;
                        debugerButton_[0] = GetParentValue(0, val);
                        debugerButton_[2] = GetStopValue();
                        SetNotify(1);
                        if (val)
                            await execDebugger_?.Open(ExecDebugger.DebuggerId.VsCodeId);
                        break;
                    }
                case 2:
                    {
                        if (val)
                        {
                            execDebugger_?.Close();
                            debugerButton_[0] = debugerButton_[1] = debugerButton_[3] = false;
                            debugerButton_[2] = true;
                            SetNotify(2);
                        }
                        break;
                    }
            }
        }

        public void SetNotify(int id = -1)
        {
            switch (id)
            {
                case 0:
                case 1:
                    {
                        OnPropertyChanged(nameof(BtnDebug1));
                        OnPropertyChanged(nameof(BtnDebug2));
                        OnPropertyChanged(nameof(BtnDebugStart));
                        OnPropertyChanged(nameof(BtnDebugStop));
                        OnPropertyChanged(nameof(CanBtnDebug1));
                        OnPropertyChanged(nameof(CanBtnDebug2));
                        OnPropertyChanged(nameof(CanBtnDebugStop));
                        OnPropertyChanged(nameof(CanBtnDebugStart));
                        OnPropertyChanged(nameof(StateBtnDebugStart));
                        OnPropertyChanged(nameof(ProgressVisible));
                        break;
                    }
                default:
                    {
                        OnPropertyChanged(nameof(ScriptPath));
                        OnPropertyChanged(nameof(BtnDebug1));
                        OnPropertyChanged(nameof(BtnDebug2));
                        OnPropertyChanged(nameof(BtnDebugStart));
                        OnPropertyChanged(nameof(BtnDebugStop));
                        OnPropertyChanged(nameof(CanBtnDebug1));
                        OnPropertyChanged(nameof(CanBtnDebug2));
                        OnPropertyChanged(nameof(CanBtnDebugStop));
                        OnPropertyChanged(nameof(CanBtnDebugStart));
                        OnPropertyChanged(nameof(StateBtnDebugStart));
                        OnPropertyChanged(nameof(ProgressVisible));
                        break;
                    }
            }
        }

        public void SetServiceCtrl(IServiceControls controls, IEventTextFormater jsfmt, ExecDebugger dbg)
        {
            jsTextFormater_ = jsfmt;
            execDebugger_ = dbg;
            ServiceCtrl = controls;
            ServiceCtrl.PropertyChanged += local_PropertyChanged;
            if (ServiceCtrl.ControlConfig.IsLoad)
            {
                IsConfigJsHost = ServiceCtrl.ControlConfig.IsConfigJsHost;
                IsConfigJsLib = ServiceCtrl.ControlConfig.IsConfigJsLib;
                IsConfigJsXMLHttpRequest = ServiceCtrl.ControlConfig.IsConfigJsXMLHttpRequest;
            }
            OnPropertyChanged(nameof(ScriptPath));
        }

        #region event control
        private void local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if ((ServiceCtrl.ControlConfig == null) || (!ServiceCtrl.ControlConfig.IsLoad))
                return;

            switch (e.PropertyName)
            {
                case "ControlConfig":
                    {
                        break;
                    }
            }
        }
        #endregion
    }
}
