﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using PlayListServiceData.Process.Log;
using WpfPlugin.EditBox;

namespace WpfPlugin.Data
{
    public class ScriptLevel : EmptyLevel
    {
        public ScriptLevel(ILogManager m_, TraceEventType t_) : base(m_,t_) { }

        public override ILogManager Write(string text)
        {
            return m.Write(text);
        }
        public override ILogManager Write(string text, Exception e)
        {
            if (e != default)
                return m.Write($"{text} : {e.Message}");
            else
                return m.Write(text);
        }
    }
    public class ScriptLogManager : EmptyLogManager
    {
        private readonly Action<LogEventArgs> wpfLog;

        public ScriptLogManager(Action<LogEventArgs> act)
        {
            wpfLog = act;
        }

        public override ILogLevel Error => new ScriptLevel(this, TraceEventType.Error);
        public override ILogLevel Warning => new ScriptLevel(this, TraceEventType.Warning);
        public override ILogLevel Info => new ScriptLevel(this, TraceEventType.Information);
        public override ILogLevel Verbose => new ScriptLevel(this, TraceEventType.Verbose);
        public override ILogManager Write(string s)
        {
            wpfLog.Invoke(new LogEventArgs(EditorState.Script, s));
            return this;
        }
    }
}
