﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using WpfPlugin.EditBox;

namespace WpfPlugin
{
    public class JsEditBehaviors : Behavior<RichTextBox>
    {
        private readonly JsEventTextFormater JsTextFormater_ = new();
        public  JsEventTextFormater JsTextFormater
        {
            get { return JsTextFormater_; }
        }

        public JsEditBehaviors() : base()
        {
            JsTextFormater_.WatcherCb = (a) =>
            {
                if (AssociatedObject != default)
                    JsTextFormater_.OpenDocument(AssociatedObject, a.FullPath);
            };
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.TextChanged += TextChanged;
            AssociatedObject.KeyUp += KeyUp;
            AssociatedObject.KeyDown += KeyDown;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.TextChanged -= TextChanged;
            AssociatedObject.KeyUp -= KeyUp;
            AssociatedObject.KeyDown -= KeyDown;
        }

        #region EditBox Event
        private async void TextChanged(object sender, TextChangedEventArgs e)
        {
            AssociatedObject.TextChanged -= TextChanged;
            await JsTextFormater_.TextChangedAsync(AssociatedObject, e);
            AssociatedObject.TextChanged += TextChanged;
        }
        private void KeyUp(object sender, KeyEventArgs e)
        {
            JsTextFormater_.KeyUp(AssociatedObject, e);
        }
        private void KeyDown(object sender, KeyEventArgs e)
        {
            JsTextFormater_.KeyDown(e);
        }
        #endregion
    }
}
