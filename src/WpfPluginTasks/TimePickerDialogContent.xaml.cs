﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license.
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using PlayListServiceData.RunManager;

namespace WpfPlugin
{
    internal static class DependencyPropertyConstruct
    {
        internal static DependencyProperty Build<T1, T2>(string n, Action<T2, DependencyPropertyChangedEventArgs> act, bool mode = false)
        {
            return DependencyProperty.Register(n, typeof(T1), typeof(T2),
                new FrameworkPropertyMetadata((a, e) => {
                    if (a is T2 cls)
                        act.Invoke(cls, e);
                })
                {
                    BindsTwoWayByDefault = mode
                });
        }
    }

    public partial class TimePickerDialogContent : UserControl, INotifyPropertyChanged
    {
        public enum ViewSettingsId : int
        {
            None,
            DiasabledAll,
            DueTime,
            PeriodTime,
            OnceTime,
            DayOfWeekTime
        }

        public class ViewSettings
        {
            public ViewSettingsId Id = ViewSettingsId.None;
            public bool [] IsEnableds = default;
            public int [] Limit = default;
            public string Title = string.Empty;
        }

        private readonly ViewSettings[] settings = new ViewSettings[]
        {
            new ViewSettings {
                Id = ViewSettingsId.PeriodTime,
                IsEnableds = new bool [] { true, true, true },
                Limit = new int [] { 31, 23, 59 },
                Title = Properties.ResourcesTask.S10
            },
            new ViewSettings {
                Id = ViewSettingsId.DueTime,
                IsEnableds = new bool [] { false, true, true },
                Limit = new int [] { 0, 23, 59 },
                Title = Properties.ResourcesTask.S11
            },
            new ViewSettings {
                Id = ViewSettingsId.DayOfWeekTime,
                IsEnableds = new bool [] { true, false, false },
                Limit = new int [] { 7, 0, 0 },
                Title = Properties.ResourcesTask.S12
            },
            new ViewSettings {
                Id = ViewSettingsId.DiasabledAll,
                IsEnableds = new bool [] { false, false, false },
                Limit = new int [] { 0, 0, 0 },
                Title = Properties.ResourcesTask.S13
            },
            new ViewSettings {
                Id = ViewSettingsId.OnceTime,
                IsEnableds = new bool [] { false, true, true },
                Limit = new int [] { 0, 23, 59 },
                Title = Properties.ResourcesTask.S14
            }
        };

        private ModernWpf.Controls.ContentDialog Dialog = default;
        private static ViewSettings Current = default;
        private TimeSpan Origin = TimeSpan.Zero;

        #region PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void OnPropertyChanged(params string [] names)
        {
            foreach (string name in names)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
            if (Dialog != default)
                Dialog.IsPrimaryButtonEnabled = true;
        }
        #endregion

        public TimePickerDialogContent(ViewSettingsId id, TimeSpan ts)
        {
            if (id == ViewSettingsId.None)
                throw new InvalidEnumArgumentException(nameof(id));

            Current = (from i in settings
                       where i.Id == id
                       select i).FirstOrDefault();
            if (Current == default)
                throw new ArgumentOutOfRangeException(nameof(id));

            Origin = ts;

            InitializeComponent();
            TimePickerDialog.DataContext = this;

            for (int i = 0; i < Current.IsEnableds.Length; i++)
                switch (i)
                {
                    case 0:
                        {
                            DaysEnabled = Current.IsEnableds[i];
                            DaysMaximum = Current.Limit[i];
                            break;
                        }
                    case 1:
                        {
                            HoursEnabled = Current.IsEnableds[i];
                            HoursMaximum = Current.Limit[i];
                            break;
                        }
                    case 2:
                        {
                            MinutesEnabled = Current.IsEnableds[i];
                            MinutesMaximum = Current.Limit[i];
                            break;
                        }
                }
            Value = ts;
            Days = ts.Days;
            Hours = ts.Hours;
            Minutes = ts.Minutes;
            if (ts.Seconds > 30)
                Minutes += 1;
        }

        #region Value Property
        public TimeSpan Value
        {
            get { return (TimeSpan)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty =
            DependencyPropertyConstruct.Build<TimeSpan, TimePickerDialogContent>(nameof(Value), (a, e) =>
            {
                if ((e.NewValue is TimeSpan ts) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.Days = ts.Days;
                    ctrl.Hours = ts.Hours;
                    ctrl.Minutes = ts.Minutes;
                }
                a.OnPropertyChanged(nameof(Value), nameof(Days), nameof(Hours), nameof(Minutes));
            }, true);
        #endregion

        #region Days Property
        public int Days
        {
            get { return (int)GetValue(DaysProperty); }
            set { SetValue(DaysProperty, value); }
        }
        public static readonly DependencyProperty DaysProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(Days), (a, e) =>
            {
                if ((a is TimePickerDialogContent ctrl) && ctrl.SetValidateNumber(e, 0))
                    ctrl.OnPropertyChanged(nameof(Days));
            }, true);

        public bool DaysEnabled
        {
            get { return (bool)GetValue(DaysEnabledProperty); }
            set { SetValue(DaysEnabledProperty, value); }
        }
        public static readonly DependencyProperty DaysEnabledProperty =
            DependencyPropertyConstruct.Build<bool, TimePickerDialogContent>(nameof(DaysEnabled), (a, e) =>
            {
                if ((e.NewValue is bool b) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.DaysEnabled = b;
                    ctrl.OnPropertyChanged(nameof(DaysEnabled));
                }
            }, true);

        public int DaysMaximum
        {
            get { return (int)GetValue(DaysMaximumProperty); }
            set { SetValue(DaysMaximumProperty, value); }
        }
        public static readonly DependencyProperty DaysMaximumProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(DaysMaximum), (a, e) =>
            {
                if ((e.NewValue is int i) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.DaysMaximum = i;
                    ctrl.OnPropertyChanged(nameof(DaysMaximum));
                }
            }, true);
        #endregion

        #region Hours Property
        public int Hours
        {
            get { return (int)GetValue(HoursProperty); }
            set { SetValue(HoursProperty, value); }
        }
        public static readonly DependencyProperty HoursProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(Hours), (a, e) =>
            {
                if ((a is TimePickerDialogContent ctrl) && ctrl.SetValidateNumber(e, 1))
                    ctrl.OnPropertyChanged(nameof(Hours));
            }, true);

        public bool HoursEnabled
        {
            get { return (bool)GetValue(HoursEnabledProperty); }
            set { SetValue(HoursEnabledProperty, value); }
        }
        public static readonly DependencyProperty HoursEnabledProperty =
            DependencyPropertyConstruct.Build<bool, TimePickerDialogContent>(nameof(HoursEnabled), (a, e) =>
            {
                if ((e.NewValue is bool b) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.HoursEnabled = b;
                    ctrl.OnPropertyChanged(nameof(HoursEnabled));
                }
            }, true);

        public int HoursMaximum
        {
            get { return (int)GetValue(HoursMaximumProperty); }
            set { SetValue(HoursMaximumProperty, value); }
        }
        public static readonly DependencyProperty HoursMaximumProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(HoursMaximum), (a, e) =>
            {
                if ((e.NewValue is int i) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.HoursMaximum = i;
                    ctrl.OnPropertyChanged(nameof(HoursMaximum));
                }
            }, true);
        #endregion

        #region Minutes Property
        public int Minutes
        {
            get { return (int)GetValue(MinutesProperty); }
            set { SetValue(MinutesProperty, value); }
        }
        public static readonly DependencyProperty MinutesProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(Minutes), (a, e) =>
            {
                if ((a is TimePickerDialogContent ctrl) && ctrl.SetValidateNumber(e, 2))
                    ctrl.OnPropertyChanged(nameof(Minutes));
            }, true);

        public bool MinutesEnabled
        {
            get { return (bool)GetValue(MinutesEnabledProperty); }
            set { SetValue(MinutesEnabledProperty, value); }
        }
        public static readonly DependencyProperty MinutesEnabledProperty =
            DependencyPropertyConstruct.Build<bool, TimePickerDialogContent>(nameof(MinutesEnabled), (a, e) =>
            {
                if ((e.NewValue is bool b) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.MinutesEnabled = b;
                    ctrl.OnPropertyChanged(nameof(MinutesEnabled));
                }
            }, true);

        public int MinutesMaximum
        {
            get { return (int)GetValue(MinutesMaximumProperty); }
            set { SetValue(MinutesMaximumProperty, value); }
        }
        public static readonly DependencyProperty MinutesMaximumProperty =
            DependencyPropertyConstruct.Build<int, TimePickerDialogContent>(nameof(MinutesMaximum), (a, e) =>
            {
                if ((e.NewValue is int i) && (a is TimePickerDialogContent ctrl))
                {
                    ctrl.MinutesMaximum = i;
                    ctrl.OnPropertyChanged(nameof(MinutesMaximum));
                }
            }, true);
        #endregion

        public static ViewSettingsId IdSelector(SheduleManager.ActionType id, ViewSettingsId vid)
        {
            switch (id)
            {
                case SheduleManager.ActionType.INT_DAYS:
                case SheduleManager.ActionType.INT_HOURS:
                case SheduleManager.ActionType.INT_MINUTES:
                case SheduleManager.ActionType.INT_SECONDS: return vid;
                case SheduleManager.ActionType.RUN_HOLD:
                case SheduleManager.ActionType.RUN_ONCE:
                    {
                        return (vid == ViewSettingsId.PeriodTime) ?
                            ViewSettingsId.DiasabledAll : ViewSettingsId.OnceTime;
                    }
                case SheduleManager.ActionType.RUN_DAYOFWEEK:
                    {
                        return (vid == ViewSettingsId.PeriodTime) ?
                            ViewSettingsId.DayOfWeekTime : ViewSettingsId.DueTime;
                    }
                default: return ViewSettingsId.None;
            }
        }

        private bool SetValidateNumber(DependencyPropertyChangedEventArgs args, int idx)
        {
            try
            {
                if (Current == default)
                {
                    Value = TimeSpan.Zero;
                    return false;
                }

                bool b = false;
                int val = 0;
                if (args.NewValue is int i)
                {
                    b = (i >= 0) && (i <= Current.Limit[idx]);
                    val = b ? i : 0;
                }
                if (!b && (args.OldValue is int old))
                    val = old;

                switch (idx)
                {
                    case 0: Days = val; break;
                    case 1: Hours = val; break;
                    case 2: Minutes = val; break;
                }
                if (b)
                    Value = new TimeSpan(Days, Hours, Minutes, 0);
                return b;
            } catch { return false; }
        }

        public async Task<bool> Show(ModernWpf.Controls.ContentDialog d)
        {
            try
            {
                Dialog = d;
                Dialog.Title = Current?.Title;
                if (Current.Id == ViewSettingsId.DiasabledAll)
                    Dialog.IsSecondaryButtonEnabled = false;
                Dialog.IsPrimaryButtonEnabled = false;
                Dialog.PrimaryButtonClick += (s, a) =>
                {
                    a.Cancel = false;
                    Application.Current.Dispatcher.Invoke(() =>
                        Value = new TimeSpan(Days, Hours, Minutes, 0));
                };
                Dialog.SecondaryButtonClick += (s, a) =>
                {
                    a.Cancel = true;
                    Application.Current.Dispatcher.Invoke(() =>
                        Value = Origin);
                    Dialog.IsPrimaryButtonEnabled = false;
                };
                var r = await Dialog.ShowAsync().ConfigureAwait(false);
                return r switch
                {
                    ModernWpf.Controls.ContentDialogResult.Primary => true,
                    _ => false
                };
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
