﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license.
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfPlugin.Data
{
    [ValueConversion(typeof(string), typeof(string))]
    public class TimePickerConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan ts)
                return ts.ToString("dd\\.hh\\:mm");
            return "00.00:00";
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string s)
                if (TimeSpan.TryParse(s, out TimeSpan ts))
                    return ts;
            return TimeSpan.Zero;
        }
    }
}
