﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license.
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using PlayListServiceData.Process.Http.Data;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class HostConfigGet : IHostConfigGet
    {
        private readonly IServiceControls ServiceCtrl;

        public HostConfigGet(IServiceControls ctl) => ServiceCtrl = ctl;

        public int ConfigNetPort => ServiceCtrl.ControlConfig.ConfigNetPort;
        public bool IsConfigNetSslEnable => ServiceCtrl.ControlConfig.IsConfigNetSslEnable;
        public bool IsConfigNetHttpSupport => ServiceCtrl.ControlConfig.IsConfigNetHttpSupport;
        public string ConfigNetSslDomain => ServiceCtrl.ControlConfig.ConfigNetSslDomain;
        public string ConfigNetSslCertStore => ServiceCtrl.ControlConfig.ConfigNetSslCertStore;
        public string ConfigNetSslCertThumbprint => ServiceCtrl.ControlConfig.ConfigNetSslCertThumbprint;
        public string ConfigNetAddr => ServiceCtrl.ControlConfig.ConfigNetAddr;
        public List<string> ConfigUserAuth => ServiceCtrl.ControlConfig.ConfigUserAuth;
    }
}
