﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using PlayListServiceData.Process.Log;

namespace WpfPlugin.Data
{
    public class LocalLogLevel : EmptyLevel
    {
        public LocalLogLevel(ILogManager m_, TraceEventType t_) : base(m_, t_) { }

        public override ILogManager Write(string text) {
#           if DEBUG
            return m.Write(text);
#           else
            return m;
#           endif
        }
        public override ILogManager Write(string text, Exception e) {
#           if DEBUG
            return m.Write($"{text} -> {e.Message}");
#           else
            return m;
#           endif
        }
    }
    public class LocalLogManager : EmptyLogManager
    {
        private readonly Action<string> Log;
        public LocalLogManager(Action<string> action) => Log = action ?? throw new ArgumentNullException(nameof(action));
        public override ILogLevel Error => new LocalLogLevel(this, TraceEventType.Error);
        public override ILogLevel Warning => new LocalLogLevel(this, TraceEventType.Warning);
        public override ILogLevel Info => new LocalLogLevel(this, TraceEventType.Information);
        public override ILogLevel Verbose => new LocalLogLevel(this, TraceEventType.Verbose);
        public override ILogManager Write(string s) {
#           if DEBUG
            System.Diagnostics.Debug.WriteLine(s);
#           endif
            Log.Invoke(s);
            return this;
        }
    }
}
