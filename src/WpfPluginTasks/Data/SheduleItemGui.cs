﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license.
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Threading;
using PlayListServiceData.Base;
using PlayListServiceData.RunManager;
using PlayListServiceData.Utils;
using Swan;
using WpfPlugin.Properties;

namespace WpfPlugin.Data
{
    public class SheduleItemGui : PropertyChangedEvent, ISheduleItemValues
    {
        private const string FORMAT_TIME = "dd\\.hh\\:mm";
        private const string FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

        public SheduleItemGui() { }
        public SheduleItemGui(ISheduleItemValues shi) => Copy(shi);

        private SheduleManager.ActionType _RunType = SheduleManager.ActionType.NONE;
        public string RunTypeName { get; private set; } = string.Empty;
        public string RunTypeDescription { get => _RunType.ToString(); }
        public SheduleManager.ActionType RunType
        {
            get => _RunType;
            private set {
                _RunType = value;
                try
                {
                    string [] ss = value.ToString().ToLowerInvariant().Split(new char[] { '_' });
                    if ((ss != default) && (ss.Length > 0)) {
                        string s = ss[ss.Length - 1];
                        RunTypeName = $"{char.ToUpper(s[0])}{s.Substring(1)}".Humanize();
                    }
                    else
                        RunTypeName = "Unknown";
                } catch { RunTypeName = "Unknown"; }
                OnPropertyChanged(nameof(RunType), nameof(RunTypeName), nameof(RunTypeDescription));
            }
        }

        private object _Id = default;
        public string IdName { get; private set; } = string.Empty;
        public string IdDescription { get => (_Id == default) ? string.Empty : _Id.ToString(); }
        public object Id
        {
            get => _Id;
            private set {
                if (value == default)
                    return;
                _Id = value;
                try {
                    string str = value.ToString().ToLowerInvariant();
                    if (str.Equals("-1"))
                        IdName = "System";
                    else
                    {
                        string[] ss = str.Split(new char[] { '_' });
                        if ((ss != default) && (ss.Length > 0))
                        {
                            string s = ss[ss.Length - 1];
                            IdName = $"{char.ToUpper(s[0])}{s.Substring(1)}".Humanize();
                        }
                        else
                            IdName = "Unknown";
                    }
                } catch { IdName = "Unknown"; }
                OnPropertyChanged(nameof(Id), nameof(IdName), nameof(IdDescription));
            }
        }
        private BaseExceptionXml _LastError = default;
        public bool IsLastError => (_LastError != default) && (!string.IsNullOrWhiteSpace(LastError.Message));
        public BaseExceptionXml LastError
        {
            get => _LastError;
            set {
                _LastError = value;
                OnPropertyChanged(nameof(LastError), nameof(IsLastError), nameof(LastErrorString));
            }
        }
        public Exception LastErrorEx
        {
            get => IsLastError ? LastError.Get() : default;
            private set {
                LastError = IsLastError ? LastError : new();
                LastError.Copy(value);
                OnPropertyChanged(nameof(LastError), nameof(IsLastError), nameof(IsShowError), nameof(LastErrorString), nameof(LastErrorEx));
            }
        }
        public string LastErrorString
        {
            get
            {
                if (!IsLastError)
                    return string.Empty;

                StringBuilder sb = new();
                sb.Append($"{RunType} - {LastError.Message}");
                if ((LastError.InnerException != default) && (LastError.InnerException.Count > 0))
                    foreach (BaseExceptionInnerXml t in LastError.InnerException)
                        sb.Append($"{Environment.NewLine}\t- {t.TypeException}: {t.Message}");
                return sb.ToString();
            }
        }
        private bool _IsShowError = false;
        public bool IsShowError
        {
            get => IsLastError && _IsShowError;
            set
            {
                _IsShowError = value;
                OnPropertyChanged(nameof(IsShowError), nameof(IsLastError), nameof(LastErrorString));
            }
        }
        private string _Status = string.Empty;
        public string Status
        {
            get => _Status;
            private set {
                _Status = value;
                OnPropertyChanged(nameof(Status));
            }
        }

        private bool _IsEnable = false;
        public bool IsEnable
        {
            get => _IsEnable;
            private set
            {
                _IsEnable = value;
                OnPropertyChanged(nameof(IsEnable));
            }
        }
        private bool _IsActive = false;
        public bool IsActive
        {
            get => _IsActive;
            private set
            {
                _IsActive = value;
                OnPropertyChanged(nameof(IsActive));
            }
        }
        private bool _IsRuning = false;
        public bool IsRuning
        {
            get => _IsRuning;
            private set
            {
                _IsRuning = value;
                OnPropertyChanged(nameof(IsRuning));
            }
        }
        private bool _IsChange = false;
        public bool IsChange
        {
            get => _IsChange;
            private set
            {
                _IsChange = value;
                OnPropertyChanged(nameof(IsChange));
            }
        }
        private bool _IsUpdate = false;
        public bool IsUpdate
        {
            get => _IsUpdate;
            private set
            {
                _IsUpdate = value;
                OnPropertyChanged(nameof(IsUpdate));
            }
        }

        private DateTime _LastRunTime = DateTime.MinValue;
        public string LastRunTimeString { get; private set; } = ResourcesTask.L1;
        public DateTime LastRunTime
        {
            get => _LastRunTime;
            private set
            {
                if (value == default)
                {
                    LastRunTimeString = ResourcesTask.L1;
                    return;
                }
                _LastRunTime = value;
                LastRunTimeString = value.ToString(FORMAT_DATE_TIME);
                LastPeriodTime = DateTime.Now - value;
                OnPropertyChanged(nameof(LastRunTime), nameof(LastRunTimeString));
            }
        }

        private TimeSpan _LastPeriodTime = Timeout.InfiniteTimeSpan;
        public string LastPeriodTimeString { get; private set; } = string.Empty;
        public TimeSpan LastPeriodTime
        {
            get => _LastPeriodTime;
            set
            {
                LastPeriodTimeString = TimeSpanParse(value, true);

                if (value == null)
                    return;

                _LastPeriodTime = value;
                OnPropertyChanged(nameof(LastPeriodTime), nameof(LastPeriodTimeString));
            }
        }

        private TimeSpan _PeriodTime = Timeout.InfiniteTimeSpan;
        public string PeriodDescription {
            get {
                return RunType switch
                {
                    SheduleManager.ActionType.RUN_DAYOFWEEK =>
                        string.Format(ResourcesTask.S18, IsDayOfWeekTime ? DayOfWeekTime.HumanizeDayOfWeek() : "-"),
                    SheduleManager.ActionType.INT_DAYS => TimeSpanDiffNext(),
                    SheduleManager.ActionType.INT_HOURS => TimeSpanDiffNext(),
                    SheduleManager.ActionType.INT_MINUTES => TimeSpanDiffNext(),
                    SheduleManager.ActionType.INT_SECONDS => TimeSpanDiffNext(),
                    SheduleManager.ActionType.RUN_ONCE => ResourcesTask.S19,
                    SheduleManager.ActionType.RUN_HOLD => ResourcesTask.S19,
                    _ => ResourcesTask.S10
                };
            }
        }
        public string PeriodTimeString { get; private set; } = ResourcesTask.L2;
        public TimeSpan PeriodTime
        {
            get => _PeriodTime;
            set
            {
                PeriodTimeString = TimeSpanParse(value);

                if (value == default)
                    return;

                _PeriodTime = value;
                IsUpdate = true;
                OnPropertyChanged(nameof(PeriodTime), nameof(PeriodTimeString), nameof(PeriodDescription));
            }
        }
        private TimeSpan _DueTime = Timeout.InfiniteTimeSpan;
        public string DueTimeString { get; private set; } = ResourcesTask.L2;
        public TimeSpan DueTime
        {
            get => _DueTime;
            set
            {
                DueTimeString = TimeSpanParse(value);

                if (value == default)
                    return;

                _DueTime = value;
                IsUpdate = true;
                OnPropertyChanged(nameof(DueTime), nameof(DueTimeString));
            }
        }
        private TimeSpan _DayOfWeekTime = TimeSpan.Zero;
        public bool IsDayOfWeekTime { get => (_DayOfWeekTime != TimeSpan.Zero) && (_DayOfWeekTime != Timeout.InfiniteTimeSpan); }
        public TimeSpan DayOfWeekTime
        {
            get => _DayOfWeekTime;
            set
            {
                if (value == default)
                    return;

                _DayOfWeekTime = value;
                IsUpdate = true;
                OnPropertyChanged(nameof(DayOfWeekTime), nameof(IsDayOfWeekTime), nameof(PeriodDescription));
            }
        }

        public void SetEnable(bool b)
        {
            IsEnable = b;
            IsUpdate = true;
        }
        public void SetUpdate(bool b)
        {
            IsUpdate = b;
        }

        public void Copy(ISheduleItemValues shi)
        {
            if (shi == default)
                return;

            Id = shi.Id;
            RunType = shi.RunType;
            IsEnable = shi.IsEnable;
            IsActive = shi.IsActive;
            IsRuning = shi.IsRuning;
            IsChange = shi.IsChange;

            DueTime = (shi.DueTime == null) ? Timeout.InfiniteTimeSpan : shi.DueTime;
            PeriodTime = (shi.PeriodTime == null) ? Timeout.InfiniteTimeSpan : shi.PeriodTime;
            DayOfWeekTime = (shi.DayOfWeekTime == null) ? Timeout.InfiniteTimeSpan : shi.DayOfWeekTime;

            LastRunTime = shi.LastRunTime;
            LastError = shi.LastError;
            Status = shi.Status;
            IsUpdate = false;
        }

        private string TimeSpanParse(TimeSpan ts, bool ishuman = false)
        {
            if ((ts == null) || (ts == Timeout.InfiniteTimeSpan))
                return ResourcesTask.L3;
            else if (ts == TimeSpan.Zero)
                return ResourcesTask.L2;
            else if (ishuman)
                return ResourcesTask.S17 + " " + ts.Humanize(new string[] {
                    ResourcesTask.N1,
                    ResourcesTask.N2,
                    ResourcesTask.N3,
                    ResourcesTask.N4
                });
            else
                return ts.ToString(FORMAT_TIME);
        }
        private string TimeSpanDiffNext()
        {
            if ((LastRunTime == DateTime.MinValue) ||
                (PeriodTime == Timeout.InfiniteTimeSpan) ||
                (PeriodTime == TimeSpan.Zero))
                return ResourcesTask.S10;

            try {
                return string.Format(ResourcesTask.S20, (PeriodTime - (DateTime.Now - LastRunTime)).ToString(FORMAT_TIME));
            } catch { return ResourcesTask.S10; }
        }
    }
}
