﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Http.Data;
using PlayListServiceData.RunManager;
using PlayListServiceData.ServiceConfig;
using PlayListServiceData.Utils;

namespace WpfPlugin.Data
{
    public class PageTasksData : PropertyChangedEvent
    {
        private static readonly string httpquery_ = "cmd/command/tasks";
        private readonly CancellationTokenSafe token = new();
        private Uri Url = default;
        private HostConfig Host = default;
        private IServiceControls _ServiceCtrl = default;
        public  IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }
        public ObservableCollection<SheduleItemGui> SheduleTasks { get; private set; }
        public SheduleItemGui SelectedTask { get; set; } = default;
        public bool IsSelectedTask
        {
            get => SelectedTask != default;
        }
        private string ErrorMessage_ = default;
        public  string ErrorMessage
        {
            get => ErrorMessage_;
            set { ErrorMessage_ = value; OnPropertyChanged(nameof(ErrorMessage), nameof(IsErrorMessage)); }
        }
        public bool IsErrorMessage { get => !string.IsNullOrWhiteSpace(ErrorMessage); }
        private bool IsUpdate_ = false;
        public bool IsUpdate
        {
            get => IsUpdate_;
            set { if (IsUpdate_ != value) IsUpdate_ = value; OnPropertyChanged(nameof(IsUpdate)); }
        }
        private bool IsNoCheckServiceRun_ = false;
        public bool IsNoCheckServiceRun
        {
            get => IsNoCheckServiceRun_;
            set { if (IsNoCheckServiceRun_ != value) IsNoCheckServiceRun_ = value; OnPropertyChanged(nameof(IsNoCheckServiceRun)); }
        }
        private bool IsPlaySound_ = true;
        public bool IsPlaySound
        {
            get => IsPlaySound_;
            set { if (IsPlaySound_ != value) IsPlaySound_ = value; OnPropertyChanged(nameof(IsPlaySound)); }
        }
        private bool IsUpdateBackground_ = true;
        public bool IsUpdateBackground
        {
            get => IsUpdateBackground_;
            set { if (IsUpdateBackground_ != value) IsUpdateBackground_ = value; OnPropertyChanged(nameof(IsUpdateBackground)); }
        }
        public bool IsReloadTimer
        {
            get => ReloadTimer.IsEnabled;
        }
        private readonly WpfTimer ReloadTimer_;
        public WpfTimer ReloadTimer
        {
            get => ReloadTimer_;
        }

        public PageTasksData()
        {
            SheduleTasks = new ObservableCollection<SheduleItemGui>();
            ReloadTimer_ = new WpfTimer(
                ReloadTimerCb,
                () => Application.Current.Dispatcher.Invoke(
                    () => OnPropertyChanged(nameof(IsReloadTimer))));
        }
        ~PageTasksData()
        {
            ReloadTimer.Dispose();
        }

        #region Reload timer
        private async void ReloadTimerCb(object sender, EventArgs e)
        {
            try {
                await LoadTasksData().ConfigureAwait(false);
                if (IsPlaySound)
                    SystemSounds.Hand.Play();
            } catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
        }
        #endregion

        #region Set Service Control
        public async void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
            if (BuildServiceUrl())
                await LoadTasksData();
        }

        public void EnableTask()
        {
            if (SelectedTask == default)
                return;
            SelectedTask.SetEnable(!SelectedTask.IsEnable);
            IsUpdate = true;
        }
        #endregion

        #region Load Tasks Data
        public async Task LoadTasksData()
        {
            try
            {
                PreCheckConnection();

                await Task.Run(async () => {
                    HttpResponseMessage r = default;
                    try
                    {
                        r = await HttpClientDefault.NewContentRequestBasicAuth(
                            Url, Host.AuthenticationHeader, token.Token).ConfigureAwait(false);

                        if ((r != default) && r.IsSuccessStatusCode)
                        {
                            string s = await r.Content.ReadAsStringAsync().ConfigureAwait(false);
                            UTF8Encoding utf = new(false);
                            using MemoryStream ms = new(utf.GetBytes(s));
                            using StreamReader sr = new(ms, utf, false);
                            XmlSerializer xml = new(typeof(SheduleItemsLite));

                            Application.Current.Dispatcher.Invoke(() => SheduleTasks.Clear());
                            if (xml.Deserialize(sr) is SheduleItemsLite items)
                                foreach (SheduleItemLite item in items.Items)
                                    Application.Current.Dispatcher.Invoke(() => SheduleTasks.Add(new SheduleItemGui(item)));
                            Application.Current.Dispatcher.Invoke(() => ErrorMessage = string.Empty);
                        }
                        else if (r != default)
                            Application.Current.Dispatcher.Invoke(() => ErrorMessage = r.ReasonPhrase);
                    }
                    catch (Exception ex) {
                        try { Application.Current.Dispatcher.Invoke(() => ErrorMessage = ex.Message); }
                        catch { }
                    }
                    finally
                    {
                        if (r != default)
                            try { r.Dispose(); } catch { }
                        try { Application.Current.Dispatcher.Invoke(() => IsUpdate = false); }
                        catch { }
                    }
                });
            }
            catch (Exception ex) { ErrorMessage = ex.Message; }
        }
        #endregion

        #region Save Tasks Data
        public async Task SaveTasksData()
        {
            try
            {
                PreCheckConnection();

                if (!IsUpdate)
                    return;

                await Task.Run(async () => {
                    try
                    {
                        SheduleItemsLite items = new();
                        foreach (SheduleItemGui item in SheduleTasks)
                            if (item.IsUpdate)
                                items.Add(new SheduleItemLite(item));

                        if (items.Items.Count == 0)
                            return;

                        UTF8Encoding utf = new(false);
                        using MemoryStream ms = new();
                        using StreamWriter sr = new(ms, utf);
                        XmlSerializer xml = new(typeof(SheduleItemsLite));
                        xml.Serialize(sr, items);
                        string body = utf.GetString(ms.ToArray());
                        if (string.IsNullOrWhiteSpace(body))
                            return;

                        bool b = await HttpClientDefault.PostContentBasicAuth(
                            Url, Host.AuthenticationHeader, body).ConfigureAwait(false);

                        if (b)
                        {
                            Application.Current.Dispatcher.Invoke(() => IsUpdate = false);
                            foreach (SheduleItemGui item in SheduleTasks)
                                if (item.IsUpdate)
                                    Application.Current.Dispatcher.Invoke(() => item.SetUpdate(false));
                        }
                    }
                    catch (Exception ex) { Application.Current.Dispatcher.Invoke(() => ErrorMessage = ex.Message); }
                });
            }
            catch (Exception ex) { ErrorMessage = ex.Message; }
        }
        #endregion

        #region TimePicker View
        public async Task<bool> TimePickerView(TimePickerDialogContent.ViewSettingsId ids)
        {
            try
            {
                if (SelectedTask == default)
                    return false;

                TimeSpan tsIn = (SelectedTask.RunType == SheduleManager.ActionType.RUN_DAYOFWEEK) ? SelectedTask.DayOfWeekTime :
                    ((ids == TimePickerDialogContent.ViewSettingsId.PeriodTime) ? SelectedTask.PeriodTime : SelectedTask.DueTime);

                TimePickerDialogContent cdc = new(
                    TimePickerDialogContent.IdSelector(SelectedTask.RunType, ids), tsIn);

                ModernWpf.Controls.ContentDialog dialog = new()
                {
                    Title = cdc.Tag.ToString(),
                    IsPrimaryButtonEnabled = true,
                    IsSecondaryButtonEnabled = true,
                    PrimaryButtonText = Properties.ResourcesTask.S1,
                    SecondaryButtonText = Properties.ResourcesTask.S2,
                    CloseButtonText = Properties.ResourcesTask.S3,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Primary,
                    VerticalContentAlignment = VerticalAlignment.Top,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Content = cdc
                };
                bool b = await cdc.Show(dialog).ConfigureAwait(false);
                if (!b)
                    return false;
                TimeSpan tsOut = TimeSpan.Zero;
                Application.Current.Dispatcher.Invoke(() => tsOut = cdc.Value);
                if (tsIn == tsOut)
                    return false;

                if (ids == TimePickerDialogContent.ViewSettingsId.DueTime)
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SelectedTask.DueTime = tsOut;
                        IsUpdate = true;
                    });
                else if (ids == TimePickerDialogContent.ViewSettingsId.PeriodTime)
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SelectedTask.PeriodTime = tsOut;
                        IsUpdate = true;
                    });
                return true;
            }
            catch (Exception ex) { Application.Current.Dispatcher.Invoke(() => ErrorMessage = ex.Message); }
            return false;
        }
        #endregion

        #region private
        private void PreCheckConnection()
        {
            Application.Current.Dispatcher.Invoke(() => ErrorMessage = string.Empty);

            if (Url == default) {
                if (!BuildServiceUrl())
                    throw new Exception(Properties.ResourcesTask.E10);
            }
            if (Host.AuthenticationHeader == default)
                throw new Exception(Properties.ResourcesTask.E14);
            if (!ServiceCtrl.IsControlConfig)
                throw new Exception(Properties.ResourcesTask.E11);
            if (!IsNoCheckServiceRun && (!ServiceCtrl.IsControlServiceRun))
                throw new Exception(Properties.ResourcesTask.E12);
            if (!ServiceCtrl.ControlConfig.IsConfigNetSslEnable)
                throw new Exception(Properties.ResourcesTask.E13);
        }
        private bool BuildServiceUrl()
        {
            Host = new(new HostConfigGet(ServiceCtrl),
                new LocalLogManager((s) => ErrorMessage = s));

            if (Host.HTTPS.IsValid)
                Url = new Uri($"{Host.HTTPS}{httpquery_}", UriKind.Absolute);
            else if (Host.HTTP.IsValid)
                Url = new Uri($"{Host.HTTP}{httpquery_}", UriKind.Absolute);

            return Url != default;
        }
        #endregion

    }
}
