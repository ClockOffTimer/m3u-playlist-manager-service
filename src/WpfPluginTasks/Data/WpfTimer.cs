﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license.
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Windows.Threading;

namespace WpfPlugin.Data
{
    public class WpfTimer : IDisposable
    {
        private readonly Lazy<DispatcherTimer> Timer_;
        private readonly Action<object,EventArgs> action_;
        private readonly Action onupdate_;
        private EventHandler TimerHandler_;

        #region init
        private DispatcherTimer TimerInit()
        {
            DispatcherTimer dt = new();
            TimerHandler_ = new EventHandler(Timer_Handler);
            dt.Tick += TimerHandler_;
            return dt;
        }
        #endregion

        #region events
        private void Timer_Handler(object sender, EventArgs e)
        {
            try {
                action_.Invoke(sender, e);
            } catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex.ToString()); }
        }
        #endregion

        public bool IsDisposed { get; private set; } = false;
        public bool IsEnabled => Timer_.Value.IsEnabled;

        public WpfTimer(Action<object,EventArgs> act, Action up = default)
        {
            action_ = act;
            if (up != default)
                onupdate_ = up;
            else
                up = delegate {};
            Timer_ = new(() => TimerInit(), true);
        }
        ~WpfTimer() => Dispose();

        #region Dispose
        public void Dispose()
        {
            if (IsDisposed)
                return;
            IsDisposed = true;

            if (Timer_.IsValueCreated)
            {
                Lazy<DispatcherTimer> dt = Timer_;
                if ((dt != default) && (dt.Value != default))
                    try {
                        dt.Value.IsEnabled = false;
                        dt.Value.Tick -= TimerHandler_;
                    } catch { }
            }
        }
        #endregion

        #region Reload timer
        public void Start()
        {
            if (Timer_.Value.Interval == TimeSpan.Zero)
                return;
            Timer_.Value.Start();
            onupdate_.Invoke();
        }
        public void Start(int minuts)
        {
            if (Timer_.Value.IsEnabled)
                Timer_.Value.Stop();
            Timer_.Value.Interval = TimeSpan.FromMinutes((double)minuts);
            Timer_.Value.Start();
            onupdate_.Invoke();
        }
        public void Stop()
        {
            if (Timer_.Value.IsEnabled)
                Timer_.Value.Stop();
            Timer_.Value.Interval = TimeSpan.Zero;
            onupdate_.Invoke();
        }
        public void Pause()
        {
            Timer_.Value.Stop();
            onupdate_.Invoke();
        }
        #endregion
    }
}
