﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define ENABLE_BACKGROUND_CHECK_UPDATE

using System;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 9),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageTasks : Page, IPlugin
    {
        public int Id { get; set; } = 0;
        public new string Title  => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView   => this;

        private readonly int delayCmd = 150;
        private readonly PageTasksData PageTasksData;

        [ImportingConstructor]
        public PageTasks(IServiceControls icontrols)
        {
            InitializeComponent();
            PageTasksData = (PageTasksData)DataContext;
            PageTasksData.SetServiceCtrl(icontrols);
            this.IsVisibleChanged += PageTasks_IsVisibleChanged;
        }

        private void PageTasks_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
#           if ENABLE_BACKGROUND_CHECK_UPDATE
            if ((PageTasksData != default) && (e != default) && (e.NewValue is bool b))
            {
                if (b && PageTasksData.IsUpdateBackground)
                    PageTasksData.ReloadTimer.Start();
                else
                    PageTasksData.ReloadTimer.Pause();
            }
#           endif
        }

        private void UpdateCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default) && PageTasksData.IsUpdate;
        private async void UpdateCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if (PageTasksData != default)
                await Dispatcher.Invoke(async () =>
                    await PageTasksData.SaveTasksData()
                                       .ConfigureAwait(false)).ConfigureAwait(false);
        }

        private void LoadCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = true;
        private async void LoadCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if (PageTasksData != default)
                await Dispatcher.Invoke(async () =>
                    await PageTasksData.LoadTasksData()
                                       .ConfigureAwait(false)).ConfigureAwait(false);
        }

        private void DueTimeCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default) && PageTasksData.IsSelectedTask;
        private async void DueTimeCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if (PageTasksData != default)
                await Dispatcher.Invoke(async () =>
                    _ = await PageTasksData.TimePickerView(TimePickerDialogContent.ViewSettingsId.DueTime)
                                           .ConfigureAwait(false)).ConfigureAwait(false);
        }

        private void PeriodTimeCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default) && PageTasksData.IsSelectedTask;
        private async void PeriodTimeCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if (PageTasksData != default)
                await Dispatcher.Invoke(async () =>
                    _ = await PageTasksData.TimePickerView(TimePickerDialogContent.ViewSettingsId.PeriodTime)
                                           .ConfigureAwait(false)).ConfigureAwait(false);
        }

        private void ShowErrorCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default) && PageTasksData.IsSelectedTask && PageTasksData.SelectedTask.IsLastError;
        private async void ShowErrorCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if ((PageTasksData != default) && PageTasksData.IsSelectedTask && PageTasksData.SelectedTask.IsLastError)
                Dispatcher.Invoke(() => PageTasksData.SelectedTask.IsShowError = !PageTasksData.SelectedTask.IsShowError);
        }

        private void EnableTaskCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default) && PageTasksData.IsSelectedTask;
        private async void EnableTaskCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await Task.Delay(delayCmd).ConfigureAwait(false);
            if (PageTasksData != default)
                Dispatcher.Invoke(() => PageTasksData.EnableTask());
        }

        private void ReloadTimerCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = (PageTasksData != default);
        private void ReloadTimerCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if ((PageTasksData != default) && (e != default) && (e.Parameter is string s))
                if (int.TryParse(s, out int i))
                {
                    if (i <= 0)
                        PageTasksData.ReloadTimer.Stop();
                    else
                        PageTasksData.ReloadTimer.Start(i);
                }
        }
    }
}
