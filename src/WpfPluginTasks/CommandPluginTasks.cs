﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginTasks
	{
		public static readonly RoutedUICommand LoadCmd = new
			(
				"",
				"LoadCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand UpdateCmd = new
			(
				"",
				"UpdateCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand EnableTaskCmd = new
			(
				"",
				"EnableTaskCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand PeriodTimeCmd = new
			(
				"",
				"PeriodTime",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand DueTimeCmd = new
			(
				"",
				"DueTimeCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand ShowErrorCmd = new
			(
				"",
				"ShowErrorCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand ReloadTimerCmd = new
			(
				"",
				"ReloadTimerCmd",
				typeof(CommandPluginTasks),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
	}
}
