﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using M3U.Model;
using M3U.Parser;
using PlayListServiceData.Base;
using PlayListServiceData.Utils;
using PlayListServiceData.Container;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public partial class PageEditorData : INotifyPropertyChanged
    {
        private CancellationTokenSource tokenSourceCancel = default;
        private readonly DataContainer<BaseSerializableChannelData> XmlDataContainer;
        private MediaPlay MediaPlayer = default;
        private readonly RunOnce onceLoadEpgChannel = new();

        private bool _IsLoaingdEpgData = false,
                     _IsLoaingdM3uData = false,
                     _IsFindAccept = false,
                     _IsInfoExpanded = true,
                     _IsFavEpgAdded = false;
        private readonly ViewSelector<PageEditorData> stateSelector;
        private int _SelectedIndexEpg = -1,
                    _SelectedIndexM3u = -1;
        private string _M3uOpenFile = default,
                       _M3uIcon = default;
        private Action<MediaPlay.PlayerStage, MediaState, Media> actionVideoPlace = delegate { };

        public ObservableCollection<ListViewM3uDataItem> M3uItems { get; set; } = new ObservableCollection<ListViewM3uDataItem>();
        public ObservableCollection<ListViewEpgDataItem> EpgItems { get; set; } = new ObservableCollection<ListViewEpgDataItem>();

        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageEditorData()
        {
            stateSelector = new ViewSelector<PageEditorData>((d) =>
            {
                try { d.flagsNotify(); } catch { }
            }, this);
            tokenSourceCancel = new CancellationTokenSource();
            XmlDataContainer = new DataContainer<BaseSerializableChannelData>(tokenSourceCancel.Token);
        }
        ~PageEditorData()
        {
            TokenDispose();
        }

        public void SetServiceCtrl(IServiceControls controls, VideoPanel panel, Action<MediaPlay.PlayerStage, MediaState, Media> act)
        {
            actionVideoPlace = act;
            ServiceCtrl = controls;
            ServiceCtrl.PropertyChanged += local_PropertyChanged;
            if (panel != null)
            {
                MediaPlayer = panel.MediaPlayer;
                MediaPlayer.PlayerStateHandler += player_StateChanged;
                MediaPlayer.PlayerCmdHandler += cmd_StateChanged;
                panel.PlayerStateHandler += player_StateChanged;
            }
            if (ServiceCtrl.IsControlConfig && !onceLoadEpgChannel.GetOnce())
                ActionStage(TypeActions.LOAD_EPG_BASE);
        }

        internal void flagsNotify()
        {
            /// associated in LeftView & RightView
            OnPropertyChanged(nameof(LeftView));
            OnPropertyChanged(nameof(RightView));
            OnPropertyChanged(nameof(IsRightListView));
            OnPropertyChanged(nameof(IsRightEditView));
            OnPropertyChanged(nameof(IsRightVideoView));
            OnPropertyChanged(nameof(IsLeftVideoView));
            OnPropertyChanged(nameof(IsInfoExpanded));
            OnPropertyChanged(nameof(IsFindEpgActions));
            OnPropertyChanged(nameof(IsAddFavEpgActions));
            OnPropertyChanged(nameof(IsReloadEpgActions));
            OnPropertyChanged(nameof(IsSaveFavEpgActions));
            OnPropertyChanged(nameof(GuiRigthVideoVisible));
            OnPropertyChanged(nameof(GuiRigthEditorVisible));
            OnPropertyChanged(nameof(GuiRigthListViewVisible));
        }

        #region private

        #region Items Actions
        internal bool selectedCheck()
        {
            if ((SelectedIndexM3u < 0) || (SelectedIndexM3u >= M3uItems.Count))
                return false;
            return true;
        }

        public async void ItemsActions<T1, T2, T3>(TypeActions t, T1 obj = default(T1), T2 ele = default(T2), T3 list = default(T3)) where T1 : class where T2 : class where T3 : class
        {
            switch (t)
            {
                case TypeActions.LOAD_M3U:
                    {
                        if ((obj is string s) && (!string.IsNullOrWhiteSpace(s)))
                        {
                            M3uOpenGroup = s;
                            loadM3uList_(s);
                        }
                        break;
                    }
                case TypeActions.SAVE_M3U_AS:
                    {
                        if ((obj is string s) && (!string.IsNullOrWhiteSpace(s)))
                            await eventM3uListSaveCb_(tokenSourceCancel.Token, s);
                        break;
                    }
                case TypeActions.SAVE_EPG_ALIAS:
                    {
                        ActionStage(TypeActions.SAVE_EPG_ALIAS);
                        break;
                    }
                case TypeActions.SAVE_EPG_FAV:
                    {
                        ActionStage(TypeActions.SAVE_EPG_FAV);
                        break;
                    }
                case TypeActions.LOAD_EPG_BASE:
                    {
                        ActionStage(TypeActions.LOAD_EPG_BASE);
                        break;
                    }
                case TypeActions.ADD_EPG_FAV:
                    {
                        if ((SelectedIndexEpg > 0) && (SelectedIndexEpg < EpgItems.Count))
                        {
                            if (!_IsFavEpgAdded)
                                _IsFavEpgAdded = true;
                            EpgItems[SelectedIndexEpg].IfFavorite = !EpgItems[SelectedIndexEpg].IfFavorite;
                            OnPropertyChanged(nameof(IsSaveFavEpgActions));
                        }
                        break;
                    }
                case TypeActions.EDIT_M3U:
                    {
                        if ((obj == null) || !selectedCheck())
                            break;

                        if (obj is EditPanel ed)
                        {
                            if (RightView == TypeElementView.VIEW_RIGHT_EDIT)
                            {
                                RightView = TypeElementView.VIEW_RIGHT_LIST;
                                return;
                            }
                            ed.SetMedia(
                                M3uItems[SelectedIndexM3u],
                                new Action<ListViewM3uDataItem>((a) =>
                                {
                                    RightView = TypeElementView.VIEW_RIGHT_LIST;
                                    if (a == null)
                                        return;
                                    var lvm = (from i in M3uItems
                                               where i.IfMedia.MediaFile.Equals(a.IfMedia.MediaFile)
                                               select i).FirstOrDefault();
                                    if (lvm == null)
                                        M3uItems.Add(a);
                                    else
                                        lvm.Set(a.IfMedia);
                                }));
                            RightView = TypeElementView.VIEW_RIGHT_EDIT;
                        }
                        break;
                    }
                case TypeActions.FIND_M3U:
                case TypeActions.FIND_EPG:
                    {
                        if ((obj == null) || (list == null))
                            return;

                        if (list is ModernWpf.Controls.ListView lv)
                        {
                            if (ele is TextBox tb)
                                await ItemsFind(t, obj, lv, tb);
                            else
                                await ItemsFind(t, obj, lv, null);
                        }
                        break;
                    }
                case TypeActions.FIND_AUTO:
                    {
                        if ((obj == null) || (ele == null) || (list == null) || (SelectedIndexM3u == -1))
                            return;

                        try
                        {
                            if ((list is ModernWpf.Controls.ListView lvm3u) &&
                                (ele is ModernWpf.Controls.ListView lvepg) &&
                                (obj is TextBox tb))
                            {
                                int idx = SelectedIndexM3u + 1;
                                SelectedIndexM3u = (idx >= M3uItems.Count) ? 0 : idx;
                                lvm3u.ScrollIntoView(M3uItems[SelectedIndexM3u]);
                                lvm3u.Focus();
                                await ItemsFind(TypeActions.FIND_EPG, M3uItems[SelectedIndexM3u], lvepg, tb);
                            }

                        }
                        catch { }
                        break;
                    }
                case TypeActions.FIND_ACCEPT:
                    {
                        if ((ele == null) || (list == null))
                            return;

                        try
                        {
                            if ((list is ModernWpf.Controls.ListView lvm3u) &&
                                (ele is ModernWpf.Controls.ListView lvepg) &&
                                (SelectedIndexM3u >= 0) && (SelectedIndexEpg >= 0))
                            {
                                M3uItems[SelectedIndexM3u].IfTag = EpgItems[SelectedIndexEpg].IfTag;
                                SelectedIndexEpg = -1;
                                IsFindAccept = false;
                            }
                        }
                        catch { }
                        break;
                    }
                case TypeActions.CLEAR_M3U:
                    {
                        if (!selectedCheck())
                            return;

                        M3uItems[SelectedIndexM3u].IfTag = default;
                        SelectedIndexM3u = -1;
                        break;
                    }
                case TypeActions.DELETE_M3U:
                    {
                        if (!selectedCheck())
                            return;

                        M3uItems.RemoveAt(SelectedIndexM3u);
                        SelectedIndexM3u = -1;
                        break;
                    }
                case TypeActions.ASSIGN_ID:
                    {
                        if (!selectedCheck())
                            return;

                        if (obj is string s)
                            M3uItems[SelectedIndexM3u].IfTag = s;
                        if (obj is ListViewEpgDataItem lve)
                            M3uItems[SelectedIndexM3u].IfTag = lve.IfTag;
                        break;
                    }
                case TypeActions.STOP_M3U:
                    {
                        if (obj is MediaPlay mp)
                            mp.Stop();
                        break;
                    }
                case TypeActions.PLAY_M3U:
                    {
                        if (obj is MediaPlay mp)
                        {
                            if ((mp.PlayerStatus == MediaState.Play) ||
                                (mp.PlayerStatus == MediaState.Pause))
                            {
                                mp.PlayStop();
                                break;
                            }

                            if ((mp.PlayerStatus != MediaState.Manual) &&
                                (mp.PlayerStatus != MediaState.Close) &&
                                (mp.PlayerStatus != MediaState.Stop))
                                break;

                            if (ele is Media m)
                            {
                                mp.PlayStop(m);
                                break;
                            }

                            if (!selectedCheck())
                                break;

                            mp.PlayStop(M3uItems[SelectedIndexM3u].IfMedia);
                        }
                        break;
                    }
                case TypeActions.PAUSE_M3U:
                    {
                        if ((obj is MediaPlay mp) &&
                            ((mp.PlayerStatus == MediaState.Play) || (mp.PlayerStatus == MediaState.Pause)))
                            mp.Pause();
                        break;
                    }
                case TypeActions.CAPTURE_IMG:
                    {
                        if (!selectedCheck())
                            break;
                        try
                        {
                            if (obj is MediaPlay mp)
                            {
                                if ((mp.PlayerStatus == MediaState.Manual) ||
                                    (mp.PlayerStatus == MediaState.Stop) ||
                                    (mp.PlayerStatus == MediaState.Close))
                                    break;

                                Media m = M3uItems[SelectedIndexM3u].IfMedia;
                                if (m == null)
                                    break;

                                string name = m.IconNameFromM3uMedia(
                                    Path.GetFileNameWithoutExtension(M3uOpenGroup),
                                    ServiceCtrl.IconSaveExtension,
                                    SelectedIndexM3u);

                                if (mp.Capture(
                                         ServiceCtrl.ControlConfig.ConfigM3uPath.IconsPathFromDirectoryString(),
                                         name,
                                         ServiceCtrl.IconSaveExtension))
                                {
                                    m.Attributes.SetTvgLogo(
                                        name.IconUrlFromString(ServiceCtrl.ControlConfig.ConfigNetAddr, ServiceCtrl.ControlConfig.ConfigNetPort));
                                    M3uIcon = m.Attributes.TvgLogo;
                                }
                            }
                        }
                        catch { }
                        break;
                    }
                case TypeActions.ADD_ICON:
                    {
                        if (!selectedCheck())
                            break;
                        try
                        {
                            if (obj is string str)
                            {
                                if (string.IsNullOrWhiteSpace(str))
                                    break;

                                Media m = M3uItems[SelectedIndexM3u].IfMedia;
                                FileInfo f = new(str);
                                if ((f == null) || (!f.Exists))
                                    break;

                                string name = m.IconNameFromM3uMedia(
                                    Path.GetFileNameWithoutExtension(M3uOpenGroup),
                                    Path.GetExtension(str),
                                    SelectedIndexM3u);
                                /// TO-DO: !!!!!!
                                string path = Path.Combine(ServiceCtrl.ControlConfig.ConfigM3uPath, "icon", name);
                                f.CopyTo(path, true);

                                m.Attributes.SetTvgLogo(
                                    name.IconUrlFromString(ServiceCtrl.ControlConfig.ConfigNetAddr, ServiceCtrl.ControlConfig.ConfigNetPort));
                                M3uIcon = m.Attributes.TvgLogo;
                            }
                        }
                        catch { }
                        break;
                    }
            }
        }

        #endregion

        #region Items Find

        private async Task ItemsFind<T>(TypeActions t, T obj, ModernWpf.Controls.ListView lv, TextBox tb) where T : class
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        switch (t)
                        {
                            case TypeActions.FIND_M3U:
                                {
                                    if (obj is string s)
                                    {
                                        for (int i = 0; i < M3uItems.Count; i++)
                                            if (((!string.IsNullOrWhiteSpace(M3uItems[i].IfChannelName)) && M3uItems[i].IfChannelName.StartsWith(s, StringComparison.OrdinalIgnoreCase)) ||
                                                ((!string.IsNullOrWhiteSpace(M3uItems[i].IfTag)) && M3uItems[i].IfTag.StartsWith(s, StringComparison.OrdinalIgnoreCase)))
                                            {
                                                Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    SelectedIndexM3u = i;
                                                    if (lv != null)
                                                    {
                                                        lv.ScrollIntoView(M3uItems[i]);
                                                        lv.Focus();
                                                    }
                                                });
                                                break;
                                            }
                                    }
                                    else if (obj is ListViewEpgDataItem lve)
                                    {
                                        bool isName = !string.IsNullOrWhiteSpace(lve.IfChannelName),
                                             isTag = !string.IsNullOrWhiteSpace(lve.IfTag);

                                        for (int i = 0; i < M3uItems.Count; i++)
                                        {
                                            if (!string.IsNullOrWhiteSpace(M3uItems[i].IfChannelName))
                                            {
                                                string str = default;
                                                if (isName && M3uItems[i].IfChannelName.StartsWith(lve.IfChannelName, StringComparison.OrdinalIgnoreCase))
                                                    str = lve.IfChannelName;
                                                else if (isTag && M3uItems[i].IfChannelName.StartsWith(lve.IfTag, StringComparison.OrdinalIgnoreCase))
                                                    str = lve.IfTag;

                                                if (str != null)
                                                {
                                                    Application.Current.Dispatcher.Invoke(() =>
                                                    {
                                                        SelectedIndexM3u = i;
                                                        if (lv != null)
                                                        {
                                                            lv.ScrollIntoView(M3uItems[i]);
                                                            lv.Focus();
                                                        }
                                                        if (tb != null)
                                                            tb.Text = str;
                                                    });
                                                    return;
                                                }
                                            }
                                            if (!string.IsNullOrWhiteSpace(M3uItems[i].IfTag))
                                            {
                                                if (isTag && M3uItems[i].IfTag.StartsWith(lve.IfTag, StringComparison.OrdinalIgnoreCase))
                                                {
                                                    Application.Current.Dispatcher.Invoke(() =>
                                                    {
                                                        SelectedIndexM3u = i;
                                                        if (lv != null)
                                                        {
                                                            lv.ScrollIntoView(M3uItems[i]);
                                                            lv.Focus();
                                                        }
                                                        if (tb != null)
                                                            tb.Text = lve.IfTag;
                                                    });
                                                    return;
                                                }
                                            }
                                        }
                                        if (isTag)
                                        {
                                            string[] arr = lve.IfTag.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (arr.Length > 0)
                                                for (int i = 0; i < M3uItems.Count; i++)
                                                    if ((!string.IsNullOrWhiteSpace(M3uItems[i].IfChannelName)) &&
                                                         M3uItems[i].IfChannelName.StartsWith(arr[0], StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        Application.Current.Dispatcher.Invoke(() =>
                                                        {
                                                            SelectedIndexM3u = i;
                                                            if (lv != null)
                                                            {
                                                                lv.ScrollIntoView(M3uItems[i]);
                                                                lv.Focus();
                                                            }
                                                            if (tb != null)
                                                                tb.Text = arr[0];
                                                        });
                                                        return;
                                                    }
                                        }
                                        if (isName)
                                        {
                                            string[] arr = lve.IfChannelName.Split(new char[] { '-', '_', ' ', '.', ',', '/', '\\', '"' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (arr.Length > 0)
                                                for (int i = 0; i < M3uItems.Count; i++)
                                                    if ((!string.IsNullOrWhiteSpace(M3uItems[i].IfChannelName)) &&
                                                         M3uItems[i].IfChannelName.StartsWith(arr[0], StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        Application.Current.Dispatcher.Invoke(() =>
                                                        {
                                                            SelectedIndexM3u = i;
                                                            if (lv != null)
                                                            {
                                                                lv.ScrollIntoView(M3uItems[i]);
                                                                lv.Focus();
                                                            }
                                                            if (tb != null)
                                                                tb.Text = arr[0];
                                                        });
                                                        return;
                                                    }
                                        }
                                        if (tb != null)
                                            Application.Current.Dispatcher.Invoke(() =>
                                            {
                                                SelectedIndexM3u = -1;
                                                IsFindAccept = false;
                                                tb.Text = (isTag) ? lve.IfTag :
                                                          (isName) ? lve.IfChannelName : default;
                                            });
                                    }
                                    break;
                                }
                            case TypeActions.FIND_EPG:
                                {
                                    if (obj is string s)
                                    {
                                        for (int i = 0; i < EpgItems.Count; i++)
                                        {
                                            if (((!string.IsNullOrWhiteSpace(EpgItems[i].IfChannelName)) && EpgItems[i].IfChannelName.StartsWith(s, StringComparison.OrdinalIgnoreCase)) ||
                                                ((!string.IsNullOrWhiteSpace(EpgItems[i].IfTag)) && EpgItems[i].IfTag.StartsWith(s, StringComparison.OrdinalIgnoreCase)))
                                            {
                                                Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    SelectedIndexEpg = i;
                                                    if (lv != null)
                                                    {
                                                        lv.ScrollIntoView(EpgItems[i]);
                                                        lv.Focus();
                                                        IsFindAccept = true;
                                                    }
                                                });
                                                break;
                                            }
                                        }
                                    }
                                    else if (obj is ListViewM3uDataItem lvm)
                                    {
                                        bool isName = !string.IsNullOrWhiteSpace(lvm.IfChannelName),
                                             isId = !string.IsNullOrWhiteSpace(lvm.IfTag);

                                        for (int i = 0; i < EpgItems.Count; i++)
                                        {
                                            if (isId &&
                                                (!string.IsNullOrWhiteSpace(EpgItems[i].IfTag)) &&
                                                EpgItems[i].IfTag.StartsWith(lvm.IfTag, StringComparison.OrdinalIgnoreCase))
                                            {
                                                Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    SelectedIndexEpg = i;
                                                    if (lv != null)
                                                    {
                                                        lv.ScrollIntoView(EpgItems[i]);
                                                        lv.Focus();
                                                        IsFindAccept = true;
                                                    }
                                                    if (tb != null)
                                                        tb.Text = lvm.IfTag;
                                                });
                                                return;
                                            }
                                            else if (isName &&
                                                (!string.IsNullOrWhiteSpace(EpgItems[i].IfChannelName)) &&
                                                EpgItems[i].IfChannelName.StartsWith(lvm.IfChannelName, StringComparison.OrdinalIgnoreCase))
                                            {
                                                Application.Current.Dispatcher.Invoke(() =>
                                                {
                                                    SelectedIndexEpg = i;
                                                    if (lv != null)
                                                    {
                                                        lv.ScrollIntoView(EpgItems[i]);
                                                        lv.Focus();
                                                        IsFindAccept = true;
                                                    }
                                                    if (tb != null)
                                                        tb.Text = lvm.IfChannelName;
                                                });
                                                return;
                                            }
                                        }
                                        if (isName)
                                        {
                                            string[] arr = lvm.IfChannelName.Split(new char[] { '-', '_', ' ', '.', ',', '/', '\\', '"' }, StringSplitOptions.RemoveEmptyEntries);
                                            if (arr.Length > 0)
                                                for (int i = 0; i < EpgItems.Count; i++)
                                                    if ((!string.IsNullOrWhiteSpace(EpgItems[i].IfChannelName)) &&
                                                         EpgItems[i].IfChannelName.StartsWith(arr[0], StringComparison.OrdinalIgnoreCase))
                                                    {
                                                        Application.Current.Dispatcher.Invoke(() =>
                                                        {
                                                            SelectedIndexEpg = i;
                                                            if (lv != null)
                                                            {
                                                                lv.ScrollIntoView(EpgItems[i]);
                                                                lv.Focus();
                                                                IsFindAccept = true;
                                                            }
                                                            if (tb != null)
                                                                tb.Text = arr[0];
                                                        });
                                                        return;
                                                    }
                                        }
                                        if (tb != null)
                                            Application.Current.Dispatcher.Invoke(() =>
                                            {
                                                SelectedIndexEpg = -1;
                                                IsFindAccept = false;
                                                tb.Text = (isName) ? lvm.IfChannelName :
                                                          (isId) ? lvm.IfTag : default;
                                            });
                                    }
                                    break;
                                }
                        }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
#else
                    catch { }
#endif

                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
#else
            catch { }
#endif
        }

        #endregion

        #endregion

        #region Cancel Token
        private void TokenDispose()
        {
            if (tokenSourceCancel != null)
            {
                tokenSourceCancel.Cancel();
                tokenSourceCancel.Dispose();
                tokenSourceCancel = default;
            }
        }
        #endregion

        #region event control
        private void local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ControlConfig":
                    {
                        if ((ServiceCtrl.ControlConfig == null) || (!ServiceCtrl.ControlConfig.IsLoad))
                            return;

                        if ((tokenSourceCancel != null) && tokenSourceCancel.IsCancellationRequested)
                            return;

                        if (!onceLoadEpgChannel.GetOnce())
                            ActionStage(TypeActions.LOAD_EPG_BASE);
                        break;
                    }
            }
        }

        private void cmd_StateChanged(MediaPlay mp, MediaState s, MediaState c, Media m)
        {
            switch (s)
            {
                case MediaState.Manual:
                    {
                        break;
                    }
                case MediaState.Close:
                    {
                        if ((c == MediaState.Play) ||
                            (c == MediaState.Pause))
                            ItemsActions(TypeActions.CAPTURE_IMG, mp, default(string), default(string));
                        break;
                    }
                case MediaState.Pause:
                    {
                        if ((c == MediaState.Play) || (c == MediaState.Pause))
                            ItemsActions(TypeActions.PAUSE_M3U, mp, default(string), default(string));
                        break;
                    }
                case MediaState.Play:
                    {
                        if ((c == MediaState.Stop) ||
                            (c == MediaState.Close) ||
                            (c == MediaState.Pause) ||
                            (c == MediaState.Manual))
                            ItemsActions(TypeActions.PLAY_M3U, mp, default(string), default(string));
                        break;
                    }
                case MediaState.Stop:
                    {
                        if ((c == MediaState.Play) ||
                            (c == MediaState.Pause))
                            ItemsActions(TypeActions.STOP_M3U, mp, default(string), default(string));
                        break;
                    }
            }
        }

        private void player_StateChanged(MediaState state, MediaPlay.PlayerStage stage, object obj)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"\t- Player event:\t{state}, {stage}");
            System.Diagnostics.Debug.WriteLine($"\t- Flags:\t{IsRightVideoView}, {IsRightEditView}, {IsRightListView}, {IsLeftVideoView}");
#endif
            switch (state)
            {
                case MediaState.Close:
                    {
                        /* this empty event id */
                        break;
                    }
                case MediaState.Manual:
                    {
                        stateSelector.End();
                        break;
                    }
                case MediaState.Play:
                    {
                        stateSelector.Begin();
                        stateSelector.SetVideoPlace();
                        break;
                    }
                case MediaState.Pause:
                    {
                        break;
                    }
                case MediaState.Stop:
                    {
                        stateSelector.End();
                        break;
                    }
            }

            OnPropertyChanged(nameof(PlayStatus));

            switch (stage)
            {
                case MediaPlay.PlayerStage.NONE:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.VIDEO_LEFT:
                case MediaPlay.PlayerStage.VIDEO_RIGHT:
                    {
                        actionVideoPlace(stage, MediaPlayer.PlayerStatus, MediaSelected);
                        stateSelector.SetVideoPlace((stage == MediaPlay.PlayerStage.VIDEO_LEFT) ?
                            TypeElementView.VIEW_LEFT_VIDEO : TypeElementView.VIEW_RIGHT_VIDEO);
                        break;
                    }
                case MediaPlay.PlayerStage.ADD_MEDIA:
                    {
                        IsInfoExpanded = false;
                        break;
                    }
                case MediaPlay.PlayerStage.BUFFERED:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.BUFFERED_END:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.CMD_ERROR:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.PLAYS:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.WAIT:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.END_PLAY:
                    {
                        stateSelector.End();
                        break;
                    }
            }
        }
        #endregion

    }
}
