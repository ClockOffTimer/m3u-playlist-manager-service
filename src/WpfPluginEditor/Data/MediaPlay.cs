﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using M3U.Model;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class MediaPlay
    {
        public static readonly string ProxyBinExe = $"ffmpeg.exe";
        public static readonly string ProxyPlayUrl = "http://127.0.0.1:11921/a.mp4";
        public static readonly string ProxyM3uFormatUrl = $@"-hide_banner -re -i ""{{0}}"" -codec copy -bsf:v h264_mp4toannexb -f mpegts -listen 1 ""{ProxyPlayUrl}""";

        public enum PlayerStage : int
        {
            NONE = 0,
            WAIT,
            BUFFERED,
            BUFFERED_END,
            CMD_ERROR,
            PLAYS,
            ADD_MEDIA,
            END_PLAY,
            VIDEO_LEFT,
            VIDEO_RIGHT,
            PLAY_NAME,
            CAP_ORIGINAL_SIZE,
            CAP_AREA_TOP_LEFT,
            CAP_AREA_TOP_RIGHT,
            CAP_AREA_BOTTOM_LEFT,
            CAP_AREA_BOTTOM_RIGHT
        };

        private readonly int waiterConstant = 10000;

        private PlayerStage _PanelVideoPlace = PlayerStage.VIDEO_RIGHT;
        public PlayerStage PanelVideoPlace
        {
            get { return _PanelVideoPlace; }
            private set
            {
                if (_PanelVideoPlace == value)
                    return;
                _PanelVideoPlace = value;
                PlayerStateHandler.Invoke(MediaState.Close, _PanelVideoPlace, null);
            }
        }

        private PlayerStage _CaptureArea = PlayerStage.CAP_ORIGINAL_SIZE;
        public PlayerStage CaptureArea
        {
            get { return _CaptureArea; }
            set
            {
                if (_CaptureArea == value)
                    return;
                _CaptureArea = value;
            }
        }

        private readonly MediaElement _Player;
        private MediaState _PlayerStatus = MediaState.Close;
        public MediaState PlayerStatus
        {
            get { return _PlayerStatus; }
            private set
            {
                if (_PlayerStatus == value)
                    return;
                _PlayerStatus = value;
                PlayerStateHandler.Invoke(_PlayerStatus, PlayerStage.NONE, null);
            }
        }
        private Media _PlayMedia = default;
        public Media PlayMedia
        {
            get { return _PlayMedia; }
            private set
            {
                if (_PlayMedia == value)
                    return;
                _PlayMedia = value;
                PlayerStateHandler.Invoke(MediaState.Close, PlayerStage.ADD_MEDIA, null);
            }
        }

        public delegate void PlayerErrorEvent(Exception e);
        public event PlayerErrorEvent PlayerErrorHandler = delegate { };
        public delegate void PlayerStateEvent(MediaState m, PlayerStage s, object obj);
        public event PlayerStateEvent PlayerStateHandler = delegate { };
        public delegate void PlayerCmdEvent(MediaPlay t, MediaState s, MediaState c, Media m);
        public event PlayerCmdEvent PlayerCmdHandler = delegate { };
        public void SendCommand(MediaState s, MediaState c)
        {
            PlayerCmdHandler.Invoke(this, s, c, PlayMedia);
        }

        private MediaPlayProxy _MediaProxy = default;
        private readonly Action<string[], Exception> _MediaProxyLog;

        public MediaPlay(MediaElement m, Action<string[], Exception> e)
        {
            _Player = m;
            _MediaProxyLog = e;
            playerInit();
        }
        ~MediaPlay()
        {
            if (_MediaProxy != default)
            {
                _MediaProxy.PropertyChanged -= local_propertyChanged;
                _MediaProxy.PlayerErrorLogHandler -= local_eventErrorLog;
            }
        }

        #region public

        public void SetServiceCtrl(IServiceControls control)
        {
            _MediaProxy = new MediaPlayProxy(control);
            _MediaProxy.PropertyChanged += local_propertyChanged;
            _MediaProxy.PlayerErrorLogHandler += local_eventErrorLog;
        }
        public void Stop() => playerStop();
        public void Pause() => playerPause();
        public void PlayStop(Media m = null) => playerStopPlay(m);
        public void SetVolume(double d) => _Player.Volume = d;
        public bool Capture(string path, string name, string ext) { return playerScreenShot(path, name, ext); }
        public void SetEmpty() { PlayMedia = default; }

        #endregion

        #region Internal

        #region playerInit

        private void playerInit()
        {
            _Player.Volume = 0.2;
            _Player.MediaOpened += (s, a) =>
            {
                PlayerStatus = MediaState.Play;
            };
            _Player.MediaEnded += (s, a) =>
            {
                playerStop();
            };
            _Player.MediaFailed += (s, a) =>
            {
                if ((a != null) && (a.ErrorException != null))
                    PlayerErrorHandler.Invoke(a.ErrorException);
                playerStop();
            };
            _Player.Unloaded += (s, a) =>
            {
                playerStop();
            };
            _Player.BufferingStarted += (s, a) =>
            {
                PlayerErrorHandler.Invoke(null);
                PlayerStateHandler.Invoke(MediaState.Close, PlayerStage.BUFFERED, null);
            };
            _Player.BufferingEnded += (s, a) =>
            {
                PlayerStateHandler.Invoke(MediaState.Close, PlayerStage.BUFFERED_END, null);
            };

            _Player.MouseRightButtonUp += (s, a) => { playerPause(); };
            /* GUI move element left <-> right panel */
            _Player.MouseLeftButtonUp += (s, a) =>
            {
                if (PanelVideoPlace == PlayerStage.VIDEO_RIGHT)
                {
                    playerPause();
                    return;
                }
                PanelVideoPlace = PlayerStage.VIDEO_LEFT;
                playerStop();
                if (_MediaProxy != default)
                    _MediaProxy.Stop();
            };
        }

        #endregion

        #region playerStop

        private void playerStop()
        {
            try
            {
                _Player.Dispatcher.Invoke(() =>
                {
                    _Player.Stop();
                    _Player.Source = default;
                    _Player.Close();
                });
                PlayerStatus = MediaState.Stop;
            }
            catch (Exception e) { PlayerErrorHandler.Invoke(e); }
            PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.END_PLAY, null);
        }

        #endregion

        #region playerPause

        private void playerPause()
        {
            try
            {
                MediaState m = MediaState.Close;

                if ((PlayerStatus == MediaState.Play) && _Player.CanPause)
                {
                    _Player.Dispatcher.Invoke(() => _Player.Pause());
                    PlayerStatus = m = MediaState.Pause;
                }
                else if (PlayerStatus == MediaState.Pause)
                {
                    _Player.Dispatcher.Invoke(() => _Player.Play());
                    PlayerStatus = m = MediaState.Play;
                }
                PlayerStateHandler.Invoke(m, PlayerStage.PLAYS, null);
            }
            catch (Exception e) { PlayerErrorHandler.Invoke(e); }
        }

        #endregion

        #region playerStopPlay

        private async void playerStopPlay(Media m)
        {
            try
            {
                if (PlayerStatus == MediaState.Play)
                {
                    playerStop();
                }
                else if (PlayerStatus == MediaState.Pause)
                {
                    playerPause();
                }
                else if ((m != null) &&
                         ((PlayerStatus == MediaState.Stop) ||
                          (PlayerStatus == MediaState.Close) ||
                          (PlayerStatus == MediaState.Manual)))
                {
                    int waiter = 0;
                    Uri uri = default;
                    do
                    {
                        if (string.IsNullOrWhiteSpace(m.MediaFile))
                            break;

                        Uri u = new Uri(m.MediaFile, UriKind.Absolute);
                        bool b = u.Scheme.Equals("http", StringComparison.OrdinalIgnoreCase);

                        if (!b)
                        {
                            if (_MediaProxy == default)
                                break;
                            if (!_MediaProxy.IsPlayableM3uUrl(u))
                                break;
                            if (!_MediaProxy.CheckAndStart(m))
                                break;

                            waiter = waiterConstant;
                            uri = new Uri(MediaPlay.ProxyPlayUrl, UriKind.Absolute);
                            break;
                        }
                        else if (b)
                        {
                            if (_MediaProxy != default)
                            {
                                if (_MediaProxy.IsPlayableDirectUrl(u))
                                {
                                    uri = u;
                                    break;
                                }
                                else if (_MediaProxy.IsPlayableM3uUrl(u))
                                {
                                    if (!_MediaProxy.CheckAndStart(m))
                                        break;

                                    waiter = waiterConstant;
                                    uri = new Uri(MediaPlay.ProxyPlayUrl, UriKind.Absolute);
                                    break;
                                }
                                else
                                    break;
                            }
                            else
                            {
                                uri = u;
                                break;
                            }
                        }
                    } while (false);

                    if (uri == default)
                        return;

                    if (m.Attributes.GroupTitle != null)
                    {
                        if (m.Attributes.ChannelNumber != null)
                            PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.PLAY_NAME, $"{m.Attributes.GroupTitle}_{m.Attributes.ChannelNumber}_{m.Title.InnerTitle}");
                        else
                            PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.PLAY_NAME, $"{m.Attributes.GroupTitle}_{m.Title.InnerTitle}");
                    }
                    else
                        PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.PLAY_NAME, $"{m.Title.InnerTitle}");

                    PlayMedia = m;
                    PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.PLAY_NAME, null);
                    await WaitService(waiter);
                    _Player.Dispatcher.Invoke(() =>
                    {
                        _Player.Source = uri;
                        _Player.Play();
                    });
                    PlayerStatus = MediaState.Play;
                    PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.PLAYS, null);
                }
                else if (m != null)
                {
                    PlayMedia = default;
                    PlayerStatus = MediaState.Close;
                    PlayerStateHandler.Invoke(PlayerStatus, PlayerStage.CMD_ERROR, null);
                    playerStop();
                }
            }
            catch (Exception e) { PlayerErrorHandler.Invoke(e); }
        }

        #endregion

        #region playerScreenShot

        private bool playerScreenShot(string path, string name, string ext)
        {
            if ((PlayerStatus == MediaState.Manual) ||
                (PlayerStatus == MediaState.Close) ||
                (PlayerStatus == MediaState.Stop))
                return false;

            try
            {
                BitmapEncoder encoder = default;
                if (ext.EndsWith("png"))
                    encoder = new PngBitmapEncoder();
                else
                    encoder = new JpegBitmapEncoder();

                int w = (int)_Player.ActualWidth,
                    h = (int)_Player.ActualHeight,
                    iw = 132,
                    ih = 100;
                Size dpi = new Size(96, 96);
                Int32Rect image = default;

                PlayerStage ps = CaptureArea;
                if (PanelVideoPlace == PlayerStage.VIDEO_LEFT)
                    ps = PlayerStage.NONE;

                switch (ps)
                {
                    case PlayerStage.NONE:
                        {
                            break;
                        }
                    case PlayerStage.CAP_ORIGINAL_SIZE:
                        {
                            image = new Int32Rect(0, 0, w, h);
                            break;
                        }
                    case PlayerStage.CAP_AREA_TOP_LEFT:
                        {
                            image = new Int32Rect(0, 0, iw, ih);
                            break;
                        }
                    case PlayerStage.CAP_AREA_TOP_RIGHT:
                        {
                            image = new Int32Rect(w - iw, 0, iw, ih);
                            break;
                        }
                    case PlayerStage.CAP_AREA_BOTTOM_LEFT:
                        {
                            image = new Int32Rect(0, h - ih, iw, ih);
                            break;
                        }
                    case PlayerStage.CAP_AREA_BOTTOM_RIGHT:
                        {
                            image = new Int32Rect(w - iw, h - ih, iw, ih);
                            break;
                        }
                    default: return false;
                }

                RenderTargetBitmap bmp =
                  new RenderTargetBitmap(w, h, dpi.Width, dpi.Height, PixelFormats.Pbgra32);

                _Player.Dispatcher.Invoke(() => bmp.Render(_Player));

                if (PanelVideoPlace == PlayerStage.VIDEO_RIGHT)
                    encoder.Frames.Add(BitmapFrame.Create(new CroppedBitmap(bmp, image)));
                else
                    encoder.Frames.Add(BitmapFrame.Create(bmp));

                DirectoryInfo dir;
                if ((dir = new DirectoryInfo(path)) == null)
                    return false;
                if (!dir.Exists)
                    dir.Create();

                using (FileStream fs = new FileStream(Path.Combine(path, name), FileMode.Create))
                {
                    encoder.Save(fs);
                    fs.Close();
                }
                return true;
            }
            catch (Exception e) { PlayerErrorHandler.Invoke(e); }
            return false;
        }

        #endregion

        #region WaitService

        private async Task WaitService(int x)
        {
            if (x == 0)
                return;

            await Task.Run(() =>
            {
                int cnt = x;
                while (cnt-- <= 0)
                {
                    Task.Delay(750);
                    Task.Yield();
                }
            });
        }

        #endregion

        #endregion

        #region event control
        private void local_propertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("IsProxyRun"))
            {
                if (!_MediaProxy.IsProxyRun)
                    playerStop();
            }
        }

        private void local_eventErrorLog(string[] s, Exception e)
        {
            _MediaProxyLog(s, e);
        }

        #endregion
    }
}
