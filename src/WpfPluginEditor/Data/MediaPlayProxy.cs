﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using M3U.Model;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Run;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class MediaPlayProxy : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public delegate void PlayerErrorLogEvent(string[] s, Exception e);
        public event PlayerErrorLogEvent PlayerErrorLogHandler = delegate { };
        protected void OnPlayerErrorLogChanged(string[] s)
        {
            PlayerErrorLogHandler?.Invoke(s, null);
        }
        protected void OnPlayerErrorLogChanged(Exception e)
        {
            PlayerErrorLogHandler?.Invoke(null, e);
        }

        private readonly object __lock = new();
        private IServiceControls ServiceCtrl { get; set; } = default;
        private CancellationTokenSource cancellToken = default;
        private Task<ProcessResults> processResults = default;

        public MediaPlayProxy(IServiceControls controls)
        {
            ServiceCtrl = controls;
            string path = getFfmpegPath();
            FileInfo f = new(path);
            if ((f != default) && f.Exists)
                IsFFExeFound = true;
        }
        ~MediaPlayProxy()
        {
            Stop();
        }

        public bool IsProxyRun
        {
            get
            {
                return (processResults != null) &&
                       (processResults.Status != TaskStatus.RanToCompletion) &&
                       (processResults.Status != TaskStatus.Canceled) &&
                       (processResults.Status != TaskStatus.Faulted);
            }
        }

        public bool _IsFFExeFound = default;
        public bool IsFFExeFound
        {
            get { return _IsFFExeFound; }
            set
            {
                if (_IsFFExeFound == value)
                    return;
                _IsFFExeFound = value;
            }
        }

        public bool IsPlayableDirectUrl(Uri u)
        {
            if (!u.Scheme.Equals("http", StringComparison.OrdinalIgnoreCase))
                return false;

            if ((!u.IsDefaultPort) && u.PathAndQuery.Equals("/"))
                return true;

            if (u.LocalPath.Equals("/"))
                return false;
            return u.AbsoluteUri.IsPlayableMediaUrl();
        }

        public bool IsPlayableM3uUrl(Uri u)
        {
            return u.IsPlayableM3uUrl();
        }

        public void Start(Media m)
        {
            try
            {
                ProcessStartInfo psi = new()
                {
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    FileName = getFfmpegPath(),
                    Arguments = string.Format(MediaPlay.ProxyM3uFormatUrl, m.MediaFile)
                };

                Stop();

                lock (__lock)
                {
                    cancellToken = new CancellationTokenSource();
                    processResults = ProcessEx.RunAsync(psi, cancellToken.Token);
                    OnPropertyChanged(nameof(IsProxyRun));
                }
            }
            catch (Exception e) { OnPlayerErrorLogChanged(e); Debug.WriteLine(e.Message); }
        }

        public void Stop()
        {
            lock (__lock)
            {
                try
                {
                    CancellationTokenSource ct = cancellToken;
                    Task<ProcessResults> pr = processResults;
                    cancellToken = default;
                    processResults = default;

                    if (pr != default)
                    {
                        try
                        {
                            if (ct != default)
                                ct.Cancel();
                            pr.Wait(TimeSpan.FromMilliseconds(10000));
                            if (pr.Result != null)
                                OnPlayerErrorLogChanged(pr.Result.StandardError);
                            try { pr.Dispose(); } catch { }
                        }
                        catch (Exception e) { OnPlayerErrorLogChanged(e); Debug.WriteLine(e.Message); }
                    }
                    if (ct != default)
                    {
                        try { ct.Dispose(); } catch { }
                    }
                }
                catch { }
            }
            OnPropertyChanged(nameof(IsProxyRun));
        }

        public bool CheckAndStart(Media m)
        {
            do
            {
                if (!IsFFExeFound)
                    break;
                Start(m);
                return true;

            } while (false);
            return false;
        }

        private string getFfmpegPath()
        {
            return Path.Combine(
                Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                ServiceCtrl.FFMpegPath,
                $"x{ServiceCtrl.IsPlatformRun}",
                MediaPlay.ProxyBinExe);
        }
    }
}
