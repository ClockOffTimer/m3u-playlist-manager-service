﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using M3U.Model;
using PlayListServiceData.Base;
using PlayListServiceData.Utils;
using PlayListServiceData.Container;
using PlayListServiceData.Container.Builder;

namespace WpfPlugin.Data
{
    public partial class PageEditorData : INotifyPropertyChanged
    {
        #region Stage load (Epg, Favorites) data callback

        private readonly RunOnce onceEpgAlias = new RunOnce();
        private readonly RunOnce onceEpgBase = new RunOnce();
        private readonly RunOnce onceEpgFav = new RunOnce();

        /* - LOAD EPG Base - */
        #region - EPG Base LOAD callback = eventEpgBaseLoadCb_ (call loadEpgBaseAsync_)
        private void eventEpgBaseLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceEpgBase.GetOnce())
                            return;
                        loadEpgBaseParser_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }

        #endregion

        /* - LOAD EPG Alias - */
        #region - EPG Alias LOAD callback = eventEpgAliasLoadCb_ (call loadEpgAliasAsync_)
        private void eventEpgAliasLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceEpgAlias.GetOnce())
                            return;
                        loadEpgAliasParse_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        /* - LOAD EPG Favorites - */
        #region - EPG Favorites LOAD callback = eventEpgFavLoadCb_ (call loadedEpgFavAsync_)
        private void eventEpgFavLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceEpgFav.GetOnce())
                            return;
                        loadEpgFavParse_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        /* - SAVE EPG Alias - */
        #region - EPG Alias SAVE callback = eventEpgAliasSaveCb_
        private void eventEpgAliasSaveCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.SaveBegin:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.SaveOk:
                    {
                        break;
                    }
                case ConstContainer.State.DataReady:
                case ConstContainer.State.SaveEnd:
                case ConstContainer.State.SaveError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        /* - SAVE EPG Favorites - */
        #region - EPG Favorites SAVE callback = eventEpgFavSaveCb_
        private void eventEpgFavSaveCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.SaveBegin:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.SaveOk:
                    {
                        break;
                    }
                case ConstContainer.State.DataReady:
                case ConstContainer.State.SaveEnd:
                case ConstContainer.State.SaveError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        #endregion

        /* PREPARE AND RUN */
        #region Stage action, pre start
        private void ActionStage(TypeActions t)
        {
            if (string.IsNullOrEmpty(ServiceCtrl.ControlConfig.ConfigM3uPath))
                return;

            onceEpgFav.Reset();
            onceEpgBase.Reset();
            onceEpgAlias.Reset();

            switch (t)
            {
                case TypeActions.LOAD_EPG_BASE:
                case TypeActions.LOAD_EPG_ALIAS:
                case TypeActions.LOAD_EPG_FAV:
                    {
                        if (!IsLoadingEpgData)
                            IsLoadingEpgData = true;

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            OnPropertyChanged(nameof(IsFindEpgActions));
                            OnPropertyChanged(nameof(IsAddFavEpgActions));
                            OnPropertyChanged(nameof(IsSaveFavEpgActions));
                        });
                        break;
                    }
            }
            ActionStageRun(t);
        }
        private void ActionStageRun(TypeActions t)
        {
            try
            {
                switch (t)
                {
                    case TypeActions.SAVE_EPG_ALIAS:
                        {
                            M3uItemSaveBuilder builder = new M3uItemSaveBuilder(M3uItems, builderEpgAliasSaveCb_);
                            _ = XmlDataContainer.OpenAndWrite(
                                BasePath.GetEpgAliasPath(),
                                builder,
                                eventEpgAliasSaveCb_,
                                ConstContainer.Actions.SAVE_EPG_ALIAS);
                            break;
                        }
                    case TypeActions.SAVE_EPG_FAV:
                        {
                            EPGItemSaveBuilder builder = new EPGItemSaveBuilder(EpgItems, builderEpgFavSaveCb_);
                            _ = XmlDataContainer.OpenAndWrite(
                                BasePath.GetEpgFavoritePath(),
                                builder,
                                eventEpgFavSaveCb_,
                                ConstContainer.Actions.SAVE_EPG_FAV);
                            break;
                        }
                    case TypeActions.LOAD_EPG_FAV:
                        {
                            _ = XmlDataContainer.OpenAndRead(
                                BasePath.GetEpgFavoritePath(),
                                eventEpgFavLoadCb_,
                                ConstContainer.Actions.LOAD_EPG_FAV);
                            break;
                        }
                    case TypeActions.LOAD_EPG_ALIAS:
                        {
                            _ = XmlDataContainer.OpenAndRead(
                                BasePath.GetEpgAliasPath(),
                                eventEpgAliasLoadCb_,
                                ConstContainer.Actions.LOAD_EPG_ALIAS);
                            break;
                        }
                    case TypeActions.LOAD_EPG_BASE:
                        {
                            _ = XmlDataContainer.OpenQueueRead(
                                ServiceCtrl.ControlConfig.ConfigM3uPath.GetEpgBasePath(ServiceCtrl.ControlConfig.ConfigEpgUrl),
                                eventEpgBaseLoadCb_,
                                ConstContainer.Actions.LOAD_EPG_BASE);

                            _ = XmlDataContainer.OpenQueueRead(
                                BasePath.GetEpgAliasPath(),
                                eventEpgAliasLoadCb_,
                                ConstContainer.Actions.LOAD_EPG_ALIAS);

                            _ = XmlDataContainer.OpenQueueRead(
                                BasePath.GetEpgFavoritePath(),
                                eventEpgFavLoadCb_,
                                ConstContainer.Actions.LOAD_EPG_FAV);
                            XmlDataContainer.QueueStart();
                            break;
                        }
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); IsLoadingEpgData = false; }
#else
            catch { IsLoadingEpgData = false; }
#endif
        }
        #endregion

        /* SAVE BUILDER */

        #region M3u Item Save Builder (Alias)
        public class M3uItemSaveBuilder : IBuilders<ObservableCollection<ListViewM3uDataItem>, BaseSerializableChannelData>
        {
            private Func<ObservableCollection<ListViewM3uDataItem>, BaseSerializableChannelData> _method = (a) => { return default; };
            private ObservableCollection<ListViewM3uDataItem> _items = default;
            public BaseSerializableChannelData Get()
            {
                return _method.Invoke(_items);
            }

            public M3uItemSaveBuilder(ObservableCollection<ListViewM3uDataItem> i, Func<ObservableCollection<ListViewM3uDataItem>, BaseSerializableChannelData> f)
            {
                _items = i;
                _method = f;
            }

            public void Set(ObservableCollection<ListViewM3uDataItem> i)
            {
                _items = i;
            }
            public void SetMethod(Func<ObservableCollection<ListViewM3uDataItem>, BaseSerializableChannelData> f)
            {
                _method = f;
            }
        }
        #endregion

        #region EPG Item Save Builder (Favorite)
        public class EPGItemSaveBuilder : IBuilders<ObservableCollection<ListViewEpgDataItem>, BaseSerializableChannelData>
        {
            private Func<ObservableCollection<ListViewEpgDataItem>, BaseSerializableChannelData> _method = (a) => { return default; };
            private ObservableCollection<ListViewEpgDataItem> _items = default;
            public BaseSerializableChannelData Get()
            {
                return _method.Invoke(_items);
            }

            public EPGItemSaveBuilder(ObservableCollection<ListViewEpgDataItem> i, Func<ObservableCollection<ListViewEpgDataItem>, BaseSerializableChannelData> f)
            {
                _items = i;
                _method = f;
            }

            public void Set(ObservableCollection<ListViewEpgDataItem> i)
            {
                _items = i;
            }
            public void SetMethod(Func<ObservableCollection<ListViewEpgDataItem>, BaseSerializableChannelData> f)
            {
                _method = f;
            }
        }
        #endregion

        /* LOAD */

        #region - EPG Base LOAD = loadEpgBaseParser_
        private void loadEpgBaseParser_(CancellationToken tokenCancel)
        {
            DataContainerItem<BaseSerializableChannelData> dc = default;
            try
            {
                dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.LOAD_EPG_BASE);
                if ((dc == null) || (dc.Data == null))
                    return;

                dc.IsComplette = false;
                Application.Current.Dispatcher.Invoke(() =>
                {
                    EpgItems.Clear();
                });

                foreach (var i in dc.Data.channel)
                {
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    List<string> list = new List<string>();
                    if (i.displayname != null)
                        foreach (var c in i.displayname)
                            list.Add(c);

                    var ico = ((i.icon == null) || (i.icon.src == null)) ? default : i.icon.src;

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        EpgItems.Add(new ListViewEpgDataItem(i.id, list.ToArray(), ico));
                    });

                    if (M3uItems.Count > 0)
                    {
                        foreach (var n in M3uItems)
                        {
                            if (!string.IsNullOrWhiteSpace(n.IfMedia.Title.InnerTitle))
                            {
                                if ((from k in list
                                     where n.IfMedia.Title.InnerTitle.Equals(k)
                                     select k).FirstOrDefault() == default)
                                    continue;
                                n.IfMedia.Attributes.SetTvgId(i.id);
                            }
                        }
                    }
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            finally
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (EpgItems.Count > 0)
                        OnPropertyChanged(nameof(EpgItems));
                    OnPropertyChanged(nameof(IsFindEpgActions));
                    OnPropertyChanged(nameof(IsAddFavEpgActions));
                    OnPropertyChanged(nameof(IsSaveFavEpgActions));
                    if (dc != null)
                        dc.IsComplette = true;
                });
#if DEBUG
                System.Diagnostics.Debug.WriteLine("* loadEpgBaseParser = IsComplette");
#endif
            }
        }
        #endregion

        #region - EPG Alias LOAD = loadEpgAliasParse_
        private void loadEpgAliasParse_(CancellationToken tokenCancel)
        {
            DataContainerItem<BaseSerializableChannelData> dc = default;
            try
            {
                if (!checkMainBase(ConstContainer.Actions.LOAD_EPG_BASE, tokenCancel))
                    return;

                dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.SAVE_EPG_ALIAS);
                if (dc == null)
                    return;

                foreach (var i in dc.Data.channel.Distinct())
                {
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    bool b = ((i.displayname != null) && (i.displayname.Length > 0));
                    var a = (from n in EpgItems
                             where n.IfTag.Equals(i.id)
                             select n).FirstOrDefault();
                    string name = getIconName(i);

                    if (a == null)
                    {
                        List<string> list = new List<string>();
                        if (b)
                            foreach (var c in i.displayname)
                                list.Add(c);

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EpgItems.Add(new ListViewEpgDataItem(i.id, list.ToArray(), name));
                        });
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(a.IfIcon) && !string.IsNullOrWhiteSpace(name))
                            Application.Current.Dispatcher.Invoke(() => { a.IfIcon = name; });
                        if (b)
                            foreach (var c in i.displayname)
                                Application.Current.Dispatcher.Invoke(() => { a.IfListItems.Add(c); });
                    }
                    ///
                    if (b)
                    {
                        for (int k = i.displayname.Length - 1; k >= 0; k--)
                        {
                            var d = (from n in M3uItems
                                     where n.IfChannelName.Equals(i.displayname[k])
                                     select n).FirstOrDefault();
                            if (d != null)
                            {
                                Application.Current.Dispatcher.Invoke(() => { d.IfTag = i.id; });
                                if (string.IsNullOrWhiteSpace(d.IfIcon) && !string.IsNullOrWhiteSpace(name))
                                    Application.Current.Dispatcher.Invoke(() => { d.IfIcon = name; });
                            }
                        }
                    }
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            finally
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (EpgItems.Count > 0)
                        OnPropertyChanged(nameof(EpgItems));
                    OnPropertyChanged(nameof(IsFindEpgActions));
                    OnPropertyChanged(nameof(IsAddFavEpgActions));
                    OnPropertyChanged(nameof(IsSaveFavEpgActions));
                    if (dc != null)
                        dc.IsComplette = true;
                    IsLoadingEpgData = false;
                });
#if DEBUG
                System.Diagnostics.Debug.WriteLine("*loadEpgAliasParse = IsComplette");
#endif
            }
        }
        #endregion

        #region - EPG Favorites LOAD = loadEpgFavParse_
        private void loadEpgFavParse_(CancellationToken tokenCancel)
        {
            DataContainerItem<BaseSerializableChannelData> dc = default;
            try
            {
                if (!checkMainBase(ConstContainer.Actions.LOAD_EPG_BASE, tokenCancel))
                    return;
                _ = checkMainBase(ConstContainer.Actions.LOAD_EPG_ALIAS, tokenCancel);

                dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.LOAD_EPG_FAV);
                if ((dc == null) || (dc.Data == null))
                    return;

                dc.IsComplette = false;
                for (int k = 0; k < EpgItems.Count; k++)
                {
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    ListViewEpgDataItem n = EpgItems[k];
                    for (int i = dc.Data.channel.Length - 1; i >= 0; i--)
                    {
                        if (n.IfTag.Equals(dc.Data.channel[i].id))
                            Application.Current.Dispatcher.Invoke(() => { n.IfFavorite = true; });
                    }
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            finally
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (EpgItems.Count > 0)
                        OnPropertyChanged(nameof(EpgItems));
                    OnPropertyChanged(nameof(IsFindEpgActions));
                    OnPropertyChanged(nameof(IsAddFavEpgActions));
                    OnPropertyChanged(nameof(IsSaveFavEpgActions));
                    if (dc != null)
                        dc.IsComplette = true;
                    IsLoadingEpgData = false;
                });
#if DEBUG
                System.Diagnostics.Debug.WriteLine("*loadEpgFavParse = IsComplette");
#endif
            }
        }
        #endregion

        #region - M3U LOAD async = loadM3uList_, loadM3uListAsync_
        private void loadM3uList_(string path)
        {
            if (IsLoadingSavingM3uData)
                return;
            IsLoadingSavingM3uData = true;
            loadM3uListAsync_(tokenSourceCancel.Token, path);
        }

        private async void loadM3uListAsync_(CancellationToken tokenCancel, string path)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                M3uItems.Clear();
                OnPropertyChanged(nameof(M3uItems));
            });
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        Extm3u m3u = M3U.M3UP.ParseFromFile(path);
                        if (m3u == null)
                            return;

                        foreach (var i in m3u.Medias)
                        {
                            if (tokenCancel.IsCancellationRequested)
                                break;
                            Application.Current.Dispatcher.Invoke(() => { M3uItems.Add(new ListViewM3uDataItem(i)); });
                        }
                        if (M3uItems.Count > 0)
                            Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(M3uItems)); });
                    }
#if DEBUG
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine(e);
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingSavingM3uData = false; });
                        return;
                    }
#else
                    catch { Application.Current.Dispatcher.Invoke(() => { IsLoadingSavingM3uData = false; }); return; }
#endif
                    ///
                    try
                    {
                        if ((EpgItems.Count == 0) || (M3uItems.Count == 0))
                            return;

                        foreach (var i in M3uItems)
                        {
                            var a = (from n in EpgItems
                                     where n.IfListItems.Contains(i.IfChannelName)
                                     select n).FirstOrDefault();
                            if (a != default)
                                i.IfTag = a.IfTag;
                        }
                        Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(M3uItems)); });
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                    finally
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingSavingM3uData = false; });
                    }
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
#else
            catch { }
#endif
        }
        #endregion

        /* SAVE as BUILDER */

        #region - EPG Alias SAVE BUILDER = builderEpgAliasSaveCb_
        private BaseSerializableChannelData builderEpgAliasSaveCb_(ObservableCollection<ListViewM3uDataItem> items)
        {
            try
            {
                if ((items == default) || (items.Count == 0))
                    return default;

                DataContainerItem<BaseSerializableChannelData> dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.SAVE_EPG_ALIAS);
                if (dc == null)
                    return default;

                List<BaseSerializableGroup> list = new List<BaseSerializableGroup>();
                foreach (var i in items)
                {
                    if ((i.IfMedia == null) ||
                        (i.IfMedia.Attributes == null) ||
                        (string.IsNullOrWhiteSpace(i.IfMedia.Attributes.TvgId)))
                        continue;

                    try
                    {
                        var a = (from n in list
                                 where n.id.Equals(i.IfMedia.Attributes.TvgId)
                                 select n).FirstOrDefault();

                        BaseSerializableGroup g;
                        if (a == null)
                        {
                            g = new BaseSerializableGroup(true)
                            {
                                id = i.IfTag
                            };
                            if (g.icon == null)
                                g.icon = new BaseSerializableIcon(true);
                            g.icon.src = i.IfIcon;
                        }
                        else
                            g = a;

                        List<string> l = (g.displayname == null) ? new List<string>() : new List<string>(g.displayname);
                        l.Add(i.IfChannelName);
                        g.displayname = l.ToArray();
                        if (a == null)
                            list.Add(g);
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                }
                if (list.Count > 0)
                {
                    BaseSerializableChannelData data = new BaseSerializableChannelData(true, list.Count)
                    {
                        channel = list.ToArray()
                    };
                    dc.IsComplette = true;
                    return data;
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return default;
        }
        #endregion

        #region - EPG Favorites SAVE BUILDER = builderEpgFavSaveCb_
        private BaseSerializableChannelData builderEpgFavSaveCb_(ObservableCollection<ListViewEpgDataItem> items)
        {
            try
            {
                DataContainerItem<BaseSerializableChannelData> dc = XmlDataContainer.GetContainerItemFromId(ConstContainer.Actions.SAVE_EPG_FAV);
                if (dc == null)
                    return default;

                List<BaseSerializableGroup> list = new List<BaseSerializableGroup>();
                foreach (var i in items)
                {
                    if (!i.IfFavorite)
                        continue;
                    try
                    {
                        BaseSerializableGroup g = new BaseSerializableGroup(true)
                        {
                            id = i.IfTag
                        };
                        if (g.icon == null)
                            g.icon = new BaseSerializableIcon(true);
                        g.icon.src = i.IfIcon;

                        if (!string.IsNullOrWhiteSpace(i.IfChannelName))
                        {
                            if (g.displayname == null)
                                g.displayname = new string[] { i.IfChannelName };
                            else if (g.displayname.Length > 0)
                            {
                                List<string> l = new List<string>(g.displayname)
                                {
                                    i.IfChannelName
                                };
                                g.displayname = l.ToArray();
                            }
                        }
                        else if (g.displayname == null)
                            g.displayname = new string[0];
                        list.Add(g);
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                }
                if (list.Count > 0)
                {
                    BaseSerializableChannelData data = new BaseSerializableChannelData(true, list.Count)
                    {
                        channel = list.ToArray()
                    };
                    dc.IsComplette = true;
                    return data;
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return default;
        }
        #endregion

        /* SAVE */

        #region - M3U List SAVE TASK = eventM3uListSaveCb_
        private async Task eventM3uListSaveCb_(CancellationToken tokenCancel, string s)
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        M3uOpenGroup = s;
                        string group = Path.GetFileNameWithoutExtension(s).Trim();

                        using (var swr = new StreamWriter(s, false, new UTF8Encoding(false)))
                        {
                            int cnt = 1;
                            swr.AutoFlush = true;
                            M3U.M3UWriter.WriteHeader(
                                swr,
                                ServiceCtrl.ControlConfig.ConfigEpgUrl
                                    .EpgArchFromString(
                                        ServiceCtrl.ControlConfig.ConfigNetAddr,
                                        ServiceCtrl.ControlConfig.ConfigNetPort));

                            foreach (var i in M3uItems)
                            {
                                i.IfMedia.Attributes.SetGroupTitle(group);
                                i.IfMedia.Attributes.SetChannelNumber(cnt++);
                                M3U.M3UWriter.WriteItem(swr, i.IfMedia);
                            }
                            swr.Close();
                        }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
        }
        #endregion

        /* UTILS */
        #region private utils

        private bool checkMainBase(ConstContainer.Actions type, CancellationToken tokenCancel)
        {
            var parent = XmlDataContainer.GetContainerItemFromId(type);
            if (parent == null)
                return false;

            int cnt = 1500000;
            while (EpgItems.Count == 0 || !parent.IsComplette)
            {
                Task.Delay(750);
                Task.Yield();
                if (cnt-- <= 0)
                    return false;
                if (tokenCancel.IsCancellationRequested)
                    return false;
            }
            return true;
        }
        private string getIconName(BaseSerializableGroup g)
        {
            return ((g.icon == null) || (g.icon.src == null)) ? default : g.icon.src;
        }

        #endregion

    }
}
