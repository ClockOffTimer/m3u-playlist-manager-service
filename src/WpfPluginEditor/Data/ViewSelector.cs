﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */
using System;

namespace WpfPlugin.Data
{
    internal class ViewSelector<T>
    {
        public PageEditorData.TypeElementView LeftView { get; set; } = PageEditorData.TypeElementView.VIEW_NONE;
        public PageEditorData.TypeElementView RightView { get; set; } = PageEditorData.TypeElementView.VIEW_RIGHT_LIST;
        public PageEditorData.TypeElementView LeftBackup { get; set; } = PageEditorData.TypeElementView.VIEW_LEFT_VIDEO;
        public PageEditorData.TypeElementView RightBackup { get; set; } = PageEditorData.TypeElementView.VIEW_RIGHT_LIST;
        public PageEditorData.TypeElementView VideoLastView { get; set; } = PageEditorData.TypeElementView.VIEW_RIGHT_VIDEO;
        private readonly Action<T> action;
        private readonly T arg;

        public ViewSelector(Action<T> act, T obj)
        {
            arg = obj;
            action = act;
        }
        public void Backup()
        {
            LeftBackup = (LeftView == PageEditorData.TypeElementView.VIEW_LEFT_VIDEO) ?
                PageEditorData.TypeElementView.VIEW_NONE : LeftView;
            RightBackup = (RightView == PageEditorData.TypeElementView.VIEW_RIGHT_VIDEO) ?
                PageEditorData.TypeElementView.VIEW_RIGHT_LIST : RightView;
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"\t - Backup({LeftView},{RightView})");
#endif
        }
        public void Begin()
        {
            Backup();
            LeftView = (VideoLastView == PageEditorData.TypeElementView.VIEW_LEFT_VIDEO) ?
                VideoLastView : PageEditorData.TypeElementView.VIEW_NONE;
            RightView = (VideoLastView == PageEditorData.TypeElementView.VIEW_RIGHT_VIDEO) ?
                VideoLastView : PageEditorData.TypeElementView.VIEW_RIGHT_LIST;

            action.Invoke(arg);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"\t - Begin({LeftView},{RightView})");
#endif
        }
        public void Set(PageEditorData.TypeElementView l, PageEditorData.TypeElementView r)
        {
            LeftView = l;
            RightView = r;
            System.Diagnostics.Debug.WriteLine($"\t - Set({l},{r})");
        }
        public void SetVideoPlace()
        {
            if (VideoLastView == PageEditorData.TypeElementView.VIEW_LEFT_VIDEO)
                LeftView = VideoLastView;
            else
                LeftView = PageEditorData.TypeElementView.VIEW_NONE;

            if (VideoLastView == PageEditorData.TypeElementView.VIEW_RIGHT_VIDEO)
                RightView = VideoLastView;
            else
                RightView = PageEditorData.TypeElementView.VIEW_RIGHT_LIST;

            action.Invoke(arg);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"\t - SetVideoPlace({LeftView},{RightView})");
#endif
        }
        public void SetVideoPlace(PageEditorData.TypeElementView t)
        {
            VideoLastView = t;
            SetVideoPlace();
        }
        public void End()
        {
            switch (RightBackup)
            {
                case PageEditorData.TypeElementView.VIEW_RIGHT_EDIT:
                case PageEditorData.TypeElementView.VIEW_RIGHT_LIST:
                    {
                        RightView = RightBackup;
                        break;
                    }
                default:
                    {
                        RightView = PageEditorData.TypeElementView.VIEW_RIGHT_LIST;
                        break;
                    }
            }
            LeftView = PageEditorData.TypeElementView.VIEW_NONE;

            action.Invoke(arg);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"\t - End({LeftView},{RightView} -> {RightBackup})");
#endif
        }
    }
}
