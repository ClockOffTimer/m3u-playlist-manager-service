﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.ObjectModel;
using System.ComponentModel;
using M3U.Model;
using PlayListServiceData.Container.Items;

namespace WpfPlugin.Data
{
    public class ListViewM3uDataItem : IViewItem<ObservableCollection<string>>, INotifyPropertyChanged
    {
        private Media _M3uMedia = default;
        private bool _IsFavorite = default;

        public Media IfMedia
        {
            get { return _M3uMedia; }
            set
            {
                if (_M3uMedia == value)
                    return;
                _M3uMedia = value;
                OnPropertyChanged(nameof(IfTag));
                OnPropertyChanged(nameof(IfIcon));
                OnPropertyChanged(nameof(IfMedia));
                OnPropertyChanged(nameof(IfFavorite));
                OnPropertyChanged(nameof(IfChannelName));
            }
        }

        /* interface IViewItem */

        public string IfTag
        {
            get { return (IfMedia == null) ? default : IfMedia.Attributes.TvgId; }
            set { if (IfMedia != null) IfMedia.Attributes.SetTvgId(value); OnPropertyChanged(nameof(IfTag)); }
        }
        public string IfIcon
        {
            get { return (IfMedia == null) ? default : IfMedia.Attributes.TvgLogo; }
            set { if (IfMedia != null) IfMedia.Attributes.SetTvgLogo(value); OnPropertyChanged(nameof(IfIcon)); }
        }
        public string IfChannelName
        {
            get { return (IfMedia == null) ? default : IfMedia.Title.InnerTitle; }
            set { if (IfMedia != null) IfMedia.Title.InnerTitle = value; OnPropertyChanged(nameof(IfChannelName)); }
        }
        public bool IfFavorite
        {
            get { return _IsFavorite; }
            set { _IsFavorite = value; OnPropertyChanged(nameof(IfFavorite)); }
        }
        public ObservableCollection<string> IfListItems { get; set; } = default;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ListViewM3uDataItem() { }
        public ListViewM3uDataItem(Media media)
        {
            IfMedia = media;
        }
        public void Set(Media media)
        {
            IfMedia = media;
        }
    }
}
