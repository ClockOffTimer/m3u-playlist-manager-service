﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using M3U.Model;

namespace WpfPlugin.Data
{
    public partial class PageEditorData : INotifyPropertyChanged
    {
        public enum TypeActions : int
        {
            LOAD_M3U = 0,
            LOAD_EPG_BASE,
            LOAD_EPG_ALIAS,
            LOAD_EPG_FAV,
            SAVE_M3U_AS,
            SAVE_EPG_ALIAS,
            SAVE_EPG_FAV,
            ADD_EPG_FAV,
            ADD_ICON,
            EDIT_M3U,
            FIND_M3U,
            FIND_EPG,
            FIND_AUTO,
            FIND_ACCEPT,
            PLAY_M3U,
            PAUSE_M3U,
            STOP_M3U,
            CAPTURE_IMG,
            ASSIGN_ID,
            CLEAR_M3U,
            DELETE_M3U,
            NONE
        };
        public enum TypeElementView : int
        {
            VIEW_NONE = 0,
            VIEW_RIGHT_LIST,
            VIEW_RIGHT_EDIT,
            VIEW_RIGHT_VIDEO,
            VIEW_LEFT_VIDEO
        };

        #region Set/Get

        public Media MediaSelected
        {
            get
            {
                if ((SelectedIndexM3u < 0) ||
                    (SelectedIndexM3u >= M3uItems.Count) ||
                    (M3uItems[SelectedIndexM3u].IfMedia == null))
                    return default(Media);
                return M3uItems[SelectedIndexM3u].IfMedia;
            }
        }

        public string M3uOpenGroup
        {
            get { return string.IsNullOrWhiteSpace(_M3uOpenFile) ? default : Path.GetFileNameWithoutExtension(_M3uOpenFile); }
            set
            {
                if (_M3uOpenFile == value)
                    return;
                _M3uOpenFile = value;
                OnPropertyChanged(nameof(M3uOpenGroup));
            }
        }

        public string M3uIcon
        {
            get { return _M3uIcon; }
            set
            {
                do
                {
                    if (value == default)
                        _M3uIcon = UrlConverterEditor.DefaultImageUri;
                    else
                        _M3uIcon = value;

                } while (false);
                OnPropertyChanged(nameof(M3uIcon));
            }
        }

        public int SelectedIndexEpg
        {
            get { return _SelectedIndexEpg; }
            set
            {
                if (_SelectedIndexEpg == value)
                    return;
                _SelectedIndexEpg = value;
                OnPropertyChanged(nameof(SelectedIndexEpg));
                OnPropertyChanged(nameof(IsAddFavEpgActions));
                OnPropertyChanged(nameof(IsSaveFavEpgActions));
            }
        }

        public int SelectedIndexM3u
        {
            get { return _SelectedIndexM3u; }
            set
            {
                if (_SelectedIndexM3u == value)
                    return;
                _SelectedIndexM3u = value;
                OnPropertyChanged(nameof(SelectedIndexM3u));
                OnPropertyChanged(nameof(IsM3uDeleteActions));
                OnPropertyChanged(nameof(IsSelectedM3uActions));
                if ((_SelectedIndexM3u >= 0) && (_SelectedIndexM3u < M3uItems.Count))
                {
                    M3uIcon = M3uItems[SelectedIndexM3u].IfMedia.Attributes.TvgLogo;
                    IsInfoExpanded = false;
                }
            }
        }

        public bool IsLoadingEpgData
        {
            get { return _IsLoaingdEpgData; }
            set
            {
                if (_IsLoaingdEpgData == value)
                    return;
                _IsLoaingdEpgData = value;
                SelectedIndexEpg = -1;
                OnPropertyChanged(nameof(IsLoadingEpgData));
                OnPropertyChanged(nameof(IsReloadEpgActions));
                OnPropertyChanged(nameof(IsFindEpgActions));
                OnPropertyChanged(nameof(IsAddFavEpgActions));
                OnPropertyChanged(nameof(IsSaveFavEpgActions));
                OnPropertyChanged(nameof(IsSaveM3uActions));
                OnPropertyChanged(nameof(GuiNormalVisible));
                OnPropertyChanged(nameof(GuiLoadVisible));
            }
        }

        public bool IsReloadEpgActions
        {
            get { return !_IsLoaingdEpgData && (RightView == TypeElementView.VIEW_RIGHT_LIST); }
        }
        public bool IsAddFavEpgActions
        {
            get { return (EpgItems.Count > 0) && (_SelectedIndexEpg != -1) && (RightView == TypeElementView.VIEW_RIGHT_LIST); }
        }

        public bool IsSaveFavEpgActions
        {
            get { return _IsFavEpgAdded && (EpgItems.Count > 0) && (_SelectedIndexEpg != -1) && (RightView == TypeElementView.VIEW_RIGHT_LIST); }
        }

        public bool IsFindEpgActions
        {
            get { return (EpgItems.Count > 0) && (RightView == TypeElementView.VIEW_RIGHT_LIST); }
        }

        public bool IsLoadingSavingM3uData
        {
            get { return _IsLoaingdM3uData; }
            set
            {
                if (_IsLoaingdM3uData == value)
                    return;
                _IsLoaingdM3uData = value;
                SelectedIndexM3u = -1;
                OnPropertyChanged(nameof(IsLoadingSavingM3uData));
                OnPropertyChanged(nameof(IsSelectedM3uActions));
                OnPropertyChanged(nameof(IsReloadM3uActions));
                OnPropertyChanged(nameof(IsSaveM3uActions));
                OnPropertyChanged(nameof(IsFindM3uActions));
                OnPropertyChanged(nameof(GuiNormalVisible));
                OnPropertyChanged(nameof(GuiLoadVisible));
            }
        }

        public bool IsFindAccept
        {
            get { return _IsFindAccept; }
            set
            {
                if (_IsFindAccept == value)
                    return;
                _IsFindAccept = value;
                OnPropertyChanged(nameof(IsFindAccept));
            }
        }

        public TypeElementView RightView
        {
            get { return stateSelector.RightView; }
            set
            {
                if (stateSelector.RightView == value)
                    return;
                stateSelector.RightView = value;
                flagsNotify();
            }
        }
        public TypeElementView LeftView
        {
            get { return stateSelector.LeftView; }
            set
            {
                if (stateSelector.LeftView == value)
                    return;
                stateSelector.LeftView = value;
                flagsNotify();
            }
        }

        public MediaState PlayStatus
        {
            get { return (MediaPlayer == default) ? MediaState.Manual : MediaPlayer.PlayerStatus; }
        }

        public bool IsRightListView
        {
            get { return RightView == TypeElementView.VIEW_RIGHT_LIST; }
        }
        public bool IsRightEditView
        {
            get { return RightView == TypeElementView.VIEW_RIGHT_EDIT; }
        }
        public bool IsRightVideoView
        {
            get { return RightView == TypeElementView.VIEW_RIGHT_VIDEO; }
        }
        public bool IsLeftVideoView
        {
            get { return LeftView == TypeElementView.VIEW_LEFT_VIDEO; }
        }

        public bool IsInfoExpanded
        {
            get
            {
                return ((RightView == TypeElementView.VIEW_RIGHT_EDIT) ||
                        (RightView == TypeElementView.VIEW_RIGHT_LIST) ||
                        (RightView == TypeElementView.VIEW_NONE) ||
                        (RightView == TypeElementView.VIEW_RIGHT_VIDEO)) && _IsInfoExpanded;
            }
            set
            {
                if (_IsInfoExpanded == value)
                    return;
                _IsInfoExpanded = value;
                OnPropertyChanged(nameof(IsInfoExpanded));
                OnPropertyChanged(nameof(GuiMediaIconVisible));
            }
        }

        public bool IsReloadM3uActions
        {
            get { return !IsLoadingSavingM3uData; }
        }

        public bool IsSaveM3uActions
        {
            get { return (M3uItems.Count > 0) && !IsLoadingSavingM3uData; }
        }

        public bool IsFindM3uActions
        {
            get { return M3uItems.Count > 0; }
        }

        public bool IsSelectedM3uActions
        {
            get { return (M3uItems.Count > 0) && (SelectedIndexM3u >= 0); }
        }

        public bool IsM3uDeleteActions
        {
            get { return SelectedIndexM3u >= 0; }
        }

        public Visibility GuiNormalVisible
        {
            get { return (_IsLoaingdEpgData || _IsLoaingdM3uData) ? Visibility.Hidden : Visibility.Visible; }
        }
        public Visibility GuiLoadVisible
        {
            get { return (_IsLoaingdEpgData || _IsLoaingdM3uData) ? Visibility.Visible : Visibility.Hidden; }
        }
        public Visibility GuiMediaIconVisible
        {
            get { return IsInfoExpanded ? Visibility.Collapsed : Visibility.Visible; }
        }
        public Visibility GuiRigthListViewVisible
        {
            get { return (RightView == TypeElementView.VIEW_RIGHT_LIST) ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility GuiRigthEditorVisible
        {
            get { return (RightView == TypeElementView.VIEW_RIGHT_EDIT) ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility GuiRigthVideoVisible
        {
            get { return (RightView == TypeElementView.VIEW_RIGHT_VIDEO) ? Visibility.Visible : Visibility.Collapsed; }
        }

        #endregion

    }
}
