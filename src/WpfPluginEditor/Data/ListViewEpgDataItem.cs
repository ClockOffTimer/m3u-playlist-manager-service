﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.ObjectModel;
using System.ComponentModel;
using M3U.Model;
using PlayListServiceData.Container.Items;

namespace WpfPlugin.Data
{
    public class ListViewEpgDataItem : IViewItem<ObservableCollection<string>>, INotifyPropertyChanged
    {
        private bool _IsFavorite = default;
        private string _Tag = default;
        private string _Icon = default;
        private string _ChannelExample = default;

        /* interface IViewItem */

        public Media IfMedia { get; set; } = default;

        public string IfTag
        {
            get { return _Tag; }
            set { _Tag = value; OnPropertyChanged(nameof(IfTag)); }
        }
        public string IfIcon
        {
            get { return _Icon; }
            set { _Icon = value; OnPropertyChanged(nameof(IfIcon)); }
        }
        public string IfChannelName
        {
            get { return _ChannelExample; }
            set { _ChannelExample = value; OnPropertyChanged(nameof(IfChannelName)); }
        }
        public bool IfFavorite
        {
            get { return _IsFavorite; }
            set { _IsFavorite = value; OnPropertyChanged(nameof(IfFavorite)); }
        }
        public ObservableCollection<string> IfListItems { get; set; } = new ObservableCollection<string>();

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ListViewEpgDataItem() { }
        public ListViewEpgDataItem(string tag, string[] channels, string ico, bool b = false)
        {
            _IsFavorite = b;
            _Tag = tag;
            _Icon = ico;
            if ((channels != null) && (channels.Length > 0))
            {
                _ChannelExample = channels[0];
                foreach (var i in channels)
                    IfListItems.Add(i);
            }
        }
    }
}
