﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using M3U.Model;
using Microsoft.Win32;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
       ExportMetadata("Id", 5),
       ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageEditor : Page, IPlugin
    {
        private IServiceControls ServiceCtrl { get; set; } = default;
        private PageEditorData pageEditorData { get; set; } = default;
        private readonly string[] controlTag;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageEditor(IServiceControls controls)
        {
            ServiceCtrl = controls;
            InitializeComponent();
            pageEditorData = (PageEditorData)DataContext;
            pageEditorData.SetServiceCtrl(controls, videoPanel, VideoViewSelect_CallBack);
            videoPanel.MediaPlayer.SetServiceCtrl(controls);

            controlTag = Enum.GetNames(typeof(PageEditorData.TypeActions)).ToArray<string>();
        }

        #region left video panel
        private void VideoViewSelect_CallBack(MediaPlay.PlayerStage stage, MediaState state, Media media)
        {
            try
            {
                if ((state == MediaState.Play) ||
                    (state == MediaState.Stop))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.STOP_M3U, videoPanel.MediaPlayer, default(string), default(string));

                switch (stage)
                {
                    case MediaPlay.PlayerStage.VIDEO_LEFT:
                        {
                            if (RightVideoContainer.Children.Count > 0)
                            {
                                RightVideoContainer.Children.Clear();
                            }
                            else if (LeftVideoContainer.Children.Count > 0)
                            {
                                LeftVideoContainer.Children.Clear();
                                stage = MediaPlay.PlayerStage.VIDEO_RIGHT;
                            }
                            break;
                        }
                    case MediaPlay.PlayerStage.VIDEO_RIGHT:
                        {
                            if (LeftVideoContainer.Children.Count > 0)
                            {
                                LeftVideoContainer.Children.Clear();
                            }
                            else if (RightVideoContainer.Children.Count > 0)
                            {
                                RightVideoContainer.Children.Clear();
                                stage = MediaPlay.PlayerStage.VIDEO_LEFT;
                            }
                            break;
                        }
                    default: return;
                }

                Task.Delay(1000);

                switch (stage)
                {
                    case MediaPlay.PlayerStage.VIDEO_LEFT:
                        {
                            LeftVideoContainer.Children.Add(videoPanel);
                            videoPanel.Width = videoPanel.MediaViewWidth = InfoExpander.ActualWidth;
                            videoPanel.Height = 200;
                            break;
                        }
                    case MediaPlay.PlayerStage.VIDEO_RIGHT:
                        {
                            RightVideoContainer.Children.Add(videoPanel);
                            videoPanel.Width = videoPanel.MediaViewWidth = RightBorderDefaultSizer.ActualWidth;
                            videoPanel.Height = 400;
                            break;
                        }
                }
                videoPanel.Visibility = Visibility.Visible;
                /*
                int z = 100000;
                while (z-- <= 0)
                {
                    Task.Delay(1000);
                    Task.Yield();
                }
                pageEditorData.ItemsActions(PageEditorData.TypeActions.PLAY_M3U, videoPanel.MediaPlayer, media, default(string));
                */
            }
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.Message); }
        }

        private void MenuItemStopPlay_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.PLAY_M3U, videoPanel.MediaPlayer, default(string), default(string));
        }

        private void MenuItemPause_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.PAUSE_M3U, videoPanel.MediaPlayer, default(string), default(string));
        }
        #endregion

        #region left panel

        #region ListView M3U

        private void ListViewM3u_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.LeftButton == MouseButtonState.Pressed) && (e.Source != null) && (sender is ModernWpf.Controls.ListView lv))
                if (lv.SelectedItem != null)
                {
                    if (e.GetPosition(lv).X > (lv.ActualHeight - 70))
                        return;
                    DragDrop.DoDragDrop(lv, lv.SelectedItem, DragDropEffects.Move);
                    e.Handled = true;
                }
        }
        private void ListViewM3u_DragEnter(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
                e.Effects = DragDropEffects.Move;
        }
        private void ListViewM3u_Drop(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    e.Handled = false;
                else if (e.Data.GetDataPresent(typeof(ListViewEpgDataItem)))
                    pageEditorData.ItemsActions(
                        PageEditorData.TypeActions.ASSIGN_ID,
                        e.Data.GetData(typeof(ListViewEpgDataItem)) as ListViewEpgDataItem,
                        default(string),
                        default(string));
                e.Handled = true;
            }
        }

        #endregion

        #region M3U LOAD
        private void ButtonLoadM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.LOAD_M3U]))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigM3uPath))
                        return;

                    OpenFileDialog ofd = new OpenFileDialog
                    {
                        Filter = @"M3UP files (m3u)|*.m3u",
                        Multiselect = false,
                        RestoreDirectory = true,
                        InitialDirectory = ServiceCtrl.ControlConfig.ConfigM3uPath
                    };
                    if (ofd.ShowDialog() == true)
                        pageEditorData.ItemsActions(PageEditorData.TypeActions.LOAD_M3U, ofd.FileName, default(string), default(string));
                }
                catch { }
            }
        }

        #endregion

        #region M3U SAVE
        private void ButtonSaveM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.SAVE_EPG_ALIAS]))
                pageEditorData.ItemsActions(PageEditorData.TypeActions.SAVE_EPG_ALIAS, default(string), default(string), default(string));
        }
        #endregion

        #region M3U SAVE AS
        private void ButtonSaveAsM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.SAVE_M3U_AS]))
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigM3uPath))
                        return;

                    SaveFileDialog sfd = new SaveFileDialog
                    {
                        Filter = @"M3UP files (m3u)|*.m3u",
                        DefaultExt = "m3u",
                        RestoreDirectory = true,
                        InitialDirectory = ServiceCtrl.ControlConfig.ConfigM3uPath
                    };
                    if (sfd.ShowDialog() == true)
                        pageEditorData.ItemsActions(PageEditorData.TypeActions.SAVE_M3U_AS, sfd.FileName, default(string), default(string));
                }
                catch { }
            }
        }

        #endregion

        #region EDIT M3U
        private void ButtonEditM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.EDIT_M3U]))
                MenuItemEditM3u_Click(null, null);
        }
        private void MenuItemEditM3u_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.EDIT_M3U, editPanel, default(string), default(string));
        }
        #endregion

        #region M3U AUTO FIND && ACCEPT
        private void ButtonAutoFindM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_AUTO]))
                pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_AUTO, TextBoxFindEpg, ListViewEPGChannel, ListViewM3UChannel);
        }
        private void ButtonAutoAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_ACCEPT]))
                pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_ACCEPT, default(string), ListViewEPGChannel, ListViewM3UChannel);
        }
        private void MenuItemAutoFind_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_AUTO, TextBoxFindEpg, ListViewEPGChannel, ListViewM3UChannel);
        }
        #endregion

        #region M3U select icon from disk
        private void ButtonIconAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.ADD_ICON]))
            {
                try
                {
                    OpenFileDialog ofd = new OpenFileDialog
                    {
                        Filter = @"Image files (png,jpg)|*.png;*.jpg|All files|*.*",
                        Multiselect = false,
                        RestoreDirectory = true,
                        InitialDirectory = $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonPictures)}{Path.DirectorySeparatorChar}"
                    };
                    if (ofd.ShowDialog() == true)
                        pageEditorData.ItemsActions(PageEditorData.TypeActions.ADD_ICON, ofd.FileName, default(string), default(string));
                }
                catch { }
            }
        }
        #endregion

        #region M3U Play/Stop
        private void ButtonPlayStopM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.PLAY_M3U]))
                MenuItemStopPlay_Click(null, null);
        }
        #endregion

        #region M3U FIND
        private void ButtonFindM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_M3U]))
            {
                if (string.IsNullOrWhiteSpace(TextBoxFindM3uChannel.Text))
                    TextBoxFindM3uChannel.Focus();
                else
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_M3U, TextBoxFindM3uChannel.Text, default(string), ListViewM3UChannel);
            }
        }
        private void TextBoxFindM3uChannel_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Return) && (sender is TextBox tb))
            {
                if (!string.IsNullOrWhiteSpace(tb.Text))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_M3U, tb.Text, default(string), ListViewM3UChannel);
                e.Handled = true;
            }
        }
        private void TextBoxFindM3uChannel_DragEnter(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_M3U]))
                e.Effects = DragDropEffects.Copy;
        }
        private void TextBoxFindM3uChannel_DragOver(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_M3U]))
            {
                if (e.Data.GetDataPresent(typeof(string)) ||
                    e.Data.GetDataPresent(typeof(ListViewEpgDataItem)))
                    e.Handled = true;
            }
        }
        private void TextBoxFindM3uChannel_Drop(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_M3U]))
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_M3U, e.Data.GetData(typeof(string)) as string, tb, ListViewM3UChannel);
                else if (e.Data.GetDataPresent(typeof(ListViewEpgDataItem)))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_M3U, e.Data.GetData(typeof(ListViewEpgDataItem)) as ListViewEpgDataItem, tb, ListViewM3UChannel);
                e.Handled = true;
            }
        }

        #endregion

        #region M3U Clear EPG Id
        private void ButtonClearM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.CLEAR_M3U]))
                pageEditorData.ItemsActions(PageEditorData.TypeActions.CLEAR_M3U, default(string), default(string), default(string));
        }
        private void MenuItemClearEpg_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.CLEAR_M3U, default(string), default(string), default(string));
        }
        #endregion

        #region M3U Delete
        private void ButtonDeleteM3u_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.DELETE_M3U]))
                pageEditorData.ItemsActions(PageEditorData.TypeActions.DELETE_M3U, default(string), default(string), default(string));
        }
        private void MenuItemDeleteChannel_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.DELETE_M3U, default(string), default(string), default(string));
        }
        #endregion

        #endregion

        #region right panel

        #region ListView Epg
        private void ListViewEpg_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.LeftButton == MouseButtonState.Pressed) && (e.Source != null) && (sender is ModernWpf.Controls.ListView lv))
                if (lv.SelectedItem != null)
                {
                    if (e.GetPosition(lv).X > (lv.ActualHeight - 70))
                        return;
                    DragDrop.DoDragDrop(lv, lv.SelectedItem, DragDropEffects.Copy);
                    e.Handled = true;
                }
        }
        private void ListViewEpg_DragEnter(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
                e.Effects = DragDropEffects.Copy;
        }
        private void ListViewEpg_DragOver(object sender, DragEventArgs e)
        {
            if (sender is ModernWpf.Controls.ListView)
                e.Handled = true;
            else
                e.Handled = false;
        }
        private void ListViewEpg_Drop(object sender, DragEventArgs e)
        {
            e.Handled = false;
        }

        #endregion

        #region EPG LOAD
        private void ButtonLoadEpg_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.LOAD_EPG_BASE]))
                MenuItemEpgRefreshFavorite_Click(sender, e);
        }
        private void MenuItemEpgRefreshFavorite_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.LOAD_EPG_BASE, default(string), default(string), default(string));
        }
        #endregion

        #region EPG FIND
        private void ButtonFindEpg_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_EPG]))
            {
                if (string.IsNullOrWhiteSpace(TextBoxFindEpg.Text))
                    TextBoxFindEpg.Focus();
                else
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_EPG, TextBoxFindEpg.Text, default(string), ListViewEPGChannel);
            }
        }
        private void TextBoxFindEpg_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Return) && (sender is TextBox tb))
            {
                if (!string.IsNullOrWhiteSpace(tb.Text))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_EPG, tb.Text, default(string), ListViewEPGChannel);
                e.Handled = true;
            }
        }
        private void TextBoxFindEpg_DragEnter(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_EPG]))
                e.Effects = DragDropEffects.Copy;
        }
        private void TextBoxFindEpg_DragOver(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_EPG]))
            {
                if (e.Data.GetDataPresent(typeof(string)) ||
                    e.Data.GetDataPresent(typeof(ListViewM3uDataItem)))
                    e.Handled = true;
            }
        }
        private void TextBoxFindEpg_Drop(object sender, DragEventArgs e)
        {
            if ((sender is TextBox tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.FIND_EPG]))
            {
                if (e.Data.GetDataPresent(typeof(string)))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_EPG, e.Data.GetData(typeof(string)) as string, tb, ListViewEPGChannel);
                else if (e.Data.GetDataPresent(typeof(ListViewM3uDataItem)))
                    pageEditorData.ItemsActions(PageEditorData.TypeActions.FIND_EPG, e.Data.GetData(typeof(ListViewM3uDataItem)) as ListViewM3uDataItem, tb, ListViewEPGChannel);
                e.Handled = true;
            }
        }
        #endregion

        #region ADD EPG FAVORITE
        private void ButtonAddFavEpg_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.ADD_EPG_FAV]))
                MenuItemEpgAddFavorite_Click(sender, e);
        }
        private void MenuItemEpgAddFavorite_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.ADD_EPG_FAV, default(string), default(string), default(string));
        }
        #endregion

        #region SAVE EPG FAVORITE
        private void ButtonSaveFavEpg_Click(object sender, RoutedEventArgs e)
        {
            if ((sender is ModernWpf.Controls.Primitives.TitleBarButton tb) &&
                (tb.Uid == controlTag[(int)PageEditorData.TypeActions.SAVE_EPG_FAV]))
                MenuItemEpgSaveFavorite_Click(sender, e);
        }
        private void MenuItemEpgSaveFavorite_Click(object sender, RoutedEventArgs e)
        {
            pageEditorData.ItemsActions(PageEditorData.TypeActions.SAVE_EPG_FAV, default(string), default(string), default(string));
        }
        #endregion

        #endregion
    }
}
