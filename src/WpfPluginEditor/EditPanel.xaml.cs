﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using M3U.Model;
using WpfPlugin.Data;

namespace WpfPlugin
{
    public partial class EditPanel : UserControl, INotifyPropertyChanged
    {
        private ListViewM3uDataItem dataItem = default;
        private Action<ListViewM3uDataItem> dataSave = (a) => { };
        private TextBlock TbAspectRatio = default;
        private TextBlock TbDeinterlace = default;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        internal ListViewM3uDataItem getDataDefault()
        {
            return new ListViewM3uDataItem(new Media("", "", 0));
        }

        public EditPanel()
        {
            dataItem = getDataDefault();
            InitializeComponent();
            DataContext = this;
        }

        #region DependencyProperty
        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); }
        }
        public static readonly new DependencyProperty WidthProperty = DependencyProperty.Register(
            "Width",
            typeof(double),
            typeof(EditPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinWidth
        {
            get { return (double)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); }
        }
        public static readonly new DependencyProperty MinWidthProperty = DependencyProperty.Register(
            "MinWidth",
            typeof(double),
            typeof(EditPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); }
        }
        public static readonly new DependencyProperty HeightProperty = DependencyProperty.Register(
            "Height",
            typeof(double),
            typeof(EditPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinHeight
        {
            get { return (double)GetValue(MinHeightProperty); }
            set { SetValue(MinHeightProperty, value); }
        }
        public static readonly new DependencyProperty MinHeightProperty = DependencyProperty.Register(
            "MinHeight",
            typeof(double),
            typeof(EditPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public new Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }
        public static readonly new DependencyProperty VisibilityProperty = DependencyProperty.Register(
            "Visibility",
            typeof(Visibility),
            typeof(EditPanel),
            new FrameworkPropertyMetadata(Visibility.Collapsed, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        #endregion

        public void SetMedia(ListViewM3uDataItem item, Action<ListViewM3uDataItem> act)
        {
            if (item == null)
                return;

            try
            {
                dataSave = act;
                dataItem = item;
                OnPropertyChanged(nameof(M3uMediaFile));
                OnPropertyChanged(nameof(M3uHttpUA));
                OnPropertyChanged(nameof(M3uInnerTitle));
                OnPropertyChanged(nameof(M3uRawTitle));
                OnPropertyChanged(nameof(M3uUrlTvg));
                OnPropertyChanged(nameof(M3uTvgShift));
                OnPropertyChanged(nameof(M3uTvgName));
                OnPropertyChanged(nameof(M3uTvgLogo));
                OnPropertyChanged(nameof(M3uTvgId));
                OnPropertyChanged(nameof(M3uGroupTitle));
                OnPropertyChanged(nameof(M3uAudioTrack));
                OnPropertyChanged(nameof(M3uMediaFileExt));
                OnPropertyChanged(nameof(M3uMediaFileMime));
                OnPropertyChanged(nameof(M3uCache));
                OnPropertyChanged(nameof(M3uChannelNumber));
                OnPropertyChanged(nameof(M3uAutoLoad));
                OnPropertyChanged(nameof(M3uRefresh));
                OnPropertyChanged(nameof(M3uDuration));
                OnPropertyChanged(nameof(M3uParserId));

                TbAspectRatio = default;
                TbDeinterlace = default;

                if (!string.IsNullOrWhiteSpace(dataItem.IfMedia.Attributes.AspectRatio))
                {
                    try
                    {
                        foreach (var i in comboAspectRatio.Items)
                            if ((i is TextBlock tb) && dataItem.IfMedia.Attributes.AspectRatio.Equals(tb.Text))
                            {
                                TbAspectRatio = tb;
                                break;
                            }
                        if (TbAspectRatio != default)
                            OnPropertyChanged(nameof(TextBoxAspectRatio));
                    }
                    catch { }
                }
                if (dataItem.IfMedia.Attributes.Deinterlace != null)
                {
                    try
                    {
                        int x = (int)dataItem.IfMedia.Attributes.Deinterlace;
                        if ((x >= 0) && (x < comboDeinterlace.Items.Count))
                        {
                            TbDeinterlace = comboDeinterlace.Items[x] as TextBlock;
                            OnPropertyChanged(nameof(TextBoxDeinterlace));
                        }
                    }
                    catch { }
                }
            }
            catch { }
        }

        public ListViewM3uDataItem GetMedia()
        {
            return dataItem;
        }

        #region Set/Get

        public string M3uMediaFile
        {
            get { return dataItem.IfMedia.MediaFile; }
            set
            {
                if (dataItem.IfMedia.MediaFile == value)
                    return;
                dataItem.IfMedia.MediaFile = value;
                OnPropertyChanged(nameof(M3uMediaFile));
            }
        }

        public string M3uHttpUA
        {
            get { return dataItem.IfMedia.HttpUA; }
            set
            {
                if (dataItem.IfMedia.HttpUA == value)
                    return;
                dataItem.IfMedia.HttpUA = value;
                OnPropertyChanged(nameof(M3uHttpUA));
            }
        }

        public string M3uInnerTitle
        {
            get { return dataItem.IfMedia.Title.InnerTitle; }
            set
            {
                if (dataItem.IfMedia.Title.InnerTitle == value)
                    return;
                dataItem.IfMedia.Title.InnerTitle = value;
                OnPropertyChanged(nameof(M3uInnerTitle));
            }
        }

        public string M3uRawTitle
        {
            get { return dataItem.IfMedia.Title.RawTitle; }
            set
            {
                if (dataItem.IfMedia.Title.RawTitle == value)
                    return;
                dataItem.IfMedia.Title.RawTitle = value;
                OnPropertyChanged(nameof(M3uRawTitle));
            }
        }

        public string M3uUrlTvg
        {
            get { return dataItem.IfMedia.Attributes.TvgUrl; }
            set
            {
                if (dataItem.IfMedia.Attributes.TvgUrl == value)
                    return;
                dataItem.IfMedia.Attributes.SetTvgUrl(value);
                OnPropertyChanged(nameof(M3uUrlTvg));
            }
        }

        public string M3uTvgShift
        {
            get { return dataItem.IfMedia.Attributes.TvgShift; }
            set
            {
                if (dataItem.IfMedia.Attributes.TvgShift == value)
                    return;
                dataItem.IfMedia.Attributes.SetTvgShift(value);
                OnPropertyChanged(nameof(M3uTvgShift));
            }
        }

        public string M3uTvgName
        {
            get { return dataItem.IfMedia.Attributes.TvgName; }
            set
            {
                if (dataItem.IfMedia.Attributes.TvgName == value)
                    return;
                dataItem.IfMedia.Attributes.SetTvgName(value);
                OnPropertyChanged(nameof(M3uTvgName));
            }
        }

        public string M3uTvgLogo
        {
            get { return dataItem.IfMedia.Attributes.TvgLogo; }
            set
            {
                if (dataItem.IfMedia.Attributes.TvgLogo == value)
                    return;
                dataItem.IfMedia.Attributes.SetTvgLogo(value);
                OnPropertyChanged(nameof(M3uTvgLogo));
            }
        }

        public string M3uTvgId
        {
            get { return dataItem.IfMedia.Attributes.TvgId; }
            set
            {
                if (dataItem.IfMedia.Attributes.TvgId == value)
                    return;
                dataItem.IfMedia.Attributes.SetTvgId(value);
                OnPropertyChanged(nameof(M3uTvgId));
            }
        }

        public string M3uGroupTitle
        {
            get { return dataItem.IfMedia.Attributes.GroupTitle; }
            set
            {
                if (dataItem.IfMedia.Attributes.GroupTitle == value)
                    return;
                dataItem.IfMedia.Attributes.SetGroupTitle(value);
                OnPropertyChanged(nameof(M3uGroupTitle));
            }
        }

        public string M3uAudioTrack
        {
            get { return dataItem.IfMedia.Attributes.AudioTrack; }
            set
            {
                if (dataItem.IfMedia.Attributes.AudioTrack == value)
                    return;
                dataItem.IfMedia.Attributes.SetAudioTrack(value);
                OnPropertyChanged(nameof(M3uAudioTrack));
            }
        }

        public TextBlock TextBoxAspectRatio
        {
            get { return TbAspectRatio; }
            set
            {
                if (value is TextBlock tb)
                {
                    TbAspectRatio = tb;

                    if ((tb == null) ||
                        string.IsNullOrWhiteSpace(tb.Text) ||
                        tb.Text.Equals(dataItem.IfMedia.Attributes.AspectRatio))
                        return;
                    dataItem.IfMedia.Attributes.SetAspectRatio(tb.Text.ToString());
                    OnPropertyChanged(nameof(TextBoxAspectRatio));
                }
            }
        }

#if FULLSETTER
        public string M3uAspectRatio
        {
            get { return dataItem.IfMedia.Attributes.AspectRatio; }
            set
            {
                if (dataItem.IfMedia.Attributes.AspectRatio == value)
                    return;
                dataItem.IfMedia.Attributes.SetAspectRatio(value);
                OnPropertyChanged(nameof(M3uAspectRatio));
            }
        }
#endif

        public string M3uMediaFileExt
        {
            get { return dataItem.IfMedia.MediaFileExt; }
            set
            {
                if (dataItem.IfMedia.MediaFileExt == value)
                    return;
                dataItem.IfMedia.MediaFileExt = value;
                OnPropertyChanged(nameof(M3uMediaFileExt));
            }
        }

        public string M3uMediaFileMime
        {
            get { return dataItem.IfMedia.MediaFileMime; }
            set
            {
                if (dataItem.IfMedia.MediaFileMime == value)
                    return;
                dataItem.IfMedia.MediaFileMime = value;
                OnPropertyChanged(nameof(M3uMediaFileMime));
            }
        }

        ///

        public int? M3uCache
        {
            get { return dataItem.IfMedia.Attributes.Cache; }
            set
            {
                if (dataItem.IfMedia.Attributes.Cache == value)
                    return;
                dataItem.IfMedia.Attributes.SetCache(value);
                OnPropertyChanged(nameof(M3uCache));
            }
        }

        public int? M3uChannelNumber
        {
            get { return dataItem.IfMedia.Attributes.ChannelNumber; }
            set
            {
                if (dataItem.IfMedia.Attributes.ChannelNumber == value)
                    return;
                dataItem.IfMedia.Attributes.SetChannelNumber(value);
                OnPropertyChanged(nameof(M3uChannelNumber));
            }
        }

        public TextBlock TextBoxDeinterlace
        {
            get { return TbDeinterlace; }
            set
            {
                if (value is TextBlock tb)
                {
                    TbDeinterlace = tb;

                    if ((tb == null) ||
                        string.IsNullOrWhiteSpace(tb.Text))
                        return;
                    try
                    {
                        int x = comboDeinterlace.Items.IndexOf(tb);
                        dataItem.IfMedia.Attributes.SetDeinterlace(x);
                    }
                    catch { }
                    OnPropertyChanged(nameof(TextBoxDeinterlace));
                }
            }
        }

#if FULLSETTER
        public int? M3uDeinterlace
        {
            get { return dataItem.IfMedia.Attributes.Deinterlace; }
            set
            {
                if (dataItem.IfMedia.Attributes.Deinterlace == value)
                    return;
                dataItem.IfMedia.Attributes.SetDeinterlace(value);
                OnPropertyChanged(nameof(M3uDeinterlace));
            }
        }
#endif

        public int? M3uAutoLoad
        {
            get { return dataItem.IfMedia.Attributes.AutoLoad; }
            set
            {
                if (dataItem.IfMedia.Attributes.AutoLoad == value)
                    return;
                dataItem.IfMedia.Attributes.SetM3UAutoLoad(value);
                OnPropertyChanged(nameof(M3uAutoLoad));
            }
        }

        public int? M3uRefresh
        {
            get { return dataItem.IfMedia.Attributes.Refresh; }
            set
            {
                if (dataItem.IfMedia.Attributes.Refresh == value)
                    return;
                dataItem.IfMedia.Attributes.SetRefresh(value);
                OnPropertyChanged(nameof(M3uRefresh));
            }
        }

        public long M3uDuration
        {
            get { return (long)dataItem.IfMedia.Duration; }
            set
            {
                if (dataItem.IfMedia.Duration == value)
                    return;
                dataItem.IfMedia.Duration = value;
                OnPropertyChanged(nameof(M3uDuration));
            }
        }

        public int M3uParserId
        {
            get { return (dataItem.IfMedia.ParserId == null) ? -1 : (int)dataItem.IfMedia.ParserId; }
            set
            {
                if (dataItem.IfMedia.ParserId == value)
                    return;
                dataItem.IfMedia.ParserId = value;
                OnPropertyChanged(nameof(M3uParserId));
            }
        }
        #endregion

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            dataSave(null);
            dataItem = getDataDefault();
        }

        private void ButtonSaveAndClose_Click(object sender, RoutedEventArgs e)
        {
            dataSave(dataItem);
            dataItem = getDataDefault();
        }

        private void ValidateTextIsNumber(object sender, TextCompositionEventArgs a)
        {
            a.Handled = !uint.TryParse(a.Text, out _);
        }
    }
}
