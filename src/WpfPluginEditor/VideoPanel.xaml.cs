﻿/* Copyright (c) 2021 WpfPluginEditor, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WpfPlugin.Data;

namespace WpfPlugin
{
    public partial class VideoPanel : UserControl, INotifyPropertyChanged
    {
        private bool _IsMediaWait = default,
                     _IsPlayEnableActions = default;
        private string _MediaError = default;

        public delegate void PlayerStateEvent(MediaState m, MediaPlay.PlayerStage s, object obj);
        public event PlayerStateEvent PlayerStateHandler = delegate { };

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public VideoPanel()
        {
            InitializeComponent();
            MediaPlayer = new MediaPlay(MediaView, errorLogCallBack);
            MediaPlayer.PlayerStateHandler += player_StateChanged;
            MediaPlayer.PlayerErrorHandler += player_ErrorMessage;
            DataContext = this;
        }

        #region DependencyProperty

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); OnPropertyChanged(nameof(ModeViewVisibility)); }
        }
        public static readonly new DependencyProperty WidthProperty = DependencyProperty.Register(
            "Width",
            typeof(double),
            typeof(VideoPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinWidth
        {
            get { return (double)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); }
        }
        public static readonly new DependencyProperty MinWidthProperty = DependencyProperty.Register(
            "MinWidth",
            typeof(double),
            typeof(VideoPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); }
        }
        public static readonly new DependencyProperty HeightProperty = DependencyProperty.Register(
            "Height",
            typeof(double),
            typeof(VideoPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinHeight
        {
            get { return (double)GetValue(MinHeightProperty); }
            set { SetValue(MinHeightProperty, value); }
        }
        public static readonly new DependencyProperty MinHeightProperty = DependencyProperty.Register(
            "MinHeight",
            typeof(double),
            typeof(VideoPanel),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public new Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }
        public static readonly new DependencyProperty VisibilityProperty = DependencyProperty.Register(
            "Visibility",
            typeof(Visibility),
            typeof(VideoPanel),
            new FrameworkPropertyMetadata(Visibility.Collapsed, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        #endregion

        public MediaPlay MediaPlayer { get; private set; } = default;
        private double _MediaViewWidth = 0;
        public double MediaViewWidth
        {
            get { return _MediaViewWidth; }
            set
            {
                if (_MediaViewWidth == value)
                    return;
                MediaView.Width = _MediaViewWidth = value;
                OnPropertyChanged(nameof(MediaViewWidth));
            }
        }

        public string PlayUrl
        {
            get
            {
                return ((MediaPlayer == default) || (MediaPlayer.PlayMedia == default)) ?
                  default : MediaPlayer.PlayMedia.MediaFile;
            }
        }
        private string PlayChannelSaved { get; set; } = default;
        public string PlayChannel
        {
            get
            {
                return ((MediaPlayer == default) || (MediaPlayer.PlayMedia == null)) ?
                  default : MediaPlayer.PlayMedia.Title.InnerTitle;
            }
        }
        public MediaState PlayStatus
        {
            get { return (MediaPlayer == default) ? MediaState.Manual : MediaPlayer.PlayerStatus; }
        }

        public bool IsMediaWait
        {
            get { return _IsMediaWait; }
            private set
            {
                if (_IsMediaWait == value)
                    return;
                _IsMediaWait = value;
                OnPropertyChanged(nameof(IsMediaWait));
            }
        }

        public string MediaError
        {
            get { return _MediaError; }
            private set
            {
                if (_MediaError == value)
                    return;
                _MediaError = value;
                OnPropertyChanged(nameof(MediaError));
                OnPropertyChanged(nameof(IsMediaError));
            }
        }
        public bool IsMediaError
        {
            get { return !string.IsNullOrWhiteSpace(MediaError); }
        }
        private bool _IsMediaErrorWrite = false;
        public bool IsMediaErrorWrite
        {
            get { return _IsMediaErrorWrite; }
            private set
            {
                _IsMediaErrorWrite = !_IsMediaErrorWrite;
                OnPropertyChanged(nameof(IsMediaErrorWrite));
            }
        }

        public bool IsPlayEnableActions
        {
            get { return _IsPlayEnableActions; }
            private set
            {
                if (_IsPlayEnableActions == value)
                    return;
                _IsPlayEnableActions = value;
                OnPropertyChanged(nameof(IsPlayEnableActions));
            }
        }

        public bool ModeViewVisibility
        {
            get { return (Width > 150.0) || (ActualWidth > 150.0); }
        }

        private void errorLogCallBack(string[] s, Exception e)
        {
            if (!IsMediaErrorWrite)
                return;
            if (e != null)
                player_ErrorMessage(e);
            else if (s != null)
                player_ErrorLogTask(s);
        }

        private void player_ErrorMessage(Exception e)
        {
            if (e == null)
                return;
            MediaError = e.Message;
        }

        private async void player_ErrorLogTask(string[] s)
        {
            await Task.Run(() =>
            {
                try
                {
                    string path = System.IO.Path.Combine(
                        System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                        "TV-Channels-Log");

                    System.IO.DirectoryInfo d = new System.IO.DirectoryInfo(path);
                    if (d == null)
                        return;
                    if (!d.Exists)
                        d.Create();

                    System.IO.File.WriteAllText(
                        System.IO.Path.Combine(path, $"{PlayChannelSaved}.log"),
                        string.Join("\n", s));
                }
                catch { }
            });
        }

        private void player_StateChanged(MediaState state, MediaPlay.PlayerStage stage, object obj)
        {
            switch (state)
            {
                case MediaState.Close:
                    {
                        break;
                    }
                case MediaState.Manual:
                    {
                        IsPlayEnableActions = false;
                        break;
                    }
                case MediaState.Play:
                    {
                        MediaError = default;
                        IsPlayEnableActions = true;
                        OnPropertyChanged(nameof(MediaError));
                        OnPropertyChanged(nameof(IsMediaError));
                        OnPropertyChanged(nameof(ModeViewVisibility));
                        break;
                    }
                case MediaState.Pause:
                    {
                        IsPlayEnableActions = true;
                        break;
                    }
                case MediaState.Stop:
                    {
                        IsPlayEnableActions = false;
                        break;
                    }
            }
            switch (stage)
            {
                case MediaPlay.PlayerStage.NONE:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.BUFFERED:
                    {
                        IsMediaWait = true;
                        break;
                    }
                case MediaPlay.PlayerStage.BUFFERED_END:
                    {
                        IsMediaWait = false;
                        break;
                    }
                case MediaPlay.PlayerStage.CMD_ERROR:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.ADD_MEDIA:
                    {
                        OnPropertyChanged(nameof(PlayUrl));
                        OnPropertyChanged(nameof(PlayChannel));
                        break;
                    }
                case MediaPlay.PlayerStage.PLAY_NAME:
                    {
                        if ((obj != null) && (obj is string s))
                            PlayChannelSaved = s;
                        break;
                    }
                case MediaPlay.PlayerStage.PLAYS:
                    {
                        IsPlayEnableActions = true;
                        break;
                    }
                case MediaPlay.PlayerStage.WAIT:
                    {
                        break;
                    }
                case MediaPlay.PlayerStage.END_PLAY:
                    {
                        IsMediaWait = false;
                        IsPlayEnableActions = false;
                        MediaPlayer.SetEmpty();
                        OnPropertyChanged(nameof(PlayUrl));
                        OnPropertyChanged(nameof(PlayChannel));
                        break;
                    }
            }
            OnPropertyChanged(nameof(PlayStatus));
        }

        private void ButtonScreenShot_Click(object sender, RoutedEventArgs e)
        {
            if (MediaPlayer == default)
                return;
            MediaPlayer.SendCommand(MediaState.Close, PlayStatus);
        }

        private void ButtonPause_Click(object sender, RoutedEventArgs e)
        {
            if (MediaPlayer == default)
                return;
            MediaPlayer.SendCommand(MediaState.Pause, PlayStatus);
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            if (MediaPlayer == default)
                return;
            MediaPlayer.SendCommand(MediaState.Stop, PlayStatus);
        }

        private void ButtonVideoPlace_Click(object sender, RoutedEventArgs e)
        {
            PlayerStateHandler.Invoke(MediaState.Close, MediaPlay.PlayerStage.VIDEO_LEFT, null);
        }

        private void BtnOriginSize_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.CaptureArea = MediaPlay.PlayerStage.CAP_ORIGINAL_SIZE;
        }
        private void BtnAreaTopLeftt_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.CaptureArea = MediaPlay.PlayerStage.CAP_AREA_TOP_LEFT;
        }
        private void BtnAreaTopRight_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.CaptureArea = MediaPlay.PlayerStage.CAP_AREA_TOP_RIGHT;
        }
        private void BtnAreaBottomLeftt_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.CaptureArea = MediaPlay.PlayerStage.CAP_AREA_BOTTOM_LEFT;
        }
        private void BtnAreaBottomRight_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.CaptureArea = MediaPlay.PlayerStage.CAP_AREA_BOTTOM_RIGHT;
        }

        private void ButtonVideoLog_Click(object sender, RoutedEventArgs e)
        {
            IsMediaErrorWrite = true;
        }

        private void Slider_VolumeChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if ((MediaPlayer == default) || (e == null) || (e.NewValue == e.OldValue))
                return;
            MediaPlayer.SetVolume((double)e.NewValue);
        }
    }
}
