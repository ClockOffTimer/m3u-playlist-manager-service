@echo off
IF EXIST "AudioMonitorKey.keystore" (
  ECHO AudioMonitorKey.keystore Exists, continue..
) ELSE (
  ECHO Build AudioMonitorKey.keystore:
  rem keytool -genkeypair -v -keystore AudioMonitorKey.keystore -alias AudioMonitorKey -keyalg RSA -keysize 2048 -validity 10000
)
ECHO Password: 123456
keytool -list -keystore AudioMonitorKey.keystore
