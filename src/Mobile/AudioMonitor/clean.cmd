@echo off

rmdir /S/Q AudioMonitor\bin
rmdir /S/Q AudioMonitor\obj
rmdir /S/Q AudioMonitor.Android\bin
rmdir /S/Q AudioMonitor.Android\obj
rmdir /S/Q AudioMonitor.iOS\bin
rmdir /S/Q AudioMonitor.iOS\obj
rmdir /S/Q AudioMonitor.UWP\bin
rmdir /S/Q AudioMonitor.UWP\obj

rmdir /S/Q LightAudioPlayer\AudioPlayer.Base\bin
rmdir /S/Q LightAudioPlayer\AudioPlayer.Base\obj
rmdir /S/Q LightAudioPlayer\AudioPlayer.Android\bin
rmdir /S/Q LightAudioPlayer\AudioPlayer.Android\obj

del /F/Q AudioMonitor.Android\Resources\Resource.designer.cs
del /F/Q LightAudioPlayer\AudioPlayer.Android\Resources\Resource.designer.cs