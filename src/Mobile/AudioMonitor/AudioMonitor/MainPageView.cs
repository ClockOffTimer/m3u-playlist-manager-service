﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define NOTSTOPONDESELECT

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using AudioMonitor.Data;
using AudioMonitor.Data.Events;
using AudioMonitor.Properties;
using GoogleCast;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.CommunityToolkit.UI.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AudioMonitor
{
    public class MainPageView : BaseEventChanger<BaseAction>
    {
        private readonly object _lock = new object();

        #region Command declare/init
        public ICommand ChannelPlayCommand { get; private set; }
        public ICommand ChannelStopCommand { get; private set; }
        public ICommand ChannelPauseCommand { get; private set; }
        public ICommand ChannelReLoadCommand { get; private set; }
        public ICommand SettingsMutedCommand { get; private set; }
        public ICommand EqSelectorCommand { get; private set; }
        public ICommand EqSaveServCommand { get; private set; }
        public ICommand StateErrorCommand { get; private set; }
        public ICommand StateServCommand { get; private set; }
        public ICommand ShareUrlCommand { get; private set; }
        public ICommand CastDeviceCommand { get; private set; }
        public ICommand PanelBackCommand { get; private set; }
        public Command<PlayListItem> ChannelSelectedCommand { get; private set; }

        private bool OnceCastDeviceCommand = false;
        private bool OnceEqSavePresetCommand = false;

        public void InitializeCommand()
        {
            ChannelSelectedCommand = new Command<PlayListItem>(async (a) => await Task.FromResult(OnChannelSelected(a)));
            ChannelReLoadCommand = new Command(async () => await CommandRun(OnReLoadCommand, false), () => IsSettings && !IsRefresh);
            ChannelPlayCommand = new Command(async () => await CommandRun(OnPlayCommand, true), () => IsChannelSelected);
            ChannelStopCommand = new Command(async () => await CommandRun(OnStopCommand, true), () => IsChannelSelected);
            ChannelPauseCommand = new Command(async () => await CommandRun(OnPauseCommand, true), () => IsChannelSelected);
            EqSaveServCommand = new Command(async () => await CommandRun(OnEqSavePresetCommand, false), () => !IsEqLocalCtrl);
            EqSelectorCommand = new Command(async () => await CommandRun(OnEqSelectCommand, true));
            SettingsMutedCommand = new Command(async () => await CommandRun(OnMutedCommand, true));
            StateErrorCommand = new Command(async () => await CommandRun(OnStateErrorCommand, true));
            StateServCommand = new Command(async () => await CommandRun(OnLoadStateCommand, false));
            ShareUrlCommand = new Command(async () => await OnShareUrlCommand(), () => IsChannelSelected);
            CastDeviceCommand = new Command(async () => await OnCastDeviceCommand(), () => IsChannelSelected);
            PanelBackCommand = new Command(async () => await CommandRun(OnPanelBackCommand, true), () => _uiStateRightPrev != LayoutState.None);
        }

        private async Task OnShareUrlCommand()
        {
            if (UiChannelSelected != null)
                await Share.RequestAsync(new ShareTextRequest
                {
                    Uri = UiChannelSelected.Url,
                    Title = UiChannelSelected.Tag
                });
        }
        private bool OnReLoadCommand() {
            NotifySends(BaseAction.ReloadCall, IsSettings);
            Device.StartTimer(TimeSpan.FromSeconds(60), () => {
                if (PlayerState != StateAudioPlayerId.Playing)
                    Device.BeginInvokeOnMainThread(() => {
                        if (IsRefresh) IsRefresh = false;
                        if (IsLoading) IsLoading = false;
                        if (IsPlayerWait) IsPlayerWait = false;
                    });
                return false;
            });
            return true;
        }
        private async Task OnCastDeviceCommand()
        {
            if (OnceCastDeviceCommand)
                return;
            OnceCastDeviceCommand = true;
            try
            {
                if (UiCastChannels.Count == 0)
                {
                    var r = await new DeviceLocator().FindReceiversAsync();
                    UiCastChannels = new ObservableCollection<IReceiver>(r);
                }
                if (UiCastChannels.Count > 0)
                    UiStateRight = LayoutState.Success;
                else
                    UiStateError = Resources.ChromeCastNotFound;
            }
            catch { }
            finally { OnceCastDeviceCommand = false; }
        }
        private bool OnEqSavePresetCommand()
        {
            if (OnceEqSavePresetCommand)
                return false;
            OnceEqSavePresetCommand = true;
            try
            {
                Device.BeginInvokeOnMainThread(() => {
                    if (CheckSlidersValues(UiChannelSelected))
                        SaveSlidersValues(UiChannelSelected, IsEqLocalCtrl);
                });
                NotifySends(BaseAction.EqSavePresetCmd, IsEqLocalCtrl);
            }
            catch { }
            finally { OnceEqSavePresetCommand = false; }
            return true;
        }
        private bool OnStateErrorCommand()
        {
            _uiStateError = string.Empty;
            if (UiStateRight == LayoutState.Error)
                UiStateRight = _uiStateRightPrev;
            OnPropertyChanged(nameof(UiStateError), nameof(IsUiStateError));
            return true;
        }
        private bool OnPlayCommand()  {
            NotifySends(BaseAction.PlayerPlay, IsPlayerPlay);
            IsPlayerWait = true; IsPlayerPlay = false;
            return true;
        }
        private bool OnStopCommand()  {
            NotifySends(BaseAction.PlayerStop, IsPlayerStop);
            IsPlayerWait = IsPlayerPause = IsPlayerStop = IsPlayerPlay = false;
            return true;
        }
        private bool OnPauseCommand() {
            NotifySends(BaseAction.PlayerPause, IsPlayerPause);
            IsPlayerWait = IsPlayerPause = false;
            return true;
        }
        private bool OnChannelSelected(PlayListItem pli) {
            IsPlayerWait = true; IsPlayerPlay = false;
            UiChannelSelected = pli;
            return true;
        }
        private bool OnPanelBackCommand()
        {
            if (_uiStateRightPrev != LayoutState.None)
                UiStateRight = _uiStateRightPrev;
            return true;
        }
        private bool OnLoadStateCommand()
        {
            NotifySends(BaseAction.LoadingStat, IsSettings);
            return true;
        }
        private bool OnEqSelectCommand() { if (IsChannelSelected) IsEqLocalCtrl = !IsEqLocalCtrl; return true; }
        private bool OnMutedCommand() { IsMuteCtrl = !IsMuteCtrl; return true; }
        #endregion

        public MainPageView()
        {
            for (int i = 0; i < StaticSettings.EqCount; i++)
            {
                _eqChannelsLocal.Add(new ChannelValues(
                    _valuesData.EqRanges[ValuesData.Local][ValuesData.Value],
                    _valuesData.EqRanges[ValuesData.Local][ValuesData.Min],
                    _valuesData.EqRanges[ValuesData.Local][ValuesData.Max],
                    _eqChannelsLocal.Count)
                    .AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeEq));
                _eqChannelsServ.Add(new ChannelValues(
                    _valuesData.EqRanges[ValuesData.Serv][ValuesData.Value],
                    _valuesData.EqRanges[ValuesData.Serv][ValuesData.Min],
                    _valuesData.EqRanges[ValuesData.Serv][ValuesData.Max],
                    _eqChannelsServ.Count));
            }
            _volumeChannel.Value = _valuesData.VolRanges[ValuesData.Local][ValuesData.Value];
            _volumeChannel.Min = _valuesData.VolRanges[ValuesData.Local][ValuesData.Min];
            _volumeChannel.Max = _valuesData.VolRanges[ValuesData.Local][ValuesData.Max];
            _volumeChannel.AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeVolume);
            _balanceChannel.AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeBalance);
            _virtualizeEfChannel.AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeVirtualize);
            _basBoostEfChannel.AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeBasBoost);
            _loudnessEfChannel.AddEvent(EqChannel_PropertyChanged, ChannelValuesId.TypeLoudness);

            base.GetBaseEvent.SetCallerPlace(nameof(MainPageView));
            InitializeCommand();
        }

        public void Once() { NotifySends(BaseAction.None, null); }
        public void HistoryClear() { _ = GetBaseEvent.HistoryClear(); }

        #region public Getter/Setter
        #region UI channels
        public ObservableCollection<PlayListItem> _uiChannels = new ObservableCollection<PlayListItem>();
        public ObservableCollection<PlayListItem> UiChannels
        {
            get => _uiChannels;
            set {
                if (_uiChannels == value)
                    return;

                lock (_lock)
                {
                    _uiChannels.Clear();
                    if (value != null)
                        foreach (var c in value)
                            _uiChannels.Add(c);
                }

                UiChannelSelected = default;
                IsRefresh = false;
                
                NotifySends(BaseAction.PlayerChannelsChange, value);
                OnPropertyChanged(
                    nameof(IsLoad),
                    nameof(UiChannels),
                    nameof(UiStateLeft),
                    nameof(UiStateLeftName),
                    nameof(HeightLeftPanel));
            }
        }
        private PlayListItem _uiChannelSelected = default;
        public PlayListItem UiChannelSelected
        {
            get => _uiChannelSelected;
            set {
#               if NOTSTOPONDESELECT
                if (value == default)
                    return;
#               endif
                if (_uiChannelSelected == value)
                    return;

                ChangePlayListItem(_uiChannelSelected, value, IsEqLocalCtrl);

                lock (_lock)
                    _uiChannelSelected = value;
                Duration = TimeSpan.Zero;

                if (_uiChannelSelected != null)
                {
                    DeviceName = _uiChannelSelected.Tag;
                    UiStateRight = IsEqLocalCtrl ? LayoutState.Loading : LayoutState.Saving;

                    NotifySends(BaseAction.VolumeChannelUpdate,
                        new Tuple<int, float, float, bool>(VolumeChannel.Id,
                            _uiChannelSelected.VolumeLocal, BalanceChannel.Value, IsEqLocalCtrl));
                    NotifySends(BaseAction.EqChannelUpdate,
                        new Tuple<int, float[], bool>(0,
                            _uiChannelSelected.EqualizersLocal, IsEqLocalCtrl));
                }
                else
                {
                    UiStateRight = LayoutState.Empty;
                    DeviceName = string.Empty;
                }

                NotifySends(BaseAction.ChannelSelected, value);
                OnPropertyChanged(
                    nameof(UiChannelSelected),
                    nameof(IsChannelSelected),
                    nameof(EqChannelsServ),
                    nameof(IsEqLocalCtrl),
                    nameof(IsEqEnableCtrl),
                    nameof(IsEfControlEnable),
                    nameof(IsEqControlsEnable),
                    nameof(IsEqControlPreset),
                    nameof(IsEfVirtualizeEnable),
                    nameof(IsEfAutoGain),
                    nameof(IsEfBasBoost),
                    nameof(IsEfLoudness),
                    nameof(VolumeChannel),
                    nameof(IsPlayerPlay),
                    nameof(IsPlayerStop),
                    nameof(IsPlayerPause));
            }
        }
        public int UiChannelSelectedIndex
        {
            get => (UiChannelSelected == null) ? -1 : UiChannelSelected.Id;
            set
            {
                if ((value < 0) || (value >= UiChannels.Count))
                    return;
                UiChannelSelected = UiChannels[value];
                OnPropertyChanged(nameof(UiChannelSelectedIndex));
            }
        }
        #endregion

        #region CAST channels
        private ObservableCollection<IReceiver> _uiCastChannels = new ObservableCollection<IReceiver>();
        public  ObservableCollection<IReceiver> UiCastChannels
        {
            get => _uiCastChannels;
            set
            {
                if (value == null)
                    return;
                _uiCastChannels.Clear();
                foreach (var channel in value)
                    _uiCastChannels.Add(channel);
                OnPropertyChanged(nameof(UiCastChannels), nameof(HeightRightPanel), nameof(HeightListCast));
            }
        }
        public IReceiver UiCastChannelSelected
        {
            set
            {
                if ((value == null) || (UiChannelSelected == null))
                    return;
                NotifySends(
                    BaseAction.CastSelected, new Tuple<IReceiver, PlayListItem>(value, UiChannelSelected));
            }
        }
        #endregion

        #region Service using STAT channels
        private ObservableCollection<AudioStatLite> _uiAudioStatChannels = new ObservableCollection<AudioStatLite>();
        public ObservableCollection<AudioStatLite> UiAudioStatChannels
        {
            get => _uiAudioStatChannels;
            set {
                if (value == null)
                    return;
                _uiAudioStatChannels = value;
                OnPropertyChanged(nameof(UiAudioStatChannels));
            }
        }
        public AudioStatsLite AudioStatsList
        {
            set {
                if (value == null)
                    return;
                _uiAudioStatChannels.Clear();
                foreach (var channel in value.Devices)
                    _uiAudioStatChannels.Add(channel);
                if (_uiAudioStatChannels.Count == 0)
                {
                    UiStateError = Resources.ServStatListEmpty;
                    return;
                }
                UiStateRight = LayoutState.Empty;
                OnPropertyChanged(nameof(UiAudioStatChannels), nameof(HeightRightPanel), nameof(HeightListStat));
            }
        }
        #endregion

        #region EQ
        #region Sliders
        private ObservableCollection<ChannelValues> _eqChannelsServ = new ObservableCollection<ChannelValues>();
        public ObservableCollection<ChannelValues> EqChannelsServ
        {
            get => _eqChannelsServ;
            set => OnPropertyChanged(nameof(EqChannelsServ));
        }
        private ObservableCollection<ChannelValues> _eqChannelsLocal = new ObservableCollection<ChannelValues>();
        public ObservableCollection<ChannelValues> EqChannelsLocal
        {
            get => _eqChannelsLocal;
            set => OnPropertyChanged(nameof(EqChannelsLocal));
        }
        private ValuesData _valuesData = new ValuesData(true);
        public IPlayerEqSettings SlidersSettings
        {
            set
            {
                if ((value == null) || value.IsEmpty)
                    return;

                try
                {
                    _valuesData.Copy(value);
                    SetEqSettings(value);
                    OnPropertyChanged(nameof(EqChannelsLocal));
                    _valuesData.Save();

                } catch (Exception ex) { LogWriter.Debug<MainPageView>(nameof(SlidersSettings), ex); }
                OnPropertyChanged(nameof(HeightRightPanel), nameof(HeightListLocalEq));
            }
        }
        public float [] EqChannelsLocalValues
        {
            set {
                if ((value == null) || value.Length == 0)
                    return;
                for (int i = 0; i < value.Length; i++)
                    EqChannelsLocal[i].Value = value[i];
                OnPropertyChanged(nameof(EqChannelsLocal));
            }
        }
        #endregion
        private bool _isEqEnableCtrl = true;
        public bool IsEqEnableCtrl
        {
            get => _isEqEnableCtrl;
            set
            {
                if (_isEqEnableCtrl == value)
                    return;
                _isEqEnableCtrl = value;
                NotifySends(BaseAction.EqEnableChange, value);
                OnPropertyChanged(
                    nameof(IsEqEnableCtrl),
                    nameof(IsEqControlsEnable),
                    nameof(IsEfControlEnable),
                    nameof(IsEfLoudness),
                    nameof(IsEfBasBoost),
                    nameof(IsEfVirtualizeEnable),
                    nameof(IsEfAutoGain));
            }
        }
        private bool _isEqLocalCtrl = true;
        public bool IsEqLocalCtrl
        {
            get => _isEqLocalCtrl;
            set
            {
                if (_isEqLocalCtrl == value)
                    return;
                _isEqLocalCtrl = value;

                if (value)
                    UiStateRight = LayoutState.Loading;
                else
                    UiStateRight = LayoutState.Saving;

                OnPropertyChanged(
                    nameof(IsEqLocalCtrl), nameof(IsEqControlsEnable),
                    nameof(IsEqControlPreset), nameof(HeightRightPanel),
                    _isEqLocalCtrl ? nameof(HeightListLocalEq) : nameof(HeightListServEq)
                    );
                NotifySends(BaseAction.EqLocalChange, value);
                ChangeEqView(UiChannelSelected, value);
            }
        }
        public bool IsEqControlsEnable
        {
            get => IsChannelSelected && IsEqEnableCtrl;
        }
        public bool IsEqControlPreset
        {
            get => IsChannelSelected && IsEqLocalCtrl && (_valuesData.PresetCount[ValuesData.Local] > 0);
        }
        #endregion

        #region VOLUME/BALANCE/VIRTUALIZE/LOUDNESS/BASBOOST
        private ChannelValues _volumeChannel = new ChannelValues(14.0f, 0.0f, 15.0f, 0);
        public ChannelValues VolumeChannel
        {
            get => _volumeChannel;
            set {
                if (value == null)
                {
                    _volumeChannel.Default();
                    return;
                }
                if (_volumeChannel == value)
                    return;

                _volumeChannel.Copy(value);
                OnPropertyChanged(nameof(VolumeChannel));
            }
        }
        private ChannelValues _balanceChannel = new ChannelValues(0.0f,-1.0f,1.0f,0);
        public ChannelValues BalanceChannel
        {
            get => _balanceChannel;
            set {
                if (value == null)
                {
                    _balanceChannel.Default();
                    return;
                }
                if (_balanceChannel == value)
                    return;

                _balanceChannel.Copy(value);
                OnPropertyChanged(nameof(BalanceChannel));
            }
        }
        public bool IsEfControlEnable
        {
            get => IsChannelSelected && IsEqEnableCtrl;
        }
        private ChannelValues _virtualizeEfChannel = new ChannelValues(500.0f, 0.0f, 1000.0f, 0);
        public ChannelValues EfVirtualizeChannel
        {
            get => _virtualizeEfChannel;
            set {
                if (value == null)
                {
                    _virtualizeEfChannel.Default();
                    return;
                }
                if (_virtualizeEfChannel == value)
                    return;

                _virtualizeEfChannel.Copy(value);
                OnPropertyChanged(nameof(EfVirtualizeChannel));
            }
        }
        private bool _isEfVirtualizeEnable = false;
        public bool IsEfVirtualizeEnable
        {
            get => IsEfControlEnable && _isEfVirtualizeEnable;
            set {
                if (_isEfVirtualizeEnable == value)
                    return;
                _isEfVirtualizeEnable = value;
                OnPropertyChanged(nameof(IsEfVirtualizeEnable));
            }
        }
        private ChannelValues _loudnessEfChannel = new ChannelValues(500.0f, 0.0f, 1000.0f, 0);
        public ChannelValues EfLoudnessChannel
        {
            get => _loudnessEfChannel;
            set
            {
                if (value == null)
                {
                    _loudnessEfChannel.Default();
                    return;
                }
                if (_loudnessEfChannel == value)
                    return;

                _loudnessEfChannel.Copy(value);
                OnPropertyChanged(nameof(EfLoudnessChannel));
            }
        }
        private bool _isEfLoudness = false;
        public bool IsEfLoudness
        {
            get => IsEfControlEnable && _isEfLoudness;
            set
            {
                if (_isEfLoudness == value)
                    return;
                _isEfLoudness = value;
                OnPropertyChanged(nameof(IsEfLoudness));
            }
        }
        private ChannelValues _basBoostEfChannel = new ChannelValues(500.0f, 0.0f, 1000.0f, 0);
        public ChannelValues EfBasBoostChannel
        {
            get => _basBoostEfChannel;
            set
            {
                if (value == null)
                {
                    _basBoostEfChannel.Default();
                    return;
                }
                if (_basBoostEfChannel == value)
                    return;

                _basBoostEfChannel.Copy(value);
                OnPropertyChanged(nameof(EfBasBoostChannel));
            }
        }
        private bool _isEfbasBoost = false;
        public bool IsEfBasBoost
        {
            get => IsEfControlEnable && _isEfbasBoost;
            set
            {
                if (_isEfbasBoost == value)
                    return;
                _isEfbasBoost = value;
                OnPropertyChanged(nameof(IsEfBasBoost));
            }
        }
        private bool _isEfautoGain = false;
        public bool IsEfAutoGain
        {
            get => _isEfautoGain;
            set
            {
                if (_isEfbasBoost == value)
                    return;
                _isEfautoGain = value;
                NotifySends(BaseAction.EfAutoGainChange, nameof(IsEfAutoGain), value);
            }
        }
        #endregion

        #region STATES
        public string PlayerStateName { get => PlayerState.ToString(); }
        private StateAudioPlayerId _playerState = StateAudioPlayerId.Stopped;
        public StateAudioPlayerId PlayerState
        {
            get => _playerState;
            set {
                do {
                    if (_playerState == value)
                        break;

                    if ((value == StateAudioPlayerId.Playing) && (_playTime == 0))
                        Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                            if (!IsPlayerPlay && IsPlayerPause)
                                Duration = TimeSpan.FromSeconds(_playTime);
                            bool b = (!IsPlayerPlay || !IsPlayerPause);
                            _playTime = b ? (_playTime + 1) : 0;
                            return b;
                        });

                    if (_playerState == StateAudioPlayerId.Loading)
                        switch (value)
                        {
                            case StateAudioPlayerId.Loaded:
                            case StateAudioPlayerId.Stopped:
                            case StateAudioPlayerId.Paused: break;
                            default: return;
                        }

                    _playerState = value;
                    OnPropertyChanged(nameof(PlayerState), nameof(PlayerStateName));

                    if (value == StateAudioPlayerId.Loading)
                    {
                        IsPlayerWait = true;
                        IsPlayerPlay = false;
                    }
                    else if (value == StateAudioPlayerId.Stopped)
                        IsPlayerWait = IsPlayerPause = IsPlayerStop = false;
                    else if ((value == StateAudioPlayerId.Playing) ||
                             (value == StateAudioPlayerId.Loaded)  ||
                             (value == StateAudioPlayerId.Paused))
                        IsPlayerWait = false;

                } while (false);
                OnPropertyChanged(
                    nameof(PlayerState),
                    nameof(PlayerStateName),
                    nameof(IsPlayerPlay),
                    nameof(IsPlayerPause),
                    nameof(IsPlayerStop),
                    nameof(IsPlayerWait));
            }
        }
        public string ServiceStateName
        {
            get { 
                string s = ServiceState.ToString();
                if (s.StartsWith("Widget"))
                    return s.Replace("Widget", "W:").Replace("ReloadList", "Reload");
                return s;
            }
        }
        private StateAudioPlayerId _serviceState = StateAudioPlayerId.StopService;
        public StateAudioPlayerId ServiceState
        {
            get => _serviceState;
            set
            {
                if (_serviceState == value)
                    return;
                if (value == StateAudioPlayerId.StopService)
                {
                    PlayerState = StateAudioPlayerId.Stopped;
                    IsPlayerWait = false;
                }
                _serviceState = value;
                OnPropertyChanged(
                    nameof(ServiceState),
                    nameof(ServiceStateName));
            }
        }
        #region State Left
        public LayoutState UiStateLeft
        {
            get
            {
                if (!IsSettings)
                    return LayoutState.Empty;
                else if (IsLoading)
                    return LayoutState.Loading;
                else if (!IsLoad)
                    return LayoutState.Saving;
                else if (IsLoad && IsSettings && (UiChannels.Count == 0))
                    return LayoutState.Success;
                else if (IsLoad && IsSettings && (UiChannels.Count > 0))
                    return LayoutState.None;
                
                return LayoutState.Error;
            }
        }
        public string UiStateLeftName
        {
            get {
                switch (UiStateLeft)
                {
                    case LayoutState.Empty:   return Resources.InfoTagEmpty;
                    case LayoutState.Error:   return Resources.InfoTagError;
                    case LayoutState.Saving:  return Resources.InfoTagSaving;
                    case LayoutState.Success: return Resources.InfoTagSuccess;
                    case LayoutState.Loading: return Resources.InfoTagLoading;
                    default: return string.Empty;
                }
            }
        }
        #endregion
        #region State Right
        private LayoutState _uiStateRightPrev = LayoutState.None;
        private LayoutState _uiStateRight = LayoutState.None;
        public LayoutState UiStateRight
        {
            get => _uiStateRight;
            set
            {
                if (_uiStateRight == value)
                    return;

                _uiStateRightPrev = value switch
                {
                    LayoutState.Error => _uiStateRight,
                    LayoutState.Empty => _uiStateRight,
                    LayoutState.Success => _uiStateRight,
                    _ => LayoutState.None
                };
                _uiStateRight = value;
                OnPropertyChanged(nameof(UiStateRight));

                if (_uiStateRight == LayoutState.Loading)
                    OnPropertyChanged(nameof(EqChannelsLocal));
                else if (_uiStateRight == LayoutState.Saving)
                    OnPropertyChanged(nameof(EqChannelsServ));
                else if (_uiStateRight == LayoutState.Empty)
                    OnPropertyChanged(nameof(UiAudioStatChannels));
                else if (_uiStateRight == LayoutState.Success)
                    OnPropertyChanged(nameof(UiCastChannels));
            }
        }
        #endregion
        #endregion

        #region UI screen size values
        public bool _isOrientation = true; /* Landscape = true ; Portrait = false */
        public bool IsOrientation
        {
            get => _isOrientation;
            set
            {
                if (value == _isOrientation)
                    return;
                _isOrientation = value;
                OnPropertyChanged(
                    nameof(IsOrientation),
                    nameof(IsLandscape),
                    nameof(IsPortrait),
                    nameof(HeightLeftPanel),
                    nameof(HeightRightPanel),
                    nameof(HeightListServEq),
                    nameof(HeightListLocalEq));
            }
        }
        public bool IsLandscape
        {
            get => _isOrientation;
        }
        public bool IsPortrait
        {
            get => !_isOrientation;
        }
        public double HeightLeftPanel
        {
            get
            {
                double h = DeviceDisplay.MainDisplayInfo.Height - 200; /* 170/200 */
                if ((DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait) ||
                    (Device.Idiom == TargetIdiom.Phone))
                {
                    double hh = (UiChannels.Count > 0) ? (UiChannels.Count * 60.0) : 10.0;
                    return (hh > h) ? h : hh;
                }
                return h;
            }
        }
        public double HeightRightPanel
        {
            get
            {
                if ((DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait) ||
                    (Device.Idiom == TargetIdiom.Phone))
                    return -1;
                return DeviceDisplay.MainDisplayInfo.Height - 265; /* 225/265 */
            }
        }
        public double HeightListLocalEq { get => _valuesData.BandCount[ValuesData.Local] * 32.0; }
        public double HeightListServEq  { get => _valuesData.BandCount[ValuesData.Serv] * 32.0; }
        public double HeightListStat    { get => UiAudioStatChannels.Count * 58.0; }
        public double HeightListCast    { get => UiCastChannels.Count * 50.0; }
        #endregion

        #region PLAYER BUTTON
        private bool _isPlayerPlay = false;
        public bool IsPlayerPlay
        {
            get => IsChannelSelected && _isPlayerPlay;
            set
            {
                if (_isPlayerPlay == value)
                    return;
                _isPlayerPlay = value;
                NotifySends(BaseAction.ButtonFade, new Tuple<BaseAction, bool>(BaseAction.PlayerPlay, _isPlayerPlay));
                OnPropertyChanged(nameof(IsPlayerPlay));
            }
        }
        private bool _isPlayerPause = false;
        public bool IsPlayerPause
        {
            get => IsChannelSelected && _isPlayerPause && !IsPlayerPlay;
            set
            {
                if (_isPlayerPause == value)
                    return;
                _isPlayerPause = value;
                if ((_isPlayerPause && !IsPlayerPlay) || !_isPlayerPause)
                    NotifySends(BaseAction.ButtonFade, new Tuple<BaseAction, bool>(BaseAction.PlayerPause, _isPlayerPause));
                OnPropertyChanged(nameof(IsPlayerPause));
            }
        }
        private bool _isPlayerStop = false;
        public bool IsPlayerStop
        {
            get => IsChannelSelected && _isPlayerStop && !IsPlayerPlay;
            set
            {
                if (_isPlayerStop == value)
                    return;
                _isPlayerStop = value;
                IsDuration = false;
                if ((_isPlayerStop && !IsPlayerPlay) || !_isPlayerStop)
                    NotifySends(BaseAction.ButtonFade, new Tuple<BaseAction, bool>(BaseAction.PlayerStop, _isPlayerStop));
                OnPropertyChanged(nameof(IsPlayerStop));
            }
        }
        #endregion

        private int  _playTime = 0;
        private bool _isDuration = false;
        public bool IsDurationLandscape { get => IsLandscape && _isDuration; }
        public  bool IsDuration
        {
            get => _isDuration;
            set
            {
                if (_isDuration == value)
                    return;

                _isDuration = value;
                OnPropertyChanged(nameof(IsDuration), nameof(IsDurationLandscape));
            }
        }
        private TimeSpan _duration = TimeSpan.Zero;
        public TimeSpan Duration
        {
            get => _duration;
            set
            {
                if (_duration == value)
                    return;

                _duration = value;
                IsDuration = true;
                OnPropertyChanged(nameof(Duration));
            }
        }
        private string _uiStateError = string.Empty;
        public string UiStateError
        {
            get => _uiStateError;
            set
            {
                if ((_uiStateError == value) || string.IsNullOrWhiteSpace(value))
                    return;

                _uiStateError = value;
                UiStateRight = LayoutState.Error;

                Device.StartTimer(TimeSpan.FromSeconds(15), () => {
                    Device.BeginInvokeOnMainThread(() => {
                        _uiStateError = string.Empty;
                        if (UiStateRight == LayoutState.Error)
                            UiStateRight = _uiStateRightPrev;
                        OnPropertyChanged(nameof(UiStateError), nameof(IsUiStateError));
                    });
                    return false;
                });
                OnPropertyChanged(nameof(UiStateError), nameof(IsUiStateError));
            }
        }
        private string _deviceName = string.Empty;
        public string DeviceName
        {
            get => _deviceName;
            set {
                if (_deviceName == value)
                    return;
                _deviceName = value; OnPropertyChanged(nameof(DeviceName));
            }
        }
        private string _baseUri = string.Empty;
        public string BaseUri
        {
            get => _baseUri;
            set {
                do
                {
                    if (string.IsNullOrWhiteSpace(_baseUri) && string.IsNullOrWhiteSpace(value))
                    {
                        IsBaseUriExpander = true;
                        break;
                    }
                    if ((_baseUri != null) && (value != null) && _baseUri.Equals(value))
                        break;

                    if (string.IsNullOrWhiteSpace(value))
                        _baseUri = string.Empty;
                    else
                        _baseUri = value;

                    IsBaseUriExpander = IsBaseUriEmpty;

                    NotifySends(BaseAction.BaseUriChange, value);
                    OnPropertyChanged(nameof(BaseUri), nameof(IsBaseUriEmpty), nameof(IsBaseUriExpander));
                    return;

                } while (false);
                OnPropertyChanged(
                    nameof(IsBaseUriEmpty),
                    nameof(IsBaseUriExpander));
            }
        }
        private bool _isLoad = false;
        public bool IsLoad
        {
            get => _isLoad;
            set {
                if (_isLoad == value)
                    return;
                _isLoad = value;
                NotifySends(BaseAction.LoadChange, value);
                OnPropertyChanged(
                    nameof(IsLoad),
                    nameof(UiStateLeft),
                    nameof(UiStateLeftName));
            }
        }
        private bool _isLoading = false;
        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                if (_isLoading == value)
                    return;
                _isLoading = value;

                if (!_isLoading)
                    IsRefresh = false;

                NotifySends(BaseAction.LoadingChange, value);
                OnPropertyChanged(
                    nameof(IsLoading),
                    nameof(IsPlayerWait),
                    nameof(UiStateLeft),
                    nameof(UiStateLeftName));
            }
        }
        private bool _isSettings = false;
        public bool IsSettings
        {
            get => _isSettings;
            set {
                if (_isSettings == value)
                    return;
                _isSettings = value;
                if (!value)
                {
                    IsLoad = false;
                    BaseUri = string.Empty;
                    UiChannelSelected = default;
                    UiChannels.Clear();
                }
                NotifySends(BaseAction.SettingsChange, value);
                OnPropertyChanged(
                    nameof(IsSettings),
                    nameof(UiStateLeft),
                    nameof(UiStateLeftName));
            }
        }
        private bool _isMuteCtrl = false;
        public bool IsMuteCtrl
        {
            get => _isMuteCtrl;
            set {
                if (_isMuteCtrl == value)
                    return;
                _isMuteCtrl = value;
                VolumeChannel.Mute(_isMuteCtrl);
                OnPropertyChanged(nameof(IsMuteCtrl), nameof(VolumeChannel));
            }
        }
        private bool _isAutoPlayCtrl = false;
        public bool IsAutoPlayCtrl
        {
            get => _isAutoPlayCtrl;
            set {
                if (_isAutoPlayCtrl == value)
                    return;
                _isAutoPlayCtrl = value;
                NotifySends(BaseAction.AutoPlayChange, nameof(IsAutoPlayCtrl), value);
            }
        }
        private bool _isLoopPlayCtrl = false;
        public bool IsLoopPlayCtrl
        {
            get => _isLoopPlayCtrl;
            set
            {
                if (_isLoopPlayCtrl == value)
                    return;
                _isLoopPlayCtrl = value;
                NotifySends(BaseAction.LoopPlayChange, nameof(IsLoopPlayCtrl), value);
            }
        }
        private bool _isRefresh = false;
        public bool IsRefresh
        {
            get => IsSettings && _isRefresh;
            set {
                if (!IsSettings || (_isRefresh == value))
                    return;
                if (value && ChannelReLoadCommand.CanExecute(null))
                    ChannelReLoadCommand.Execute(null);
                _isRefresh = value;
                OnPropertyChanged(nameof(IsRefresh));
            }
        }
        private bool _isPlayerWait = false;
        public bool IsPlayerWait
        {
            get => IsLoading || _isPlayerWait;
            set
            {
                if (_isPlayerWait == value)
                    return;
                _isPlayerWait = value;
                OnPropertyChanged(nameof(IsPlayerWait));
            }
        }
        private bool _isBaseUriExpander = true;
        public bool IsBaseUriExpander
        {
            get => _isBaseUriExpander;
            set {
                if (_isBaseUriExpander == value)
                    return;
                _isBaseUriExpander = value;
                OnPropertyChanged(nameof(IsBaseUriExpander));
            }
        }
        public bool IsBaseUriEmpty
        {
            get => string.IsNullOrWhiteSpace(BaseUri);
        }
        public bool IsUiStateError
        {
            get => !string.IsNullOrWhiteSpace(UiStateError);
        }
        public bool IsChannelSelected
        {
            get => UiChannelSelected != null;
        }
        #endregion

        #region private EQ/Volume/Balance Updates/Set
        /* check from new selected PlayListItem */
        private bool CheckSlidersValues(PlayListItem pli)
        {
            if (pli == null)
                return false;
            try
            {
                if (EqChannelsServ.Count != EqChannelsLocal.Count)
                    throw new ArgumentOutOfRangeException(nameof(EqChannelsLocal));
                if (pli.Equalizers.Length != pli.EqualizersLocal.Length)
                    throw new ArgumentOutOfRangeException(nameof(pli.EqualizersLocal));
                if (pli.Equalizers.Length != EqChannelsServ.Count)
                    throw new ArgumentOutOfRangeException(nameof(pli.Equalizers.Length));
                return true;
            }
            catch (Exception ex) { LogWriter.Debug<MainPageView>(nameof(CheckSlidersValues), ex); }
            return false;
        }
        /* change new selected PlayListItem */
        private void ChangePlayListItem(PlayListItem pli, PlayListItem npli, bool b)
        {
            if (CheckSlidersValues(pli))
                SaveSlidersValues(pli, b);
            if (CheckSlidersValues(npli))
                SetSlidersValues(npli, b);
        }
        /* change view EQ controls - Local/Service */
        private void ChangeEqView(PlayListItem pli, bool b)
        {
            if (pli == null)
                return;

            lock (_lock)
            {
                bool prev = !b;
                for (int i = 0; i < EqChannelsServ.Count; i++)
                {
                    if (b)
                    {
                        pli.Equalizers[i] = EqChannelsServ[i].Value;
                        EqChannelsLocal[i].Value = pli.EqualizersLocal[i];
                    }
                    else
                    {
                        pli.EqualizersLocal[i] = EqChannelsLocal[i].Value;
                        EqChannelsServ[i].Value = pli.Equalizers[i];
                    }
                }
                SaveSliderVolume(pli, !b);
                SetSliderVolume(pli, b);
            }
            OnPropertyChanged(b ? nameof(EqChannelsLocal) : nameof(EqChannelsServ));
        }
        /* save old selected PlayListItem from new PlayListItem */
        private void SaveSlidersValues(PlayListItem pli, bool b)
        {
            if (pli == null)
                return;

            try {
                lock (_lock)
                {
                    for (int i = 0; i < EqChannelsServ.Count; i++)
                    {
                        pli.Equalizers[i] = EqChannelsServ[i].Value;
                        pli.EqualizersLocal[i] = EqChannelsLocal[i].Value;
                    }
                    SaveSliderVolume(pli, b);
                }
                NotifySends(BaseAction.ChannelUpdate,
                    new Tuple<PlayListItem, bool>(pli, IsEqLocalCtrl));
            } catch (Exception ex) { LogWriter.Debug<MainPageView>(nameof(SaveSlidersValues), ex); }
        }
        /* set GUI values from new selected PlayListItem */
        private void SetSlidersValues(PlayListItem pli, bool b)
        {
            if (pli == null)
                return;

            try {
                lock (_lock)
                {
                    for (int i = 0; i < pli.Equalizers.Length; i++)
                    {
                        EqChannelsServ[i].Value = pli.Equalizers[i];
                        EqChannelsLocal[i].Value = pli.EqualizersLocal[i];
                        EqChannelsLocal[i].Visible = _valuesData.BandCount[ValuesData.Local] > i;
                    }
                    if (_valuesData.EqLevelsDefault != null)
                    {
                        if (EqChannelsLocal.ToArray().IsEmptyValues())
                            for (int i = 0; (i < _valuesData.EqLevelsDefault.Length) && (i < EqChannelsLocal.Count); i++)
                                EqChannelsLocal[i].Value = _valuesData.EqLevelsDefault[i];
                    }
                    SetSliderVolume(pli, b);
                }
                OnPropertyChanged(nameof(EqChannelsServ), nameof(EqChannelsLocal));
            }
            catch (Exception ex) { LogWriter.Debug<MainPageView>(nameof(SetSlidersValues), ex); }
        }
        /* set GUI sliders from changed EQ preset or receive EQ settings */
        private void SetEqSettings(IPlayerEqSettings eqs)
        {
            if (eqs == null)
                return;
            PlayListItem pli = UiChannelSelected;

            try {
                lock (_lock)
                {
                    int idx = (eqs.BandLevels != null) && (eqs.BandLevels.Length > 0) ? eqs.BandLevels.Length : -1;
                    for (int i = 0; i < EqChannelsLocal.Count; i++)
                    {
                        EqChannelsLocal[i].Min = _valuesData.EqRanges[ValuesData.Local][ValuesData.Min];
                        EqChannelsLocal[i].Max = _valuesData.EqRanges[ValuesData.Local][ValuesData.Max];
                        if (idx > i)
#                           pragma warning disable CS8602
                            EqChannelsLocal[i].Value = eqs.BandLevels[i];
#                           pragma warning restore CS8602
                    }
                    if (idx > 0)
                    {
                        if (idx < EqChannelsLocal.Count)
                            for (int i = idx; i < EqChannelsLocal.Count; i++)
                                EqChannelsLocal[i].Visible = false;
                        if (pli != null)
                            for (int i = 0; (i < idx) && (i < pli.EqualizersLocal.Length); i++)
#                               pragma warning disable CS8602
                                pli.EqualizersLocal[i] = eqs.BandLevels[i];
#                               pragma warning restore CS8602
                    }
                    OnPropertyChanged(nameof(EqChannelsLocal));
                }
            } catch (Exception ex) { LogWriter.Debug<MainPageView>(nameof(SetEqSettings), ex); }
        }
        #region EQ internal
        /* internal: volume from PlayListItem */
        private void SaveSliderVolume(PlayListItem pli, bool b)
        {
            int idx = b ? ValuesData.Local : ValuesData.Serv;
            float val = (_valuesData.VolRanges[idx][ValuesData.Min] > VolumeChannel.Value) ?
                    _valuesData.VolRanges[idx][ValuesData.Min] : ((_valuesData.VolRanges[idx][ValuesData.Max] < VolumeChannel.Value) ?
                    _valuesData.VolRanges[idx][ValuesData.Max] : VolumeChannel.Value);
            if (b)
                pli.VolumeLocal = val;
            else
                pli.Volume = val;
            pli.Balance = BalanceChannel.Value;
        }
        /* internal: set volume from PlayListItem */
        private void SetSliderVolume(PlayListItem pli, bool b)
        {
            int idx = b ? ValuesData.Local : ValuesData.Serv;
            VolumeChannel.Min = _valuesData.VolRanges[idx][ValuesData.Min];
            VolumeChannel.Max = _valuesData.VolRanges[idx][ValuesData.Max];
            VolumeChannel.Value = b ?
                ((pli.VolumeLocal == 0.0f) ? (_volumeChannel.Max / 1.2f) : pli.VolumeLocal) :
                    ((pli.Volume == 0.0f) ? (_volumeChannel.Max / 2.0f) : pli.Volume);
            BalanceChannel.Value = ((pli.Balance > 1.0f) || (pli.Balance < -1.0f)) ? 0.0f : pli.Balance;
        }
        #endregion
        #endregion

        #region private Command method
        private async Task<bool> CommandRun(Func<bool> fun, bool isUiTask) =>
            await Task.FromResult<bool>(ShellRun(fun, isUiTask));
        private async Task<bool> CommandRun<T>(Func<T, bool> fun, T item, bool isUiTask) =>
            await Task.FromResult<bool>(ShellRun(fun, item, isUiTask));
        private bool ShellRun<T>(Func<T, bool> fun, T item, bool isUiTask)
        {
            if (isUiTask)
            {
                bool b = false;
                Device.BeginInvokeOnMainThread(() => b = fun.Invoke(item));
                return b;
            }
            return fun.Invoke(item);
        }
        private bool ShellRun(Func<bool> fun, bool isUiTask)
        {
            if (isUiTask)
            {
                bool b = false;
                Device.BeginInvokeOnMainThread(() => b = fun.Invoke());
                return b;
            }
            return fun.Invoke();
        }
        #endregion

        #region event
        private float[] _eqServ = default;
        private float[] _eqLocal = default;
        private float _volValue = 0.0f;
        private float _balValue = 0.0f;
        private float _virtValue = 0.0f;
        private float _basValue = 0.0f;
        private float _loundValue = 0.0f;
        public void ValuesChacheReset()
        {
            _eqServ = _eqLocal = default;
            _volValue = _balValue = _virtValue = _basValue = _loundValue = 0.0f;
        }
        private bool ValueCompare(float v, ref float vv)
        {
            if (!v.Equals(vv)) {
                v = vv;
                return true;
            }
            return false;
        }
        private bool EqCompare(float[] f, bool b)
        {
            do {
                float[] f_ = b ? _eqLocal : _eqServ;
                if (f == null || f_ == null)
                    break;

                if (!f.FloatArrayCompare(f_))
                    return false;

            } while (false);
            if (b)
                _eqLocal = f;
            else
                _eqServ = f;
            return true;
        }
        private void EqChannel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (IsChannelSelected && (sender is ChannelValues cv))
            {
                switch (cv.TypeChannel)
                {
                    case ChannelValuesId.TypeEq: {
                            if (!IsEqLocalCtrl || !IsEqControlsEnable)
                                return;

                            float[] eqs = new float[EqChannelsLocal.Count];
                            for (int i = 0; i < EqChannelsLocal.Count; i++)
                                eqs[i] = EqChannelsLocal[i].Value;
                            if (EqCompare(eqs, IsEqLocalCtrl))
                                NotifySends(
                                    BaseAction.EqChannelUpdate,
                                    new Tuple<int, float [], bool>(cv.Id, eqs, IsEqLocalCtrl));
                            return;
                        }
                    case ChannelValuesId.TypeVirtualize:
                    case ChannelValuesId.TypeLoudness:
                    case ChannelValuesId.TypeBasBoost: {
                            if (!IsEqControlsEnable || float.IsNaN(VolumeChannel.Value))
                                return;
                            BaseAction ba;
                            switch (cv.TypeChannel)
                            {
                                case ChannelValuesId.TypeBasBoost:
                                    {
                                        if (!ValueCompare(cv.Value, ref _basValue))
                                            return;
                                        ba = BaseAction.EfBasBoostChannelUpdate;
                                        break;
                                    }
                                case ChannelValuesId.TypeLoudness:
                                    {
                                        if (!ValueCompare(cv.Value, ref _loundValue))
                                            return;
                                        ba = BaseAction.EfLoudnessChannelUpdate;
                                        break;
                                    }
                                case ChannelValuesId.TypeVirtualize:
                                    {
                                        if (!ValueCompare(cv.Value, ref _virtValue))
                                            return;
                                        ba = BaseAction.EfVirtualizeChannelUpdate;
                                        break;
                                    }
                                default: return;
                            }
                            NotifySends(
                                ba, new Tuple<int, float, bool>(cv.Id, Math.Min(cv.Max, cv.Value), IsEqControlsEnable));
                            return;
                        }
                    case ChannelValuesId.TypeVolume:
                    case ChannelValuesId.TypeBalance: {
                            if (!IsEqLocalCtrl || !IsEqControlsEnable || float.IsNaN(VolumeChannel.Value) || float.IsNaN(BalanceChannel.Value))
                                return;

                            float vval = Math.Min(VolumeChannel.Max,  Math.Max(VolumeChannel.Min, VolumeChannel.Value)),
                                  bval = Math.Min(BalanceChannel.Max, Math.Max(BalanceChannel.Min, BalanceChannel.Value));
                            if (ValueCompare(vval, ref _volValue) || ValueCompare(bval, ref _balValue))
                                NotifySends(BaseAction.VolumeChannelUpdate, new Tuple<int, float, float, bool>(cv.Id, vval, bval, IsEqLocalCtrl));
                            break;
                        }
                    default: return;
                }
            }
        }
        #endregion
    }
}
