﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace AudioMonitor.Data
{
    public static class StaticSettings
    {
        public const int EqCount = 10;

        public const  string KeySettingsLoclEq = "LEq";
        public const  string KeySettingsLoclVol = "LVol";
        public const  string KeySettingsAutoPlay = nameof(KeySettingsAutoPlay);
        public const  string KeySettingsLoopPlay = nameof(KeySettingsLoopPlay);
        public const  string KeySettingsEqEnable = nameof(KeySettingsEqEnable);
        public const  string KeySettingsEqLocal = nameof(KeySettingsEqLocal);
        public const  string KeyCloseServiceOnExit = nameof(KeyCloseServiceOnExit);

        public const  string KeySettingsMono = nameof(KeySettingsMono);
        public const  string KeySettingsUri = nameof(KeySettingsUri);

        public static string KeySettingsLocalEq(int id) => $"{KeySettingsLoclEq}{id}";
        public static string BuildServiceUrl(string s) => $"{s}{SettingsUri}";
        public static string BuildServiceUrl(string s, int id) => $"{s}{ChannelUri}{id}";
        public static string BuildServicePostUrl(string s, int id) => $"{s}{SettingsUri}/{id}";
        public static string BuildServiceStatUrl(string s) => $"{s}{SettingsUri}/stat";

        public const string SettingsUri = "/stream/audio/settings";
        public const string ChannelUri = "/stream/audio/";
        public const string GitUri = "https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service";
    }
}
