﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace AudioMonitor.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioSettingsLite
    {
        [XmlElement("channels")]
        public List<PlayListItem> Channels = new List<PlayListItem>();
        public AudioSettingsLite Add(PlayListItem acr) { Channels.Add(acr); return this; }
        public AudioSettingsLite AddRange(IEnumerable<PlayListItem> list) { Channels.AddRange(list); return this; }
        public int Count() => Channels.Count;
    }
}
