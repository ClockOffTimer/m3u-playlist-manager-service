﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace AudioMonitor.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioChannelLite
    {
        [XmlElement("id")]
        public int Id { get; set; } = -1;
        [XmlElement("tag")]
        public string Tag { get; set; } = default;
        [XmlElement("mono")]
        public bool IsMono { get; set; } = false;
        [XmlElement("volume")]
        public float Volume { get; set; } = 0.5f;
        [XmlElement("equalizer")]
        public float[] Equalizers { get; set; } = new float[10];
#if STREAM_EXTENDED_IMPORT
        [XmlElement("type")]
        public AudioStreamType Atype { get; set; } = AudioStreamType.None;
#endif
        public AudioChannelLite() { }

        public AudioChannelLite Copy(PlayListItem pli)
        {
            if (pli == null)
                return null;

            Id = pli.Id;
            Tag = pli.Tag;
            IsMono = pli.IsMono;
            Volume = pli.Volume;
            Equalizers = pli.Equalizers;
            return this;
        }
    }
}
