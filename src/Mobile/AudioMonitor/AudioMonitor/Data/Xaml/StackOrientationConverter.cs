﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AudioMonitor.Data.Xaml
{
    public class StackOrientationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Device.Idiom == TargetIdiom.Phone)
                return StackOrientation.Vertical;

            if (value is bool b)
                return b ? StackOrientation.Horizontal : StackOrientation.Vertical;
            return StackOrientation.Horizontal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
