﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AudioMonitor.Data.Xaml
{
    public class TimeSpanDurationConverter : IValueConverter
    {
        const string defaulValue = "0:0";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is TimeSpan ts)
            {
                if (ts == TimeSpan.MinValue)
                    return defaulValue;
            }
            if (ts.Duration().Days > 0)
                return $"{ts.Duration().Days} - {ts.Duration().Hours}:{ts.Duration().Minutes}:{ts.Duration().Seconds}";
            if (ts.Duration().Hours > 0)
                return $"{ts.Duration().Hours}:{ts.Duration().Minutes}:{ts.Duration().Seconds}";
            if (ts.Duration().Minutes > 0)
                return $"{ts.Duration().Minutes}:{ts.Duration().Seconds}";
            if (ts.Duration().Seconds > 0)
                return $"0:{ts.Duration().Seconds}";
            return defaulValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
