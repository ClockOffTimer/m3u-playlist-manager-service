﻿using System;
using System.Globalization;
using System.Net;
using Xamarin.Forms;

namespace AudioMonitor.Data.Xaml
{
    public class IPEndPointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try {
                if (value is IPEndPoint ipe)
                    return $"{ipe.Address}:{ipe.Port}";
            } catch { }
            return "unknown source";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
