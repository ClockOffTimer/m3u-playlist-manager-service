﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace AudioMonitor.Data.Xaml
{
    public class IconVolumeConverter : IValueConverter
    {
        private static char[] _chars = new char[]
        {
            (char)0xe80c, (char)0xe80d, (char)0xe80e, (char)0xe80f
        };

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if ((value is double volume) &&
                    (!double.IsNaN(volume)) &&
                    (parameter != null) &&
                    (parameter is Binding bind) &&
                    (bind != null) &&
                    (bind.Source is CC.CircularSlider slider))
                {
                    if (volume == slider.Minimum)
                        return _chars[0].ToString();
                    if (volume == slider.Maximum)
                        return _chars[3].ToString();

                    double d = (slider.Minimum < 0.0) ? slider.Maximum / 3 : slider.Maximum / 4;
                    double[] matrix = new double[]
                    {
                        (slider.Minimum / 1.08),
                        (slider.Minimum < 0.0) ? (slider.Minimum / 2) : d,
                        d + d,
                        slider.Maximum
                    };
                    for (int i = 0; i < matrix.Length; i++)
                        if (matrix[i] >= volume)
                            return _chars[i].ToString();
                }
            } catch {}
            return _chars[0].ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
