﻿using System;
using System.Collections.Generic;
using System.Globalization;
using AudioMonitor.Properties;
using Xamarin.Forms;

namespace AudioMonitor.Data.Xaml
{
    public class ListStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try {
                if (value is List<string> list)
                    return string.Join(", ", list);
            }
            catch { }
            return Resources.ServStatEmpty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
