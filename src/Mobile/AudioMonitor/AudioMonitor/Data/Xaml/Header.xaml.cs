﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AudioMonitor.Data.Xaml
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Header : StackLayout
    {
        public Header()
        {
            InitializeComponent();
        }
    }
}