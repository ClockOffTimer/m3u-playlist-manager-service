﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.CommunityToolkit.Helpers;

namespace AudioMonitor.Data.Events
{
    public class BaseEventChanger<T1> : INotifyPropertyChanged, IBaseEventChanger<T1> where T1 : struct, Enum
    {
        #region Event INotifyPropertyChanged
        private readonly DelegateWeakEventManager _pcmgr = new DelegateWeakEventManager();

        public event PropertyChangedEventHandler PropertyChanged
        {
            add => _pcmgr.AddEventHandler(value);
            remove => _pcmgr.RemoveEventHandler(value);
        }

        public void OnChanged([CallerMemberName] string name = "") =>
            _Rise(name);

        public void OnPropertyChanged(string name) =>
            _Rise(name);

        public void OnPropertyChanged(params string[] names)
        {
            foreach (var s in names)
                _Rise(s);
        }

        private void _Rise(string s) =>
            _pcmgr.RaiseEvent(this, new PropertyChangedEventArgs(s), nameof(PropertyChanged));

        #endregion

        #region Event BaseEvent
        public BaseEvent<T1> GetBaseEvent { get; private set; } = new BaseEvent<T1>();
        #endregion

        public void NotifySends(T1 a, object val, [CallerMemberName] string pn = "") =>
            _NotifySends(a, null, val, null, pn);

        public void NotifySends(T1 a, object val, IList<string> list, [CallerMemberName] string pn = "") =>
            _NotifySends(a, null, val, list, pn);

        public void NotifySends(T1 a, string val, IList<string> list, [CallerMemberName] string pn = "") =>
            _NotifySends(a, null, val, list, pn);

        public void NotifySends(T1 a, string s, object val, [CallerMemberName] string pn = "") =>
            _NotifySends(a, s, val, null, pn);

        public void NotifySends(T1 a, string s, object val, IList<string> list, [CallerMemberName] string pn = "") =>
            _NotifySends(a, s, val, list, pn);

        private void _NotifySends(T1 a, string s, object val, IList<string> list, string pn)
        {
            GetBaseEvent.NotifySend_(a, val, pn);
            if (s != null)
                _Rise(s);
            if (list != null)
                foreach (var field in list)
                    _Rise(field);
        }
    }
}
