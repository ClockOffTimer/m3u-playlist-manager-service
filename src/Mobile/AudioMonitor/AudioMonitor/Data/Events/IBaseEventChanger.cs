﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace AudioMonitor.Data.Events
{
    public interface IBaseEventChanger<T1> where T1 : struct, Enum
    {
        BaseEvent<T1> GetBaseEvent { get; }

        event PropertyChangedEventHandler PropertyChanged;

        void NotifySends(T1 a, object val, [CallerMemberName] string pn = "");
        void NotifySends(T1 a, object val, IList<string> list, [CallerMemberName] string pn = "");
        void NotifySends(T1 a, string val, IList<string> list, [CallerMemberName] string pn = "");
        void NotifySends(T1 a, string s, object val, [CallerMemberName] string pn = "");
        void NotifySends(T1 a, string s, object val, IList<string> list, [CallerMemberName] string pn = "");
        void OnChanged([CallerMemberName] string name = "");
        void OnPropertyChanged(params string[] names);
        void OnPropertyChanged(string name);
    }
}