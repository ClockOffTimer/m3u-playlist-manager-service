﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Forms;

namespace AudioMonitor.Data.Events
{
    public class BaseEventArgs<T1> : EventArgs where T1 : struct, Enum
    {
        public T1 Tag;
        public object Value;
        public Type ValueType;
        public IList<string> Targets;
        public string CallerName;
        public string CallerPlaceName = string.Empty;

        private BaseEventArgs(T1 a, object val, Type t, IList<string> list, string calln)
        {
            Tag = a;
            Value = val ?? default;
            ValueType = val != null ? ((t == Type.Missing.GetType()) ? val.GetType() : t) :
                            Type.Missing.GetType();
            Targets = list ?? default(IList<string>);
            CallerName = string.IsNullOrWhiteSpace(calln) ? string.Empty : calln;
        }
        public static BaseEventArgs<T1> Create(T1 a, object val, Type t, IList<string> list = default, string calln = default) =>
            new BaseEventArgs<T1>(a, val, t, list, calln);

        public static BaseEventArgs<T1> Create(T1 a, object val, string calln = default) =>
            new BaseEventArgs<T1>(a, val, Type.Missing.GetType(), default, calln);

        public BaseEventArgs<T1> SetCaller(string calln)
        {
            CallerName = string.IsNullOrWhiteSpace(calln) ? string.Empty : calln;
            return this;
        }
        public BaseEventArgs<T1> SetCallerPlace(string calln)
        {
            CallerPlaceName = string.IsNullOrWhiteSpace(calln) ? string.Empty : calln;
            return this;
        }
    }

    public class BaseEvent<T1> where T1 : struct, Enum
    {
        private int _eventCount { get; set; } = 0;
        private readonly DelegateWeakEventManager _emgr = new DelegateWeakEventManager();
        private readonly Queue<BaseEventArgs<T1>> _queue = new Queue<BaseEventArgs<T1>>();
        public event EventHandler EventCb
        {
            add { _emgr.AddEventHandler(value); _eventCount++; }
            remove { _emgr.RemoveEventHandler(value); _eventCount--; }
        }

        public void CallEvent(BaseEventArgs<T1> be) =>
            _emgr.RaiseEvent(this, be, nameof(EventCb));
        public void CallEvent(BaseEventArgs<T1> be, [CallerMemberName] string pn = "") =>
            _emgr.RaiseEvent(this, be.SetCaller(pn), nameof(EventCb));
        public void CallEvent(T1 a, object val, Type t, IList<string> list, [CallerMemberName] string pn = "") =>
            _emgr.RaiseEvent(this, BaseEventArgs<T1>.Create(a, val, t, list, pn), nameof(EventCb));
        public void CallEvent(T1 a, object val, IList<string> list = default, [CallerMemberName] string pn = "") =>
            CallEvent(
                a,
                val,
                val == null ? Type.Missing.GetType() : val.GetType(),
                list: list ?? default(IList<string>),
                pn);
        public void CallEventTH(BaseEventArgs<T1> be) =>
            Device.BeginInvokeOnMainThread(() => _emgr.RaiseEvent(this, be, nameof(EventCb)));

        public BaseEvent([CallerMemberName] string calle = "") => CallerPlaceName = calle;
        ~BaseEvent() => _queue.Clear();

        public void NotifySend(T1 a, object val, [CallerMemberName] string pn = "") =>
            _NotifySend(a, val, pn);

        public void NotifySend_(T1 a, object val, string pn) =>
            _NotifySend(a, val, pn);

        private async void _NotifySend(T1 a, object val, string pn)
        {
            BaseEventArgs<T1> args = BaseEventArgs<T1>
                                        .Create(a, val, pn)
                                        .SetCallerPlace(CallerPlaceName);
            if (Count() == 0)
            {
                if (IsDebugPrint)
                    LogWriter.Debug<BaseEvent<T1>>($"{nameof(_NotifySend)} -> History ADD args: ({CallerPlaceName}): {args.Tag}/{args.Value}");
                try
                {
                    _queue.Enqueue(args);
                    return;
                }
#if DEBUG
                catch (Exception ex)
                {
                    if (IsDebugPrint)
                        LogWriter.Debug<BaseEvent<T1>>($"{nameof(_NotifySend)} -> History ADD error:", ex);
                }
#else
                catch { }
#endif
                if (Count() == 0)
                    return;
            }
            if (_queue.Count > 0)
                await HistoryCall();
            CallEvent(args);
        }

        public int Count() => _eventCount;
        public string CallerPlaceName { get; set; } = string.Empty;
        public bool IsDebugPrint { get; set; } = false;
        public BaseEvent<T1> SetCallerPlace(string calle) { if (!string.IsNullOrWhiteSpace(calle)) CallerPlaceName = calle; return this; }
        public BaseEvent<T1> SetDebugPrint(bool b = false) { IsDebugPrint = b; return this; }
        public BaseEvent<T1> HistoryClear() { _queue.Clear(); return this;  }

        private async Task HistoryCall()
        {
            await Task.Run(() =>
            {
                try
                {
                    DuplicateComparer<T1> dup = new DuplicateComparer<T1>();

                    while (_queue.Count > 0)
                    {
                        BaseEventArgs<T1> args = _queue.Dequeue();
                        if (args == null)
                            continue;
#if DEBUG
                        if (IsDebugPrint)
                            LogWriter.Debug<BaseEvent<T1>>($"{nameof(HistoryCall)} -> History SEND args: ({CallerPlaceName}): {args.Tag}/{args.Value}");
#endif
                        try
                        {
                            if (!dup.Compare(args.Tag, args.Value))
                                CallEvent(args);
                        }
#if DEBUG
                        catch (Exception ex) {
                            if (IsDebugPrint)
                                LogWriter.Debug<BaseEvent<T1>>($"{nameof(HistoryCall)} -> History SEND error:", ex);
                        }
#else
                        catch { }
#endif
                    }
                } catch { _queue.Clear(); }
            }).ConfigureAwait(false);
        }
    }
}
