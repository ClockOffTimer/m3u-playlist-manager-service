﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace AudioMonitor.Data.Events
{
    internal class DuplicateComparer<T2>
    {
        private int Tag = -1;
        private int Value = -1;

        public void Set(T2 tag, object val)
        {
            Tag = tag?.GetHashCode() ?? -3;
            Value = val?.GetHashCode() ?? -3;
        }
        public void Set(int itag, int ival)
        {
            Tag = itag;
            Value = ival;
        }
        public bool Compare(T2 tag, object val)
        {
            int itag = tag?.GetHashCode() ?? -2, ival = val?.GetHashCode() ?? -2;
            if ((Tag == itag) && (Value == ival))
                return false;
            Set(itag, ival);
            return false;
        } 
    }

    public class BaseEventObserver<T1> where T1 : struct, Enum
    {
        private readonly Dictionary<string, Tuple<T1, Action<object, Type, IList<string>>, IList<string>>> _watch =
            new Dictionary<string, Tuple<T1, Action<object, Type, IList<string>>, IList<string>>>();

        DuplicateComparer<T1> dupa = new DuplicateComparer<T1>();
        DuplicateComparer<string> dups = new DuplicateComparer<string>();

        WeakReference<BaseEvent<T1>> _weakBaseEvent = default;
        WeakReference<INotifyPropertyChanged> _weakInpc = default;

        public BaseEventObserver(BaseEvent<T1> be) => _weakBaseEvent = new WeakReference<BaseEvent<T1>>(be);
        public BaseEventObserver(BaseEvent<T1> be, INotifyPropertyChanged inpc)
        {
            _weakBaseEvent = new WeakReference<BaseEvent<T1>>(be);
            _weakInpc = new WeakReference<INotifyPropertyChanged>(inpc);
        }
        ~BaseEventObserver()
        {
            try
            {
                if ((_weakBaseEvent != null) && _weakBaseEvent.TryGetTarget(out BaseEvent<T1> be))
                    be.EventCb -= _EventCb;
                if ((_weakInpc != null) && _weakInpc.TryGetTarget(out INotifyPropertyChanged inpc))
                    inpc.PropertyChanged -= _PropertyChanged;
            } catch { }
        }

        public void Start()
        {
            try
            {
                if ((_weakBaseEvent != null) && _weakBaseEvent.TryGetTarget(out BaseEvent<T1> be))
                    be.EventCb += _EventCb;
                if ((_weakInpc != null) && _weakInpc.TryGetTarget(out INotifyPropertyChanged inpc))
                    inpc.PropertyChanged += _PropertyChanged;
            } catch { }
        }

        public bool IsNoDuplicate { get; set; } = false;
        public BaseEventObserver<T1> NoDuplicate(bool b = true)
        {
            IsNoDuplicate = b;
            return this;
        }
        public bool IsDebugPrint { get; set; } = false;
        public BaseEventObserver<T1> SetDebugPrint(bool b = false)
        {
            IsDebugPrint = b;
            return this;
        }


        public void Add(T1 a, Action<object, Type, IList<string>> act, IList<string> list)
        {
            var s = a.ToString();
            if (_watch.ContainsKey(s))
                throw new DuplicateWaitObjectException($"Dictionary: {s}");
            _watch.Add(s, new Tuple<T1, Action<object, Type, IList<string>>, IList<string>>(a, act, list));
        }
        public void Add(string s, Action<object, Type, IList<string>> act, IList<string> list)
        {
            if (_watch.ContainsKey(s))
                throw new DuplicateWaitObjectException($"Dictionary: {s}");
            _watch.Add(s, new Tuple<T1, Action<object, Type, IList<string>>, IList<string>>(default(T1), act, list));
        }
        public void Remove(T1 a)
        {
            var s = a.ToString();
            if (!_watch.ContainsKey(s))
                return;
            _watch.Remove(s);
        }
        public void Clear()
        {
            if (_watch.Count > 0)
                _watch.Clear();
        }

        private async void _PropertyChanged(object _, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == default)
                return;
            if (IsNoDuplicate && dups.Compare(e.PropertyName, e))
                return;
            await __PropertyChanged(e.PropertyName, e);
        }
        private async Task __PropertyChanged(string s, PropertyChangedEventArgs e)
        {
            await Task.Run(() =>
            {
                try {
                    if (!_watch.ContainsKey(s))
                        return;
                    if (!_watch.TryGetValue(s, out var t) || (t == null))
                        return;
                    t.Item2.Invoke(e, typeof(PropertyChangedEventArgs), t.Item3);
                } catch { }
            }).ConfigureAwait(false);
        }

        private async void _EventCb(object _, EventArgs args)
        {
            if (args is BaseEventArgs<T1> bea)
            {
                if (IsNoDuplicate && dupa.Compare(bea.Tag, bea.Value))
                    return;
                await __EventCb(bea.Tag.ToString(), bea);
            }
        }
        private async Task __EventCb(string key, BaseEventArgs<T1> args)
        {
            await Task.Run(() =>
            {
                try {
                    if (!_watch.ContainsKey(key))
                        return;
                    if (!_watch.TryGetValue(key, out var t) || (t == null))
                        return;
                    t.Item2.Invoke(args.Value, args.ValueType, (args.Targets == null) ? t.Item3 : args.Targets);
#if DEBUG
                    if (IsDebugPrint)
                        Console.WriteLine($"\tBaseEvent Observer: {key}/{args.CallerPlaceName}/{args.CallerName} = {args.Value}/{args.ValueType}");
#endif
                } catch { }
            }).ConfigureAwait(false);
        }
    }
}
