﻿namespace AudioMonitor.Data.Events
{
    public enum BaseAction : int
    {
        None = 0,
        Error = 1,
        PlayerChannelsChange,
        ChannelSelected,
        ChannelIndexSelected,
        BaseUriChange,
        SettingsChange,
        LoadChange,
        LoadingChange,
        LoadingStat,
        SaveChange,
        SavingChange,
        ///
        EqLocalChange,
        EqChannelUpdate,
        VolumeChannelUpdate,
        EfAutoGainChange,
        EfBasBoostChannelUpdate,
        EfLoudnessChannelUpdate,
        EfVirtualizeChannelUpdate,
        ChannelUpdate,
        ///
        LoopPlayChange,
        AutoPlayChange,
        EqEnableChange,
        EqSavePresetCmd,
        ReloadCall,
        PlayerPlay,
        PlayerStop,
        PlayerPause,
        ///
        ButtonFade,
        CastSelected
    };
}
