﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace AudioMonitor.Data.Interfaces
{
    public interface ICloseApplication
    {
        void CloseApp();
    }
}
