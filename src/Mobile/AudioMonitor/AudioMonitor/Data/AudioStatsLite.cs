﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace AudioMonitor.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioStatsLite
    {
        [XmlElement("devices")]
        public List<AudioStatLite> Devices = new();
        [XmlElement("count")]
        public int Count = 0;
    }
}
