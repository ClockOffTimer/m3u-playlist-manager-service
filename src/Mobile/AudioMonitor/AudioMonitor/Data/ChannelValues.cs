﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.CommunityToolkit.Helpers;

namespace AudioMonitor.Data
{
    public enum ChannelValuesId : int
    {
        None = 0,
        TypeEq,
        TypeVolume,
        TypeBalance,
        TypeVirtualize,
        TypeBasBoost,
        TypeLoudness,
    };

    public class ChannelValues : INotifyPropertyChanged
    {
        private int _Id = 0;
        public int Id { get => _Id; set { if (_Id != value) _Id = value; OnChanged(nameof(Id)); }}

        private ChannelValuesId _TypeChannel = ChannelValuesId.None;
        public  ChannelValuesId TypeChannel { get => _TypeChannel; set { if (_TypeChannel != value) _TypeChannel = value; OnChanged(nameof(TypeChannel)); }}
        
        private float _Min = -20.0f;
        public  float Min { get => _Min; set { if (_Min != value) _Min = value; OnChanged(nameof(Min)); }}
        private float _Max = 20.0f;
        public  float Max { get => _Max; set { if (_Max != value) _Max = value; OnChanged(nameof(Max)); }}
        
        private bool _Visible = true;
        public  bool Visible { get => _Visible; set { if (_Visible != value) _Visible = value; OnChanged(nameof(Visible)); }}
        
        private float _Value = 0.0f;
        public  float Value { get => _Value; set { if (_Value != value) _Value = value; OnChanged(nameof(Value), nameof(IsEmpty)); }}
        public  float OldValue { get; set; } = 0.0f;
        
        public  bool IsEmpty => Value == 0.0f;
        public  bool IsParent => _Parent != null;
        private object _Parent = default(object);
        public  object Parent
        {
            get { object _obj = _Parent; _Parent = default(object); return _obj; }
        }

        #region Event INotifyPropertyChanged
        private readonly DelegateWeakEventManager _pcmgr = new DelegateWeakEventManager();
        public event PropertyChangedEventHandler PropertyChanged
        {
            add => _pcmgr.AddEventHandler(value);
            remove => _pcmgr.RemoveEventHandler(value);
        }
        public void OnChanged([CallerMemberName] string name = "") => _Rise(name);
        public void OnChanged(params string[] names)
        {
            foreach (var s in names) _Rise(s);
        }
        private void _Rise(string s) =>
            _pcmgr.RaiseEvent(this, new PropertyChangedEventArgs(s), nameof(PropertyChanged));
        #endregion

        public ChannelValues(int id, bool isvisible = true)
        {
            Id = id;
            Visible = isvisible;
        }
        public ChannelValues(float val, float min, float max, int id)
        {
            Id = id;
            Visible = true;
            Set(val, min, max);
        }
        public void Set(float val)
        {
            Value = val;
        }
        public void Set(float val, float min, float max)
        {
            Value = val;
            Min = min;
            Max = max;
        }
        public void Copy(ChannelValues ech, object parent = default)
        {
            if (Id != ech.Id)
                return;
            Value = ech.Value;
            Min = ech.Min;
            Max = ech.Max;
            _Parent = ech.IsParent ? ech.Parent : parent;
        }
        public void Default()
        {
            Value = 0.0f;
        }
        public void Mute(bool ismuted)
        {
            if (ismuted)
            {
                OldValue = Value;
                Value = Min;
            }
            else if (OldValue > Min)
            {
                Value = OldValue;
                OldValue = Min;
            }
        }
        public ChannelValues AddEvent(PropertyChangedEventHandler handler, ChannelValuesId type)
        {
            PropertyChanged += handler;
            TypeChannel = type;
            return this;
        }
        public override string ToString()
        {
            try {
                return $"{Id} = {Min}/{Max}, {Value}/{OldValue}, {Visible}/{IsEmpty}/{IsParent}";
            } catch { }
            return base.ToString();
        }
    }

    public static class ChannelValuesExtensions
    {
        public static bool IsEmptyValues(this ChannelValues [] list)
        {
            if (list == null)
                return true;
            for (int i = 0; i < list.Length; i++)
                if (!list[i].IsEmpty && (list[i].Value != 0.0f))
                    return false;
            return true;
        }
        public static bool FloatCompare(this float v, float v_, float b, float b_) => (v != v_) || (b != b_);
        public static bool FloatArrayCompare(this float[] f, float[] f_) => !Array.Equals(f, f_);
    }
}
