﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.Essentials;

namespace AudioMonitor.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class ValuesData
    {
        [XmlIgnore]
        public static readonly int Local = 0;
        [XmlIgnore]
        public static readonly int Serv = 1;
        [XmlIgnore]
        public static readonly int Min = 0;
        [XmlIgnore]
        public static readonly int Max = 1;
        [XmlIgnore]
        public static readonly int Value = 2;

        [XmlElement("bandcount")]
        public int[] BandCount = new int[] { 5, 10 };
        [XmlElement("presetcount")]
        public int[] PresetCount = new int[] { 5, 0 };
        [XmlElement("presetcurent")]
        public int[] PresetCurent = new int[] { -1, -1 };
        [XmlElement("eqranges")]
        public float[][] EqRanges  = new float[][] {
            new float [] {-1500.0f, 1500.0f, 0.0f },
            new float [] { -20.0f, 20.0f, 0.0f },
        };
        [XmlElement("volranges")]
        public float[][] VolRanges = new float[][] {
            new float [] { 0.0f, 1.0f, 0.5f },
            new float [] { 0.0f, 1.0f, 0.5f },
        };
        [XmlElement("eqdefault")]
        public float[] EqLevelsDefault = default;

        [XmlElement("complette")]
        public bool IsComplette       = false;

        public ValuesData() { }
        public ValuesData(bool _) => Load();

        public void Copy(ValuesData data)
        {
            if ((data == null) || !data.IsComplette)
                return;

            BandCount[Local] = data.BandCount[Local];
            BandCount[Serv]  = data.BandCount[Serv];
            
            PresetCount[Local] = data.PresetCount[Local];
            PresetCount[Serv] = data.PresetCount[Serv];

            PresetCurent[Local] = data.PresetCurent[Local];
            PresetCurent[Serv] = data.PresetCurent[Serv];

            EqRanges[Local][Min] = data.EqRanges[Local][Min];
            EqRanges[Local][Max] = data.EqRanges[Local][Max];
            EqRanges[Local][Value] = data.EqRanges[Local][Value];
            EqRanges[Serv][Min] = data.EqRanges[Serv][Min];
            EqRanges[Serv][Max] = data.EqRanges[Serv][Max];
            EqRanges[Serv][Value] = data.EqRanges[Serv][Value];

            VolRanges[Local][Min] = data.VolRanges[Local][Min];
            VolRanges[Local][Max] = data.VolRanges[Local][Max];
            VolRanges[Local][Value] = data.VolRanges[Local][Value];
            VolRanges[Serv][Min] = data.VolRanges[Serv][Min];
            VolRanges[Serv][Max] = data.VolRanges[Serv][Max];
            VolRanges[Serv][Value] = data.VolRanges[Serv][Value];
            IsComplette = true;
        }
        public void Copy(IPlayerEqSettings data)
        {
            BandCount[Local] = data.Bands;
            PresetCount[Local] = data.Preset;
            PresetCurent[Local] = data.PresetCurent;
            if (data.BandRanges != null)
            {
                EqRanges[Local][Min] = (data.BandRanges.Length > Min) ? data.BandRanges[Min] : EqRanges[Local][Min];
                EqRanges[Local][Max] = (data.BandRanges.Length > Max) ? data.BandRanges[Max] : EqRanges[Local][Max];
                EqRanges[Local][Value] = (data.BandRanges.Length > Value) ? data.BandRanges[Value] : EqRanges[Local][Value];
            }
            EqLevelsDefault = data.BandLevels;
            IsComplette = true;
        }
        public async void Save()
        {
            await Task.Run(() => {
                try {
                    UTF8Encoding utf = new UTF8Encoding(false);
                    using MemoryStream ms = new MemoryStream();
                    using StreamWriter sr = new(ms, utf);
                    XmlSerializer xml = new(typeof(ValuesData));
                    xml.Serialize(sr, this);
                    string s = utf.GetString(ms.ToArray());
                    if (string.IsNullOrWhiteSpace(s))
                        return;

                    Preferences.Set(nameof(ValuesData), s);
                } catch (Exception ex) { LogWriter.Debug<ValuesData>(nameof(Save), ex); }
            });
        }
        public async void Load()
        {
            await Task.Run(() => {
                try {
                    string s = Preferences.Get(nameof(ValuesData), string.Empty);
                    if (string.IsNullOrWhiteSpace(s))
                        return;

                    UTF8Encoding utf = new UTF8Encoding(false);
                    using MemoryStream ms = new MemoryStream(utf.GetBytes(s));
                    using StreamReader sr = new(ms, utf, false);
                    XmlSerializer xml = new(typeof(ValuesData));
                    this.Copy(xml.Deserialize(sr) as ValuesData);

                } catch (Exception ex) { LogWriter.Debug<ValuesData>(nameof(Load), ex); }
            });
        }
    }
}