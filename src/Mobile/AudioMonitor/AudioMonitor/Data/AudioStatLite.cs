﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace AudioMonitor.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioStatLite
    {
        [XmlElement("id")]
        public int Id { get; set; } = -1;
        [XmlElement("device")]
        public string Device { get; set; } = string.Empty;
        [XmlElement("clients")]
        public List<string> Clients { get; set; } = new();
        [XmlElement("count")]
        public int Count { get; set; } = 0;
    }
}
