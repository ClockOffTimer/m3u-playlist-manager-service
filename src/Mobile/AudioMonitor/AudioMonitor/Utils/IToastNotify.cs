﻿using System;

namespace AudioMonitor.Utils
{
    public interface IToastNotify
    {
        void Show(Exception e);
        void Show(string s);
    }
}