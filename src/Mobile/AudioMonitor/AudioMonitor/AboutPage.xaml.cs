﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using Plugin.LightAudioPlayer.Base.Events;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AudioMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        private readonly AboutPageView AboutPageView;
        public AboutPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            AboutPageView = (AboutPageView)BindingContext;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<object, IEventAudioPlayerArgs>(this, EventAudioPlayerArgs.MessagingCenterId, (s, args) => {
                if (MainThread.IsMainThread)
                    AboutPageView.AddEventToMonitor = args;
                else
                    MainThread.BeginInvokeOnMainThread(() => {
                        AboutPageView.AddEventToMonitor = args;
                    });
            });
        }
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            switch (DeviceDisplay.MainDisplayInfo.Orientation)
            {
                case DisplayOrientation.Landscape: AboutPageView.IsOrientation = true; break;
                case DisplayOrientation.Portrait: AboutPageView.IsOrientation = false; break;
            }
        }
    }
}