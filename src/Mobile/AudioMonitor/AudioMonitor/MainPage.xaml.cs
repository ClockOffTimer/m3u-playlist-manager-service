﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.Linq;
using AudioMonitor.Data;
using AudioMonitor.Data.Events;
using AudioMonitor.Media;
using AudioMonitor.Utils;
using Plugin.LightAudioPlayer.Base.Utils;
using Plugin.LightAudioPlayer.Base.Events;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.PlayList;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.LightAudioPlayer.Base.Enums;
using GoogleCast.Channels;
using GoogleCast.Models.Media;
using GoogleCast;
using Plugin.LightAudioPlayer.Base.Player;
using System.Collections.Generic;

namespace AudioMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage, IDisposable
    {
        private readonly MainPageView PageView;
        private readonly ICrossAudioPlayer Control;
        private readonly ManagerChannels ChannelsManager = ManagerChannels.Instance;
        private readonly BaseEventObserver<BaseAction> MediaControlObserver;
        private readonly BaseEventObserver<BaseAction> PageViewObserver;
        private List<PlayListItem> SavedPlayList = default;
        private int SavedTrackSelected = -1;
        private string GetUrl(Uri u) => $"{u.Scheme}://{u.DnsSafeHost}:{u.Port}";

        public MainPage()
        {
            Control = DependencyService.Get<ICrossAudioPlayer>();
            if (Control == null)
                throw new NullReferenceException(nameof(Control));

            MessagingCenter.Subscribe<object, IEventAudioPlayerArgs>(this, EventAudioPlayerArgs.MessagingCenterId, (s, args) => {
                if (MainThread.IsMainThread)
                    OnControl_EventCb(s, args);
                else
                    MainThread.BeginInvokeOnMainThread(() => {
                        OnControl_EventCb(s, args);
                    });
            });

            MediaControlObserver = new BaseEventObserver<BaseAction>(ChannelsManager)
                .NoDuplicate(true)
                .SetDebugPrint(true);

            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            PageView = (MainPageView)BindingContext;
            PageViewObserver = new BaseEventObserver<BaseAction>(PageView.GetBaseEvent)
                .NoDuplicate(true)
                .SetDebugPrint(true);
        }
        ~MainPage()
        {
            Dispose();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            Connectivity.ConnectivityChanged += Connectivity_ConnectivityChanged;

            MediaControlObserver.Clear();
            MediaControlObserver.Add(
                BaseAction.Error,
                (o, t, l) => {
                    if (o is Exception ex)
                        Dispatcher.BeginInvokeOnMainThread(() => {
                            PageView.UiStateError = ex.Message;
                            PageView.IsLoading = false;
                            DependencyService.Get<IToastNotify>()?.Show(ex);
                        });
                },
                default);
            MediaControlObserver.Add(
                BaseAction.PlayerChannelsChange,
                async (o, t, l) => {
                    if ((o is int i) && (i > 0))
                    {
                        await Control.StopAsync();

                        if (SavedPlayList != null)
                            foreach (PlayListItem item in ChannelsManager.PlayerChannels)
                            {
                                PlayListItem saved = (from m in SavedPlayList
                                                      where m.Url.Equals(item.Url)
                                                      select m).FirstOrDefault();
                                if (saved != null) {
                                    item.Balance = saved.Balance;
                                    item.VolumeLocal = saved.Volume;
                                    item.EqualizersLocal = saved.EqualizersLocal;
                                }
                            }

                        await Control.LoadAsync(ChannelsManager.PlayerChannels.ToList());

                        Dispatcher.BeginInvokeOnMainThread(() => {
                            PageView.UiChannels = new ObservableCollection<PlayListItem>(ChannelsManager.PlayerChannels);
                            if ((SavedTrackSelected >= 0) && (SavedTrackSelected < PageView.UiChannels.Count))
                                PageView.UiChannelSelectedIndex = SavedTrackSelected;
                        });
                    }
                },
                default);
            MediaControlObserver.Add(
                BaseAction.ChannelIndexSelected,
                async (o, t, l) => {
                    if (o is int i)
                    {
                        if (i == -1)
                            Dispatcher.BeginInvokeOnMainThread(() => PageView.UiChannelSelected = default);
                        else if (i >= 0)
                            await Control.PlayAsync(i);
                    }
                },
                default);
            MediaControlObserver.Add(
                BaseAction.ChannelSelected,
                async (o, t, l) => {
                    if (o is PlayListItem pli)
                    {
                        if (pli == default)
                            Dispatcher.BeginInvokeOnMainThread(() => PageView.UiChannelSelected = default);
                        else if (pli.Id >= 0)
                            await Control.PlayAsync(pli.Id);
                    }
                },
                default);
            MediaControlObserver.Add(
                BaseAction.SettingsChange,
                (o, t, l) => {
                    Dispatcher.BeginInvokeOnMainThread(() => PageView.IsSettings = (o is bool b) ? b : false);
                    if ((o is bool b) && (!b) && (SavedPlayList != null) && (SavedPlayList.Count > 0))
                    {
                        try {
                            ChannelsManager.BaseUri = GetUrl(new Uri(SavedPlayList[0].Url));
                        } catch { }
                    }
                },
                default);
            MediaControlObserver.Add(
                BaseAction.BaseUriChange,
                (o, t, l) => {
                    if (o is string s)
                        LogWriter.Debug<MainPage>($"{nameof(BaseAction.BaseUriChange)} -> Change -> {s}");
                },
                default);
            MediaControlObserver.Add(
                BaseAction.LoadChange,
                (o, t, l) => { Dispatcher.BeginInvokeOnMainThread(() => PageView.IsLoad = (o is bool b) ? b : false); },
                default);
            MediaControlObserver.Add(
                BaseAction.LoadingChange,
                (o, t, l) => { Dispatcher.BeginInvokeOnMainThread(() => PageView.IsLoading = (o is bool b) ? b : false); },
                default);
            MediaControlObserver.Add(
                BaseAction.LoadingStat,
                (o, t, l) => { Dispatcher.BeginInvokeOnMainThread(() => PageView.AudioStatsList = (o is AudioStatsLite a) ? a : null); },
                default);

            PageViewObserver.Clear();
            PageViewObserver.Add(
                BaseAction.Error,
                (o, t, l) => {
                    if (o is Exception ex) {
                        LogWriter.Debug<MainPage>(nameof(BaseAction.Error), ex);
                        Dispatcher.BeginInvokeOnMainThread(() => DependencyService.Get<IToastNotify>()?.Show(ex));
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.BaseUriChange,
                (o, t, l) => {
                    if ((!ChannelsManager.IsLoading) && (o is string s))
                    {
                        ChannelsManager.BaseUri = s;
                        try { Preferences.Set(StaticSettings.KeySettingsUri, s); }
                        catch (Exception e) { PageView.UiStateError = (e != null) ? e.Message : string.Empty; }
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.ChannelSelected,
                (o, t, l) => { if (o is PlayListItem c) ChannelsManager.ChannelSelected = c; },
                default);
            PageViewObserver.Add(
                BaseAction.ChannelUpdate,
                (o, t, l) => {
                    if (o is Tuple<PlayListItem, bool> tuple) {
                        if (tuple.Item1 == default)
                            return;
                        ChannelsManager.ChannelUpdate = tuple.Item1;
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.EqEnableChange,
                async (o, t, l) => {
                    if (o is bool b) {
                        await Control.EqEnableAsync(b);
                        Preferences.Set(StaticSettings.KeySettingsEqEnable, b);
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.AutoPlayChange,
                async (o, t, l) => {
                    if (o is bool b) {
                        await Control.AutoPlayAsync(b);
                        Preferences.Set(StaticSettings.KeySettingsAutoPlay, b);
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.LoopPlayChange,
                async (o, t, l) => {
                    if (o is bool b) {
                        await Control.LoopPlayAsync(b);
                        Preferences.Set(StaticSettings.KeySettingsLoopPlay, b);
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.VolumeChannelUpdate,
                async (o, t, l) => {
                    if ((o is Tuple<int, float, float, bool> tuple) && tuple.Item4)
                        await Control.VolumeAsync(new float[] { tuple.Item2, tuple.Item3 });
                },
                default);
            PageViewObserver.Add(
                BaseAction.EqChannelUpdate,
                async (o, t, l) => {
                    if (o is Tuple<int, float [], bool> tuple)
                        await Control.EqChannelsAsync(tuple.Item2);
                },
                default);
            PageViewObserver.Add(
                BaseAction.EqLocalChange,
                (o, t, l) => {
                    if (o is bool b)
                        Preferences.Set(StaticSettings.KeySettingsEqLocal, b);
                },
                default);
            PageViewObserver.Add(
                BaseAction.EfVirtualizeChannelUpdate,
                async (o, t, l) => {
                    if ((o is Tuple<int, float, bool> tuple) && tuple.Item3)
                        await Control.EfVirtualizerAsync((int)Math.Round((float)tuple.Item2));
                },
                default);
            PageViewObserver.Add(
                BaseAction.EfLoudnessChannelUpdate,
                async (o, t, l) => {
                    if ((o is Tuple<int, float, bool> tuple) && tuple.Item3)
                        await Control.EfLoudnessAsync((int)Math.Round((float)tuple.Item2));
                },
                default);
            PageViewObserver.Add(
                BaseAction.EfBasBoostChannelUpdate,
                async (o, t, l) => {
                    if ((o is Tuple<int, float, bool> tuple) && tuple.Item3)
                        await Control.EfBasBoostAsync((int)Math.Round((float)tuple.Item2));
                },
                default);
            PageViewObserver.Add(
                BaseAction.EfAutoGainChange,
                async (o, t, l) => {
                    if (o is bool b)
                        await Control.EfAutoGainAsync(b);
                },
                default);

            /* UI Effect */
            PageViewObserver.Add(
                BaseAction.ButtonFade,
                async (o, t, l) => {
                    if (o is Tuple<BaseAction, bool> tuple) {
                        Label btn;
                        switch (tuple.Item1)
                        {
                            case BaseAction.PlayerPlay:  btn = (Label)BtnIconPlay; break;
                            case BaseAction.PlayerPause: btn = (Label)BtnIconPause; break;
                            case BaseAction.PlayerStop:  btn = (Label)BtnIconStop; break;
                            default: return;
                        }
                        await btn.FadeTo(tuple.Item2 ? 1 : 0, 950);
                    }
                },
                default);

            /* Command */
            PageViewObserver.Add(
                BaseAction.CastSelected,
                async (o, t, l) => {
                    if (o is Tuple<IReceiver, PlayListItem> tuple)
                    {
                        if ((tuple.Item1 == null) || (tuple.Item2 == null))
                            return;
                        try
                        {
                            var sender = new Sender();
                            await sender.ConnectAsync(tuple.Item1);
                            var mediaChannel = sender.GetChannel<IMediaChannel>();
                            await sender.LaunchAsync(mediaChannel);
                            var mediaStatus = await mediaChannel.LoadAsync(
                                new MediaInformation() { ContentId = tuple.Item2.Url });
                        }
                        catch (Exception ex) { LogWriter.Debug<MainPage>($"{nameof(BaseAction.CastSelected)}", ex); }
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.ReloadCall,
                async (o, t, l) => {
                    if (!ChannelsManager.IsSettings || ChannelsManager.IsLoading)
                    {
                        if (!ChannelsManager.IsSettings)
                            PageView.IsLoading = false;
                        return;
                    }
                    await ChannelsManager.LoadAsync().ConfigureAwait(false);
                },
                default);
            PageViewObserver.Add(
                BaseAction.EqSavePresetCmd,
                async (o, t, l) => { if ((o is bool b) && b) { await ChannelsManager.SaveAsync().ConfigureAwait(false); } },
                default);
            PageViewObserver.Add(
                BaseAction.LoadingStat,
                async (o, t, l) => { if ((o is bool b) && b) await ChannelsManager.LoadStatAsync().ConfigureAwait(false); },
                default);
            PageViewObserver.Add(
                BaseAction.PlayerPlay,
                async (o, t, l) => {
                    if (o is bool)
                    {
                        if (ChannelsManager.IndexSelected >= 0)
                            await Control.PlayAsync(ChannelsManager.IndexSelected);
                        else
                            await Control.StopAsync();
                    }
                },
                default);
            PageViewObserver.Add(
                BaseAction.PlayerStop,
                async (o, t, l) => { if ((o is bool b) && b) await Control.StopAsync(); },
                default);
            PageViewObserver.Add(
                BaseAction.PlayerPause,
                async (o, t, l) => { if ((o is bool b) && b) await Control.PauseAsync(); },
                default);


            MediaControlObserver.Start();
            ChannelsManager.HistoryCall();
            PageView.HistoryClear();
            PageViewObserver.Start();

            try {
                if (Preferences.ContainsKey(StaticSettings.KeySettingsEqLocal))
                    PageView.IsEqLocalCtrl = Preferences.Get(StaticSettings.KeySettingsEqLocal, true);

                if (Preferences.ContainsKey(StaticSettings.KeySettingsUri)) {
                    string s = Preferences.Get(StaticSettings.KeySettingsUri, string.Empty);
                    if (!string.IsNullOrWhiteSpace(s) && Uri.IsWellFormedUriString(s, UriKind.Absolute))
                        PageView.BaseUri = s;
                    else
                        PageView.BaseUri = string.Empty;
                }

                _ = ChannelsManager.GetStatus();

                await Control.GetServiceSettingsAsync();
                await Control.EqSettingsAsync();
            }
            catch (Exception ex) { LogWriter.Debug<MainPage>($"{nameof(OnAppearing)}", ex); }
        }

        public void Dispose()
        {
        }

        protected override async void OnDisappearing()
        {
            if (Preferences.ContainsKey(StaticSettings.KeyCloseServiceOnExit))
                if (Preferences.Get(StaticSettings.KeyCloseServiceOnExit, false))
                    await Control.StopForegroundServiceCompatAsync();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            switch (DeviceDisplay.MainDisplayInfo.Orientation)
            {
                case DisplayOrientation.Landscape: PageView.IsOrientation = true; break;
                case DisplayOrientation.Portrait:  PageView.IsOrientation = false; break;
            }
        }

        public void SetSettingsValues(AudioPlayerSettings aps)
        {
            if (aps == null)
                return;
            try
            {
                PageView.ValuesChacheReset();
                PageView.IsLoopPlayCtrl = aps.LoopPlay;
                PageView.IsAutoPlayCtrl = aps.AutoPlay;
                PageView.IsEfAutoGain   = aps.EfAutoGain;
                PageView.IsEqEnableCtrl = (!PageView.IsEqEnableCtrl) ? aps.EqEnable : PageView.IsEqEnableCtrl;
                if (aps.EfLoudness >= 0)
                {
                    PageView.IsEfLoudness = true;
                    PageView.EfLoudnessChannel.Value = aps.EfLoudness;
                }
                if (aps.EfBasBoost >= 0)
                {
                    PageView.IsEfBasBoost = true;
                    PageView.EfBasBoostChannel.Value = aps.EfBasBoost;
                }
                if (aps.EqPreset >= 0)
                {
                    switch (aps.EqPreset)
                    {
                        case  0: EqPreset0.IsChecked = true; break;
                        case  1: EqPreset1.IsChecked = true; break;
                        case  2: EqPreset2.IsChecked = true; break;
                        case  3: EqPreset3.IsChecked = true; break;
                        case  4: EqPreset4.IsChecked = true; break;
                        default: EqPresetOff.IsChecked = true; break;
                    }
                    PageView.IsEqEnableCtrl = !EqPresetOff.IsChecked;
                }
                if (aps.EfVirtualizer >= 0)
                {
                    if (aps.EfVirtualizerMode >= 0)
                        switch (aps.EfVirtualizerMode)
                        {
                            case 0: EfVirtualize0.IsChecked = true; break;
                            case 1: EfVirtualize1.IsChecked = true; break;
                            case 2: EfVirtualize2.IsChecked = true; break;
                            default: EfVirtualizeOff.IsChecked = true; break;
                        }

                    PageView.IsEfVirtualizeEnable = !EfVirtualizeOff.IsChecked;
                    PageView.EfVirtualizeChannel.Value = (float)aps.EfVirtualizer;
                }
                if ((aps.VolumeChannel != null) && (aps.VolumeChannel.Length >= 2))
                {
                    PageView.VolumeChannel.Value = aps.VolumeChannel[0];
                    PageView.BalanceChannel.Value = aps.VolumeChannel[1];
                }
                if (aps.EqChannels != null)
                    PageView.EqChannelsLocalValues = aps.EqChannels;

                if ((aps.PlayList != null) && (aps.PlayList.Count > 0))
                {
                    if (!PageView.IsSettings && !ChannelsManager.IsSettings && string.IsNullOrWhiteSpace(ChannelsManager.BaseUri))
                        ChannelsManager.BaseUri = GetUrl(new Uri(aps.PlayList[0].Url));
                    SavedPlayList = aps.PlayList;
                }
                if ((aps.PlayIdx >= 0) && (PageView.UiChannels.Count > 0))
                    PageView.UiChannelSelectedIndex = aps.PlayIdx;

            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(SetSettingsValues), ex); }
        }

        #region events
        private async void Connectivity_ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            NetworkAccess state = e.NetworkAccess;
            if ((state == NetworkAccess.Unknown) || (state == NetworkAccess.None)) {
                Dispatcher.BeginInvokeOnMainThread(() => {
                    PageView.UiStateError = Properties.Resources.NetworkError;
                    PageView.PlayerState = StateAudioPlayerId.Error;
                });
                await Control.StopAsync();
            }
        }

        private async void OnControl_EventCb(object sender, IEventAudioPlayerArgs args)
        {
#           if DEBUG
            Console.WriteLine($"*** RECEIVEFROMEVENT ({nameof(MainPage)}): {args} ***");
#           endif

            try {
                do {
                    StateAudioPlayerId state;

                    if ((args.EventId == EventAudioPlayerId.EvService) &&
                        (args.StateId == StateAudioPlayerId.Settings) &&
                        (args.Obj is AudioPlayerSettings aps))
                    {
                        SetSettingsValues(aps);
                        break;
                    }

                    (string se, int ie) = args.PlayErrorFilterEvent();
                    if (!string.IsNullOrWhiteSpace(se))
                    {
                        PageView.UiStateError = se;
                        if (args.EventId == EventAudioPlayerId.EvError)
                            PageView.PlayerState = StateAudioPlayerId.Error;
                        break;
                    }
                    if ((state = args.ServiceStatusFilterEvent()) != StateAudioPlayerId.None)
                    {
                        PageView.ServiceState = state;
                        break;
                    }
                    (state, bool[] btn) = args.PlayStatusFilterEvent();
                    if (state != StateAudioPlayerId.None)
                    {
                        PageView.PlayerState = state;
                        if (btn == null)
                            break;
                    }
                    if ((btn != null) && (btn.Length == 6))
                    {
                        PageView.IsPlayerPlay  = btn[1];
                        PageView.IsPlayerPause = btn[2];
                        PageView.IsPlayerStop  = btn[3];

                        if (btn[5] && (ChannelsManager.Count() > 0))
                            await Control.LoadAsync(ChannelsManager.PlayerChannels.ToList());
                        break;
                    }
                    (TimeSpan? tm, int i) = args.PlayTimeFilterEvent();
                    if ((tm != null) && (tm != TimeSpan.Zero))
                    {
                        PageView.Duration = (TimeSpan)tm;
                        break;
                    }
                    (string s, int n, Uri u) = args.PlayTrackFilterEvent();
                    if ((u != null) && (s != null) && (n >= 0))
                    {
                        PageView.DeviceName = s;
                        PageView.UiChannelSelectedIndex = n;
                        break;
                    }
                    if (args.EqSettingsFilterEvent() is IPlayerEqSettings eqs)
                        PageView.SlidersSettings = eqs;

                    switch (args.EventId)
                    {
                        case EventAudioPlayerId.EqEnableStatus:
                            PageView.IsEqEnableCtrl = args.IsValue; break;
                        case EventAudioPlayerId.EfAutoGainEnableStatus:
                            PageView.IsEfAutoGain = args.IsValue; break;
                        case EventAudioPlayerId.EfBassBoostEnableStatus:
                            PageView.IsEfBasBoost = args.IsValue; break;
                        case EventAudioPlayerId.EfLoudnessEnableStatus:
                            PageView.IsEfLoudness = args.IsValue; break;
                        case EventAudioPlayerId.EfVirtualizeEnableStatus:
                            PageView.IsEfVirtualizeEnable = args.IsValue; break;
                    }

                } while (false);
            } catch (Exception ex) { LogWriter.Debug<MainPage>($"{nameof(OnControl_EventCb)}", ex); }
        }

        private async void EqPreset_Changed(object sender, CheckedChangedEventArgs e)
        {
            try
            {
                if ((sender is RadioButton rb) && (rb.Value != null) && (e != null) && e.Value)
                    if (int.TryParse(rb.Value.ToString(), out int idx))
                    {
                        bool b = idx >= 0;
                        if (MainThread.IsMainThread)
                            PageView.IsEqEnableCtrl = b;
                        else
                            MainThread.BeginInvokeOnMainThread(() =>
                                PageView.IsEqEnableCtrl = b);

                        if (b)
                            await Control.EqPresetAsync(idx);
                        else
                            await Control.EqEnableAsync(false);
                    }
            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(EqPreset_Changed), ex); }
        }

        private async void EfVirtualize_Changed(object sender, CheckedChangedEventArgs e)
        {
            try
            {
                if ((sender is RadioButton rb) && (rb.Value != null) && (e != null) && e.Value)
                    if (int.TryParse(rb.Value.ToString(), out int idx))
                    {
                        bool b = idx >= 0;
                        if (MainThread.IsMainThread)
                            PageView.IsEfVirtualizeEnable = b;
                        else
                            MainThread.BeginInvokeOnMainThread(() =>
                                PageView.IsEfVirtualizeEnable = b);
                        await Control.EfVirtualizerModeAsync(idx);
                    }
            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(EfVirtualize_Changed), ex); }
        }

        private void Editor_Completed(object sender, EventArgs e)
        {
            try
            {
                string s = default;
                if (sender is Editor ed)
                    s = ed.Text.Trim();
                else
                    return;

                if (!string.IsNullOrWhiteSpace(s))
                    if (Uri.IsWellFormedUriString(s, UriKind.Absolute))
                    {
                        s = s.EndsWith("/") ? s.Substring(0, s.Length - 1) : s;
                        ed.Text = s;
                        PageView.BaseUri = s;
                        PageView.IsBaseUriExpander = false;
                    }
                    else
                        ed.Text = string.Empty;
            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(Editor_Completed), ex); }
        }

        private void Editor_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender is Editor ed)
                    if ((e != null) && (e.NewTextValue != null) && (e.NewTextValue.LastIndexOf('\n') != -1))
                    {
                        ed.Text = ed.Text.Trim();
                        ed.Unfocus();
                    }
            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(Editor_TextChanged), ex); }
        }

        private void Editor_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                if ((sender is Editor) && (e != null) && !e.IsFocused)
                    PageView.IsBaseUriExpander = false;

            } catch (Exception ex) { LogWriter.Debug<MainPage>(nameof(Editor_Unfocused), ex); }
        }
        #endregion
    }
}
