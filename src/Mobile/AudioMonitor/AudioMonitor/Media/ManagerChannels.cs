﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AudioMonitor.Data;
using AudioMonitor.Data.Events;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AudioMonitor.Media
{
#   pragma warning disable CS8604
    public class ManagerChannels : BaseEvent<BaseAction>
    {
        private bool _loading = false;
        private string _baseUri = string.Empty;
        public  static ManagerChannels Instance { get; private set; } = new ManagerChannels();
        public  void HistoryCall() { NotifySend(BaseAction.None, null); }

        public bool IsIndexValid() => IsIndexValid(_indexSelected);
        public bool IsIndexValid(int idx) => (idx >= 0) && (idx < playerChannels.Count);
        public PlayListItem GetChannel() => ChannelSelected;

        public ManagerChannels()
        {
            base.SetCallerPlace(nameof(ManagerChannels));
            base.SetDebugPrint(true);
        }

        #region Get/Set
        public IEnumerable<PlayListItem> PlayerChannels => _playerChannels.AsEnumerable();
        private IList<PlayListItem> _playerChannels = new List<PlayListItem>();
        private IList<PlayListItem> playerChannels
        {
            get => _playerChannels;
            set {
                if (value == null)
                {
                    _playerChannels.Clear();
                    IsLoad = false;
                }
                else if (value == _playerChannels)
                    return;
                else
                    _playerChannels = value;
                NotifySend(BaseAction.PlayerChannelsChange, (value == null) ? 0 : value.Count);
            }
        }
        public PlayListItem ChannelSelected
        {
            get => (IsIndexValid()) ? playerChannels[_indexSelected] : default;
            set
            {
                if (value == null)
                    IndexSelected = -1;
                else if (value.Id == _indexSelected)
                    return;
                else
                {
                    IndexSelected = ((from i in playerChannels
                                       where i.Id == value.Id
                                       select i).FirstOrDefault() != null) ? value.Id : -1;
                }
                NotifySend(BaseAction.ChannelSelected, value);
            }
        }
        public PlayListItem ChannelUpdate
        {
            set
            {
                try {
                    if (value == null)
                        return;

                    var pli = (from i in _playerChannels
                               where i.Id == value.Id
                               select i).FirstOrDefault();
                    if (pli == null)
                        return;
                    pli.Copy(value);
                } catch { }
            }
        }
        private int _indexSelected = -1;
        public int IndexSelected
        {
            get => _indexSelected;
            set
            {
                if (_indexSelected == value)
                    return;
                _indexSelected = value;
                NotifySend(BaseAction.ChannelIndexSelected, value);
            }
        }
        public string BaseUri
        {
            get => _baseUri;
            set
            {
                do
                {
                    if (string.IsNullOrWhiteSpace(_baseUri) && string.IsNullOrWhiteSpace(value))
                        break;
                    if ((_baseUri != null) && (value != null) && _baseUri.Equals(value))
                        break;

                    IsSettings = IsLoad = false;

                    if (string.IsNullOrWhiteSpace(value))
                        _baseUri = string.Empty;
                    else
                    {
                        _baseUri = value;
                        IsSettings = true;
                    }
                    NotifySend(BaseAction.BaseUriChange, value);
                    Load();

                } while (false);
            }
        }
        private bool _isSettings = false;
        public bool IsSettings
        {
            get => _isSettings;
            set {
                if (_isSettings == value)
                    return;
                _isSettings = value;

                if (!_loading && _isSettings && CheckSettings())
                    Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                    {
                        Load();
                        return false;
                    });
                else
                    BaseUri = string.Empty;
                NotifySend(BaseAction.SettingsChange, value);
            }
        }
        private bool _isLoad = false;
        public bool IsLoad
        {
            get => _isLoad;
            set {
                if (_isLoad == value)
                    return;
                _isLoad = value;

                if (_isLoad && (CountChannels > 0))
                    NotifySend(BaseAction.PlayerChannelsChange, CountChannels);
                else
                    playerChannels.Clear();
                _indexSelected = -1;
                NotifySend(BaseAction.LoadChange, value);
            }
        }
        public int CountChannels => playerChannels.Count;
        public bool IsLoading => _loading;
        #endregion

        public bool Add(PlayListItem pli, int count, string host)
        {
            pli.CheckToDefault();
            pli.Id = count;
            pli.Host = host;
            pli.Url = StaticSettings.BuildServiceUrl(_baseUri, count);
            playerChannels.Add(pli);
            return true;
        }
        public bool GetStatus()
        {
            do
            {
                if (!IsSettings)
                    break;
                NotifySend(BaseAction.SettingsChange, IsSettings);

                if (string.IsNullOrWhiteSpace(BaseUri))
                    break;
                NotifySend(BaseAction.BaseUriChange, BaseUri);

                if (!IsLoad)
                    break;
                NotifySend(BaseAction.LoadChange, IsLoad);

                if (CountChannels == 0)
                    break;
                NotifySend(BaseAction.PlayerChannelsChange, _playerChannels.Count);
                return true;

            } while (false);
            return false;
        }

        #region Load/Reload
        public async void CheckIsLoad()
        {
            NotifySend(BaseAction.LoadingChange, _loading);
            NotifySend(BaseAction.SettingsChange, IsSettings);
            NotifySend(BaseAction.LoadChange, IsLoad);

            if (IsSettings && IsLoad && (CountChannels > 0))
                NotifySend(BaseAction.PlayerChannelsChange, CountChannels);
            else if (IsSettings && !IsLoad)
                await LoadAsync().ConfigureAwait(false);
        }

        public async void Load()
        {
            await LoadAsync().ConfigureAwait(false);
        }
        public async Task LoadAsync()
        {
            if (_loading)
                return;
            _loading = true;

            if (!CheckSettings())
            {
                _loading = false;
                return;
            }
                
            IsLoad = false;
            NotifySend(BaseAction.LoadingChange, _loading);

            await Task.Run(async () =>
            {
                try
                {
                    do
                    {
                        string s = string.Empty;
                        try {
                            using HttpClient client = new HttpClient();
                            s = await client.GetStringAsync(StaticSettings.BuildServiceUrl(_baseUri));
                        }
                        catch (Exception e) {
                            LogWriter.Debug<ManagerChannels>(nameof(LoadAsync), e);
                            NotifySend(BaseAction.Error, e);
                            break;
                        }

                        if (string.IsNullOrEmpty(s))
                            break;

                        UTF8Encoding utf = new UTF8Encoding(false);
                        using MemoryStream ms = new MemoryStream(utf.GetBytes(s));
                        using StreamReader sr = new(ms, utf, false);
                        XmlSerializer xml = new(typeof(AudioSettingsLite));
                        AudioSettingsLite channels = xml.Deserialize(sr) as AudioSettingsLite;

                        if ((channels == null) || (channels.Channels.Count == 0))
                            break;

                        playerChannels.Clear();

                        string baseUriHost = ParseDefaultUri(BaseUri);
                        if (string.IsNullOrWhiteSpace(baseUriHost))
                            baseUriHost = BaseUri;

                        foreach (var channel in channels.Channels)
                            await Task.FromResult(Add(channel, playerChannels.Count(), baseUriHost));

                    } while (false);
                }
                catch (Exception e) {
                    LogWriter.Debug<ManagerChannels>(nameof(LoadAsync), e);
                    NotifySend(BaseAction.Error, e);
                }
                IsLoad = playerChannels.Count > 0;
                _loading = false;
                NotifySend(BaseAction.LoadingChange, _loading);
            }).ConfigureAwait(false);
        }
        #endregion

        #region Save to service
        public async void Save()
        {
            await SaveAsync().ConfigureAwait(false);
        }
        public async Task SaveAsync()
        {
            PlayListItem pli = ChannelSelected;
            if (_loading || (pli == null))
                return;
            _loading = true;

            NotifySend(BaseAction.SavingChange, _loading);

            await Task.Run(async () =>
            {
                try
                {
                    do
                    {
                        string s = string.Empty;
                        UTF8Encoding utf = new UTF8Encoding(false);
                        AudioChannelLite acl = new AudioChannelLite().Copy(pli);
                        if (acl == null)
                            break;

                        try
                        {
                            using MemoryStream ms = new MemoryStream();
                            using StreamWriter sr = new(ms, utf);
                            XmlSerializer xml = new(typeof(AudioChannelLite));
                            xml.Serialize(sr, acl);
                            s = utf.GetString(ms.ToArray());
                        }
                        catch (Exception e)
                        {
                            LogWriter.Debug<ManagerChannels>(nameof(SaveAsync), e);
                            NotifySend(BaseAction.Error, e);
                            break;
                        }

                        if (string.IsNullOrWhiteSpace(s))
                            break;

                        try
                        {
                            using HttpClient client = new HttpClient();
                            HttpResponseMessage r = await client.PostAsync(
                                StaticSettings.BuildServicePostUrl(_baseUri, pli.Id),
                                new StringContent(s, utf, "text/xml"));
#                           if DEBUG
                            string result = r.Content.ReadAsStringAsync().Result;
                            LogWriter.Debug<ManagerChannels>($"{nameof(SaveAsync)} -> {result}");
#                           endif
                            try { r.Dispose(); } catch { }
                        }
                        catch (Exception e)
                        {
                            LogWriter.Debug<ManagerChannels>(nameof(SaveAsync), e);
                            NotifySend(BaseAction.Error, e);
                            break;
                        }

                    } while (false);
                }
                catch (Exception e)
                {
                    LogWriter.Debug<ManagerChannels>(nameof(LoadAsync), e);
                    NotifySend(BaseAction.Error, e);
                }
                _loading = false;
                NotifySend(BaseAction.SaveChange, _loading);
            }).ConfigureAwait(false);
        }
        #endregion

        #region Load STAT from service
        public async void LoadStat()
        {
            await LoadStatAsync().ConfigureAwait(false);
        }
        public async Task LoadStatAsync()
        {
            if (_loading)
                return;
            _loading = true;

            if (!CheckSettings())
            {
                _loading = false;
                return;
            }
            await Task.Run(async () =>
            {
                try
                {
                    do
                    {
                        string s = string.Empty;
                        try
                        {
                            using HttpClient client = new HttpClient();
                            s = await client.GetStringAsync(StaticSettings.BuildServiceStatUrl(_baseUri));
                        }
                        catch (Exception e)
                        {
                            LogWriter.Debug<ManagerChannels>(nameof(LoadStatAsync), e);
                            NotifySend(BaseAction.Error, e);
                            break;
                        }

                        if (string.IsNullOrEmpty(s))
                            break;

                        UTF8Encoding utf = new UTF8Encoding(false);
                        using MemoryStream ms = new MemoryStream(utf.GetBytes(s));
                        using StreamReader sr = new(ms, utf, false);
                        XmlSerializer xml = new(typeof(AudioStatsLite));
                        AudioStatsLite channels = xml.Deserialize(sr) as AudioStatsLite;

                        if ((channels == null) || (channels.Devices.Count == 0))
                            break;
                        NotifySend(BaseAction.LoadingStat, channels);

                    } while (false);
                }
                catch (Exception e)
                {
                    LogWriter.Debug<ManagerChannels>(nameof(LoadStatAsync), e);
                    NotifySend(BaseAction.Error, e);
                }
                _loading = false;
            }).ConfigureAwait(false);
        }
        #endregion

        #region private
        private bool CheckSettings()
        {
            bool __loading = _loading,
                 __return = false;
            _loading = true;
            do
            {
                if (!IsSettings)
                    break;

                string s = BaseUri;
                if (string.IsNullOrWhiteSpace(s))
                    break;

                if (Uri.IsWellFormedUriString(s, UriKind.Absolute))
                {
                    __return = true;
                    break;
                }
                else
                    NotifySend(BaseAction.Error, new Exception(Properties.Resources.ErrorBadUrl));
                
                if (!string.IsNullOrWhiteSpace(BaseUri))
                    BaseUri = string.Empty;
                __loading = false;

            } while (false);
            _loading = __loading;
            return __return;
        }

        private string ParseDefaultUri(string s)
        {
            try {
                if (string.IsNullOrWhiteSpace(s))
                    return string.Empty;
                return new Uri(s).DnsSafeHost;
            } catch { return string.Empty; }
        }
        #endregion
    }
#   pragma warning restore CS8604
}
