﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using AudioMonitor.Data;
using Xamarin.CommunityToolkit.UI.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Enums;

namespace AudioMonitor
{
    public class AboutPageView : INotifyPropertyChanged
    {
        const string BooleanYes = "!";
        const string BooleanNo = "-";
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string s) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        private void OnPropertyChanged(params string [] arr)
        {
            foreach (string s in arr)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }

        #region Command declare/init
        public ICommand AppSettingsCommand { get; private set; }
        public ICommand PanelBackCommand { get; private set; }
        public ICommand GitOpenCommand { get; private set; }
        #endregion

        public AboutPageView()
        {
            AppSettingsCommand = new Command(() => AppInfo.ShowSettingsUI());
            GitOpenCommand = new Command(async () => await Launcher.TryOpenAsync(StaticSettings.GitUri));
            PanelBackCommand = new Command(async () => { await Application.Current.MainPage.Navigation.PopToRootAsync(); });
            try {
                if (Preferences.ContainsKey(StaticSettings.KeyCloseServiceOnExit))
                    IsCloseServiceOnExit = Preferences.Get(StaticSettings.KeyCloseServiceOnExit, false);
            } catch { }
            try {
                var list = from i in VersionTracking.VersionHistory
                           from n in VersionTracking.BuildHistory
                           select string.Format("{0}.{1}", i, n);
                AppHistoryList = new ObservableCollection<string>(list);
            }
            catch { AppHistoryList = new ObservableCollection<string>(); }
            OnPropertyChanged(nameof(AppHistoryList));
        }

        #region UI screen size values
        public bool _isOrientation = true; /* Landscape = true ; Portrait = false */
        public bool IsOrientation
        {
            get => _isOrientation;
            set {
                if (value == _isOrientation)
                    return;
                _isOrientation = value;
                OnPropertyChanged(
                    nameof(IsOrientation),
                    nameof(IsLandscape),
                    nameof(IsPortrait),
                    nameof(DockLeft),
                    nameof(DockRight),
                    nameof(WidthPanel),
                    nameof(HeightPanel),
                    nameof(HeightHistoryList),
                    nameof(AppHistoryList));
            }
        }
        public bool IsLandscape
        {
            get => _isOrientation;
        }
        public bool IsPortrait
        {
            get => !_isOrientation;
        }
        public double WidthPanel
        {
            get {
                if ((DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait) ||
                    (Device.Idiom == TargetIdiom.Phone))
                    return DeviceDisplay.MainDisplayInfo.Width - 20;
                return (DeviceDisplay.MainDisplayInfo.Width / 2) - 40;
            }
        }
        public double HeightPanel
        {
            get {
                if ((DeviceDisplay.MainDisplayInfo.Orientation == DisplayOrientation.Portrait) ||
                    (Device.Idiom == TargetIdiom.Phone))
                    return -1;
                return DeviceDisplay.MainDisplayInfo.Height - 200;
            }
        }
        public double HeightHistoryList
        {
            get => AppHistoryList.Count * 25;
        }
        public Dock DockLeft
        {
            get => _isOrientation ? Dock.Left : Dock.Top;
        }
        public Dock DockRight
        {
            get => _isOrientation ? Dock.Right : Dock.Top;
        }
        #endregion

        #region Device Info
        public string Manufacturer => DeviceInfo.Manufacturer;
        public string DeviceModel => DeviceInfo.Model;
        public string DeviceName => DeviceInfo.Name;
        public string DeviceVersion => DeviceInfo.VersionString;
        public string DevicePlatform => DeviceInfo.Platform.ToString();
        public string DeviceType => $"{DeviceInfo.Idiom} - {DeviceInfo.DeviceType}";
        #endregion

        #region App Info
        public string AppName => AppInfo.Name;
        public string AppPackageName => AppInfo.PackageName;
        public string AppVersion => AppInfo.VersionString;
        public string AppBuildNumber => AppInfo.BuildString;
        #endregion

        #region App Tracking
        public string AppTrackFirstLaunch => VersionTracking.IsFirstLaunchEver ? BooleanYes : BooleanNo;
        public string AppTrackFirstLaunchCurrent => VersionTracking.IsFirstLaunchForCurrentVersion ? BooleanYes : BooleanNo;
        public string AppTrackFirstLaunchBuild => VersionTracking.IsFirstLaunchForCurrentBuild ? BooleanYes : BooleanNo;
        public string AppTrackVersion => VersionTracking.CurrentVersion;
        public string AppTrackBuild => VersionTracking.CurrentBuild;
        public string AppTrackPrevVersion => VersionTracking.PreviousVersion;
        public string AppTrackPrevBuild => VersionTracking.PreviousBuild;
        public string AppTrackStartVersion => VersionTracking.FirstInstalledVersion;
        public string AppTrackStartBuild => VersionTracking.FirstInstalledBuild;
        public ObservableCollection<string> AppHistoryList { get; } = default;
        #endregion

        private string _lastError = string.Empty;
        public  string LastError
        {
            get => _lastError;
            set {
                if (_lastError == value)
                    return;
                _lastError = value;
                OnPropertyChanged(nameof(LastError));
            }
        }
        public ObservableCollection<string> EventMonitor { get; } = new ObservableCollection<string>();
        public IEventAudioPlayerArgs AddEventToMonitor
        {
            set
            {
                if (value == null)
                    return;

                if ((value.EventId == EventAudioPlayerId.None) &&
                    (value.StateId == StateAudioPlayerId.None))
                    return;

                if (((value.EventId == EventAudioPlayerId.EvError) ||
                     (value.StateId == StateAudioPlayerId.Error)) &&
                    !string.IsNullOrWhiteSpace(value.Text))
                    LastError = value.Text;

                EventMonitor.Insert(0, $"{value.EventId}/{value.StateId}");
                if (EventMonitor.Count > 10)
                    for (int i = EventMonitor.Count - 1; i > 9; i--)
                        EventMonitor.RemoveAt(i);
                OnPropertyChanged(nameof(EventMonitor));
            }
        }

        private bool _iscloseServiceOnExit = false;
        public bool IsCloseServiceOnExit
        {
            get => _iscloseServiceOnExit;
            set {
                if (_iscloseServiceOnExit == value)
                    return;
                _iscloseServiceOnExit = value;
                try { Preferences.Set(StaticSettings.KeyCloseServiceOnExit, value); } catch { }
                OnPropertyChanged(nameof(IsCloseServiceOnExit));
            }
        }
    }
}
