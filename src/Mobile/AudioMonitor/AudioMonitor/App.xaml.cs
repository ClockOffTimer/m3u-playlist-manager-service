﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Threading.Tasks;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.CommunityToolkit.Helpers;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AudioMonitor
{
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += (s,e) => {
                LogWriter.Debug<App>(nameof(App), e.ExceptionObject as Exception);
            };
            TaskScheduler.UnobservedTaskException += (s, e) => {
                LogWriter.Debug<App>(nameof(App), e.Exception);
            };
            /*
            Current.RequestedThemeChanged += (s, a) =>
            {
                // Respond to the theme change
            };
            */
            LocalizationResourceManager.Current.PropertyChanged += (sender, e) =>
                AudioMonitor.Properties.Resources.Culture = LocalizationResourceManager.Current.CurrentCulture;
            LocalizationResourceManager.Current.Init(AudioMonitor.Properties.Resources.ResourceManager);
            LocalizationResourceManager.Current.CurrentCulture = new CultureInfo("ru");

            InitializeComponent();

            try { AppActions.OnAppAction += _OnAppAction; }
            catch (FeatureNotSupportedException ex) { LogWriter.Debug<App>(nameof(App), ex); }

            MainPage = new NavigationPage(new IntroPage());
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected async override void OnStart()
        {
            base.OnStart();
            try {
                await AppActions.SetAsync(
                    new AppAction("MainThisApp", AudioMonitor.Properties.Resources.AppIntroTag, null, "ic_home"),
                    new AppAction("AboutThisApp", AudioMonitor.Properties.Resources.AppInfoTag, null, "ic_info"),
                    new AppAction("CloseServiceThisApp", AudioMonitor.Properties.Resources.AppStopServiceTag, null, "ic_close"));
            }
            catch (FeatureNotSupportedException ex) {
                LogWriter.Debug<App>(nameof(OnStart), ex);
            }
        }

        protected override void OnSleep()
        {
            base.OnSleep();
        }

        protected override void OnResume()
        {
            base.OnResume();
        }

        private void _OnAppAction(object sender, AppActionEventArgs a)
        {
            if (Current != this && Current is App app)
            {
                AppActions.OnAppAction -= app._OnAppAction;
                return;
            }
            Dispatcher.BeginInvokeOnMainThread(async () =>
            {
                Page page = a.AppAction.Id switch {
                    "AboutThisApp" => new AboutPage(),
                    "CloseServiceThisApp" => new AppExitPage(),
                    "MainThisApp" => new MainPage(),
                    _ => null
                };
                if (page != null) {
                    await Current.MainPage.Navigation.PopToRootAsync();
                    await Current.MainPage.Navigation.PushAsync(page);
                }
            });
        }
    }
}
