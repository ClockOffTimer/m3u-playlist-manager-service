﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using AudioMonitor.Data.Interfaces;
using AudioMonitor.Utils;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AudioMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AppExitPage : ContentPage
    {
        public AppExitPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected override async void OnAppearing()
        {
            try {
                DependencyService.Get<IToastNotify>()?.Show("Audio traslate service stopped");
            } catch { }
            try {
                ICrossAudioPlayer Control = DependencyService.Get<ICrossAudioPlayer>();
                if (Control != null)
                    await Control.StopForegroundServiceCompatAsync();
            } catch { }
            try {
                ICloseApplication end = DependencyService.Get<ICloseApplication>();
                end?.CloseApp();
            } catch { }
            Application.Current.Quit();
        }
    }
}