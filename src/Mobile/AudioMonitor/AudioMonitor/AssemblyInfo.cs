/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Resources;
using Android.App;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
[assembly: NeutralResourcesLanguage("en-US")]

#if DEBUG
// [assembly: Application(Debuggable=true)]
#else
// [assembly: Application(Debuggable=false)]
#endif