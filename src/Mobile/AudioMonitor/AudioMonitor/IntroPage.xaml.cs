﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AudioMonitor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroPage : ContentPage, IDisposable
    {
        private CancellationTokenSource _cancell = new CancellationTokenSource();
        public ICommand GoCommand { get; private set; }

        public IntroPage()
        {
            GoCommand = new Command(async () => {
                await Task.Run(() => {
                try {
                    Dispatcher.BeginInvokeOnMainThread(async () => {
                        await Application.Current.MainPage.Navigation.PopToRootAsync();
                        await Application.Current.MainPage.Navigation.PushAsync(new MainPage());
                        Application.Current.MainPage.Navigation.RemovePage(this);
                    });
                } catch { }});
            });
            InitializeComponent();
            BindingContext = this;
        }
        ~IntroPage() => Dispose();

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Dispose();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_cancell == null)
                return;

            CancellationToken token = _cancell.Token;
            Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                Task.Run(() => {
                    uint tt = 1200;
                    double d = DeviceDisplay.MainDisplayInfo.Width / 2;
                    if (token.IsCancellationRequested) return;

                    Dispatcher.BeginInvokeOnMainThread(async () =>
                    {
                        await introImage.TranslateTo(d, 0, 0);
                        if (token.IsCancellationRequested) return;
                        await Task.WhenAll(
                            introImage.FadeTo(1, tt, Easing.Linear),
                            introImage.TranslateTo(0, introImage.Y, tt, Easing.CubicInOut));
                    });
                    Device.StartTimer(TimeSpan.FromSeconds(8), () => {
                        if (!token.IsCancellationRequested)
                            ButtonAnimation(1000);
                        return !token.IsCancellationRequested;
                    });
                }, token).ConfigureAwait(false);
                return false;
            });
        }

        public void Dispose()
        {
            CancellationTokenSource cts = _cancell;
            _cancell = null;
            if (cts != null)
                try { cts.Cancel(); cts.Dispose(); } catch { }
        }

        internal void ButtonAnimation(uint d)
        {
            Animation a = new Animation();
            a.WithConcurrent(
               (f) => buttonGo.Scale = f, buttonGo.Scale, buttonGo.Scale, Easing.Linear, 0, 0.1);
            a.WithConcurrent(
               (f) => buttonGo.Scale = f, buttonGo.Scale, buttonGo.Scale * 1.1, Easing.Linear, 0.1, 0.4);
            a.WithConcurrent(
               (f) => buttonGo.Scale = f, buttonGo.Scale * 1.1, buttonGo.Scale, Easing.Linear, 0.4, 0.5);
            a.WithConcurrent(
              (f) => buttonGo.Scale = f, buttonGo.Scale, buttonGo.Scale * 1.1, Easing.Linear, 0.5, 0.8);
            a.WithConcurrent(
               (f) => buttonGo.Scale = f, buttonGo.Scale * 1.1, buttonGo.Scale, Easing.Linear, 0.8, 1);
            buttonGo.Animate(nameof(ButtonAnimation), a, 16, d);
        }
    }
}