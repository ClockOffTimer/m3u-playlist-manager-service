﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Player;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.Essentials;

namespace Plugin.LightAudioPlayer.Droid.Services
{
    public class AudioPlayerServiceSettings
    {
        private readonly object __lock = new object();
        private AudioPlayerSettings _settings = new AudioPlayerSettings();
        public AudioPlayerSettings Get() => _settings;
        public bool IsSaved { get; private set; } = false;

        public async Task<bool> SetValuesAsyng(IAudioPlayer player) =>
            await Task.FromResult(SetValues(player));

        public void Remove() => Preferences.Remove(nameof(AudioPlayerServiceSettings));
        public void Copy(AudioPlayerSettings apss)
        {
            if (apss == null)
                return;
            try {
                lock (__lock)
                {
                    _settings.PlayIdx = apss.PlayIdx;
                    _settings.LoopPlay = apss.LoopPlay;
                    _settings.AutoPlay = apss.AutoPlay;
                    _settings.EqEnable = apss.EqEnable;
                    _settings.EfAutoGain = apss.EfAutoGain;
                    _settings.EqPreset = apss.EqPreset;
                    _settings.EfLoudness = apss.EfLoudness;
                    _settings.EfBasBoost = apss.EfBasBoost;
                    _settings.EfVirtualizer = apss.EfVirtualizer;
                    _settings.EfVirtualizerMode = apss.EfVirtualizerMode;
                    if ((apss.VolumeChannel != null) && (apss.VolumeChannel.Length > 0))
                    {
                        _settings.VolumeChannel = new float[apss.VolumeChannel.Length];
                        Array.Copy(apss.VolumeChannel, 0, _settings.VolumeChannel, 0, apss.VolumeChannel.Length);
                    }
                    if ((apss.EqChannels != null) && (apss.EqChannels.Length > 0))
                    {
                        _settings.EqChannels = new float[apss.EqChannels.Length];
                        Array.Copy(apss.EqChannels, 0, _settings.EqChannels, 0, apss.EqChannels.Length);
                    }
                    if ((apss.PlayList != null) && (apss.PlayList.Count > 0))
                    {
                        if (_settings.PlayList.Count > 0)
                            _settings.PlayList.Clear();
                        foreach (var a in apss.PlayList)
                            _settings.PlayList.Add(a);
                    }
                    _settings.IsOnce = false;
                }
            }
            catch (Exception ex) { LogWriter.Debug<AudioPlayerServiceSettings>(nameof(Copy), ex); }
            IsSaved = false;
        }
        public bool SetValues(IAudioPlayer player)
        {
            if (player == null)
                return false;
            try {
                lock (__lock)
                {
                    if (_settings.LoopPlay)
                        player.IsLooping = _settings.LoopPlay;
                    if (_settings.AutoPlay)
                        player.IsAutoPlay = _settings.AutoPlay;
                    if (_settings.EqEnable)
                        player.SetEqEnabled(_settings.EqEnable);
                    if (_settings.EfAutoGain)
                        player.SetEfAutoGain(_settings.EfAutoGain);
                    if (_settings.EqPreset >= 0)
                        player.SetEqPresets(_settings.EqPreset);
                    if (_settings.EfLoudness >= 0)
                        player.SetEfLoudness(_settings.EfLoudness);
                    if (_settings.EfBasBoost >= 0)
                        player.SetEfBasBoost(_settings.EfBasBoost);
                    if (_settings.EfVirtualizer >= 0)
                        player.SetEfVirtualizer(_settings.EfVirtualizer);
                    if (_settings.EfVirtualizerMode >= 0)
                        player.SetEfVirtualizerMode(_settings.EfVirtualizerMode);
                    if ((_settings.VolumeChannel != null) && (_settings.VolumeChannel.Length >= 2))
                        player.SetVolume(_settings.VolumeChannel[0], _settings.VolumeChannel[1]);
                    if (_settings.EqChannels != null)
                        for (int i = 0; (i < _settings.EqChannels.Length) && (i < 5); i++)
                            player.SetEqChannel(i, _settings.EqChannels[i]);
                    if ((_settings.PlayList != null) && (_settings.PlayList.Count > 0))
                    {
                        player.Load(_settings.PlayList);
                        if (_settings.PlayIdx >= 0)
                            player.SetPlayIndex(_settings.PlayIdx);
                        else
                            player.SetPlayIndex(0);
                    }
                    _settings.IsOnce = true;
                }
                return true;

            } catch (Exception ex) { LogWriter.Debug<AudioPlayerServiceSettings>(nameof(SetValues), ex); }
            return false;
        }
        public async Task SaveAsync()
        {
            if (!_settings.IsOnce)
                return;

            await Task.Run(() => {
                try {
                    UTF8Encoding utf = new UTF8Encoding(false);
                    using MemoryStream ms = new MemoryStream();
                    using StreamWriter sr = new(ms, utf);
                    XmlSerializer xml = new(typeof(AudioPlayerSettings));
                    lock (__lock)
                        xml.Serialize(sr, _settings);
                    string s = utf.GetString(ms.ToArray());
                    if (string.IsNullOrWhiteSpace(s))
                        return;

                    Preferences.Set(nameof(AudioPlayerServiceSettings), s);
                    IsSaved = true;
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerServiceSettings>(nameof(SaveAsync), ex); }
            });
        }
        public async Task LoadAsync()
        {
            await Task.Run(() => {
                try {
                    string s = Preferences.Get(nameof(AudioPlayerServiceSettings), string.Empty);
                    if (string.IsNullOrWhiteSpace(s))
                        return;

                    UTF8Encoding utf = new UTF8Encoding(false);
                    using MemoryStream ms = new MemoryStream(utf.GetBytes(s));
                    using StreamReader sr = new(ms, utf, false);
                    XmlSerializer xml = new(typeof(AudioPlayerSettings));
                    this.Copy(xml.Deserialize(sr) as AudioPlayerSettings);
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerServiceSettings>(nameof(LoadAsync), ex); }
            });
        }
        public override string ToString()
        {
            try {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("\tIsOnce:{0} | LoopPlay:{1} | AutoPlay:{2} | EqEnable:{3} | EqPreset:{4}\n",
                    _settings.IsOnce, _settings.LoopPlay, _settings.AutoPlay, _settings.EqEnable, _settings.EqPreset);
                sb.AppendFormat("\tEfAutoGain:{0} | EfLoudness:{1} | EfBasBoost:{2} | EfVirtualizer:{3} | EfVirtualizerMode:{4} | PlayIdx:{5}\n",
                    _settings.EfAutoGain, _settings.EfLoudness, _settings.EfBasBoost, _settings.EfVirtualizer, _settings.EfVirtualizerMode,
                    _settings.PlayIdx);

                if ((_settings.VolumeChannel != null) && (_settings.VolumeChannel.Length >= 2))
                {
                    sb.Append("\t[Volume Channels: ");
                    for (int i = 0; i < _settings.VolumeChannel.Length; i++)
                        sb.AppendFormat("{0}={1},", i, _settings.VolumeChannel[i]);
                    sb.AppendLine("]");
                }
                if (_settings.EqChannels != null)
                {
                    sb.Append("\t[Eq Channels: ");
                    for (int i = 0; i < _settings.EqChannels.Length; i++)
                        sb.AppendFormat("{0}={1},", i, _settings.EqChannels[i]);
                    sb.AppendLine("]");
                }
                if ((_settings.PlayList != null) && (_settings.PlayList.Count > 0))
                {
                    sb.AppendLine("\t[PlayList:");
                    foreach (var a in _settings.PlayList)
                        sb.AppendFormat("\t\t{0}\n", a.ToString());
                    sb.AppendLine("\t]");
                }
                return sb.ToString();

            } catch { return base.ToString(); }
        }
    }

    /// 

    public static class AudioPlayerServiceSettingsExtension
    {
        public static async Task DumpSettings(this AudioPlayerServiceSettings apss, string s = default)
        {
#           if DEBUG
            await Task.Run(() => {
                try {
                    string tag;
                    if (string.IsNullOrEmpty(s))
                        tag = string.Empty;
                    else
                        tag = $" {s} ->";
                    LogWriter.Debug<AudioPlayerServiceSettings>($"{nameof(DumpSettings)} ->{tag} DUMP SETTINGS\n{apss}");
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(DumpSettings), ex); }
            });
#           else
            await Task.FromResult(false);
#           endif
        }
    }
}