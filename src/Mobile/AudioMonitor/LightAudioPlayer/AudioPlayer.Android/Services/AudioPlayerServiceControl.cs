﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Droid.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(AudioPlayerServiceControl))]
namespace Plugin.LightAudioPlayer.Droid.Services
{
    [Preserve(AllMembers = true)]
    public class AudioPlayerServiceControl : IAudioPlayerServiceControl
    {
        public const string nameInteger     = "mediaint";
        public const string nameMediaArray  = "mediaarray";
        public const string nameBoolean     = "mediaboolean";
        public const string nameFloatArray  = "mediafloatarray";

        private readonly object __lock = new object();

        private Intent GetIntent() =>
            new Intent(Android.App.Application.Context, typeof(AudioPlayerService))
               .AddFlags(AudioPlayerService.LaunchServiceFlags);

        public void Play(int idx)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameInteger, idx)
                    .SetAction(nameof(StateAudioPlayerId.Playing)));
        }
        public void Play()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Playing)));
        }
        public void Pause()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Paused)));
        }
        public void Stop()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Stopped)));
        }
        public void Prev()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Prev)));
        }
        public void Next()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Next)));
        }
        public void Load(List<PlayListItem> list)
        {
            string s = SerializingList(list);
            if (string.IsNullOrWhiteSpace(s))
                return;

            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameMediaArray, s)
                    .SetAction(nameof(StateAudioPlayerId.LoadingList)));
        }
        public void Volume(float [] f)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameFloatArray, f)
                    .SetAction(nameof(StateAudioPlayerId.Volume)));
        }
        public void LoopPlay(bool b)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameBoolean, b)
                    .SetAction(nameof(StateAudioPlayerId.PlayingLoop)));
        }
        public void AutoPlay(bool b)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameBoolean, b)
                    .SetAction(nameof(StateAudioPlayerId.PlayingAuto)));
        }
        public void EqEnable(bool b)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameBoolean, b)
                    .SetAction(nameof(StateAudioPlayerId.EqEnabled)));
        }
        public void EqPreset(int idx)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameFloatArray, idx)
                    .SetAction(nameof(StateAudioPlayerId.EqPreset)));
        }
        public void EqChannels(float [] channels)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameFloatArray, channels)
                    .SetAction(nameof(StateAudioPlayerId.EqChannels)));
        }
        public void EqSettings()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.EqSettings)));
        }
        public void EfAutoGain(bool b)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameBoolean, b)
                    .SetAction(nameof(StateAudioPlayerId.EfAutoGain)));
        }
        public void EfLoudness(int i)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameInteger, i)
                    .SetAction(nameof(StateAudioPlayerId.EfLoudness)));
        }
        public void EfBasBoost(int i)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameInteger, i)
                    .SetAction(nameof(StateAudioPlayerId.EfBasBoost)));
        }
        public void EfVirtualizer(int i)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameInteger, i)
                    .SetAction(nameof(StateAudioPlayerId.EfVirtualizer)));
        }

        public void EfVirtualizerMode(int i)
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .PutExtra(nameInteger, i)
                    .SetAction(nameof(StateAudioPlayerId.EfVirtualizerMode)));
        }

        public void GetServiceSettings()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.Settings)));
        }
        public void StartForegroundServiceCompat()
        {
            lock (__lock) 
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.StartService)));
        }
        public void StopForegroundServiceCompat()
        {
            lock (__lock)
                ConnectServiceCompat(
                    GetIntent()
                    .SetAction(nameof(StateAudioPlayerId.StopService)));
        }
        private void ConnectServiceCompat(Intent intent)
        {
            try
            {
                if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
                    Android.App.Application.Context.StartForegroundService(intent);
                else
                    Android.App.Application.Context.StartService(intent);
            }
            catch (Exception e) { Log.Debug(nameof(ConnectServiceCompat), $"{e}"); }
        }
        private string SerializingList<T>(List<T> list)
        {
            try {
                UTF8Encoding utf = new UTF8Encoding(false);
                XmlSerializer xml = new XmlSerializer(typeof(List<T>));
                using MemoryStream ms = new MemoryStream();
                using StreamWriter sr = new(ms, utf);
                xml.Serialize(sr, list);
                ms.Close();
                return utf.GetString(ms.ToArray());
            }
            catch (Exception e) {
                Log.Debug(nameof(SerializingList), $"{e}");
                return string.Empty;
            }
        }
    }
}