﻿using System;
using Android;
using Android.App;
using Android.Content;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Droid.Notify;
using Plugin.LightAudioPlayer.Base.Utils;

[assembly: UsesPermission(Manifest.Permission.ReceiveBootCompleted)]
namespace Plugin.LightAudioPlayer.Droid.Services
{
    [BroadcastReceiver(Enabled = true)]
    [IntentFilter(new[] { Intent.ActionBootCompleted })]
    public class AudioReceiverBoot : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            try {
                if (intent.Action.Equals(Intent.ActionBootCompleted))
                    context.StartForegroundService(
                        new Intent(
                            context,
                            typeof(AudioPlayerService))
                            .SetComponent(
                                NotificationExtension.GetComponentName<AudioPlayerService>())
                            .SetAction(StateAudioPlayerId.StartService.ToString())
                            .AddFlags(AudioPlayerService.LaunchServiceFlags));
            } catch (Exception ex) { LogWriter.Debug<AudioReceiverBoot>(nameof(OnReceive), ex); }
        }
    }
}