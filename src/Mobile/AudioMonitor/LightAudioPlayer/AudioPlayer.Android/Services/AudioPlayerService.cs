﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Android;
using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Events;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Base.Utils;
using Plugin.LightAudioPlayer.Droid.Notify;
using Plugin.LightAudioPlayer.Droid.Player;
using Xamarin.Forms;

[assembly: UsesPermission(Manifest.Permission.ModifyAudioSettings)]
[assembly: UsesPermission(Manifest.Permission.ForegroundService)]
[assembly: UsesPermission(Manifest.Permission.BindCallRedirectionService)]
[assembly: UsesPermission(Manifest.Permission.KillBackgroundProcesses)]
namespace Plugin.LightAudioPlayer.Droid.Services
{
    [IntentFilter(actions: new string[] {
        nameof(StateAudioPlayerId.Playing),
        nameof(StateAudioPlayerId.Paused),
        nameof(StateAudioPlayerId.Stopped),
        nameof(StateAudioPlayerId.Prev),
        nameof(StateAudioPlayerId.Next),
        nameof(StateAudioPlayerId.Volume),
        nameof(StateAudioPlayerId.EqPreset),
        nameof(StateAudioPlayerId.EqEnabled),
        nameof(StateAudioPlayerId.EqChannels),
        nameof(StateAudioPlayerId.EqSettings),
        nameof(StateAudioPlayerId.PlayingLoop),
        nameof(StateAudioPlayerId.PlayingAuto),
        nameof(StateAudioPlayerId.LoadingList),
        nameof(StateAudioPlayerId.LoadingService),
        nameof(StateAudioPlayerId.StartService),
        nameof(StateAudioPlayerId.StopService),
        nameof(StateAudioPlayerId.Settings),
        nameof(StateAudioPlayerId.WidgetUpdate),
        nameof(StateAudioPlayerId.WidgetDeleted),
        nameof(StateAudioPlayerId.WidgetDisabled),
        nameof(StateAudioPlayerId.WidgetReloadList)
    })]
    [Service(
        Enabled = true,
        DirectBootAware = true,
        Label = "Audio player service",
        Icon = "@mipmap/icon",
        Exported = false,
        IsolatedProcess = false,
        ForegroundServiceType = Android.Content.PM.ForegroundService.TypeMediaPlayback |
                                Android.Content.PM.ForegroundService.TypeMediaProjection |
                                Android.Content.PM.ForegroundService.TypeDataSync)]
    [Preserve(AllMembers = true)]
    public class AudioPlayerService : Service, IAudioPlayerService
    {
        public class AudioPlayerServiceBinder : Binder
        {
            private readonly AudioPlayerService serv;
            public AudioPlayerServiceBinder(AudioPlayerService service) => this.serv = service;
            public AudioPlayerService Get() => serv;
        }
#if DEBUG
        public static readonly ActivityFlags LaunchActivityFlags = ActivityFlags.SingleTop | ActivityFlags.ClearTop  | ActivityFlags.IncludeStoppedPackages | ActivityFlags.DebugLogResolution;
        public static readonly ActivityFlags LaunchServiceFlags  = ActivityFlags.SingleTop | ActivityFlags.ClearTask | ActivityFlags.NewTask | ActivityFlags.DebugLogResolution;
#else
        public static readonly ActivityFlags LaunchActivityFlags = ActivityFlags.SingleTop | ActivityFlags.ClearTop | ActivityFlags.IncludeStoppedPackages;
        public static readonly ActivityFlags LaunchServiceFlags = ActivityFlags.SingleTop | ActivityFlags.ClearTask | ActivityFlags.NewTask;
#endif
        private int _timeSaveSettings = 0;
        private IBinder _binder = null;
        private Handler _handler = null;
        private Handler _handler_settings = null;
        private Action _runnable = null;
        private Action _runnable_settings = null;
        private ServiceCommandReceiver _cmd_receiver = null;
        private ServiceAudioReceiver _ain_receiver = null;
        private ServiceKeyReceiver _key_receiver = null;
        private NotificationService _notify = null;
        private IAudioPlayer _player = default;
        private AudioPlayerServiceSettings _settings = null;
        private StateAudioPlayerId _RunState = StateAudioPlayerId.None;
        public  StateAudioPlayerId RunState
        {
            get => _RunState;
            set { _RunState = value; }
        }
        private StateAudioPlayerId _PlayState = StateAudioPlayerId.None;
        public StateAudioPlayerId PlayState
        {
            get => _PlayState;
            set { _PlayState = value; }
        }

        public AudioPlayerService() {
            LogWriter.Debug<AudioPlayerService>($"{nameof(AudioPlayerService)}/Start -> {Class.Name}");
        }
        ~AudioPlayerService() => OnDestroy();

        private Intent SetIntentStart(Intent i) =>
            i.SetAction(nameof(StateAudioPlayerId.StartService))
             .SetFlags(LaunchServiceFlags);

        #region OnBind
        public override IBinder OnBind(Intent intent)
        {
            if (_binder == null)
                _binder = new AudioPlayerService.AudioPlayerServiceBinder(this);
            LogWriter.Debug<AudioPlayerService>($"{nameof(OnBind)}");
            return _binder;
        }
        #endregion

        #region OnBind
        public override bool OnUnbind(Intent intent)
        {
            _binder = null;
            LogWriter.Debug<AudioPlayerService>($"{nameof(OnUnbind)}");
            return true;
        }
        #endregion

        #region OnCreate/Event
        public async override void OnCreate()
        {
            base.OnCreate();
            AudioPlayerAndroid.SetServiceActive(true);
            LogWriter.Debug<AudioPlayerService>($"{nameof(OnCreate)}/Init");
            _settings = new AudioPlayerServiceSettings();

            global::Xamarin.Forms.Forms.Init(this, null);
            global::Xamarin.Essentials.Platform.Init(Application);

            _notify = new NotificationService(this);
            _notify.CreateChannel();
            _notify.CreateNotify();

            //this.StartForegroundService(
                 //SetIntentStart(new Intent(this, typeof(AudioPlayerService))));

            _player = new AudioPlayer();
            _player.StateEvent += AudioPlayer_StateEvent;

            _handler = new Handler(Application.MainLooper);
            _runnable = new Action(async () =>
            {
                try {
                    if (RunState == StateAudioPlayerId.RuningService)
                    {
                        Handler _h = _handler;
                        Action _a = _runnable;
                        if ((_h != null) && (_a != null))
                        {
                            _h.PostDelayed(_a, 300000);
                            if (PlayState != StateAudioPlayerId.Playing)
                                MessageEventSend(StateAudioPlayerId.Idle);
                        }
                        if (++_timeSaveSettings > 50)
                        {
                            _timeSaveSettings = 0;
                            if (!_settings.IsSaved)
                                await _settings.SaveAsync();
                        }
                    }
                } catch { }
            });
            _handler_settings = new Handler(Application.MainLooper);
            _runnable_settings = new Action(async () =>
            {
                try {
                    if ((RunState == StateAudioPlayerId.RuningService) &&
                        (_settings != null) && (_player != null) && !_settings.Get().IsOnce)
                    {
#                       if DEBUG
                        await _settings.DumpSettings($"{nameof(OnCreate)} -> {RunState}");
#                       endif
                        await _settings.SetValuesAsyng(_player);
                        _notify.CreateNotify();
                    }
                    try {
                        _handler_settings.RemoveCallbacks(_runnable_settings);
                        _handler_settings.Dispose();
                        _handler_settings = null;
                    } catch { }
                } catch { }
            });
            _handler_settings.PostDelayed(_runnable_settings, 180000);
            MessageEventSend(StateAudioPlayerId.StartService);

            _cmd_receiver = new ServiceCommandReceiver();
            _cmd_receiver.CommandEvent += Receivers_CommandEvent;
            RegisterReceiver(_cmd_receiver, new IntentFilter(ServiceCommandReceiver.IntentFilterName));

            _ain_receiver = new ServiceAudioReceiver();
            _ain_receiver.CommandEvent += Receivers_CommandEvent;
            RegisterReceiver(_ain_receiver, new IntentFilter(ServiceAudioReceiver.IntentFilterName));

            _key_receiver = new ServiceKeyReceiver();
            _key_receiver.CommandEvent += Receivers_CommandEvent;
            RegisterReceiver(_key_receiver, new IntentFilter(ServiceKeyReceiver.IntentFilterName));

            await _settings.LoadAsync();
#if         DEBUG
            await _settings.DumpSettings(nameof(OnCreate));
#           endif
        }

        private void Receivers_CommandEvent(object sender, EventServiceCommandArgs e)
        {
            try {
                LogWriter.Debug<AudioPlayerService>($"{nameof(Receivers_CommandEvent)} -> EVENT: {e.Id}");
                PlayerCommand(e.Id);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(Receivers_CommandEvent), ex); }
        }

        private void MessageEventSend(StateAudioPlayerId id)
        {
            if (RunState != StateAudioPlayerId.StopService)
                try {
                    MessagingCenter.Send<object, IEventAudioPlayerArgs>(this,
                        EventAudioPlayerArgs.MessagingCenterId,
                        new EventAudioPlayerArgs
                        {
                            EventId = EventAudioPlayerId.EvService,
                            StateId = id,
                            IsValue = true
                        });
                } catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(AudioPlayer_StateEvent), ex); }
        }

        private async void AudioPlayer_StateEvent(object sender, IEventAudioPlayerArgs args)
        {
            if (args == null)
                return;

            await Task.Run(() => {
                try {
                    MessagingCenter.Send<object, IEventAudioPlayerArgs>(
                        this, EventAudioPlayerArgs.MessagingCenterId, args);

                    if (sender is IAudioPlayer player)
                    {
                        switch (args.StateId)
                        {
                            case StateAudioPlayerId.Playing:
                            case StateAudioPlayerId.Paused:
                            case StateAudioPlayerId.Stopped:
                            case StateAudioPlayerId.Loaded:
                                {
                                    _notify.CreateNotify(
                                        player.CurrentPlay(), player.ButtonsState);
                                    break;
                                }
                            case StateAudioPlayerId.LoadedList:
                                {
                                    _notify.CreateNotifyListLoad(args.Value >= 1);
                                    break;
                                }
                        }
                    }
                } catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(AudioPlayer_StateEvent), ex); }
            });
        }
        #endregion

        #region OnDestroy
        public async override void OnDestroy()
        {
            LogWriter.Debug<AudioPlayerService>($"{nameof(OnDestroy)}/Dispose");
            RunState = StateAudioPlayerId.StopService;
            if (!_settings.IsSaved)
                await _settings.SaveAsync();
#if         DEBUG
            await _settings.DumpSettings(nameof(OnDestroy));
#           endif
            _notify.StopNotify();

            ServiceCommandReceiver _sr = _cmd_receiver;
            _cmd_receiver = null;
            if (_sr != null)
            {
                try
                {
                    _sr.CommandEvent -= Receivers_CommandEvent;
                    _sr.ClearAbortBroadcast();
                    UnregisterReceiver(_sr);
                    _sr.Dispose();
                } catch { }
            }

            ServiceAudioReceiver _ar = _ain_receiver;
            _ain_receiver = null;
            if (_ar != null)
            {
                try
                {
                    _ar.CommandEvent -= Receivers_CommandEvent;
                    _ar.ClearAbortBroadcast();
                    UnregisterReceiver(_ar);
                    _ar.Dispose();
                }
                catch { }
            }

            ServiceKeyReceiver _kr = _key_receiver;
            _key_receiver = null;
            if (_kr != null)
            {
                try
                {
                    _kr.CommandEvent -= Receivers_CommandEvent;
                    _kr.ClearAbortBroadcast();
                    UnregisterReceiver(_kr);
                    _kr.Dispose();
                }
                catch { }
            }

            IAudioPlayer _p = _player;
            _player = null;
            if (_p != null)
            {
                try
                {
                    if (_p.HasPlaying)
                        await _p.Stop();
                    _p.StateEvent -= AudioPlayer_StateEvent;
                    _p.Dispose();
                }
                catch { }
            }

            Handler _h = _handler;
            _handler = null;
            if (_h != null)
            {
                try
                {
                    _h.RemoveCallbacks(_runnable);
                    _h.Dispose();
                }
                catch { }
            }
            _h = _handler_settings;
            _handler_settings = null;
            if (_h != null)
            {
                try
                {
                    _h.RemoveCallbacks(_runnable_settings);
                    _h.Dispose();
                }
                catch { }
            }
            _runnable = null;
            _runnable_settings = null;
            RunState = StateAudioPlayerId.None;
            AudioPlayerAndroid.SetServiceActive(false);
            base.OnDestroy();

            /*
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
            */
        }
        #endregion

        #region OnStartCommand
        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
#           if DEBUG
            bool isi = intent != null, isia = isi && (intent.Action != null);
            string iss = isia ? intent.Action : string.Empty;
            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> START: {RunState}/{PlayState} = {isi}/{isia}/{iss}");
#           endif

            try
            {
                switch (RunState)
                {
                    case StateAudioPlayerId.None:
                        {
                            try
                            {
                                if ((intent != null) && (intent.Action != null) &&
                                    Enum.TryParse(intent.Action, out StateAudioPlayerId idx) &&
                                    (idx == StateAudioPlayerId.StopService))
                                    return StartCommandResult.NotSticky;

                                StartForegroundService(
                                     SetIntentStart(
                                         (intent == default) ?
                                            new Intent(this, typeof(AudioPlayerService)) : intent));

                                RunState = StateAudioPlayerId.LoadingService;
                                MessageEventSend(RunState);
                            }
                            catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(OnStartCommand), ex); }
                            return StartCommandResult.Sticky;
                        }
                    case StateAudioPlayerId.LoadingService:
                        {
                            _notify.CreateNotify();
                            if (_handler != null)
                            {
                                try { _handler.RemoveCallbacks(_runnable); } catch { }
                                _handler.PostDelayed(_runnable, 10000);
                            }
                            RunState = StateAudioPlayerId.RuningService;
                            MessageEventSend(RunState);
                            break;
                        }
                    case StateAudioPlayerId.RuningService:
                        {
                            break;
                        }
                    case StateAudioPlayerId.StopService:
                        {
                            return StartCommandResult.NotSticky;
                        }
                }
                if (RunState != StateAudioPlayerId.RuningService)
                    return StartCommandResult.Sticky;

                if ((intent == null) || (intent.Action == null))
                    return StartCommandResult.StickyCompatibility;

                StateAudioPlayerId id = (StateAudioPlayerId)Enum.Parse(typeof(StateAudioPlayerId), intent.Action);
                if (PlayState == id)
                    switch (id)
                    {
                        case StateAudioPlayerId.Paused:
                        case StateAudioPlayerId.Stopped:
                        case StateAudioPlayerId.WidgetReloadList:
                        case StateAudioPlayerId.LoadingList: return StartCommandResult.NotSticky;
                    }

                switch (id)
                {
                    case StateAudioPlayerId.StartService:
                        {
                            if (_handler != null)
                                _handler.PostDelayed(_runnable, 10000);
                            return StartCommandResult.StickyCompatibility;
                        }
                    case StateAudioPlayerId.StopService:
                        {
                            if (RunState == StateAudioPlayerId.None)
                                return StartCommandResult.Sticky;

                            MessageEventSend(StateAudioPlayerId.StopService);
                            RunState = StateAudioPlayerId.StopService;
                            StopSelf();
                            return StartCommandResult.RedeliverIntent;
                        }
                    case StateAudioPlayerId.Settings:
                        {
                            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> {nameof(StateAudioPlayerId.Settings)}");
                            MessagingCenter.Send<object, IEventAudioPlayerArgs>(this,
                                EventAudioPlayerArgs.MessagingCenterId,
                                new EventAudioPlayerArgs
                                {
                                    EventId = EventAudioPlayerId.EvService,
                                    StateId = StateAudioPlayerId.Settings,
                                    Obj = _settings.Get()
                                });
                            _settings.SetValues(_player);
                            return StartCommandResult.NotSticky;
                        }
                    case StateAudioPlayerId.WidgetUpdate:
                        {
                            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> {nameof(StateAudioPlayerId.WidgetUpdate)}");
                            _notify.UpdateWidget();
                            return StartCommandResult.NotSticky;
                        }
                    case StateAudioPlayerId.WidgetDisabled:
                        {
                            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> {nameof(StateAudioPlayerId.WidgetDisabled)}");
                            _notify.WidgetDisable();
                            return StartCommandResult.NotSticky;
                        }
                    case StateAudioPlayerId.WidgetDeleted:
                        {
                            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> {nameof(StateAudioPlayerId.WidgetDeleted)}");
                            _notify.WidgetDelete();
                            return StartCommandResult.NotSticky;
                        }
                    case StateAudioPlayerId.WidgetReloadList:
                        {
                            LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> {nameof(StateAudioPlayerId.WidgetReloadList)}");
                            if ((_settings != null) && (_player != null))
                                _ = _settings.SetValues(_player);
                            return StartCommandResult.NotSticky;
                        }
                }

                if (_player == null)
                {
                    LogWriter.Debug<AudioPlayerService>($"{nameof(OnStartCommand)} -> Player instance is NULL, abort command..");
                    return StartCommandResult.Sticky;
                }
                PlayerCommand(id, intent);
            }
            catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(OnStartCommand), ex); }
            return StartCommandResult.Sticky;
        }
        #endregion

        #region PlayerCommand
        private async void PlayerCommand(StateAudioPlayerId id, Intent intent = null)
        {
            try
            {
                switch (id)
                {
                    case StateAudioPlayerId.Playing:
                        {
                            if (!_settings.Get().IsOnce)
                                _ = await _settings.SetValuesAsyng(_player);

                            int? idx = (intent == null) ? null : intent.GetIntExtra(AudioPlayerServiceControl.nameInteger, -1);
                            if ((idx == null) || (idx == -1))
                                PlayerCommand(_player.Play);
                            else
                            {
                                PlayerCommand(_player.Play, (int)idx);
                                _settings.Get().PlayIdx = (int)idx;
                            }
                            break;
                        }
                    case StateAudioPlayerId.Paused:
                        {
                            PlayerCommand(_player.Pause);
                            break;
                        }
                    case StateAudioPlayerId.Stopped:
                        {
                            PlayerCommand(_player.Stop);
                            break;
                        }
                    case StateAudioPlayerId.Prev:
                        {
                            PlayerCommand(_player.Prev);
                            break;
                        }
                    case StateAudioPlayerId.Next:
                        {
                            PlayerCommand(_player.Next);
                            break;
                        }
                    case StateAudioPlayerId.LoadingList:
                        {
                            string xml = (intent == null) ? string.Empty : intent.GetStringExtra(AudioPlayerServiceControl.nameMediaArray);
                            if (!string.IsNullOrWhiteSpace(xml))
                                PlayerLoadCommand(xml);
                            break;
                        }
                    case StateAudioPlayerId.Volume:
                        {
                            float[] f = (intent == null) ? null : intent.GetFloatArrayExtra(AudioPlayerServiceControl.nameFloatArray);
                            if ((f == null) || (f.Length < 2))
                                break;
                            PlayerVolumeCommand(f);
                            _settings.Get().VolumeChannel = f;
                            break;
                        }
                    case StateAudioPlayerId.PlayingLoop:
                        {
                            bool b = intent.GetBooleanExtra(AudioPlayerServiceControl.nameBoolean, false);
                            PlayerCommand((d) => _player.IsLooping = d, b);
                            _settings.Get().LoopPlay = b;
                            break;
                        }
                    case StateAudioPlayerId.PlayingAuto:
                        {
                            bool b = intent.GetBooleanExtra(AudioPlayerServiceControl.nameBoolean, false);
                            PlayerCommand((d) => _player.IsAutoPlay = d, b);
                            _settings.Get().AutoPlay = b;
                            break;
                        }
                    case StateAudioPlayerId.EqEnabled:
                        {
                            bool b = intent.GetBooleanExtra(AudioPlayerServiceControl.nameBoolean, false);
                            PlayerCommand(_player.SetEqEnabled, b);
                            _settings.Get().EqEnable = b;
                            break;
                        }
                    case StateAudioPlayerId.EqPreset:
                        {
                            int? idx = (intent == null) ? null : intent.GetIntExtra(AudioPlayerServiceControl.nameFloatArray, -1);
                            if ((idx == null) || (idx == -1))
                                PlayerCommand(_player.SetEqEnabled, false);
                            else
                            {
                                PlayerCommand(_player.SetEqPresets, (int)idx);
                                _settings.Get().EqPreset = (int)idx;
                            }
                            break;
                        }
                    case StateAudioPlayerId.EqChannels:
                        {
                            float[] f = (intent == null) ? null : intent.GetFloatArrayExtra(AudioPlayerServiceControl.nameFloatArray);
                            if ((f == null) || (f.Length == 0))
                                break;
                            PlayerEqChannelsCommand(f);
                            _settings.Get().EqChannels = f;
                            break;
                        }
                    case StateAudioPlayerId.EqSettings:
                        {
                            PlayerCommand(_player.GetEqSettings);
                            break;
                        }
                    case StateAudioPlayerId.EfAutoGain:
                        {
                            bool b = intent.GetBooleanExtra(AudioPlayerServiceControl.nameBoolean, false);
                            PlayerCommand(_player.SetEfAutoGain, b);
                            _settings.Get().EqEnable = b;
                            break;
                        }
                    case StateAudioPlayerId.EfLoudness:
                        {
                            int i = intent.GetIntExtra(AudioPlayerServiceControl.nameInteger, 0);
                            PlayerCommand(_player.SetEfLoudness, i);
                            _settings.Get().EfLoudness = i;
                            break;
                        }
                    case StateAudioPlayerId.EfBasBoost:
                        {
                            int i = intent.GetIntExtra(AudioPlayerServiceControl.nameInteger, 0);
                            PlayerCommand(_player.SetEfBasBoost, i);
                            _settings.Get().EfBasBoost = i;
                            break;
                        }
                    case StateAudioPlayerId.EfVirtualizer:
                        {
                            int i = intent.GetIntExtra(AudioPlayerServiceControl.nameInteger, 0);
                            PlayerCommand(_player.SetEfVirtualizer, i);
                            _settings.Get().EfVirtualizer = i;
                            break;
                        }
                    case StateAudioPlayerId.EfVirtualizerMode:
                        {
                            int i = intent.GetIntExtra(AudioPlayerServiceControl.nameInteger, 0);
                            PlayerCommand(_player.SetEfVirtualizerMode, i);
                            _settings.Get().EfVirtualizerMode = i;
                            break;
                        }
                    default: return;
                }
                PlayState = id;
                MessageEventSend(StateAudioPlayerId.Alive);
                LogWriter.Debug<AudioPlayerService>($"{nameof(PlayerCommand)} -> {RunState}/{PlayState}");

            } catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(PlayerCommand), ex); }

        }
        #endregion

        #region ExecCommand
        private async void PlayerCommand(Func<Task<bool>> f) =>
            await Task.Run(async () => { await f.Invoke(); });

        private async void PlayerCommand<T1>(Func<T1, Task<bool>> f, T1 i) =>
            await Task.Run(async () => { await f.Invoke(i); });

        private async void PlayerCommand<T1, T2>(Func<T1, T2> f, T1 i) =>
            await Task.Run(() => { _ = f.Invoke(i); });

        private async void PlayerCommand<T1>(Action<T1> a, T1 i) =>
            await Task.Run(() => { a.Invoke(i); });

        private async void PlayerCommand(Action a) =>
            await Task.Run(() => { a.Invoke(); });

        private async void PlayerLoadCommand(string s)
        {
            await Task.Run(async () =>
            {
                try {
                    XmlSerializer sr = new XmlSerializer(typeof(List<PlayListItem>));
                    using MemoryStream ms = new MemoryStream(new UTF8Encoding(false).GetBytes(s));
                    List<PlayListItem> list = (List<PlayListItem>)sr.Deserialize(ms);
                    if (list == null)
                        return;
                    LogWriter.Debug<AudioPlayerService>($"{nameof(PlayerLoadCommand)} -> {s} -> {list}");
                    _settings.Get().PlayList = list;
                    _ = await _player.Load(list);
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(PlayerLoadCommand), ex); }
            });
        }
        private async void PlayerEqChannelsCommand(float[] f)
        {
            await Task.Run(() =>
            {
                try {
                    LogWriter.Debug<AudioPlayerService>($"{nameof(PlayerEqChannelsCommand)}/{StateAudioPlayerId.EqChannels} -> {(f != null)}");
                    for (int i = 0; i < f.Length; i++)
                        _player.SetEqChannel(i, f[i]);
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(PlayerEqChannelsCommand), ex); }
            });
        }
        private async void PlayerVolumeCommand(float[] f)
        {
            await Task.Run(() =>
            {
                try {
                    LogWriter.Debug<AudioPlayerService>($"{nameof(PlayerVolumeCommand)}/{StateAudioPlayerId.Volume} -> {(f == null)}");
                    _player.SetVolume(f[0], f[1]);
                }
                catch (Exception ex) { LogWriter.Debug<AudioPlayerService>(nameof(PlayerVolumeCommand), ex); }
            });
        }
        #endregion

        ///

        #region Service Receivers
        [IntentFilter(actions: new string[] { ServiceCommandReceiver.IntentFilterName })]
        public class ServiceCommandReceiver : BroadcastReceiver
        {
            public string ComponentName => Class.Name;
            public const string IntentFilterName = "com.m3uservice.receiver.internal_notify_command";
            public event EventHandler<EventServiceCommandArgs> CommandEvent = delegate { };
            public static PendingIntent GetPendingIntent(Context ctx, StateAudioPlayerId id) =>
                ctx.GetServiceReceiverPendingIntent(id, IntentFilterName);

            public override void OnReceive(Context context, Intent intent)
            {
                try
                {
                    string s = intent.GetStringExtra("ID");
                    if (!string.IsNullOrWhiteSpace(s) && Enum.TryParse(s, out StateAudioPlayerId id))
                        CommandEvent?.Invoke(this, new EventServiceCommandArgs(id));
                    else
                        LogWriter.Debug<ServiceCommandReceiver>($"{nameof(OnReceive)} -> RECEIVE BAD: {intent.Action}/{s}");
                } catch (Exception ex) { LogWriter.Debug<ServiceCommandReceiver>(nameof(OnReceive), ex); }
            }
        }

        [IntentFilter(actions: new string[] { ServiceKeyReceiver.IntentFilterName })]
        public class ServiceKeyReceiver : BroadcastReceiver
        {
            public string ComponentName => Class.Name;
            public const string IntentFilterName = Intent.ActionMediaButton;
            public event EventHandler<EventServiceCommandArgs> CommandEvent = delegate { };
            public static PendingIntent GetPendingIntent(Context ctx, StateAudioPlayerId id) =>
                ctx.GetServiceReceiverPendingIntent(id, IntentFilterName);

            public override void OnReceive(Context context, Intent intent)
            {
                try
                {
                    if (intent.Action != IntentFilterName)
                        return;

                    var key = (KeyEvent)intent.GetParcelableExtra(Intent.ExtraKeyEvent);
                    if (key.Action != KeyEventActions.Down)
                        return;

                    switch (key.KeyCode)
                    {
                        case Keycode.Headsethook:
                        case Keycode.MediaPause:
                            {
                                CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Paused));
                                break;
                            }
                        case Keycode.MediaPlay:
                        case Keycode.MediaPlayPause:
                            {
                                CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Playing));
                                break;
                            }
                        case Keycode.MediaStop:
                            {
                                CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Stopped));
                                break;
                            }
                        case Keycode.MediaPrevious:
                            {
                                CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Prev));
                                break;
                            }
                        case Keycode.MediaNext:
                            {
                                CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Next));
                                break;
                            }
                        default:
                            {
                                LogWriter.Debug<ServiceKeyReceiver>($"{nameof(OnReceive)} -> RECEIVE BAD: {key.KeyCode}");
                                break;
                            }
                    }
                } catch (Exception ex) { LogWriter.Debug<ServiceKeyReceiver>(nameof(OnReceive), ex); }
            }
        }

        [IntentFilter(actions: new string[] { ServiceAudioReceiver.IntentFilterName })]
        public class ServiceAudioReceiver : BroadcastReceiver
        {
            public string ComponentName => Class.Name;
            public const string IntentFilterName = AudioManager.ActionAudioBecomingNoisy;
            public event EventHandler<EventServiceCommandArgs> CommandEvent = delegate { };
            public static PendingIntent GetPendingIntent(Context ctx, StateAudioPlayerId id) =>
                ctx.GetServiceReceiverPendingIntent(id, IntentFilterName);

            private bool _sw = false;

            public override void OnReceive(Context context, Intent intent)
            {
                try
                {
                    if (intent.Action != IntentFilterName)
                        return;
                    if (AudioManager.ActionAudioBecomingNoisy.Equals(intent.Action))
                    {
                        if (!_sw)
                            CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Paused));
                        else
                            CommandEvent?.Invoke(this, new EventServiceCommandArgs(StateAudioPlayerId.Playing));
                        _sw = !_sw;
                    }
                    else if (AudioManager.ActionHeadsetPlug.Equals(intent.Action))
                    {
                    }
                }
                catch (Exception ex) { LogWriter.Debug<ServiceAudioReceiver>(nameof(OnReceive), ex); }
            }
        }
        #endregion
    }
}