﻿using System;
using System.Linq;
using Android.App;

namespace Plugin.LightAudioPlayer.Droid.Player
{
    internal static class MediaPlayerErrors
    {
        public static readonly Tuple<bool,uint,int> [] ErrorMessages = new Tuple<bool, uint, int>[]
        {
            new Tuple<bool,uint,int> (false,0x00000001,Resource.String.mp_error_0x00000001),
            new Tuple<bool,uint,int> (false,0x00000003,Resource.String.mp_error_0x00000003),
            new Tuple<bool,uint,int> (false,0x00000064,Resource.String.mp_error_0x00000064),
            new Tuple<bool,uint,int> (false,0x000000c8,Resource.String.mp_error_0x000000c8),
            new Tuple<bool,uint,int> (false,0x000002bc,Resource.String.mp_error_0x000002bc),
            new Tuple<bool,uint,int> (false,0x000002bd,Resource.String.mp_error_0x000002bd),
            new Tuple<bool,uint,int> (false,0x000002be,Resource.String.mp_error_0x000002be),
            new Tuple<bool,uint,int> (false,0x00000320,Resource.String.mp_error_0x00000320),
            new Tuple<bool,uint,int> (false,0x00000321,Resource.String.mp_error_0x00000321),
            new Tuple<bool,uint,int> (false,0x00000322,Resource.String.mp_error_0x00000322),
            new Tuple<bool,uint,int> (false,0x00000385,Resource.String.mp_error_0x00000385),
            new Tuple<bool,uint,int> (false,0x00000386,Resource.String.mp_error_0x00000386),
            new Tuple<bool,uint,int> (true, 0xffffff92,Resource.String.mp_error_0xffffff92),
            new Tuple<bool,uint,int> (true, 0xffffffda,Resource.String.mp_error_0xffffffda),
            new Tuple<bool,uint,int> (true, 0xfffffc14,Resource.String.mp_error_0xfffffc14),
            new Tuple<bool,uint,int> (true, 0xfffffc11,Resource.String.mp_error_0xfffffc11),
            new Tuple<bool,uint,int> (true, 0xfffffc0e,Resource.String.mp_error_0xfffffc0e),
            new Tuple<bool,uint,int> (true, 0x80000000,Resource.String.mp_error_0x80000000),
            new Tuple<bool,uint,int> (true, 0x80131500,Resource.String.mp_error_0x80131500)
        };

        public static string FindMessage(this int idx, bool b = false) => FindMessage((uint)idx, b);
        public static string FindMessage(this uint idx, bool b = false)
        {
            try {
                return (from i in ErrorMessages
                        where i.Item1 == b && i.Item2 == idx
                        select Application.Context.GetString(i.Item3)).FirstOrDefault();
            } catch { return string.Empty; }
        }
    }
}
