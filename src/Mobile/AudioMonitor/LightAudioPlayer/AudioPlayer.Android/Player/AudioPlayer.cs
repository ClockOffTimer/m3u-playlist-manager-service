﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

// #define PLAY_TIMER
// #define USING_MESSAGINGCENTER

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Android;
using Android.App;
using Android.Content;
using Android.Media;
using Android.Media.Audiofx;
using Android.Runtime;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Events;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Player;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Base.Utils;
using Xamarin.Forms;

[assembly: UsesPermission(Manifest.Permission.ModifyAudioSettings)]
[assembly: Dependency(typeof(Plugin.LightAudioPlayer.Droid.Player.AudioPlayer))]
namespace Plugin.LightAudioPlayer.Droid.Player
{
    [Preserve(AllMembers = true)]
    public class AudioPlayer : EventAudioPlayer, IAudioPlayer, IDisposable
    {
        private MediaPlayer _mediaPlayer = default;
        private Equalizer _mediaEq = default;
        private Virtualizer _virtualizer = default;
        private LoudnessEnhancer _loudness = default;
        private BassBoost _bboost = default;
        private AutomaticGainControl _autogain = default;
        private AudioAttributes _attributes = default;
        private PlayListItems _playList = new PlayListItems();
        private Android.Net.Uri _uri = default;

        public AudioPlayer() => Init();
        ~AudioPlayer() => Dispose();

        private void Init()
        {
            try {
                _attributes = new AudioAttributes.Builder()
                             .SetUsage(AudioUsageKind.Media)
                             .SetContentType(AudioContentType.Music)
                             .Build();

                _mediaPlayer = new MediaPlayer();
                _mediaPlayer.AudioSessionId = GetAudioSessionId_();
                _mediaPlayer.SetAudioAttributes(_attributes);

                _mediaPlayer.Info += Event_Player_Info;
                _mediaPlayer.Error += Event_Player_Error;
                _mediaPlayer.Prepared += Event_Player_Prepared;
                _mediaPlayer.Completion += Event_Player_Completion;
                _mediaPlayer.BufferingUpdate += Event_Player_BufferingUpdate;

                _mediaEq = new Equalizer(0, _mediaPlayer.AudioSessionId);
                _mediaEq.EnableStatus += Event_Eqf_EnableStatus;
                _mediaEq.ControlStatus += Event_Eqf_ControlStatus;
                _mediaEq.Parameter += Event_Eq_Parameter;

                _loudness = new LoudnessEnhancer(_mediaPlayer.AudioSessionId);
                if (_loudness != null)
                {
                    _loudness.SetEnabled(false);
                    _loudness.EnableStatus += Event_Eqf_EnableStatus;
                    _loudness.ControlStatus += Event_Eqf_ControlStatus;
                }

                _bboost = new BassBoost(0, _mediaPlayer.AudioSessionId);
                if (_bboost != null)
                {
                    _bboost.SetEnabled(false);
                    _bboost.EnableStatus += Event_Eqf_EnableStatus;
                    _bboost.ControlStatus += Event_Eqf_ControlStatus;
                }

                _autogain = AutomaticGainControl.Create(_mediaPlayer.AudioSessionId);
                if (_autogain != null)
                {
                    _autogain.SetEnabled(false);
                    _autogain.EnableStatus += Event_Eqf_EnableStatus;
                    _autogain.ControlStatus += Event_Eqf_ControlStatus;
                }

                _virtualizer = new Virtualizer(0, _mediaPlayer.AudioSessionId);
                if (_virtualizer != null)
                {
                    _virtualizer.SetEnabled(false);
                    _virtualizer.EnableStatus += Event_Eqf_EnableStatus;
                    _virtualizer.ControlStatus += Event_Eqf_ControlStatus;
                }

                Event_EqSettings();

            } catch (Exception ex) {
                LogWriter.Debug<AudioPlayer>(nameof(Init), ex);
                DefaultEvent(ex);
            }
        }

        #region Dispose
        public void Dispose()
        {
            MediaPlayer mp = _mediaPlayer;
            _mediaPlayer = null;
            if (mp != null)
            {
                mp.Info -= Event_Player_Info;
                mp.Error -= Event_Player_Error;
                mp.Prepared -= Event_Player_Prepared;
                mp.Completion -= Event_Player_Completion;
                mp.BufferingUpdate -= Event_Player_BufferingUpdate;
                try
                {
                    mp.Reset();
                    mp.Release();
                    mp.Dispose();
                }
                catch { }
            }

            Virtualizer vr = _virtualizer;
            _virtualizer = null;
            if (vr != null)
                try
                {
                    vr.EnableStatus -= Event_Eqf_EnableStatus;
                    vr.ControlStatus -= Event_Eqf_ControlStatus;
                    try { vr.ForceVirtualizationMode(VirtualizationMode.Off); } catch { }
                    vr.Release();
                    vr.Dispose();
                }
                catch { }

            LoudnessEnhancer ll = _loudness;
            _loudness = null;
            if (ll != null)
                try
                {
                    ll.EnableStatus -= Event_Eqf_EnableStatus;
                    ll.ControlStatus -= Event_Eqf_ControlStatus;
                    ll.Release();
                    ll.Dispose();
                }
                catch { }

            BassBoost bb = _bboost;
            _bboost = null;
            if (bb != null)
                try
                {
                    bb.EnableStatus -= Event_Eqf_EnableStatus;
                    bb.ControlStatus -= Event_Eqf_ControlStatus;
                    bb.Release();
                    bb.Dispose();
                }
                catch { }

            AutomaticGainControl gc = _autogain;
            _autogain = null;
            if (gc != null)
                try
                {
                    gc.EnableStatus -= Event_Eqf_EnableStatus;
                    gc.ControlStatus -= Event_Eqf_ControlStatus;
                    gc.Release();
                    gc.Dispose();
                }
                catch { }

            Equalizer eq = _mediaEq;
            _mediaEq = null;
            if (eq != null)
            {
                _mediaEq.EnableStatus -= Event_Eqf_EnableStatus;
                _mediaEq.ControlStatus -= Event_Eqf_ControlStatus;
                _mediaEq.Parameter -= Event_Eq_Parameter;
                try
                {
                    eq.Release();
                    eq.Dispose();
                }
                catch { }
            }
            AudioAttributes atr = _attributes;
            _attributes = null;
            if (atr != null)
                try { atr.Dispose(); } catch { }

            Android.Net.Uri u = _uri;
            _uri = null;
            if (u != null)
                try { u.Dispose(); } catch { }

            GC.SuppressFinalize(this);
        }
        #endregion

        #region Methods/Functions
        public async Task<bool> Load(IList<PlayListItem> list) => await Task.FromResult(SetPlayList_(list));
        public async Task<bool> Play(int idx) => await Play_(idx);
        public async Task<bool> Play() => await Play_(_playList.Last());
        public async Task<bool> Pause() => await Task.FromResult(Pause_());
        public async Task<bool> Stop() => await Task.FromResult(Stop_());
        public async Task<bool> Prev() => await Prev_();
        public async Task<bool> Next() => await Next_();

        public PlayListItem CurrentPlay() => _playList.Current();
        public void SetPlayIndex(int i) => _playList.Set(i);
        public void SetVolume(float vol) => SetVolume(vol, 0.0f);
        public void SetVolume(float vol, float bal)
        {
            float _min = _mediaEq.Enabled ? 0.4f : 0.2f,
                  bal0 = Math.Min(1.0f, Math.Max(-1.0f, bal)),
                  vol0 = Math.Min(1.0f, Math.Max(0.0f,  vol)),
                  vol1 = Math.Min(1.0f, Math.Max(_min, (float)Math.Cos((Math.PI * (bal0 + 1)) / 4) * vol0)),
                  vol2 = Math.Min(1.0f, Math.Max(_min, (float)Math.Sin((Math.PI * (bal0 + 1)) / 4) * vol0));
            _mediaPlayer.SetVolume(vol1, vol2);
        }
        public void SetEfAutoGain(bool b)
        {
            try
            {
                if (_autogain == null)
                    return;
                _autogain.SetEnabled(b);
            } catch { }
        }
        public void SetEfLoudness(int val)
        {
            try {
                if (_loudness == null)
                    return;
                _loudness.SetEnabled(val > 0);
                _loudness.SetTargetGain(val);
            } catch { }
        }
        public void SetEfBasBoost(int val) /* 0, 1000 */
        {
            try {
                if (_bboost == null)
                    return;
                _bboost.SetEnabled(val > 0);
                _bboost.SetStrength((short)val);
            } catch { }
        }
        public void SetEfVirtualizer(int val) /* 0, 1000 */
        {
            try {
                if (_virtualizer == null)
                {
                    DefaultEventEf(false, -1);
                    return;
                }
                if (val < 0)
                {
                    SetEfVirtualizerMode(-1);
                    return;
                }
                if (!_virtualizer.Enabled)
                    SetEfVirtualizerMode(0);
                _virtualizer.SetStrength((short)val);
            } catch { }
        }
        public void SetEfVirtualizerMode(int val)
        {
            try
            {
                if (_virtualizer == null)
                {
                    DefaultEventEf(false, -1);
                    return;
                }
                switch (val)
                {
                    case 0:
                        {
                            _virtualizer.SetEnabled(true);
                            if (!_virtualizer.ForceVirtualizationMode(VirtualizationMode.Auto))
                                _virtualizer.SetEnabled(false);
                            break;
                        }
                    case 1:
                        {
                            _virtualizer.SetEnabled(true);
                            if (!_virtualizer.ForceVirtualizationMode(VirtualizationMode.Binaural))
                                if (!_virtualizer.ForceVirtualizationMode(VirtualizationMode.Auto))
                                    _virtualizer.SetEnabled(false);
                            break;
                        }
                    case 2:
                        {
                            _virtualizer.SetEnabled(true);
                            if (!_virtualizer.ForceVirtualizationMode(VirtualizationMode.Transaural))
                                if (!_virtualizer.ForceVirtualizationMode(VirtualizationMode.Auto))
                                    _virtualizer.SetEnabled(false);
                            break;
                        }
                    default:
                        {
                            if (!_virtualizer.Enabled)
                                break;
                            _ = _virtualizer.ForceVirtualizationMode(VirtualizationMode.Off);
                            _virtualizer.SetEnabled(false);
                            break;
                        }
                }
                DefaultEventEf(_virtualizer.Enabled, val);
            } catch { }
        }

        public bool GetEqEnabled() => _mediaEq != null && _mediaEq.Enabled;
        public void SetEqEnabled(bool b) => _mediaEq?.SetEnabled(b);
        public void GetEqSettings() => Event_GetEqSettings();
        public void SetEqChannel(int idx, float val)
        {
            if ((_mediaEq == null) || (idx < 0) || (idx >= _mediaEq.NumberOfBands))
                return;
            _mediaEq.SetBandLevel((short)idx,
                     (short)Math.Min(short.MaxValue - 1, Math.Max(short.MinValue + 1, Math.Round(val))));
        }
        public void SetEqPresets(int idx)
        {
            if (_mediaEq == null)
                return;
            if (idx < 0) {
                _mediaEq.SetEnabled(false);
                return;
            }
            if (!_mediaEq.Enabled)
                _mediaEq.SetEnabled(true);
            _mediaEq.UsePreset((short)idx);
            try {
                Event_EqSettings();
            } catch { }
        }
        public PlayListItem SetEqPresetsReturn(int idx)
        {
            if (_mediaEq == null)
                return default;
            if (!_mediaEq.Enabled)
                _mediaEq.SetEnabled(true);
            _mediaEq.UsePreset((short)idx);
            try
            {
                PlayListItem pli = _playList.Current();
                if (pli == null)
                    return default;

                if ((pli.EqualizersLocal == null) || (pli.EqualizersLocal.Length < _mediaEq.NumberOfBands))
                    pli.EqualizersLocal = new float[_mediaEq.NumberOfBands];
                for (int i = 0; i < _mediaEq.NumberOfBands; i++)
                    pli.EqualizersLocal[i] = (float)_mediaEq.GetBandLevel((short)i);
                return pli;

            }
            catch { return default; }
        }
        #endregion

        #region Getter/Setter
        public int Duration { get => _mediaPlayer.Duration; }
        public int Position { get => _mediaPlayer.CurrentPosition; }
        public int PlayListCount { get => _playList.Count; }
        public int AudioSessionId { get => _mediaPlayer.AudioSessionId; }
        public bool [] ButtonsState => new bool [6] {
            HasButtonPrev,
            HasButtonPlay,
            HasButtonPause,
            HasButtonStop,
            HasButtonNext,
            _playList.Count == 0
        };

        public bool IsLooping { get => _mediaPlayer.Looping; set { if (_mediaPlayer.Looping != value) _mediaPlayer.Looping = value; } }
        public bool IsAutoPlay { get; set; } = false;
        public bool IsAutoNext { get; set; } = true;

        public bool HasPlaying => _playerPlay && _playerPrepare && _mediaPlayer.IsPlaying;
        public bool HasStoped  => (_playerStop || !_playerPlay) && !_playerPrepare && !_mediaPlayer.IsPlaying;
        public bool HasPlayListPreload { get; private set; } = false;
        public bool HasButtonPlay  => (_playerStop || _playerPause) && (_playList.Count > 0);
        public bool HasButtonPause => _playerPlay && !_playerPause;
        public bool HasButtonStop  => _playerPlay && _playerPrepare;
        public bool HasButtonPrev  => (_playerPlay || _playerStop) && (_playList.Index > 0);
        public bool HasButtonNext  => (_playerPlay || _playerStop) && (_playList.Index < _playList.Count - 1);

        private bool IsPauseCommand() => HasPlayListPreload && _playerPlay && !_playerPause && _mediaPlayer.IsPlaying;
        private bool IsStopCommand() => HasPlayListPreload && _playerPrepare && _playerPlay && _mediaPlayer.IsPlaying;

#       if PLAY_TIMER
        private int  _playerTime = 0;
#       endif
        private int _reOpenCount = 0;
        private bool _playerPlay = true,
                     _playerPause = true,
                     _playerStop = true,
                     _playerPrepare = false,
                     _playerReset = true,
                     _playerStart = false;

        private void playerPrepare() { if (!_playerPrepare) {
                _mediaPlayer.PrepareAsync();
                _playerPrepare = true;
                _playerPlay = false;
                _playerStart = true;
#               if PLAY_TIMER
                _playerTime = 0;
#               endif
                DefaultEvent(StateAudioPlayerId.Loading, _playList.Index);
            }
        }
        private void playerReset()   { if (!_playerReset && (_playerStop || _playerPause)) {
                _mediaPlayer.Reset();
                _playerReset = true;
                _playerPrepare = false;
            }
        }
        private void playerResetFromError() {
            try {
                _mediaPlayer.Stop();
            } catch (Exception ex) { DefaultEvent(ex); }
            Task.Delay(900).Wait();
            try {
                _mediaPlayer.Reset();
            } catch (Exception ex) { DefaultEvent(ex); }
            _playerPause = _playerStop = _playerReset = true;
            _playerPrepare = false;
        }
        private void playerPlay()    { if (!_playerPlay && _playerPrepare) {
                _mediaPlayer.Start();
                _playerPlay = true;
                _playerStart = _playerStop = _playerPause = false;
                _reOpenCount = 0;
                DefaultEvent(StateAudioPlayerId.Playing, _playList.Index);
            }
        }
        private void playerStop()    { if (!_playerStop && _playerPrepare && _playerPlay) {
                _mediaPlayer.Stop();
                _playerPause = _playerStop = true;
                _playerReset = false;
                DefaultEvent(StateAudioPlayerId.Stopped, _playList.Index);
            }
        }
        private void playerPause() { if (!_playerPause && _playerPlay) {
                _mediaPlayer.Pause();
                _playerStop = _playerPause = true;
                _playerPlay = false;
                DefaultEvent(StateAudioPlayerId.Paused, _playList.Index);
            }
        }
        #endregion

        #region events
        private void Event_Player_Prepared(object sender, EventArgs e)
        {
            try {
                bool b = IsAutoPlay || _playerStart;
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EvPrepared,
                    StateId = StateAudioPlayerId.Loaded
                };
                CallEventByType(args, true);
                if (b)
                    playerPlay();
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_Prepared), ex); }
        }
        private async void Event_Player_Completion(object sender, EventArgs e)
        {
            try
            {
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EvCompletion,
                    StateId = StateAudioPlayerId.Stopped
                };
                CallEventByType(args, true);
                if (IsAutoPlay && !_playerStart)
                    await Next_();
                else
                    _ = Stop_();

            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_Completion), ex); }
        }
        private void Event_Player_BufferingUpdate(object sender, MediaPlayer.BufferingUpdateEventArgs e)
        {
            try
            {
                if (e == null)
                    return;

                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EvBufferingUpdate,
                    StateId = (e.Percent == 100) ? StateAudioPlayerId.Loaded : StateAudioPlayerId.Loading,
                    Value = e.Percent
                };
                CallEventByType(args);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_BufferingUpdate), ex); }
        }
        private void Event_Player_Info(object sender, MediaPlayer.InfoEventArgs e)
        {
            try {
                if (e == null)
                    return;

                e.Handled = true;
                StateAudioPlayerId id = StateAudioPlayerId.None;
                switch (e.What)
                {
                    case MediaInfo.Unknown: id = StateAudioPlayerId.Error; break;
                    case MediaInfo.AudioNotPlaying: id = StateAudioPlayerId.Error; break;
                    case MediaInfo.BadInterleaving: id = StateAudioPlayerId.Error; break;
                    case MediaInfo.BufferingStart: id = StateAudioPlayerId.Loading; break;
                    case MediaInfo.BufferingEnd: id = StateAudioPlayerId.Loaded; break;
                    case MediaInfo.StartedAsNext: id = StateAudioPlayerId.Next; break;
                }
                string s = ((uint)e.What).FindMessage();
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EvInfo,
                    StateId = id,
                    Text = string.IsNullOrWhiteSpace(s) ? e.What.ToString() : s,
                    Value = e.Extra
                };
                CallEventByType(args);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_Info), ex); }
        }
        private async void Event_Player_Error(object sender, MediaPlayer.ErrorEventArgs e)
        {
            try
            {
                if (e == null)
                    return;
                e.Handled = true;

                string s = ((uint)e.What).FindMessage();
                if (string.IsNullOrWhiteSpace(s))
                    s = ((uint)e.Extra).FindMessage(true);

                do {
                    if (((uint)e.Extra == 0xffffffda) || ((uint)e.What == 0xffffffda))
                    {
                        DefaultEvent(StateAudioPlayerId.Loading, _playList.Index);

                        if (IsAutoPlay || _playerStart)
                            try {
                                playerResetFromError();

                                if (_uri != null)
                                    try { _uri.Dispose(); } catch { }
                                _uri = Android.Net.Uri.Parse(_playList.ById(_playList.Index));
                                if (_uri == null)
                                    break;
                                await _mediaPlayer.SetDataSourceAsync(Android.App.Application.Context, _uri)
                                                  .ConfigureAwait(false);
                                Task.Delay(500).Wait();
                                playerPrepare();

                            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_Error), ex); }
                        break;
                    }
                    else if ((int)e.What == 0x000002bd)
                        DefaultEvent(StateAudioPlayerId.Loading, _playList.Index);
                    else if ((int)e.What == 0x000002be)
                        DefaultEvent(StateAudioPlayerId.Loaded, _playList.Index);

                    EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                    {
                        EventId = EventAudioPlayerId.EvError,
                        StateId = StateAudioPlayerId.Error,
                        Text = string.IsNullOrWhiteSpace(s) ? e.What.ToString() : s,
                        Value = e.Extra
                    };
                    CallEventByType(args, true);

                } while (false);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Player_Error), ex); }
            try {
                playerResetFromError();
                await Task.Delay(500).ConfigureAwait(false);
                if (IsAutoPlay)
                {
                    if (IsAutoNext)
                        await Next_();
                    else if (_reOpenCount++ < 101)
                        await Play();
                }
            } catch (Exception ex) { DefaultEvent(ex); }
        }
        private void Event_Eq_Parameter(object sender, Equalizer.ParameterChangeEventArgs e)
        {
            try {
                if (e == null)
                    return;
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EqParameter,
                    StateId = StateAudioPlayerId.None,
                    Value = e.Value
                };
                if (Enum.TryParse(typeof(EqualizerParamId).Name, out EqualizerParamId eid1))
                    args.EqParamId[0] = eid1;
                if (Enum.TryParse(typeof(EqualizerParamId).Name, out EqualizerParamId eid2))
                    args.EqParamId[1] = eid2;
                CallEventByType(args);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Eq_Parameter), ex); }
        }
        private EventAudioPlayerId GetEvent_Eqf_AudioPlayerId(object sender, bool b)
        {
            return (sender is Equalizer) ? (b ? EventAudioPlayerId.EqEnableStatus : EventAudioPlayerId.EqControlStatus) :
                    (sender is Virtualizer) ? (b ? EventAudioPlayerId.EfVirtualizeEnableStatus : EventAudioPlayerId.EfVirtualizeControlStatus) :
                     (sender is LoudnessEnhancer) ? (b ? EventAudioPlayerId.EfLoudnessEnableStatus : EventAudioPlayerId.EfLoudnessControlStatus) :
                      (sender is BassBoost) ? (b ? EventAudioPlayerId.EfBassBoostEnableStatus : EventAudioPlayerId.EfBassBoostControlStatus) :
                       (sender is AutomaticGainControl) ? (b ? EventAudioPlayerId.EfAutoGainEnableStatus : EventAudioPlayerId.EfAutoGainControlStatus) :
                        EventAudioPlayerId.None;
        }
        private void Event_Eqf_ControlStatus(object sender, AudioEffect.ControlStatusChangeEventArgs e)
        {
            try {
                if ((e == null) || (sender == null))
                    return;
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = GetEvent_Eqf_AudioPlayerId(sender, false),
                    StateId = StateAudioPlayerId.None,
                    IsValue = e.ControlGranted
                };
                CallEventByType(args);
            } catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Eqf_ControlStatus), ex); }
        }
        private void Event_Eqf_EnableStatus(object sender, AudioEffect.EnableStatusChangeEventArgs e)
        {
            try {
                if ((e == null) || (sender == null))
                    return;
                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = GetEvent_Eqf_AudioPlayerId(sender, true),
                    StateId = StateAudioPlayerId.None,
                    IsValue = e.Enabled
                };
                CallEventByType(args);
            }
            catch (Exception ex) { LogWriter.Debug<AudioPlayer>(nameof(Event_Eqf_EnableStatus), ex); }
        }
        private void Event_EqSettings()
        {
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                CancellationTokenSource token = default;
                try
                {
                    bool b = true;
                    token = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                    Task<bool>.Run(() =>
                    {
                        return Event_GetEqSettings();
                    }, token.Token).ContinueWith((t) => {

                        if (t.Exception != null)
                            DefaultEvent(t.Exception);
                        if ((t == null) || t.IsFaulted || t.IsCanceled)
                            return;
                        b = t.Result;
                    }).Wait();
                    return b;
                }
                catch (Exception ex) { DefaultEvent(ex); }
                finally { if (token != null) token.Dispose(); }
                return true;
            });
        }
        private bool Event_GetEqSettings()
        {
            if (_mediaEq == default)
                return false;

            bool isenabled = false;
            try
            {
                isenabled = _mediaEq.Enabled;
                if (!isenabled)
                    _mediaEq.SetEnabled(true);

                Equalizer.Settings settings = _mediaEq.Properties;
                if (settings == null)
                    return true;

                EventAudioPlayerArgs args = new EventAudioPlayerArgs()
                {
                    EventId = EventAudioPlayerId.EqSettings,
                    StateId = StateAudioPlayerId.None,
                    EqSettings = new PlayerEqSettings(
                                    settings,
                                    _mediaEq.GetBandLevelRange(),
                                    _mediaEq.NumberOfBands,
                                    _mediaEq.NumberOfPresets)
                };
                CallEventByType(args);
                return false;
            }
            catch { return true; }
            finally { _mediaEq.SetEnabled(isenabled); }
        }

        private void DefaultEvent(StateAudioPlayerId id, int idx = -1)
        {
            EventAudioPlayerArgs args = new EventAudioPlayerArgs()
            {
                EventId = EventAudioPlayerId.EvPlayer,
                StateId = id,
                Value = idx,
                HasButtons = ButtonsState
            };
            CallEventByType(args);
        }
        private void DefaultEvent(PlayListItem pli)
        {
            if (pli == null)
                return;

            EventAudioPlayerArgs args = new EventAudioPlayerArgs()
            {
                EventId = EventAudioPlayerId.EvPlayer,
                StateId = StateAudioPlayerId.PlayingTrack,
                Text = pli.Tag,
                Url = new Uri(pli.Url),
                Value = pli.Id
            };
            CallEventByType(args);
        }
        private void DefaultEvent(Exception ex)
        {
            if (ex == null)
                return;

            EventAudioPlayerArgs args = new EventAudioPlayerArgs()
            {
                EventId = EventAudioPlayerId.EvPlayer,
                StateId = StateAudioPlayerId.Error,
                Text = ex.Message,
                Value = ex.HResult
            };
            CallEventByType(args, true);
        }
        private void DefaultEvent(TimeSpan ts)
        {
            EventAudioPlayerArgs args = new EventAudioPlayerArgs()
            {
                EventId = EventAudioPlayerId.EvPlayer,
                StateId = StateAudioPlayerId.PlayingTime,
                PlayTime = ts,
                Value = _playList.Index,
                IsValue = _mediaPlayer.IsPlaying
                
            };
            CallEventByType(args);
        }
        private void DefaultEventEf(bool b, int val)
        {
            EventAudioPlayerArgs args = new EventAudioPlayerArgs()
            {
                EventId = EventAudioPlayerId.EfVirtualize,
                StateId = StateAudioPlayerId.None,
                Value = val,
                IsValue = b
            };
            CallEventByType(args);
        }
        private void CallEventByType(IEventAudioPlayerArgs args, bool iswait = false)
        {
#           if USING_MESSAGINGCENTER
            MessagingCenter.Send<object, IEventAudioPlayerArgs>(this, EventAudioPlayerArgs.MessagingCenterId, args);
#           else
            if (iswait)
                CallEventWait(args);
            else
                CallEvent(args);
#           endif
        }
        #endregion

        #region private
        private int GetAudioSessionId_()
        {
            try
            {
                AudioManager am = (AudioManager)Android.App.Application.Context.GetSystemService(Context.AudioService);
                int id = am.GenerateAudioSessionId();
                am.Dispose();
                return id;
            }
            catch { return 0; }
        }
        private bool SetPlayList_(IList<PlayListItem> list)
        {
            try
            {
                if ((list == null) || (list.Count() == 0))
                    return false;

                HasPlayListPreload = false;
                DefaultEvent(StateAudioPlayerId.LoadingList, list.Count());
                _playList.Load(list);
                HasPlayListPreload = _playList.Enable;
                DefaultEvent(StateAudioPlayerId.LoadedList, _playList.Count);
                return HasPlayListPreload;
            } catch (Exception ex) { DefaultEvent(ex); HasPlayListPreload = false; return false; }
        }
        private async Task<bool> Play_(int idx)
        {
            try
            {
                if (!HasPlayListPreload)
                    return false;

                if ((idx >= _playList.Count) || (idx < 0))
                    return false;

                do
                {
                    string url = _playList.ById(idx);
                    if (string.IsNullOrWhiteSpace(url))
                        return false;

                    if ((_uri != null) && url.Equals(_uri.Path) && HasPlaying)
                        break;

                    if (_playerPause && _playerPrepare)
                    {
                        playerPlay();
                        DefaultEvent(StateAudioPlayerId.Playing, idx);
                        break;
                    }

                    if (_uri != null)
                        try { _uri.Dispose(); } catch { }

                    _uri = Android.Net.Uri.Parse(url);
                    if (_uri == null)
                        return false;

                    if (_playerPlay && _playerPrepare)
                        try {
                            playerStop();
                            Task.Delay(500).Wait();
                            playerReset();
                        } catch { }
                    
                    await _mediaPlayer.SetDataSourceAsync(Android.App.Application.Context, _uri)
                                      .ConfigureAwait(false);
                    Task.Delay(500).Wait();
                    playerPrepare();

                } while (false);

                DefaultEvent(_playList.Current());
#if PLAY_TIMER
                Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                    if (_playerPlay && !_playerPause)
                        DefaultEvent(TimeSpan.FromSeconds(++_playerTime));
                    return _playerPlay || _playerPause || _playerPrepare;
                });
#endif
                return true;
            } catch (Exception ex) { DefaultEvent(ex); return false; }
        }
        private bool Pause_()
        {
            try
            {
                if (!IsPauseCommand())
                    return false;

                playerPause();
                return true;
            } catch (Exception ex) { DefaultEvent(ex); return false; }
        }
        private bool Stop_()
        {
            try
            {
                if (!IsStopCommand())
                    return false;

                playerStop();
                Task.Delay(250).Wait();
                playerReset();
                return true;
            } catch (Exception ex) { DefaultEvent(ex); return false; }
        }
        private async Task<bool> Prev_()
        {
            try
            {
                if (!HasPlayListPreload || (_playList.Count == 0))
                    return false;

                int idx = _playList.Prev();
                if (idx < 0)
                {
                    if (_playList.Count > 0)
                        idx = _playList.Count - 1;
                    else
                        return false;
                }
                return await Play_(idx);
            } catch (Exception ex) { DefaultEvent(ex); return false; }
        }
        private async Task<bool> Next_()
        {
            try
            {
                if (!HasPlayListPreload || (_playList.Count == 0))
                    return false;

                int idx = _playList.Next();
                if (idx < 0)
                {
                    if (_playList.Count > 0)
                        idx = 0;
                    else
                        return false;
                }
                return await Play_(idx);
            } catch (Exception ex) { DefaultEvent(ex); return false; }
        }
#endregion
    }
}