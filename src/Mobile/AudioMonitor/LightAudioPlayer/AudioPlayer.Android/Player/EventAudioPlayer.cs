﻿using System;
using System.Collections.Generic;
using Xamarin.CommunityToolkit.Helpers;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Enums;
using System.Threading.Tasks;

namespace Plugin.LightAudioPlayer.Droid.Player
{
    public abstract class EventAudioPlayer : IEventAudioPlayer
    {
        private readonly DelegateWeakEventManager _emgr = new DelegateWeakEventManager();
        private readonly Queue<IEventAudioPlayerArgs> _queue = new Queue<IEventAudioPlayerArgs>();
        protected int _eventCount { get; private set; } = 0;
        public event EventHandler<IEventAudioPlayerArgs> StateEvent
        {
            add { _emgr.AddEventHandler(value); _eventCount++; }
            remove { _emgr.RemoveEventHandler(value); _eventCount--; }
        }
        protected void CallEventWait(IEventAudioPlayerArgs args, int ms = 250)
        {
            CallEvent(args);
            Task.Delay(ms).Wait();
        }
        protected void CallEvent(IEventAudioPlayerArgs args)
        {
            if (args == null)
                return;

            if (_eventCount == 0)
            {
                switch (args.EventId)
                {
                    case EventAudioPlayerId.None:
                    case EventAudioPlayerId.EvBufferingUpdate: return;
                }
                switch (args.StateId)
                {
                    case StateAudioPlayerId.None:
                    case StateAudioPlayerId.Loading:
                    case StateAudioPlayerId.LoadingList:
                    case StateAudioPlayerId.PlayingTime: return;
                }
                _queue.Enqueue(args);
                return;
            }
            else if (_queue.Count > 0)
            {
                try {
                    while (_queue.Count > 0)
                        _emgr.RaiseEvent(this, _queue.Dequeue(), nameof(StateEvent));
                } catch { }
            }
            _emgr.RaiseEvent(this, args, nameof(StateEvent));
        }
    }
}