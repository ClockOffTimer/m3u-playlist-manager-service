﻿using Android.Media.Audiofx;
using Plugin.LightAudioPlayer.Base.Interfaces;

namespace Plugin.LightAudioPlayer.Base.Player
{
    public class PlayerEqSettings : IPlayerEqSettings
    {
        public float [] BandRanges { get; set; } = default(float[]);
        public float [] BandLevels { get; set; } = default(float[]);
        public short PresetCurent { get; set; } = -1;
        public short Preset { get; set; } = -1;
        public short Bands { get; set; } = -1;
        public bool  IsEmpty { get; private set; } = true;

        public PlayerEqSettings(Equalizer.Settings s, short[] range, short bands, short presets)
        {
            if (s == null)
                return;
            if ((s.BandLevels != null) && (s.BandLevels.Count > 0))
            {
                BandLevels = new float [s.BandLevels.Count];
                for (int i = 0; i < s.BandLevels.Count; i++)
                    BandLevels[i] = (float)s.BandLevels[i];

                if ((range != null) && (range.Length > 0))
                {
                    BandRanges = new float [range.Length];
                    for (int i = 0; i < range.Length; i++)
                        BandRanges[i] = (float)range[i];
                }
            }
            Bands = (bands <= 0) ? s.NumBands : bands;
            Preset = presets;
            PresetCurent = s.CurPreset;
            IsEmpty = false;
        }
        public override string ToString()
        {
            bool [] b = new bool[2] {
                (BandLevels != null) && (BandLevels.Length > 0),
                (BandRanges != null) && (BandRanges.Length > 0)
            };
            if (b[0] && b[1])
                return $"{Preset}/{PresetCurent}, {Bands}:{string.Join('|', BandLevels)}, {BandRanges.Length}:{string.Join('|', BandRanges)}";
            else
            if (b[0])
                return $"{Preset}/{PresetCurent}, {Bands}:{string.Join('|', BandLevels)}";
            else
            if (b[1])
                return $"{Preset}/{PresetCurent}, {Bands}, {BandRanges.Length}:{string.Join('|', BandRanges)}";
            else
                return $"{Preset}/{PresetCurent}, {Bands}:settings is null, ranges is null";
        }
    }
}
