﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using AndroidX.Core.App;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Base.Utils;
using LocalString = Plugin.LightAudioPlayer.Droid.Resource.String;

[assembly: Xamarin.Forms.Dependency(typeof(Plugin.LightAudioPlayer.Droid.Notify.NotificationService))]
namespace Plugin.LightAudioPlayer.Droid.Notify
{
    [Preserve(AllMembers = true)]
    public class NotificationService : IDisposable
    {
        public static readonly int ID_NOTIFY = 1;
        public static readonly int ID_CHANNEL = 1007;
        public static readonly string ID_LayoutNotify = "notification_player";
        public static readonly string ID_LayoutWdget = "widget_player";

        public bool IsVibro { get; set; } = false;
        public RemoteViews viewLayout = default(RemoteViews);
        private readonly WeakReference<Service> WeakContext;
        private NotificationBuilder notifyBuilder = new NotificationBuilder();
        private PlayerWidgetBuilder widgetBuilder = new PlayerWidgetBuilder();

        public static readonly NotificationServiceItem[] ButtonIndex = new NotificationServiceItem[] {
            new NotificationServiceItem(StateAudioPlayerId.Prev,    Resource.Id.ico_prev),
            new NotificationServiceItem(StateAudioPlayerId.Playing, Resource.Id.ico_playing),
            new NotificationServiceItem(StateAudioPlayerId.Stopped, Resource.Id.ico_paused),
            new NotificationServiceItem(StateAudioPlayerId.Paused,  Resource.Id.ico_stoped),
            new NotificationServiceItem(StateAudioPlayerId.Next,    Resource.Id.ico_next),
            new NotificationServiceItem(StateAudioPlayerId.Notify,  Resource.Id.ico_notify)
        };
        public NotificationService(Service ctx) => WeakContext = new WeakReference<Service>(ctx);
        ~NotificationService() => Dispose();

        public void Dispose()
        {
            notifyBuilder.Dispose();
            widgetBuilder.Dispose();
        }

        #region public
        public void WidgetDisable() => widgetBuilder.WidgetDisable();
        public void WidgetDelete() => widgetBuilder.WidgetDelete();

        public void CreateChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
                return;

            NotificationManager manager = default;
            NotificationChannel cnotify = default;

            try
            {
                if (!WeakContext.TryGetTarget(out Service srv))
                    throw new NullReferenceException(nameof(WeakContext));

                manager = srv.GetSystemService(Context.NotificationService) as NotificationManager;
                if (manager == null)
                    throw new NullReferenceException(nameof(manager));

                cnotify = new NotificationChannel(
                                ID_CHANNEL.ToString(),
                                srv.GetString(LocalString.notify_channel_name),
                                NotificationImportance.Default) {
                                    Description = srv.GetString(LocalString.notify_channel_desc),
                                    LightColor = Android.Graphics.Color.DarkOrange,
                                    LockscreenVisibility = NotificationVisibility.Public
                                };
                if (IsVibro)
                {
                    cnotify.EnableVibration(true);
                    cnotify.SetVibrationPattern(new long[] { 100, 200, 300, 400, 500, 400, 300, 200, 400 });
                }
                cnotify.EnableLights(true);
                cnotify.SetShowBadge(true);
                manager.CreateNotificationChannel(cnotify);
            }
            catch (Exception) { throw; }
            finally
            {
                if (cnotify != null)
                    cnotify.Dispose();
                if (manager != null)
                    manager.Dispose();
            }
        }
        
        public void StopNotify()
        {
            try {
                if (!WeakContext.TryGetTarget(out Service srv))
                    throw new NullReferenceException(nameof(WeakContext));

                NotificationManager manager = srv.GetSystemService(Context.NotificationService) as NotificationManager;
                manager.CancelAll();
                manager.Dispose();
            }
            catch (Exception ex) { LogWriter.Debug<NotificationService>(nameof(StopNotify), ex); }
        }
        
        public void CreateNotify() =>
            CreateNotify(null, new bool[] { false, false, false, false, false, true });
        
        public void CreateNotify(string title, string info, bool[] arr) =>
            CreateNotify(null, arr, title, info);

        public void CreateNotify(PlayListItem item, bool[] arr, string title = null, string info = null)
        {
            try {
                if (!WeakContext.TryGetTarget(out Service srv))
                    throw new NullReferenceException(nameof(WeakContext));
                if ((arr == null) || (arr.Length < 6))
                    throw new IndexOutOfRangeException(nameof(arr));

                if (widgetBuilder.IsEnable)
                    widgetBuilder.Build();
                NotificationCompat.Builder builder = notifyBuilder.Build(srv, !arr[5]);
                (string t, string i) = CreateTextBody(item, title, info);
                ParseNotifyBody(srv, builder, t, i, arr);
                srv.StartForeground(ID_NOTIFY, builder.Build());
                builder.Dispose();
            }
            catch (Exception ex) { LogWriter.Debug<NotificationService>(nameof(CreateNotify), ex); }
        }

        public void CreateNotifyListLoad(bool isnext)
        {
            try {
                if (!WeakContext.TryGetTarget(out Service srv))
                    throw new NullReferenceException(nameof(WeakContext));

                if (widgetBuilder.IsEnable)
                    widgetBuilder.Build();
                widgetBuilder.Build();
                NotificationCompat.Builder builder = notifyBuilder.Build(srv, true);
                ParseNotifyBody(srv, builder,
                    srv.GetString(LocalString.notify_title_load),
                    srv.GetString(LocalString.notify_desc),
                    new bool[] { false, true, false, false, isnext, false });
                srv.StartForeground(ID_NOTIFY, builder.Build());
                builder.Dispose();
            }
            catch (Exception ex) { LogWriter.Debug<NotificationService>(nameof(CreateNotifyListLoad), ex); }
        }
        public void UpdateWidget() => widgetBuilder.UpdateWidget();
        #endregion

        #region private
        private NotificationServiceItem FindNotificationItem(StateAudioPlayerId id)
        {
            return (from i in ButtonIndex
                    where i.ActionId == id
                    select i).FirstOrDefault();
        }
        private async void ParseNotifyBody(Context ctx, NotificationCompat.Builder builder, string title, string info, bool [] arr)
        {
            try {
                List<NotificationServiceItem> actions = new List<NotificationServiceItem>();

                if (arr[5])
                {
                    notifyBuilder.UpdateButton(ctx, 5, true, StateAudioPlayerId.None);
                    widgetBuilder.UpdateButton(ctx, 5, true, StateAudioPlayerId.None);
                }
                else
                {
                    for (int i = 0, n = 0; (i < ButtonIndex.Length) && (i < arr.Length); i++)
                    {
                        if (!ButtonIndex[i].Enable)
                            continue;

                        switch (i)
                        {
                            case 0:
                                {
                                    if (!arr[i])
                                        break;
                                    if (arr[1] && (n < 3))
                                    {
                                        actions.Add(FindNotificationItem(StateAudioPlayerId.Prev));
                                        n++;
                                    }
                                    ButtonEnable(ctx, 0, StateAudioPlayerId.Prev);
                                    break;
                                }
                            case 1:
                                {
                                    if (!arr[i])
                                        break;
                                    if (n < 3)
                                    {
                                        actions.Add(FindNotificationItem(StateAudioPlayerId.Playing));
                                        n++;
                                    }
                                    ButtonEnable(ctx, 1, StateAudioPlayerId.Playing);
                                    break;
                                }
                            case 2:
                                {
                                    if (arr[1] || !arr[i])
                                        break;
                                    if (n < 3)
                                    {
                                        actions.Add(FindNotificationItem(StateAudioPlayerId.Paused));
                                        n++;
                                    }
                                    ButtonEnable(ctx, 2, StateAudioPlayerId.Paused);
                                    break;
                                }
                            case 3:
                                {
                                    if (arr[1] || !arr[i]) 
                                        break;
                                    if (n < 3)
                                    {
                                        actions.Add(FindNotificationItem(StateAudioPlayerId.Stopped));
                                        n++;
                                    }
                                    ButtonEnable(ctx, 3, StateAudioPlayerId.Stopped);
                                    break;
                                }
                            case 4:
                                {
                                    if (!arr[i])
                                        break;
                                    if (n < 3)
                                    {
                                        actions.Add(FindNotificationItem(StateAudioPlayerId.Next));
                                        n++;
                                    }
                                    ButtonEnable(ctx, 4, StateAudioPlayerId.Next);
                                    break;
                                }
                        }
                    }
                }

                notifyBuilder.UpdateActions(ctx, builder, actions);
                actions.Clear();

                Type t = AudioPlayerAndroid.GetActivityType();
                PendingIntent pi = (t == default) ? ctx.GetActivityPendingIntent() : ctx.GetActivityPendingIntent(t);
                notifyBuilder.UpdateText(ctx, title, info, pi);
                widgetBuilder.UpdateText(ctx, title, info, pi);
                await widgetBuilder.UpdateWidgetAsync().ConfigureAwait(false);
            }
            catch (Exception ex) {
                LogWriter.Debug<NotificationService>(nameof(ParseNotifyBody), ex);
                try {
                    NotificationCompat.BigTextStyle txt =
                        new NotificationCompat.BigTextStyle()
                            .BigText(title)
                            .SetSummaryText(info);
                    builder.SetStyle(txt);
                } catch { }
            }
        }
        private (string, string) CreateTextBody(PlayListItem item, string title, string info)
        {
            try {
                if (item != null)
                {
                    title = (item == null) ? ((title == null) ? string.Empty : title) : item.Tag;
                    info = (item == null) ? ((info == null) ? string.Empty : title) :
                            (string.IsNullOrWhiteSpace(item.Url) ? string.Empty : $"{item.Id}: {new Uri(item.Url).DnsSafeHost}");
                }
                if (string.IsNullOrWhiteSpace(title))
                    title = Application.Context.GetString(LocalString.notify_title);
                if (string.IsNullOrWhiteSpace(info))
                    info = Application.Context.GetString(LocalString.notify_desc);
                return (title, info);
            } catch { }
            return (string.Empty, string.Empty);
        }
        private void ButtonEnable(Context ctx, int idx, StateAudioPlayerId id)
        {
            notifyBuilder.UpdateButton(ctx, idx, true, id);
            widgetBuilder.UpdateButton(ctx, idx, true, id);
        }
        #endregion
    }
}