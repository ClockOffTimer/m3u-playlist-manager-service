﻿
using System;
using System.Threading.Tasks;
using Android.App;
using Android.Appwidget;
using Android.Content;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Events;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Utils;
using Plugin.LightAudioPlayer.Droid.Services;
using Xamarin.Forms;

namespace Plugin.LightAudioPlayer.Droid.Notify
{
    [BroadcastReceiver(Enabled = true, Label = "LightAudioPlayerWidget")]
	[IntentFilter(new string[] { PlayerWidgetProvider.IntentFilterName })]
	[MetaData("android.appwidget.provider", Resource = "@xml/appwidgetprovider")]
	public class PlayerWidgetProvider : AppWidgetProvider
    {
		public static readonly string WidgetIdsName = "WidgetIds";
		public string ComponentName => Class.Name;
		public const string IntentFilterName = "android.appwidget.action.APPWIDGET_UPDATE";
		public static PendingIntent GetPendingIntent(Context ctx, StateAudioPlayerId id) =>
			ctx.GetReceiverPendingIntent<PlayerWidgetProvider>(id, IntentFilterName);

		public PlayerWidgetProvider() { }

		public async override void OnReceive(Context context, Intent intent)
		{
			base.OnReceive(context, intent);
			string s = intent.GetStringExtra("ID");
			if (!string.IsNullOrWhiteSpace(s) && Enum.TryParse(s, out StateAudioPlayerId id))
			{
				LogWriter.Debug<PlayerWidgetProvider>($"{nameof(OnReceive)} -> RECEIVE: {id}/{s}");
				switch (id)
                {
					case StateAudioPlayerId.WidgetReloadList:
                        {
							try {
								MessagingCenter.Send<object, IEventAudioPlayerArgs>(
									this,
									EventAudioPlayerArgs.MessagingCenterId,
									new EventAudioPlayerArgs
									{
										EventId = EventAudioPlayerId.EvService,
										StateId = StateAudioPlayerId.WidgetReloadList,
										IsValue = true
									});
							} catch { }
							break;
                        }
					case StateAudioPlayerId.WidgetUpdate:
						{
							await UpdateWidget(context, StateAudioPlayerId.WidgetUpdate, new int[] { 0 });
							break;
						}
				}
			}
		}

		public async override void OnUpdate(Context context, AppWidgetManager appWidgetManager, int[] ids)
		{
			base.OnUpdate(context, appWidgetManager, ids);
			await UpdateWidget(context, StateAudioPlayerId.WidgetUpdate, ids);
		}
		public async override void OnDeleted(Context context, int[] appWidgetIds)
		{
			base.OnDeleted(context, appWidgetIds);
			await UpdateWidget(context, StateAudioPlayerId.WidgetDeleted);
		}
		public async override void OnDisabled(Context context)
		{
			base.OnDisabled(context);
			await UpdateWidget(context, StateAudioPlayerId.WidgetDisabled);
		}

		private async Task UpdateWidget(Context context, StateAudioPlayerId id, int[] ids = default)
        {
			await Task.Run(() => {
				Intent i = new Intent(context, typeof(AudioPlayerService)).SetAction(id.ToString());
				if (ids != null)
					i.PutExtra(WidgetIdsName, ids);
				context.StartForegroundService(i);
			}).ConfigureAwait(false);
		}
	}
}