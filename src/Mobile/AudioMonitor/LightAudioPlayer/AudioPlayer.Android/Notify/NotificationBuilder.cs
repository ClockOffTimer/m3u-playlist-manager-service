﻿
using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Views;
using Android.Widget;
using AndroidX.Core.App;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Utils;
using static Plugin.LightAudioPlayer.Droid.Services.AudioPlayerService;

namespace Plugin.LightAudioPlayer.Droid.Notify
{
    internal class NotificationBuilder : IDisposable
    {
        private RemoteViews view = default;

        public NotificationBuilder() => CreateView();
        ~NotificationBuilder() => Dispose();

        public NotificationCompat.Builder Build(Context ctx, bool b) => Create(ctx, b);

        public void UpdateButton(Context ctx, int idx, bool b, StateAudioPlayerId id)
        {
            if (idx < 0 || idx >= NotificationService.ButtonIndex.Length)
                return;
            view.SetViewVisibility(
                NotificationService.ButtonIndex[idx].LayoutId, b ? ViewStates.Visible : ViewStates.Gone);
            if (id != StateAudioPlayerId.None)
                view.SetOnClickPendingIntent(
                    NotificationService.ButtonIndex[idx].LayoutId,
                    ServiceCommandReceiver.GetPendingIntent(ctx, id));
        }
        public void UpdateText(Context _, string title, string info, PendingIntent pi = default)
        {
            view.SetTextViewText(Resource.Id.txt_info, info);
            view.SetTextViewText(Resource.Id.txt_title, title);
            if (pi != null)
                view.SetOnClickPendingIntent(Resource.Id.tap_region, pi);
        }
        public void UpdateActions(Context ctx, NotificationCompat.Builder builder, List<NotificationServiceItem> actions = default)
        {
            builder.SetAllowSystemGeneratedContextualActions(true);
            builder.SetStyle(new NotificationCompat.DecoratedCustomViewStyle());
            builder.SetCustomContentView(view);
            builder.SetCustomBigContentView(view);
            if (actions != null)
                foreach (var a in actions)
                    AddAction(ctx, builder, a);
        }
        public void Dispose()
        {
            RemoteViews v = view;
            view = null;
            if (v != null)
                try { v.Dispose(); } catch { }
        }

        #region private
        private NotificationCompat.Builder Create(Context ctx, bool b)
        {
            CreateView();

            foreach (var a in NotificationService.ButtonIndex)
                view.SetViewVisibility(a.LayoutId, ViewStates.Gone);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(ctx, NotificationService.ID_CHANNEL.ToString())
               .SetDefaults((int)NotificationDefaults.Lights)
               .SetVisibility((int)NotificationVisibility.Public)
               .SetCategory(Notification.CategoryService)
               .SetSmallIcon(Resource.Drawable.notify_light)
               .SetOngoing(true)
               .SetAutoCancel(b);
            return builder;
        }
        private void AddAction(Context ctx, NotificationCompat.Builder builder, NotificationServiceItem item)
        {
            try {
                if (item == null)
                    return;
                _ = builder.AddAction(new NotificationCompat.Action(
                        ctx.GetResourceId(item.ImageId),
                        ctx.GetResourceString(item.TitleId),
                        ServiceCommandReceiver.GetPendingIntent(ctx, item.ActionId)));
            } catch (Exception ex) { LogWriter.Debug<NotificationBuilder>($"{nameof(AddAction)} -> {item}", ex); }
        }
        private void CreateView()
        {
            if (view == null)
                view = new RemoteViews(
                    Application.Context.PackageName,
                    Application.Context.GetResourceId(NotificationService.ID_LayoutNotify, "layout"));
        }
        #endregion
    }
}