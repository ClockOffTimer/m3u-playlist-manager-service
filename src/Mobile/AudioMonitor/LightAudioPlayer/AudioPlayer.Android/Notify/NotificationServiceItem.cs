﻿using Plugin.LightAudioPlayer.Base.Enums;

namespace Plugin.LightAudioPlayer.Droid.Notify
{
    public class NotificationServiceItem
    {
        public bool   Enable { get => ActionId != StateAudioPlayerId.None; }
        public int LayoutId { get; private set; } = -1;
        public string ImageId { get; private set; } = string.Empty;
        public string TitleId { get; private set; } = string.Empty;
        public StateAudioPlayerId ActionId { get; set; } = StateAudioPlayerId.None;
        public NotificationServiceItem(StateAudioPlayerId id, int lid)
        {
            string sid = id.ToString();
            ActionId = id;
            LayoutId = lid;
            ImageId = $"{sid.ToLowerInvariant()}_dark";
            switch(id)
            {
                case StateAudioPlayerId.Next:
                case StateAudioPlayerId.Prev:
                case StateAudioPlayerId.Stopped:
                case StateAudioPlayerId.Paused:
                case StateAudioPlayerId.Playing: TitleId = $"notify_{sid.ToLowerInvariant()}"; break;
                default: TitleId = "notify_empty"; break;
            };
        }
        public override string ToString()
        {
            return $"Item:({Enable}, {LayoutId}, {ImageId}, {TitleId}, {ActionId})";
        }
    }
}