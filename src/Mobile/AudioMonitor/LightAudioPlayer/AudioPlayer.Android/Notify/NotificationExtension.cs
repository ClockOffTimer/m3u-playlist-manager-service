﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using Android.App;
using Android.Content;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Droid.Services;

namespace Plugin.LightAudioPlayer.Droid.Notify
{
    internal static class NotificationExtension
    {
        private static Intent ServiceIntent(Context ctx, StateAudioPlayerId id) =>
            new Intent(ctx, typeof(AudioPlayerService)).SetAction(id.ToString());

        #region static *Intent
        public static PendingIntent GetActivityPendingIntent(this Context ctx) =>
            PendingIntent.GetActivity((ctx == null) ? Application.Context : ctx, 0,
                    new Intent()
                        .SetComponent(GetComponentName("droid.mainactivity")),
                        //.SetFlags(ActivityFlags.BroughtToFront),
                    PendingIntentFlags.UpdateCurrent);

        public static PendingIntent GetActivityPendingIntent(this Context ctx, Type t) =>
            PendingIntent.GetActivity((ctx == null) ? Application.Context : ctx, 0,
                    new Intent((ctx == null) ? Application.Context : ctx, t),
                        //.SetFlags(ActivityFlags.BroughtToFront),
                    PendingIntentFlags.UpdateCurrent);

        public static PendingIntent GetActivityStackPendingIntent(this Context ctx)
        {
            TaskStackBuilder tb = TaskStackBuilder.Create((ctx == null) ? Application.Context : ctx);
            tb.AddParentStack(GetComponentName("droid.mainactivity"));
            tb.AddNextIntent(new Intent().SetComponent(GetComponentName("droid.mainactivity")).SetFlags(ActivityFlags.ClearTop));
            return tb.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);
        }

        public static PendingIntent GetServicePendingIntent(this
            Context ctx, StateAudioPlayerId id = StateAudioPlayerId.None, PendingIntentFlags flags = PendingIntentFlags.UpdateCurrent) =>
            PendingIntent.GetService(ctx, (int)id, ServiceIntent(ctx, id), flags);

        public static PendingIntent GetReceiverPendingIntent<T>(this Context ctx, StateAudioPlayerId id, string filter) =>
            PendingIntent.GetBroadcast(ctx as ContextWrapper, (int)id,
                new Intent(ctx as ContextWrapper, typeof(T))
                    .SetAction(filter)
                    .PutExtra("ID", id.ToString()),
                PendingIntentFlags.UpdateCurrent);

        public static PendingIntent GetServiceReceiverPendingIntent(this Context ctx, StateAudioPlayerId id, string filter) =>
            PendingIntent.GetBroadcast(ctx as ContextWrapper, (int)id,
                new Intent("com.m3uservice.receiver.internal_notify_command")
                    .PutExtra("ID", id.ToString()),
                PendingIntentFlags.UpdateCurrent);

        public static int GetResourceId(this Context ctx, string name, string group = "drawable") =>
            ((ctx == null) ? Application.Context : ctx).Resources.GetIdentifier(name, group, Application.Context.PackageName);

        public static string GetResourceString(this Context ctx, string id) =>
            ((ctx == null) ? Application.Context : ctx).GetString(
                ((ctx == null) ? Application.Context : ctx).Resources.GetIdentifier(
                    id, "string", Application.Context.PackageName));

        public static ComponentName GetComponentName<T>() =>
            new ComponentName(Application.Context, Java.Lang.Class.FromType(typeof(T)).Name);

        public static ComponentName GetComponentName(string s) =>
            new ComponentName(Application.Context.PackageName, $"{Application.Context.PackageName}.{s}");

        public static void SendToService(this Context ctx, StateAudioPlayerId id) =>
            ctx.StartForegroundService(ServiceIntent(ctx, id));

        #endregion
    }
}