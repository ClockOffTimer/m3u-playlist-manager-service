﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading.Tasks;
using Android.App;
using Android.Appwidget;
using Android.Content;
using Android.Views;
using Android.Widget;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Utils;
using static Plugin.LightAudioPlayer.Droid.Services.AudioPlayerService;

namespace Plugin.LightAudioPlayer.Droid.Notify
{
    internal class PlayerWidgetBuilder : IDisposable
    {
        private RemoteViews view = default;

        public PlayerWidgetBuilder() => CreateView();
        ~PlayerWidgetBuilder() => Dispose();

        public bool IsEnable { get; private set; } = false;

        public void Build()
        {
            if (!IsEnable)
                return;
            Create();
        }
        public void UpdateButton(Context ctx, int idx, bool b, StateAudioPlayerId id)
        {
            if (!IsEnable || (idx < 0) || (idx >= NotificationService.ButtonIndex.Length))
                return;
            view.SetViewVisibility(
                NotificationService.ButtonIndex[idx].LayoutId, b ? ViewStates.Visible : ViewStates.Gone);
            if (id != StateAudioPlayerId.None)
                view.SetOnClickPendingIntent(
                    NotificationService.ButtonIndex[idx].LayoutId,
                    ServiceCommandReceiver.GetPendingIntent(ctx, id));
        }
        public void UpdateText(Context ctx, string title, string info, PendingIntent pi = null)
        {
            if (!IsEnable)
                return;
            view.SetTextViewText(Resource.Id.txt_info, info);
            view.SetTextViewText(Resource.Id.txt_title, title);

            if (pi == null) {
                Type t = AudioPlayerAndroid.GetActivityType();
                pi = (t == default) ? ctx.GetActivityPendingIntent() : ctx.GetActivityPendingIntent(t);
            }
            view.SetOnClickPendingIntent(Resource.Id.ico_settings, pi);
            view.SetOnClickPendingIntent(Resource.Id.ico_reload,
                 ctx.GetServicePendingIntent(StateAudioPlayerId.WidgetReloadList));
            view.SetOnClickPendingIntent(Resource.Id.tap_region,
                 ctx.GetServicePendingIntent(StateAudioPlayerId.WidgetUpdate));
        }
        public async Task UpdateWidgetAsync()
        {
            if (!IsEnable)
                return;
            await Task.Run(() => Update()).ConfigureAwait(false);
        }

        public void UpdateWidget()
        {
            IsEnable = true;
            CreateView();
            Update();
        }

        public void WidgetDisable() => IsEnable = false;
        public void WidgetDelete()
        {
            IsEnable = false;
            Dispose();
        }

        public void Dispose()
        {
            RemoteViews v = view;
            view = null;
            if (v != null)
                try { v.Dispose(); } catch { }
        }

        #region private
        private void Create()
        {
            CreateView();

            foreach (var a in NotificationService.ButtonIndex)
                view.SetViewVisibility(a.LayoutId, ViewStates.Gone);
        }
        private void CreateView()
        {
            if (view == null)
                view = new RemoteViews(
                    Application.Context.PackageName,
                    Application.Context.GetResourceId(NotificationService.ID_LayoutWdget, "layout"));
        }
        private void Update()
        {
            if (!IsEnable)
                return;
            try {

                AppWidgetManager mgr = AppWidgetManager.GetInstance(Application.Context);
                if (mgr == null)
                    return;
                mgr.UpdateAppWidget(NotificationExtension.GetComponentName<PlayerWidgetProvider>(), view);
            } catch (Exception ex) { LogWriter.Debug<PlayerWidgetBuilder>(nameof(UpdateWidget), ex); }
        }
        #endregion

    }
}