﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Runtime;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.PlayList;
using Plugin.LightAudioPlayer.Droid.Services;

namespace Plugin.LightAudioPlayer.Droid
{
    [Preserve(AllMembers = true)]
    public class AudioPlayerAndroid : ICrossAudioPlayer
    {
        private IAudioPlayerServiceControl _iacontrol = null;
        private static Lazy<AudioPlayerAndroid> _Instace = new Lazy<AudioPlayerAndroid>(new AudioPlayerAndroid());
        public  static ICrossAudioPlayer Get() => _Instace.Value;
#       pragma warning disable CS8632
        private static Type? _ActivityType = default;
        public  static Type? GetActivityType() => _ActivityType;
#       pragma warning restore CS8632
        private static bool _ServiceActive = false;
        public static bool GetServiceActive() => _ServiceActive;
        public static void SetServiceActive(bool b) => _ServiceActive = b;
        public void SetActivityType(Type t) => AudioPlayerAndroid._ActivityType = t;

        private AudioPlayerAndroid() { }
        ~AudioPlayerAndroid() { }

        public IAudioPlayerServiceControl Control
        {
            get
            {
                if (_iacontrol == null)
                    _iacontrol = new AudioPlayerServiceControl();
                return _iacontrol;
            }
        }
        public void Load(List<PlayListItem> list) => Control?.Load(list);
        public void Play(int idx) => Control?.Play(idx);
        public void Play() => Control?.Play();
        public void Pause() => Control?.Pause();
        public void Stop() => Control?.Stop();
        public void Prev() => Control?.Prev();
        public void Next() => Control?.Next();
        public void Volume(float[] val) => Control?.Volume(val);
        public void LoopPlay(bool b) => Control?.LoopPlay(b);
        public void AutoPlay(bool b) => Control?.AutoPlay(b);
        public void EqEnable(bool b) => Control?.EqEnable(b);
        public void EqPreset(int idx) => Control?.EqPreset(idx);
        public void EqChannels(float[] channels) => Control?.EqChannels(channels);
        public void EqSettings() => Control?.EqSettings();
        public void EfAutoGain(bool b) => Control?.EfAutoGain(b);
        public void EfLoudness(int i) => Control?.EfLoudness(i);
        public void EfBasBoost(int i) => Control?.EfBasBoost(i);
        public void EfVirtualizer(int i) => Control?.EfVirtualizer(i);
        public void EfVirtualizerMode(int i) => Control?.EfVirtualizerMode(i);
        public void GetServiceSettings() => Control?.GetServiceSettings();
        public void StartForegroundServiceCompat() {
            if (!AudioPlayerAndroid._ServiceActive)
                Control?.StartForegroundServiceCompat();
        }
        public void StopForegroundServiceCompat() {
            if (AudioPlayerAndroid._ServiceActive)
                Control?.StopForegroundServiceCompat();
        }

        public async Task LoadAsync(List<PlayListItem> list) => await Task.Run(() => {
            Control?.Load(list);
        });
        public async Task PlayAsync(int idx) => await Task.Run(() => {
            Control?.Play(idx);
        });
        public async Task PlayAsync() => await Task.Run(() => {
            Control?.Play();
        });
        public async Task PauseAsync() => await Task.Run(() => {
            Control?.Pause();
        });
        public async Task StopAsync() => await Task.Run(() => {
            Control?.Stop();
        });
        public async Task PrevAsync() => await Task.Run(() => {
            Control?.Prev();
        });
        public async Task NextAsync() => await Task.Run(() => {
            Control?.Next();
        });
        public async Task VolumeAsync(float[] val) => await Task.Run(() => {
            Control?.Volume(val);
        });
        public async Task LoopPlayAsync(bool b) => await Task.Run(() => {
            Control?.LoopPlay(b);
        });
        public async Task AutoPlayAsync(bool b) => await Task.Run(() => {
            Control?.AutoPlay(b);
        });
        public async Task EqEnableAsync(bool b) => await Task.Run(() => {
            Control?.EqEnable(b);
        });
        public async Task EqPresetAsync(int idx) => await Task.Run(() => {
            Control?.EqPreset(idx);
        });
        public async Task EqChannelsAsync(float[] channels) => await Task.Run(() => {
            Control?.EqChannels(channels);
        });
        public async Task EqSettingsAsync() => await Task.Run(() => {
            Control?.EqSettings();
        });
        public async Task EfAutoGainAsync(bool b) => await Task.Run(() => {
            Control?.EfAutoGain(b);
        });
        public async Task EfLoudnessAsync(int idx) => await Task.Run(() => {
            Control?.EfLoudness(idx);
        });
        public async Task EfBasBoostAsync(int idx) => await Task.Run(() => {
            Control?.EfBasBoost(idx);
        });
        public async Task EfVirtualizerAsync(int idx) => await Task.Run(() => {
            Control?.EfVirtualizer(idx);
        });
        public async Task EfVirtualizerModeAsync(int idx) => await Task.Run(() => {
            Control?.EfVirtualizerMode(idx);
        });
        public async Task GetServiceSettingsAsync() => await Task.Run(() => {
            Control?.GetServiceSettings();
        });
        public async Task StartForegroundServiceCompatAsync() => await Task.Run(() => {
            if (!AudioPlayerAndroid._ServiceActive)
                Control?.StartForegroundServiceCompat();
        });
        public async Task StopForegroundServiceCompatAsync() => await Task.Run(() => {
            if (AudioPlayerAndroid._ServiceActive)
                Control?.StopForegroundServiceCompat();
        });
    }
}
