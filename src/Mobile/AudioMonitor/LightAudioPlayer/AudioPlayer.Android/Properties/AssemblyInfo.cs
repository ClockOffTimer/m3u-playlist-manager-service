﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Reflection;
using System.Runtime.InteropServices;
using Android.App;

[assembly: AssemblyTitle("AudioPlayer")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("AudioPlayer")]
[assembly: AssemblyCopyright("Copyright © 2021-2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

[assembly: AssemblyVersion("1.0.2.0")]
// [assembly: AssemblyFileVersion("1.0.1.0")]

#if DEBUG
// [assembly: Application(Debuggable=true)]
#else
// [assembly: Application(Debuggable=false)]
#endif

[assembly: UsesPermission(Android.Manifest.Permission.ReceiveBootCompleted)]
[assembly: UsesPermission(Android.Manifest.Permission.Internet)]
[assembly: UsesPermission(Android.Manifest.Permission.ModifyAudioSettings)]
[assembly: UsesPermission(Android.Manifest.Permission.CaptureAudioOutput)]
[assembly: UsesPermission(Android.Manifest.Permission.ForegroundService)]
[assembly: UsesPermission(Android.Manifest.Permission.StartForegroundServicesFromBackground)]
[assembly: UsesPermission(Android.Manifest.Permission.KillBackgroundProcesses)]
