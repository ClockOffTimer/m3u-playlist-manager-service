﻿namespace Plugin.LightAudioPlayer.Base.Enums
{
    public enum EventAudioPlayerId : int
    {
        None = 0,
        EvInfo,
        EvError,
        EvPrepared,
        EvCompletion,
        EvBufferingUpdate,
        EqControlStatus,
        EqEnableStatus,
        EqSettings,
        EqParameter,
        EfVirtualize,
        EfVirtualizeControlStatus,
        EfVirtualizeEnableStatus,
        EfLoudnessControlStatus,
        EfLoudnessEnableStatus,
        EfBassBoostControlStatus,
        EfBassBoostEnableStatus,
        EfAutoGainControlStatus,
        EfAutoGainEnableStatus,
        EvService,
        EvPlayer
    }
}