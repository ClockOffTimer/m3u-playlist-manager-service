﻿namespace Plugin.LightAudioPlayer.Base.Enums
{
    public enum EqualizerParamId : int
    {
        None = -1,
        BandFreqRange = 4,
        BandLevel = 2,
        CenterFreq = 3,
        CurrentPreset = 6,
        GetBand = 5,
        GetNumOfPresets = 7,
        GetPresetName = 8,
        LevelRange = 1,
        NumBands = 0,
        StringSizeMax = 0x20
    }
}