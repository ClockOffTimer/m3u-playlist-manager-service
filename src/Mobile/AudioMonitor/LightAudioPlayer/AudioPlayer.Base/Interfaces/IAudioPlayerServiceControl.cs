﻿using System.Collections.Generic;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IAudioPlayerServiceControl
    {
        void Load(List<PlayListItem> list);
        void Play(int idx);
        void Play();
        void Pause();
        void Stop();
        void Prev();
        void Next();
        void Volume(float[] val);
        void LoopPlay(bool b);
        void AutoPlay(bool b);
        void EqEnable(bool b);
        void EqPreset(int idx);
        void EqChannels(float[] channels);
        void EqSettings();
        void EfAutoGain(bool b);
        void EfLoudness(int i);
        void EfBasBoost(int i);
        void EfVirtualizer(int i);
        void EfVirtualizerMode(int i);
        void GetServiceSettings();
        void StartForegroundServiceCompat();
        void StopForegroundServiceCompat();
    }
}