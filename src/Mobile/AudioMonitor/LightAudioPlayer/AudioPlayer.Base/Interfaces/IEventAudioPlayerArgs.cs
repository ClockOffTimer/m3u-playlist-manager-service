﻿using System;
using Plugin.LightAudioPlayer.Base.Enums;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IEventAudioPlayerArgs
    {
        bool [] HasButtons { get; set; }
        EqualizerParamId[] EqParamId { get; set; }
        IPlayerEqSettings  EqSettings { get; set; }
        EventAudioPlayerId EventId { get; set; }
        StateAudioPlayerId StateId { get; set; }
        TimeSpan PlayTime { get; set; }
        Uri Url { get; set; }
        object Obj { get; set; }
        int Value { get; set; }
        bool IsValue { get; set; }
        string Text { get; set; }

        bool Copy(IEventAudioPlayerArgs args);
        string ToString();
    }
}