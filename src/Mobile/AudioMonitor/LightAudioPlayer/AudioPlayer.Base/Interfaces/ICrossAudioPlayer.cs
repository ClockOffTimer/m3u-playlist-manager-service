﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface ICrossAudioPlayer : IAudioPlayerServiceControl
    {
        IAudioPlayerServiceControl Control { get; }
        void SetActivityType(Type t);

        Task LoadAsync(List<PlayListItem> list);
        Task PlayAsync(int idx);
        Task PlayAsync();
        Task PauseAsync();
        Task StopAsync();
        Task PrevAsync();
        Task NextAsync();

        Task VolumeAsync(float[] val);
        Task LoopPlayAsync(bool b);
        Task AutoPlayAsync(bool b);

        Task EqEnableAsync(bool b);
        Task EqPresetAsync(int idx);
        Task EqChannelsAsync(float[] channels);
        Task EqSettingsAsync();

        Task EfAutoGainAsync(bool b);
        Task EfLoudnessAsync(int i);
        Task EfBasBoostAsync(int i);
        Task EfVirtualizerAsync(int i);
        Task EfVirtualizerModeAsync(int idx);

        Task GetServiceSettingsAsync();
        Task StartForegroundServiceCompatAsync();
        Task StopForegroundServiceCompatAsync();

    }
}