﻿using Plugin.LightAudioPlayer.Base.Enums;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IAudioPlayerService
    {
        StateAudioPlayerId PlayState { get; set; }
        StateAudioPlayerId RunState { get; set; }
    }
}