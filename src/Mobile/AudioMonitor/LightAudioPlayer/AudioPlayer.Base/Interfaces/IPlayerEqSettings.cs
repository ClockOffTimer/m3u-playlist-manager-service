﻿namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IPlayerEqSettings
    {
        float[] BandRanges { get; set; }
        float[] BandLevels { get; set; }
        short Bands { get; set; }
        short Preset { get; set; }
        short PresetCurent { get; set; }
        bool IsEmpty { get; }

        string ToString();
    }
}