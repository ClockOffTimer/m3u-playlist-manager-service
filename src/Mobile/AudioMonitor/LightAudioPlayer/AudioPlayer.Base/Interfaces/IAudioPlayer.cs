﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IAudioPlayer : IEventAudioPlayer
    {
        public bool[] ButtonsState { get; }
        bool HasPlaying { get; }
        bool HasStoped { get; }
        bool HasPlayListPreload { get; }
        bool HasButtonPlay { get; }
        bool HasButtonPause { get; }
        bool HasButtonStop { get; }
        bool HasButtonPrev { get; }
        bool HasButtonNext { get; }
        bool IsAutoPlay { get; set; }
        bool IsAutoNext { get; set; }
        bool IsLooping { get; set; }
        int AudioSessionId { get; }
        int PlayListCount { get; }
        int Duration { get; }
        int Position { get; }

        Task<bool> Load(IList<PlayListItem> list);
        Task<bool> Play();
        Task<bool> Play(int idx);
        Task<bool> Pause();
        Task<bool> Stop();
        Task<bool> Prev();
        Task<bool> Next();
        PlayListItem CurrentPlay();
        PlayListItem SetEqPresetsReturn(int idx);
        void SetPlayIndex(int i);
        void SetEqPresets(int idx);
        void SetEqChannel(int idx, float val);
        void SetEqEnabled(bool b);
        bool GetEqEnabled();
        void GetEqSettings();
        void SetVolume(float vol);
        void SetVolume(float vol, float bal);
        void SetEfAutoGain(bool b);
        void SetEfLoudness(int val);
        void SetEfBasBoost(int val);
        void SetEfVirtualizer(int val);
        void SetEfVirtualizerMode(int val);
        void Dispose();
    }
}