﻿using System;

namespace Plugin.LightAudioPlayer.Base.Interfaces
{
    public interface IEventAudioPlayer
    {
        event EventHandler<IEventAudioPlayerArgs> StateEvent;
    }
}