﻿using System;
using Plugin.LightAudioPlayer.Base.Interfaces;

namespace Plugin.LightAudioPlayer.Base.Events
{
    public class EventAudioPlayerChange : EventArgs
    {
        public WeakReference<IAudioPlayer> WeakPlayer = default;
        public WeakReference<IAudioPlayerService> WeakService = default;
        public int EventCount = 0;
    }
}
