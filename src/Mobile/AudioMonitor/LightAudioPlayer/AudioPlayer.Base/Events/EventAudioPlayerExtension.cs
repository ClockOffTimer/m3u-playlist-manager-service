﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Interfaces;

namespace Plugin.LightAudioPlayer.Base.Events
{
    public static class EventAudioPlayerExtension
    {
        public static void ObservableEvent(this EventAudioPlayerChange ev, EventHandler<IEventAudioPlayerArgs> handler)
        {
            try {
                if ((ev == null) || (ev.WeakPlayer == null))
                    return;
                if (ev.WeakPlayer.TryGetTarget(out IAudioPlayer player))
                    player.StateEvent += handler;
            } catch { }
        }

        #region play events filters
        public static (string, int) PlayErrorFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) ||
                ((ev.EventId != EventAudioPlayerId.EvError) && (ev.EventId != EventAudioPlayerId.EvInfo)))
                return default;
            return (ev.Text, ev.Value);
        }
        public static (TimeSpan, int) PlayTimeFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) || (ev.StateId != StateAudioPlayerId.PlayingTime))
                return default;
            return (ev.PlayTime, ev.Value);
        }
        public static (string, int, Uri) PlayTrackFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) ||
                (ev.EventId != EventAudioPlayerId.EvPlayer) || (ev.StateId != StateAudioPlayerId.PlayingTrack))
                return default;
            return (ev.Text, ev.Value, ev.Url);
        }
        public static IPlayerEqSettings EqSettingsFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) || (ev.EventId != EventAudioPlayerId.EqSettings))
                return default;
            return ev.EqSettings;
        }
        #endregion

        #region status
        public static (StateAudioPlayerId, bool[]) PlayStatusFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) || (ev.EventId == EventAudioPlayerId.EvService))
                return (StateAudioPlayerId.None, default);

            switch (ev.StateId)
            {
                case StateAudioPlayerId.Prev:
                case StateAudioPlayerId.Next:
                case StateAudioPlayerId.Playing: return (StateAudioPlayerId.Playing, ev.HasButtons);
                case StateAudioPlayerId.Loaded:
                case StateAudioPlayerId.Loading:
                case StateAudioPlayerId.Stopped:
                case StateAudioPlayerId.Paused: return (ev.StateId, ev.HasButtons);
                case StateAudioPlayerId.Error: return ((ev.EventId == EventAudioPlayerId.EvError) ? ev.StateId : StateAudioPlayerId.None, ev.HasButtons);
                default: return (StateAudioPlayerId.None, ev.HasButtons);
            }
        }
        public static StateAudioPlayerId ServiceStatusFilterEvent(this IEventAudioPlayerArgs ev)
        {
            if ((ev == null) || (ev.EventId != EventAudioPlayerId.EvService))
                return StateAudioPlayerId.None;

            switch (ev.StateId)
            {
                case StateAudioPlayerId.Idle:
                case StateAudioPlayerId.Alive:
                case StateAudioPlayerId.WidgetUpdate:
                case StateAudioPlayerId.WidgetDeleted:
                case StateAudioPlayerId.WidgetDisabled:
                case StateAudioPlayerId.WidgetReloadList:
                case StateAudioPlayerId.LoadingService:
                case StateAudioPlayerId.RuningService:
                case StateAudioPlayerId.StartService:
                case StateAudioPlayerId.StopService: return ev.StateId;
                default: return StateAudioPlayerId.None;
            }
        }
        #endregion
    }
}
