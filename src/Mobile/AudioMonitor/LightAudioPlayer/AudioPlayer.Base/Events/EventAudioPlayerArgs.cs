﻿using System;
using System.Linq;
using System.Text;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Interfaces;

namespace Plugin.LightAudioPlayer.Base.Events
{
    public class EventAudioPlayerArgs : EventArgs, IEventAudioPlayerArgs
    {
        public static readonly string MessagingCenterId = "MessageAudioPlayer";

        public EventAudioPlayerId EventId { get; set; } = EventAudioPlayerId.None;
        public StateAudioPlayerId StateId { get; set; } = StateAudioPlayerId.None;
        public TimeSpan PlayTime { get; set; } = TimeSpan.Zero;
        public Uri Url { get; set; } = default;
        public IPlayerEqSettings EqSettings { get; set; } = default;
        public string Text { get; set; } = string.Empty;
        public int Value { get; set; } = -1;
        public bool IsValue { get; set; } = false;
        public bool[] HasButtons { get; set; } = default;
        public object Obj { get; set; } = default;
        public EqualizerParamId[] EqParamId { get; set; } = new EqualizerParamId[] {
            EqualizerParamId.None, EqualizerParamId.None
        };

        public EventAudioPlayerArgs() { }
        public EventAudioPlayerArgs(IEventAudioPlayerArgs args) => _ = Copy(args);

        public bool Copy(IEventAudioPlayerArgs args)
        {
            if (args == null)
                return false;

            EventId = args.EventId;
            StateId = args.StateId;
            PlayTime = args.PlayTime;
            Value = args.Value;
            Text = args.Text;
            IsValue = args.IsValue;

            if (args.Url != null)
                Url = new Uri(args.Url.IsAbsoluteUri ? args.Url.AbsoluteUri : args.Url.OriginalString);
            if (args.EqSettings != null)
                EqSettings = args.EqSettings;
            if (args.HasButtons != null) {
                HasButtons = new bool [args.HasButtons.Length];
                for (int i = 0; i < args.HasButtons.Length; i++)
                    HasButtons[i] = args.HasButtons[i];
            }
            if (args.EqParamId != null) {
                EqParamId[0] = (args.EqParamId.Length > 0) ? args.EqParamId[0] : EqParamId[0];
                EqParamId[1] = (args.EqParamId.Length > 1) ? args.EqParamId[1] : EqParamId[1];
            }
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{EventId}/{StateId} = ");
            sb.Append($"{IsValue}/{Value}, ");
            if (!string.IsNullOrWhiteSpace(Text))
                sb.Append($"'{Text}', ");
            if (StateId == StateAudioPlayerId.PlayingTime)
                sb.Append($"{PlayTime}, ");
            if ((EventId == EventAudioPlayerId.EqParameter) && (EqParamId != null))
                sb.Append($"{EqParamId[0]}|{EqParamId[1]}, ");
            if ((EventId == EventAudioPlayerId.EqSettings) && (EqSettings != null))
                sb.Append($"[{EqSettings}]");
            return sb.ToString();
        }
    }
}