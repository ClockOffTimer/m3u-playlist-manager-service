﻿using System;
using Plugin.LightAudioPlayer.Base.Enums;

namespace Plugin.LightAudioPlayer.Base.Events
{
    public class EventServiceCommandArgs : EventArgs
    {
        public StateAudioPlayerId Id = StateAudioPlayerId.None;
        public int Volume = -1;

        public EventServiceCommandArgs() { }
        public EventServiceCommandArgs(StateAudioPlayerId id) => Id = id;
    }
}
