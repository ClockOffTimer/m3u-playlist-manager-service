﻿using System;

namespace Plugin.LightAudioPlayer.Base.Utils
{
    public static class LogWriter
    {
        public static void Debug<T>(string s, Exception ex = null)
        {
#           if DEBUG
            if (ex == null)
                Console.WriteLine($"*** {typeof(T).Name} -> {s} ***");
            else
                Console.WriteLine($"*** {typeof(T).Name} -> {s} -> Exception: {ex} ***");
#           endif
        }
    }
}