﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using Plugin.LightAudioPlayer.Base.PlayList;

namespace Plugin.LightAudioPlayer.Base.Player
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioPlayerSettings
    {
        [XmlIgnore]
        public bool IsOnce { get; set; } = false;

        [XmlElement(nameof(LoopPlay))]
        private bool _loopPlay = false;
        public bool LoopPlay { get => _loopPlay; set { _loopPlay = value; IsOnce = true; } }

        [XmlElement(nameof(AutoPlay))]
        private bool _autoPlay = false;
        public bool AutoPlay { get => _autoPlay; set { _autoPlay = value; IsOnce = true; } }

        [XmlElement(nameof(EqEnable))]
        private bool _eqEnable = false;
        public bool EqEnable { get => _eqEnable; set { _eqEnable = value; IsOnce = true; } }

        [XmlElement(nameof(EfAutoGain))]
        private bool _efAutoGain = false;
        public bool EfAutoGain { get => _efAutoGain; set { _efAutoGain = value; IsOnce = true; } }

        [XmlElement(nameof(PlayIdx))]
        private int _playIdx = -1;
        public int PlayIdx { get => _playIdx; set { _playIdx = value; IsOnce = true; } }

        [XmlElement(nameof(EqPreset))]
        private int _eqPreset = -1;
        public int EqPreset { get => _eqPreset; set { _eqPreset = value; IsOnce = true; } }

        [XmlElement(nameof(EfLoudness))]
        private int _efLoudness = -1;
        public int EfLoudness { get => _efLoudness; set { _efLoudness = value; IsOnce = true; } }

        [XmlElement(nameof(EfBasBoost))]
        private int _efBasBoost = -1;
        public int EfBasBoost { get => _efBasBoost; set { _efBasBoost = value; IsOnce = true; } }

        [XmlElement(nameof(EfVirtualizer))]
        private int _efVirtualizer = -1;
        public int EfVirtualizer { get => _efVirtualizer; set { _efVirtualizer = value; IsOnce = true; } }

        [XmlElement(nameof(EfVirtualizerMode))]
        private int _efVirtualizerMode = -1;
        public int EfVirtualizerMode { get => _efVirtualizerMode; set { _efVirtualizerMode = value; IsOnce = true; } }

        [XmlElement(nameof(VolumeChannel))]
        private float[] _volumeChannel = default(float[]);
        public float[] VolumeChannel
        {
            get => _volumeChannel;
            set
            {
                if ((value == null) || (value.Length == 0))
                    return;

                _volumeChannel = new float[value.Length];
                Array.Copy(value, 0, _volumeChannel, 0, value.Length);
                IsOnce = true;
            }
        }

        [XmlElement(nameof(EqChannels))]
        private float[] _eqChannel = default(float[]);
        public float[] EqChannels
        {
            get => _eqChannel;
            set
            {
                if ((value == null) || (value.Length == 0))
                    return;

                _eqChannel = new float[value.Length];
                Array.Copy(value, 0, _eqChannel, 0, value.Length);
                IsOnce = true;
            }
        }

        [XmlElement(nameof(PlayList))]
        private List<PlayListItem> _playList = new List<PlayListItem>();
        public List<PlayListItem> PlayList
        {
            get => _playList;
            set
            {
                if ((value == null) || (value.Count == 0))
                    return;

                if (_playList.Count > 0)
                    _playList.Clear();
                _playList.AddRange(value);
                IsOnce = true;
            }
        }
    }
}
