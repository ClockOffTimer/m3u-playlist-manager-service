﻿using System;
using System.ComponentModel;
using System.Text;
using System.Xml.Serialization;

namespace Plugin.LightAudioPlayer.Base.PlayList
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class PlayListItem
    {
        [XmlElement("id")]
        public int Id { get; set; } = -1;
        
        [XmlElement("tag")]
        public string Tag { get; set; } = string.Empty;

        [XmlElement("desc")]
        public string Host { get; set; } = string.Empty;

        [XmlElement("mono")]
        public bool IsMono { get; set; } = false;

        [XmlElement("volume")]
        public float Volume { get; set; } = 0.5f;

        [XmlElement("balance")]
        public float Balance { get; set; } = 0.0f;

        [XmlElement("equalizer")]
        public float[] Equalizers { get; set; } = new float[10];

        [XmlElement("volumelocal")]
        public float VolumeLocal { get; set; } = 0.5f;

        [XmlElement("equalizerlocal")]
        public float[] EqualizersLocal { get; set; } = new float[10];

        [XmlElement("url")]
        public string Url { get; set; } = string.Empty;

        [XmlElement("seekable")]
        public bool IsSeekable { get; set; } = false;

#if STREAM_EXTENDED_IMPORT
        [XmlElement("type")]
        public AudioStreamType Atype { get; set; } = AudioStreamType.None;
#endif
        public void Copy(PlayListItem pli)
        {
            if ((pli == null) || (Id != pli.Id))
                return;

            Tag = pli.Tag;
            Host = pli.Host;
            Volume = pli.Volume;
            VolumeLocal = pli.VolumeLocal;
            Equalizers = (pli.Equalizers != null) ? pli.Equalizers : Equalizers;
            EqualizersLocal = (pli.EqualizersLocal != null) ? pli.EqualizersLocal : EqualizersLocal;
            Url = pli.Url;
            IsMono = pli.IsMono;
            IsSeekable = pli.IsSeekable;
        }
        public void CheckToDefault()
        {
            if (Equalizers == null)
                Equalizers = new float[10];
            else if (Equalizers.Length != 10)
            {
                float[] f = Equalizers;
                Equalizers = new float[10];
                Array.Copy(f, 0, Equalizers, 0, (Equalizers.Length > f.Length) ? f.Length : Equalizers.Length);
            }
            if (EqualizersLocal == null)
                EqualizersLocal = new float[10];
            else if (EqualizersLocal.Length != 10)
            {
                float[] f = EqualizersLocal;
                EqualizersLocal = new float[10];
                Array.Copy(f, 0, EqualizersLocal, 0, (EqualizersLocal.Length > f.Length) ? f.Length : EqualizersLocal.Length);
            }
            if (Volume == 0.0f)
                Volume = 0.5f;
            if (VolumeLocal == 0.0f)
                VolumeLocal = 0.5f;
            if ((Balance < -1.0f) || (Balance > 1.0f))
                Balance = 0.0f;
        }
        public override string ToString() => ToString(false);
        public string ToString(bool b)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(Tag))
                    sb.Append(Tag);
                if (!string.IsNullOrWhiteSpace(Host))
                    sb.Append($"|{Host}");
                if (!string.IsNullOrWhiteSpace(Url))
                    sb.AppendFormat("|{0}{1}", Url, b ? "\n" : ", ");
                else
                    sb.AppendFormat("{0}", b ? "\n" : ", ");

                AddText(sb, $"{Volume}/{VolumeLocal}/{Balance}", b);
                if (Equalizers != null)
                    AddText(sb, FloatToString(Equalizers), b);
                if (EqualizersLocal != null)
                    AddText(sb, FloatToString(EqualizersLocal), b);

                return sb.ToString();
            }
            catch { }
            return base.ToString();
        }
        private string FloatToString(float [] f) => string.Join("|", f);
        private void AddText(StringBuilder sb, string s, bool b)
        {
            if (b) sb.AppendLine($"\t\t{s}");
            else   sb.Append($"{s}, ");
        }
    }
}
