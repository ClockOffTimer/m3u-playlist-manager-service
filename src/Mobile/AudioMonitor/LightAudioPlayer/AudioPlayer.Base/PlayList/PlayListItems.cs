﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;

namespace Plugin.LightAudioPlayer.Base.PlayList
{
    public class PlayListItems
    {
        private IList<PlayListItem> _list = new List<PlayListItem>();
        private bool Check(int idx) => Enable && (idx >= 0) && (idx < _list.Count);
        private int _index { get; set; } = 0;
        public int  Index { get => _index; }
        public int  Count  { get => _list.Count; }
        public bool Enable { get => _list.Count > 0; }
        public bool HasSeek { get => Check(_index) ? _list[_index].IsSeekable : false; }

        public void Load(IEnumerable<PlayListItem> items)
        {
            if (Enable)
                _list.Clear();
            ((List<PlayListItem>)_list).AddRange(items);
        }
        public PlayListItem Current()
        {
            if (!Check(_index))
                return default;
            return _list[_index];
        }
        public string ById(int idx)
        {
            if (!Check(_index))
                return default;
            _index = idx;
            return _list[_index].Url;
        }
        public int Last()
        {
            if (!Check(_index))
                return -1;
            return _index;
        }
        public int Prev()
        {
            if (!Enable)
                return -1;
            _index = (_index == 0) ? (_list.Count - 1) : (_index - 1);
            return _index;
        }
        public int Next()
        {
            if (!Enable)
                return -1;
            _index = (_index >= (_list.Count - 1)) ? 0 : (_index + 1);
            return _index;
        }
        public int Random()
        {
            if (!Enable)
                return -1;
            System.Random rand = new System.Random(_list.Count);
            return rand.Next(_list.Count);
        }
        public void Set(int i)
        {
            if ((i >= 0) && (i < _list.Count))
                _index = i;
        }
    }
}
