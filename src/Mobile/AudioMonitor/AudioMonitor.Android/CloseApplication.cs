﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using Android.Runtime;
using AudioMonitor.Data.Interfaces;
using AudioMonitor.Droid;
using Xamarin.Essentials;
using Xamarin.Forms;

[assembly: Dependency(typeof(CloseApplication))]
namespace AudioMonitor.Droid
{
    [Preserve(AllMembers = true)]
    public class CloseApplication : ICloseApplication
    {
        public void CloseApp()
        {
            var activity = Platform.CurrentActivity;
            activity?.FinishAffinity();
            Application.Current.Quit();
        }
    }
}