﻿using System;
using Android.Runtime;
using Android.Widget;
using AudioMonitor;
using AudioMonitor.Droid.Utils;
using AudioMonitor.Utils;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastNotify))]
namespace AudioMonitor.Droid.Utils
{
    [Preserve(AllMembers = true)]
    public class ToastNotify : IToastNotify
    {
        public void Show(Exception e)
        {
            if (e == null)
                return;
            Show_(e.Message);
        }
        public void Show(string s)
        {
            if (s == null)
                return;
            Show_(s);
        }
        private void Show_(string s) =>
            Toast.MakeText(Android.App.Application.Context, s, ToastLength.Long).Show();
    }
}