﻿/* Copyright (c) 2021-2022 AudioMonitor for PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Plugin.LightAudioPlayer.Base.Enums;
using Plugin.LightAudioPlayer.Base.Interfaces;
using Plugin.LightAudioPlayer.Base.Utils;
using Plugin.LightAudioPlayer.Droid.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AudioMonitor.Droid
{
    [IntentFilter(
            new[] { Platform.Intent.ActionAppAction },
            Categories = new[] { Intent.CategoryDefault })]
    [Activity(
        Label = "AudioMonitor",
        Icon = "@mipmap/icon",
        Theme = "@style/MainTheme",
        // LaunchMode = LaunchMode.SingleTop,
        MainLauncher = true,
        ShowForAllUsers = true,
        ShowWhenLocked = true,
        Exported = true,
        Name = "com.m3uservice.audiomonitor.droid.mainactivity",
        ConfigurationChanges = ConfigChanges.ScreenSize |
                               ConfigChanges.Orientation |
                               ConfigChanges.UiMode |
                               ConfigChanges.ScreenLayout |
                               ConfigChanges.SmallestScreenSize)]
    [IntentFilter(new[] { Platform.Intent.ActionAppAction }, Categories = new[] { Intent.CategoryDefault })]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            AndroidEnvironment.UnhandledExceptionRaiser += (s, e) =>
            {
                LogWriter.Debug<MainActivity>(nameof(App), e.Exception);
#if DEBUG
                e.Handled = false;
#else
                e.Handled = true;
#endif
            };
            Platform.Init(this, savedInstanceState);
            Forms.Init(this, savedInstanceState);
            DependencyService.RegisterSingleton<ICrossAudioPlayer>(
                Plugin.LightAudioPlayer.Droid.AudioPlayerAndroid.Get());
            DependencyService.Get<ICrossAudioPlayer>().SetActivityType(typeof(MainActivity));
            LoadApplication(new App());
            VersionTracking.Track();
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        protected override void OnResume()
        {
            base.OnResume();
            Platform.OnResume(this);
        }
        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            Platform.OnNewIntent(intent);
        }
        protected override void OnStart()
        {
            base.OnStart();
            StartForegroundService(
                new Intent(
                    base.ApplicationContext,
                    typeof(AudioPlayerService))
                    .SetAction(StateAudioPlayerId.StartService.ToString())
                    .AddFlags(AudioPlayerService.LaunchServiceFlags));
        }
    }
}