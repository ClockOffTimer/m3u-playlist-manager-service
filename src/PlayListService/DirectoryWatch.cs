﻿/* Copyright (c) 2021-2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Threading;
using M3U;
using PlayListService.Properties;
using PlayListService.Task;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Http;
using PlayListServiceData.RunManager;
using PlayListServiceData.UPNP;
using Tasks = System.Threading.Tasks;

namespace PlayListService
{
    public class DirectoryWatch : IDisposable
    {
        public enum ProcessId : int
        {
            SYSTEM = -1,
            NONE = 0,
            ID_SHED_MANAGER = 10,
            ID_CHECK_NETWORK = 20,
            ID_CONFIG_RELOAD = 130,
            ID_SCAN = 131,
            ID_SCAN_LOCAL_VIDEO = 132,
            ID_SCAN_LOCAL_AUDIO = 133,
            ID_CHECK_MEDIA = 134,
            ID_RENEW_EPG = 135,
            ID_STAGE_MOVE = 136,
            ID_STAGE_DOWNLOAD = 137,
            ID_STAGE_VERSION = 138,
            ID_SAVE_HISTORY = 139,
            ID_SCAN_TRANSLATE_AUDIO = 140
        }
        private readonly DLNA dlna;
        private readonly ConcurrentQueue<ProcessId> queue;
        private FileSystemWatcher fileWatcherM3u;
        private FileSystemWatcher fileWatcherVideo;
        private FileSystemWatcher fileWatcherAudio;
        private readonly FileSystemEventHandler eventHandlerM3u;
        private readonly FileSystemEventHandler eventHandlerVideo;
        private readonly FileSystemEventHandler eventHandlerAudio;
        private readonly ProcessId[] actions = new ProcessId[]
                        {
                            ProcessId.ID_SCAN,
                            ProcessId.ID_SCAN_LOCAL_VIDEO,
                            ProcessId.ID_SCAN_LOCAL_AUDIO,
                            ProcessId.ID_SCAN_TRANSLATE_AUDIO,
                            ProcessId.ID_CHECK_MEDIA,
                            ProcessId.ID_STAGE_MOVE,
                            ProcessId.ID_RENEW_EPG,
                            ProcessId.ID_STAGE_DOWNLOAD
                        };
        private readonly string[] sdup = new string[] { default, default, default };
        private long crun_ = 0L;
        private ProcessId ScanAfter = ProcessId.NONE;
        private ProcessId CurrentRun
        {
            get => (ProcessId)Interlocked.Read(ref crun_);
            set => _ = Interlocked.Exchange(ref crun_, (long)value);
        }

        public DirectoryWatch()
        {
            SheduleManager shedule = ServiceStatic.GetSheduleManager();
            if (shedule == default)
                throw new NullReferenceException("Shedule manager is null");

            queue = new ConcurrentQueue<ProcessId>();
            HttpClientDefault.HTTP.RequestTimeout = Settings.Default.ConfigCpuPriority switch
            {
                0 => Settings.Default.ConfigTimeOutRequest,
                1 => Settings.Default.ConfigTimeOutRequest,
                3 => (int)(Settings.Default.ConfigTimeOutRequest * 1.2),
                4 => (int)(Settings.Default.ConfigTimeOutRequest * 1.5),
                _ => (int)(Settings.Default.ConfigTimeOutRequest * 1.8)
            };
            ///
            fileWatcherM3u = new FileSystemWatcher
            {
                Path = PlayListServiceUtils.GetM3uRootDirectory(),
                IncludeSubdirectories = true,
                NotifyFilter =
                    NotifyFilters.FileName |
                    NotifyFilters.Size |
                    NotifyFilters.LastWrite,
                Filter = "*.*"
            };
            eventHandlerM3u = new FileSystemEventHandler(WatcherM3uOnChanged);
            ///
            fileWatcherVideo = new FileSystemWatcher
            {
                Path = PlayListServiceUtils.GetVideoRootDirectory(),
                IncludeSubdirectories = true,
                NotifyFilter =
                    NotifyFilters.FileName |
                    NotifyFilters.Size,
                Filter = "*.*"
            };
            eventHandlerVideo = new FileSystemEventHandler(WatcherVideoOnChanged);
            ///
            fileWatcherAudio = new FileSystemWatcher
            {
                Path = PlayListServiceUtils.GetAudioRootDirectory(),
                IncludeSubdirectories = true,
                NotifyFilter =
                    NotifyFilters.FileName |
                    NotifyFilters.Size,
                Filter = "*.*"
            };
            eventHandlerAudio = new FileSystemEventHandler(WatcherAudioOnChanged);
            ///
            shedule.ManageQueueCb = () =>
            {
                try
                {
                    if (queue.Count == 0)
                        return;
                    QueueCheck();
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(DirectoryWatch)} -> Manage Queue callback -> {e}"); }
            };

            int minutes = DateTime.Today.Minute + 1;

            /// RUN_HOLD
            #region RUN_HOLD
            shedule.Add(
                ProcessId.ID_SCAN,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_SCAN))
                        return;
                    var cls = new ServiceTaskScan(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            shedule.Add(
                ProcessId.ID_SCAN_LOCAL_VIDEO,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_SCAN_LOCAL_VIDEO))
                        return;
                    var cls = new ServiceTaskScanLocalVideo(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            shedule.Add(
                ProcessId.ID_SCAN_LOCAL_AUDIO,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_SCAN_LOCAL_AUDIO))
                        return;
                    var cls = new ServiceTaskScanLocalAudio(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            shedule.Add(
                ProcessId.ID_STAGE_MOVE,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_STAGE_MOVE))
                        return;
                    var cls = new ServiceTaskMove(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            shedule.Add(
                ProcessId.ID_SAVE_HISTORY,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_SAVE_HISTORY))
                        return;
                    var cls = new ServiceTaskHistorySave(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            #endregion

            /// RUN_DAYOFWEEK
            #region RUN_DAYOFWEEK
            if (Settings.Default.IsConfigEpg)
            {
                shedule.Add(
                    ProcessId.ID_RENEW_EPG,
                    minutes++,
                    DayOfWeek.Sunday,
                    (s, a) => {
                        if (!TryTasksIsRun(ProcessId.ID_RENEW_EPG))
                            return;
                        if (PlayListServiceUtils.CheckNetworkConnectionState())
                        {
                            var cls = new ServiceTaskEpgDownloader(a);
                            cls.EventCb += WatcherRunStateEvent;
                            cls.Start();
                        }
                    });
            }
            #endregion

            /// INT_DAYS
            #region INT_DAYS
            shedule.Add(
                ProcessId.ID_STAGE_VERSION,
                SheduleManager.ActionType.INT_DAYS,
                TimeSpan.FromDays(1.0),
                minutes++,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_STAGE_VERSION))
                        return;
                    if (PlayListServiceUtils.CheckNetworkConnectionState())
                    {
                        var cls = new ServiceTaskVersionDownloader(a);
                        cls.EventCb += WatcherRunStateEvent;
                        cls.Start();
                    }
                });
            shedule.Add(
                ProcessId.ID_SCAN_TRANSLATE_AUDIO,
                SheduleManager.ActionType.INT_DAYS,
                TimeSpan.FromDays(1.0),
                minutes++,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_SCAN_TRANSLATE_AUDIO))
                        return;
                    var cls = new ServiceTaskScanTranslateAudio(a);
                    cls.EventCb += WatcherRunStateEvent;
                    cls.Start();
                });
            shedule.Add(
                ProcessId.ID_STAGE_DOWNLOAD,
                SheduleManager.ActionType.INT_DAYS,
                TimeSpan.FromDays(3.0),
                minutes++,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_STAGE_DOWNLOAD))
                        return;
                    if (PlayListServiceUtils.CheckNetworkConnectionState())
                    {
                        var cls = new ServiceTaskJsDownloader(a);
                        cls.EventCb += WatcherRunStateEvent;
                        cls.Start();
                    }
                });
            shedule.Add(
                ProcessId.ID_CHECK_MEDIA,
                SheduleManager.ActionType.INT_DAYS,
                TimeSpan.FromDays((double)Settings.Default.ConfigRunCheckPeriodDays),
                minutes++,
                (s, a) => {
                    if (!TryTasksIsRun(ProcessId.ID_CHECK_MEDIA))
                        return;
                    if (PlayListServiceUtils.CheckNetworkConnectionState())
                    {
                        var cls = new ServiceTaskCheck(a);
                        cls.EventCb += WatcherRunStateEvent;
                        cls.Start();
                    }
                });
            #endregion

            /// INT_SECONDS
            #region INT_SECONDS
            if (!PlayListServiceUtils.CheckNetworkConnectionState())
            {
                shedule.Add(
                    ProcessId.ID_CHECK_NETWORK,
                    SheduleManager.ActionType.INT_SECONDS,
                    TimeSpan.FromSeconds(45.0),
                    minutes++,
                    (s, a) => {
                        if (PlayListServiceUtils.CheckNetworkConnectionState())
                        {
                            if (!TryTasksIsRun(ProcessId.ID_CHECK_MEDIA))
                                if (shedule.RunTask(ProcessId.ID_CHECK_MEDIA))
                                    try { s.Dispose(); } catch { }
                        }
                    });
            }
            #endregion

            ///
            if (Settings.Default.IsConfigDlnaEnable)
            {
                dlna = ServiceStatic.GetDlna();
                dlna.EventError.EvCb += (b, a) =>
                {
                    if (a.Item2 is DLNAException ex)
                        ServiceStatic.GetLog().Error.Write($"DLNA -> {ex.ShortMessage}");
                    else
                        ServiceStatic.GetLog().Error.Write($"DLNA -> {a.Item2.Message}");
                };
                dlna.EventTrack.EvCb += (b, a) =>
                {
                    if (a.Item2 == default)
                        ServiceStatic.GetLog().Info.Write($"DLNA CMD:{a.Item1} -> {a.Item2}");
                    else
                        ServiceStatic.GetLog().Info.Write($"DLNA CMD:{a.Item1}");
                };
            }
            QueueAdd(ProcessId.ID_STAGE_VERSION);
            QueueAdd(ProcessId.ID_STAGE_DOWNLOAD);
            QueueAdd(ProcessId.ID_SCAN_TRANSLATE_AUDIO);
            QueueAdd(ProcessId.ID_RENEW_EPG);
#if DEBUG
            Tasks.Task.Yield();
            shedule.PrintList();
            Tasks.Task.Yield();

#if DEBUG_TEST
            Run(ProcessId.ID_CHECK_MEDIA); /* Test only */
#endif
#endif
        }
        ~DirectoryWatch()
        {
            Dispose();
        }

        #region Add Job
        public void Add(ProcessId id)
        {
            if ((int)id >= (int)ProcessId.ID_CONFIG_RELOAD)
                QueueAdd(id);
        }
        #endregion

        #region Custom Command
        public int CustomCommand
        {
            set
            {
                ProcessId pid;
                try { pid = (ProcessId)value; } catch { return; }

                switch (pid)
                {
                    case ProcessId.ID_CONFIG_RELOAD:
                        {
                            try
                            {
                                Settings.Default.Reload();
                                Stop();
                                Tasks.Task.Delay(1000);
                                Tasks.Task.Yield();
                                Tasks.Task.Delay(1000);
                                Start();
                            }
                            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> Reload config -> {e}"); }
                            return;
                        }
                    case ProcessId.ID_RENEW_EPG:
                        {
                            try
                            {
                                FileInfo f = new(
                                    Settings.Default.ConfigM3uPath.GetEpgSavePath(Settings.Default.ConfigEpgUrl)
                                    );
                                if ((f != default) && f.Exists)
                                    f.Delete();
                            }
                            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> Epg delete -> {e}"); }
                            break;
                        }
                    case ProcessId.ID_SCAN:
                    case ProcessId.ID_SCAN_LOCAL_VIDEO:
                    case ProcessId.ID_SCAN_LOCAL_AUDIO:
                    case ProcessId.ID_CHECK_MEDIA:
                    case ProcessId.ID_STAGE_MOVE:
                    case ProcessId.ID_STAGE_DOWNLOAD: break;
                    default: return;
                }
                if (!CheckIsRuns())
                    Run(pid);
                else
                    queue.Enqueue(pid);
            }
        }
        #endregion

        #region Start
        public void Start()
        {
            if (fileWatcherM3u != default)
            {
                fileWatcherM3u.EnableRaisingEvents = true;
                fileWatcherM3u.Changed += eventHandlerM3u;
                fileWatcherM3u.Created += eventHandlerM3u;
            }
            if (fileWatcherVideo != default)
            {
                fileWatcherVideo.EnableRaisingEvents = true;
                fileWatcherVideo.Changed += eventHandlerVideo;
                fileWatcherVideo.Created += eventHandlerVideo;
            }
            if (fileWatcherAudio != default)
            {
                fileWatcherAudio.EnableRaisingEvents = true;
                fileWatcherAudio.Changed += eventHandlerAudio;
                fileWatcherAudio.Created += eventHandlerAudio;
            }
            SheduleManager shedule = ServiceStatic.GetSheduleManager();
            if (shedule != default)
                shedule.Resume();
        }
        #endregion

        #region Stop
        public void Stop()
        {
            if (fileWatcherM3u != default)
            {
                fileWatcherM3u.EnableRaisingEvents = false;
                fileWatcherM3u.Changed -= eventHandlerM3u;
                fileWatcherM3u.Created -= eventHandlerM3u;
            }
            if (fileWatcherVideo != default)
            {
                fileWatcherVideo.EnableRaisingEvents = false;
                fileWatcherVideo.Changed -= eventHandlerVideo;
                fileWatcherVideo.Created -= eventHandlerVideo;
            }
            if (fileWatcherAudio != default)
            {
                fileWatcherAudio.EnableRaisingEvents = false;
                fileWatcherAudio.Changed -= eventHandlerAudio;
                fileWatcherAudio.Created -= eventHandlerAudio;
            }
            SheduleManager shedule = ServiceStatic.GetSheduleManager();
            if (shedule != default)
                shedule.Pause();
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            Stop();

            if ((queue != default) && !queue.IsEmpty)
                while (queue.Count > 0)
                    queue.TryDequeue(out ProcessId _);

            SheduleManager shedule = ServiceStatic.GetSheduleManager();
            if (shedule != default)
                shedule.Clear();

            FileSystemWatcher fw;

            fw = fileWatcherM3u;
            fileWatcherM3u = default;
            if (fw != null)
            {
                try { fw.Dispose(); } catch { }
            }
            fw = fileWatcherVideo;
            fileWatcherVideo = default;
            if (fw != null)
            {
                try { fw.Dispose(); } catch { }
            }
            fw = fileWatcherAudio;
            fileWatcherAudio = default;
            if (fw != null)
            {
                try { fw.Dispose(); } catch { }
            }
        }
        #endregion

        #region event callback RunState
        private void WatcherRunStateEvent(ProcessId id, bool b)
        {
            {
                string s = b ? "Started" : "Ended";
                ServiceStatic.GetLog().Verbose.Write($"Watch -> Event -> {id} = {s}");
            }

#           if !DEBUG_TEST
            switch (id)
            {
                case ProcessId.ID_SCAN:
                    {
                        if (!b)
                        {
                            //TaskTimerOff(ProcessId.ID_SCAN);
                            QueueAdd(ProcessId.ID_SCAN_LOCAL_VIDEO);
                            sdup[0] = default;
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                        {
                            CurrentRun = ProcessId.ID_SCAN;
                            ScanAfter = ProcessId.ID_SCAN;
                        }
                        break;
                    }
                case ProcessId.ID_SCAN_LOCAL_VIDEO:
                    {
                        if (!b)
                        {
                            //TaskTimerOff(ProcessId.ID_SCAN_LOCAL_VIDEO);
                            QueueAdd(ProcessId.ID_SCAN_LOCAL_AUDIO);
                            sdup[1] = default;
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_SCAN_LOCAL_VIDEO;
                        break;
                    }
                case ProcessId.ID_SCAN_LOCAL_AUDIO:
                    {
                        if (!b)
                        {
                            //TaskTimerOff(ProcessId.ID_SCAN_LOCAL_AUDIO);
                            QueueAdd(ProcessId.ID_SCAN_TRANSLATE_AUDIO);
                            sdup[2] = default;
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_SCAN_LOCAL_AUDIO;
                        break;
                    }
                case ProcessId.ID_SCAN_TRANSLATE_AUDIO:
                    {
                        if (!b)
                        {
                            //TaskTimerChange(ProcessId.ID_SCAN_TRANSLATE_AUDIO, TimeSpan.FromDays(1));
                            if (ScanAfter == ProcessId.ID_SCAN)
                                QueueAdd(ProcessId.ID_CHECK_MEDIA);
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_SCAN_TRANSLATE_AUDIO;
                        break;
                    }
                case ProcessId.ID_CHECK_MEDIA:
                    {
                        if (!b)
                        {
                            //TaskTimerChange(ProcessId.ID_CHECK_MEDIA, TimeSpan.FromDays(Settings.Default.ConfigRunCheckPeriodDays));
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                        {
                            CurrentRun = ProcessId.ID_CHECK_MEDIA;
                            ScanAfter = ProcessId.NONE;
                        }
                        break;
                    }
                case ProcessId.ID_RENEW_EPG:
                    {
                        if (!b)
                        {
                            int d = 6 - ((int)DateTime.Now.DayOfWeek);
                            //TaskTimerChange(ProcessId.ID_RENEW_EPG, TimeSpan.FromDays(d + 1));
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_RENEW_EPG;
                        break;
                    }
                case ProcessId.ID_STAGE_DOWNLOAD:
                    {
                        if (!b)
                        {
                            //TaskTimerChange(ProcessId.ID_STAGE_DOWNLOAD, TimeSpan.FromDays(Settings.Default.ConfigRunDownloadPeriodDays));
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_STAGE_DOWNLOAD;
                        break;
                    }
                case ProcessId.ID_STAGE_VERSION:
                    {
                        if (!b)
                        {
                            //TaskTimerChange(ProcessId.ID_STAGE_VERSION, TimeSpan.FromDays(1));
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_STAGE_VERSION;
                        break;
                    }
                case ProcessId.ID_STAGE_MOVE:
                    {
                        if (!b)
                        {
                            //TaskTimerOff(ProcessId.ID_STAGE_MOVE);
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_STAGE_MOVE;
                        break;
                    }
                case ProcessId.ID_SAVE_HISTORY:
                    {
                        if (!b)
                        {
                            //TaskTimerOff(ProcessId.ID_SAVE_HISTORY);
                            CurrentRun = ProcessId.NONE;
                        }
                        else
                            CurrentRun = ProcessId.ID_SAVE_HISTORY;
                        break;
                    }
            }
#           endif
        }
        #endregion

        #region event callback M3u OnChanged
        private void WatcherM3uOnChanged(object sender, FileSystemEventArgs a)
        {
            try {
                /*
                 * ServiceStatic.GetLog().Write($"Watch -> M3u OnChanged check request -> {a.FullPath} : {a.ChangeType}");
                 */
                if (!a.Name.IsM3uXFileName() ||
                     a.FullPath.IsSystemDirectory() ||
                     a.FullPath.IsM3uRootDirectory() ||
                     a.FullPath.CheckExcludeDir(Settings.Default.ConfigM3uDirExclude) ||
                    Path.GetFileNameWithoutExtension(a.FullPath).IsM3uAutoGenerateFile())
                    return;

                if ((sdup[0] != default) && sdup[0].Equals(a.FullPath))
                    return;
                sdup[0] = a.FullPath;

                if (CheckIsRuns()) {
                    TaskTimerOff(ProcessId.ID_SCAN);
                    if (!CheckIsRunId(ProcessId.ID_SCAN)) {
                        QueueAdd(ProcessId.ID_SCAN);
                        ServiceStatic.GetLog().Write($"Watch -> M3u OnChanged -> Queue Add -> {a.FullPath}");
                    }
                    return;
                }

                ServiceStatic.GetLog().Verbose.Write($"Watch -> M3u OnChanged -> run SCAN -> {a.FullPath} : {a.ChangeType}");
                Run(ProcessId.ID_SCAN);
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Verbose.Write($"Watch -> M3u OnChanged -> {a.FullPath} : {a.ChangeType}", e);
            }
        }
        #endregion

        #region event callback Video OnChanged
        private void WatcherVideoOnChanged(object sender, FileSystemEventArgs a)
        {
            try
            {
                do {
                    /*
                     * ServiceStatic.GetLog().Write($"Watch -> Video OnChanged check request -> {a.FullPath}");
                     */
                    if (a.FullPath.IsSystemDirectory())
                        return;
                    if ((sdup[1] != default) && sdup[1].Equals(a.FullPath))
                        return;
                    sdup[1] = a.FullPath;
                    string ext = Path.GetExtension(a.FullPath);
                    if (string.IsNullOrWhiteSpace(ext))
                        break;

                    if (!ext.CheckFileExtension(BaseConstant.extVideoUri) ||
                         a.FullPath.CheckExcludeDir(Settings.Default.ConfigVideoDirExclude))
                        break;

                    try {
                        if (CheckIsRuns()) {
                            TaskTimerOff(ProcessId.ID_SCAN_LOCAL_VIDEO);
                            if (!CheckIsRunId(ProcessId.ID_SCAN_LOCAL_VIDEO)) {
                                QueueAdd(ProcessId.ID_SCAN_LOCAL_VIDEO);
                                ServiceStatic.GetLog().Write($"Watch -> Video OnChanged -> Queue Add -> {a.FullPath}");
                            }
                            break;
                        }
                    }
                    catch (Exception e) {
                        ServiceStatic.GetLog().Verbose.Write($"Watch -> Video OnChanged -> check is run -> {a.FullPath}", e);
                    }

                    ServiceStatic.GetLog().Write($"Watch -> Video OnChanged -> run SCAN LOCAL VIDEO -> {a.FullPath}");
                    Run(ProcessId.ID_SCAN_LOCAL_VIDEO);

                } while (false);
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> Watcher Video OnChanged ->", e); }
        }
        #endregion

        #region event callback Audio OnChanged
        private void WatcherAudioOnChanged(object sender, FileSystemEventArgs a)
        {
            try
            {
                do {
                    /*
                     * ServiceStatic.GetLog().Write($"Watch -> Audio OnChanged check request -> {a.FullPath}");
                     */
                    if (a.FullPath.IsSystemDirectory())
                        return;
                    if ((sdup[2] != default) && sdup[2].Equals(a.FullPath))
                        return;
                    sdup[2] = a.FullPath;
                    string ext = Path.GetExtension(a.FullPath);
                    if (string.IsNullOrWhiteSpace(ext))
                        break;

                    if (!ext.CheckFileExtension(BaseConstant.extAudioUri) ||
                         a.FullPath.CheckExcludeDir(Settings.Default.ConfigAudioDirExclude))
                        break;

                    try {
                        if (CheckIsRuns()) {
                            TaskTimerOff(ProcessId.ID_SCAN_LOCAL_AUDIO);
                            if (!CheckIsRunId(ProcessId.ID_SCAN_LOCAL_AUDIO)) {
                                QueueAdd(ProcessId.ID_SCAN_LOCAL_AUDIO);
                                ServiceStatic.GetLog().Write($"Watch -> Audio OnChanged -> Queue Add -> {a.FullPath}");
                            }
                            break;
                        }
                    }
                    catch (Exception e) {
                        ServiceStatic.GetLog().Verbose.Write($"Watch -> Audio OnChanged -> check is run -> {a.FullPath}", e);
                    }

                    ServiceStatic.GetLog().Write($"Watch -> Audio OnChanged -> run SCAN LOCAL AUDIO -> {a.FullPath}");
                    Run(ProcessId.ID_SCAN_LOCAL_AUDIO);

                } while (false);
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> Watcher Audio OnChanged ->", e); }
        }
        #endregion

        #region private Runner
        private async void Run(ProcessId start, ProcessId end = ProcessId.NONE)
        {
#           if DEBUG
            ServiceStatic.GetLog().Verbose.Write($"Watch -> Run -> PID START: {start}, PID END: {end}, COUNT: {queue.Count}");
#           endif
            await Tasks.Task.Run(() =>
            {
                try
                {
                    SheduleManager shedule = ServiceStatic.GetSheduleManager();
                    if (shedule == default)
                        return;
                    SheduleItem task = shedule.GetById(start);
                    if (task == default)
                        return;

                    if (end != ProcessId.NONE)
                    {
                        task = shedule.GetById(end);
                        if (task != default)
                            while (task.IsRuning)
                            {
                                Tasks.Task.Delay(750);
                                Tasks.Task.Yield();
                            }
                    }
                    if (!CheckIsRuns())
                        _ = shedule.RunTask(start);
                    else
                        QueueAdd(start);
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> wait: {end} start: {start} -> {e}"); }
            });
        }
        private bool CheckIsRuns()
        {
            return CheckIsRunsSkip(ProcessId.NONE) != ProcessId.NONE;
        }
        private ProcessId CheckIsRunsSkip(ProcessId skipid)
        {
            try
            {
                SheduleManager shedule = ServiceStatic.GetSheduleManager();
                if (shedule == default)
                    return ProcessId.NONE;

                for (int i = 0; i < actions.Length; i++)
                {
                    if (actions[i] == skipid)
                        continue;
                    SheduleItem task = shedule.GetById(actions[i]);
                    if ((task != default) && task.IsRuning)
                        return actions[i];
                }
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> check is Run -> {e}"); }
            return ProcessId.NONE;
        }
        private bool CheckIsRunId(ProcessId id)
        {
            try
            {
                SheduleManager shedule = ServiceStatic.GetSheduleManager();
                if (shedule == default)
                    return false;

                SheduleItem task = shedule.GetById(id);
                if ((task != default) && task.IsRuning)
                    return true;
                else
                    return false;
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> check is Run Id -> {e}"); }
            return false;
        }
        private bool TryTasksIsRun(ProcessId pid)
        {
            try
            {
                if (CurrentRun == pid)
                    return false;

                if (CheckIsRunsSkip(pid) == ProcessId.NONE)
                    return true;

                QueueAdd(pid);
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Watch -> try task Run by Id -> {e}"); }
            return false;
        }
        private void TaskTimerOff(ProcessId id)
        {
            TaskTimerChange(id, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        private void TaskTimerChange(ProcessId id, TimeSpan period, TimeSpan? due = null)
        {
            SheduleManager shedule = ServiceStatic.GetSheduleManager();
            if (shedule == default)
                return;

            SheduleItem item = shedule.GetById(id);
            if (item != default)
            {
                TimeSpan span = (due == null) ? item.DueTime : (TimeSpan)due;
                item.ChangeTimer(period, span);
            }
        }
        private void QueueCheck()
        {
            while (queue.Count > 0)
            {
                if (!CheckIsRuns())
                {
                    if (queue.TryDequeue(out ProcessId pid))
                    {
#if DEBUG
                        ServiceStatic.GetLog().Verbose.Write($"Watch -> Check Run -> PID: {pid}, COUNT: {queue.Count}");
#endif
                        Run(pid);
                        return;
                    }
                }
                else
                    return;
            }
        }
        private void QueueAdd(ProcessId id)
        {
            if (!queue.Contains(id))
            {
#if DEBUG
                ServiceStatic.GetLog().Verbose.Write($"Watch -> Queue Add -> PID: {id}, COUNT: {queue.Count}");
#endif
                queue.Enqueue(id);
            }
        }
        #endregion
    }
}
