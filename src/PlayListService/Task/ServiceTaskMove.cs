﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Threading;
using PlayListService.Properties;
using PlayListServiceData.Process;

namespace PlayListService.Task
{
    public class ServiceTaskMove : ServiceTaskBase, IServiceTask
    {
        public ServiceTaskMove(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ServiceStatic.GetLog();
            CurrentStatId = ProcessStat.Id.Move;
        }

        public void Start()
        {
            try
            {
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_MOVE, true);
                LogWriter.Info.Write("Once dot move: started!");

                var dAuto = PlayListServiceUtils.CreateM3uDirectory("auto");
                if (dAuto == null)
                    return;
                var dBack = PlayListServiceUtils.CreateM3uDirectory("backup");
                if (dBack == null)
                    return;

                string prefix = DateTime.Now.ToString("yyyy-MM-dd-HH-mm");

                foreach (var f in Directory.EnumerateFiles(PlayListServiceUtils.GetConfigM3uPath(), "*.m3*", SearchOption.AllDirectories))
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    try
                    {
                        if (Path.GetDirectoryName(f).IsSystemDirectory())
                            continue;
                        FileInfo file = new(f.Normalize(NormalizationForm.FormC));
                        if (!file.Exists || file.IsReadOnly)
                            continue;

                        var ppath = Path.GetDirectoryName(file.FullName);
                        var pfile = Path.GetFileName(file.FullName);

                        if (pfile.StartsWith(Settings.Default.ConfigVideoPrefix) ||
                            pfile.StartsWith(Settings.Default.ConfigAudioPrefix) ||
                            ppath.EndsWith("m3u") ||
                            ppath.EndsWith("backup"))
                            continue;

                        file.MoveTo(Path.Combine(dBack.FullName, prefix, pfile));
                        CheckStat.SetHostTotal(1);
                    }
                    catch (Exception e) { ErrorToLog($"Once dot move (move) ->", e); }
                }
                foreach (var f in Directory.EnumerateFiles(PlayListServiceUtils.GetConfigM3uPath(), "*.m3u", SearchOption.TopDirectoryOnly))
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    try
                    {
                        FileInfo file = new FileInfo(f.Normalize(NormalizationForm.FormC));
                        if (!file.Exists || file.IsReadOnly)
                            continue;

                        var pfile = Path.GetFileName(file.FullName);
                        if (pfile.StartsWith(Settings.Default.ConfigVideoPrefix) ||
                            pfile.StartsWith(Settings.Default.ConfigAudioPrefix))
                            continue;

                        file.CopyTo(Path.Combine(dAuto.FullName, pfile));
                        CheckStat.SetHostGood();
                    }
                    catch (Exception e) { ErrorToLog($"Once dot move (copy) ->", e); }
                }
            }
            catch (OperationCanceledException e) { ErrorToLog($"Once dot move -> All operation canceled ->", e); }
            catch (Exception e) { ErrorToLog($"Once dot move ->", e, true, true); }
            finally
            {
                StatExport();
                LogWriter.Info.Write(CheckStat.ToString());
                LogWriter.Info.Write("Once dot move: ended!");
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_MOVE, false);
            }
        }
    }
}
