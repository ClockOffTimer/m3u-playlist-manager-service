﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;

namespace PlayListService.Task
{
    public class ServiceTaskHistorySave : ServiceTaskBase, IServiceTask
    {
        static readonly string Tag = "History Save";

        public ServiceTaskHistorySave(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ServiceStatic.GetLog();
        }

        public void Start()
        {
            try
            {
                CallEvent(DirectoryWatch.ProcessId.ID_SAVE_HISTORY, true);
                ServiceStatic.GetHistory().Save();
            }
            catch (Exception e) { ErrorToLog($"{Tag} error -> {e}", e); }
            finally
            {
                CallEvent(DirectoryWatch.ProcessId.ID_SAVE_HISTORY, false);
            }
        }

    }
}
