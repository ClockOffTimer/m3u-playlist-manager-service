﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using M3U.Model;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceScript;

namespace PlayListService.Task
{
    public class ServiceTaskJsDownloader : ServiceTaskBase, IServiceTask
    {
        private ScriptsManager<Media> scriptsManager = default;

        public ServiceTaskJsDownloader(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ServiceStatic.GetLog();
        }

        public void Start()
        {
            if ((scriptsManager != default) || tokenCancel.IsCancellationRequested)
                return;
            Start_();
        }

        private void Start_()
        {
            try
            {
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_DOWNLOAD, true);
                scriptsManager = new ScriptsManager<Media>(
                    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                    new ScriptRunnerDownload<Media>()
                    {
                        OptExtendedAdd = (a) =>
                        {
                            a.AddHostObject("xSettings", Microsoft.ClearScript.HostItemFlags.GlobalMembers, new JsProxySettings());
                            a.AddHostObject("xConsole", Microsoft.ClearScript.HostItemFlags.GlobalMembers, new ScriptExportConsole(LogWriter));
                        }
                    },
                    LogWriter,
                    false)
                {
                    IsConfigJsHost = Settings.Default.IsConfigJsHost,
                    IsConfigJsLib = Settings.Default.IsConfigJsLib,
                    IsConfigJsXMLHttpRequest = Settings.Default.IsConfigJsXMLHttpRequest
                };

                tokenCancel.ThrowIfCancellationRequested();

                #region SCRIPT RUN
                if (scriptsManager.IsEnable(CALLSTATE.BEGIN_DOWNLOAD))
                    scriptsManager.Run(CALLSTATE.BEGIN_DOWNLOAD, out List<Media> m);
                #endregion
            }
            catch (Exception e) { ErrorToLog("Watch -> JS Download ->", e); }
            finally
            {
                scriptsManager.Clear();
                scriptsManager = default;
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_DOWNLOAD, false);
            }
        }

    }
}
