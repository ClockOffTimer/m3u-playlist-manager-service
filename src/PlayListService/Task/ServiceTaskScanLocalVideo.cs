﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using M3U;
using M3U.Model;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Report;
using PlayListServiceData.Tags;
using PlayListServiceScript;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Task
{
    public class ServiceTaskScanLocalVideo : ServiceTaskBase, IServiceTask
    {
        private readonly FilesStreamSelector baseStreams;
        private readonly List<Tasks.Task> tasks = new();
        public const string Tag = "Scan local video";

        public ServiceTaskScanLocalVideo(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ProcessLog.Create(
                new FileLog(BasePath.GetScanLocalVideoLogPath(), Tag),
                Settings.Default.IsConfigReport ? new Reporter() : default);
            baseStreams = new FilesStreamSelector(
                "video",
                PlayListServiceUtils.GetConfigM3uPath(),
                base.LogWriter,
                false)
            {
                    MapFilesPath = PlayListServiceUtils.GetConfigVideoPath()
            };
            CurrentStatId = ProcessStat.Id.ScanLocalVideo;
        }
        ~ServiceTaskScanLocalVideo()
        {
            ILogManager pl = LogWriter;
            if (pl != default)
            {
                try { pl.Close(); } catch { }
            }
        }

        public void Startkk()
        {
            Waiter(150000);
            ScanLocalProcess();
        }
        public void Start()
        {
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN_LOCAL_VIDEO, true);
            Waiter(150000);

            var t = new Thread(() => ScanLocalProcess())
            {
                Priority = Settings.Default.ConfigCpuPriority switch
                {
                    0 => ThreadPriority.Lowest,
                    1 => ThreadPriority.BelowNormal,
                    3 => ThreadPriority.AboveNormal,
                    4 => ThreadPriority.Highest,
                    _ => ThreadPriority.Normal
                }
            };
            t.Start();
            t.Join(TimeSpan.FromHours(1));
            Waiter(150000);
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN_LOCAL_VIDEO, false);
            LogWriter.Close();
            LogWriter = default;
            t = default;
            GC.Collect();
        }

        private void ScanLocalProcess()
        {
            try
            {
                $"{Tag} -> Started!".WriteLog('i', LogWriter);

                foreach (var f in Directory.EnumerateFiles(
                    Settings.Default.ConfigM3uPath,
                    Settings.Default.ConfigVideoPrefix + "*.m3u",
                    SearchOption.TopDirectoryOnly))
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    if (Path.GetDirectoryName(f).IsSystemDirectory())
                        continue;
                    FileInfo file = new(f.Normalize(NormalizationForm.FormC));
                    if (file.Exists && (!file.IsReadOnly))
                        file.Delete();
                }
                ScanLocalDirectory();
            }
            catch (OperationCanceledException e) { ErrorToLog($"{Tag} -> All operation canceled ->", e); }
            catch (Exception e) { ErrorToLog($"{Tag} -> Task ->", e); }
            finally
            {
                baseStreams.Close();
                StatExport();
                LogWriter.Info.Write(CheckStat.ToString());
                LogWriter.Info.Write($"{Tag} -> Ended!");
            }
        }

        private void ScanLocalDirectory()
        {
            {
                string path = PlayListServiceUtils.GetConfigVideoPath();
                if (string.IsNullOrWhiteSpace(path))
                    return;
                $"{Tag} -> Begin add local directories: {path}".WriteLog('v', LogWriter);
            }
            ScriptsManager<Media> scriptsManager = default;
            try
            {
                scriptsManager = GetScriptsManager<Media>();
                ForeachPathEntrys(
                    baseStreams,
                    (a, b) =>
                    {
                        tasks.Add(ParseLocalFile(scriptsManager, a, b));
                    },
                    Settings.Default.ConfigVideoPrefix,
                    PlayListServiceUtils.GetConfigVideoPath(), default);

                Waiter(150000);
                Tasks.Task task = Tasks.Task.WhenAll(tasks);
                try { task.Wait(); } catch { }
                tasks.Clear();
            }
            catch (UnauthorizedAccessException ea) { ErrorToLog($"{Tag} -> Administrative role needed ->", ea); }
            catch (Exception e) { ErrorToLog($"{Tag} -> foreach files ->", e, true); }
            finally
            {
                scriptsManager?.Clear();
                $"{Tag} -> All local directories added to tasks!".WriteLog('i', LogWriter);
            }
        }

        private async Tasks.Task ParseLocalFile(ScriptsManager<Media> scriptsManager, FileInfo file, Tuple<string, FileStream, StreamWriter, object, bool> tr)
        {
            await Tasks.Task.Run(() =>
            {
                try
                {
                    Media media = default;
                    FileInfo newfile = default;
                    bool isrename = Settings.Default.IsConfigRenameLocalFiles;

                    if (string.IsNullOrWhiteSpace(file.Extension))
                    {
                        CheckStat.SetHostReject();
                        return;
                    }
                    try
                    {
                        if (!file.Extension.CheckFileExtension(BaseConstant.extVideoUri) ||
                             file.FullName.CheckExcludeDir(Settings.Default.ConfigVideoDirExclude))
                            return;

                        CheckStat.SetHostTotal(1);
                        string newpath = isrename ?
                            BasePathRename.TryFileRename(file) : file.FullName;

                        if (string.IsNullOrWhiteSpace(newpath))
                        {
                            CheckStat.SetHostError();
                            newpath = file.FullName;
                            isrename = false;
                        }

                        newfile = new FileInfo(file.FullName);
                        if (newfile == default)
                        {
                            CheckStat.SetHostError();
                            return;
                        }
                        if (isrename)
                        {
                            newfile.MoveTo(newpath);
                            baseStreams.RenameFiles(file, newfile);
                        }
                    }
                    catch (Exception e) { ErrorToLog($"{Tag} -> Rename media file -> {file.FullName}", e); return; }

                    try
                    {
                        media = new Media(
                            baseStreams.GetM3uUrl(newfile.FullName),
                            baseStreams.GetM3uName(newfile.Name),
                            file.Length)
                        {
                            MediaFileExt = newfile.Extension
                        };

                        FileTags tags = default;

                        try
                        {
                            tags = new FileTags(newfile, media);
                            tags?.ImagesFromTags();
                            if (Settings.Default.IsConfigAddTagsToLocalFiles)
                                tags?.WriteTags(media);
                        }
                        catch (Exception e) { ErrorToLog($"{Tag} -> File Tags init -> {newfile.FullName}", e); }
                        finally
                        {
                            if (tags != default)
                                tags.Dispose();
                        }

                        string poster = baseStreams.GetPoster(newfile);
                        if ((poster != default) && string.IsNullOrWhiteSpace(media.Attributes.TvgLogo))
                            media.Attributes.SetTvgLogo(poster);
                    }
                    catch (Exception e) { ErrorToLog($"{Tag} -> Import media file -> {file.FullName}", e); }

                    if (media == null)
                        return;

                    #region SCRIPT BEGIN_LOCALSCAN
                    if (scriptsManager.IsEnable(CALLSTATE.BEGIN_LOCALSCAN))
                    {
                        switch (scriptsManager.Check(CALLSTATE.BEGIN_LOCALSCAN, media, out Media mo))
                        {
                            case CONTINUESTATE.CONTINUE_ABORT: CheckStat.SetHostReject(); return;
                            case CONTINUESTATE.CONTINUE_CHANGE:
                                {
                                    if (mo != default)
                                        media = mo;
                                    break;
                                }
                        }
                    }
                    #endregion

                    lock (tr.Item4)
                    {
                        M3UWriter.WriteItem(tr.Item3, media, default);
                    }
                    CheckStat.SetHostGood();
                }
                catch (Exception e) { ErrorToLog($"{Tag} -> Import media file -> {file.FullName}", e); }
            });
        }
    }
}
