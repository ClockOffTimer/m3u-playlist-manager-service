﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Threading;
using M3U;
using M3U.Model;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Report;
using PlayListServiceData.Utils;
using PlayListServiceStream;
using PlayListServiceStream.Data;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Task
{
    public class ServiceTaskScanTranslateAudio : ServiceTaskBase, IServiceTask
    {
        public const string Tag = "Scan translate audio";
        private string GetMediaUrl(int i) => $"{PlayListServiceUtils.localMediaBaseUrl("stream/audio")}{i}";

        public ServiceTaskScanTranslateAudio(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ProcessLog.Create(
                new FileLog(BasePath.GetScanTranslateAudioLogPath(), Tag),
                Settings.Default.IsConfigReport ? new Reporter() : default);
            CurrentStatId = ProcessStat.Id.ScanTranslateAudio;
        }

        public void Start()
        {
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN_TRANSLATE_AUDIO, true);
            Waiter(150000);

            var t = new Thread(() => ScanTranslateProcess())
            {
                Priority = Settings.Default.ConfigCpuPriority switch
                {
                    0 => ThreadPriority.Lowest,
                    1 => ThreadPriority.BelowNormal,
                    3 => ThreadPriority.AboveNormal,
                    4 => ThreadPriority.Highest,
                    _ => ThreadPriority.Normal
                }
            };
            t.Start();
            t.Join(TimeSpan.FromMinutes(5));
            Waiter(150000);
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN_TRANSLATE_AUDIO, false);
            LogWriter.Close();
            LogWriter = default;
            t = default;
            GC.Collect();
        }
        private void ScanTranslateProcess()
        {
            try
            {
                $"{Tag} -> Started!".WriteLog('i', LogWriter);

                string name = string.IsNullOrWhiteSpace(Settings.Default.ConfigAudioTranslateM3u) ?
                                BasePath.NameAudioBroadcastM3u : Settings.Default.ConfigAudioTranslateM3u;

                FileInfo file = new(
                    Path.Combine(Settings.Default.ConfigM3uPath, $"{name}.m3u"));

                if (file.Exists && file.IsReadOnly)
                    throw new Exception($"file is read only! -> {file.FullName}");
                if (file.Exists)
                    file.Delete();

                AudioStreamManagerService asm = ServiceStatic.GetAudioCapture();
                if (asm == default)
                    throw new Exception("manager audio capture devices return null, abort..");
                if (!asm.IsLoading)
                {
                    CancellationTokenSafe wtoken = default;
                    try
                    {
                        wtoken = new(TimeSpan.FromSeconds(5));
                        while (!asm.IsLoading)
                        {
                            if (wtoken.Token.IsCancellationRequested)
                                break;
                            Tasks.Task.Delay(500, wtoken.Token);
                        }
                    }
                    catch { }
                    finally
                    {
                        if (wtoken != default)
                            wtoken.Dispose();
                    }
                }
                if ((!asm.IsLoading) || (asm.ReaderCount == 0))
                    throw new Exception("audio reader list is empty, abort..");

                AudioSettingsLite aset = asm.GetChannelsSettings();
                if (aset.Count() == 0)
                    throw new Exception("active audio channel list is empty, abort..");

                CheckStat.SetHostTotal(aset.AudioChannels.Count);
                string group = Path.GetFileNameWithoutExtension(name),
                       logo = PlayListServiceUtils.localMediaBaseUrl("images/audiobroadcast/150", false);

                using FileStream fs = File.Open(file.FullName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                using StreamWriter sw = new(fs, new UTF8Encoding(false))
                {
                    AutoFlush = true,
                    NewLine = "\n"
                };

                M3U.M3UWriter.WriteHeader(sw,
                    PlayListServiceUtils.localMediaBaseUrl($"epg/{Path.GetFileName(Settings.Default.ConfigEpgUrl)}"));

                for (int i = 0; i < aset.AudioChannels.Count; i++)
                {
                    Media media = new(
                        GetMediaUrl(i), aset.AudioChannels[i].Tag, group, logo);
                    M3UWriter.WriteItem(sw, media, default);
                    CheckStat.SetHostGood();
                }
            }
            catch (OperationCanceledException e) { ErrorToLog($"{Tag} -> All operation canceled ->", e); }
            catch (Exception e) { ErrorToLog($"{Tag} -> Task ->", e); }
            finally
            {
                StatExport();
                LogWriter.Info.Write(CheckStat.ToString());
                $"{Tag} -> Ended!".WriteLog('i', LogWriter);
            }
        }

    }
}
