﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using PlayListServiceData.Base;

namespace PlayListService.Task
{
    public interface IServiceTask
    {
        event BaseEvent<DirectoryWatch.ProcessId, bool>.DelegateEvent EventCb;
        void Start();
    }
}