﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.IO.Compression;
using System.Text;
using System.Threading;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Utils;

namespace PlayListService.Task
{
    public class ServiceTaskEpgDownloader : ServiceTaskBase, IServiceTask
    {
        static readonly string Tag = "Epg Downloader";
        private readonly string Url_;
        private readonly string PathFile_;

        public ServiceTaskEpgDownloader(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ServiceStatic.GetLog();
            Url_ = string.IsNullOrWhiteSpace(Settings.Default.ConfigEpgUrl) ?
                default : Settings.Default.ConfigEpgUrl;
            PathFile_ = string.IsNullOrWhiteSpace(Settings.Default.ConfigM3uPath) ?
                default : Settings.Default.ConfigM3uPath.GetEpgSavePath(Url_);
        }

        public void Start()
        {
            try
            {
                CallEvent(DirectoryWatch.ProcessId.ID_RENEW_EPG, true);
                if (string.IsNullOrWhiteSpace(Url_) || string.IsNullOrWhiteSpace(PathFile_))
                    return;

                $"{Tag} begin check -> {Url_} -> {PathFile_}".WriteLog('i', LogWriter);
                FileInfo fgz = new(
                    PathFile_.Normalize(NormalizationForm.FormC));
                if (fgz == default)
                {
                    $"{Tag} abort -> gzip file instance error..".WriteLog('!', LogWriter);
                    return;
                }

                FileInfo fxml = new(Path.Combine(
                    Path.GetDirectoryName(fgz.FullName),
                    Path.GetFileNameWithoutExtension(Settings.Default.ConfigEpgUrl)));
                if (fxml == default)
                {
                    $"{Tag} abort -> epg.xml file instance error..".WriteLog('!', LogWriter);
                    return;
                }

                if (fgz.Exists)
                {
                    int days = (int)Math.Round((DateTime.Now - fgz.CreationTime).TotalDays, 1);
                    if ((days < 7) && ((days == 0) || (DateTime.Now.DayOfWeek != DayOfWeek.Sunday)))
                    {
                        $"{Tag} abort -> not download, data file not expired! left {days} days.".WriteLog('v', LogWriter);
                        if (!fxml.Exists)
                        {
                            $"{Tag} -> extract 'epg.xml' from old archive.".WriteLog('v', LogWriter);
                            ExtractFile(fgz, fxml);
                        }
                        return;
                    }
                }
                if (!CheckFileIO(fgz) || !CheckFileIO(fxml))
                    return;

                if (tokenCancel.IsCancellationRequested)
                    return;

                $"{Tag} start -> {Url_} -> {fgz.FullName} -> {fxml.FullName}".WriteLog('i', LogWriter);
                _ = new Uri(Url_).DownloadFileAsync(fgz).ContinueWith((t) =>
                {
                    try
                    {
                        do
                        {
                            if (t == default)
                            {
                                NullToLog(Tag, "Task");
                                break;
                            }
                            if (t.IsFaulted || t.IsCanceled)
                            {
                                if (t.Exception != default)
                                    t.Exception.WriteLog('!', $"{Tag} Task abort ->", LogWriter);
                                break;
                            }
                            else if (!t.IsCompleted)
                                t.Wait(tokenCancel);

                            if (!t.IsCompleted)
                                break;

                            fgz.Refresh();
                            if (!fgz.Exists)
                            {
                                $"{Tag} unzip abort -> {fgz.FullName} not found!".WriteLog('!', LogWriter);
                                break;
                            }

                            if (!CheckFileIO(fxml))
                                break;

                            if (tokenCancel.IsCancellationRequested)
                                break;

                            ExtractFile(fgz, fxml);

                        } while (false);
                    }
                    catch (Exception e) { e.WriteLog('!', "Task DownloadFileAsync", LogWriter); }
                });
            }
            catch (Exception e) { e.WriteLog('!', $"{Tag} error -> {Url_} -> {e.Message}", LogWriter); }
            finally
            {
                CallEvent(DirectoryWatch.ProcessId.ID_RENEW_EPG, false);
            }
        }

        private void ExtractFile(FileInfo fgz, FileInfo fxml)
        {
            try
            {
                $"{Tag} -> Unzip EPG file -> {fxml.FullName}, from {fgz.FullName}".WriteLog('i', LogWriter);
                using (FileStream ifs = new(fgz.FullName, FileMode.Open, FileAccess.Read))
                {
                    if (ifs == default) { NullToLog(Tag, nameof(ifs)); return; }
                    using Stream arch = new GZipStream(ifs, CompressionMode.Decompress);
                    if (arch == default) { NullToLog(Tag, nameof(arch)); return; }
                    using FileStream ofs = new(fxml.FullName, FileMode.Create, FileAccess.Write, FileShare.Read);
                    if (ofs == default) { NullToLog(Tag, nameof(ofs)); return; }
                    arch.CopyTo(ofs);
                }
                fxml.Refresh();
                $"{Tag} -> Unziped EPG file -> {fxml.FullName}, size {fxml.Length.Humanize()}".WriteLog('i', LogWriter);
            }
            catch (Exception e) { e.WriteLog('!', "Unzip file error", LogWriter); }
        }

        private bool CheckFileIO(FileInfo f)
        {
            if (f == default)
            {
                NullToLog(Tag, "file");
                return false;
            }
            if (f.Exists)
            {
                if (f.IsReadOnly)
                {
                    $"{Tag} abort -> {f.FullName} is readonly!".WriteLog('!', LogWriter);
                    return false;
                }
                f.Delete();
            }
            return true;
        }
    }
}
