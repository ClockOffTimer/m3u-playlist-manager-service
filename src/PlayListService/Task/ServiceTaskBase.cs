﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using M3U.Model;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Log;
using PlayListServiceScript;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Task
{
    public abstract class ServiceTaskBase : BaseEvent<DirectoryWatch.ProcessId, bool>
    {
        protected CancellationToken tokenCancel = default;
        protected ILogManager LogWriter = default;
        protected readonly ProcessStatCheck CheckStat = new();
        protected ProcessStat.Id CurrentStatId = ProcessStat.Id.None;

        ~ServiceTaskBase()
        {
            StatExport();
        }

        protected void StatExport()
        {
            if (CurrentStatId == ProcessStat.Id.None)
                return;
            ServiceStatic.SetStat(CurrentStatId, CheckStat);
        }

        protected ScriptsManager<T> GetScriptsManager<T>()
        {
            ScriptRunnerMedia<T> runner = new()
            {
                OptExtendedAdd = (a) =>
                {
                    a.AddHostObject("xSettings", Microsoft.ClearScript.HostItemFlags.GlobalMembers, new JsProxySettings());
                    a.AddHostObject("xConsole",  Microsoft.ClearScript.HostItemFlags.GlobalMembers, new ScriptExportConsole(LogWriter));
                }
            };
            ScriptsManager<T> scriptsManager =
                new(
                    Path.GetDirectoryName(Assembly.GetEntryAssembly().Location),
                    runner, LogWriter) {
                IsConfigJsHost = Settings.Default.IsConfigJsHost,
                IsConfigJsLib = Settings.Default.IsConfigJsLib,
                IsConfigJsXMLHttpRequest = Settings.Default.IsConfigJsXMLHttpRequest
            };
            return scriptsManager;
        }

        protected bool CheckExtm3u(Extm3u extm3u, string path)
        {
            do
            {
                if ((extm3u == default) || (extm3u.Medias == default))
                {
                    ErrorToLog($"!> Ext.m3u list is null, from {path}");
                    break;
                }
                if (extm3u.Medias.Count() == 0)
                {
                    ErrorToLog($"!> Ext.m3u list is empty, from {path}");
                    break;
                }
                if (extm3u?.Warnings.Count() > 0)
                    foreach (string w in extm3u.Warnings)
                        LogWriter.Warning.Write($"!> M3U parse Warning -> {w}, file: {path}");

                return true;

            } while (false);
            return false;
        }

        protected void Waiter(int cnt)
        {
            while (cnt-- > 0)
            {
                Tasks.Task.Delay(150);
                Tasks.Task.Yield();
            }
        }

        protected void ForeachPathEntrys(
            FilesStreamSelector bstreams,
            Action<FileInfo, Tuple<string, FileStream, StreamWriter, object, bool>> act,
            string prefix, string root, string ngroup = default)
        {
            var  dirlist = Directory.EnumerateDirectories(root, "*", SearchOption.TopDirectoryOnly).ToList();
            bool IsFirst = (string.IsNullOrWhiteSpace(ngroup));
            foreach (var d in dirlist)
            {
                if (tokenCancel.IsCancellationRequested)
                    return;
                if (d.IsSystemDirectory())
                    continue;

                DirectoryInfo dir = new(d.Normalize(NormalizationForm.FormC));
                if ((dir == null) || !dir.Exists)
                    continue;

                if (IsFirst)
                    ngroup = bstreams.GetM3uName(dir.FullName);
                Tuple<string, FileStream, StreamWriter, object, bool> tr = bstreams.GetStream(ngroup, prefix);

                foreach (var f in Directory.EnumerateFiles(dir.FullName, "*.*", SearchOption.TopDirectoryOnly))
                {
                    if (tokenCancel.IsCancellationRequested)
                        return;
                    FileInfo file = new(f.Normalize(NormalizationForm.FormC));
                    if ((file != null) && file.Exists && (!file.IsReadOnly))
                        act.Invoke(file, tr);
                }
                ForeachPathEntrys(bstreams, act, prefix, d, ngroup);
            }
        }

        protected void TaskErrorToLog(Tasks.Task t, string s = default)
        {
            if (t.Exception == null)
                return;
            StringBuilder sb = new();
            Exception e = t.Exception.InnerException;
            sb.AppendFormat("\r\n\t[{0}]", t.Exception.Message);
            while (e != null)
            {
                if (!string.IsNullOrWhiteSpace(e.Message))
                    sb.AppendFormat("\r\n\t[{0}]", e.Message);
                e = e.InnerException;
            }
            if (s == default)
                LogWriter.Warning.Write($"!> {t.Status} -> error = {sb}");
            else
                LogWriter.Warning.Write($"!> {t.Status} -> error = {sb} \r\n\t\t: source = {s}");
        }

        protected void ErrorToLog(string s, Exception e = default, bool r = false, bool t = false)
        {
            CheckStat.SetHostError();
            bool b1 = e != default,
                 b2 = s != default;

            if (b1 && b2 && r && t)
                e.WriteLog('!', s, LogWriter).WriteStackTrace().Report();
            else if (b1 && r && t)
                e.WriteLog('!', LogWriter).WriteStackTrace().Report();
            else if (b2 && r && t)
                s.WriteLog('!', LogWriter).WriteStackTrace().Report();
            else if (b1 && b2 && r)
                e.WriteLog('!', s, LogWriter).Report();
            else if (b1 && r)
                e.WriteLog('!', LogWriter).Report();
            else if (b2 && r)
                s.WriteLog('!', LogWriter).Report();
            else if (b1 && b2 && t)
                e.WriteLog('!', s, LogWriter).WriteStackTrace();
            else if (b1 && t)
                e.WriteLog('!', LogWriter).WriteStackTrace();
            else if (b2 && t)
                s.WriteLog('!', LogWriter).WriteStackTrace();
            else if (b1 && b2)
                e.WriteLog('!', s, LogWriter);
            else if (b1)
                e.WriteLog('!', LogWriter);
            else if (b2)
                s.WriteLog('!', LogWriter);
        }

        protected void NullToLog(string tag, string s)
        {
            $"{tag} abort -> instance '{s}' is null".WriteLog('!', LogWriter);
        }
    }
}
