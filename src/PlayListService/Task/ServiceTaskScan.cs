﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using M3U;
using M3U.Model;
using M3U.Parser;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Container;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Report;
using PlayListServiceScript;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Task
{
    public class ServiceTaskScan : ServiceTaskBase, IServiceTask
    {
        private readonly FilesStreamSelector baseStreams;
        private readonly List<Tuple<List<Media>, string, bool>> m3uNewList =
            new();
        private readonly List<Tasks.Task> tasks = new();

        public ServiceTaskScan(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ProcessLog.Create(
                new FileLog(BasePath.GetScanLogPath(), "Scan"),
                Settings.Default.IsConfigReport ? new Reporter() : default);
            baseStreams = new FilesStreamSelector(
                "m3u",
                PlayListServiceUtils.GetM3uRootDirectory(),
                LogWriter,
                Settings.Default.IsConfigEpg)
            {
                MapFilesPath = PlayListServiceUtils.GetConfigM3uPath()
            };
            CurrentStatId = ProcessStat.Id.Scan;
        }
        ~ServiceTaskScan()
        {
            ILogManager pl = LogWriter;
            if (pl != default)
            {
                try { pl.Close(); } catch { }
            }
        }

        public void Start()
        {
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN, true);
            Waiter(150000);

            var t = new Thread(() => ScanStageProcess())
            {
                Priority = Settings.Default.ConfigCpuPriority switch
                {
                    0 => ThreadPriority.Lowest,
                    1 => ThreadPriority.BelowNormal,
                    3 => ThreadPriority.AboveNormal,
                    4 => ThreadPriority.Highest,
                    _ => ThreadPriority.Normal
                }
            };
            t.Start();
            t.Join(TimeSpan.FromHours(5));
            Waiter(150000);
            CallEvent(DirectoryWatch.ProcessId.ID_SCAN, false);
            ServiceStatic.ClearXmlBase();
            LogWriter.Close();
            LogWriter = default;
            t = default;
            GC.Collect();
        }


        #region Scan Stage run async
        private void ScanStageProcess()
        {
            LogWriter.Info.Write($"New media: {PlayListServiceUtils.GetConfigM3uPath()} - Scan started!");

            try
            {
                ServiceStatic.InitXmlBase();
                Tuple<int, int, int> t = ServiceStatic.CountXmlBase();
                $"Xml base count: {t.Item1}/{t.Item2}/{t.Item3}".WriteLog('v', LogWriter);
            }
            catch (Exception e) { e.WriteLog('v', $"Scan -> Init Xml base ->", LogWriter).WriteStackTrace(e); }

            try
            {
                foreach (var f in Directory.EnumerateFiles(PlayListServiceUtils.GetConfigM3uPath(), "*.m3u", SearchOption.TopDirectoryOnly))
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    FileInfo file = new(f.Normalize(NormalizationForm.FormC));
                    if (file.Exists && (!file.IsReadOnly))
                        file.Delete();
                }

                List<Tuple<List<Media>, string, bool>> m3uList = new();
                foreach (var d in Directory.GetDirectories(PlayListServiceUtils.GetConfigM3uPath()))
                {
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    if (d.IsSystemDirectory() || d.EndsWith("backup"))
                        continue;

                    foreach (var f in Directory.EnumerateFiles(d, "*.*", SearchOption.AllDirectories)
                                               .Where(s => M3U.M3UWriter.IsM3uXFileName(s))
                                               .Select(s => s.Normalize(NormalizationForm.FormC)))
                    {
                        tokenCancel.ThrowIfCancellationRequested();
                        if (!f.IsM3uRootDirectory() &&
                            !f.CheckExcludeDir(Settings.Default.ConfigM3uDirExclude))
                        {
                            Extm3u extm3u = M3U.M3UP.ParseFromFile(f);
                            if (!CheckExtm3u(extm3u, f))
                                continue;

                            CheckStat.SetHostTotal(extm3u.Medias.Count());
                            var list = extm3u.Medias.Distinct(new M3U.MediaComparer()).ToList();
                            bool b = (!string.IsNullOrEmpty(extm3u.PlayListType)) &&
                                                            extm3u.PlayListType.Equals("playlist", StringComparison.OrdinalIgnoreCase);
                            if (extm3u.Medias.Count() > list.Count)
                                CheckStat.SetHostDuplicate(extm3u.Medias.Count() - list.Count);
                            m3uList.Add(new Tuple<List<Media>, string, bool>(list, f, b));
                        }
                    }
                }

                for (int i = m3uList.Count - 1; i >= 0; i--)
                {
                    var t1 = m3uList[i];
                    for (int k = t1.Item1.Count - 1; k >= 0; k--)
                    {
                        if (string.IsNullOrWhiteSpace(t1.Item1[k].MediaFile))
                        {
                            t1.Item1.RemoveAt(k);
                            CheckStat.SetHostBad();
                            continue;
                        }
                        for (int m = i - 1; m >= 0; m--)
                        {
                            var t2 = m3uList[m];
                            for (int n = t2.Item1.Count - 1; n >= 0; n--)
                            {
                                if (string.IsNullOrWhiteSpace(t2.Item1[n].MediaFile))
                                {
                                    CheckStat.SetHostBad();
                                    t2.Item1.RemoveAt(n);
                                }
                                else if (t1.Item1[k].Equals(t2.Item1[n]))
                                {
                                    CheckStat.SetHostDuplicate();
                                    t2.Item1.RemoveAt(n);
                                }

                            }
                        }
                    }
                }
                for (int i = m3uList.Count - 1; i >= 0; i--)
                {
                    if (m3uList[i].Item1.Count == 0)
                    {
                        string path = m3uList[i].Item2;
                        if (!string.IsNullOrWhiteSpace(path))
                        {
                            FileInfo file = new(path);
                            if ((file != default) && file.Exists && !file.IsReadOnly)
                                file.Delete();
                        }
                        m3uList.RemoveAt(i);
                        CheckStat.SetHostBad();
                        continue;
                    }
                    tasks.Add(ParseFileAsync(m3uList[i].Item1, m3uList[i].Item2, m3uList[i].Item3));
                }
                m3uList.Clear();
                m3uList = default;

                Waiter(150000);
                Tasks.Task task = Tasks.Task.WhenAll(tasks);
                try { task.Wait(); } catch { }
                tasks.Clear();

                if (m3uNewList.Count > 0)
                {
                    foreach (var t in m3uNewList)
                    {
                        tokenCancel.ThrowIfCancellationRequested();
                        tasks.Add(ParseFileAsync(t.Item1, t.Item2, t.Item3));
                    }
                    m3uNewList.Clear();
                    task = Tasks.Task.WhenAll(tasks);
                    try { task.Wait(); } catch { }
                }
            }
            catch (Exception e) { ErrorToLog($"Scan -> Task ->", e, true); }
            finally
            {
                baseStreams.Close();
                StatExport();
                LogWriter.Info.Write(CheckStat.ToString());
                LogWriter.Info.Write("Scan media: Ended!");
            }
        }
        #endregion

        #region Parse
        private async Tasks.Task ParseFileAsync(List<Media> medias, string path, bool isplaylist = false)
        {
            await Tasks.Task.Run(() =>
            {
                ScriptsManager<Media> scriptsManager = default;
                try
                {
                    scriptsManager = GetScriptsManager<Media>();
                    ParseFile(medias, path, isplaylist, scriptsManager);
                }
                catch (Exception e) { ErrorToLog($"Scan -> Import file -> {path}", e); }
                finally
                {
                    ScriptsManager<Media> sm = scriptsManager;
                    scriptsManager = default;
                    if (sm != default)
                        sm.Clear();
                }
            });
        }
        private void ParseFile(List<Media> medias, string path, bool isplaylist, ScriptsManager<Media> scriptsManager)
        {
            $"Parser: start file - '{path}'".WriteLog('i', LogWriter);

            if (medias == null)
            {
                $"List M3U is null, from '{path}'".WriteLog('!', LogWriter);
                return;
            }
            if (medias.Count() == 0)
            {
                $"List M3U is empty, from '{path}'".WriteLog('!', LogWriter);
                return;
            }
            try
            {
                foreach (var m in medias)
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    if (string.IsNullOrEmpty(m.MediaFile))
                    {
                        CheckStat.SetHostBad();
                        continue;
                    }
                    try
                    {
                        Media media = m;
                        string group = default;
                        try
                        {
                            string [] arr = new string[]
                            {
                                media.Attributes.GroupTitle,
                                media.Title.InnerTitle,
                                media.Title.RawTitle,
                                media.MediaFile
                            };
                            for (int i = 0; i < arr.Length; i++)
                            {
                                if (string.IsNullOrWhiteSpace(arr[i]))
                                    continue;
                                group = GetGroupAlias(baseStreams.GroupNameNormalize(arr[i]), i);
                                break;
                            }
                        } catch (Exception e) { e.WriteLog('!', $"Scan -> Parse file -> Group select ->", LogWriter); }

                        if (string.IsNullOrWhiteSpace(group))
                            group = FilesStreamSelector.groupDefault;

                        if (Settings.Default.IsConfigSeparateUdpSource && M3U.M3UWriter.IsUdpUrl(media.MediaFile))
                        {
                            if (!group.StartsWith(Settings.Default.ConfigUdpPrefix))
                                group = $"{Settings.Default.ConfigUdpPrefix}{group}";
                        }
                        media.Attributes.SetGroupTitle(group);

                        #region JS BEGIN_SCAN
                        if (scriptsManager.IsEnable(CALLSTATE.BEGIN_SCAN))
                        {
                            switch (scriptsManager.Check(CALLSTATE.BEGIN_SCAN, media, out Media mo))
                            {
                                case CONTINUESTATE.CONTINUE_ABORT: CheckStat.SetHostReject(); continue;
                                case CONTINUESTATE.CONTINUE_CHANGE:
                                    {
                                        if (mo == null)
                                            continue;

                                        media = mo;
                                        if (string.IsNullOrEmpty(media.Attributes.GroupTitle))
                                            media.Attributes.SetGroupTitle(group);
                                        else
                                            group = media.Attributes.GroupTitle;
                                        break;
                                    }
                            }
                        }
                        #endregion

                        if ((M3U.M3UWriter.IsM3uFileName(media.MediaFile) && (!media.IsStream)) || isplaylist)
                        {
                            NewMediaFile(media, path);
                            continue;
                        }

                        Tuple<string, FileStream, StreamWriter, object, bool> tr;
                        ServiceDataItems epgbase = default;

                        do
                        {
                            if ((tr = baseStreams.GetStream(group)) == default)
                            {
                                $"Open group file is null -> '{group}'".WriteLog('!', LogWriter);
                                CheckStat.SetHostError();
                                break;
                            }

                            if (ServiceStatic.IfXmlBase(DataTarget.Id_EpgChannels))
                                epgbase = ServiceStatic.GetXmlBase(DataTarget.Id_EpgChannels);

                            lock (tr.Item4)
                                Writer(tr.Item3, media, epgbase);

                            CheckStat.SetHostGood();

                        } while (false);
                        do
                        {
                            if (string.IsNullOrWhiteSpace(media.Attributes.TvgId))
                                break;
                            if (!ServiceStatic.IfXmlBase(DataTarget.Id_EpgFavorite))
                                break;

                            ServiceDataItems favbase = ServiceStatic.GetXmlBase(DataTarget.Id_EpgFavorite);
                            if ((favbase == default) || ((from i in favbase.Items
                                                          where i.IfTag.Equals(media.Attributes.TvgId, StringComparison.InvariantCultureIgnoreCase)
                                                          select i).FirstOrDefault() == default))
                                break;

                            string fvgroup = string.IsNullOrWhiteSpace(Settings.Default.ConfigM3uFavoriteName) ?
                                BasePath.NameFavoritesM3u : Settings.Default.ConfigM3uFavoriteName;

                            if ((tr = baseStreams.GetStream(fvgroup)) == default)
                            {
                                $"Open Favorite group file is null -> '{fvgroup}'".WriteLog('!', LogWriter);
                                CheckStat.SetHostError();
                                break;
                            }
                            media.Attributes.SetGroupTitle(fvgroup);

                            lock (tr.Item4)
                                Writer(tr.Item3, media, epgbase);

                        } while (false);
                    }
                    catch (Exception e) { e.WriteLog('!', $"Scan -> ParseFile -> Medias ->", LogWriter); }
                }
            }
            catch (Exception e) { e.WriteLog('!', $"Scan -> ParseFile -> M3UP Init ->", LogWriter); }
        }

        private void Writer(StreamWriter w, Media m, ServiceDataItems d)
        {
            w.WriteItem(m, d);
            if (m.MediaFileCount > 1)
                w.WriteItem(m.MediaFileSwap(), d);
        }

        private string GetGroupAlias(string s, int id)
        {
            string group = default;
            do
            {
                if (ServiceStatic.IfXmlBase(DataTarget.Id_GroupWildcardChannels) &&
                    ((group = ServiceStatic.GetXmlBase(DataTarget.Id_GroupWildcardChannels).GetContainsIdFromName(s)) != default))
                    break;
                if (id == 3)
                    break;
                if (ServiceStatic.IfXmlBase(DataTarget.Id_GroupAlias) &&
                    ((group = ServiceStatic.GetXmlBase(DataTarget.Id_GroupAlias).GetStartsWithIdFromName(s)) != default))
                    break;
                if (id == 0)
                    return s;
            } while (false);
            return group;
        }
        #endregion

        #region Create new media file
        private async void NewMediaFile(Media m, string f)
        {
            LogWriter.Info.Write($"parser: new media file - {m.Title.RawTitle}");

            try
            {
                var d = PlayListServiceUtils.CreateM3uDirectory("new");
                if (d == null)
                    return;

                FileInfo file = new(Path.Combine(
                        d.FullName,
                        $"{Path.GetFileNameWithoutExtension(f)}-{BasePathRename.FileNameFromUrlString(m.MediaFile)}")
                        .Normalize(NormalizationForm.FormC));
                if (file.Exists && (!file.IsReadOnly))
                    file.Delete();

                await HttpClientDefault.DownloadFileAsync(new Uri(m.MediaFile), file);

                Extm3u extm3u = M3U.M3UP.ParseFromFile(file.FullName);
                if (!CheckExtm3u(extm3u, file.FullName))
                {
                    file.Delete();
                    $"Scan -> New media file -> '{file.FullName}' is empty, delete ..".WriteLog('i', LogWriter);
                    return;
                }
                m3uNewList.Add(new Tuple<List<Media>, string, bool>(extm3u.Medias.Distinct().ToList(), file.FullName, false));
            }
            catch (Exception e) { ErrorToLog($"Scan -> New media file ->", e); }
        }
        #endregion

    }
}
