﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Threading;
using PlayListService.Data;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;

namespace PlayListService.Task
{
    public class ServiceTaskVersionDownloader : ServiceTaskBase, IServiceTask
    {
        static readonly string Tag = "Version Downloader";
        private readonly string Url_;

        public ServiceTaskVersionDownloader(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ServiceStatic.GetLog();
            Url_ = BaseConstant.RepoDownloadUrl("Releases/M3U-PlayListServiceVersion.xml");
        }

        public async void Start()
        {
            try
            {
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_VERSION, true);
                if (tokenCancel.IsCancellationRequested)
                    return;

                $"{Tag} begin -> {Url_}".WriteLog('i', LogWriter);
                string s = await new Uri(Url_).DownloadStringAsync(20);
                do
                {
                    if (string.IsNullOrWhiteSpace(s))
                        break;
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    UTF8Encoding utf8 = new(false);
                    FilesPlayListVersion data = GetVersionFromString(s, utf8);
                    if (data == default)
                        break;
                    ServiceStatic.SetRepoVersion(data);
                    $"{Tag} complete -> {data.AppVersion}".WriteLog('i', LogWriter);
                    File.WriteAllText(BasePath.RepoLastVersion, s, utf8);

                } while (false);
                $"{Tag} end -> {Url_}".WriteLog('i', LogWriter);
            }
            catch (Exception e) { e.WriteLog('i', $"{Tag} error -> {Url_} -> {e.Message}", LogWriter); }
            finally
            {
                CallEvent(DirectoryWatch.ProcessId.ID_STAGE_VERSION, false);
            }
        }

        private FilesPlayListVersion GetVersionFromString(string s, UTF8Encoding utf8)
        {
            using MemoryStream ms = new();
            using TextWriter tw = new StreamWriter(ms, utf8);
            tw.Write(s);
            tw.Flush();
            ms.Position = 0;
            return ms.DeserializeFromStream<FilesPlayListVersion>(utf8);
        }
    }
}
