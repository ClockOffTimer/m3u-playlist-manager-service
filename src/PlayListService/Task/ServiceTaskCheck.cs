﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using M3U.Model;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Report;
using PlayListServiceScript;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Task
{
    public class ServiceTaskCheck : ServiceTaskBase, IServiceTask
    {
        private readonly HttpTaskGlobalSettings GlobalSettings;
        private readonly List<string> LocalUrlList = new();

        public ServiceTaskCheck(CancellationToken token)
        {
            tokenCancel = token;
            LogWriter = ProcessLog.Create(
                new FileLog(BasePath.GetCheckLogPath(), "Check"),
                Settings.Default.IsConfigReport ? new Reporter() : default);
            CurrentStatId = ProcessStat.Id.Check;
            GlobalSettings = new HttpTaskGlobalSettings(
                CheckStat, HttpClientDefault.HTTP.HttpLocker, tokenCancel, LogWriter,
                new bool[]
                        {
                            Settings.Default.IsConfigCheckSkipDash,
                            Settings.Default.IsConfigCheckSkipMime,
                            Settings.Default.IsConfigCheckSkipProto,
                            Settings.Default.IsConfigCheckSkipUdpProxy
                        });

            LocalUrlList.Add(PlayListServiceUtils.localMediaBaseUrl("video", true));
            LocalUrlList.Add(PlayListServiceUtils.localMediaBaseUrl("audio", true));
        }
        ~ServiceTaskCheck()
        {
            ILogManager pl = LogWriter;
            if (pl != default)
            {
                try { pl.Close(); } catch { }
            }
        }

        public void Start()
        {
            CallEvent(DirectoryWatch.ProcessId.ID_CHECK_MEDIA, true);
            Waiter(150000);

            var t = new Thread(() => CheckProcess())
            {
                Priority = Settings.Default.ConfigCpuPriority switch
                {
                    0 => ThreadPriority.Lowest,
                    1 => ThreadPriority.BelowNormal,
                    3 => ThreadPriority.AboveNormal,
                    4 => ThreadPriority.Highest,
                    _ => ThreadPriority.Normal
                }
            };
            t.Start();
            t.Join(TimeSpan.FromHours(5));
            Waiter(150000);
            CallEvent(DirectoryWatch.ProcessId.ID_CHECK_MEDIA, false);
            LogWriter.Close();
            LogWriter = default;
            t = default;
            GC.Collect();
        }

        private void CheckProcess()
        {
            "Check media: Started!".WriteLog('i', LogWriter);
            try
            {
                if (HttpClientDefault.HTTP.ProxyType != HttpProxyType.None)
                {
                    $"Check {HttpClientDefault.HTTP.ProxyCount} proxy list, wait..".WriteLog('i', LogWriter);
                    HttpClientDefault.HTTP.CheckProxies().ConfigureAwait(false).GetAwaiter().GetResult();
                    $"Check proxy end, active: {HttpClientDefault.HTTP.ProxyCount}".WriteLog('i', LogWriter);
                }
            } catch (Exception e) { ErrorToLog($"Check proxy ->", e); }

            try
            {
                List<Tasks.Task> tasks = new();
                List<string> files =
                    Directory.EnumerateFiles(PlayListServiceUtils.GetConfigM3uPath(), "*.m3u", SearchOption.TopDirectoryOnly)
                        .ToList();

                int idx = (Settings.Default.ConfigCpuPriority <= 1) ? 4 : (4 * Settings.Default.ConfigCpuPriority);
                bool b = Settings.Default.ConfigCpuPriority < 4;
                foreach (var f in files)
                {
                    tokenCancel.ThrowIfCancellationRequested();
                    FileInfo file = new(f);
                    if (!file.Exists)
                        continue;
                    if (file.IsReadOnly)
                    {
                        $"Check -> file {file.Name} is read-only, Skip..".WriteLog('!', LogWriter);
                        continue;
                    }
                    if (file.Length == 0)
                    {
                        $"Check -> file {file.Name} is empty, Delete..".WriteLog('!', LogWriter);
                        file.Delete();
                        continue;
                    }
                    tasks.Add(ParseFile(file));
                    if (!b && (tasks.Count > idx))
                        TaskWaitFiles(tasks);
                    Tasks.Task.Yield();
                }
                Tasks.Task.Yield();
                TaskWaitFiles(tasks);
                tasks = default;
                files = default;
            }
            catch (AggregateException e) { HttpClientException.Create(e).WriteLog('!', $"Check -> +", LogWriter); }
            catch (OperationCanceledException e) { e.WriteLog('!', $"Check -> All operation canceled ->", LogWriter); }
            catch (Exception e) { e.WriteLog('!', $"Check -> foreach files ->", LogWriter); }
            finally
            {
                StatExport();
                CheckStat.ToString().WriteLog('i', LogWriter);
                "Check media: End!".WriteLog('i', LogWriter);
                GC.Collect();
            }
        }

        private void TaskWaitFiles(List<Tasks.Task> tasks)
        {
            #region Task files pool clear
            Tasks.Task task = Tasks.Task.WhenAll(tasks);
            try { task.Wait(); } catch { }

            for (int i = tasks.Count - 1; i >= 0; i--)
            {
                try
                {
                    Tasks.Task t = tasks[i];
                    if (t == null)
                    {
                        tasks.RemoveAt(i);
                        $"Check Task is null, id: {i}".WriteLog('!', LogWriter);
                        continue;
                    }
                    if (t.Exception != null)
                        TaskErrorToLog(t);
                    if ((t.Status == Tasks.TaskStatus.Faulted) ||
                        (t.Status == Tasks.TaskStatus.Canceled) ||
                        (t.Status == Tasks.TaskStatus.RanToCompletion))
                    {
                        tasks.RemoveAt(i);
                        try { t.Dispose(); }
                        catch (Exception e) { e.WriteLog('!', $"Check -> Task wait -> Dispose:", LogWriter); }
                        continue;
                    }
                }
                catch (Exception e) { e.WriteLog('!', $"Check -> Task wait -> Error:", LogWriter); }
                Tasks.Task.Yield();
            }
            tasks.Clear();
            #endregion
        }

        private async Tasks.Task ParseFile(FileInfo f)
        {
            await Tasks.Task.Run(() =>
            {
                ScriptsManager<Media> scriptsManager = default;
                $"Check: start file - {f.Name}".WriteLog('v', LogWriter);

                try
                {
                    if (f.FullName.Equals(
                        PlayListServiceUtils.GetAudioTranslateFile(),
                        StringComparison.InvariantCultureIgnoreCase))
                        return;

                    Extm3u extm3u = M3U.M3UP.ParseFromFile(f.FullName);
                    if (!CheckExtm3u(extm3u, f.FullName))
                        return;

                    scriptsManager = GetScriptsManager<Media>();

                    BasePathMapping mapVideo = new(
                        LocalUrlList[0], LocalUrlList[0],
                        Settings.Default.ConfigVideoPath);

                    BasePathMapping mapAudio = new(
                        LocalUrlList[1], LocalUrlList[1],
                        Settings.Default.ConfigAudioPath);

                    List<Media> outlist = new();
                    List<Media> srclist = extm3u.Medias.Distinct(new M3U.MediaComparer()).ToList();
                    if ((srclist == null) || (srclist.Count == 0))
                    {
                        $"M3U select distinct is null, from {f.FullName}".WriteLog('!', LogWriter);
                        return;
                    }

                    CheckStat.SetHostTotal(srclist.Count);
                    #region Check DNS -> IP
                    try
                    {
                        for (int i = 0; i >= 0; i--)
                        {
                            Media m = srclist[i];
                            try
                            {
                                if ((m.MediaUrl == default) || string.IsNullOrWhiteSpace(m.MediaUrl.Host))
                                    throw new WebException($"Host {m.MediaUrl} is empty", WebExceptionStatus.NameResolutionFailure);
                                IPAddress[] addr = Dns.GetHostAddresses(m.MediaUrl.Host);
                                if ((addr == default) || (addr.Length == 0))
                                    throw new WebException($"IP Address from {m.MediaUrl} is empty", WebExceptionStatus.NameResolutionFailure);

                            } catch (Exception e) {
                                e.WriteLog('!', $"Bad DNS {m.MediaFile}, remove..", LogWriter);
                                srclist.RemoveAt(i);
                                CheckStat.SetHostBad();
                            }
                        }

                    } catch (Exception e) { e.WriteLog('!', "Check DNS to IP ->", LogWriter); }
                    #endregion

                    #region DEBUG_DETAILS foreach In M3U List
#if DEBUG_DETAILS
                    try
                    {
                        foreach (var m in srclist)
                            LogWriter.Verbose.Write($"+M3U-In\t{m.MediaFile}\t\t({f.FullName})");
                    }
                    catch { }
#endif
                    #endregion

                    #region HTTP URL CHECK
                    List<Tasks.Task<HttpTaskRequest>> tasks = new();

                    for (int i = 0; i < srclist.Count; i++)
                    {
                        Media media = srclist[i];
                        tokenCancel.ThrowIfCancellationRequested();

                        do
                        {
                            if (!string.IsNullOrWhiteSpace(media.MediaFile))
                                break;
                            if (media.MediaFileCount > 0)
                                _ = media.MediaFileSwap();
                            if (!string.IsNullOrWhiteSpace(media.MediaFile))
                                break;

                            CheckStat.SetHostReject();
                            continue;

                        } while (false);

                        #region SCRIPT BEGIN_LOCALCHECK

                        bool islocal = false;
                        for (int n = 0; n < LocalUrlList.Count; n++)
                        {
                            if ((n == 0) || (n == 1))
                            {
                                if (!mapVideo.IsFileExistFromUrl(media.MediaFile))
                                    continue;
                                else { islocal = true; break; }
                            }
                            else if ((n == 2) || (n == 3))
                            {
                                if (!mapAudio.IsFileExistFromUrl(media.MediaFile))
                                    continue;
                                else { islocal = true; break; }
                            }
                        }
                        if (islocal)
                        {
                            if (scriptsManager.IsEnable(CALLSTATE.BEGIN_LOCALCHECK))
                            {
                                switch (scriptsManager.Check(CALLSTATE.BEGIN_LOCALCHECK, media, out Media mo))
                                {
                                    case CONTINUESTATE.CONTINUE_ABORT: CheckStat.SetHostReject(); continue;
                                    case CONTINUESTATE.CONTINUE_CHANGE:
                                        {
                                            if (mo != default)
                                                media = mo;
                                            break;
                                        }
                                }
                            }
                            CheckStat.SetHostGood();
                            outlist.Add(media);
                            continue;
                        }
                        #endregion

                        if (M3U.M3UWriter.IsUdpUrl(media.MediaFile))
                        {
                            outlist.Add(media);
                            continue;
                        }

                        #region SCRIPT BEGIN_CHECK
                        if (scriptsManager.IsEnable(CALLSTATE.BEGIN_CHECK))
                        {
                            switch (scriptsManager.Check(CALLSTATE.BEGIN_CHECK, media, out Media mo))
                            {
                                case CONTINUESTATE.CONTINUE_ABORT: CheckStat.SetHostReject(); continue;
                                case CONTINUESTATE.CONTINUE_CHANGE:
                                    {
                                        if (mo != default)
                                            media = mo;
                                        break;
                                    }
                            }
                        }
                        #endregion

                        if (i % 10 == 0)
                        {
                            #region WAIT TASK COMPLETTE HTTP CHECK
                            List<Media> mlist = HttpTaskRequest.TasksCheckResult(tasks, LogWriter);
                            if (mlist.Count > 0)
                            {
                                outlist.AddRange(mlist.ToArray());
                                $"+ Cycle, tasks left:{tasks.Count}, add {mlist.Count}/{outlist.Count} good url to {Path.GetFileNameWithoutExtension(f.Name)}".WriteLog('v', LogWriter);
                            }
                            #endregion
                        }
                        tasks.Add(new HttpTaskRequest(media, GlobalSettings).BuildResult());
                    }
                    #endregion

                    #region WAIT TASK COMPLETTE HTTP CHECK
                    {
                        List<Media> mlist = HttpTaskRequest.TasksCheckResult(tasks, LogWriter, tokenCancel);
                        if (mlist.Count > 0)
                        {
                            outlist.AddRange(mlist.ToArray());
                            $"+ Final, tasks left:{tasks.Count}, add {mlist.Count}/{outlist.Count} good url to {Path.GetFileNameWithoutExtension(f.Name)}".WriteLog('v', LogWriter);
                        }
                    }
                    #endregion

                    #region RUN RETRY HTTP CHECK AND WAIT ALL
                    if (Settings.Default.IsConfigTimeOutRetry)
                    {
                        foreach (Media m in srclist)
                        {
                            if ((from mm in outlist
                                         where mm.Equals(m)
                                         select mm).FirstOrDefault() == default)
                                tasks.Add(new HttpTaskRequest(m, GlobalSettings).BuildResult());
                        }
                        if (tasks.Count > 0)
                        {
                            LogWriter.Verbose.Write($"+ Retry hosts re-check, COUNT = {tasks.Count}");
                            List<Media> mlist = HttpTaskRequest.TasksCheckResult(tasks, LogWriter, tokenCancel);
                            if (mlist.Count > 0)
                            {
                                outlist.AddRange(mlist.ToArray());
                                $"+ Final, tasks left:{tasks.Count}, add {mlist.Count}/{outlist.Count} good url to {Path.GetFileNameWithoutExtension(f.Name)}".WriteLog('v', LogWriter);
                            }
                        }
                    }
                    #endregion

                    #region DEBUG_DETAILS foreach Out M3U List
#if DEBUG_DETAILS
                    try
                    {
                        foreach (var m in outlist)
                            System.Diagnostics.Debug.WriteLine($"\t+M3U-Out\t{m.MediaFile}\t\t({f.FullName})");
                    } catch { }
#endif
                    #endregion

                    if (outlist.Count == 0)
                    {
                        f.Delete();
                        $"Checked urls not collected -> delete empty file -> {f.Name}".WriteLog('w', LogWriter);
                        return;
                    }

                    using var sw = new StreamWriter(f.FullName, false, new UTF8Encoding(false));
                    if (Settings.Default.IsConfigEpg)
                        M3U.M3UWriter.WriteHeader(sw,
                            PlayListServiceUtils.localMediaBaseUrl($"epg/{Path.GetFileName(Settings.Default.ConfigEpgUrl)}"));
                    else
                        M3U.M3UWriter.WriteHeader(sw);

                    foreach (var m in outlist)
                    {
                        Media media = m;
                        tokenCancel.ThrowIfCancellationRequested();
                        if (string.IsNullOrEmpty(media.MediaFile))
                        {
                            CheckStat.SetHostReject();
                            continue;
                        }

                        #region SCRIPT END_CHECK
                        if (scriptsManager.IsEnable(CALLSTATE.END_CHECK))
                        {
                            switch (scriptsManager.Check(CALLSTATE.END_CHECK, media, out Media mo))
                            {
                                case CONTINUESTATE.CONTINUE_ABORT: CheckStat.SetHostReject(); continue;
                                case CONTINUESTATE.CONTINUE_CHANGE:
                                    {
                                        if (mo != default)
                                            media = mo;
                                        break;
                                    }
                            }
                        }
                        #endregion

                        M3U.M3UWriter.WriteItem(sw, media, default);
                        if (media.MediaFileCount > 1)
                            M3U.M3UWriter.WriteItem(sw, media.MediaFileSwap(), default);

                        #region DEBUG_DETAILS write M3U item
#if DEBUG_DETAILS
                            LogWriter.Verbose.Write($"Check -> ParseFile -> {f.FullName}\r\n\t-> {media.GetShortUrl}");
#endif
                        #endregion
                    }

                    outlist.Clear();
                    mapVideo = default;
                    mapAudio = default;
                    outlist = default;
                    srclist = default;
                    extm3u = default;
                }
                catch (AggregateException e) { HttpClientException.Create(e).WriteLog('!', "Check -> ", LogWriter); }
                catch (Tasks.TaskCanceledException ec) { ec.WriteLog('!', $"Check -> ParseFile -> Cancellation request ->", LogWriter); }
                catch (OperationCanceledException ex) { ex.WriteLog('!', $"Check -> ParseFile -> All operation canceled ->", LogWriter); }
                catch (Exception ez) { ez.WriteLog('!', $"Check -> ParseFile -> collection ->", LogWriter); }
                finally
                {
                    scriptsManager?.Clear();
                    scriptsManager = default;
                }
            });
        }
    }
}
