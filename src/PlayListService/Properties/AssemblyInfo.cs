﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// общие сведения об этой сборке.
[assembly: AssemblyTitle("PlayListService")]
[assembly: AssemblyDescription("The M3U Playlist service supports periodic source checking, sorting and dividing the playlist into groups, access to the program guide, web access to .m3u and .m3u8 files. For hardware TV players and M3UP format playback devices.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service/-/tree/main/Releases")]
[assembly: AssemblyProduct("M3U Play List Service")]
[assembly: AssemblyCopyright("Copyright ©  2021")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Установка значения False для параметра ComVisible делает типы в этой сборке невидимыми
// для компонентов COM. Если необходимо обратиться к типу в этой сборке через
// компонент COM, задайте для атрибута ComVisible этого типа значение TRUE.
[assembly: ComVisible(false)]

// Указанный ниже идентификатор GUID предназначен для идентификации библиотеки типов, если этот проект будет видимым для COM-объектов
[assembly: Guid("69a37cb5-3cc9-4574-98c6-ef574ce618bf")]

// Сведения о версии сборки состоят из указанных ниже четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии
//      Номер сборки
//      Номер редакции
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: NeutralResourcesLanguage("en")]
