﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using PlayListServiceData.Base;
using PlayListServiceData.Settings;

namespace PlayListService.Data
{
    public partial class JsProxySettings : BaseEvent<IServiceSettings, bool>, IServiceSettings
    {
        public bool IsLoad
        {
            get => true;
            set { }
        }
        public int ConfigNetOldPort
        {
            get { return ConfigNetPort; }
        }
        public string ConfigNetOldSslDomain
        {
            get { return ConfigNetSslDomain; }
        }
        public string ExeConfigName
        {
            get => string.Empty;
        }


#pragma warning disable CS0067
        public event PropertyChangedEventHandler PropertyChanged;
#pragma warning restore CS0067

        public bool IsSSlChange()
        {
            throw new NotImplementedException();
        }

        public void Load()
        {
            throw new NotImplementedException();
        }

        public void ReLoad()
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void WaitLoad()
        {
            throw new NotImplementedException();
        }
    }
}
