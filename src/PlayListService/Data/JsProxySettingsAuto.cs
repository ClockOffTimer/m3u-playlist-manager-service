
/* Copyright (c) 2021 PlayListService, NT, MIT license
   AUTO-GENERATED! NOT MANUAL EDIT!
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Settings;

namespace PlayListService.Data
{
    public partial class JsProxySettings : BaseEvent<IServiceSettings, bool>, IServiceSettings
    {
        public bool FirstConfigure
        {
            get => Settings.Default.FirstConfigure;
            set {}
        }
        public bool IsConfigAddTagsToLocalFiles
        {
            get => Settings.Default.IsConfigAddTagsToLocalFiles;
            set {}
        }
        public bool IsConfigAudioCache
        {
            get => Settings.Default.IsConfigAudioCache;
            set {}
        }
        public bool IsConfigCheckSkipDash
        {
            get => Settings.Default.IsConfigCheckSkipDash;
            set {}
        }
        public bool IsConfigCheckSkipMime
        {
            get => Settings.Default.IsConfigCheckSkipMime;
            set {}
        }
        public bool IsConfigCheckSkipProto
        {
            get => Settings.Default.IsConfigCheckSkipProto;
            set {}
        }
        public bool IsConfigCheckSkipUdpProxy
        {
            get => Settings.Default.IsConfigCheckSkipUdpProxy;
            set {}
        }
        public bool IsConfigDlnaEnable
        {
            get => Settings.Default.IsConfigDlnaEnable;
            set {}
        }
        public bool IsConfigEpg
        {
            get => Settings.Default.IsConfigEpg;
            set {}
        }
        public bool IsConfigJsHost
        {
            get => Settings.Default.IsConfigJsHost;
            set {}
        }
        public bool IsConfigJsLib
        {
            get => Settings.Default.IsConfigJsLib;
            set {}
        }
        public bool IsConfigJsXMLHttpRequest
        {
            get => Settings.Default.IsConfigJsXMLHttpRequest;
            set {}
        }
        public bool IsConfigLogEnable
        {
            get => Settings.Default.IsConfigLogEnable;
            set {}
        }
        public bool IsConfigLogHttpEnable
        {
            get => Settings.Default.IsConfigLogHttpEnable;
            set {}
        }
        public bool IsConfigM3UCache
        {
            get => Settings.Default.IsConfigM3UCache;
            set {}
        }
        public bool IsConfigNetHttpProxy
        {
            get => Settings.Default.IsConfigNetHttpProxy;
            set {}
        }
        public bool IsConfigNetHttpSupport
        {
            get => Settings.Default.IsConfigNetHttpSupport;
            set {}
        }
        public bool IsConfigNetSocks5Proxy
        {
            get => Settings.Default.IsConfigNetSocks5Proxy;
            set {}
        }
        public bool IsConfigNetSslEnable
        {
            get => Settings.Default.IsConfigNetSslEnable;
            set {}
        }
        public bool IsConfigRenameLocalFiles
        {
            get => Settings.Default.IsConfigRenameLocalFiles;
            set {}
        }
        public bool IsConfigReport
        {
            get => Settings.Default.IsConfigReport;
            set {}
        }
        public bool IsConfigSeparateUdpSource
        {
            get => Settings.Default.IsConfigSeparateUdpSource;
            set {}
        }
        public bool IsConfigTimeOutRetry
        {
            get => Settings.Default.IsConfigTimeOutRetry;
            set {}
        }
        public bool IsConfigTorrentEnable
        {
            get => Settings.Default.IsConfigTorrentEnable;
            set {}
        }
        public bool IsConfigVideoCache
        {
            get => Settings.Default.IsConfigVideoCache;
            set {}
        }
        public List<string> ConfigAudioDirExclude
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigAudioDirExclude);
            set {}
        }
        public List<string> ConfigM3uDirExclude
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigM3uDirExclude);
            set {}
        }
        public List<string> ConfigNetAccess
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigNetAccess);
            set {}
        }
        public List<string> ConfigTorrentTrackers
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigTorrentTrackers);
            set {}
        }
        public List<string> ConfigUserAuth
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigUserAuth);
            set {}
        }
        public List<string> ConfigVideoDirExclude
        {
            get => new((IEnumerable<string>)Settings.Default.ConfigVideoDirExclude);
            set {}
        }
        public int ConfigAudioReadTimeout
        {
            get => Settings.Default.ConfigAudioReadTimeout;
            set {}
        }
        public int ConfigCpuPriority
        {
            get => Settings.Default.ConfigCpuPriority;
            set {}
        }
        public int ConfigLogLevel
        {
            get => Settings.Default.ConfigLogLevel;
            set {}
        }
        public int ConfigNetPort
        {
            get => Settings.Default.ConfigNetPort;
            set {}
        }
        public int ConfigRunCheckPeriodDays
        {
            get => Settings.Default.ConfigRunCheckPeriodDays;
            set {}
        }
        public int ConfigRunDownloadPeriodDays
        {
            get => Settings.Default.ConfigRunDownloadPeriodDays;
            set {}
        }
        public int ConfigTimeOutRequest
        {
            get => Settings.Default.ConfigTimeOutRequest;
            set {}
        }
        public string ConfigAudioDirFormat
        {
            get => Settings.Default.ConfigAudioDirFormat;
            set {}
        }
        public string ConfigAudioFileFormat
        {
            get => Settings.Default.ConfigAudioFileFormat;
            set {}
        }
        public string ConfigAudioPath
        {
            get => Settings.Default.ConfigAudioPath;
            set {}
        }
        public string ConfigAudioPrefix
        {
            get => Settings.Default.ConfigAudioPrefix;
            set {}
        }
        public string ConfigAudioTranslateM3u
        {
            get => Settings.Default.ConfigAudioTranslateM3u;
            set {}
        }
        public string ConfigConfiguratorExe
        {
            get => Settings.Default.ConfigConfiguratorExe;
            set {}
        }
        public string ConfigEpgUrl
        {
            get => Settings.Default.ConfigEpgUrl;
            set {}
        }
        public string ConfigLanguage
        {
            get => Settings.Default.ConfigLanguage;
            set {}
        }
        public string ConfigLogName
        {
            get => Settings.Default.ConfigLogName;
            set {}
        }
        public string ConfigLogSource
        {
            get => Settings.Default.ConfigLogSource;
            set {}
        }
        public string ConfigM3uFavoriteName
        {
            get => Settings.Default.ConfigM3uFavoriteName;
            set {}
        }
        public string ConfigM3uPath
        {
            get => Settings.Default.ConfigM3uPath;
            set {}
        }
        public string ConfigNetAddr
        {
            get => Settings.Default.ConfigNetAddr;
            set {}
        }
        public string ConfigNetSslCertStore
        {
            get => Settings.Default.ConfigNetSslCertStore;
            set {}
        }
        public string ConfigNetSslCertThumbprint
        {
            get => Settings.Default.ConfigNetSslCertThumbprint;
            set {}
        }
        public string ConfigNetSslDomain
        {
            get => Settings.Default.ConfigNetSslDomain;
            set {}
        }
        public string ConfigTorrentsPath
        {
            get => Settings.Default.ConfigTorrentsPath;
            set {}
        }
        public string ConfigUdpPrefix
        {
            get => Settings.Default.ConfigUdpPrefix;
            set {}
        }
        public string ConfigVideoPath
        {
            get => Settings.Default.ConfigVideoPath;
            set {}
        }
        public string ConfigVideoPrefix
        {
            get => Settings.Default.ConfigVideoPrefix;
            set {}
        }
    }
}

