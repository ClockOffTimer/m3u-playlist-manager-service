﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Threading;
using EmbedIO.Utilities;
using PlayListService.log;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.Process.Log;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Data
{
    public class HistoryBase
    {
        private readonly FilesPlayList HistoryList;
        private readonly object lock__ = new();

        public HistoryBase()
        {
            HistoryList = new FilesPlayList()
            {
                Url = "/history",
                Lang = Thread.CurrentThread.CurrentCulture.Name,
                Ip = Settings.Default.ConfigNetAddr,
                Port = Settings.Default.ConfigNetPort
            };
            HistoryList.RootPaths.Add(new PathItem("m3u", Settings.Default.ConfigM3uPath));
            HistoryList.RootPaths.Add(new PathItem("video", Settings.Default.ConfigVideoPath));
            HistoryList.RootPaths.Add(new PathItem("audio", Settings.Default.ConfigAudioPath));
            HistoryList.RootPaths.Add(new PathItem("history", ""));
            Load();
        }

        public FilesPlayList Get()
        {
            try
            {
                lock (lock__)
                {
                    FilesPlayList fpl = HistoryList;
                    fpl.VersionC = ServiceStatic.CurrentVersion;
                    fpl.VersionR = ServiceStatic.RepoVersion;
                    fpl.FilesCount = HistoryList.Files.Items.Count;
                    fpl.Files.Items = HistoryList.Files.Items.AsEnumerable().Reverse().ToList();
                    fpl.DirsCount = 1;
                    return fpl;
                }
            } catch (Exception e) { e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e); }
            return default;
        }

        public async Tasks.Task Add(HistoryLogger.MessageExpand msg)
        {
            await Tasks.Task.Run(() =>
            {
                try
                {
                    lock (lock__)
                    {
                        if (HistoryList.Files.Items.Count > 99)
                            HistoryList.Files.Items.RemoveAt(0);

                        if (HistoryList.Files.Items.Count > 0)
                        {
                            FileItem fi = HistoryList.Files.Items[(HistoryList.Files.Items.Count - 1)];
                            if (fi.Name.Equals(msg.Name))
                            {
                                fi.Time = HttpDate.Format(DateTime.Now);
                                return;
                            }
                        }

                        string uri = $"/{msg.Folder}/{msg.File}.{msg.FileExt}";

                        try
                        {
                            FileItem item = (from i in HistoryList.Files.Items
                                             where i.Uri.Equals(uri)
                                             select i).FirstOrDefault();
                            if (item != default)
                                HistoryList.Files.Items.Remove(item);
                        } catch { }

                        HistoryList.Files.Add(
                            new FileItem(
                                HistoryList.Files.Items.Count,
                                msg.Name,
                                uri,
                                HttpDate.Format(DateTime.Now),
                                msg.IpAddress)
                            );
                    }
                    ServiceStatic.GetWatch().Add(
                        DirectoryWatch.ProcessId.ID_SAVE_HISTORY);

                } catch (Exception e) { e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e); }
            });
        }

        public void Save()
        {
            if (HistoryList.Files.Items.Count == 0)
                return;
            try
            {
                lock (lock__)
                {
                    using FileStream stream = File.Open(BasePath.GetHistoryBasePath(), FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                    HistoryList.Files.SerializeToStream(stream);
                }
            } catch (Exception e) { e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e); }
        }

        public void Load()
        {
            try
            {
                lock (lock__)
                {
                    using FileStream stream = File.Open(BasePath.GetHistoryBasePath(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                    FileItems fi = stream.DeserializeFromStream<FileItems>();
                    if ((fi != default) && (fi.Items.Count > 0))
                    {
                        HistoryList.Files.Items.InsertRange(0, fi.Items);

                        if (HistoryList.Files.Items.Count > 99)
                        {
                            for (int i = 0; i < (HistoryList.Files.Items.Count - 100); i++)
                                HistoryList.Files.Items.RemoveAt(i);
                        }
                    }
                }
            } catch (Exception e) { e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e); }
        }
    }
}
