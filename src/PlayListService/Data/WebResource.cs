﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Data
{
    public class WebResource
    {
        public WebResource() {}

        public async Tasks.Task ReadAsStringAsync(string s, Stream ss, CancellationToken token = default)
        {
            if (ss == default)
                return;

            await Tasks.Task.Run(() =>
            {
                try
                {
                    if ((token != default) && token.IsCancellationRequested)
                        return;

                    using Stream sr = GetAssemblyStream_(s);
                    if ((sr == default) || !sr.CanRead)
                        return;

                    UTF8Encoding utf8 = new(false);
                    using StreamWriter sw = new(ss, utf8);
                    if (sw == default)
                        return;

                    while (true)
                    {
                        byte[] buf = new byte[81920];
                        int i = sr.Read(buf, 0, buf.Length);
                        if (i <= 0)
                            break;
                        sw.Write(utf8.GetString(buf, 0, i));
                        if ((token != default) && token.IsCancellationRequested)
                            break;
                    }
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(WebResource)} -> Read ->", e); }
            });
        }

        public async Tasks.Task ReadAsByteAsync(string s, Stream ss, CancellationToken token = default)
        {
            byte[] b = await ReadAsByteAsync(s, token);
            if (b != default)
                await ss.WriteAsync(b, 0, b.Length).ConfigureAwait(false);
        }

        public async Tasks.Task<byte[]> ReadAsByteAsync(string s, CancellationToken token = default)
        {
            byte [] ret = new byte[0];
            await Tasks.Task<byte[]>.Run(() =>
            {
                try
                {
                    do
                    {
                        if ((token != default) && token.IsCancellationRequested)
                            break;

                        using Stream sr = GetAssemblyStream_(s);
                        if ((sr == default) || !sr.CanRead)
                            break;

                        using MemoryStream sm = new();
                        if (sm == default)
                            break;

                        while (true)
                        {
                            byte[] buf = new byte[81920];
                            int i = sr.Read(buf, 0, buf.Length);
                            if (i <= 0)
                                break;
                            sm.Write(buf, 0, i);
                            if ((token != default) && token.IsCancellationRequested)
                                break;
                        }
                        ret = (sm.Length > 0) ? sm.ToArray() : ret;

                    } while (false);
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(WebResource)} -> Read ->", e); }
                return ret;
            });
            return ret;
        }

        private Stream GetAssemblyStream_(string s)
        {
            Assembly ass = Assembly.GetExecutingAssembly();
            string name = $"{ass.GetName().Name.Replace('\\', '.')}.{s}";
            return ass.GetManifestResourceStream(name);
        }
    }
}

