﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListService.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class FilesPlayList
    {
        [XmlElement("dirs")]
        public FileItems Dirs = new();
        [XmlElement("files")]
        public FileItems Files = new();
        [XmlElement("paths")]
        public List<PathItem> RootPaths = new();
        [XmlElement("url")]
        public string Url = default;
        [XmlElement("dirscount")]
        public int DirsCount = 0;
        [XmlElement("filescount")]
        public int FilesCount = 0;
        [XmlElement("lang")]
        public string Lang = default;
        [XmlElement("ip")]
        public string Ip = default;
        [XmlElement("port")]
        public int Port = 0;
        [XmlElement("versionc")]
        public FilesPlayListVersion VersionC = default;
        [XmlElement("versionr")]
        public FilesPlayListVersion VersionR = default;

        public FilesPlayList() { }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class FileItems
    {
        [XmlElementAttribute("items")]
        public List<FileItem> Items = new();

        public FileItems() { }
        public void Add(FileItem item)
        {
            if (!Items.Contains(item))
                Items.Add(item);
        }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class PathItem
    {
        [XmlElement("tag")]
        public string Tag = default;
        [XmlElement("name")]
        public string Name = default;

        public PathItem() { }
        public PathItem(string t, string s)
        {
            Tag = t;
            Name = s;
        }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class FileItem
    {
        [XmlElement("id")]
        public int Id = -1;
        [XmlElement("name")]
        public string Name = default;
        [XmlElement("uri")]
        public string Uri = default;
        [XmlElement("imgurl")]
        public string ImageUri = default;
        [XmlElement("time")]
        public string Time = default;
        [XmlElement("ip")]
        public string IpAddress = default;
        [XmlElement("size")]
        public long Size = 0;
        [XmlElement("duration")]
        public int Duration = 0;
        [XmlElement("isdfmt")]
        public bool IsDateFormat = true;
        [XmlElement("issfmt")]
        public bool IsSizeFormat = true;
        [XmlElement("isip")]
        public bool IsIpAddress = false;

        public FileItem() { }
        public FileItem(int id, string n, string s, string t, long l, string image)
        {
            Name = n;
            Id = id;
            Uri = s;
            Time = t;
            Size = l;
            ImageUri = image;
        }
        public FileItem(int id, string n, string s, string t, string ip)
        {
            Name = n;
            Id = id;
            Uri = s;
            Time = t;
            IpAddress = ip;
            IsIpAddress = true;
            IsSizeFormat = false;
        }
    }
}
