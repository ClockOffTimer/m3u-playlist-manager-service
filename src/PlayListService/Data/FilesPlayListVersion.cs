﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListService.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class FilesPlayListVersion
    {
        [XmlElement("appname")]
        public string AppName = default;
        [XmlElement("appdate")]
        public string AppDate = default;
        [XmlElement("appversion")]
        public string AppVersion = default;
        [XmlElement("appfile")]
        public string AppFile = default;
        [XmlElement("appdesc")]
        public string AppDesc = default;
        [XmlElement("appbaseurl")]
        public string AppBaseUrl = default;
    }
}
