﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.Linq;
using PlayListService.Properties;
using PlayListServiceData.Process.Http.Data;

namespace PlayListService.Data
{
    public class HostConfigGet : IHostConfigGet
    {
        public int    ConfigNetPort => Settings.Default.ConfigNetPort;
        public bool   IsConfigNetSslEnable => Settings.Default.IsConfigNetSslEnable;
        public bool   IsConfigNetHttpSupport => Settings.Default.IsConfigNetHttpSupport;
        public string ConfigNetSslDomain => Settings.Default.ConfigNetSslDomain;
        public string ConfigNetSslCertStore => Settings.Default.ConfigNetSslCertStore;
        public string ConfigNetSslCertThumbprint => Settings.Default.ConfigNetSslCertThumbprint;
        public string ConfigNetAddr => Settings.Default.ConfigNetAddr;
        public List<string> ConfigUserAuth {
            get
            {
                if (Settings.Default.ConfigUserAuth.Count == 0)
                    return new();

                string[] ss = new string [Settings.Default.ConfigUserAuth.Count];
                Settings.Default.ConfigUserAuth.CopyTo(ss, 0);
                return ss.ToList();
            }
        }
    }
}
