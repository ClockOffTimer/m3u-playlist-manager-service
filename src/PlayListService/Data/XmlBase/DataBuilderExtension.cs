﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Process.Log;

namespace PlayListService.Data.XmlBase
{
    public static class DataBuilderExtension
    {
        private static bool Check_(ServiceDataItems b, string name)
        {
            return !((b == default) || (b.Items.Count == 0) || string.IsNullOrWhiteSpace(name));
        }

        public static string GetContainsIdFromName(this ServiceDataItems b, string name)
        {
            if (!Check_(b, name))
                return default;
            try
            {
                name = name.ToLowerInvariant();
                return (from i in b.Items
                        from n in i.IfItems
                        where name.Contains(n)
                        select i.IfTag.ToUpper()).FirstOrDefault();
            }
            catch (Exception e) { _ = e.WriteLog("Get Contains Id From Name:", ServiceStatic.GetLog()); }
            return default;
        }
        public static string GetStartsWithIdFromName(this ServiceDataItems b, string name)
        {
            if (!Check_(b, name))
                return default;
            try
            {
                string [] ss = name.Split(' ');
                if ((ss.Length > 0) && (ss[0].Length > 0))
                    name = ss[0];

                return (from i in b.Items
                        from n in i.IfItems
                        where name.StartsWith(n, StringComparison.OrdinalIgnoreCase)
                        select i.IfTag.ToUpper()).FirstOrDefault();
            }
            catch (Exception e) { _ = e.WriteLog("Get Starts with Id From Name:", ServiceStatic.GetLog()); }
            return default;
        }
    }
}
