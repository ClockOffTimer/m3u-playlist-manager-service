﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using PlayListServiceData.BaseXml;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Process.Log;

namespace PlayListService.Data.XmlBase
{
    public class DataBuilder<T2> where T2 : class, new()
    {
        private readonly ConcurrentDictionary<DataTarget, Lazy<T2>> Base_;
        private readonly Dictionary<DataTarget, Tuple<DataTarget, string, Func<DataTarget, string, T2>>> BaseInit_;

        public DataBuilder()
        {
            BaseInit_ = new Dictionary<DataTarget, Tuple<DataTarget, string, Func<DataTarget, string, T2>>>();
            Base_ = new ConcurrentDictionary<DataTarget, Lazy<T2>>();
        }

        public void Clear()
        {
            try { Base_.Clear(); } catch { }
        }

        private void LazyInit_()
        {
            Clear();
            if (BaseInit_.Count == 0)
                throw new DataBuilderException("init base is empty");

            foreach (var n in BaseInit_)
                if (!Base_.TryAdd(
                    n.Key,
                    new Lazy<T2>(() => n.Value.Item3.Invoke(n.Value.Item1, n.Value.Item2))
                    ))
                    throw new DataBuilderException($"add {n.Key} error");
        }

        public void LazyInit()
        {
            try
            {
                LazyInit_();
            } catch (Exception e) { _ = e.WriteLog("Lazy Init:", ServiceStatic.GetLog()); }
        }

        public void InitAll()
        {
            try
            {
                LazyInit_();

                var t = new Thread(() =>
                {
                    foreach (var n in Base_)
                    {
                        try
                        {
                            if (!n.Value.IsValueCreated)
                                _ = n.Value.Value;
                        } catch (Exception e) { _ = e.WriteLog($"init base {n.Key}:", ServiceStatic.GetLog()); }
                    }
                });
                t.Start();
                t.Join(TimeSpan.FromMinutes(5));

            } catch (Exception e) { _ = e.WriteLog(ServiceStatic.GetLog()); }
        }

        public bool IsBaseEnable(DataTarget target)
        {
            if (Base_.TryGetValue(target, out Lazy<T2> val))
                return (val != default) && (val.Value != default);
            return false;
        }

        public T2 Get(DataTarget target)
        {
            if (Base_.TryGetValue(target, out Lazy<T2> val))
                return val.Value;
            return default;
        }

        public void Add(DataTarget target, string path, Func<DataTarget, string, T2> fun)
        {
            try
            {
                BaseInit_.Add(
                    target,
                    new Tuple<DataTarget, string, Func<DataTarget,string,T2>>(target, path, fun));
            }
            catch (Exception e) { _ = e.WriteLog($"add init {target}:", ServiceStatic.GetLog()); }
        }

        public T2 Load<T1>(DataTarget target, string path) where T1 : class
        {
            try
            {
                using FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                T1 data = stream.DeserializeFromStream<T1>();
                if (data == default)
                    throw new DataBuilderException("serialize data is empty");

                if (data is IDataCopy idc)
                {
                    T2 val = idc.Copy<T2>(target, path, ServiceStatic.GetLog());
                    if (data == default)
                        throw new DataBuilderException("build data is empty");
                    return val;
                }
            }
            catch (Exception e) { _ = e.WriteLog("load xml:", ServiceStatic.GetLog()); }
            return default(T2);
        }
    }
}
