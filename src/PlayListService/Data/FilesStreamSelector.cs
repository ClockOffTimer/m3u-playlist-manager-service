﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;

namespace PlayListService.Data
{
    #region Scan Stream selector
    public class FilesStreamSelector
    {
        private static readonly string[] iconExtension = new string[]
        {
            ".jpg", ".png", ".gif", ".jpeg"
        };

        public static readonly string groupDefault = @"Other";
        private readonly bool isepgheader__;
        private readonly object lock__ = new();
        private readonly ConcurrentDictionary<string, Tuple<string, FileStream, StreamWriter, object, bool>> streams = new();
        private BasePathMapping pathMap = default;
        public string Tag { get; private set; } = default;
        public string RootPath { get; private set; } = default;
        public ILogManager LogWriter { get; private set; } = default;

        public string MapFilesPath
        {
            get { return (pathMap == null) ? string.Empty : pathMap.FilePath.Value;  }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    return;
                pathMap = new BasePathMapping(
                    PlayListServiceUtils.localMediaBaseUrl(Tag),
                    PlayListServiceUtils.localMediaBaseUrl(Tag),
                    value);
            }
        }

        public FilesStreamSelector(string tag, string path, ILogManager logWriter, bool isEpgHeader)
        {
            Tag = tag;
            RootPath = path;
            LogWriter = logWriter;
            isepgheader__ = isEpgHeader;
        }
        ~FilesStreamSelector()
        {
            Close();
        }

        public void Close()
        {
            foreach (var i in streams)
                try { CloseStream(i.Value); } catch { }
            streams.Clear();
        }

        #region Strings utilites
        public string GroupNameNormalize(string group)
        {
            try
            {
                if (string.IsNullOrEmpty(group))
                    return default;

                return WebUtility.HtmlEncode(group
                       .Replace('-', ' ')
                       .Replace('_', ' ')
                       .Replace('/', ' ')
                       .Replace('\\', ' ')
                       .Replace("`", string.Empty)
                       .Replace("~", string.Empty)
                       .Replace("\"", string.Empty)
                       .Replace("'", string.Empty)
                       .Replace("«", string.Empty)
                       .Replace("»", string.Empty)
                       .Replace("°", string.Empty)
                       .Replace("^", string.Empty)
                       .Replace("%", string.Empty)
                       .Replace("@", string.Empty)
                       .Replace("$", string.Empty)
                       .Replace("#", string.Empty)
                       .Replace("&", string.Empty)
                       .Replace(";", string.Empty)
                       .Replace(":", string.Empty)
                       .Replace("?", string.Empty)
                       .Replace("<", string.Empty)
                       .Replace(">", string.Empty)
                       .Replace("*", string.Empty)
                       .Normalize(NormalizationForm.FormC))
                       .Replace("#", string.Empty)
                       .Replace("&", string.Empty)
                       .Replace(";", string.Empty);

                // Files name remove char: "\/?:<>*|
            }
            catch { }
            return default;
        }
        public string GetM3uUrl(string path)
        {
            if (pathMap == default)
                return string.Empty;
            return pathMap.GetUrlFromPath(path);
        }
        public string GetM3uName(string path)
        {
            return WebUtility.HtmlEncode(GroupNameNormalize(Path.GetFileNameWithoutExtension(path)));
        }
        #endregion

        #region Rename files / Get poster utilites
        public void RenameFiles(FileInfo oldf, FileInfo newf)
        {
            try
            {
                int idx = 0,
                    len = Path.GetFileNameWithoutExtension(oldf.FullName).Length;
                string newname = Path.GetFileNameWithoutExtension(newf.FullName);

                foreach (var f in Directory.EnumerateFiles(
                    oldf.DirectoryName,
                    $"{Path.GetFileNameWithoutExtension(oldf.Name)}*.*",
                    SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        bool b = IsImageFile_(f);
                        FileInfo file = CheckFile_(f);
                        if (file == default)
                            continue;

                        if (Settings.Default.IsConfigRenameLocalFiles)
                        {
                            string filename = Path.GetFileNameWithoutExtension(file.FullName);
                            int k = filename.Length - len;
                            string postfix = (k > 0) ? filename.Substring(filename.Length - k) : string.Empty;
                            string s = Path.Combine(
                                        Path.GetDirectoryName(newf.FullName),
                                        $"{newname}{postfix}{file.Extension}");
                            if (b)
                            {
                                while (File.Exists(s))
                                {
                                    s = Path.Combine(
                                        Path.GetDirectoryName(newf.FullName),
                                        $"{newname}{postfix}_{idx++}{file.Extension}");
                                }
                            }
                            else if (File.Exists(s))
                                continue;
                            file.MoveTo(s);
                        }
                    }
                    catch (Exception e) { _ = e.WriteLog($"Scan local -> {nameof(RenameFiles)} ->", LogWriter); }
                }
            }
            catch (Exception e) { _ = e.WriteLog($"Scan local -> {nameof(RenameFiles)} -> {oldf.FullName}", LogWriter); }
        }

        public string GetPoster(FileInfo newf)
        {
            try
            {
                List<string> posters = new();

                foreach (string f in Directory.EnumerateFiles(
                    newf.DirectoryName,
                    $"{Path.GetFileNameWithoutExtension(newf.Name)}*.*",
                    SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        if (IsImageFile_(f))
                        {
                            FileInfo file = CheckFile_(f);
                            if (file == default)
                                continue;
                            posters.Add(GetM3uUrl(file.FullName));
                        }
                    }
                    catch (Exception e) { _ = e.WriteLog($"Scan local -> {nameof(GetPoster)} url ->", LogWriter); }
                }
                if (posters.Count == 0)
                    return default;

                Random rnd = new();
                int idx = rnd.Next(posters.Count);
                return posters[(idx >= 0) ? idx : 0];
            }
            catch (Exception e) { _ = e.WriteLog($"Scan local -> {nameof(GetPoster)} file -> {newf.FullName}", LogWriter); }
            return default;
        }

        private FileInfo CheckFile_(string s)
        {
            FileInfo file = new(s.Normalize(NormalizationForm.FormC));
            if ((file == null) || !file.Exists || (file.Attributes == FileAttributes.Directory))
                return default;
            return file;
        }
        private bool IsImageFile_(string s)
        {
            return (from i in FilesStreamSelector.iconExtension
                      where s.EndsWith(i)
                      select i).FirstOrDefault() != null;
        }
        #endregion

        public Tuple<string, FileStream, StreamWriter, object, bool> GetStream(string thisGroup, string prefix = default)
        {
            try
            {
                string groupname;
                Tuple<string, FileStream, StreamWriter, object, bool> rt = default;

                if (string.IsNullOrEmpty(thisGroup))
                    groupname = groupDefault;
                else
                    groupname = thisGroup;

                bool b = streams.ContainsKey(groupname);
                if (b)
                    if (b && streams.TryGetValue(groupname, out rt))
                        return rt;

                if (b)
                    streams.TryRemove(groupname, out rt);
                rt = BuildStreamWriter(groupname, prefix);
                if (rt == default)
                    return default;

                if (rt.Item5)
                {
                    lock (lock__)
                    {
                        Tuple<string, FileStream, StreamWriter, object, bool> rx =
                            new(rt.Item1, rt.Item2, rt.Item3, new object(), false);
                        try { streams.TryAdd(groupname, rx); } catch (ArgumentException e)
                        {
                            CloseStream(rx);
                            if (streams.ContainsKey(groupname) && streams.TryGetValue(groupname, out rt))
                                return rt;
                            _ = e.WriteLog($"Stream Selector -> Add Stream ->", LogWriter);
                            return default;
                        }
                        return rx;
                    }
                }
                return rt;
            }
            catch (Exception e)
            {
                _ = e.WriteLog($"Stream Selector -> GetStream ->", LogWriter);
                return default;
            }
        }

        #region private
        private void CloseStream(Tuple<string, FileStream, StreamWriter, object, bool> rt)
        {
            if (rt == default)
                return;

            lock (rt.Item4)
            {
                if (rt.Item3 != null)
                {
                    if (rt.Item3.BaseStream.CanWrite)
                    {
                        try
                        {
                            if (rt.Item3.BaseStream.CanWrite)
                                rt.Item3.Flush();
                            if (rt.Item3.BaseStream.CanRead || rt.Item3.BaseStream.CanWrite || rt.Item3.BaseStream.CanSeek)
                            {
                                rt.Item3.Close();
                                rt.Item3.Dispose();
                            }
                        }
                        catch (Exception e) { _ = e.WriteLog($"Stream Selector -> Close StreamWriter ->", LogWriter); }
                    }
                }
                if (rt.Item2 != null)
                {
                    try
                    {
                        rt.Item2.Close();
                        rt.Item2.Dispose();
                    }
                    catch (Exception e) { _ = e.WriteLog($"Stream Selector -> Close FileStream ->", LogWriter); }
                }
            }
        }

        private Tuple<string, FileStream, StreamWriter, object, bool> BuildStreamWriter(string groupname, string prefix)
        {
            try
            {
                lock (lock__)
                {
                    FileInfo f;
                    if (string.IsNullOrWhiteSpace(prefix))
                        f = new FileInfo(Path.Combine(RootPath, $"{groupname}.m3u"));
                    else
                        f = new FileInfo(Path.Combine(RootPath, $"{prefix}{groupname}.m3u"));

                    bool IsAppend = f.Exists && (f.Length > 0);
                    FileStream stream = File.Open(f.FullName, IsAppend ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.Write);
                    StreamWriter sw = new(stream, new UTF8Encoding(false))
                    {
                        AutoFlush = true
                    };
                    if (!IsAppend)
                    {
                        if (isepgheader__)
                            M3U.M3UWriter.WriteHeader(sw,
                                PlayListServiceUtils.localMediaBaseUrl($"epg/{Path.GetFileName(Settings.Default.ConfigEpgUrl)}"));
                        else
                            M3U.M3UWriter.WriteHeader(sw);
                    }
                    return new Tuple<string, FileStream, StreamWriter, object, bool>(groupname, stream, sw, null, true);
                }
            }
            catch (IOException ie)
            {
                if (streams.ContainsKey(groupname))
                {
                    _ = ie.WriteLog($"Stream Selector -> BuildStreamWriter -> IOException -> Key found -> {groupname}", LogWriter);
                    if (streams.TryGetValue(groupname, out Tuple<string, FileStream, StreamWriter, object, bool> rt))
                        return rt;
                }
                _ = ie.WriteLog($"Stream Selector -> BuildStreamWriter -> IOException ->", LogWriter);
            }
            catch (Exception e)
            {
                _ = e.WriteLog($"Stream Selector -> BuildStreamWriter ->", LogWriter);
            }
            return default;
        }
        #endregion
    }
    #endregion
}

