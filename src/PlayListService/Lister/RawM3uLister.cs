﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using M3U;
using M3U.Model;
using PlayListService.Properties;
using Swan;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class RawM3uLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new RawM3uLister());
        private string GetNameFromFile(string s) => s.Replace('_', ' ').Replace('-', ' ').Replace('.', ' ').Replace('#', ' ').Trim();
        private string GetUrlFromFile(string s) => $"{PlayListServiceUtils.localMediaBaseUrl(Uri.EscapeDataString($"{s}"))}";

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = "audio/mpegurl";
        public async Tasks.Task ListDirectoryAsync(
           MappedResourceInfo info,
           string absolutePath,
           IEnumerable<MappedResourceInfo> entries,
           Stream stream,
           CancellationToken token)
        {
            if (!info.IsDirectory)
                throw SelfCheck.Failure($"{nameof(RawM3uLister)}.{nameof(ListDirectoryAsync)} invoked with a file, not a directory.");
            try
            {
                string logo_ = PlayListServiceUtils.localMediaBaseUrl("images/logo/72", false);
                using StreamWriter sw = new(stream, new UTF8Encoding(false));
                M3U.M3UWriter.WriteHeader(sw);

                foreach (var file in entries.Where(m => m.IsFile).OrderBy(e => e.Name))
                {
                    if (string.IsNullOrWhiteSpace(file.Name) ||
                        file.Name.StartsWith(Settings.Default.ConfigVideoPrefix) ||
                        file.Name.StartsWith(Settings.Default.ConfigAudioPrefix))
                        continue;

                    Media media = new(
                        GetUrlFromFile(file.Name),
                        GetNameFromFile(Path.GetFileNameWithoutExtension(file.Name).Humanize()),
                        "M3uRoot",
                        logo_);
                    M3UWriter.WriteItem(sw, media, default);
                    await Tasks.Task.Yield();
                }
            } catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(RawM3uLister)} -> {e}"); }
        }
    }
}
