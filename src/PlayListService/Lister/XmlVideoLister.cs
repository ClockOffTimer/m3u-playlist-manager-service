﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using PlayListService.Properties;
using Swan;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class XmlVideoLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new XmlVideoLister());
        private XmlVideoLister()
        {
        }

        private string ReplaceName(string s) => s.Replace('_', ' ').Replace('-', ' ').Replace('.', ' ');
        private string ReName(string s, bool b)
        {
            string str;
            if (b)
                str = ReplaceName(s);
            else
                str = $"{ReplaceName(Path.GetFileNameWithoutExtension(s))} [{Path.GetExtension(s).Substring(1)}]";
            return WebUtility.HtmlEncode(str);
        }

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = Encoding.UTF8.WebName.WebXmlType();
        public async Tasks.Task ListDirectoryAsync(
           MappedResourceInfo info,
           string absolutePath,
           IEnumerable<MappedResourceInfo> entries,
           Stream stream,
           CancellationToken token)
        {
            if (!info.IsDirectory)
                throw SelfCheck.Failure($"{nameof(XmlVideoLister)}.{nameof(ListDirectoryAsync)} invoked with a file, not a directory.");
            try
            {
                await new XmlBaseLister("video", Settings.Default.ConfigVideoPath)
                            .XmlLister(absolutePath, entries, stream, token, ReName);
                await Tasks.Task.Yield();
            } catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(XmlVideoLister)} -> {e}"); }
        }
    }
}
