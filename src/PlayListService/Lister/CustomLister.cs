﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using EmbedIO.Files;

namespace PlayListService.Lister
{
    public static class CustomLister
    {
        public static string WebXmlType(this string s) => $"text/xml; encoding={s}";
        public static string WebEncodingType(this string s) => $"; encoding={s}";
        public static IDirectoryLister RAWM3U => RawM3uLister.Instance;
        public static IDirectoryLister Torrents => XmlTorrentLister.Instance;
        public static IDirectoryLister M3U8  => XmlM3uLister.Instance;
        public static IDirectoryLister Audio => XmlAudioLister.Instance;
        public static IDirectoryLister Video => XmlVideoLister.Instance;
        public static IDirectoryLister Html  => HtmlRootLister.Instance;
    }
}

