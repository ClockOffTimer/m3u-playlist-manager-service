﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using EmbedIO;
using EmbedIO.Files;
using PlayListService.Data;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class HtmlRootLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new HtmlRootLister());
        private HtmlRootLister()
        {
        }

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = MimeType.Html + Encoding.UTF8.WebName.WebEncodingType();
        public async Tasks.Task ListDirectoryAsync(
            MappedResourceInfo info,
            string absolutePath,
            IEnumerable<MappedResourceInfo> entries,
            Stream stream,
            CancellationToken token)
        {
            if (absolutePath.Equals("/"))
                await new WebResource().ReadAsStringAsync("Data.App.html", stream, token);
            else if (absolutePath.Equals("/favicon.ico"))
                await new WebResource().ReadAsByteAsync("Data.FavIcon.ico", stream, token);
            else
                await new WebResource().ReadAsStringAsync("Data.Redirect.html", stream, token);
            await Tasks.Task.Yield();
        }
    }
}
