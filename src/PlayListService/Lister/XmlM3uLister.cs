﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using PlayListService.Properties;
using Swan;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class XmlM3uLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new XmlM3uLister());
        private XmlM3uLister()
        {
        }
        private string replaceName(string s) => s.Replace('_', ' ').Replace('-', ' ').Replace('.', ' ');
        private string reName(string s, bool b)
        {
            string str, ext = Path.GetExtension(s);
            if (string.IsNullOrWhiteSpace(ext))
                ext = string.Empty;
            else
                ext = ext.Substring(1);
            if (b)
                str = replaceName(s);
            else if (s.StartsWith(Settings.Default.ConfigUdpPrefix))
            {
                var ss = replaceName(Path.GetFileNameWithoutExtension(s.Substring(Settings.Default.ConfigUdpPrefix.Length)));
                str = $"{ss} [UDP:{ext}]";
            }
            else if (s.StartsWith(Settings.Default.ConfigVideoPrefix))
            {
                var ss = replaceName(Path.GetFileNameWithoutExtension(s.Substring(Settings.Default.ConfigVideoPrefix.Length)));
                str = $"{ss} [VIDEO:{ext}]";
            }
            else if (s.StartsWith(Settings.Default.ConfigAudioPrefix))
            {
                var ss = replaceName(Path.GetFileNameWithoutExtension(s.Substring(Settings.Default.ConfigAudioPrefix.Length)));
                str = $"{ss} [AUDIO:{ext}]";
            }
            else
                str = $"{replaceName(Path.GetFileNameWithoutExtension(s))} [{ext}]";
            return WebUtility.HtmlEncode(str);
        }

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = Encoding.UTF8.WebName.WebXmlType();
        public async Tasks.Task ListDirectoryAsync(
           MappedResourceInfo info,
           string absolutePath,
           IEnumerable<MappedResourceInfo> entries,
           Stream stream,
           CancellationToken token)
        {
            if (!info.IsDirectory)
                throw SelfCheck.Failure($"{nameof(XmlM3uLister)}.{nameof(ListDirectoryAsync)} invoked with a file, not a directory.");
            try
            {
                await new XmlBaseLister().XmlLister(absolutePath, entries, stream, token, reName);
                await Tasks.Task.Yield();
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(XmlM3uLister)} -> {e}"); }
        }
    }
}
