﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using EmbedIO.Utilities;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class XmlBaseLister
    {
        private readonly BasePathMapping pathMap;

        public XmlBaseLister() { pathMap = default; }
        public XmlBaseLister(string tag, string path) {
            pathMap = new BasePathMapping(
                PlayListServiceUtils.localMediaBaseUrl(tag),
                PlayListServiceUtils.localMediaBaseUrl(tag),
                path);
        }

        public async Tasks.Task XmlLister(string path, IEnumerable<MappedResourceInfo> entries, Stream stream, CancellationToken token, Func<string, bool, string> fun)
        {
            try
            {
                string path_;
                try { path_ = WebUtility.UrlDecode(path); } catch { path_ = path; }
                FilesPlayList playList = new()
                {
                    Url = path_.EndsWith("/") ? path_.Substring(0, path_.Length - 1) : path_,
                    Lang = Thread.CurrentThread.CurrentCulture.Name,
                    Ip = Settings.Default.ConfigNetAddr,
                    Port = Settings.Default.ConfigNetPort,
                    VersionC = ServiceStatic.CurrentVersion,
                    VersionR = ServiceStatic.RepoVersion
                };
                playList.RootPaths.Add(new PathItem("m3u", Settings.Default.ConfigM3uPath));
                playList.RootPaths.Add(new PathItem("video", Settings.Default.ConfigVideoPath));
                playList.RootPaths.Add(new PathItem("audio", Settings.Default.ConfigAudioPath));
                playList.RootPaths.Add(new PathItem("history", ""));
                if (Settings.Default.IsConfigTorrentEnable)
                    playList.RootPaths.Add(new PathItem("torrents", Settings.Default.ConfigTorrentsPath));

                int id = 1;
                foreach (var d in entries.Where(m => m.IsDirectory).OrderBy(e => e.Name))
                {
                    if (token.IsCancellationRequested)
                        break;
                    if (d.Name.IsSystemDirectory())
                        continue;

                    string name = d.Name.Normalize(NormalizationForm.FormC);
                    playList.Dirs.Add(new FileItem(
                        id++,
                        fun(name, true),
                        name,
                        HttpDate.Format(d.LastModifiedUtc),
                        0,
                        GetDirIcon(entries, d.Path)));
                }

                playList.DirsCount = id; id = 1;
                foreach (var f in entries.Where(m => m.IsFile).OrderBy(e => e.Name))
                {
                    if (token.IsCancellationRequested)
                        break;

                    string name = f.Name.Normalize(NormalizationForm.FormC);
                    playList.Files.Add(new FileItem(
                        id++,
                        fun(name, false),
                        name,
                        HttpDate.Format(f.LastModifiedUtc),
                        f.Length,
                        GetFileIcon(entries, f.Path)));
                }
                playList.FilesCount = id;

                if (token.IsCancellationRequested)
                    return;
                playList.SerializeToStream(stream);
                await Tasks.Task.Yield();
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(XmlBaseLister)} -> {e}"); }
        }

        private string GetDirIcon(IEnumerable<MappedResourceInfo> list, string path)
        {
            if (pathMap == default)
                return string.Empty;

            int idx_ = path.LastIndexOf(' ');
            if ((idx_ > 0) && int.TryParse(path.Substring(idx_), out int n))
            {
                string img_;
                do {
                    string num = (n >= 10) ? $"{n}" : $"0{n}",
                           src = $"{path.Substring(0, idx_)}{num}-";

                    img_ = GetImageIcon(list, $"{src}thumb");
                    if (!string.IsNullOrWhiteSpace(img_)) break;
                    img_ = GetImageIcon(list, $"{src}banner");
                    if (!string.IsNullOrWhiteSpace(img_)) break;
                    img_ = GetImageIcon(list, $"{src}poster");

                } while (false);
                return img_;
            }
            return string.Empty;
        }

        private string GetFileIcon(IEnumerable<MappedResourceInfo> list, string path)
        {
            if (pathMap == default)
                return string.Empty;

            do {
                string ext = Path.GetExtension(path);
                if ((from i in BaseConstant.extVideoUri
                     where i.Equals(ext)
                     select i).FirstOrDefault() != default)
                    break;

                if ((from i in BaseConstant.extAudioUri
                     where i.Equals(ext)
                     select i).FirstOrDefault() != default)
                    break;

                return string.Empty;
            } while (false);

            return GetImageIcon(list, path);
        }

        private string GetImageIcon(IEnumerable<MappedResourceInfo> list, string path)
        {
            string name = Path.GetFileNameWithoutExtension(path);
            string val = (from i in list
                          where i.IsFile && i.Name.StartsWith(name, StringComparison.InvariantCultureIgnoreCase) &&
                            (i.Name.EndsWith(".png") || i.Name.EndsWith(".jpg") || i.Name.EndsWith(".gif"))
                          select i.Name).FirstOrDefault();
            if (string.IsNullOrEmpty(val))
                return string.Empty;
            return pathMap.GetUrlFromPath(Path.Combine(Path.GetDirectoryName(path), val));
        }
    }
}
