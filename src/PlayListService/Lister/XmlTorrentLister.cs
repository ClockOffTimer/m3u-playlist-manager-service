﻿/* Copyright (c) 2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using Swan;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class XmlTorrentLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new XmlTorrentLister());

        private string replaceName(string s) => s.Replace('_', ' ').Replace('-', ' ').Replace('.', ' ');
        private string reName(string s, bool b)
        {
            string str;
            if (b)
                str = replaceName(s);
            else
                str = $"{replaceName(Path.GetFileNameWithoutExtension(s))} [{Path.GetExtension(s).Substring(1)}]";
            return WebUtility.HtmlEncode(str);
        }

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = Encoding.UTF8.WebName.WebXmlType();
        public async Tasks.Task ListDirectoryAsync(
           MappedResourceInfo info,
           string absolutePath,
           IEnumerable<MappedResourceInfo> entries,
           Stream stream,
           CancellationToken token)
        {
            if (!info.IsDirectory)
                throw SelfCheck.Failure($"{nameof(XmlAudioLister)}.{nameof(ListDirectoryAsync)} invoked with a file, not a directory.");
            try
            {
                await new XmlBaseLister().XmlLister(absolutePath, entries, stream, token, reName);
                await Tasks.Task.Yield();
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(XmlAudioLister)} -> {e}"); }
        }

    }
}
