﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO.Files;
using PlayListService.Properties;
using Swan;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Lister
{
    public class XmlAudioLister : IDirectoryLister
    {
        private static readonly Lazy<IDirectoryLister> LazyInstance = new(() => new XmlAudioLister());
        private XmlAudioLister()
        {
        }

        private readonly bool[] IsFormat = new bool[] {
            !string.IsNullOrEmpty(Settings.Default.ConfigAudioDirFormat),
            !string.IsNullOrEmpty(Settings.Default.ConfigAudioFileFormat)
        };

        private string ReName(string s, bool b)
        {
            do
            {
                if ((b && !IsFormat[0]) || (!b && !IsFormat[1]))
                    break;
                try
                {
                    if (DateTime.TryParseExact(
                        Path.GetFileNameWithoutExtension(s),
                        b ? Settings.Default.ConfigAudioDirFormat : Settings.Default.ConfigAudioFileFormat,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                        out DateTime d))
                        return WebUtility.HtmlEncode(b ? $"{d.Day}/{d.Month}/{d.Year}" : $"{d.Hour}:{d.Minute}:{d.Second}");
                } catch { }

            } while (false);
            return WebUtility.HtmlEncode(s);
        }

        public static IDirectoryLister Instance => LazyInstance.Value;
        public string ContentType { get; } = Encoding.UTF8.WebName.WebXmlType();
        public async Tasks.Task ListDirectoryAsync(
           MappedResourceInfo info,
           string absolutePath,
           IEnumerable<MappedResourceInfo> entries,
           Stream stream,
           CancellationToken token)
        {
            if (!info.IsDirectory)
                throw SelfCheck.Failure($"{nameof(XmlAudioLister)}.{nameof(ListDirectoryAsync)} invoked with a file, not a directory.");
            try
            {
                await new XmlBaseLister("audio", Settings.Default.ConfigAudioPath)
                            .XmlLister(absolutePath, entries, stream, token, ReName);
                await Tasks.Task.Yield();
            }
            catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{nameof(XmlAudioLister)} -> {e}"); }
        }
    }
}
