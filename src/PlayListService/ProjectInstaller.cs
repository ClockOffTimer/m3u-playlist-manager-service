﻿using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;

namespace PlayListService
{
    [RunInstaller(true)]
    public partial class M3uPlayListProjectInstaller : System.Configuration.Install.Installer
    {
        public M3uPlayListProjectInstaller()
        {
            InitializeComponent();

            try
            {
                if (!EventLog.SourceExists(serviceM3uPlayListInstaller.ServiceName))
                    EventLog.CreateEventSource(serviceM3uPlayListInstaller.ServiceName, "PlayListServiceLog");
            }
            catch { }

            try
            {
                EventLogInstaller installer = FindInstaller(this.Installers);
                if (installer != null)
                    installer.Log = "PlayListServiceLog";
            }
            catch { }
        }

        private EventLogInstaller FindInstaller(InstallerCollection installers)
        {
            foreach (Installer installer in installers)
            {
                if (installer is EventLogInstaller ins)
                    return ins;

                EventLogInstaller eventLogInstaller = FindInstaller(installer.Installers);
                if (eventLogInstaller != null)
                    return eventLogInstaller;
            }
            return null;
        }
        private void ServiceM3uPlayListProccessInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            if (sender is M3uPlayListProjectInstaller si)
            {
                if (si.serviceM3uPlayListInstaller == null)
                    return;
                try
                {
                    using var sc = new System.ServiceProcess.ServiceController(si.serviceM3uPlayListInstaller.ServiceName);
                    sc.Start();

                    /// Try check this code! crush!!!!!!
                    /*
                    using (System.ServiceProcess.ServiceController sc = new
                        System.ServiceProcess.ServiceController(serviceM3uPlayListInstaller.ServiceName))
                    {
                        sc.Start();
                    }
                    */
                }
                catch { }
            }
        }
    }
}
