﻿namespace PlayListService
{
    partial class PlayListService
    {
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region конструктор компонентов
        private void InitializeComponent()
        {
            // 
            // PlayListService
            // 
            this.CanHandlePowerEvent = true;
            this.CanShutdown = true;
            this.ServiceName = "PlayListService";

        }
        #endregion
    }
}
