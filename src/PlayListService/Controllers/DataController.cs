﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Net;
using System.Text;
using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.Tags;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Controllers
{
    public sealed class DataController : WebApiController
    {
        private static readonly string[] DefaultGroupTags = new string[] { "/video/", "/audio/" };

        [Route(HttpVerb.Get, "/css")]
        public async Tasks.Task GetDashCss()
        {
            HttpContext.Response.ContentType = "text/css";
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsStringAsync("Data.App.css", stream);
        }

        [Route(HttpVerb.Get, "/js")]
        public async Tasks.Task GetDashJs()
        {
            HttpContext.Response.ContentType = "text/javascript";
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsStringAsync("Data.App.js", stream);
        }

        [Route(HttpVerb.Get, "/workboxjs")]
        public async Tasks.Task GetWorkBoxJs()
        {
            HttpContext.Response.ContentType = "text/javascript";
            HttpContext.Response.Headers.Add("Service-Worker-Allowed", "/");
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsStringAsync("Data.AppSW.js", stream);
        }

        [Route(HttpVerb.Get, "/manifest")]
        public async Tasks.Task GetManifest()
        {
            HttpContext.Response.ContentType = "text/json";
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsStringAsync("Data.WebManifest.json", stream);
        }

        [Route(HttpVerb.Get, "/images/{name}/{w}")]
        public async Tasks.Task GetImage(string name, int w)
        {
            HttpContext.Response.ContentType = "image/png";
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsByteAsync($"Data.images.{name}{w}x{w}.png", stream);
        }

        [Route(HttpVerb.Get, "/pwa/offline")]
        public async Tasks.Task GetOffLine()
        {
            HttpContext.Response.ContentType = "text/html";
            using Stream stream = HttpContext.OpenResponseStream();
            await new WebResource().ReadAsStringAsync("Data.AppOffLine.html", stream);
        }

        [Route(HttpVerb.Get, "/tag")]
        public async Tasks.Task GetTag([QueryField] string src)
        {
            if (string.IsNullOrWhiteSpace(src))
                throw HttpResponseException.Create(HttpStatusCode.BadRequest, "Empty TAG source");

            await Tasks.Task.Run(async () =>
            {
                Stream stream = default;
                try
                {
                    FileInfo file = default;
                    do
                    {
                        int tagi = -1, idx;
                        string uripart = default,
                               uri = WebUtility.UrlDecode(src)
                                        .Normalize(NormalizationForm.FormC)
                                        .Trim(new char[] { '"', ' ' });

                        if (uri.Contains("/../"))
                            throw HttpResponseException.Create(HttpStatusCode.BadRequest, "Bad TAG source path");

                        if ((idx = uri.IndexOf(DefaultGroupTags[0])) >= 0)
                        {
                            idx += DefaultGroupTags[0].Length;
                            if (idx >= uri.Length) break;
                            uripart = uri.Substring(0, idx); tagi = 1;
                        }
                        else if ((idx = uri.IndexOf(DefaultGroupTags[1])) >= 0)
                        {
                            idx += DefaultGroupTags[1].Length;
                            if (idx >= uri.Length) break;
                            uripart = uri.Substring(0, idx); tagi = 2;
                        }
                        if ((tagi < 1) || string.IsNullOrWhiteSpace(uripart))
                            break;

                        BasePathMapping map = new(
                            uripart, default,
                            (tagi == 1) ?
                                PlayListServiceUtils.GetVideoRootDirectory() :
                                PlayListServiceUtils.GetAudioRootDirectory()
                            );

                        file = map.GetFileInfoFromUrl(uri);
                        if ((file == default) || !file.Exists)
                            throw HttpResponseException.Create(HttpStatusCode.NotFound, "file not found from TAG");

                    } while (false);

                    if (file == default)
                        throw HttpResponseException.Create(HttpStatusCode.NotFound, "TAG group not found");

                    FileTags ft = new(file);
                    MediaTags mt = ft.DetailsTags();
                    try { ft.Dispose(); } catch { }
                    if (mt == default)
                        throw HttpResponseException.Create(HttpStatusCode.InternalServerError, "TAG data empty");
                    if (!string.IsNullOrWhiteSpace(mt.Error))
                        throw HttpResponseException.Create(HttpStatusCode.BadRequest, mt.Error);

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    mt.SerializeToStream(stream);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, $"Web request: /data/tag/?src={src} ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }

        [Route(HttpVerb.Get, "/dlna/list")]
        public async Tasks.Task CommandDlnaList()
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    if (!Settings.Default.IsConfigDlnaEnable)
                        throw HttpResponseException.Create(HttpStatusCode.NotImplemented, "DLNA disabled by configuration");

                    string r = ServiceStatic.DlnaListXmlDevices().Trim();
                    if (string.IsNullOrEmpty(r))
                        r = $"{HttpErrorToXml.DefaultXmlHeader()}{HttpErrorToXml.DefaultXmlFooter()}";

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    using StreamWriter sw = new(stream, new UTF8Encoding(false));
                    await sw.WriteAsync(r);
                }
                catch (Exception e) {
                    HttpContext.WebHttpError(e, $"Web request: /data/dlna/list ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }
        [Route(HttpVerb.Get, "/dlna/select/{id}")]
        public void CommandDlnaSelect(int id)
        {
            ServiceStatic.GetDlna().SelectDevice(id);
        }
        [Route(HttpVerb.Get, "/dlna/play/{url}/{title}")]
        public void CommandDlnaPlay(string url, string title)
        {
            ServiceStatic.GetDlna().PlayToDevice(url, title);
        }
        [Route(HttpVerb.Get, "/dlna/pause/{id}")]
        public void CommandDlnaPause(int id)
        {
            ServiceStatic.GetDlna().PauseToDevice(id);
        }
        [Route(HttpVerb.Get, "/dlna/stop/{id}")]
        public void CommandDlnaStop(int id)
        {
            ServiceStatic.GetDlna().StopToDevice(id);
        }

    }
}
