﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Net;
using EmbedIO;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Controllers
{
    public class HttpResponseException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }  = HttpStatusCode.NotImplemented;
        private HttpResponseException(string s, Exception e, HttpStatusCode c) : base(s, e) { StatusCode = c; }
        public static HttpResponseException Create(HttpStatusCode c, Exception e) =>
            new(default, e, c);
        public static HttpResponseException Create(HttpStatusCode c, string s) =>
            new(s, default, c);
        public static HttpResponseException Create(HttpStatusCode c, Exception e, string s) =>
            new(s, e, c);
    }

    public static class HttpErrorToXml
    {
        public static string DefaultXmlHeader() => $"<? xml version = \"1.0\" encoding=\"utf-8\"?>\r\n<root xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n\t<status>\r\n";
        public static string DefaultXmlFooter() => "\t</status>\r\n</root>";
        public static string DefaultXmlDateTimeFooter(this DateTime d) => $"\t\t<update>{d:dd/MM/yyyy HH:mm}</update>\r\n\t</status>\r\n</root>";

        public static async Tasks.Task HttpError(this IHttpContext context, Exception ex, HttpStatusCode code = HttpStatusCode.InternalServerError, Stream s = default)
        {
            Stream stream = default;
            try
            {
                BaseExceptionXml exceptionXml;
                if (ex == default)
                    exceptionXml = new BaseExceptionXml {
                        Message = code.ToString()
                    };
                else
                    exceptionXml = new BaseExceptionXml(ex);

                try
                {
                    context.Response.ContentType = "text/xml";
                    context.Response.StatusCode = exceptionXml.Code = (int)code;
                    Stream ss;
                    if (s == default)
                        ss = stream = context.OpenResponseStream();
                    else
                        ss = s;

                    exceptionXml.SerializeToStream(ss);
                    await Tasks.Task.Yield();

                    if (code != HttpStatusCode.OK)
                        ServiceStatic.GetLog().Error.Write($"Web Error ->", ex);
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"HttpError -> Internal exceptiion ->", e).WriteStackTrace(); }
                await Tasks.Task.Yield();
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Error.Write($"HttpError ->", e).WriteStackTrace();
                throw new EmbedIO.HttpException(HttpStatusCode.InternalServerError, e.Message);
            }
            finally
            {
                if (stream != default)
                    try { stream.Dispose(); } catch { }
            }
        }

        public async static void WebHttpError(this EmbedIO.IHttpContext context, Exception e, string s = default, Stream stream = default)
        {
            try
            {
                ServiceStatic.GetLog().Error.Write(s, e).WriteStackTrace();
                if (e is EmbedIO.HttpException ex)
                    await context.HttpError(ex, (HttpStatusCode)ex.StatusCode, stream);
                else if (e is HttpResponseException exr)
                    await context.HttpError(exr, exr.StatusCode, stream);
                else
                    await context.HttpError(e, HttpStatusCode.InternalServerError, stream);

            } catch (Exception exc) { ServiceStatic.GetLog().Error.Write(nameof(WebHttpError), exc); }
        }
    }
}
