﻿/* Copyright (c) 2021-2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using HttpMultipartParser;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.Process;
using PlayListServiceData.RunManager;
using PlayListServiceTorrent;
using PlayListServiceTorrent.Data;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Controllers
{
    public class CmdController : WebApiController
    {
        public enum UploadType : int
        {
            AUTO = 0,
            PLIST = 1,
            VIDEO = 2,
            AUDIO = 3,
            TORRENT = 4,
            ZIP = 5,
            GZ = 6,
            M3U = 7
        };

        private static readonly char[] InvalidPathChars = Path.GetInvalidFileNameChars();
        private readonly string m3uFmt = "{0:yyyy-MM-dd_HH-mm-ss}-{1}";
        private readonly string zipFmt = "{0:yyyy-MM-dd_HH-mm-ss}-{1}-{2}";
        private readonly DirectoryWatch instance;

        public CmdController(DirectoryWatch dw)
        {
            instance = dw;
            Torrent.SavePath = Settings.Default.ConfigTorrentsPath;
        }

        #region LOGIN
        [Route(HttpVerb.Get, "/login")]
        public string LoginConfig()
        {
            HttpContext.Response.ContentType = "text/plain";
            if (Thread.CurrentThread.CurrentCulture.Name.Contains("RU"))
                return "Аутентификация прошла успешно. Теперь вы можете использовать расширенные команды и управлять сервисом.";
            return "Authentication Ok. Now you can use advanced commands and manage the service.";
        }

        [Route(HttpVerb.Get, "/unlogin")]
        public void UnLoginConfig()
        {
            throw HttpException.Unauthorized("Missing Credentials");
        }
        #endregion

        #region TASKS
        [Route(HttpVerb.Get, "/command/tasks")]
        public async Tasks.Task SheduleGetTask()
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    SheduleManager shedule = ServiceStatic.GetSheduleManager();
                    if (shedule == default)
                        throw HttpResponseException.Create(HttpStatusCode.ServiceUnavailable, "Shedule manager is null");

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    shedule.GetAllTasks().SerializeToStream(stream);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /command/tasks GET ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }

        [Route(HttpVerb.Post, "/command/tasks")]
        public async Tasks.Task ShedulePostTask()
        {
            await Tasks.Task.Run(async () => {
                try
                {
                    SheduleManager shedule = ServiceStatic.GetSheduleManager();
                    if (shedule == default)
                        throw HttpResponseException.Create(HttpStatusCode.ServiceUnavailable, "Shedule manager is null");

                    using MemoryStream ms = await HttpContext.GetRequestBodyAsMemoryStreamAsync();
                    if (ms == default)
                        throw HttpResponseException.Create(HttpStatusCode.BadRequest, "post data is empy");

                    SheduleItemsLite sil = ms.DeserializeFromStream<SheduleItemsLite>();
                    if (sil != default)
                        shedule.SetAllTasks(sil);

                    await HttpContext.HttpError(default, HttpStatusCode.OK);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /command/tasks POST ->");
                }
            });
        }

        [Route(HttpVerb.Get, "/command/tasks/stat")]
        public async Tasks.Task GetProcessStatistic()
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    ProcessStatsXml stat = ServiceStatic.GetStatsSerializable();
                    if (stat == default)
                        throw HttpResponseException.Create(HttpStatusCode.ServiceUnavailable, "Process statistic empty");

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    stat.SerializeToStream(stream);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, "Web request: /data/stat ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }
        #endregion

        #region COMMANDS
        [Route(HttpVerb.Get, "/command/{icmd}")]
        public void CommandConfig(int icmd)
        {
            try
            {
                HttpContext.Response.ContentType = "text/plain";
                DirectoryWatch.ProcessId cmd = (DirectoryWatch.ProcessId)icmd;
                switch (cmd)
                {
                    case DirectoryWatch.ProcessId.ID_SCAN:
                    case DirectoryWatch.ProcessId.ID_RENEW_EPG:
                    case DirectoryWatch.ProcessId.ID_SCAN_LOCAL_VIDEO:
                    case DirectoryWatch.ProcessId.ID_STAGE_MOVE:
                    case DirectoryWatch.ProcessId.ID_CHECK_MEDIA:
                    case DirectoryWatch.ProcessId.ID_CONFIG_RELOAD:
                        {
                            ServiceCommands(cmd);
                            break;
                        }
                    default: break;
                }
                ServiceStatic.GetLog().Info.Write($"Web command call: /cmd/command -> {icmd}/{cmd}");
            }
            catch (Exception e) {
                HttpContext.WebHttpError(e, $"Web command: /cmd/command = {icmd} ->");
            }
        }
        #endregion

        #region UPLOAD
        [Route(HttpVerb.Post, "/upload")]
        public async Tasks.Task UploadFiles()
        {
            Stream stream = default;
            try
            {
                UploadType utype = UploadType.AUTO;
                HttpContext.Response.ContentType = "text/plain";
                MultipartFormDataParser parser = await MultipartFormDataParser.ParseAsync(Request.InputStream);
                try {
                    string target = GetFromMultipartForm(parser, "target");
                    if (string.IsNullOrWhiteSpace(target))
                        utype = UploadType.AUTO;
                    else
                        utype = (UploadType)Enum.Parse(typeof(UploadType), target);
                } catch { utype = UploadType.AUTO; }

                string path = GetFromMultipartForm(parser, "path");
                List<string> errors = new();
                foreach (var f in parser.Files)
                {
                    if ((f == null) || (f.Data == null) || (f.FileName == null))
                        continue;

                    ActionFile(utype, f, path, errors);
                }
                if (errors.Count > 0)
                {
                    UTF8Encoding utf8 = new(false);
                    stream = HttpContext.OpenResponseStream();
                    using TextWriter tw = new StreamWriter(stream, utf8);
                    tw.WriteLine(
                        string.Join($"{Environment.NewLine}", errors.Select(s => $"{s}<br/>").ToArray())
                        );
                    await Tasks.Task.Yield();
                }
            }
            catch (Exception e)
            {
                HttpContext.WebHttpError(e, "Web command: /cmd/upload ->", stream);
            }
            finally
            {
                if (stream != default)
                    try { stream.Dispose(); } catch { }
            }
        }
        #endregion

        #region TORRENTS
        [Route(HttpVerb.Post, "/torrent/new")]
        public async Tasks.Task CreateTorrentFile()
        {
            try
            {
                if (!Settings.Default.IsConfigTorrentEnable)
                    throw HttpResponseException.Create(HttpStatusCode.ServiceUnavailable, "creation of torrent files is disabled in the settings");
                if (string.IsNullOrWhiteSpace(Settings.Default.ConfigTorrentsPath))
                    throw HttpResponseException.Create(HttpStatusCode.ServiceUnavailable, "directory for saving torrents is not filled in the configuration");


                MultipartFormDataParser parser = await MultipartFormDataParser.ParseAsync(Request.InputStream);
                string mpPart = GetFromMultipartForm(parser, "path");
                if (string.IsNullOrWhiteSpace(mpPart))
                    throw HttpResponseException.Create(HttpStatusCode.BadRequest, "object not selected directory or file to create torrent file");

                UploadType utype = UploadType.AUTO;
                string mpPath = string.Empty;
                try
                {
                    mpPath = WebUtility.UrlDecode(mpPart);
                    if (string.IsNullOrWhiteSpace(mpPath))
                        throw HttpResponseException.Create(HttpStatusCode.BadRequest, "object directory bad encoding or empty");
                    int idx = mpPart.IndexOf('/');
                    if (idx < 0)
                        throw HttpResponseException.Create(HttpStatusCode.BadRequest, "wrong object directory group sequence");
                    utype = (UploadType)Enum.Parse(typeof(UploadType), mpPath.Substring(0, idx).ToUpperInvariant());

                } catch { throw; }

                switch (utype)
                {
                    case UploadType.VIDEO:
                    case UploadType.AUDIO:
                    case UploadType.M3U: break;
                    default:
                        throw HttpResponseException.Create(HttpStatusCode.BadRequest, "path is not a group for video, audio or M3U");
                }

                await Tasks.Task.Yield();

                string mpMask = GetFromMultipartForm(parser, "ext");
                string mpMode = GetFromMultipartForm(parser, "torrentselectdir");
                string mpTrackers = GetFromMultipartForm(parser, "trackers");

                await Tasks.Task.Run(async () =>
                {
                    Stream stream = default;
                    try
                    {
                        bool IsAllDirectory = false;
                        if (int.TryParse(mpMode, out int m))
                            IsAllDirectory = m == 2;

                        string path = mpPath.Substring(utype.ToString().Length + 1).Replace('/', '\\');
                        string mask = string.IsNullOrWhiteSpace(mpMask) ? "*.*" : mpMask;
                        List<string> trackers = new();
                        try {
                            do {
                                string[] ss;

                                if (string.IsNullOrWhiteSpace(mpTrackers))
                                {
                                    if (Settings.Default.ConfigTorrentTrackers.Count == 0)
                                        break;

                                    ss = new string[Settings.Default.ConfigTorrentTrackers.Count];
                                    Settings.Default.ConfigTorrentTrackers.CopyTo(ss, 0);
                                    trackers.AddRange(ss);
                                    break;
                                }

                                ss = ParseStringToArray(' ', mpTrackers);
                                if ((ss != null) && (ss.Length > 0))
                                {
                                    trackers.AddRange(ss);
                                    break;
                                }
                                ss = ParseStringToArray(';', mpTrackers);
                                if ((ss != null) && (ss.Length > 0))
                                {
                                    trackers.AddRange(ss);
                                    break;
                                }
                                ss = ParseStringToArray(',', mpTrackers);
                                if ((ss != null) && (ss.Length > 0))
                                {
                                    trackers.AddRange(ss);
                                    break;
                                }
                                trackers.Add(mpTrackers.Trim());

                            } while (false);
                        }
                        catch { }

                        Torrent tr = default;
                        string target = utype switch
                        {
                            UploadType.VIDEO => Path.Combine(PlayListServiceUtils.GetVideoRootDirectory(), path),
                            UploadType.AUDIO => Path.Combine(PlayListServiceUtils.GetAudioRootDirectory(), path),
                            UploadType.M3U   => Path.Combine(PlayListServiceUtils.GetM3uRootDirectory(), path),
                            _ => string.Empty
                        };
                        if (string.IsNullOrWhiteSpace(target))
                            throw new DirectoryNotFoundException(utype.ToString());
                        do
                        {
                            DirectoryInfo dir = new(target);
                            if ((dir != default) && dir.Exists)
                            {
                                tr = TorrentExtension.CreateFromDirectory(
                                    target, trackers, mask,
                                    IsAllDirectory ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                                break;
                            }
                            FileInfo file = new(target);
                            if ((file == default) || !file.Exists)
                            {
                                tr = TorrentExtension.CreateFromFile(target, trackers);
                                break;
                            }
                            throw HttpResponseException.Create(HttpStatusCode.NotFound, "file or directory not found");
                        } while (false);

                        await Tasks.Task.Yield();

                        if (tr != null)
                        {
                            tr.Save(Settings.Default.ConfigTorrentsPath);
                            TorrentWebResponse trsp = new TorrentWebResponse().Copy(tr);
                            do
                            {
                                var web = ServiceStatic.GetWebServer();
                                if (web == default)
                                    break;

                                string host = web.Host.HTTPS.ToString();
                                if (string.IsNullOrWhiteSpace(host))
                                    host = web.Host.HTTP.ToString();

                                if (string.IsNullOrWhiteSpace(host))
                                    break;

                                trsp.TorrentUrl = $"{host}torrents/{Path.GetFileName(tr.SavedTorrentPath)}";

                            } while (false);

                            Encoding utf = tr.UtfEncoding;
                            HttpContext.Response.ContentType = "text/xml";
                            stream = HttpContext.OpenResponseStream();
                            trsp.SerializeToStream(stream, utf);
                            await Tasks.Task.Yield();
                            try {
                                File.WriteAllText(trsp.MagnetPath, trsp.MagnetLink, utf);
                            } catch { }
                            await Tasks.Task.Yield();
                            return;
                        }
                        throw HttpResponseException.Create(HttpStatusCode.InternalServerError, "not created torrent file");
                    }
                    catch (Exception e) {
                        HttpContext.WebHttpError(e, "Web command: /cmd/torrent/new ->", stream);
                    }
                    finally
                    {
                        if (stream != default)
                            try { stream.Dispose(); } catch { }
                    }
                });
            }
            catch (Exception e)
            {
                HttpContext.WebHttpError(e, "Web command: /cmd/torrent/new ->");
            }
        }
        #endregion

        #region Implement
        private async void ServiceCommands(DirectoryWatch.ProcessId id)
        {
            await Tasks.Task.Run(() => {
                try
                {
                    instance.CustomCommand = (int)id;
                }
                catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Web command: {id} ->", e).WriteStackTrace(); }
            });
        }

        private async void FileAsZipArchive(FilePart f, List<string> errors)
        {
            try
            {
                var d = PlayListServiceUtils.CreateM3uDirectory("new");
                if (d == null)
                    return;

                using ZipArchive arch = new(f.Data);
                foreach (ZipArchiveEntry entry in arch.Entries)
                {
                    if (M3U.M3UWriter.IsM3uXFileName(entry.FullName))
                    {
                        string path = Path.Combine(
                            d.FullName,
                            string.Format(zipFmt, DateTime.Now, Path.GetFileNameWithoutExtension(f.FileName), entry.FullName));

                        if (path.IndexOfAny(InvalidPathChars) >= 0)
                            continue;

                        using FileStream ofs = new(path, FileMode.Create, FileAccess.Write, FileShare.Read);
                        await entry.Open().CopyToAsync(ofs);
                    }
                }
            }
            catch (Exception e) {
                string s = $"Upload File: file as ZIP archive, {e.Message}";
                ServiceStatic.GetLog().Error.Write(s, e).WriteStackTrace();
                errors.Add(s);
            }
        }

        private async void FileAsGzArchive(FilePart f, List<string> errors)
        {
            try
            {
                var d = PlayListServiceUtils.CreateM3uDirectory("new");
                if (d == null)
                    return;

                FileInfo file = new(
                    Path.Combine(
                        d.FullName,
                        Path.GetFileNameWithoutExtension(f.FileName)));
                if (file == default)
                    return;

                if (file.Exists)
                    file.Delete();

                using Stream arch = new GZipStream(f.Data, CompressionMode.Decompress);
                using FileStream ofs = new(file.FullName, FileMode.Create, FileAccess.Write, FileShare.Read);
                await arch.CopyToAsync(ofs);
            }
            catch (Exception e) {
                string s = $"Upload File: file as GZ archive, {e.Message}";
                ServiceStatic.GetLog().Error.Write(s, e).WriteStackTrace();
                errors.Add(s);
            }
        }

        private async void FileAsBinary(FilePart f, UploadType ut, string append, List<string> errors)
        {
            try
            {
                FileInfo file;
                string basepath = ut switch
                {
                    UploadType.VIDEO => Settings.Default.ConfigVideoPath,
                    UploadType.AUDIO => Settings.Default.ConfigAudioPath,
                    UploadType.TORRENT => Settings.Default.ConfigTorrentsPath,
                    _ => string.Empty
                };
                if (string.IsNullOrWhiteSpace(basepath))
                    return;
                if (string.IsNullOrWhiteSpace(append))
                    file = new FileInfo(
                        Path.Combine(
                            basepath,
                            Path.GetFileName(f.FileName)));
                else
                    file = new FileInfo(
                        Path.Combine(
                            basepath,
                            append,
                            Path.GetFileName(f.FileName)));

                if (file == null)
                    return;
                if (file.Exists)
                {
                    string s = $"Upload File: file exists {file.FullName}, abort..";
                    ServiceStatic.GetLog().Warning.Write(s);
                    errors.Add(s);
                    return;
                }
                Directory.CreateDirectory(Path.GetDirectoryName(file.FullName));
                using FileStream ofs = new(file.FullName, FileMode.Create, FileAccess.Write, FileShare.Read);
                await f.Data.CopyToAsync(ofs);
            }
            catch (Exception e) {
                string s = $"Upload File: file as binary, {e.Message}";
                ServiceStatic.GetLog().Error.Write(s, e).WriteStackTrace();
                errors.Add(s);
            }
        }

        private async void FileAsMedia(FilePart f, List<string> errors)
        {
            try
            {
                var d = PlayListServiceUtils.CreateM3uDirectory("new");
                if (d == null)
                    return;

                string path = Path.Combine(
                    d.FullName,
                    string.Format(m3uFmt, DateTime.Now, f.FileName));
                using FileStream ofs = new(path, FileMode.Create, FileAccess.Write, FileShare.Read);
                await f.Data.CopyToAsync(ofs);
            }
            catch (Exception e) {
                string s = $"Upload File: file as media, {e.Message}";
                ServiceStatic.GetLog().Error.Write(s, e).WriteStackTrace();
                errors.Add(s);
            }
        }

        private UploadType GetFileType(string ext, string mime)
        {
            if (!string.IsNullOrWhiteSpace(mime))
            {
                if (mime.StartsWith("video/")) return UploadType.VIDEO;
                if (mime.StartsWith("audio/")) return UploadType.AUDIO;
                if (mime.Equals("application/zip", StringComparison.InvariantCultureIgnoreCase)) return UploadType.ZIP;
                if (mime.Equals("application/gzip", StringComparison.InvariantCultureIgnoreCase)) return UploadType.GZ;
                if ((from i in BaseConstant.videoMimes
                     where i.Equals(mime)
                     select i).FirstOrDefault() != default)
                        return UploadType.VIDEO;
            }
            if (ext.EndsWith("zip", StringComparison.InvariantCultureIgnoreCase))
                return UploadType.ZIP;
            if (ext.EndsWith("gz", StringComparison.InvariantCultureIgnoreCase))
                return UploadType.GZ;
            if (ext.EndsWith("torrent", StringComparison.InvariantCultureIgnoreCase))
                return UploadType.TORRENT;

            bool b;
            b = (from i in BaseConstant.extM3uUri
                 where i.Equals(ext)
                 select i).FirstOrDefault() != default;
            if (b)
                return UploadType.PLIST;

            b = (from i in BaseConstant.extAudioUri
                 where i.Equals(ext)
                 select i).FirstOrDefault() != default;
            if (b)
                return UploadType.AUDIO;

            b = (from i in BaseConstant.extVideoUri
                 where i.Equals(ext)
                 select i).FirstOrDefault() != default;
            if (b)
                return UploadType.VIDEO;
            return UploadType.AUTO;
        }

        private void ActionFile(UploadType ut, FilePart file, string path, List<string> errors)
        {
            UploadType ft = GetFileType(Path.GetExtension(file.FileName), file.ContentType);
            UploadType t = (ut == UploadType.AUTO) ? ft : ((ut == UploadType.PLIST) ? ft : ut);

            switch (t)
            {
                case UploadType.AUTO:
                    {
                        string s = $"Upload File: file {file.FileName} not '{t}' category type, abort..";
                        ServiceStatic.GetLog().Warning.Write(s);
                        errors.Add(s);
                        break;
                    }
                case UploadType.ZIP:
                    {
                        FileAsZipArchive(file, errors);
                        break;
                    }
                case UploadType.GZ:
                    {
                        FileAsGzArchive(file, errors);
                        break;
                    }
                case UploadType.PLIST:
                    {
                        FileAsMedia(file, errors);
                        break;
                    }
                case UploadType.TORRENT:
                    {
                        FileAsBinary(file, t, string.Empty, errors);
                        break;
                    }
                case UploadType.AUDIO:
                case UploadType.VIDEO:
                    {
                        if ((ft == UploadType.ZIP) || (ft == UploadType.GZ))
                        {
                            string s = $"Upload File: file {file.FileName} not allowed to '{t}', abort..";
                            ServiceStatic.GetLog().Warning.Write(s);
                            errors.Add(s);
                            break;
                        }
                        FileAsBinary(file, t, path, errors);
                        break;
                    }
            }
        }
        private string GetFromMultipartForm(MultipartFormDataParser parser, string tag)
        {
            try {
                return (from i in parser.Parameters
                        where i.Name.Equals(tag)
                        select i.Data).FirstOrDefault();
            } catch { return string.Empty; }

        }
        private string [] ParseStringToArray(char c, string s)
        {
            try {
                string [] ss = s.Split(new char[] { c }, StringSplitOptions.RemoveEmptyEntries);
                if ((ss != null) && (ss.Length > 0)) {
                    for (int i = ss.Length - 1; i >= 0; i--) ss[i] = ss[i].Trim();
                    return ss;
                }

            } catch {}
            return null;
        }
        #endregion
    }
}
