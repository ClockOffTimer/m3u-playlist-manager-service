﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using EmbedIO;
using PlayListService.Data;
using PlayListServiceData.BaseXml;
using PlayListServiceData.Process.Log;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Controllers
{
    public class HistoryModule : WebModuleBase
    {
        public HistoryModule(string uri) : base(uri) { }
        public override bool IsFinalHandler => true;

        protected override async Tasks.Task OnRequestAsync(IHttpContext context)
        {
            await Tasks.Task.Run(() =>
            {
                Stream stream = default;
                try
                {
                    FilesPlayList fpl = ServiceStatic.GetHistory().Get();
                    if (fpl == default)
                        throw HttpException.InternalServerError();

                    context.Response.ContentType = "text/xml";
                    stream = context.OpenResponseStream();
                    fpl.SerializeToStream(stream);
                }
                catch (Exception e)
                {
                    e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e);
                    throw HttpException.InternalServerError(e.Message);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }
    }
}
