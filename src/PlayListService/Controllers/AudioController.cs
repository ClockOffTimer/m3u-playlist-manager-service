﻿/* Copyright (c) 2021-2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define MODIFY_EMBEDIO
#define MODIFY_EMBEDIO_MANUAL_SET

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Serialization;
using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using PlayListService.Properties;
using PlayListServiceData.Utils;
using PlayListServiceStream;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;
using Swan.Logging;
using Tasks = System.Threading.Tasks;
using PlayListServiceData.BaseXml;

#if DEBUG
using PlayListServiceData.Process.Log;
#endif

namespace PlayListService.Controllers
{
    public sealed class AudioController : WebApiController
    {
        [Route(HttpVerb.Get, "/audio/settings")]
        public async Tasks.Task GetAudioSettingsList()
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    AudioStreamManagerService asm = GetAudioStreamManagerService(-1, false);
                    AudioSettingsLite asl = asm.GetChannelsSettings();
                    if (asl == default)
                        throw new HttpException(HttpStatusCode.InternalServerError, $"list of audio channels is empty.");

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    asl.SerializeToStream(stream);
                    await Tasks.Task.Yield();
                }
                catch (Exception e) {
                    HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /stream/audio/settings GET ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }

        [Route(HttpVerb.Get, "/audio/settings/stat")]
        public async Tasks.Task GetAudioSettingsStat()
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    AudioStreamManagerService asm = GetAudioStreamManagerService(-1, false);
                    AudioSettingsLite asl = asm.GetChannelsSettings();


                    List<Tuple<string, List<string>>> list = ServiceStatic.GetAudioCaptureStat();
                    if ((list == default) || (list.Count == 0))
                        throw new HttpException(HttpStatusCode.InternalServerError, "list of audio channels is empty.");

                    AudioStatsLite data = new();
                    data.AddRange(list);

                    if (data.Count == 0)
                        throw new HttpException(HttpStatusCode.InternalServerError, "building status list failed, input audio channels is empty.");

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    data.SerializeToStream(stream);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /stream/audio/settings GET ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }

        [Route(HttpVerb.Get, "/audio/settings/{idx}")]
        public async Tasks.Task GetAudioSettings(int idx)
        {
            await Tasks.Task.Run(async () => {
                Stream stream = default;
                try
                {
                    AudioStreamManagerService asm = GetAudioStreamManagerService(idx);
                    AudioChannelLite acl = asm.GetChannelSettings(idx);
                    if (acl == default)
                        throw new EmbedIO.HttpException(HttpStatusCode.InternalServerError, $"audio channel {idx} not found.");

                    HttpContext.Response.ContentType = "text/xml";
                    stream = HttpContext.OpenResponseStream();
                    using TextWriter tw = new StreamWriter(stream, new UTF8Encoding(false));
                    XmlSerializer xml = new(typeof(AudioChannelLite));
                    xml.Serialize(tw, acl);
                    await Tasks.Task.Yield();
                }
                catch (Exception e)
                {
                    HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /stream/audio/settings/{idx} GET ->", stream);
                }
                finally
                {
                    if (stream != default)
                        try { stream.Dispose(); } catch { }
                }
            });
        }

        [Route(HttpVerb.Post, "/audio/settings/{idx}")]
        public async Tasks.Task PostAudioSettings(int idx)
        {
            try
            {
                AudioStreamManagerService asm = GetAudioStreamManagerService(idx);
                AudioChannelLite acl =
                    await HttpContext.GetRequestDataAsync<AudioChannelLite>(RequestDeserializerCb<AudioChannelLite>);
                if (acl == default)
                    throw new EmbedIO.HttpException(HttpStatusCode.BadRequest, "Audio channel is null");
                asm.SetChannelSettings(idx, acl);
                await HttpContext.HttpError(default, HttpStatusCode.OK);
            }
            catch (ObjectDisposedException e) {
                await HttpContext.HttpError(e, HttpStatusCode.InternalServerError);
            }
            catch (Exception e) {
                HttpContext.WebHttpError(e, $"Web request from {HttpContext.RemoteEndPoint}: /stream/audio/settings/{idx} POST ->");
            }
        }

        #region Audio stream DEBUG print
        private async Tasks.Task AudioStreamDebugPrint_(char t, IPEndPoint ip, string s, Exception e = default)
        {
            await Tasks.Task.Run(() =>
            {
                try {
                    string text;
                    if (e == default)
                        text = $"{nameof(GetAudioStream)}/{ip} - {s}";
                    else
                        text = $"{nameof(GetAudioStream)}/{ip} - {s} -> {e.Message}";

                    Logger.Info(text);
#                   if DEBUG
                    if (e == default)
                        text.WriteLog(t, ServiceStatic.GetLog());
                    else
                        text.WriteLog(t, ServiceStatic.GetLog()).WriteStackTrace(e);
#                   endif
                } catch { }
            });
        }
        private async Tasks.Task AudioStatisticDebugPrint_()
        {
            await Tasks.Task.Run(() =>
            {
                try
                {
                    List<Tuple<string, List<string>>> list = ServiceStatic.GetAudioCaptureStat();
                    if ((list == default) || (list.Count == 0))
                        return;

                    StringBuilder sb = new($"Audio broadcast statistic:{Environment.NewLine}");
                    for (int i = 0; i < list.Count; i++)
                    {
                        Tuple<string, List<string>> t = list[i];
                        sb.Append($"\t[{i}] '{t.Item1}': ");
                        if ((t.Item2 == default) || (t.Item2.Count == 0))
                            sb.AppendLine($"Not active clients..");
                        else
                            sb.AppendLine($"{string.Join<string>(", ", t.Item2)}");
                    }
                    string text = sb.ToString();
                    Logger.Info(text);
#                   if DEBUG
                    text.WriteLog('v', ServiceStatic.GetLog());
#                   endif
                }
                catch { }
            });
        }
        #endregion

        [Route(HttpVerb.Get, "/audio/{idx}")]
        public async Tasks.Task GetAudioStream(int idx)
        {
            int tid = -1;
            Stream stream = default;
            AudioStreamReader reader = default;
            AudioStreamClient client = default;
            CancellationTokenSafe token = new();
            try
            {
                await AudioStatisticDebugPrint_();
                await AudioStreamDebugPrint_('i', HttpContext.RemoteEndPoint, $"Begin stream, channel {idx}");

                AudioStreamManagerService asm = GetAudioStreamManagerService(idx);
                reader = asm.GetAudioReader(idx);
                if (reader == default)
                    throw new HttpException(HttpStatusCode.InternalServerError, $"audio channel {idx} to {HttpContext.RemoteEndPoint} is null.");

                reader.ReadTimeout = Settings.Default.ConfigAudioReadTimeout;
                _ = reader.ChannelOutFormat switch
                {
                    AudioStreamFormat.Aac => HttpContext.Response.ContentType = "audio/aac",
                    AudioStreamFormat.Mp3 => HttpContext.Response.ContentType = "audio/mpeg",
                    AudioStreamFormat.Wma => HttpContext.Response.ContentType = "audio/x-ms-wma",
                    AudioStreamFormat.Wav => HttpContext.Response.ContentType = "audio/vnd.wave",
                    AudioStreamFormat.None => throw new NotImplementedException(),
                    _ => HttpContext.Response.ContentType = "audio/vnd.wave"
                };

                HttpContext.OnClose(async (a) =>
                {
                    token?.Cancel();
                    client?.Disable();
                    await AudioStreamDebugPrint_('i', HttpContext.RemoteEndPoint, $"OnClose, Http audio channel {idx} close");
                });

#               if MODIFY_EMBEDIO_MANUAL_SET
                stream = SetStream(HttpContext.OpenResponseStreamAllowException(false));
#               elif MODIFY_EMBEDIO
                stream = HttpContext.OpenResponseStreamAllowException(false);
#               else
                stream = HttpContext.OpenResponseStream();
#               endif
                client = reader.AddClient(HttpContext.RemoteEndPoint, async (a) => {
                    try
                    {
                        if (token.IsCancellationRequested || HttpContext.CancellationToken.IsCancellationRequested)
                            throw new OperationCanceledException($"{nameof(GetAudioStream)} -> On Close");
                        if (!stream.CanWrite)
                            throw new InvalidDataException($"{nameof(GetAudioStream)} -> Can Write");
                        stream.Write(a, 0, a.Length);
                    }
                    catch (IOException) { token.Cancel(); }
                    catch (ObjectDisposedException) { token.Cancel(); }
                    catch (OperationCanceledException)
                    {
                        if (HttpContext.CancellationToken.IsCancellationRequested)
                            token.Cancel();
                    }
                    catch (Exception e)
                    {
                        token.Cancel();
                        await AudioStreamDebugPrint_('i', HttpContext.RemoteEndPoint, $"Client CB, channel {idx}", e);
                    }
                });
                tid = client.Tid;
                using WaitHandle wait = token.SafeToken.WaitHandle;
                if (wait == default)
                    throw new NullReferenceException(nameof(WaitHandle));
                await Tasks.Task.FromResult(wait.WaitOne());
            }
            catch (ObjectDisposedException) { }
            catch (Exception e)
            {
                await AudioStreamDebugPrint_('!', HttpContext.RemoteEndPoint, $"Client MAIN, channel {idx}", e);
            }
            finally
            {
                RemoveStream(reader, client, tid);
                await AudioStreamDebugPrint_('i', HttpContext.RemoteEndPoint, $"End stream, channel: {idx}");
                try { token.Dispose(); } catch { }
                if (stream != default)
                    try { stream.Dispose(); } catch { }
            }
        }

        private void RemoveStream(AudioStreamReader ac, AudioStreamClient client, int id)
        {
            if (ac == default)
                return;
            if ((client != default) && (client.Tid > 0))
                ac.RemoveClient(client);
            else if (id > 0)
                ac.RemoveClient(id);
        }

#       if MODIFY_EMBEDIO_MANUAL_SET
        private static Stream SetStream(Stream s)
        {
#           if MODIFY_EMBEDIO
            if (s is EmbedIO.Net.ResponseStream stream)
                stream.StreamIgnoreErrors = false;
            else
                Debug.WriteLine("!!! Warning !!! uncnown type of Stream! (NOT EmbedIO.Net.ResponseStream)");
#           endif
            return s;
        }
#       endif

        private AudioStreamManagerService GetAudioStreamManagerService(int idx, bool b = true)
        {
            AudioStreamManagerService asm = ServiceStatic.GetAudioCapture();
            if (asm == default)
                throw new EmbedIO.HttpException(HttpStatusCode.InternalServerError, $"audio capture device {idx} is null.");

            if (!asm.IsLoading)
            {
                CancellationTokenSafe wtoken = default;
                try {
                    wtoken = new (TimeSpan.FromSeconds(5));
                    while (!asm.IsLoading)
                    {
                        if (wtoken.SafeToken.IsCancellationRequested)
                            break;
                        Tasks.Task.Delay(500, wtoken.Token);
                    }
                }
                catch { }
                finally
                {
                    if (wtoken != default)
                        wtoken.Dispose();
                }
            }
            if ((!asm.IsLoading) || (asm.ReaderCount == 0))
                throw new EmbedIO.HttpException(HttpStatusCode.NotFound, "audio channel list is empty");

            if (b && ((idx < 0) || (idx >= asm.ReaderCount)))
                throw new EmbedIO.HttpException(HttpStatusCode.NotFound, $"audio channel {idx} not active, total channels: {asm.ReaderCount}");
            return asm;
        }

        private async Tasks.Task<T1> RequestDeserializerCb<T1>(IHttpContext context) where T1 : class
        {
            return await Tasks.Task.Run(async () =>
            {
                using MemoryStream ms = await context.GetRequestBodyAsMemoryStreamAsync();
                return ms.DeserializeFromStream<T1>();
            });
        }
    }
}
