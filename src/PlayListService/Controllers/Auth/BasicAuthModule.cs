﻿using System;
using System.Collections.Concurrent;
using System.Security.Principal;
using System.Text;
using EmbedIO;
using PlayListService.Properties;
using PlayListServiceData.Certificate;
using PlayListServiceData.Process.Log;
using Tasks = System.Threading.Tasks;

namespace PlayListService.Controllers.Auth
{
    public class BasicAuthModule : WebModuleBase
    {
        public sealed override bool IsFinalHandler => false;
        private const string AuthType = "Basic";
        private const string AuthDefault = "admin";
        public ConcurrentDictionary<string, string> Accounts { get; } =
            new ConcurrentDictionary<string, string>(StringComparer.InvariantCulture);

        private readonly string RealmValue_;
        private string AuthMessage(string app) => $"Requires {app} app access account configuration";
        private string AuthString() => $"Basic realm=\"{RealmValue_}\" charset=UTF-8";
        private IPrincipal NoUser() => new GenericPrincipal(new GenericIdentity(string.Empty, string.Empty), null);
        private IPrincipal Unauthenticate(string type) => new GenericPrincipal(new GenericIdentity(string.Empty, type), null);
        private IPrincipal Authenticate(string user, string type) => new GenericPrincipal(new GenericIdentity(user, type), new string[0] { });
        private bool VerifyCredentials(string user, string p)
            => Accounts.TryGetValue(user, out string op) && string.Equals(p, op, StringComparison.Ordinal);

        public BasicAuthModule(string baseRoute, string realm) : base(baseRoute)
        {
            RealmValue_ = string.IsNullOrWhiteSpace(realm) ? "PlayListService" : realm;
            if (Settings.Default.ConfigUserAuth != default)
            {
                foreach (string s in Settings.Default.ConfigUserAuth)
                {
                    try
                    {
                        if (string.IsNullOrWhiteSpace(s))
                            continue;

                        string ss = EncryptString.Decrypt(s);
                        if (string.IsNullOrWhiteSpace(ss))
                            continue;

                        string[] arr = ss.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        if ((arr == default) || (arr.Length == 0))
                            continue;

                        if (arr.Length == 2)
                            try {
                                Accounts.TryAdd(arr[0], arr[1]);
                            } catch (Exception e) { e.WriteLog($"Accounts Add -> {e.Message}", ServiceStatic.GetLog()); }

                    } catch (Exception e) { e.WriteLog($"Basic Auth Module -> {e.Message}", ServiceStatic.GetLog()); }
                }
            }
#if DEBUG
            if (Accounts.Count == 0)
            {
                Accounts.TryAdd(AuthDefault, "1234");
                foreach (var t in Accounts)
                    System.Diagnostics.Debug.WriteLine($"{t.Key} = {t.Value}");
            }
#endif
        }

        protected sealed override async Tasks.Task OnRequestAsync(IHttpContext context)
        {
            if ((context == default) || (context.User.Identity.AuthenticationType.Length > 0))
                return;

            await Tasks.Task.Run(() =>
            {
                IPrincipal user = AuthenticateVerify(context);
                context.GetImplementation().User = user;
                if (user.Identity.AuthenticationType.Length == 0)
                    throw HttpException.Unauthorized("Missing Credentials", AuthMessage(Settings.Default.ConfigConfiguratorExe));
                else if (user.Identity.Name.Length == 0)
                    throw HttpException.Unauthorized("Invalid Credentials", AuthMessage(Settings.Default.ConfigConfiguratorExe));
            });
        }

        private IPrincipal AuthenticateVerify(IHttpContext context)
        {
            context.Response.Headers.Set(HttpHeaderNames.WWWAuthenticate, AuthString());

            string credentials,
                   authHeader = context.Request.Headers[HttpHeaderNames.Authorization];

            if (authHeader == null || !authHeader.StartsWith("basic ", StringComparison.OrdinalIgnoreCase))
                return NoUser();

            try
            {
                credentials = Encoding.UTF8.GetString(Convert.FromBase64String(authHeader.Substring(6).Trim()));
            }
            catch (FormatException)
            {
                return Unauthenticate(AuthType);
            }

            int pos = credentials.IndexOf(':');
            var (user, password) = pos < 0
                ? (credentials, string.Empty)
                : (credentials.Substring(0, pos), credentials.Substring(pos + 1));

            if ((user.Length > 0) && VerifyCredentials(user, password))
                return Authenticate(user, AuthType);
            return Unauthenticate(AuthType);
        }
    }
}
