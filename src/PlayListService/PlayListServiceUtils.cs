﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using PlayListService.Properties;
using PlayListServiceData.Base;

namespace PlayListService
{
    public static class PlayListServiceUtils
    {
        [FlagsAttribute]
        enum NetConnectionState : int
        {
            Modem = 0x1,
            LAN = 0x2,
            Proxy = 0x4,
            RasInstalled = 0x10,
            Offline = 0x20,
            Configured = 0x40,
        }

        public static string localMediaBaseUrl(string tag, bool isslash = false)
        {
            var web = ServiceStatic.GetWebServer();
            if (web == default)
                return string.Empty;

            string slash = isslash ? "/" : "";
            if (web.Host.HTTPS.IsValid)
                return $"{web.Host.HTTPS.ToString()}{tag}{slash}";
            else if (web.Host.HTTP.IsValid)
                return $"{web.Host.HTTP.ToString()}{tag}{slash}";
            else
                return string.Empty;
        }

        [System.Runtime.InteropServices.DllImport("wininet.dll")]
        private static extern bool InternetGetConnectedState(out int dwFlag, int rv);
        public static bool CheckNetworkConnectionState()
        {
            try
            {
                if (InternetGetConnectedState(out int dw, 0))
                {
                    if (((dw & (int)NetConnectionState.LAN) != 0) ||
                        ((dw & (int)NetConnectionState.Modem) != 0))
                        return true;
                }
            }
            catch { }
            return false;
        }

        public static string GetConfigM3uPath()
        {
            return string.IsNullOrWhiteSpace(Settings.Default.ConfigM3uPath) ?
                $"{Environment.GetFolderPath(Environment.SpecialFolder.MyVideos)}{Path.DirectorySeparatorChar}ServiceM3uPlayList{Path.DirectorySeparatorChar}" :
                Settings.Default.ConfigM3uPath;
        }

        public static string GetConfigVideoPath()
        {
            return string.IsNullOrWhiteSpace(Settings.Default.ConfigVideoPath) ?
                $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonVideos)}{Path.DirectorySeparatorChar}" :
                Settings.Default.ConfigVideoPath;
        }

        public static string GetConfigAudioPath()
        {
            return string.IsNullOrWhiteSpace(Settings.Default.ConfigAudioPath) ?
                $"{Environment.GetFolderPath(Environment.SpecialFolder.CommonMusic)}{Path.DirectorySeparatorChar}" :
                Settings.Default.ConfigAudioPath;
        }

        public static bool IsM3uRootDirectory(this string s)
        {
            string path = GetConfigM3uPath();
            if (path.EndsWith("\\"))
                return Path.GetDirectoryName(s)
                    .Equals(
                        path.Substring(0, path.Length - 1));
            else
                return Path.GetDirectoryName(s).Equals(path);
        }

        public static bool IsM3uAutoGenerateFile(this string s)
        {
            string [] names = new string [] {
                string.IsNullOrWhiteSpace(Settings.Default.ConfigAudioTranslateM3u) ?
                            BasePath.NameAudioBroadcastM3u : Settings.Default.ConfigAudioTranslateM3u,
                string.IsNullOrWhiteSpace(Settings.Default.ConfigM3uFavoriteName) ?
                            BasePath.NameFavoritesM3u : Settings.Default.ConfigM3uFavoriteName
            };
            foreach (var name in names)
                if (name.Equals(s)) return true;
            return false;
        }

        public static DirectoryInfo CreateM3uDirectory(this string s)
        {
            DirectoryInfo d;
            if ((d = new DirectoryInfo(Path.Combine(GetM3uRootDirectory(), s).Normalize(NormalizationForm.FormC))) == null)
                return null;
            if (!d.Exists)
                d.Create();
            return d;
        }

        public static string GetM3uRootDirectory()
        {
            string path = GetConfigM3uPath();
            if (path.EndsWith("\\"))
                return path.Substring(0, path.Length - 1);
            else
                return path;
        }

        public static string GetVideoRootDirectory()
        {
            string path = GetConfigVideoPath();
            if (path.EndsWith("\\"))
                return path.Substring(0, path.Length - 1);
            else
                return path;
        }

        public static string GetAudioRootDirectory()
        {
            string path = GetConfigAudioPath();
            if (path.EndsWith("\\"))
                return path.Substring(0, path.Length - 1);
            else
                return path;
        }

        public static string GetTorrentsRootDirectory()
        {
            string path = Settings.Default.ConfigTorrentsPath;
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;
            if (path.EndsWith("\\"))
                return path.Substring(0, path.Length - 1);
            else
                return path;
        }

        public static string GetAudioTranslateFile()
        {
            string name = $"{Settings.Default.ConfigAudioTranslateM3u}.m3u",
                   path = GetConfigM3uPath();

            if (path.EndsWith("\\"))
                return Path.Combine(path.Substring(0, path.Length - 1), name);
            else
                return Path.Combine(path, name);
        }

        public static bool IsSystemDirectory(this string path)
        {
            return path.EndsWith("$RECYCLE.BIN") || path.EndsWith("System Volume Information");
        }

        public static bool CheckExcludeDir(this string name, StringCollection collection)
        {
            return (collection != default) &&
                    (collection.Count > 0) &&
                    (collection
                        .Cast<string>()
                        .Select(x => name.Contains(x))
                        .FirstOrDefault() != default);
        }

        public static bool CheckFileExtension(this string ext, string[] collection)
        {
            return (from i in collection
                    where i.EndsWith(ext)
                    select i).FirstOrDefault() != default;
        }

    }
}
