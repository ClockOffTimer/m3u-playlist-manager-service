﻿
namespace PlayListService
{
    partial class M3uPlayListProjectInstaller
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(M3uPlayListProjectInstaller));
            this.serviceM3uPlayListProccessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceM3uPlayListInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceM3uPlayListProccessInstaller
            // 
            this.serviceM3uPlayListProccessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceM3uPlayListProccessInstaller.Password = null;
            this.serviceM3uPlayListProccessInstaller.Username = null;
            this.serviceM3uPlayListProccessInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ServiceM3uPlayListProccessInstaller_AfterInstall);
            // 
            // serviceM3uPlayListInstaller
            // 
            this.serviceM3uPlayListInstaller.DelayedAutoStart = true;
            this.serviceM3uPlayListInstaller.Description = resources.GetString("serviceM3uPlayListInstaller.Title");
            this.serviceM3uPlayListInstaller.DisplayName = resources.GetString("serviceM3uPlayListInstaller.DisplayName");
            this.serviceM3uPlayListInstaller.ServiceName = resources.GetString("serviceM3uPlayListInstaller.ServiceName");
            this.serviceM3uPlayListInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            this.serviceM3uPlayListInstaller.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.ServiceM3uPlayListProccessInstaller_AfterInstall);
            // 
            // M3uPlayListProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
                this.serviceM3uPlayListProccessInstaller,
                this.serviceM3uPlayListInstaller}
            );
        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceM3uPlayListProccessInstaller;
        private System.ServiceProcess.ServiceInstaller serviceM3uPlayListInstaller;
    }
}