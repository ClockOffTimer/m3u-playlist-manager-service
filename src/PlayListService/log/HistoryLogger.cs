﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Log.Formater;
using Swan.Logging;
using Tasks = System.Threading.Tasks;

namespace PlayListService.log
{
    public class HistoryLogger : ILogger
    {
        public class MessageExpand
        {
            private static readonly Regex r1_ = new(@"^\[(\S+)\]\s(\S+)\sGET\s\/([a-zA-Z]+)\/(\S+)\.(\S+)\s",
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace
                );
            private static readonly Regex r2_ = new(@"^\:\:\S+\:(\S+)\:\d+\:$",
                RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace
                );
            private GroupCollection mgc_ = default;

            public bool IsEnable { get; private set; } = false;
            public string Hash = default;
            public string IpAddress = default;
            public string Folder = default;
            public string File = default;
            public string FileExt = default;
            public string Name = default;

            public MessageExpand(string s)
            {
                if (string.IsNullOrWhiteSpace(s) || s.Contains("tag/?src="))
                    return;
                MatchCollection mc = MessageExpand.r1_.Matches(s);
                if (mc.Count == 0)
                    return;
                mgc_ = mc[0].Groups;
            }

            public async Tasks.Task Build()
            {
                if ((mgc_ == default) || (mgc_.Count != 6))
                    return;

                await Tasks.Task.Run(() =>
                {
                    for (int i = 0; i < mgc_.Count; i++)
                    {
                        string t = mgc_[i].Value;
                        if (string.IsNullOrWhiteSpace(t))
                            return;
                        switch (i)
                        {
                            case 1: Hash = t; break;
                            case 2: IpAddress = t; break;
                            case 3: Folder = t; break;
                            case 4: File = t; break;
                            case 5: FileExt = t; break;
                            default: break;
                        }
                    }

                    mgc_ = default;

                    do
                    {
                        try
                        {
                            string ext = $".{FileExt}";
                            if ((from i in BaseConstant.extM3uUri
                                 where i.Equals(ext)
                                 select i).FirstOrDefault() != default)
                                break;

                            if ((from i in BaseConstant.extAudioUri
                                 where i.Equals(ext)
                                 select i).FirstOrDefault() != default)
                                break;

                            if ((from i in BaseConstant.extVideoUri
                                 where i.Equals(ext)
                                 select i).FirstOrDefault() != default)
                                break;

                            return;
                        }
                        catch { return; }

                    } while (false);
                    try
                    {
                        Name = WebUtility.UrlDecode(File);
                    } catch { Name = File; }
                    try
                    {
                        MatchCollection mc = MessageExpand.r2_.Matches(IpAddress);
                        if (mc.Count > 0)
                        {
                            GroupCollection mgc = mc[0].Groups;
                            if (mgc.Count >= 2)
                                IpAddress = mgc[1].Value;
                        }
                    } catch { }
                    IsEnable = true;
                });
            }
        }

        Swan.Logging.LogLevel ILogger.LogLevel => Swan.Logging.LogLevel.Debug;
        private ILogManager LogManager = default;

        public HistoryLogger()
        {
            if (Settings.Default.IsConfigLogHttpEnable)
            {
                LogManager = ProcessLog.Create(
                    new FileLog(
                        BasePath.GetHttpLogPath(),
                        nameof(HistoryLogger)),
                    new LogFormaterFile());
                LogManager.Verbose.SetLogLevel();
            }
        }
        ~HistoryLogger()
        {
            Dispose();
        }

        async void ILogger.Log(LogMessageReceivedEventArgs ev)
        {
            if (Settings.Default.IsConfigLogHttpEnable && (LogManager != default))
            {
                try
                {
                    string extdata = (ev.ExtendedData == default) ? "" : $" [{ev.ExtendedData}]";

                    switch (ev.MessageType)
                    {
                        case Swan.Logging.LogLevel.None:
                        case Swan.Logging.LogLevel.Trace:
                        case Swan.Logging.LogLevel.Debug:
                            {
                                LogManager.Verbose.Write($"{ev.Source}:{ev.Sequence} - {ev.Message}{extdata}");
                                break;
                            }
                        case Swan.Logging.LogLevel.Info:
                            {
                                LogManager.Info.Write($"{ev.Source}: {ev.Message}{extdata}");
                                break;
                            }
                        case Swan.Logging.LogLevel.Warning:
                            {
                                LogManager.Warning.Write($"{ev.Source}: {ev.Message}");
                                break;
                            }
                        case Swan.Logging.LogLevel.Error:
                        case Swan.Logging.LogLevel.Fatal:
                            {
                                LogManager.Error.Write($"{ev.Source}: {ev.Message}");
                                break;
                            }
                    }
                } catch { }
            }
            try
            {
                if ((ev.MessageType == Swan.Logging.LogLevel.Debug) && (ev.Source != null) && ev.Source.Equals("WebServer"))
                {
                    MessageExpand me = new(ev.Message);
                    await me.Build().ConfigureAwait(false);
                    if (me.IsEnable)
                        await ServiceStatic.GetHistory().Add(me);
                }
            } catch (Exception e) { e.WriteLog(ServiceStatic.GetLog()).WriteStackTrace(e); }
        }

        public void Dispose()
        {
            ILogManager lm = LogManager;
            LogManager = default;
            if (lm != default)
                lm.Close();
            /* GC.SuppressFinalize(this); */
        }
    }
}
