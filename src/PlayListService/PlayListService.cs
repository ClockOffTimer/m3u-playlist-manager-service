﻿using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using Swan.Logging;

namespace PlayListService
{
    public partial class PlayListService : ServiceBase, IDisposable
    {
        public PlayListService()
        {
            InitializeComponent();
            CanShutdown = CanStop = true;
            AutoLog = true;
            ServiceStatic.SetEventLog(this.EventLog);
            ServiceStatic.GetLog().Info.Write(
                $"{Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location)} version: {Assembly.GetExecutingAssembly().GetName().Version}");
            _ = ServiceStatic.GetWatch();
        }
        ~PlayListService()
        {
            Dispose();
        }

        private string ServiceActualName() => Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().Location);

        public new void Dispose()
        {
            base.Dispose();
            ServiceStatic.Get().Clear();
        }

        protected override void OnShutdown()
        {
            ServiceStatic.GetLog().Info.Write($"{ServiceActualName()} - OnShutdown");
            OnStop();
            base.OnShutdown();
        }

        protected override void OnStart(string[] args)
        {
            ServiceStatic.GetLog().Info.Write($"{ServiceActualName()} - OnStart");
            try
            {
                ServiceStatic.GetWebServer().Start();
                ServiceStatic.GetWatch().Start();
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Error.Write("OnStart ->", e).WriteStackTrace(e);
            }
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            ServiceStatic.GetLog().Info.Write($"{ServiceActualName()} - OnStop");
            try
            {
                ServiceStatic.GetWatch().Stop();
                ServiceStatic.GetWebServer().Stop();
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Error.Write("OnStart ->", e).WriteStackTrace(e);
            }
            base.OnStop();
        }

        protected override void OnCustomCommand(int cmd)
        {
            ServiceStatic.GetLog().Info.Write($"{ServiceActualName()} - OnCustomCommand ({cmd})");
            try
            {
                if ((cmd < 128) || (cmd > 255))
                    return;
                ServiceStatic.GetWatch().CustomCommand = cmd;
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Error.Write("OnStart ->", e).WriteStackTrace(e);
            }
        }

#if DEBUG
        public void UserInteractiveRun()
        {
            if (Environment.UserInteractive)
            {
                OnStart(null);
                Console.ReadKey();
                Console.Write(Environment.NewLine);
                Console.WriteLine("-> Manual End, press any key..");
                OnStop();
                Logger.NoLogging();
                Console.ReadKey();
            }
        }
#endif
    }
}
