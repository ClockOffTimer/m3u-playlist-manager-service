﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define TRACE

using System;
using System.Security.Principal;
using System.Threading.Tasks;
using PlayListService.log;
using PlayListService.Properties;
using Swan.Logging;
#if !DEBUG
using System.IO;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.ComponentModel;
using System.Globalization;
using System.ServiceProcess;
using System.Configuration.Install;
#endif

namespace PlayListService
{
    static class Program
    {
        private static PlayListService plService = default;

        public static int Main(string[] args)
        {
            if (Environment.UserInteractive)
            {
#if DEBUG
                WriteColor(
                    "run Console DEBUG mode", ConsoleColor.White);
                WriteColor(
                    "Is using HTTPS mode, run this service only as administrator, otherwise access to certificates is limited.",
                    ConsoleColor.DarkCyan);

                if (Settings.Default.IsConfigNetSslEnable &&
                    !new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator))
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    WriteColor(
                        "HTTPS mode, run only as administrator, otherwise access to certificates is limited!",
                        ConsoleColor.DarkRed);

                    Console.ReadKey();
                    return -1;

                }

                Logger.RegisterLogger<HistoryLogger>();

                plService = new PlayListService();
                TaskScheduler.UnobservedTaskException +=
                    new EventHandler<UnobservedTaskExceptionEventArgs>(ServiceStatic.GetLog().UnobservedTaskLogging);
                AppDomain.CurrentDomain.UnhandledException +=
                    new UnhandledExceptionEventHandler(ServiceStatic.GetLog().UnhandledLogging);
                plService.UserInteractiveRun();
#else
                if (!new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator))
                    return -1;

                /* "use command line options: /i - install service, /u uinstall service" */
                if ((args != null) && (args.Length > 0))
                {
                    if (args[0].StartsWith("/i", StringComparison.OrdinalIgnoreCase))
                        return InstallService();
                    else if (args[0].StartsWith("/u", StringComparison.OrdinalIgnoreCase))
                        return UninstallService();
                }
                else
                {
                    try
                    {
                        using var p = new Process();
                        p.StartInfo.FileName = Path.Combine(
                            Path.GetDirectoryName(path: Assembly.GetAssembly(typeof(Program)).Location),
                            Settings.Default.ConfigConfiguratorExe);
                        p.Start();
                    }
                    catch { return -1; }
                }
                return 0;
#endif
            }
            else
            {
#if DEBUG
                Logger.NoLogging();
                Logger.UnregisterLogger<HistoryLogger>();
                WriteColor(
                    "Console output only is UserInteractive and DEBUG mode!", ConsoleColor.DarkRed);
                return -1;
#else
                try
                {
                    if (!String.IsNullOrEmpty(Settings.Default.ConfigLanguage))
                        Thread.CurrentThread.CurrentUICulture =
                            Thread.CurrentThread.CurrentCulture =
                                new CultureInfo(Settings.Default.ConfigLanguage);
                } catch { }

                Logger.NoLogging();
                Logger.RegisterLogger<HistoryLogger>();

                plService = new PlayListService();
                ServiceBase [] ServicesToRun = new ServiceBase [] { plService };
                TaskScheduler.UnobservedTaskException +=
                    new EventHandler<UnobservedTaskExceptionEventArgs>(ServiceStatic.GetLog().UnobservedTaskLogging);
                AppDomain.CurrentDomain.UnhandledException +=
                    new UnhandledExceptionEventHandler(ServiceStatic.GetLog().UnhandledLogging);
                ServiceBase.Run(ServicesToRun);
#endif
            }
            if (plService != default)
                plService.Dispose();
            Logger.UnregisterLogger<HistoryLogger>();
            return 0;
        }

#if !DEBUG
        private static int InstallService()
        {
            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { Assembly.GetExecutingAssembly().Location });
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException.GetType() == typeof(Win32Exception))
                    return ((Win32Exception)ex.InnerException).ErrorCode;
                else
                    return -1;
            }
            return 0;
        }
        private static int UninstallService()
        {
            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { "/u", Assembly.GetExecutingAssembly().Location });
            }
            catch (Exception ex)
            {
                if (ex.InnerException.GetType() == typeof(Win32Exception))
                    return ((Win32Exception)ex.InnerException).ErrorCode;
                else
                    return -1;
            }
            return 0;
        }
#else
        static void WriteColor(string s, ConsoleColor clr)
        {
            Console.ForegroundColor = clr;
            Console.WriteLine(s);
            Console.ResetColor();
        }
#endif
    }
}
