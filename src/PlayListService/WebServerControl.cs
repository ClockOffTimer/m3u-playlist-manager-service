﻿/* Copyright (c) 2021-2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using EmbedIO;
using EmbedIO.Files;
using EmbedIO.Security;
using EmbedIO.WebApi;
using PlayListService.Controllers;
using PlayListService.Controllers.Auth;
using PlayListService.Data;
using PlayListService.Lister;
using PlayListService.Properties;
using PlayListServiceData.Certificate;
using PlayListServiceData.Process.Http.Data;
using PlayListServiceData.Utils;
using Tasks = System.Threading.Tasks;

namespace PlayListService
{
    public class WebServerControl : IDisposable
    {
        private static readonly string LOG_TAG = "Web Server";
        private CancellationTokenSafe ctoken = default;
        private X509Certificate2 cert = default;
        private WebServer servMaster = default;
        private WebServer servSlave = default;

        public HostConfig Host { get; private set; } = default;

        public WebServerControl()
        {
            if (Settings.Default.IsConfigNetSslEnable)
            {
                try
                {
                    CertManager cm = new(
                        Settings.Default.ConfigNetSslCertStore,
                        Settings.Default.ConfigNetSslCertThumbprint,
                        Settings.Default.ConfigNetSslDomain,
                        Settings.Default.ConfigNetPort
                        );
                    cm.EventCb += (s, e) =>
                    {
                        ServiceStatic.GetLog().Error.Write($"CertManager -> {s} ->", e);
                    };
                    cert = cm.Get();
                } catch (Exception e) { ServiceStatic.GetLog().Error.Write($"{LOG_TAG} ->", e); }
            }
            Host = new(new HostConfigGet(), cert, ServiceStatic.GetLog());
            ServiceStatic.GetLog().Info.Write($"{nameof(WebServerControl)} Begin..");
        }
        ~WebServerControl()
        {
            Dispose();
        }

        public async void Start()
        {
            try
            {
                WebCancellation();
                WebDispose();
                string [] list = GetWhiteList();
                {
                    StringBuilder sb = new();
                    if (Settings.Default.IsConfigNetSslEnable)
                    {
                        sb.AppendFormat("HTTPS|{0}", Host.HTTPS.ToString());
                        if (cert != default)
                            sb.AppendFormat("|{0}", cert.Subject);
                    }
                    if (Settings.Default.IsConfigNetHttpSupport)
                        sb.AppendFormat(", HTTP|{0}", Host.HTTP.ToString());

                    ServiceStatic.GetLog().Verbose.Write($"Start {LOG_TAG} -> Begin init.. {sb}");
                }

                ctoken = new();
                servMaster = (Settings.Default.IsConfigNetSslEnable && (cert != default)) ?
                    CreateHttpsWebServer(cert) : CreateHttpWebServer();
                servMaster = CreateHttpMasterEndPoint(servMaster);
                if (list != default)
                    servMaster.WithIPBanning(o => o.WithWhitelist(list));
                servMaster.StateChanged += ServStateChanged;
                servMaster.HandleUnhandledException((c,e) => {
                    ServiceStatic.GetLog().Error.Write($"HTTPS {LOG_TAG} -> Unhandled ->", e);
                    return Tasks.Task.CompletedTask;
                });

                if ((cert == default) || !Settings.Default.IsConfigNetSslEnable || !Settings.Default.IsConfigNetHttpSupport)
                {
                    await Run(servMaster, ctoken.Token, false);
                    return;
                }
                await Run(servMaster, ctoken.Token, true);

                servSlave = CreateHttpWebServer();
                servSlave = CreateHttpSlaveEndPoint(servSlave);
                if (list != default)
                    servSlave.WithIPBanning(o => o.WithWhitelist(list));
                servSlave.StateChanged += ServStateChanged;
                servSlave.HandleUnhandledException((c, e) => {
                    ServiceStatic.GetLog().Error.Write($"HTTP {LOG_TAG} -> Unhandled ->", e);
                    return Tasks.Task.CompletedTask;
                });
                await Run(servSlave, ctoken.Token, false);
            }
            catch (Exception e)
            {
                ServiceStatic.GetLog().Error.Write($"Start {LOG_TAG} ->", e).WriteStackTrace(e);
                WebCancellation();
                WebDispose();
            }
        }

        private async Tasks.Task Run(WebServer srv, CancellationToken token, bool isThread = false)
        {
            if (isThread)
                srv.Start(token);
            else
                await srv.RunAsync(token).ConfigureAwait(false);
        }

        private void ServStateChanged(object sender, WebServerStateChangedEventArgs e)
        {
            ServiceStatic.GetLog().Verbose.Write($"{LOG_TAG} state -> {e.OldState} -> {e.NewState}");
        }

        public void Stop()
        {
            try
            {
                WebCancellation(true);
                WebDispose();
                ServiceStatic.GetLog().Verbose.Write($"Clear {LOG_TAG} -> End All..");
            } catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Stop {LOG_TAG} ->", e).WriteStackTrace(e); }
        }

        public void Dispose()
        {
            try
            {
                WebCancellation();
                WebDispose();
                X509Certificate2 x = cert;
                cert = default;
                if (x != default)
                    try { x.Dispose(); } catch { }

            } catch (Exception e) { ServiceStatic.GetLog().Error.Write($"Clear {LOG_TAG} ->", e).WriteStackTrace(e); }
        }

        #region Web server

        private async void WebCancellation(bool isCancellOnly = false)
        {
            CancellationTokenSafe cts = ctoken;
            ctoken = default;
            if (cts != default)
            {
                try {
                    if (!cts.IsCancellationRequested)
                    {
                        cts.Cancel();
                        await Tasks.Task.Delay(1000).ConfigureAwait(false);
                    }
                }
                catch (System.OperationCanceledException e) { ServiceStatic.GetLog().Error.Write($"Cancellation {LOG_TAG} -> {e.Message}"); }
                catch { }

                if (isCancellOnly)
                    ctoken = cts.Renew();
                else
                    cts.Dispose();
            }
        }
        private void WebDispose()
        {
            WebServer web;
            web = servMaster;
            servMaster = default;
            if (web != default)
                try { web.Dispose(); } catch { }

            web = servSlave;
            servSlave = default;
            if (web != default)
                try { web.Dispose(); } catch { }
        }

        private string[] GetWhiteList()
        {
            if ((Settings.Default.ConfigNetAccess == default) || (Settings.Default.ConfigNetAccess.Count == 0))
                return default;

            string[] list = new string[Settings.Default.ConfigNetAccess.Count];
            Settings.Default.ConfigNetAccess.CopyTo(list, 0);
            return list;
        }

        private string CreatePrefixUrl(string proto)
        {
            string url;
            if (proto.Equals("https"))
                url = Host.HTTPS.ToString();
            else
                url = Host.HTTP.ToString();

            if (string.IsNullOrWhiteSpace(url))
                return $"{proto}://*:8089";
            return url;
        }

        private WebServer CreateHttpsWebServer(X509Certificate2 cert)
        {
            return new WebServer(o => o
                .WithCertificate(cert)
                .WithUrlPrefix(CreatePrefixUrl("https"))
                .WithMode(HttpListenerMode.EmbedIO))
                .WithCors()
                .WithLocalSessionManager();
        }

        private WebServer CreateHttpWebServer()
        {
            return new WebServer(o => o
                .WithUrlPrefix(CreatePrefixUrl("http"))
                .WithMode(HttpListenerMode.EmbedIO))
                .WithCors()
                .WithLocalSessionManager();
        }

        private WebServer CreateHttpMasterEndPoint(WebServer web)
        {
            if (Settings.Default.ConfigUserAuth.Count > 0)
                web.WithModule(new BasicAuthModule("/cmd", nameof(PlayListService)))
                   .WithWebApi("/cmd", m => m.WithController(() => new CmdController(ServiceStatic.GetWatch())));

            if (Settings.Default.IsConfigTorrentEnable)
                web.WithStaticFolder("/torrents", PlayListServiceUtils.GetTorrentsRootDirectory(), true, m => m
                    .WithDirectoryLister(CustomLister.Torrents)
                    .WithoutDefaultDocument()
                    .WithContentCaching(false));

            return web
                .WithWebApi("/data", m => m.WithController(() => new DataController()))
                .WithWebApi("/stream", m => m.WithController(() => new AudioController()))
                .WithModule(new HistoryModule("/history"))
                .WithStaticFolder("/m3u", PlayListServiceUtils.GetConfigM3uPath(), true, m => m
                    .WithDirectoryLister(CustomLister.M3U8)
                    .WithoutDefaultDocument()
                    .WithContentCaching(Settings.Default.IsConfigM3UCache))
                .WithStaticFolder("/audio", PlayListServiceUtils.GetConfigAudioPath(), true, m => m
                    .WithDirectoryLister(CustomLister.Audio)
                    .WithoutDefaultDocument()
                    .WithContentCaching(Settings.Default.IsConfigAudioCache))
                .WithStaticFolder("/video", PlayListServiceUtils.GetConfigVideoPath(), true, m => m
                    .WithDirectoryLister(CustomLister.Video)
                    .WithoutDefaultDocument()
                    .WithContentCaching(Settings.Default.IsConfigVideoCache))
                .WithStaticFolder("/", PlayListServiceUtils.GetConfigM3uPath(), true, m => m
                    .WithDirectoryLister(CustomLister.Html)
                    .WithoutDefaultDocument()
                    .WithContentCaching(true));
        }
        private WebServer CreateHttpSlaveEndPoint(WebServer web)
        {
            return web
                .WithStaticFolder("/", PlayListServiceUtils.GetConfigM3uPath(), true, m => m
                    .WithDirectoryLister(CustomLister.RAWM3U)
                    .WithoutDefaultDocument()
                    .WithContentCaching(Settings.Default.IsConfigM3UCache));
        }
        #endregion
    }
}
