﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using PlayListService.Data;
using PlayListService.Properties;
using PlayListServiceData.Base;
using PlayListServiceData.BaseXml;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Process;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Log.Formater;
using PlayListServiceData.RunManager;
using PlayListServiceData.UPNP;
using PlayListServiceData.Utils;
using PlayListServiceStream;
using PlayListServiceStream.Data;

#if !DEBUG
using PlayListServiceData.Process.Report;
#endif

namespace PlayListService
{
    public class ServiceStatic
    {
        private readonly CacheValue<ILogManager> log;
        private readonly CacheValue<SheduleManager> shed;
        private readonly CacheValue<DirectoryWatch> watch =
            new(() => new DirectoryWatch());
        private readonly CacheValue<WebServerControl> web =
            new(() => new WebServerControl());
        private readonly CacheValue<DLNA> dlna =
            new(() => new DLNA());
        private readonly CacheValue<HistoryBase> history =
            new(() => new HistoryBase());
        private readonly CacheValue<DataBuilder<ServiceDataItems>> xmlbase =
            new(() => new DataBuilder<ServiceDataItems>(GetLog()));
        private readonly CacheValue<AudioStreamManagerService> audiostream =
            new(() => new AudioStreamManagerService(
                ServiceStatic.ToDefaultLog, BasePath.GetAudioStreamSettingsPath()));

        private readonly ProcessStatCheck [] stats = new ProcessStatCheck[7];
        private static ServiceStatic root = default;
        private static EventLog ServiceEventLog = default;

        public static FilesPlayListVersion CurrentVersion { get; private set; } = default;
        public static FilesPlayListVersion RepoVersion { get; private set; } = default;

        private ServiceStatic()
        {
            log = new CacheValue<ILogManager>(() => LogInit());
            shed = new CacheValue<SheduleManager>(() => SheduleManager.Create(log.Value));
            CurrentVersion = BuildCurrentVersion();
            RepoVersion = new FilesPlayListVersion();
            HttpClientDefault.HTTP.EnableProxy = Settings.Default.IsConfigNetHttpProxy || Settings.Default.IsConfigNetSocks5Proxy;
        }
        ~ServiceStatic()
        {
            Clear();
        }

        public void Clear()
        {
            try
            {
                web.Value.Dispose();
                shed.Value.Clear();
                watch.Value.Dispose();
                if (xmlbase.Value != default)
                    xmlbase.Value.Dispose();
                log.Value.Close();
            }
            catch (Exception e) { Debug.WriteLine($"Clear -> {e}");  }
            ServiceStatic.root = default;
        }

        public static ServiceStatic Get()
        {
            if (root == default)
                root = new ServiceStatic();
            return root;
        }
        public static ILogManager GetLog()
        {
            return Get().log.Value;
        }
        public static void SetEventLog(EventLog eventLog)
        {
            ServiceStatic.ServiceEventLog = eventLog;
        }
        public static DirectoryWatch GetWatch()
        {
            return Get().watch.Value;
        }
        public static WebServerControl GetWebServer()
        {
            return Get().web.Value;
        }
        public static SheduleManager GetSheduleManager()
        {
            return Get().shed.Value;
        }
        public static DLNA GetDlna()
        {
            return Get().dlna.Value;
        }
        public static HistoryBase GetHistory()
        {
            return Get().history.Value;
        }
        public static DataBuilder<ServiceDataItems> GetXmlBase()
        {
            return Get().xmlbase.Value;
        }
        public static AudioStreamManagerService GetAudioCapture()
        {
            return Get().audiostream.Value;
        }

        #region Audio Capture call
        public static AudioStreamReader GetAudioReader(int idx)
        {
            AudioStreamManagerService asm = GetAudioCapture();
            if (asm == default)
                return default;
            return asm.GetAudioReader(idx);
        }
        public static AudioChannelLite GetAudioReaderSettings(int idx)
        {
            AudioStreamManagerService asm = GetAudioCapture();
            if (asm == default)
                return default;
            return asm.GetChannelSettings(idx);
        }
        public static List<Tuple<string, List<string>>> GetAudioCaptureStat()
        {
            AudioStreamManagerService asm = GetAudioCapture();
            if (asm == default)
                return default;
            return asm.GetChannelsStat();
        }
        #endregion

        #region Stats call
        public static DateTime GetStatsLastDate()
        {
            try
            {
                DateTime dt = DateTime.MinValue;
                foreach (var s in Get().stats)
                    if ((s != default) && (s.LastDate > dt)) dt = s.LastDate;
                return dt;

            } catch { return DateTime.MinValue; }
        }
        public static ProcessStatsXml GetStatsSerializable()
        {
            return new ProcessStatsXml(Get().stats);
        }
        public static ProcessStatCheck [] GetStats()
        {
            return Get().stats;
        }
        public static ProcessStatCheck GetStat(ProcessStat.Id idx)
        {
            return GetStat((long)idx);
        }
        public static ProcessStatCheck GetStat(long id)
        {
            return Get().stats[id];
        }
        public static void SetStat(ProcessStat.Id id, ProcessStatCheck psc)
        {
            if (psc == default)
                return;
            psc.LastDate = DateTime.Now;
            psc.Id = id;
            var @this = Get();
            @this.stats[(long)id] = psc;
        }
        #endregion

        #region Dlna call
        public static void DlnaTimerDeviceStart()
        {
            Get().dlna.Value.TimerDeviceStart();
        }
        public static void DlnaTimerDeviceStop()
        {
            Get().dlna.Value.TimerDeviceStop();
        }
        public static string DlnaListXmlDevices()
        {
            return Get().dlna.Value.ListXmlDevices();
        }
        #endregion

        #region Xml Bases
        public static ServiceDataItems GetXmlBase(DataTarget target)
        {
            DataBuilder<ServiceDataItems> builder = Get().xmlbase.Value;
            if (builder == default)
                return default;
            return builder.Get(target);
        }
        public static bool IfXmlBase(DataTarget target)
        {
            DataBuilder<ServiceDataItems> builder = Get().xmlbase.Value;
            if (builder == default)
                return default;
            return builder.IsBaseEnable(target);
        }
        public static void ClearXmlBase()
        {
            DataBuilder<ServiceDataItems> builder = Get().xmlbase.Value;
            if (builder == default)
                return;
            builder.Clear(DataTarget.None);
        }
        public static void InitXmlBase()
        {
            DataBuilder<ServiceDataItems> builder = Get().xmlbase.Value;
            if (builder == default)
                return;

            builder.Add(
                DataTarget.Id_GroupWildcardChannels,
                BasePath.GetGroupWildcardPath(),
                (t, s) => builder.Load<SerializeDataChannels>(t, s)
            );
            builder.Add(
                DataTarget.Id_GroupAlias,
                BasePath.GetGroupAliasPath(),
                (t, s) => builder.Load<SerializeDataChannels>(t, s)
            );
            builder.Add(
                DataTarget.Id_EpgFavorite,
                BasePath.GetEpgFavoritePath(),
                (t, s) => builder.Load<SerializeDataChannels>(t, s)
            );
            builder.Add(
                DataTarget.Id_EpgChannels,
                Settings.Default.ConfigM3uPath.GetEpgBasePath(Settings.Default.ConfigEpgUrl),
                (t, s) => builder.Load<SerializeDataChannels>(
                    t, s,
                    new DataTarget[] { DataTarget.Id_EpgAlias, DataTarget.Id_EpgFavorite },
                    new string[] { BasePath.GetEpgAliasPath(), BasePath.GetEpgFavoritePath() })
            );
            builder.InitLazy();
        }
        public static Tuple<int,int,int> CountXmlBase()
        {
            DataBuilder<ServiceDataItems> builder = Get().xmlbase.Value;
            if (builder == default)
                return new Tuple<int, int, int>(0,0,0);
            return new Tuple<int, int, int>(
                CountXmlBaseSafe(builder.Get(DataTarget.Id_GroupWildcardChannels)),
                CountXmlBaseSafe(builder.Get(DataTarget.Id_GroupAlias)),
                CountXmlBaseSafe(builder.Get(DataTarget.Id_EpgChannels))
                );
        }
        private static int CountXmlBaseSafe(ServiceDataItems item)
        {
            if (item == default)
                return 0;
            return item.Items.Count;
        }
        #endregion

        #region Log default/initialize
        private ILogManager LogInit()
        {
            ILogManager log_ = default;
#if DEBUG
            log_ = ProcessLog.Create(
                new FileLog(
                    BasePath.GetMainLogPath(),
                    nameof(PlayListService)),
                new LogFormaterFile());
            log_.Verbose.SetLogLevel();
#elif JOURNAL_DEBUG
            log_ = ProcessLog.Create(
                new JournalLog(
                    Assembly.GetExecutingAssembly().FullName,
                    nameof(PlayListService)));
#elif SERVICE_DEBUG_JournalLog
            log_ = ProcessLog.Create(
                new JournalLog(
                    Settings.Default.ConfigLogSource,
                    Settings.Default.ConfigLogName));
#elif SERVICE_DEBUG_ServiceLog
            log_ = ProcessLog.Create(
                new ServiceLog(
                    ServiceStatic.ServiceEventLog,
                    Settings.Default.ConfigLogSource,
                    Settings.Default.ConfigLogName), new Reporter());
#else
            log_ = ProcessLog.Create(
                new ServiceLog(ServiceStatic.ServiceEventLog),
                new LogFormaterService(),
                Settings.Default.IsConfigReport ? new Reporter() : default);
            log_.Warning.SetLogLevel();
#endif
            return log_;
        }
        public static void ToDefaultLog(char c, string s, Exception e)
        {
            ILogManager lm = ServiceStatic.GetLog();
            if (lm == default)
                return;

            if (e != default)
            {
                if (s != default)
                    e.WriteLog(c, s, lm);
                else
                    e.WriteLog(c, lm);
            }
            else if (s != default)
                s.WriteLog(c, lm);
        }
        #endregion

        #region Current/Repo version
        private FilesPlayListVersion BuildCurrentVersion()
        {
            try
            {
                FileVersionInfo fvi =
                    FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
                FileInfo f = new(fvi.FileName);

                return new FilesPlayListVersion() {
                    AppDate = $"{f.CreationTime:yyyy-MM-dd}",
                    AppName = $"{fvi.ProductName}",
                    AppDesc = $"{fvi.FileDescription}",
                    AppVersion = $"{fvi.ProductVersion}",
                    AppFile = $"Releases/M3U-PlayList-Service-RU-{fvi.ProductVersion}.msi",
                    AppBaseUrl = BaseConstant.RepoBaseRawUrl()
                };
            }
            catch (Exception e) { System.Diagnostics.Debug.WriteLine($"CreateVersion -> {e}"); }
            return new FilesPlayListVersion();
        }
        public static void SetRepoVersion(FilesPlayListVersion plv)
        {
            RepoVersion = (plv != default) ? plv : RepoVersion;
        }
        #endregion

    }
}
