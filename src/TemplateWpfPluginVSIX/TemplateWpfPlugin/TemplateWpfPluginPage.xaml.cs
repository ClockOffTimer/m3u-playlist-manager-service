﻿/* Copyright (c) 2021-2022 $safeprojectname$, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel.Composition;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 9),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class Page$projecttag$ : Page, IPlugin
    {
        public int Id { get; set; } = 0;
        public new string Title  => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView   => this;

        private readonly Page$projecttag$Data PageData_;

        [ImportingConstructor]
        public Page$projecttag$(IServiceControls icontrols)
        {
            InitializeComponent();
            PageData_ = (Page$projecttag$Data)DataContext;
            PageData_.SetServiceCtrl(icontrols);
            this.IsVisibleChanged += event_IsVisibleChanged;
        }

        private void event_IsVisibleChanged(object sender, System.Windows.DependencyPropertyChangedEventArgs e)
        {
            if ((e != default) && (e.NewValue is bool b))
            {
            }
        }

        private void TestCmd_CanExecute(object sender, CanExecuteRoutedEventArgs e) =>
            e.CanExecute = PageData_ != default;
        private async void TestCmd_Executed(object sender, ExecutedRoutedEventArgs e)
        {
        }
    }
}
