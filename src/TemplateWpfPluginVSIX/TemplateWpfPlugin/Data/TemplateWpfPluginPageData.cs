﻿/* Copyright (c) 2021-2022 WpfPluginTasks, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Media;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Http.Data;
using PlayListServiceData.RunManager;
using PlayListServiceData.ServiceConfig;
using PlayListServiceData.Utils;

namespace WpfPlugin.Data
{
    public class Page$projecttag$Data : PropertyChangedEvent
    {
        private IServiceControls _ServiceCtrl = default;
        public  IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }
        private string ErrorMessage_ = default;
        public  string ErrorMessage
        {
            get => ErrorMessage_;
            set { ErrorMessage_ = value; OnPropertyChanged(nameof(ErrorMessage), nameof(IsErrorMessage)); }
        }
        public bool IsErrorMessage { get => !string.IsNullOrWhiteSpace(ErrorMessage); }

        public Page$projecttag$Data()
        {
        }
        ~Page$projecttag$Data()
        {
        }

        #region Set Service Control
        public async void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
        }
    }
}
