﻿/* Copyright (c) 2021-2022 $safeprojectname$, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;

namespace WpfPlugin.Data
{
    public abstract class PropertyChanged$projecttag$ : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChanged(params string[] names)
        {
            foreach (var s in names)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
    }
}
