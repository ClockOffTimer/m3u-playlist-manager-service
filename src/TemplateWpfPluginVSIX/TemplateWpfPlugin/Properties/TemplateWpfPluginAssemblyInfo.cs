﻿/* Copyright (c) 2021-2022 $safeprojectname$, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("$safeprojectname$")]
[assembly: AssemblyDescription("$time$")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NT")]
[assembly: AssemblyProduct("$safeprojectname$")]
[assembly: AssemblyCopyright("Copyright ©  2021-2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("$guid1$")]
[assembly: AssemblyVersion("1.0.*")]
