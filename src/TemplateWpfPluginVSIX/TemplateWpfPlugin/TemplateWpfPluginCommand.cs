﻿/* Copyright (c) 2021-2022 $safeprojectname$, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPlugin$projecttag$
	{
		public static readonly RoutedUICommand TestCmd = new
			(
				"",
				"TestCmd",
				typeof(CommandPlugin$projecttag$),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
	}
}
