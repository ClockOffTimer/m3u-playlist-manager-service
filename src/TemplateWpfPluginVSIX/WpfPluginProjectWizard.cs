﻿using System;
using System.Collections.Generic;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;

namespace TemplateWpfPluginVSIX
{
    public class WpfPluginProjectWizard : IWizard
    {
        public void BeforeOpeningFile(ProjectItem projectItem) { }
        public void ProjectFinishedGenerating(Project project) { }
        public void ProjectItemFinishedGenerating(ProjectItem projectItem) { }
        public void RunFinished() { }
        public bool ShouldAddProjectItem(string filePath) => true;

        public void RunStarted(
            object aobj,
            Dictionary<string, string> dict,
            WizardRunKind kind,
            object[] custom)
        {
            _ = dict.TryGetValue("$destinationdirectory$", out string path);
            try
            {
                if (dict.TryGetValue("$safeprojectname$", out string s))
                    dict.AddDictionary("$projecttag$", s.Replace("WpfPlugin", "").Replace(" ", "").Trim());
                dict.DumpDictionary(path);

            } catch (Exception ex) { ex.WriteException(path); }
        }
    }
}
