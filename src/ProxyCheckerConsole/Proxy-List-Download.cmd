@echo off
if "%1"=="" goto :default
ProxyCheckerConsole.exe --spysme --dst %1
goto :end

:default
ProxyCheckerConsole.exe --spysme --dst config\proxylist-out.txt
copy /b config\HttpClientProxy.list+config\proxylist-out.txt config\HttpClientProxy.spysme.list

:end
exit 0
