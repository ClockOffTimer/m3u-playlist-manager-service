﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ProxyCheckerConsole")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service/-/tree/main/Releases")]
[assembly: AssemblyProduct("M3U Play List Service - Utilites - ProxyCheckerConsole")]
[assembly: AssemblyCopyright("Copyright © NT 2021-2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("c8495f53-863f-43ba-b445-ac1c86d4c7a0")]

[assembly: AssemblyVersion("1.0.0.1")]
[assembly: AssemblyFileVersion("1.0.0.1")]
