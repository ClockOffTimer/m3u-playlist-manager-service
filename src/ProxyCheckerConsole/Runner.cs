﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PlayListServiceData.Process.CmdLine;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Http.Utils;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Log.Formater;
using PlayListServiceData.Process.Log.Handler;

namespace ProxyCheckerConsole
{
    public class Options
    {
        [CmdOption(Key = "--src", IsFile = true,  /* IsFileExists = true, */
            ResourceId = "H1")]
        public FileInfo Src { get; set; } = default;
        [CmdOption(Key = "--dst", IsFile = true,
            ResourceId = "H2")]
        public FileInfo Dst { get; set; } = default;
        [CmdOption(Key = "--hidemy", IsSwitch = true,
            ResourceId = "H3")]
        public bool IsHidemy { get; set; } = false;
        [CmdOption(Key = "--spysme", IsSwitch = true,
            ResourceId = "H4")]
        public bool IsSpysme { get; set; } = false;
        [CmdOption(Key = "--socks5", IsSwitch = true,
            ResourceId = "H5")]
        public bool IsProxySOCKS5 { get; set; } = false;
        [CmdOption(Key = "--close", IsSwitch = true,
            ResourceId = "H6")]
        public bool IsAutoClose { get; set; } = false;
    }

    class Runner
    {
        private const string uspysme = "https://spys.me/proxy.txt";
        private static Options opt = default;
        private static HttpClientManager hcmgr = default;
        private static ILogManager log = default;

        static void Main(string[] args)
        {
            try
            {
                log = ProcessLog.Create(
                    new ConsoleLog(),
                    new LogFormaterConsole());
                log.Verbose.SetLogLevel();

                AppDomain.CurrentDomain.UnhandledException +=
                    new UnhandledExceptionEventHandler(log.UnhandledLogging);
                TaskScheduler.UnobservedTaskException +=
                    new EventHandler<UnobservedTaskExceptionEventArgs>(log.UnobservedTaskLogging);
            }
            catch (Exception e) { Console.WriteLine(e.Message); return; }

            try
            {
                opt = CmdOptionParse.Parse<Options>(args);

                if (opt == default)
                    throw CmdOptionException.Create(new ArgumentNullException(nameof(Options)), true);
                if (opt.Dst == default)
                    throw CmdOptionException.Create(new ArgumentException(Properties.Resources.E1), true);
                if ((!opt.IsSpysme) && (opt.Src == default))
                    throw CmdOptionException.Create(new ArgumentException(Properties.Resources.E2), true);

                string ss = (opt.IsSpysme || (opt.Src == default)) ? uspysme : opt.Src.Name;
                $"{Properties.Resources.S1}: Auto Close:{opt.IsAutoClose}, Convert:{opt.IsHidemy}/{opt.IsSpysme}".WriteLog('*', log);
                $"IO: {ss} -> {opt.Dst.Name}".WriteLog('*', log);

                /* Test(); */

                if (opt.IsSpysme || opt.IsHidemy)
                    ProxyConverter();
                else
                    ProxyChecker();
            }
            catch (Exception e) when (e is CmdOptionException exc)
            {
                if (exc.IsCallHelp)
                    CmdOptionParse.Help<Options>();
                exc.WriteLog('!', Environment.NewLine, log);
            }
            catch (Exception ex)
            {
                ex.WriteLog('!', log);
            }
            if ((opt != default) && !opt.IsAutoClose)
            {
                $"{Properties.Resources.S2}..".WriteLog('*', log);
                Console.ReadKey();
                if (log != default)
                    log.Close();
            }
        }

        private static async void ProxyConverter()
        {
            try
            {
                ProxyListConverter plc = opt.IsSpysme ?
                    new ProxyListConverter(new Uri(uspysme), opt.Dst) :
                    new ProxyListConverter(opt.Src, opt.Dst);

                plc.EventCb += (c, s, a) => {
                    DefaultPrint(c, s, a);
                };

                if (opt.IsSpysme)
                    _ = await plc.UrlConvertAsync();
                else if (opt.IsHidemy)
                    _ = await plc.FileConvertAsync();
            }
            catch (Exception e) { e.WriteLog('!', $"{nameof(ProxyConverter)}:", log); }
        }

        private static async void ProxyChecker()
        {
            try
            {
                hcmgr = new HttpClientManager();
                hcmgr.EventCb += (c, s, a) => {
                    DefaultPrint(c, s, a);
                };
                hcmgr.CheckFileOut = opt.Dst;

                string[] proxys = File.ReadAllLines(opt.Src.FullName);
                if (opt.IsProxySOCKS5)
                    await hcmgr.ProxySocks5AddRange(proxys.Distinct()).ConfigureAwait(false);
                else
                    await hcmgr.ProxyHttpAddRange(proxys.Distinct()).ConfigureAwait(false);
            }
            catch (Exception e) { e.WriteLog('!', $"{nameof(ProxyChecker)}:", log); }
        }

        private static void DefaultPrint(char c, string s, Exception e)
        {
            if (e == default)
                s.WriteLog(c, log);
            else
                e.WriteLog(c, s, log);
        }
    }
}
