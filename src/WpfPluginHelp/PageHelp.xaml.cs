﻿/* Copyright (c) 2021 WpfPluginHelp, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel.Composition;
using System.Windows.Controls;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 3),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_BTN)]
    public partial class PageHelp : Page, IPlugin
    {
        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageHelp(IServiceControls icontrols)
        {
            InitializeComponent();
            PageHelpData pageHelpData = (PageHelpData)DataContext;
            pageHelpData.SetServiceCtrl(icontrols);

        }
    }
}
