﻿/* Copyright (c) 2021 WpfPluginHelp, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class PageHelpData : INotifyPropertyChanged
    {
        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageHelpData()
        {
        }

        public void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
            OnPropertyChanged(nameof(UrlM3u));
            OnPropertyChanged(nameof(UrlM3uAll));
            OnPropertyChanged(nameof(UrlAudio));
            OnPropertyChanged(nameof(UrlVideo));
        }

        public string UrlM3u
        {
            get { return $"{getDefaultUrl()}/"; }
        }
        public string UrlM3uAll
        {
            get { return $"{getDefaultUrl()}/m3u/"; }
        }
        public string UrlAudio
        {
            get { return $"{getDefaultUrl()}/audio/"; }
        }
        public string UrlVideo
        {
            get { return $"{getDefaultUrl()}/video/"; }
        }

        internal string getDefaultUrl()
        {
            return ((ServiceCtrl != null) && (ServiceCtrl.ControlConfig != null)) ?
                $"http://{ServiceCtrl.ControlConfig.ConfigNetAddr}:{ServiceCtrl.ControlConfig.ConfigNetPort}" :
                "http://you.service.run.ip.address:8089";
        }
    }
}
