﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace M3U.Model.Tests
{
    [TestClass()]
    public class MediaTests
    {
        const string url00 = "http://xxx.jrg:9898//video///test/file.mp3";
        const string url01 = "http://xxx.jrg:9898/video/test/file.mp3";
        const string url02 = "http://xxx.jrg:9898//video/test/file.mp3?sec=0;total=1600;name=Aaaaa";
        const string url10 = "http://zzz.frg:9090///path///test/other.mp3";
        const string url11 = "http://zzz.frg:9090/path/test/other.mp3";
        readonly Media media = new Media(url00, "Name", -1);
        readonly Media compare1 = new Media(url02, "Name", -1);
        readonly Media compare2 = new Media(url01, "Name", -1);

        [TestMethod()]
        public void MediaTest()
        {
            Assert.AreEqual<string>(media.MediaFile, url01);
            media.MediaUrl = new System.Uri(url10, System.UriKind.Absolute);
            Assert.AreEqual<string>(media.MediaFile, url11);

            Uri uri = media.MediaUrl;
            Assert.IsNotNull(uri);
            System.Diagnostics.Debug.WriteLine($"{uri.AbsoluteUri}");

            Assert.IsTrue(compare1.Equals(compare2));
            Assert.IsTrue(compare1.GetHashCode() == compare2.GetHashCode());
            System.Diagnostics.Debug.WriteLine($"{compare1.GetHashCode()} == {compare2.GetHashCode()}");

        }
    }
}