﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PlayListServiceData.Base.Tests
{
    [TestClass()]
    public class BasePathMappingTests
    {
        readonly BasePathMapping mapAudio = new BasePathMapping(
                "http://192.168.0.5:8088/audio/", "https://my.domain.be:8089/audio/",
                @"D:\Video\Sound\");
        readonly string audioHttpsUrl = @"https://my.domain.be:8089/audio/%D0%A4%D1%83%D1%80%D1%81%D0%BE%D0%B2/%D0%9D%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D1%80%D1%83%D0%BF%D0%BD%D0%B5%D0%B9%D1%88%D0%B8%D0%B9%20%D0%BA%D1%80%D0%B8%D0%B7%D0%B8%D1%81%20%D0%B2%20%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%B8%20%D1%87%D0%B5%D0%BB%D0%BE%D0%B2%D0%B5%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0.mp3";
        readonly string audioHttpUrl = @"http://192.168.0.5:8088/audio/%D0%A4%D1%83%D1%80%D1%81%D0%BE%D0%B2/%D0%9D%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D0%B5%D1%82%D1%81%D1%8F%20%D0%BA%D1%80%D1%83%D0%BF%D0%BD%D0%B5%D0%B9%D1%88%D0%B8%D0%B9%20%D0%BA%D1%80%D0%B8%D0%B7%D0%B8%D1%81%20%D0%B2%20%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%B8%20%D1%87%D0%B5%D0%BB%D0%BE%D0%B2%D0%B5%D1%87%D0%B5%D1%81%D1%82%D0%B2%D0%B0.mp3";
        readonly string audioFile = @"D:\Video\Sound\Фурсов\Начинается крупнейший кризис в истории человечества.mp3";

        [TestMethod()]
        public void BasePathMappingTest()
        {
            Assert.IsNotNull(mapAudio);
        }

        [TestMethod()]
        public void GetFileInfoFromUrlTest()
        {
            FileInfo f = mapAudio.GetFileInfoFromUrl(audioHttpsUrl);
            Assert.IsNotNull(f);
        }

        [TestMethod()]
        public void GetPathFromUrlTest()
        {
            Assert.AreEqual<string>(mapAudio.GetPathFromUrl(audioHttpsUrl), audioFile);
            Assert.AreEqual<string>(mapAudio.GetPathFromUrl(audioHttpUrl), audioFile);
        }

        [TestMethod()]
        public void GetUrlFromPathTest()
        {
            Assert.AreEqual<string>(mapAudio.GetUrlFromPath(audioFile, true), audioHttpsUrl);
            Assert.AreEqual<string>(mapAudio.GetUrlFromPath(audioFile, false), audioHttpUrl);
        }

        [TestMethod()]
        public void IsFileExistFromUrlTest()
        {
            Assert.AreEqual<bool>(mapAudio.IsFileExistFromUrl(audioHttpsUrl), true);
            Assert.AreEqual<bool>(mapAudio.IsFileExistFromUrl(audioHttpUrl), true);
        }
    }
}