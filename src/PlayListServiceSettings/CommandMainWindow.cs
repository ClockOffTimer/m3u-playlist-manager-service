﻿using System.Windows.Input;

namespace PlayListServiceSettings
{
	public static class CommandMainWindow
	{
		public static readonly RoutedUICommand Theme = new RoutedUICommand
			(
				"",
				"Theme",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand Setup = new RoutedUICommand
			(
				"",
				"Setup",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand Settings = new RoutedUICommand
			(
				"",
				"Settings",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand Controls = new RoutedUICommand
			(
				"",
				"Controls",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand Help = new RoutedUICommand
			(
				"",
				"Help",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
		public static readonly RoutedUICommand SetupSave = new RoutedUICommand
			(
				"",
				"SetupSave",
				typeof(CommandMainWindow),
				new InputGestureCollection()
			);
	}
}
