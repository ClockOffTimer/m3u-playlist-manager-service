﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using PlayListServiceData.ServiceConfig;

/// <summary>
/// <para>
/// Sources: 
/// <para>https://stackoverflow.com/questions/358700/how-to-install-a-windows-srv-programmatically-in-c </para>
/// <para>https://www.c-sharpcorner.com/article/create-windows-services-in-c-sharp/</para>
/// </para>
/// ServiceHelper.SetServiceName("MyServiceName");
/// <para>
/// Installs and starts the srv
/// ServiceHelper.InstallService("MyServiceDisplayName", "MyDescription", "C:\\PathToServiceFile.exe", true);
/// ServiceHelper.InstallAndStartService("MyServiceDisplayName", "MyDescription", "C:\\PathToServiceFile.exe", true);
/// </para>
/// <para>
/// Removes the srv
/// ServiceHelper.UninstallService();
/// </para>
/// <para>
/// Checks the status of the srv
/// ServiceHelper.GetServiceStatus();
/// </para>
/// <para>
/// Starts the srv
/// ServiceHelper.StartService();
/// </para>
/// <para>
/// Stops the srv
/// ServiceHelper.StopService();
/// </para>
/// <para>
/// Check if srv is installed
/// ServiceHelper.IsServiceInstalled();
/// </para>
/// </summary>
/// 
namespace PlayListServiceSettings.Helper
{

    #region enum
    [Flags]
    public enum ScmAccessRights
    {
        Connect = 0x0001,
        CreateService = 0x0002,
        EnumerateService = 0x0004,
        Lock = 0x0008,
        QueryLockStatus = 0x0010,
        ModifyBootConfig = 0x0020,
        StandardRightsRequired = 0xF0000,
        AllAccess = (StandardRightsRequired | Connect | CreateService |
                     EnumerateService | Lock | QueryLockStatus | ModifyBootConfig)
    }

    [Flags]
    public enum ServiceAccessRights
    {
        QueryConfig = 0x1,
        ChangeConfig = 0x2,
        QueryStatus = 0x4,
        EnumerateDependants = 0x8,
        Start = 0x10,
        Stop = 0x20,
        PauseContinue = 0x40,
        Interrogate = 0x80,
        UserDefinedControl = 0x100,
        Delete = 0x00010000,
        StandardRightsRequired = 0xF0000,
        AllAccess = (StandardRightsRequired | QueryConfig | ChangeConfig |
                     QueryStatus | EnumerateDependants | Start | Stop | PauseContinue |
                     Interrogate | UserDefinedControl),
    }

    public enum ServiceBootFlag
    {
        Start = 0x00000000,
        SystemStart = 0x00000001,
        AutoStart = 0x00000002,
        DemandStart = 0x00000003,
        Disabled = 0x00000004
    }

    public enum ServiceControl
    {
        Stop = 0x00000001,
        Pause = 0x00000002,
        Continue = 0x00000003,
        Interrogate = 0x00000004,
        Shutdown = 0x00000005,
        ParamChange = 0x00000006,
        NetBindAdd = 0x00000007,
        NetBindRemove = 0x00000008,
        NetBindEnable = 0x00000009,
        NetBindDisable = 0x0000000A,
        UserCommand = 128
    }

    public enum ServiceError
    {
        Ignore = 0x00000000,
        Normal = 0x00000001,
        Severe = 0x00000002,
        Critical = 0x00000003
    }

    public enum ServiceConfig : uint
    {
        ServiceConfigDescription = 0x00000001,
        ServiceConfigFailureActions = 0x00000002,
        ServiceConfigDelayedAutoStartInfo = 0x00000003,
        ServiceConfigFailureActionsFlag = 0x00000004,
        ServiceConfigServiceSidInfo = 0x00000005,
        ServiceConfigRequiredPrivilegesInfo = 0x00000006,
        ServiceConfigPreshutdownInfo = 0x00000007,
        ServiceConfigTriggerInfo = 0x00000008,
        ServiceConfigPreferredNode = 0x00000009,
        ServiceConfigLaunchProtected = 0x00000012
    }
    #endregion

    public static class ServiceHelper
    {
        internal class PtrData
        {
            public IntPtr scm = IntPtr.Zero;
            public IntPtr srv = IntPtr.Zero;
            private bool IsCloseBegin = false;
            public bool IsInit
            {
                get { return ((scm != IntPtr.Zero) && (srv != IntPtr.Zero)); }
            }

            public PtrData(ScmAccessRights mar, ServiceAccessRights sar = ServiceAccessRights.AllAccess)
            {
                scm = ServiceHelper.OpenSCManager_(mar);
                if (scm == IntPtr.Zero)
                    return;
                srv = ServiceHelper.OpenService(scm, ServiceHelper.ServiceName, sar);
                if (srv == IntPtr.Zero)
                    return;
            }
            ~PtrData()
            {
                Close();
            }

            public void Close()
            {
                if (IsCloseBegin)
                    return;
                IsCloseBegin = true;

                try
                {
                    if (srv != IntPtr.Zero)
                        CloseServiceHandle(srv);
                    if (scm != IntPtr.Zero)
                        CloseServiceHandle(scm);
                }
                catch { }
                scm = IntPtr.Zero;
                srv = IntPtr.Zero;
            }
        }

        /* private const int STANDARD_RIGHTS_REQUIRED = 0xF0000; */
        private const int SERVICE_WIN32_OWN_PROCESS = 0x00000010;
        public static string ServiceName { get; private set; } = default;

        [StructLayout(LayoutKind.Sequential)]
        private class SERVICE_STATUS
        {
            public int dwServiceType = 0;
            public ServiceState dwCurrentState = 0;
            public int dwControlsAccepted = 0;
            public int dwWin32ExitCode = 0;
            public int dwServiceSpecificExitCode = 0;
            public int dwCheckPoint = 0;
            public int dwWaitHint = 0;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Size = 8)]
        private class SERVICE_DESCRIPTION_INFO
        {
            [MarshalAs(UnmanagedType.LPStr)] //? LPUTF8Str -> don't work
            public string lpDescription;
        }

        [StructLayout(LayoutKind.Sequential, Size = 8, Pack = 4)]
        private class SERVICE_DELAYED_AUTO_START_INFO
        {
            [MarshalAs(UnmanagedType.Bool)]
            public bool fDelayedAutostart;
        }

        #region OpenSCManager
        [DllImport("advapi32.dll", EntryPoint = "OpenSCManagerW", ExactSpelling = true, CharSet = CharSet.Unicode, SetLastError = true)]
        static extern IntPtr OpenSCManager(string machineName, string databaseName, ScmAccessRights dwDesiredAccess);
        #endregion

        #region OpenService
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern IntPtr OpenService(IntPtr hSCManager, string lpServiceName, ServiceAccessRights dwDesiredAccess);
        #endregion

        #region CreateService
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr CreateService(IntPtr hSCManager, string lpServiceName, string lpDisplayName, ServiceAccessRights dwDesiredAccess, int dwServiceType, ServiceBootFlag dwStartType, ServiceError dwErrorControl, string lpBinaryPathName, string lpLoadOrderGroup, IntPtr lpdwTagId, string lpDependencies, string lp, string lpPassword);
        #endregion

        #region ChangeServiceConfig2A (SERVICE_DESCRIPTION)
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ChangeServiceConfig2A(IntPtr hService, ServiceConfig dwInfoLevel, SERVICE_DESCRIPTION_INFO lpInfo);
        #endregion

        #region ChangeServiceConfig2W (SERVICE_DELAYED_AUTO_START)
        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ChangeServiceConfig2W(IntPtr hService, ServiceConfig dwInfoLevel, SERVICE_DELAYED_AUTO_START_INFO lpInfo);
        #endregion

        #region CloseServiceHandle
        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CloseServiceHandle(IntPtr hSCObject);
        #endregion

        #region QueryServiceStatus
        [DllImport("advapi32.dll")]
        private static extern int QueryServiceStatus(IntPtr hService, SERVICE_STATUS lpServiceStatus);
        #endregion

        #region DeleteService
        [DllImport("advapi32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool DeleteService(IntPtr hService);
        #endregion

        #region ControlService
        [DllImport("advapi32.dll")]
        private static extern int ControlService(IntPtr hService, ServiceControl dwControl, SERVICE_STATUS lpServiceStatus);
        #endregion

        #region StartService
        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern int StartService(IntPtr hService, int dwNumServiceArgs, int lpServiceArgVectors);
        #endregion

        #region public

        public static void SetServiceName(string s)
        {
            ServiceHelper.ServiceName = (!String.IsNullOrEmpty(s) && !s.Equals(ServiceName)) ? s : ServiceName;
        }

        public static void UninstallService()
        {
            PtrData pd = new PtrData(ScmAccessRights.AllAccess);

            try
            {
                if (!pd.IsInit)
                    throw new ApplicationException("Service not installed.");

                StopService_(pd.srv);
                if (!DeleteService(pd.srv))
                    throw new ApplicationException("Could not delete srv " + Marshal.GetLastWin32Error());
            }
            finally
            {
                pd.Close();
            }
        }

        public static bool IsServiceInstalled()
        {
            PtrData pd = new PtrData(ScmAccessRights.Connect, ServiceAccessRights.QueryStatus);

            try
            {
                if (!pd.IsInit)
                    return false;
                return true;
            }
            finally
            {
                pd.Close();
            }
        }

        public static void InstallAndStartService(string displayName, string displayDesc, string fileName, bool isDelay = false)
        {
            PtrData pd = new PtrData(ScmAccessRights.AllAccess);

            try
            {
                Install_(ref pd, ref displayName, ref displayDesc, ref fileName, isDelay);
                StartService_(pd.srv);
            }
            finally
            {
                pd.Close();
            }
        }

        public static void InstallService(string displayName, string displayDesc, string fileName, bool isDelay = false)
        {
            PtrData pd = new PtrData(ScmAccessRights.AllAccess);

            try
            {
                Install_(ref pd, ref displayName, ref displayDesc, ref fileName, isDelay);
            }
            finally
            {
                pd.Close();
            }
        }

        public static void StartService()
        {
            PtrData pd = new PtrData(ScmAccessRights.Connect, ServiceAccessRights.QueryStatus | ServiceAccessRights.Start);

            try
            {
                if (!pd.IsInit)
                    throw new ApplicationException("Failed to init SCManager.");
                StartService_(pd.srv);
            }
            finally
            {
                pd.Close();
            }
        }

        public static void StopService()
        {
            PtrData pd = new PtrData(ScmAccessRights.Connect, ServiceAccessRights.QueryStatus | ServiceAccessRights.Stop);

            try
            {
                if (!pd.IsInit)
                    throw new ApplicationException("Failed to init SCManager.");
                StopService_(pd.srv);
            }
            finally
            {
                pd.Close();
            }
        }

        public static ServiceState GetServiceStatus()
        {
            PtrData pd = new PtrData(ScmAccessRights.Connect, ServiceAccessRights.QueryStatus);

            try
            {
                if (!pd.IsInit)
                    return ServiceState.NotFound;
                return GetServiceStatus_(pd.srv);
            }
            finally
            {
                pd.Close();
            }
        }

        public static ServiceState SetUserCommand(int cmd)
        {
            PtrData pd = new PtrData(ScmAccessRights.AllAccess);

            try
            {
                if (!pd.IsInit)
                    return ServiceState.Unknown;

                SERVICE_STATUS status = new SERVICE_STATUS();
                ControlService(pd.srv, ServiceControl.UserCommand + cmd, status);
                return status.dwCurrentState;
            }
            finally
            {
                pd.Close();
            }
        }

        #endregion

        #region private

        private static void Install_(ref PtrData pd, ref string displayName, ref string displayDesc, ref string fileName, bool isDelay = false)
        {
            if (pd.scm == IntPtr.Zero)
                throw new ApplicationException("Failed to init SCManager.");

            if (pd.srv == IntPtr.Zero)
                pd.srv = CreateService(pd.scm, ServiceName, displayName, ServiceAccessRights.AllAccess, SERVICE_WIN32_OWN_PROCESS, ServiceBootFlag.AutoStart, ServiceError.Normal, fileName, null, IntPtr.Zero, null, null, null);

            if (pd.srv == IntPtr.Zero)
                throw new ApplicationException("Failed to install srv.");

            {
                SERVICE_DESCRIPTION_INFO desc = new SERVICE_DESCRIPTION_INFO()
                {
                    lpDescription = displayDesc
                };
                if (!ChangeServiceConfig2A(pd.srv, ServiceConfig.ServiceConfigDescription, desc))
                    throw new ApplicationException("Failed to set descriptions = " + Marshal.GetLastWin32Error());
            }
            if (isDelay)
            {
                SERVICE_DELAYED_AUTO_START_INFO delays = new SERVICE_DELAYED_AUTO_START_INFO()
                {
                    fDelayedAutostart = true
                };
                if (!ChangeServiceConfig2W(pd.srv, ServiceConfig.ServiceConfigDelayedAutoStartInfo, delays))
                    throw new ApplicationException("Failed to set delay = " + Marshal.GetLastWin32Error());
            }
        }

        private static void StartService_(IntPtr srv)
        {
            StartService(srv, 0, 0);
            var changedStatus = WaitForServiceStatus_(srv, ServiceState.StartPending, ServiceState.Running);
            if (!changedStatus)
                throw new ApplicationException("Unable to start srv");
        }

        private static void StopService_(IntPtr srv)
        {
            SERVICE_STATUS status = new SERVICE_STATUS();
            ControlService(srv, ServiceControl.Stop, status);
            var changedStatus = WaitForServiceStatus_(srv, ServiceState.StopPending, ServiceState.Stopped);
            if (!changedStatus)
                throw new ApplicationException("Unable to stop srv");
        }

        private static ServiceState GetServiceStatus_(IntPtr srv)
        {
            SERVICE_STATUS status = new SERVICE_STATUS();

            if (QueryServiceStatus(srv, status) == 0)
                throw new ApplicationException("Failed to query srv status.");

            return status.dwCurrentState;
        }

        private static bool WaitForServiceStatus_(IntPtr srv, ServiceState waitStatus, ServiceState desiredStatus)
        {
            SERVICE_STATUS status = new SERVICE_STATUS();

            QueryServiceStatus(srv, status);
            if (status.dwCurrentState == desiredStatus) return true;

            int dwStartTickCount = Environment.TickCount;
            int dwOldCheckPoint = status.dwCheckPoint;

            while (status.dwCurrentState == waitStatus)
            {
                int dwWaitTime = status.dwWaitHint / 10;
                if (dwWaitTime < 1000) dwWaitTime = 1000;
                else if (dwWaitTime > 10000) dwWaitTime = 10000;

                Thread.Sleep(dwWaitTime);

                if (QueryServiceStatus(srv, status) == 0)
                    break;
                if (status.dwCheckPoint > dwOldCheckPoint)
                {
                    dwStartTickCount = Environment.TickCount;
                    dwOldCheckPoint = status.dwCheckPoint;
                }
                else if (Environment.TickCount - dwStartTickCount > status.dwWaitHint)
                    break;
            }
            return (status.dwCurrentState == desiredStatus);
        }

        private static IntPtr OpenSCManager_(ScmAccessRights rights)
        {
            IntPtr scm = OpenSCManager(null, null, rights);
            if (scm == IntPtr.Zero)
                throw new ApplicationException("Could not connect to srv control manager.");

            return scm;
        }
        #endregion
    }
}

