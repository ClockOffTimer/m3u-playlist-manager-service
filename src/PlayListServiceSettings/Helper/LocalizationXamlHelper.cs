﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;

namespace PlayListServiceSettings.Helper
{
    public static class LocalizationXamlHelper
    {
        private static readonly string resPathFmt = @"Resources/{0}.{1}.xaml";
        private static readonly string resFmt = @"Resources/Lang{0}.xaml";
        private static readonly string resWild = @"Resources/Lang.";
        public static string LangId { get; private set; } = default;

        /*
         * SetAppLanguage(Application.Current, System.Threading.Thread.CurrentThread.CurrentUICulture)
         * using in App.xaml.cs:
         *      protected override void OnStartup(StartupEventArgs e) { .. insert this .. }
         */
        public static void SetAppLanguage(Application app, CultureInfo ci)
        {
            if (ci == null)
                return;
            LangId = ci.Name;

            if ((app == null) || (!LangId.Equals("ru-ru", StringComparison.OrdinalIgnoreCase)))
                return;
            try
            {
                ResourceDictionary oDict, nDict = new ResourceDictionary
                {
                    Source = new Uri(String.Format(resFmt, ".ru-RU"), UriKind.Relative)
                };
                if ((oDict = (from d in app.Resources.MergedDictionaries
                              where d.Source != null && d.Source.OriginalString.StartsWith(resWild)
                              select d).FirstOrDefault()) != null)
                {
                    int idx = app.Resources.MergedDictionaries.IndexOf(oDict);
                    app.Resources.MergedDictionaries.Remove(oDict);
                    app.Resources.MergedDictionaries.Insert(idx, nDict);
                }
                else
                    app.Resources.MergedDictionaries.Add(nDict);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public static ResourceDictionary GetResourceDictionary(string s)
        {
            if (String.IsNullOrEmpty(s))
                return default;
            try
            {
                if (String.IsNullOrEmpty(LangId))
                    LangId = System.Threading.Thread.CurrentThread.CurrentUICulture.Name;

                ResourceDictionary nDict = new ResourceDictionary
                {
                    Source = new Uri(String.Format(resPathFmt, s, LangId), UriKind.Relative)
                };
                return nDict;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return default;
        }
    }
}
