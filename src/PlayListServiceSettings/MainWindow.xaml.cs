﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ModernWpf;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using PlayListServiceSettings.Properties;

namespace PlayListServiceSettings
{
    public partial class MainWindow : Window
    {
        private readonly object __locked = new object();
        private readonly MainWindowData mainWindowData;
        private readonly HostPlugins<IServiceControls> hostPlugins;
        private readonly IServiceControls ServiceCtrl;
        private TabItem tabItem = default;
        private bool IsFullScreen = default;
        private bool IsKeyLock = default;

        public MainWindow()
        {
            ThemeManager.Current.ApplicationTheme = (ApplicationTheme)Settings.Default.ThemeIndex;
            ServiceCtrl = ((App)Application.Current).ServiceCtrl;
            ServiceCtrl.ControlMainWindow = this;
            InitializeComponent();
            mainWindowData = (MainWindowData)DataContext;
            mainWindowData.SetServiceCtrl(ServiceCtrl);
            hostPlugins = new HostPlugins<IServiceControls>(
                PluginsToTab,
                "WpfPlugin*.dll",
                ServiceCtrl,
                PluginType.PLUG_PLACE_TAB,
                Settings.Default.PluginsGuiList);
        }

        public void PluginsToTab(List<IPlugin> list, PluginGuiList plist)
        {
            if (list == null)
                return;

            foreach (var p in list)
            {
                try
                {
                    if (!plist.Update(p))
                        continue;
                    TabItem item = BuildTabItem(p);
                    if (item != null)
                        TabControlMain.Items.Add(item);
                } catch (Exception e) { System.Diagnostics.Debug.WriteLine($"to Tab plugins: {e}"); }
            }
        }

        private TabItem BuildTabItem(IPlugin iplug, bool ischeck = false)
        {
            if (ischeck)
            {
                lock (__locked)
                    if (tabItem != default)
                        return tabItem;
            }
            if (iplug.GetView == null)
                return null;

            TabItem item = new TabItem
            {
                Header = iplug.TitleShort,
                Content = new Frame()
            };
            ((Frame)item.Content).Content = iplug.GetView;
            return item;
        }

        private void TabManager(IPlugin iplug)
        {
            try
            {
                string title = default;

                lock (__locked)
                {
                    if (tabItem != default)
                    {
                        if (TabControlMain.Items.Contains(tabItem))
                            TabControlMain.Items.Remove(tabItem);

                        if (tabItem.Header is string head)
                            title = head;
                        tabItem = default;
                    }
                    TabItem item = BuildTabItem(iplug, true);
                    if (item == null)
                        return;

                    if (TabControlMain.Items.Contains(item))
                    {
                        TabControlMain.Items.Remove(item);
                        return;
                    }
                    if ((item.Header is string header) && header.Equals(title))
                        return;

                    tabItem = item;
                    TabControlMain.Items.Insert(0, tabItem);
                    TabControlMain.SelectedIndex = 0;
                }
            } catch (Exception e) { System.Diagnostics.Debug.WriteLine($"Tab Manager plugins: {e}"); }
        }

        private void TabRefresh()
        {
            try
            {
                lock (__locked)
                {
                    var list = hostPlugins.GuiList.Plugins.Select(c => { c.IsFound = false; return c; }).ToList();
                    for (int i = TabControlMain.Items.Count - 1; i >= 0; i--)
                    {
                        TabItem t = TabControlMain.Items[i] as TabItem;
                        var a = hostPlugins.GuiList.Get(list, t.Header.ToString());
                        if (a == default)
                            continue;
                        if (!a.IsEnable)
                            TabControlMain.Items.Remove(t);
                        a.IsFound = true;
                    }
                    foreach(var p in list.Where(c => !c.IsFound && c.IsEnable))
                    {
                        TabItem item = BuildTabItem(p.Context, false);
                        if (item == null)
                            break;
                        if (TabControlMain.Items.Contains(item))
                            break;
                        TabControlMain.Items.Add(item);
                        tabItem = item;
                    }
                    TabControlMain.SelectedIndex = 0;
                }
            } catch (Exception e) { System.Diagnostics.Debug.WriteLine($"Tab Refresh plugins: {e}"); }
        }

        private void CreateBtnTabItem(int id)
        {
            try
            {
                var list = hostPlugins.GetPlugins(PluginType.PLUG_PLACE_BTN, id);
                if ((list == null) || (list.Count == 0))
                    return;
                TabManager(list[0]);
            } catch { }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            if (IsKeyLock)
                return;
            IsKeyLock = true;

            try
            {
                if ((e.Key == Key.F12) ||
                    ((e.Key == Key.System) && (e.SystemKey == Key.F12)))
                {
                    Theme_Executed(null, null);
                }
                else if ((e.Key == Key.F11) ||
                    ((e.Key == Key.System) && (e.SystemKey == Key.F11)))
                {
                    if (IsFullScreen)
                    {
                        WindowStyle = WindowStyle.SingleBorderWindow;
                        WindowState = WindowState.Normal;
                    }
                    else
                    {
                        WindowStyle = WindowStyle.None;
                        WindowState = WindowState.Maximized;
                    }
                    IsFullScreen = !IsFullScreen;
                }
                else if ((e.Key == Key.F10) ||
                    ((e.Key == Key.System) && (e.SystemKey == Key.F10)))
                {
                    Settings_Executed(null, null);
                }
                else if (e.Key == Key.F9)
                {
                    Controls_Executed(null, null);
                }
                else if (e.Key == Key.F8)
                {
                    Setup_Executed(null, null);
                }
                else if (e.Key == Key.F1)
                {
                    Help_Executed(null, null);
                }
            }
            finally
            {
                IsKeyLock = false;
            }
        }

        private void Settings_Executed(object sender, ExecutedRoutedEventArgs e) => CreateBtnTabItem(1);
        private void Controls_Executed(object sender, ExecutedRoutedEventArgs e) => CreateBtnTabItem(2);
        private void Help_Executed(object sender, ExecutedRoutedEventArgs e) => CreateBtnTabItem(3);
        private void Theme_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (ThemeManager.Current.ApplicationTheme == ApplicationTheme.Light)
                ThemeManager.Current.ApplicationTheme = ApplicationTheme.Dark;
            else
                ThemeManager.Current.ApplicationTheme = ApplicationTheme.Light;
            Settings.Default.ThemeIndex = (int)ThemeManager.Current.ApplicationTheme;
            Settings.Default.Save();
        }
        private async void Setup_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                SettingsControlsDialogContent cdc = new SettingsControlsDialogContent(hostPlugins.GuiList.Plugins);
                ModernWpf.Controls.ContentDialog dialog = new ModernWpf.Controls.ContentDialog
                {
                    Title = cdc.Tag.ToString(),
                    PrimaryButtonText = ServiceCtrl.ControlLanguage.DialogMessage1,
                    CloseButtonText = ServiceCtrl.ControlLanguage.DialogMessage2,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Primary,
                    Content = cdc
                };

                ModernWpf.Controls.ContentDialogResult result = await dialog.ShowAsync();
                if (result == ModernWpf.Controls.ContentDialogResult.Primary)
                    TabRefresh();
            } catch { }
        }
        private void SetupSave_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Settings.Default.PluginsGuiList = hostPlugins.GuiList.Export();
                Settings.Default.Save();
            } catch { }
        }
    }
}
