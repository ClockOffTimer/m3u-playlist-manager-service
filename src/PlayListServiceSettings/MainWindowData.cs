﻿using System.ComponentModel;
using PlayListServiceData.ServiceConfig;

namespace PlayListServiceSettings
{
    public class MainWindowData : INotifyPropertyChanged
    {
        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public MainWindowData()
        {
        }
        public void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
        }
    }
}
