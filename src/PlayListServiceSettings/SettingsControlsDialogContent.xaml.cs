﻿/* Copyright (c) 2021 PlayListServiceSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.ObjectModel;
using System.Windows.Controls;
using PlayListServiceData.Plugins;

namespace PlayListServiceSettings
{
    public partial class SettingsControlsDialogContent : UserControl
    {
        public ObservableCollection<PluginGuiItem> PluginItems { get; private set; } = default;

        public SettingsControlsDialogContent(ObservableCollection<PluginGuiItem> list)
        {
            PluginItems = list;
            InitializeComponent();
            DataContext = this;
        }
    }
}
