﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Security.Principal;
using System.Threading;
using System.Windows;
using PlayListServiceData.ServiceConfig;
using PlayListServiceData.Settings;
using PlayListServiceSettings.Helper;
using PlayListServiceSettings.Properties;

namespace PlayListServiceSettings.Data
{
    public class ServiceControls : INotifyPropertyChanged, IServiceControls
    {
        private readonly object __locked = new object();
        private Timer checkTimer = default;
        private ServiceState _ControlServiceState = ServiceState.Unknown;
        private bool _IsControlAdminRun = false,
                     _IsControlServiceRun = false,
                     _IsControlServiceStop = false,
                     _IsControlServiceInstall = false;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public event IServiceControls.DelegateOnCloseEvent OnCloseEventCb;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public void CallOnCloseEvent()
        {
            OnCloseEventCb?.Invoke();
        }

        public ServiceControls()
        {
            try
            {
                ControlConfig = new ServiceSettings("PlayListService");
                ControlConfig.PropertyChanged += config_PropertyChanged;
                ControlServiceTags = new ServiceTags(LocalizationXamlHelper.GetResourceDictionary("ServiceText"));
                ControlLanguage = new ServiceLanguage(LocalizationXamlHelper.GetResourceDictionary("Lang"));
                checkTimer = new Timer(new TimerCallback(CheckTimerCallback), null, 0, 6000);
                IsControlAdminRun = new WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator);
                ControlCommand.SetServiceName(ControlServiceTags.Name);
                EpgHostGuiList = Settings.Default.EpgHostGuiList.Cast<string>().ToList();
            }
            catch { }
        }

        ~ServiceControls()
        {
            ControlConfig.PropertyChanged -= config_PropertyChanged;
            if (checkTimer != null)
            {
                try
                {
                    checkTimer.Dispose();
                    checkTimer = default;
                }
                catch { }
            }
        }

        public void ReadConfig()
        {
            ControlConfig.Load();
        }
        public void WriteConfig(Action act = null)
        {
            ControlConfig.Save();
            if (act != null)
                act?.Invoke();
        }

        #region get/set

        public Window ControlMainWindow { get; set; } = default;
        public IServiceCommand ControlCommand => new ServiceCommand();
        public IServiceSettings ControlConfig { get; set; } = default;
        public IServiceLanguage ControlLanguage { get; set; } = default;
        public IServiceTags ControlServiceTags { get; set; } = default;

        public bool IsControlConfig
        {
            get { return ControlConfig != null && ControlConfig.IsLoad; }
        }

        public ServiceState ControlServiceState
        {
            get { return _ControlServiceState; }
            set
            {
                if (_ControlServiceState == value)
                    return;
                _ControlServiceState = value;
                OnPropertyChanged(nameof(ControlServiceState));
                OnPropertyChanged(nameof(ControlServiceStateText));
            }
        }
        public string ControlServiceStateText
        {
            get { return _ControlServiceState.ToString(); }
        }

        public bool IsControlServiceRun
        {
            get { return _IsControlServiceRun; }
            set
            {
                if (_IsControlServiceRun == value)
                    return;
                _IsControlServiceRun = value;
                if (value)
                {
                    IsControlServiceInstall = true;
                    OnPropertyChanged(nameof(IsControlServiceInstall));
                }
                OnPropertyChanged(nameof(IsControlServiceRun));
            }
        }

        public bool IsControlServiceStop
        {
            get { return _IsControlServiceStop; }
            set
            {
                if (_IsControlServiceStop == value)
                    return;
                _IsControlServiceStop = value;
                if (value)
                {
                    IsControlServiceInstall = true;
                    OnPropertyChanged(nameof(IsControlServiceInstall));
                }
                OnPropertyChanged(nameof(IsControlServiceStop));
            }
        }

        public bool IsControlServiceInstall
        {
            get { return _IsControlServiceInstall; }
            set
            {
                if (_IsControlServiceInstall == value)
                    return;
                _IsControlServiceInstall = value;
                OnPropertyChanged(nameof(IsControlServiceInstall));
            }
        }

        public bool IsControlAdminRun
        {
            get { return _IsControlAdminRun; }
            set
            {
                if (_IsControlAdminRun == value)
                    return;
                _IsControlAdminRun = value;
                OnPropertyChanged(nameof(IsControlAdminRun));
            }
        }

        public bool IsControlServicePath
        {
            get
            {
                return (ControlConfig.ConfigM3uPath == default) ||
                       (ControlConfig.ConfigAudioPath == default) ||
                       (ControlConfig.ConfigVideoPath == default);
            }
        }

        public int IsPlatformRun => IntPtr.Size == 4 ? 86 : 64;
        public string IconSaveExtension => Settings.Default.IconSaveExtension;
        public string FFMpegPath => Settings.Default.FFMpegPath;
        public List<string> EpgHostGuiList { get; }

        #endregion

        #region public

        public void ServiceStateChange(ServiceState state)
        {
            ControlServiceState = state;
            switch (state)
            {
                case ServiceState.Running:
                    {
                        IsControlServiceRun = true;
                        IsControlServiceStop = false;
                        IsControlServiceInstall = true;
                        break;
                    }
                case ServiceState.Paused:
                case ServiceState.Stopped:
                    {
                        IsControlServiceRun = false;
                        IsControlServiceStop = true;
                        IsControlServiceInstall = true;
                        break;
                    }
                case ServiceState.StartPending:
                case ServiceState.StopPending:
                case ServiceState.PausePending:
                case ServiceState.ContinuePending:
                    {
                        // PendingTimerStart();
                        IsControlServiceRun = false;
                        IsControlServiceStop = false;
                        IsControlServiceInstall = false;
                        break;
                    }
                case ServiceState.NotFound:
                    {
                        IsControlServiceRun = false;
                        IsControlServiceStop = false;
                        IsControlServiceInstall = false;
                        break;
                    }
                case ServiceState.Unknown:
                    {
                        IsControlServiceRun = false;
                        IsControlServiceStop = false;
                        IsControlServiceInstall = false;
                        break;
                    }
                case ServiceState.None:
                    {
                        IsControlServiceRun = false;
                        IsControlServiceStop = false;
                        IsControlServiceInstall = true;
                        break;
                    }
            }
            switch (state)
            {
                case ServiceState.StartPending:
                case ServiceState.StopPending:
                case ServiceState.PausePending:
                case ServiceState.ContinuePending:
                case ServiceState.None:
                    {
                        break;
                    }
                default:
                    {
                        // PendingTimerStop();
                        break;
                    }
            }
            OnPropertyChanged(nameof(ControlServiceState));
            OnPropertyChanged(nameof(IsControlServiceRun));
            OnPropertyChanged(nameof(IsControlServiceStop));
            OnPropertyChanged(nameof(IsControlServiceInstall));
            OnPropertyChanged(nameof(ControlServiceStateText));
        }

        public void CheckServiceStatus()
        {
            try
            {
                lock (__locked)
                    ServiceStateChange(ControlCommand.GetServiceStatus());
            }
            catch { }
        }

        #endregion

        #region private

        private void CheckTimerCallback(object o)
        {
            CheckServiceStatus();
        }

        private void config_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "ServiceSettingsLoad":
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            OnPropertyChanged(nameof(ControlConfig));
                            OnPropertyChanged(nameof(IsControlServicePath));
                        });
                        break;
                    }
            }
        }

        #endregion
    }
}
