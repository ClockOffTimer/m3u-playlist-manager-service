﻿namespace PlayListServiceData.ServiceConfig
{
    public class ServiceCommand : IServiceCommand
    {
        public ServiceState GetServiceStatus()
        {
            return PlayListServiceSettings.Helper.ServiceHelper.GetServiceStatus();
        }

        public void InstallAndStartService(string displayName, string displayDesc, string fileName, bool isDelay = false)
        {
            PlayListServiceSettings.Helper.ServiceHelper.InstallAndStartService(displayName, displayDesc, fileName, isDelay);
        }

        public void InstallService(string displayName, string displayDesc, string fileName, bool isDelay = false)
        {
            PlayListServiceSettings.Helper.ServiceHelper.InstallService(displayName, displayDesc, fileName, isDelay);
        }

        public bool IsServiceInstalled()
        {
            return PlayListServiceSettings.Helper.ServiceHelper.IsServiceInstalled();
        }

        public void SetServiceName(string s)
        {
            PlayListServiceSettings.Helper.ServiceHelper.SetServiceName(s);
        }

        public ServiceState SetUserCommand(int cmd)
        {
            return PlayListServiceSettings.Helper.ServiceHelper.SetUserCommand(cmd);
        }

        public void StartService()
        {
            PlayListServiceSettings.Helper.ServiceHelper.StartService();
        }

        public void StopService()
        {
            PlayListServiceSettings.Helper.ServiceHelper.StopService();
        }

        public void UninstallService()
        {
            PlayListServiceSettings.Helper.ServiceHelper.UninstallService();
        }
    }
}
