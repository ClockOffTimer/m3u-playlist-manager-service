﻿using System;
using System.Windows;

namespace PlayListServiceSettings.Data
{
    public class ServiceLanguage : IServiceLanguage
    {
        public string DialogMessage1 { get; private set; } = default;
        public string DialogMessage2 { get; private set; } = default;

        public ServiceLanguage(ResourceDictionary r)
        {
            try
            {
                if (r == null)
                    return;

                DialogMessage1 = r["dialogMessage1"] as string;
                DialogMessage2 = r["dialogMessage2"] as string;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e.Message);
            }
        }
    }
}
