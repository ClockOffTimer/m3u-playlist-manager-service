﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using PlayListServiceSettings.Helper;

namespace PlayListServiceSettings
{
    public partial class App : Application
    {
        public Data.ServiceControls ServiceCtrl { get; private set; } = default;

        protected override void OnStartup(StartupEventArgs e)
        {
            LocalizationXamlHelper.SetAppLanguage(Current, Thread.CurrentThread.CurrentUICulture);
            ServiceCtrl = new Data.ServiceControls();
            ServiceCtrl.ControlConfig.ConfigLanguage = LocalizationXamlHelper.LangId;
#if DEBUG
            try
            {
                const string log = @"log\wpf.log";
                Directory.CreateDirectory(Path.GetDirectoryName(log));
                FileInfo f = new FileInfo(log);
                if ((f != default) && f.Exists)
                    f.Delete();
                Debug.Listeners.Add(new TextWriterTraceListener(log));
                Debug.AutoFlush = true;
                Debug.Indent();
                Debug.WriteLine("OnStartup App");

            } catch (Exception ex) { Trace.WriteLine($"{this.GetType().Name} {nameof(OnStartup)}: {ex}"); }
#endif
            base.OnStartup(e);
        }
        protected override void OnExit(ExitEventArgs e)
        {
            ServiceCtrl.CallOnCloseEvent();
#if DEBUG
            Debug.WriteLine("OnExit App");
            Debug.Unindent();
            Debug.Flush();
#endif
            base.OnExit(e);
        }
    }
}
