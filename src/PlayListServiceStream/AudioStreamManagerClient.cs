﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using CSCore;
using CSCore.CoreAudioAPI;
using CSCore.SoundIn;
using CSCore.SoundOut;
using CSCore.Streams;
using CSCore.Streams.Effects;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Utils;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;
using PlayListServiceStream.Exceptions;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceStream
{
    public class AudioStreamManagerClient : AudioStreamBase, IAudioStreamObject
    {
        private readonly object lock__ = new();
        private readonly AudioMMDevice mmdevice;
        private CancellationTokenSafe tokenCancell = default;
        private CancellationTokenSafe tokenRun = default;
        private WeakReference<Equalizer> equalizer = default;
        private WeakReference<GainSource> gain = default;
        private WeakReference<WasapiCapture> din = default;
        private ISoundOut sout;
        private bool IsDesposed = false;

        public AudioStreamManagerClient(AudioMMDevice mmd, ILogManager log)
        {
            LogWriter = log;
            mmdevice = mmd;
            for (int i = 0; i < 10; i++)
                ChannelEqualizersDefault_.Add(new EqualizerChannel { Idx = i, Value = i * 0.2f });
            ChannelEqualizers_ = ChannelEqualizersDefault_;
            AudioLevels = AudioMetersExtension.Create();

            if (WasapiOut.IsSupportedOnCurrentPlatform)
                sout = new WasapiOut(true, AudioClientShareMode.Shared, 500, ThreadPriority.AboveNormal);
            else
                sout = new DirectSoundOut();
            sout.Stopped += Sout_Stopped;
        }
        ~AudioStreamManagerClient()
        {
            Dispose();
        }

        #region Get/Set
        public ObservableCollection<AudioMeters> AudioLevels { get; private set; }
        public bool IsActive { get => (tokenCancell != default) && !(bool)tokenCancell?.IsCancellationRequested; }
        public int ReadTimeout { get; set; } = 5;
        private float ChannelVolume_ = 0.5f;
        private float MasterVolume_ = 0.5f;
        private float MasterVolumeMute_ = 0.0f;
        public float MasterVolumeMute
        {
            get => MasterVolumeMute_;
            set
            {
                MasterVolumeMute_ = value;
                OnPropertyChanged(nameof(MasterVolumeMute));
            }
        }
        public float MasterVolume
        {
            get => MasterVolume_;
            set
            {
                MasterVolumeMute = MasterVolume_;
                MasterVolume_ = value;
                ISoundOut iso = sout;
                if (iso != default)
                    iso.Volume = value;
                OnPropertyChanged(nameof(MasterVolume));
            }
        }
        public float ChannelVolume
        {
            get => ChannelVolume_;
            set
            {
                ChannelVolume_ = value;
                if ((gain != default) && gain.TryGetTarget(out GainSource g))
                    g.Volume = value;
                OnPropertyChanged(nameof(ChannelVolume));
            }
        }

        public MMDevice AudioRawDevice
        {
            get
            {
                if ((din != default) && din.TryGetTarget(out WasapiCapture w))
                    return w.Device;
                return default;
            }
        }

        #region Equalizer object property
        private Equalizer EqualizerObject
        {
            get
            {
                if (equalizer.TryGetTarget(out Equalizer eq))
                    return eq;
                return default;
            }
        }
        private readonly ObservableCollection<EqualizerChannelBase> ChannelEqualizersDefault_ = new();
        private ObservableCollection<EqualizerChannelBase> ChannelEqualizers_ = default;
        public ObservableCollection<EqualizerChannelBase> ChannelEqualizers
        {
            get => ChannelEqualizers_;
            set { ChannelEqualizers_ = value; OnPropertyChanged(nameof(ChannelEqualizers)); }
        }
        #endregion

        #region not use
        public string DeviceName { get => string.Empty; set {}}
        public AudioStreamFormat ChannelOutFormat { get => AudioStreamFormat.None; set {}}
        public IAudioChannel AudioChannel { get => default; set {}}
        public void SetAudioChannel(IAudioChannel channel) { }
        #endregion

        #endregion

        public void SetEventParent(Action<char, string, Exception> act) =>
            logHandle = new WeakReference<Action<char, string, Exception>>(act);
        public void SetEventCb() => EventCb += SubscribeLog;
        public void RemoveEventCb() => EventCb -= SubscribeLog;

        private async Task StopSinAsync(WasapiCapture a) => await Task.Run(() => a.Stop());
        private async Task StopSoutAsync(ISoundOut a) => await Task.Run(() => a.Stop());

        public async void ReStart(IAudioChannel channel)
        {
            Stop();
            await Task.Delay(2000).ConfigureAwait(false);
            Start(channel);
        }

        public void Start()
        {
            throw new NotImplementedException();
        }
        public async void Start(IAudioChannel channel)
        {
            try
            {
                lock (lock__)
                {
                    CancellationTokenSafe cts;
                    cts = tokenCancell;
                    if (cts != default)
                        return;

                    cts = tokenRun;
                    if (cts != default)
                        return;
                }
                if (channel.IsEnabled)
                    await ActiveControl(channel).ConfigureAwait(false);

            } catch (Exception e) { ToLog(this, '!', $"{nameof(Start)} ->", e); }
        }

        public void Stop()
        {
            try
            {
                lock (lock__)
                {
                    CancellationCancell(tokenCancell);
                    CancellationCancell(tokenRun);
                }
            } catch (Exception e) { ToLog(this, '!', $"{nameof(Stop)} ->", e); }
        }

        #region Dispose
        public void Dispose(bool b)
        {
            try
            {
                ISoundOut iso = sout;
                sout = b ? default : sout;
                IsDesposed = b;

                if (iso != default)
                {
                    switch (iso.PlaybackState)
                    {
                        case PlaybackState.Playing:
                        case PlaybackState.Paused:
                            {
                                iso.Stop();
                                break;
                            }
                    }
                    if (b)
                        try { iso.Dispose(); } catch { }
                }
                if ((din != default) && din.TryGetTarget(out WasapiCapture wc))
                    try { wc.Stop(); } catch { }

                CancellationTokenSafe cts;
                cts = tokenCancell;
                tokenCancell = default;
                CancellationDispose(cts);

                cts = tokenRun;
                tokenRun = default;
                CancellationDispose(cts);
            }
            catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(Dispose)}", LogWriter); }
        }
        public override void Dispose()
        {
            Dispose(true);
            base.Dispose();
        }
        #endregion

        private async Tasks.Task ActiveControl(IAudioChannel channel)
        {
            lock (lock__)
            {
                if (tokenCancell != default)
                    return;
                tokenCancell = new();
            }
            if (IsDesposed)
                throw new ArgumentNullException(nameof(IsDesposed));

            await Tasks.Task.Run(async () =>
            {
                MMDevice mmd = default;
                using WasapiCapture sin = new(true, AudioClientShareMode.Shared, 500, channel.GetWaveFormat());

                try
                {
                    tokenCancell?.Token.ThrowIfCancellationRequested();

                    if (string.IsNullOrWhiteSpace(channel.Tag))
                        throw new DeviceTagNotFoundException(channel.DeviceId);

                    if (!string.IsNullOrWhiteSpace(channel.DeviceId))
                    {
                        mmd = mmdevice.GetDeviceById(channel.DeviceId);
                        sin.Device = mmd ?? throw new DeviceNotFoundException(channel.Tag);
                    }

                    ChannelVolume = channel.Volume;
                    ChannelToEqualizer(channel);

                    using AudioMeterInformation am = AudioMeterInformation.FromDevice(sin.Device);

                    sin.Initialize();

                    using SoundInSource isrc = new(sin)
                    {
                        FillWithZeros = true
                    };
                    using var asrc = new GainSource(
                        channel.IsMono ? isrc.ToMono().ToSampleSource() : isrc.ToSampleSource())
                    {
                        Volume = ChannelVolume
                    };
                    using var eq = Equalizer.Create10BandEqualizer(asrc);
                    using var aout = eq.ToWaveSource(16);

                    channel.EqualizerSet(eq);
                    sin.Start();
                    sin.Stopped += Sin_Stopped;
                    sout.Initialize(aout);

                    din = new WeakReference<WasapiCapture>(sin);
                    gain = new WeakReference<GainSource>(asrc);
                    equalizer = new WeakReference<Equalizer>(eq);
                    OnPropertyChanged(nameof(AudioRawDevice));

                    sout.Volume = MasterVolume;
                    sout.Play();

                    tokenRun = new();
                    while (true)
                    {
                        try
                        {
                            AudioLevels.SetLevel(am.GetChannelsPeakValues());
                            await Task.Delay(TimeSpan.FromMilliseconds(10.0), tokenRun.Token);
                        }
                        catch (TaskCanceledException) { break; }
                        catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(ActiveControl)}", LogWriter); }
                    }
                    try
                    {
                        if (sout != default)
                            await StopSoutAsync(sout).ConfigureAwait(false);
                        if (sin != default)
                        {
                            await StopSinAsync(sin).ContinueWith((t) => {
                                sin.Stopped -= Sin_Stopped;
                                if (t != default)
                                    try { t.Dispose(); } catch { }
                            }).ConfigureAwait(false);
                            if (mmd != default)
                                mmd.Dispose();
                        }
                    } catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(ActiveControl)}", LogWriter); }

                    channel.Volume = ChannelVolume;
                    EqualizerToClear();
                    AudioLevels.SetLevel();

                } catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(ActiveControl)}", LogWriter); }
                finally
                {
                    lock (lock__)
                    {
                        din = default;
                        gain = default;
                        equalizer = default;
                        Dispose(false);
                    }
                    OnPropertyChanged(nameof(AudioRawDevice));
                }
            }, tokenCancell.Token);
        }

        private void ChannelToEqualizer(IAudioChannel channel)
        {
            foreach (EqualizerChannel c in channel.EqualizerChannels)
                c.ChangeValueEventCb += EqualizerChannel_Event;
            ChannelEqualizers = channel.EqualizerChannels;
        }

        private void EqualizerToClear()
        {
            foreach (EqualizerChannel c in ChannelEqualizers)
                c.ChangeValueEventCb -= EqualizerChannel_Event;
            ChannelEqualizers = ChannelEqualizersDefault_;
        }

        #region Event callback
        private void EqualizerChannel_Event(int i, float v)
        {
            try
            {
                Equalizer eq = EqualizerObject;
                if (eq == default)
                    return;
                eq.SampleFilters[i].AverageGainDB = v;
            } catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(EqualizerChannel_Event)}", LogWriter); }
        }
        private void Sin_Stopped(object sender, RecordingStoppedEventArgs e)
        {
            try
            {
                CancellationTokenSafe cts = tokenRun;
                tokenRun = default;
                CancellationDispose(cts);
            } catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(Sin_Stopped)}", LogWriter); }
        }
        private void Sout_Stopped(object sender, PlaybackStoppedEventArgs e)
        {
            try
            {
                CancellationTokenSafe cts = tokenRun;
                tokenRun = default;
                CancellationDispose(cts);
            } catch (Exception ex) { ex.WriteLog('!', $"Client:{nameof(Sout_Stopped)}", LogWriter); }

        }
        #endregion
    }
}
