﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE1006:Стили именования", Justification = "<Ожидание>", Scope = "member", Target = "~P:PlayListServiceStream.QueueAudioStream.length_")]
[assembly: SuppressMessage("CodeQuality", "IDE0079:Удалить ненужное подавление", Justification = "<Ожидание>", Scope = "member", Target = "~M:PlayListServiceStream.TagAudioStream.CheckHeaderSize")]
[assembly: SuppressMessage("CodeQuality", "IDE0079:Удалить ненужное подавление", Justification = "<Ожидание>", Scope = "type", Target = "~T:PlayListServiceStream.QueueAudioStream")]
[assembly: SuppressMessage("CodeQuality", "IDE0079:Удалить ненужное подавление", Justification = "<Ожидание>", Scope = "type", Target = "~T:PlayListServiceStream.TagAudioStream")]
