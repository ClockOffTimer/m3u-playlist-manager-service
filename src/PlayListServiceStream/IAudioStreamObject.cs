﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;

namespace PlayListServiceStream
{
    public interface IAudioStreamObject
    {
        string DeviceName { get; set; }
        bool IsActive { get; }
        AudioStreamFormat ChannelOutFormat { get; set; }
        int ReadTimeout { get; set; }
        IAudioChannel AudioChannel { get; set; }
        float ChannelVolume { get; set; }
        float MasterVolumeMute { get; set; }

        void Dispose();
        void SetEventCb();
        void SetEventParent(Action<char, string, Exception> act);
        void RemoveEventCb();
        void SetAudioChannel(IAudioChannel channel);
        void ReStart(IAudioChannel channel);
        void Start(IAudioChannel channel);
        void Start();
        void Stop();
    }
}