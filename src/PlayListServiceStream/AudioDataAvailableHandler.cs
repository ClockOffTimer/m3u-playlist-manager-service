﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using CSCore;
using CSCore.MediaFoundation;
using CSCore.SoundIn;
using PlayListServiceData.Utils;

namespace PlayListServiceStream
{
    public class AudioDataAvailableHandler
    {
        private readonly WeakReference<IWaveSource> src_;
        private readonly WeakReference<MediaFoundationEncoder> mfe_;
        private readonly CancellationTokenSafe token_;
        private readonly int bsize_;

        public AudioDataAvailableHandler(CancellationTokenSafe token, IWaveSource src, MediaFoundationEncoder mfe)
        {
            src_ = new WeakReference<IWaveSource>(src);
            mfe_ = new WeakReference<MediaFoundationEncoder>(mfe);
            token_ = token;
            bsize_ = (int)src?.WaveFormat.BytesPerSecond * 2;
        }
        public void StreamWrite(object obj, DataAvailableEventArgs args)
        {
            try
            {
                if ((!src_.TryGetTarget(out IWaveSource src)) ||
                    (!mfe_.TryGetTarget(out MediaFoundationEncoder mfe)))
                    return;

                byte[] buffer = new byte[bsize_];
                int n = (int)src?.Read(buffer, 0, buffer.Length);
                mfe?.Write(buffer, 0, n);
            }
            catch
            {
                if ((token_ != default) && !token_.IsCancellationRequested)
                    token_.Cancel();
            }
        }

    }
}
