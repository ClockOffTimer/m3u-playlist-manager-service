﻿/* Copyright (c) 2021-2022 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceStream.Exceptions
{
    [System.Serializable]
    public class DeviceTagNotFoundException : Exception
    {
        public DeviceTagNotFoundException() { }
        public DeviceTagNotFoundException(string message) : base(message) { }
        public DeviceTagNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected DeviceTagNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
