﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Diagnostics;

namespace PlayListServiceStream
{
    public class TagAudioStream
    {
        private static readonly byte[] _header = new byte[] { 0x49, 0x44, 0x33, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private static readonly byte[] _pad = new byte[] { 0x00, 0x00, 0x00 };
        private static readonly string[] _tags = new string[] { "TIT2", "TPE1", "WOAS", "TRCK", "TALB", "TYER" };

        private byte[] _cache = default;
        public byte[] TagsHeader
        {
            get => _cache;
            private set => _cache = value;
        }
        public bool DefaultEncoding { get; set; } = false;
        private Encoding GetEncoding() =>
            DefaultEncoding ? Encoding.Default : Encoding.UTF8;

        public TagAudioStream(string title, int bps0, int bps1, int chan, int id)
        {
            Build(title, bps0, bps1, chan, id);
        }

        public void Build(string title, int bps0, int bps1, int chan, int id = 0)
        {
            try
            {
                using MemoryStream ms = new();
                ms.Write(_header, 0, _header.Length);

                if (!string.IsNullOrWhiteSpace(title))
                    WriteBlock(ms, title, 0);
                if (id >= 0)
                    WriteBlock(ms, id.ToString(), 3);

                DateTime dt = DateTime.Now;
                WriteBlock(ms, dt.ToString("MM-dd-yyyy HH:mm"), 4);
                WriteBlock(ms, dt.Year.ToString(), 5);
                WriteBlock(ms, $"Audio: {bps0}/{bps1}:{chan}", 1);

                _cache = ms.ToArray();

                for (int i = 6, m = 0, n = _cache.Length - 10; i < 10; i++, m++)
                    _cache[i] = (byte)(n >> ((3 - m) * 7) & 0x7f);
#           if DEBUG
                CheckHeaderSize();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"--- TAG Audio Stream: {ex}");
                _cache = default;
            }
#           else
            } catch { _cache = default; }
#           endif
        }

        private void WriteBlock(MemoryStream ms, string s, int idx)
        {
            byte[] b;

            b = Encoding.Default.GetBytes(_tags[idx]);
            ms.Write(b, 0, b.Length);

            b = GetEncoding().GetBytes(s);
            ms.Write(_pad, 0, _pad.Length);
            ms.Write(new byte[] { (byte)(b.Length + 1) }, 0, 1);
            ms.Write(_pad, 0, _pad.Length);
            ms.Write(b, 0, b.Length);
        }

        [Conditional("DEBUG")]
#       pragma warning disable IDE0051
        private void CheckHeaderSize()
        {
            int length = _cache[6] << 21 | _cache[7] << 14 | _cache[8] << 7 | _cache[9];
            Debug.WriteLine($"--- TAG Header length:{length}/{_cache.Length - 10}");
        }
#       pragma warning restore IDE0051
    }
}
