﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceStream.Data
{
    public interface IEqualizerChannel
    {
        int Idx { get; set; }
        float Value { get; set; }

        EqualizerChannelBase Copy(IEqualizerChannel a);
    }
}