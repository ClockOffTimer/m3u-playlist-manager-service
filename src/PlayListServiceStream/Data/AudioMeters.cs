﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.ObjectModel;

namespace PlayListServiceStream.Data
{
    public class AudioMeters : PropertyChangedEvent
    {
        public float Value { get => Value_; set { Value_ = value; OnPropertyChanged(nameof(Value)); } }
        private float Value_ = 0.0f;
    }

    public static class AudioMetersExtension
    {
        public static ObservableCollection<AudioMeters> Create()
        {
            var list = new ObservableCollection<AudioMeters>();
            list.Add(new AudioMeters()); list.Add(new AudioMeters());
            return list;
        }
        public static void SetLevel(this ObservableCollection<AudioMeters> list)
        {
            list[0].Value = 0.0f;
            list[1].Value = 0.0f;
        }
        public static void SetLevel(this ObservableCollection<AudioMeters> list, float [] data)
        {
            if (data == default)
                return;
            if (data.Length >= 1)
                list[0].Value = data[0];
            if (data.Length >= 2)
                list[1].Value = data[1];
        }
    }
}
