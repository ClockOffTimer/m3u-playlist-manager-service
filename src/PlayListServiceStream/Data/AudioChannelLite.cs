﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.ComponentModel;
using System.Xml.Serialization;
using PlayListServiceStream.Base;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioChannelLite
    {
        [XmlElement("id")]
        public int Id { get; set; } = -1;
        [XmlElement("tag")]
        public string Tag { get; set; } = default;
        [XmlElement("mono")]
        public bool IsMono { get; set; } = false;
        [XmlElement("volume")]
        public float Volume { get; set; } = 0.5f;
        [XmlElement("equalizer")]
        public float[] Equalizer { get; set; } = new float[10];
        [XmlElement("type")]
        public AudioStreamType Atype { get; set; } = AudioStreamType.None;

        public AudioChannelLite() { }
        public AudioChannelLite(int id, IAudioChannel channel)
        {
            Id = id;
            Tag = channel.Tag;
            IsMono = channel.IsMono;
            Volume = channel.Volume;
            Atype = channel.Atype;
            Equalizer = channel.EqualizerChannels.Select(x => x.Value).ToArray();
        }
    }
}
