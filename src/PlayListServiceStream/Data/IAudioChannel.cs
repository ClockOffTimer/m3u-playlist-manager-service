﻿/* Copyright (c) 2021-2022 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using CSCore;
using CSCore.Streams.Effects;
using PlayListServiceStream.Base;

namespace PlayListServiceStream.Data
{
    public interface IAudioChannel
    {
        int Id { get; set; }
        int BitsPerSample { get; set; }
        int BytesPerSecond { get; set; }
        int Channels { get; set; }
        int PreCache { get; set; }
        bool IsEnabled { get; set; }
        bool IsMono { get; set; }
        float Volume { get; set; }
        string Tag { get; set; }
        string DeviceId { get; set; }
        AudioStreamType Atype { get; set; }
        AudioStreamFormat OutFormatTag { get; set; }
        AudioEncoding WaveFormatTag { get; set; }
        TagAudioStream Id3Tag { get; }
        ObservableCollection<EqualizerChannelBase> EqualizerChannels { get; set; }

        IEqualizerChannel Get(int idx);
        void Add(int idx, float val);
        void Clear();
        void DefaultEqChannels();
        IAudioChannel Default();
        IAudioChannel Default(string s, bool b, bool isclient = false);
        IAudioChannel Default(string name, string did, bool b, bool isclient = false);

        IAudioChannel Copy(IAudioChannel a, bool isclient = false);
        void Copy(IEnumerable<IEqualizerChannel> list);

        void EqualizerSet(Equalizer eq);
        void EqualizerSet(Equalizer eq, int idx, float val);
        void EqualizerSet(Equalizer eq, IEnumerable<IEqualizerChannel> list);
        void EqualizerSet(int idx, float val);
        WaveFormat GetWaveFormat();

        event PropertyChangedEventHandler PropertyChanged;
        void RefreshPropertyChanged();
    }
}