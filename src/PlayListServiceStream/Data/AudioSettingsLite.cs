﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioSettingsLite
    {
        [XmlElement("channels")]
        public List<AudioChannelLite> AudioChannels = new();
        public AudioSettingsLite Add(AudioChannelLite acr) { AudioChannels.Add(acr); return this; }
        public AudioSettingsLite AddRange(IEnumerable<AudioChannelLite> list) { AudioChannels.AddRange(list); return this; }
        public int Count() => AudioChannels.Count;
    }
}
