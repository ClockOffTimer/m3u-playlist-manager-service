﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlInclude(typeof(EqualizerChannel))]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public abstract class EqualizerChannelBase : PropertyChangedEvent, IEqualizerChannel
    {
        public delegate void ChangeValueEvent(int i, float v);
        public event ChangeValueEvent ChangeValueEventCb = delegate { };

        [XmlElement("id")]
        public int Idx { get => Idx_; set { Idx_ = value; Notify_(nameof(Value), Idx, Value); }}
        protected int Idx_ = -1;
        [XmlElement("val")]
        public float Value { get => Value_; set { Value_ = value; Notify_(nameof(Value), Idx, Value); }}
        protected float Value_ = 0.0f;

        public virtual EqualizerChannelBase Copy(IEqualizerChannel a)
        {
            Idx = a.Idx;
            Value = a.Value;
            return this;
        }
        private void Notify_(string s, int idx, float val)
        {
            ChangeValueEventCb?.Invoke(idx, val);
            OnPropertyChanged(s);
        }
    }
}
