﻿/* Copyright (c) 2021 WpfPluginAudio, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;

namespace PlayListServiceStream.Data
{
    public abstract class PropertyChangedEvent : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChanged(params string[] names)
        {
            foreach (var s in names)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }
        public virtual void RefreshPropertyChanged()
        {

        }
    }
}
