﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Xml.Serialization;
using CSCore;
using CSCore.Streams.Effects;
using PlayListServiceStream.Base;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlInclude(typeof(AudioChannel))]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public abstract class AudioChannelBase : PropertyChangedEvent, IAudioChannel
    {
        [XmlElement("id")]
        public int Id { get => Id_; set { Id_ = value; OnPropertyChanged(nameof(Id)); } }
        protected int Id_ = -1;
        [XmlElement("tag")]
        public string Tag { get => Tag_; set { Tag_ = value; OnPropertyChanged(nameof(Tag)); }}
        protected string Tag_ = default;
        [XmlElement("enable")]
        public bool IsEnabled { get => IsEnable_; set { IsEnable_ = value; OnPropertyChanged(nameof(IsEnabled)); }}
        protected bool IsEnable_ = false;
        [XmlElement("mono")]
        public bool IsMono { get => IsMono_; set { IsMono_ = value; OnPropertyChanged(nameof(IsMono)); }}
        protected bool IsMono_ = false;
        [XmlElement("volume")]
        public float Volume { get => Volume_; set { Volume_ = value; OnPropertyChanged(nameof(Volume)); }}
        protected float Volume_ = 0.9f;
        [XmlElement("bytes")]
        public int BytesPerSecond { get => BytesPerSecond_; set { BytesPerSecond_ = value; OnPropertyChanged(nameof(BytesPerSecond)); }}
        protected int BytesPerSecond_ = 44100;
        [XmlElement("bits")]
        public int BitsPerSample { get => BitsPerSample_; set { BitsPerSample_ = value; OnPropertyChanged(nameof(BitsPerSample)); }}
        protected int BitsPerSample_ = 16;
        [XmlElement("channels")]
        public int Channels { get => Channels_; set { Channels_ = value; OnPropertyChanged(nameof(Channels)); }}
        protected int Channels_ = 2;
        [XmlElement("precache")]
        public int PreCache { get => PreCache_; set { PreCache_ = value; OnPropertyChanged(nameof(PreCache)); } }
        protected int PreCache_ = 250;
        [XmlElement("type")]
        public AudioStreamType Atype { get => Atype_; set { Atype_ = value; OnPropertyChanged(nameof(Atype)); }}
        protected AudioStreamType Atype_ = AudioStreamType.A_2_16_44100;
        [XmlElement("infmt")]
        public AudioEncoding WaveFormatTag { get => WaveFormatTag_; set { WaveFormatTag_ = value; OnPropertyChanged(nameof(WaveFormatTag)); }}
        protected AudioEncoding WaveFormatTag_ = AudioEncoding.Adpcm;
        [XmlElement("outfmt")]
        public AudioStreamFormat OutFormatTag { get => OutFormatTag_; set { OutFormatTag_ = value; OnPropertyChanged(nameof(OutFormatTag)); } }
        protected AudioStreamFormat OutFormatTag_ = AudioStreamFormat.Mp3;
        [XmlElement("equalizer")]
        public ObservableCollection<EqualizerChannelBase> EqualizerChannels { get => EqualizerChannels_; set { EqualizerChannels_ = value; OnPropertyChanged(nameof(EqualizerChannels)); } }
        protected ObservableCollection<EqualizerChannelBase> EqualizerChannels_ = new();

        public virtual WaveFormat GetWaveFormat() { throw new NotImplementedException(); }

        [XmlIgnore]
        public TagAudioStream Id3Tag { get; protected set; }
        [XmlIgnore]
        public string DeviceId { get; set; } = string.Empty;

        #region EqualizerChannels
        public virtual IEqualizerChannel Get(int idx) { throw new NotImplementedException(); }
        public virtual void Add(int idx, float val) { throw new NotImplementedException(); }
        public virtual void Clear() { throw new NotImplementedException(); }
        public virtual void DefaultEqChannels() { throw new NotImplementedException(); }
        #endregion

        #region Default object
        public virtual IAudioChannel Default() { throw new NotImplementedException(); }
        public virtual IAudioChannel Default(string s, bool b, bool isclient = false) { throw new NotImplementedException(); }
        public virtual IAudioChannel Default(string name, string did, bool b, bool isclient = false) { throw new NotImplementedException(); }
        #endregion

        #region Copy object
        public virtual IAudioChannel Copy(IAudioChannel a, bool isclient = false) { throw new NotImplementedException(); }
        public virtual void Copy(IEnumerable<IEqualizerChannel> list) { throw new NotImplementedException(); }
        #endregion

        #region Equalizer set
        public virtual void EqualizerSet(Equalizer eq) { throw new NotImplementedException(); }
        public virtual void EqualizerSet(Equalizer eq, int idx, float val) { throw new NotImplementedException(); }
        public virtual void EqualizerSet(Equalizer eq, IEnumerable<IEqualizerChannel> list) { throw new NotImplementedException(); }
        public virtual void EqualizerSet(int idx, float val) { throw new NotImplementedException(); }
        #endregion
    }
}
