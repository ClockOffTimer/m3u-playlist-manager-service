﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using PlayListServiceData.Process.Log;
using PlayListServiceStream.Base;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class AudioSettings : PropertyChangedEvent, IDisposable
    {
        private readonly bool isclient_;
        private readonly string path_;
        private readonly ILogManager LogWriter = default;

        [XmlIgnore]
        public AudioSettings SavedAudioSettings { get; private set; } = default;
        [XmlIgnore]
        public AudioStreamStateEvent State = new();

        public AudioSettings() { path_ = default; MMDevice_ = default; isclient_ = false; }
        public AudioSettings(bool isclient) { path_ = default; MMDevice_ = default; isclient_ = isclient; }
        public AudioSettings(string path, ILogManager log, bool isclient = false)
        {
            LogWriter = log;
            MMDevice_ = new(log);
            path_ = path;
            isclient_ = isclient;
            Load(path, isclient_);
        }

        #region MMDevice/MMDeviceList/MMNotification
        [XmlIgnore]
        public AudioMMDevice MMDevice { get => MMDevice_; }
        private readonly AudioMMDevice MMDevice_;
        #endregion

        #region Channels
        [XmlElement("channels")]
        public ObservableCollection<AudioChannel> Channels {
            get => Channels_; set { Channels_ = value; OnPropertyChanged(nameof(Channels)); }
        }
        private ObservableCollection<AudioChannel> Channels_ = new();
        public void Clear() => Channels.Clear();
        public void Add(IAudioChannel a)
        {
            if ((from i in Channels
                 where i.Tag != default && i.Tag.Equals(a.Tag)
                 select i).FirstOrDefault() == default)
                Channels.Add((AudioChannel)a);
        }
        public IAudioChannel Get(string tag) => (from i in Channels
                                                 where i.Tag != default && tag.Equals(i.Tag)
                                                 select i).FirstOrDefault();
        #endregion

        public AudioSettings Copy()
        {
            AudioSettings a = SavedAudioSettings;
            SavedAudioSettings = default;
            if (a == default)
                return default;
            return Copy(a);
        }

        public AudioSettings Copy(AudioSettings a)
        {
            Channels.Clear();
            for (int i = 0; i < a.Channels.Count; i++)
                Channels.Add((AudioChannel)new AudioChannel(i).Copy(a.Channels[i], isclient_));
            return this;
        }

        public void DisposeService()
        {
            try
            {
                foreach (var c in Channels)
                    c.Clear();
                Channels.Clear();
                Dispose();

            } catch (Exception ex) { ex.WriteLog('!', $"Settings:{nameof(DisposeService)}", LogWriter); }
        }
        public void Dispose()
        {
            /* Channels.Clear();  Gui dispatcher!!! */
            try { MMDevice.Dispose(); }
            catch (Exception ex) { ex.WriteLog('!', $"Settings:{nameof(Dispose)}", LogWriter); }
        }

        public void Load(string p = default, bool isui = false)
        {
            string cnfpath = (p == default) ? path_ : p;
            State.CallEvent(AudioStreamState.Load, false);

            _ = Task<AudioSettings>.Run(() => {
                  try {
                      do {
                          if (cnfpath == default)
                              break;

                          FileInfo f = new(cnfpath);
                          if ((f == default) || !f.Exists)
                              break;

                          using FileStream stream = File.Open(f.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                          using StreamReader sr = new(stream, new UTF8Encoding(false), false);
                          XmlSerializer xml = new(typeof(AudioSettings));
                          return xml.Deserialize(sr) as AudioSettings;

                      } while (false);
                  } catch (Exception ex) { ex.WriteLog('!', $"Settings:{nameof(Load)}:Xml", LogWriter); }
                  return default;

            }).ContinueWith((t) =>
            {
                do
                {
                    if (t == default)
                        break;
                    if (t.Exception != default)
                        t.Exception.WriteLog('!', $"Settings:{nameof(Load)}:Task", LogWriter);
                    if ((t.Status == TaskStatus.Canceled) || (t.Status == TaskStatus.Faulted))
                        break;
                    if (t is Task<AudioSettings> tr)
                    {
                        if (!tr.IsCompleted)
                            break;
                        if ((tr.Result == default) || (tr.Result.Channels.Count == 0))
                            break;
                        if (isui)
                            SavedAudioSettings = tr.Result;
                        else
                            _ = Copy(tr.Result);
                    }

                } while (false);

                if (t != default)
                    try { t.Dispose(); } catch { }

                State.CallEvent(AudioStreamState.Load, true);
            });
        }

        public async Task Save(string p = default)
        {
            string cnfpath = (p == default) ? path_ : p;
            State.CallEvent(AudioStreamState.Save, false);

            await Task.Run(() =>
            {
                try {
                    if (cnfpath == default)
                        return;

                    AudioSettings settings = new(isclient_);
                    foreach (AudioChannel c in Channels)
                        if (c.IsEnabled)
                            settings.Add(c);

                    if (settings.Channels.Count == 0)
                        return;

                    using FileStream stream = File.Open(cnfpath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                    using TextWriter tw = new StreamWriter(stream, new UTF8Encoding(false));
                    XmlSerializer xml = new(typeof(AudioSettings));
                    xml.Serialize(tw, settings);
                }
                catch (Exception ex) { ex.WriteLog('!', $"Settings:{nameof(Save)}", LogWriter); }
                State.CallEvent(AudioStreamState.Save, true);
            });
        }

        public void Delete(string p = default)
        {
            try {
                string cnfpath = (p == default) ? path_ : p;
                if (cnfpath == default)
                    return;

                FileInfo f = new(cnfpath);
                if ((f == default) || f.IsReadOnly || !f.Exists)
                    return;
                f.Delete();
                State.CallEvent(AudioStreamState.Delete, true);

            } catch (Exception ex) { ex.WriteLog('!', $"Settings:{nameof(Delete)}", LogWriter); }
        }
    }
}
