﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioStatLite
    {
        [XmlElement("id")]
        public int Id = -1;
        [XmlElement("device")]
        public string Device = string.Empty;
        [XmlElement("clients")]
        public List<string> Clients = new ();
        [XmlElement("count")]
        public int Count = 0;

        public AudioStatLite() { }
        public AudioStatLite(string d, List<string> c, int i)
        {
            Id = i;
            Device = d;
            Clients = c;
            Count = Clients.Count;
        }
    }
}
