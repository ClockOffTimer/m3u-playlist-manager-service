﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Net;
using System.Threading;
using System.Xml.Serialization;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]

    public class AudioStreamClient : PropertyChangedEvent
    {
        [XmlElement("sid")]
        public int Sid
        {
            get => Sid_;
            private set { Sid_ = value; OnPropertyChanged(nameof(Sid)); }
        }
        private int Sid_ = -1;

        [XmlElement("tid")]
        public int Tid
        {
            get => Tid_;
            private set { Tid_ = value; OnPropertyChanged(nameof(Tid)); }
        }
        private int Tid_ = -1;

        [XmlIgnore]
        public Action<byte[]> Act { get; set; } = (a) => { };

        [XmlIgnore]
        public IPEndPoint IPClient { get; set; } = default;

        public IAudioChannel ChannelSetting { get; set; } = default;

        public AudioStreamClient() { }
        public AudioStreamClient(IPEndPoint ip, Action<byte[]> a, IAudioChannel chs = default)
        {
            Tid = Thread.CurrentThread.ManagedThreadId;
            Act = a;
            IPClient = ip;
            ChannelSetting = chs;
        }
        public void Disable() => Tid = -1;
        public AudioStreamClient SendTagsHeader()
        {
            try {
                if ((ChannelSetting != default) && (ChannelSetting.Id3Tag.TagsHeader != default))
                    Act.Invoke(ChannelSetting.Id3Tag.TagsHeader);
            } catch { }
            return this;
        }
    }
}
