﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.Linq;
using CSCore;
using CSCore.Streams.Effects;
using PlayListServiceStream.Base;

namespace PlayListServiceStream.Data
{
    public class AudioChannel : AudioChannelBase
    {
        public AudioChannel() { }
        public AudioChannel(int id) => Id = id;

        #region EqualizerChannels
        public override void Clear() => EqualizerChannels.Clear();
        public override IEqualizerChannel Get(int idx) => (from i in EqualizerChannels
                                                  where i.Idx == idx
                                                  select i).FirstOrDefault();
        public override void Add(int idx, float val)
        {
            var a = Get(idx);
            if (a == default)
                EqualizerChannels.Add(new EqualizerChannel { Idx = idx, Value = val });
            else
                a.Value = val;
        }
        #endregion

        #region EqualizerSet
        public override void EqualizerSet(Equalizer eq)
        {
            if (eq != default)
                foreach (var es in EqualizerChannels)
                    eq.SampleFilters[es.Idx].AverageGainDB = es.Value;
        }
        public override void EqualizerSet(Equalizer eq, IEnumerable<IEqualizerChannel> list)
        {
            if ((list != default) && (list.Count() == 10))
                foreach (var es in list)
                {
                    EqualizerChannels[es.Idx].Value = es.Value;
                    if (eq != default)
                        eq.SampleFilters[es.Idx].AverageGainDB = es.Value;
                }
        }
        public override void EqualizerSet(Equalizer eq, int idx, float val)
        {
            if ((EqualizerChannels.Count != 10) || (idx < 0) || (idx >= 10))
                return;
            EqualizerChannels[idx].Value = val;
            if (eq != default)
                eq.SampleFilters[idx].AverageGainDB = val;
        }
        public override void EqualizerSet(int idx, float val)
        {
            EqualizerSet(default, idx, val);
        }
        #endregion

        public override WaveFormat GetWaveFormat() =>
            new(BytesPerSecond, BitsPerSample, Channels, WaveFormatTag);

        public override void RefreshPropertyChanged()
        {
            OnPropertyChanged(new string[] {
                nameof(Tag),
                nameof(DeviceId),
                nameof(IsEnabled),
                nameof(IsMono),
                nameof(Volume),
                nameof(BytesPerSecond),
                nameof(BitsPerSample),
                nameof(Channels),
                nameof(Atype),
                nameof(WaveFormatTag),
                nameof(OutFormatTag)
            });
        }

        #region Copy/Default
        public override IAudioChannel Copy(IAudioChannel a, bool isclient = false)
        {
            Id = (Id < 0) ? a.Id : Id;
            Tag = a.Tag;
            DeviceId = a.DeviceId;
            IsEnabled = a.IsEnabled;
            IsMono = a.IsMono;
            Volume = a.Volume;
            PreCache = a.PreCache;
            BytesPerSecond = a.BytesPerSecond;
            BitsPerSample = a.BitsPerSample;
            Channels = a.Channels;
            WaveFormatTag = a.WaveFormatTag;
            Id3Tag = isclient ? default : new TagAudioStream(Tag, BytesPerSecond, BitsPerSample, Channels, Id);

            if (a.Atype == AudioStreamType.None)
                Atype = AudioStreamTypeString.GetStreamType(new int[] { Channels, BitsPerSample, BytesPerSecond });
            else
                Atype = a.Atype;

            EqualizerChannels.Clear();
            foreach (var c in a.EqualizerChannels)
                EqualizerChannels.Add(new EqualizerChannel().Copy(c));

            return this;
        }
        public override void Copy(IEnumerable<IEqualizerChannel> list)
        {
            foreach (var eq in list)
                EqualizerChannels[eq.Idx].Value = eq.Value;
        }
        public override IAudioChannel Default()
        {
            return Default(string.Empty, false);
        }
        public override IAudioChannel Default(string name, string did, bool b, bool isclient = false)
        {
            DeviceId = did;
            return Default(name, b, isclient);
        }
        public override IAudioChannel Default(string s, bool b, bool isclient = false)
        {
            Tag = s;
            Channels = 2;
            IsEnabled = b;
            IsMono = false;
            Volume = 0.9f;
            PreCache = 200;
            BitsPerSample = 16;
            BytesPerSecond = 48000;
            WaveFormatTag = AudioEncoding.Adpcm;
            OutFormatTag = AudioStreamFormat.Mp3;
            Atype = AudioStreamType.A_2_16_48000;
            Id3Tag = isclient ? default : new TagAudioStream(Tag, BytesPerSecond, BitsPerSample, Channels, Id);

            EqualizerChannels.Clear();
            for (int i = 0; i < 10; i++)
                EqualizerChannels.Add(new EqualizerChannel { Idx = i, Value = 0.2f });
            return this;
        }
        public override void DefaultEqChannels()
        {
            int idx = EqualizerChannels.Count;
            if (idx > 10)
                for (int i = (idx - 10 - 1); i >= 10; i--)
                    EqualizerChannels.RemoveAt(i);
            else if (idx < 10)
                for (int i = 0; i < (10 - idx); i++)
                    EqualizerChannels.Add(new EqualizerChannel { Idx = i, Value = 0.2f });
        }
        #endregion
    }
}
