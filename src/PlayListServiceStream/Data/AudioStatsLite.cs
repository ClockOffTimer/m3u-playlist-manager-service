﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceStream.Data
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class AudioStatsLite
    {
        [XmlElement("devices")]
        public List<AudioStatLite> Devices = new();
        [XmlElement("count")]
        public int Count = 0;

        public void Add(Tuple<string, List<string>> t)
        {
            Devices.Add(new AudioStatLite(t.Item1, t.Item2, Devices.Count));
            Count = Devices.Count;
        }
        public void AddRange(List<Tuple<string, List<string>>> list)
        {
            foreach (var t in list)
                Devices.Add(new AudioStatLite(t.Item1, t.Item2, Devices.Count));
            Count = Devices.Count;
        }
    }
}
