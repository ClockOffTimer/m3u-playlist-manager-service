﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */


namespace PlayListServiceStream.Base
{
    public enum AudioStreamFormat : int
    {
        None = 0,
        Pcm = 1,
        APcm = 2,
        Wav,
        Mp3,
        Wma,
        Aac
    };
}
