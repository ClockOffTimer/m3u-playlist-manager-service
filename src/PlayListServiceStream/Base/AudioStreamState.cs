﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceStream.Base
{
    public enum AudioStreamState : int
    {
        None = 0,
        Change = 1,
        Add = 2,
        Remove,
        Delete,
        Load,
        Save,
        ChangeDefault
    };
}
