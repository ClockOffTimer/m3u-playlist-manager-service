﻿/* Copyright (c) 2021-2022 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CSCore.CoreAudioAPI;
using PlayListServiceData.Process.Log;
using PlayListServiceStream.Data;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceStream.Base
{
    public class AudioMMDevice : PropertyChangedEvent, IDisposable
    {
        private readonly object lock__ = new();
        private readonly ILogManager LogWriter = default;

        private static MMDeviceEnumerator de_ = default;
        private static MMNotificationClient dn_ = default;
        private static List<MMDevice> ld_ { get; set; } = default;

        public AudioMMDevice(ILogManager log)
        {
            LogWriter = log;
            Init();
        }
        ~AudioMMDevice() => Dispose();

        #region Get/Set
        private bool DeviceListCheck() => ((AudioMMDevice.ld_ != default) && (AudioMMDevice.ld_.Count >= 0));
        private bool DeviceEnumeratorCheck() => ((AudioMMDevice.de_ != default) && !AudioMMDevice.de_.IsDisposed);
        private bool DeviceNotificationCheck() => (AudioMMDevice.dn_ != default);

        public List<MMDevice> DeviceList
        {
            get
            {
                if (!DeviceListCheck())
                {
                    Init();
                    OnPropertyChanged(nameof(DeviceList));
                }
                return AudioMMDevice.ld_;
            }
        }
        public MMDeviceEnumerator DeviceEnumerator
        {
            get
            {
                if (!DeviceEnumeratorCheck())
                    lock (lock__)
                    {
                        (bool _, MMDeviceEnumerator mme) = AudioMMDevice.GetMMDeviceEnumeratorInternal();
                        OnPropertyChanged(nameof(DeviceEnumerator));
                        return mme;
                    }
                return AudioMMDevice.de_;
            }
        }
        public MMNotificationClient DeviceNotification
        {
            get
            {
                if (!DeviceNotificationCheck())
                    lock (lock__)
                    {
                        (bool _, MMNotificationClient mmn) = AudioMMDevice.GetMMNotificationInternal();
                        OnPropertyChanged(nameof(DeviceNotification));
                        return mmn;
                    }
                return AudioMMDevice.dn_;
            }
        }
        #endregion

        #region Dispose
        public void Dispose() => DisposeDevices(true);
        public void DisposeDevices(bool b = false)
        {
            lock (lock__)
            {
                AudioMMDevice.DisposeMMDeviceInternal();
                AudioMMDevice.DisposeMMDeviceEnumeratorInternal();
                if (b) AudioMMDevice.DisposeMMNotificationInternal();
            }
        }
        #endregion

        #region Init
        private void Init()
        {
            if (DeviceListCheck())
                return;
            try {
                MMDeviceEnumerator dev = default;
                if (!DeviceEnumeratorCheck())
                    dev = DeviceEnumerator;

                if (dev != default) {
                    lock (lock__)
                        AudioMMDevice.ld_ = GetDevicesInternal(dev, DeviceState.Active);
                    if (DeviceListCheck())
                        OnPropertyChanged(nameof(GetDevices));
                }
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(Init), LogWriter); }
        }
        #endregion

        #region GetDevice* = MMDevice
        public List<string> GetDevicesName()
        {
            try
            {
                if (!CreateListDeviceInternal())
                    return new List<string>();

                List<string> list = default;
                lock (lock__)
                    list = (from i in DeviceList
                            where (i != default) && (i.FriendlyName != default)
                            select i.FriendlyName).ToList();
                return list;
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDevicesName)}", LogWriter); }
            return new List<string>();
        }
        public List<Tuple<string, string>> GetDevicesBase()
        {
            try
            {
                if (!CreateListDeviceInternal())
                    return new List<Tuple<string, string>>();

                List<Tuple<string, string>> list = default;
                lock (lock__)
                    list = (from i in DeviceList
                            where (i != default) && (i.FriendlyName != default)
                            select new Tuple<string, string>(i.FriendlyName, i.DeviceID)).ToList();
                return list;
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDevicesBase)}", LogWriter); }
            return new List<Tuple<string, string>>();
        }
        public string GetDeviceId(string s)
        {
            try
            {
                if (!CreateListDeviceInternal())
                    return string.Empty;

                lock (lock__)
                    return (from i in DeviceList
                            where (i != default) && (i.FriendlyName != default) && s.Equals(i.FriendlyName)
                            select i.DeviceID).FirstOrDefault();
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDeviceId)}", LogWriter); }
            return string.Empty;
        }

        public bool IsDeviceFound(string s)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(s) || !CreateListDeviceInternal())
                    return false;
                lock (lock__)
                    return (from i in DeviceList
                            where (i != default) && (i.FriendlyName != default) && s.Equals(i.FriendlyName)
                            select i).FirstOrDefault() != default;
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(IsDeviceFound)}:{s}", LogWriter); }
            return false;
        }

        public MMDevice GetDeviceById(string s)
        {
            try
            {
                MMDeviceEnumerator mme = DeviceEnumerator;
                if (mme == default)
                    lock (lock__)
                        (_, mme) = AudioMMDevice.GetMMDeviceEnumeratorInternal();
                if (mme != default)
                    return mme.GetDevice(s);
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDeviceById)}:{s}", LogWriter); }
            finally
            {
                lock (lock__)
                    AudioMMDevice.DisposeMMDeviceEnumeratorInternal();
                OnPropertyChanged(nameof(GetDeviceById));
            }
            return default;
        }

        public MMDevice GetDevice(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return default;
            try
            {
                if (!CreateListDeviceInternal())
                    return default;

                lock (lock__)
                {
                    var mmd = (from i in DeviceList
                               where (i != default) && (i.FriendlyName != default) && s.Equals(i.FriendlyName)
                               select i).FirstOrDefault();

                    if (mmd != default)
                    {
                        DeviceList.Remove(mmd);
                        OnPropertyChanged(nameof(GetDevice));
                    }
                    return mmd;
                }
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDevice)}:{s}", LogWriter); }
            return default;
        }

        public List<MMDevice> GetDevices()
        {
            if (!DeviceListCheck())
                Init();
            OnPropertyChanged(nameof(GetDevices));
            return DeviceList;
        }
        #endregion

        #region private
        private bool CreateListDeviceInternal()
        {
            try {
                if (!DeviceListCheck())
                {
                    Init();
                    if (!DeviceListCheck())
                        return false;
                }
                OnPropertyChanged(nameof(DeviceList));
                return true;
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(CreateListDeviceInternal)}", LogWriter); }
            return false;
        }

        private List<MMDevice> GetDevicesInternal(MMDeviceEnumerator dev, DeviceState ds = DeviceState.Active)
        {
            if (dev == default)
                return default;
            try
            {
                lock (lock__)
                {
                    List<MMDevice> list = default;
                    CancellationTokenSource cts = new(TimeSpan.FromSeconds(2));
                    try
                    {
                        while (list == default)
                        {
                            var mlist = dev.EnumAudioEndpoints(DataFlow.Capture, ds);
                            if ((mlist != default) && (mlist.Count > 0))
                            {
                                list = mlist.ToList();
                                break;
                            }
                            if (cts.IsCancellationRequested)
                                break;
                            Tasks.Task.Delay(250);
                        }
                    }
                    catch { }
                    finally
                    {
                        cts.Dispose();
                    }
                    if ((list != default) && (list.Count > 0))
                        OnPropertyChanged(nameof(DeviceList));
                    return list;
                }
            }
            catch (Exception ex) { ex.WriteLog('!', $"MMDevice:{nameof(GetDevicesInternal)}", LogWriter); }
            return default;
        }

        #region static
        private static (bool, MMDeviceEnumerator) GetMMDeviceEnumeratorInternal()
        {
            if (AudioMMDevice.de_ == default)
            {
                AudioMMDevice.de_ = new();
                return (true, AudioMMDevice.de_);
            }
            return (false, AudioMMDevice.de_);
        }
        private static (bool, MMNotificationClient) GetMMNotificationInternal()
        {
            if (AudioMMDevice.dn_ == default)
            {
                do
                {
                    (bool b, MMDeviceEnumerator mmd) = AudioMMDevice.GetMMDeviceEnumeratorInternal();
                    if (mmd == default)
                        break;
                    AudioMMDevice.dn_ = new(mmd);
                    return (b, AudioMMDevice.dn_);

                } while (false);
            }
            return (false, AudioMMDevice.dn_);
        }
        private static void DisposeMMDeviceEnumeratorInternal()
        {
            try {
                MMDeviceEnumerator mme = AudioMMDevice.de_;
                AudioMMDevice.de_ = default;
                if ((mme != default) && !mme.IsDisposed)
                    mme.Dispose();
            } catch {}
        }
        private static void DisposeMMNotificationInternal()
        {
            try {
                MMNotificationClient mmn = AudioMMDevice.dn_;
                AudioMMDevice.dn_ = default;
                if (mmn != default)
                    mmn.Dispose();
            } catch {}
        }
        private static void DisposeMMDeviceInternal()
        {
            try {
                List<MMDevice> lmmd = AudioMMDevice.ld_;
                AudioMMDevice.ld_ = default;
                if (lmmd != default)
                {
                    foreach (var d in lmmd)
                        if ((d != default) && !d.IsDisposed)
                            try { d.Dispose(); } catch { }
                    lmmd.Clear();
                }
            } catch {}
        }
        #endregion
        #endregion
    }
}
