﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Linq;
using System.Collections.Generic;
using PlayListServiceStream.Data;

namespace PlayListServiceStream.Base
{
    public static class AudioStreamEqualizerPreset
    {
        private static readonly float[][] preset = new float[][] {
            new float[]{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            new float[]{ 6.4f, 6.4f, 6.4f, 6.4f, 6.4f, 6.4f, 6.4f, 6.4f, 6.4f, 6.4f },
            new float[]{ -10.3f, -1.9f, 1.1f, 3.8f, 6.6f, 9.3f, 9.6f, 9.7f, 6.3f, 3.3f },
            new float[]{ 9.6f, 11.6f, 8.7f, 6.2f, 4.3f, 5.1f, 7.3f, 9.7f, 12.6f, 15.0f },
            new float[]{ -6.8f, -2.9f, 0.9f, 3.5f, 5.2f, 5.8f, 7.3f, 9.7f, 12.6f, 15.0f },
            new float[]{ -9.3f, -8.6f, -2.0f, 2.0f, 5.1f, 7.8f, 10.9f, 12.9f, 14.9f, 17.9f },
            new float[]{ 13.4f, 10.5f, 8.5f, 5.7f, 4.0f, 4.3f, 6.0f, 9.8f, 13.1f, 16.7f },
            new float[]{ -16.9f, -13.3f, 6.1f, 3.6f, -10.2f, -7.3f, -9.3f, 4.0f, 7.1f, 9.7f },
            new float[]{ 13.4f, 10.5f, 8.5f, 5.7f, 2.4f, 0.1f, 1.5f, 4.3f, 7.8f, 10.5f }
        };
        public static List<EqualizerChannelBase> GetEqualizerPreset(this int idx)
        {
            List<EqualizerChannelBase> list = new();
            if ((idx < 0) || (idx >= preset.Length))
                return list;

            for (int i = 0; i < preset[idx].Length; i++)
                list.Add(new EqualizerChannel { Idx = i, Value = preset[idx][i] });
            return list;
        }
        public static void SetEqualizerPreset(this IEnumerable<EqualizerChannelBase> list, int idx)
        {
            if ((idx < 0) || (idx >= preset.Length) || (list.Count() != preset[0].Length))
                return;

            for (int i = 0; i < preset[idx].Length; i++)
                list.ElementAt(i).Value = preset[idx][i];
        }
    }
}
