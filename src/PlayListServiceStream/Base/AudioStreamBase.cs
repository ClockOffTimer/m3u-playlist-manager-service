﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Utils;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceStream.Base
{
    public abstract class AudioStreamBase : AudioStreamLogEvent<char, string, Exception>, INotifyPropertyChanged, IDisposable
    {
        public virtual void Dispose() { }
        protected ILogManager LogWriter = default;
        protected WeakReference<Action<char, string, Exception>> logHandle = default;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        protected void OnPropertyChanged(params string[] names)
        {
            foreach (var s in names)
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(s));
        }

        protected void SubscribeLog(char c, string s, Exception e)
        {
            if ((logHandle != default) && logHandle.TryGetTarget(out Action<char, string, Exception> act))
                act.Invoke(c, s, e);
        }
        protected void ToLog<T>(T _, char c, string s, Exception e)
        {
            if (s == default)
                CallEvent(c, $"{typeof(T).Name}: ", e);
            else
                CallEvent(c, $"{typeof(T).Name}: {s}", e);
        }
        protected void ChildLog(char c, string s, Exception e) => CallEvent(c, s, e);

        //

        protected void CancellationDispose(CancellationTokenSafe cts)
        {
            if (cts != default)
            {
                if (!cts.IsCancellationRequested)
                {
                    cts.Cancel();
                    Tasks.Task.Delay(1000);
                }
                if (!cts.IsDisposed) cts.Dispose();
            }
        }
        protected void CancellationCancell(CancellationTokenSafe cts)
        {
            if ((cts != default) && !cts.IsDisposed && !cts.IsCancellationRequested)
                try { cts.Cancel(); } catch { }
        }
    }
}
