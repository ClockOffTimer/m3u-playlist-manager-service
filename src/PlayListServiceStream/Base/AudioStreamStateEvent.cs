﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceStream.Base
{
    public class AudioStreamStateEvent
    {
        public delegate void DelegateStateEvent(AudioStreamState s, bool b);
        public event DelegateStateEvent EventCb = delegate { };
        public void CallEvent(AudioStreamState s, bool b)
        {
            EventCb?.Invoke(s, b);
        }
    }
}
