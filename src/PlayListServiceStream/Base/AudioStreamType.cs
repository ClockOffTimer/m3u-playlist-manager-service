﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;

namespace PlayListServiceStream.Base
{
    public enum AudioStreamType : int
    {
        None = 0,
        A_1_16_22050,
        A_1_16_32000,
        A_1_16_44100,
        A_1_16_48000,

        A_2_16_22050,
        A_2_16_32000,
        A_2_16_44100,
        A_2_16_48000,

        A_2_32_22050,
        A_2_32_32000,
        A_2_32_44100,
        A_2_32_48000
    };
    public static class AudioStreamTypeString
    {
        private static readonly int[][] samples = new int[][] {
            new int[]{ 0, 0, 0 },
            new int[]{ 1, 16, 22050 },
            new int[]{ 1, 16, 32000 },
            new int[]{ 1, 16, 44100 },
            new int[]{ 1, 16, 48000 },
            new int[]{ 2, 16, 22050 },
            new int[]{ 2, 16, 32000 },
            new int[]{ 2, 16, 44100 },
            new int[]{ 2, 16, 48000 },
            new int[]{ 2, 32, 22050 },
            new int[]{ 2, 32, 32000 },
            new int[]{ 2, 32, 44100 },
            new int[]{ 2, 32, 48000 },
        };
        public static int GetStreamType(this AudioStreamType t)
        {
            return t switch
            {
                AudioStreamType.A_1_16_22050 => 1,
                AudioStreamType.A_1_16_32000 => 2,
                AudioStreamType.A_1_16_44100 => 3,
                AudioStreamType.A_1_16_48000 => 4,
                AudioStreamType.A_2_16_22050 => 5,
                AudioStreamType.A_2_16_32000 => 6,
                AudioStreamType.A_2_16_44100 => 7,
                AudioStreamType.A_2_16_48000 => 8,
                AudioStreamType.A_2_32_22050 => 9,
                AudioStreamType.A_2_32_32000 => 10,
                AudioStreamType.A_2_32_44100 => 11,
                AudioStreamType.A_2_32_48000 => 12,
                _ => 0
            };
        }
        public static AudioStreamType GetStreamType(this int n)
        {
            return n switch
            {
                1 => AudioStreamType.A_1_16_22050,
                2 => AudioStreamType.A_1_16_32000,
                3 => AudioStreamType.A_1_16_44100,
                4 => AudioStreamType.A_1_16_48000,
                5 => AudioStreamType.A_2_16_22050,
                6 => AudioStreamType.A_2_16_32000,
                7 => AudioStreamType.A_2_16_44100,
                8 => AudioStreamType.A_2_16_48000,
                9 => AudioStreamType.A_2_32_22050,
                10 => AudioStreamType.A_2_32_32000,
                11 => AudioStreamType.A_2_32_44100,
                12 => AudioStreamType.A_2_32_48000,
                _ => AudioStreamType.None
            };
        }
        public static AudioStreamType GetStreamType(this int[] n)
        {
            if (n == default)
                return AudioStreamType.None;

            for (int i = 0; i < samples.Length; i++)
                if (n.Equals(samples[i]))
                {
                    if (Enum.TryParse(i.ToString(), out AudioStreamType t))
                        return t;
                }
            return AudioStreamType.None;
        }
        public static int[] Get(this int n)
        {
            return ((n >= 0) && (n < samples.Length)) ? samples[n] : default;
        }
        public static int [] Get(this AudioStreamType t)
        {
            if (t == AudioStreamType.None)
                return default;
            return samples[AudioStreamTypeString.GetStreamType(t)];
        }
        public static string Get(this AudioStreamType t, string fmt, string emptyTxt)
        {
            if (t == AudioStreamType.None)
                return emptyTxt;
            int[] s = samples[AudioStreamTypeString.GetStreamType(t)];
            return string.Format(fmt, s[0], s[1], s[2]);
        }
        public static List<string> GetList(this string fmt, string emptyTxt)
        {
            List<string> list = new();
            foreach (AudioStreamType t in Enum.GetValues(typeof(AudioStreamType)))
                list.Add(AudioStreamTypeString.Get(t, fmt, emptyTxt));
            return list;
        }
    }
}
