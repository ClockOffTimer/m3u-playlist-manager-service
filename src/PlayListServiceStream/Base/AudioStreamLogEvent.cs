﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceStream.Base
{
    public class AudioStreamLogEvent<T1, T2, T3>
    {
        public delegate void DelegateLogEvent(T1 t1, T2 t2, T3 t3);
        public event DelegateLogEvent EventCb = delegate { };
        public void CallEvent(T1 t1, T2 t2, T3 t3)
        {
            EventCb?.Invoke(t1, t2, t3);
        }
    }
}
