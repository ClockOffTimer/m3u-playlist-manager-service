﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using CSCore;
using CSCore.CoreAudioAPI;
using CSCore.MediaFoundation;
using CSCore.SoundIn;
using CSCore.Streams;
using CSCore.Streams.Effects;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Utils;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;
using PlayListServiceStream.Exceptions;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceStream
{
    public class AudioStreamReader : AudioStreamBase, IAudioStreamObject
    {
        private readonly object lock__ = new();
        private readonly IAudioChannel channelsetting = new AudioChannel(-1);
        private readonly AudioMMDevice mmdevice;
        private readonly List<AudioStreamClient> clients = new();
        private CancellationTokenSafe tokenCancell = new();
        private CancellationTokenSafe tokenStart = default;
        private CancellationTokenSafe tokenEnd = default;
        private WeakReference<Equalizer> equalizer = default;
        private WeakReference<GainSource> gain = default;
        private Thread thread = default;

        public AudioStreamReader(AudioMMDevice mmd, ILogManager log)
        {
            LogWriter = log;
            mmdevice = mmd;
            channelsetting.OutFormatTag = AudioStreamFormat.Mp3;
            thread = new Thread(async () => { await ActiveTask(); });
            thread.Start();
        }
        public AudioStreamReader(AudioMMDevice mmd, Action<char, string, Exception> act, IAudioChannel channel = default)
        {
            logHandle = new WeakReference<Action<char, string, Exception>>(act);
            EventCb += SubscribeLog;
            mmdevice = mmd;
            channelsetting.OutFormatTag = AudioStreamFormat.Mp3;
            if (channel != default)
                channelsetting.Copy(channel);
            thread = new Thread(async () => { await ActiveTask(); });
            thread.Start();
        }
        ~AudioStreamReader()
        {
            Dispose();
            EventCb -= SubscribeLog;
        }

        public void SetEventParent(Action<char, string, Exception> act) =>
            logHandle = new WeakReference<Action<char, string, Exception>>(act);
        public void SetEventCb() => EventCb += SubscribeLog;
        public void RemoveEventCb() => EventCb -= SubscribeLog;
        public void SetAudioChannel(IAudioChannel channel) => channelsetting.Copy(channel);

        #region Dispose
        public override void Dispose()
        {
            try
            {
                CancellationTokenSafe cts;
                cts = tokenCancell;
                lock (lock__)
                    CancellationCancell(cts);

                cts = tokenEnd;
                lock (lock__)
                    CancellationCancell(cts);

                cts = tokenStart;
                lock (lock__)
                    CancellationCancell(cts);

                if (clients.Count > 0)
                {
                    foreach (var a in clients)
                        a.Disable();
                    lock (lock__)
                        clients.Clear();
                }

                Tasks.Task.Delay(1000);

                lock (lock__)
                {
                    cts = tokenCancell;
                    tokenCancell = default;
                    CancellationDispose(cts);

                    cts = tokenEnd;
                    tokenEnd = default;
                    CancellationDispose(cts);

                    cts = tokenStart;
                    tokenStart = default;
                    CancellationDispose(cts);
                }
                Thread th = thread;
                thread = default;
                if (th != default)
                    _ = th.Join(TimeSpan.FromMilliseconds(1000));

            } catch (Exception ex) { ex.WriteLog('!', $"StreamReader:{nameof(Dispose)}", LogWriter); }
            base.Dispose();
        }
        #endregion

        #region Get/Set
        public AudioStreamType   ChannelType { get => channelsetting.Atype; set => channelsetting.Atype = value; }
        public AudioStreamFormat ChannelOutFormat { get => channelsetting.OutFormatTag; set => channelsetting.OutFormatTag = value; }
        public bool  IsActive { get => (tokenEnd != default) && !(bool)tokenEnd?.IsCancellationRequested; }
        public float MasterVolumeMute { get => 0.0f; set { } }
        public int   ReadTimeout { get; set; } = 5;
        public string DeviceId { get => channelsetting.DeviceId; set => channelsetting.DeviceId = value; }
        public string DeviceName { get => channelsetting.Tag; set => channelsetting.Tag = value; }
        public bool  ChannelMono { get => channelsetting.IsMono; set => channelsetting.IsMono = value; }
        public int   ChannelBytes { get => channelsetting.BytesPerSecond; set => channelsetting.BytesPerSecond = value; }
        public int   ChannelBits  { get => channelsetting.BitsPerSample; set => channelsetting.BitsPerSample = value; }
        public int   ChannelNumber { get => channelsetting.Channels; set => channelsetting.Channels = value; }
        public float ChannelVolume
        {
            get => channelsetting.Volume;
            set
            {
                channelsetting.Volume = value;
                if ((gain != default) && gain.TryGetTarget(out GainSource g))
                    g.Volume = value;
            }
        }
        public float [] ChannelEqualizers
        {
            get => channelsetting.EqualizerChannels.Select(x => x.Value).ToArray();
            set
            {
                if ((value == default) || (value.Length == 0))
                    return;
                if (!equalizer.TryGetTarget(out Equalizer eq))
                    return;
                for (int i = 0; (i < value.Length) && (i < 10); i++)
                {
                    eq.SampleFilters[i].AverageGainDB = value[i];
                    channelsetting.EqualizerChannels[i].Value = value[i];
                }
            }
        }
        public IAudioChannel AudioChannel
        {
            get { return channelsetting; }
            set
            {
                if (value == default)
                    return;
                channelsetting.Copy(value);
                if (string.IsNullOrWhiteSpace(channelsetting.DeviceId))
                    channelsetting.DeviceId = mmdevice.GetDeviceId(channelsetting.Tag);
                if (equalizer.TryGetTarget(out Equalizer eq))
                    channelsetting.EqualizerSet(eq);
            }
        }
        #endregion

        #region *Client
        public AudioStreamClient ContainsClient(AudioStreamClient asc)
        {
            if ((asc == default) || (asc.Tid == -1))
                return default;
            return ContainsClient(asc.Tid);
        }
        public AudioStreamClient ContainsClient(int tid)
        {
            lock (lock__)
                return (from i in clients
                        where i.Tid == tid
                        select i).FirstOrDefault();
        }
        public AudioStreamClient AddClient(IPEndPoint ip, Action<byte[]> a)
        {
            try
            {
                AudioStreamClient asc = new(ip, a, channelsetting);
                RemoveClient(asc);
                lock (lock__)
                    clients.Add(asc.SendTagsHeader());
                if ((clients.Count > 0) && (tokenStart != default))
                    tokenStart.Cancel();
                return asc;
            } catch (Exception ex) { ex.WriteLog('!', $"StreamReader:{nameof(AddClient)}", LogWriter); }
            return default(AudioStreamClient);
        }
        public void RemoveClient(AudioStreamClient asc)
        {
            if ((asc == default) || (asc.Tid == -1))
                return;
            RemoveClient(asc.Tid);
        }
        public void RemoveClient(int tid)
        {
            if (tid == -1)
                return;
            try
            {
                AudioStreamClient old = ContainsClient(tid);
                if (old == default)
                    return;

                lock (lock__)
                    try { old.Disable(); clients.Remove(old); } catch { }
                if ((clients.Count == 0) && (tokenEnd != default))
                    tokenEnd.Cancel();
            } catch (Exception ex) { ex.WriteLog('!', $"StreamReader:{nameof(RemoveClient)}", LogWriter); }
        }
        public List<string> GetClientsStats()
        {
            return (from i in clients
                    where  i.IPClient != default
                    select i.IPClient.ToString()).ToList();
        }
        #endregion

        #region Start/Stop
        public void Start()
        {
            if ((tokenStart != default) && !tokenStart.IsCancellationRequested)
                tokenStart?.Cancel();
        }

        public void Stop()
        {
            if ((tokenEnd != default) && !tokenEnd.IsCancellationRequested)
                tokenEnd?.Cancel();
        }
        public void ReStart(IAudioChannel channel) { }
        public void Start(IAudioChannel channel) { }
        #endregion

        #region private
        private async Tasks.Task ActiveTask()
        {
            try
            {
                while (tokenCancell.CancellationCheckBoolean())
                {
                    tokenStart = new CancellationTokenSafe();
                    using (WaitHandle wait = tokenStart.Token.WaitHandle)
                    {
                        try
                        {
                            _ = wait.WaitOne();
                            CancellationTokenSafe cts = tokenStart;
                            tokenStart = default;
                            if (cts != default)
                                cts.Dispose();
                        } catch { }
                    }

                    try
                    {
                        CancellationTokenSafe cts = tokenEnd;
                        tokenEnd = default;
                        if (cts != default)
                            cts.Dispose();
                    } catch { }

                    lock (lock__)
                        tokenEnd = new CancellationTokenSafe();

                    await Tasks.Task.Run(() =>
                    {
                        MMDevice mmd = default;
                        try
                        {
                            tokenCancell.CancellationCheckException();
                            tokenEnd.CancellationCheckException();

                            if (string.IsNullOrWhiteSpace(DeviceName))
                                throw new DeviceIdNotFoundException(DeviceName);

                            $"{nameof(ActiveTask)} -> started device: {DeviceName}/{DeviceId}".WriteLog('v', LogWriter);

                            using WasapiCapture sin = new(
                                true,
                                AudioClientShareMode.Shared,
                                channelsetting.PreCache,
                                channelsetting.GetWaveFormat(),
                                ThreadPriority.AboveNormal);

                            try
                            {
                                if (!string.IsNullOrWhiteSpace(channelsetting.DeviceId))
                                {
                                    mmd = mmdevice.GetDeviceById(channelsetting.DeviceId);
                                    sin.Device = mmd ?? throw new DeviceNotFoundException(DeviceName);
                                }
                                sin.Initialize();

                                bool iscancell = false;
                                TimeSpan expired = TimeSpan.FromSeconds(ReadTimeout);

                                using SoundInSource ss = new(sin, 65536 * 2)
                                {
                                    FillWithZeros = false
                                };
                                using var ssrc = new GainSource(
                                    channelsetting.IsMono ? ss.ToMono().ToSampleSource() : ss.ToSampleSource())
                                {
                                    Volume = channelsetting.Volume
                                };
                                using var eq = Equalizer.Create10BandEqualizer(ssrc);
                                using var src = eq.ToWaveSource();
                                using var qas = new QueueAudioStream();
                                using var mfe = channelsetting.OutFormatTag switch
                                {
                                    AudioStreamFormat.Mp3 => MediaFoundationEncoder.CreateMP3Encoder(src.WaveFormat, qas),
                                    AudioStreamFormat.Wma => MediaFoundationEncoder.CreateWMAEncoder(src.WaveFormat, qas),
                                    AudioStreamFormat.Aac => MediaFoundationEncoder.CreateAACEncoder(src.WaveFormat, qas),
                                    _ => default
                                };
                                if (mfe == default)
                                    throw new NotSupportedException($"{channelsetting.OutFormatTag}");

                                tokenEnd.CancellationCheckException();
                                gain = new WeakReference<GainSource>(ssrc);
                                equalizer = new WeakReference<Equalizer>(eq);
                                channelsetting.EqualizerSet(eq);

                                qas.LogEvent.EventCb += ChildLog;

                                AudioDataAvailableHandler data = new(tokenEnd, src, mfe);
                                sin.DataAvailable += data.StreamWrite;
                                sin.Stopped += (s, e) =>
                                {
                                    if (tokenEnd.CancellationCheckBoolean())
                                        tokenEnd?.Cancel();
                                    ToLog(this, 'i', $"{nameof(sin.Stopped)}: {tokenEnd?.IsCancellationRequested}", default);
                                };

                                sin.Start();

                                while (clients.Count > 0)
                                {
                                    if (!tokenEnd.CancellationCheckBoolean() ||
                                        !tokenCancell.CancellationCheckBoolean())
                                        break;

                                    byte[] buffer = qas.Read();
                                    if (buffer == default)
                                    {
                                        if (!iscancell)
                                        {
                                            iscancell = true;
                                            if (tokenEnd.CancellationCheckBoolean())
                                                tokenEnd?.CancelAfter(expired);
                                            else
                                                break;
                                        }
                                        Tasks.Task.Yield();
                                    }
                                    else
                                    {
                                        for (int i = clients.Count - 1; i >= 0; i--)
                                        {
                                            if (!tokenEnd.CancellationCheckBoolean())
                                                break;
                                            try
                                            {
                                                if (clients[i].Tid == -1)
                                                {
                                                    RemoveClient(clients[i]);
                                                    continue;
                                                }
                                                clients[i].Act.Invoke(buffer);

                                            }
                                            catch (Exception e)
                                            {
                                                RemoveClient(clients[i]);
                                                ToLog(this, '!', $"{nameof(ActiveTask)}:{clients[i].Tid} ->", e);
                                            }
                                        }
                                        if (iscancell)
                                        {
                                            iscancell = false;
                                            if (tokenEnd.CancellationCheckBoolean())
                                                tokenEnd?.CancelAfter(Timeout.InfiniteTimeSpan);
                                            else
                                                break;
                                        }
                                    }
                                }
                                qas.LogEvent.EventCb -= ChildLog;
                                sin.DataAvailable -= data.StreamWrite;
                                data = default;
                            }
                            catch (OperationCanceledException ex) { $"Reader:{nameof(ActiveTask)}:InSource -> {DeviceName} -> {ex.Message}".WriteLog('!', LogWriter); }
                            catch (Exception ex) { ex.WriteLog('!', $"Reader:{nameof(ActiveTask)}:InSource -> {DeviceName}", LogWriter).WriteStackTrace(); }
                        }
                        catch (OperationCanceledException ex) { $"Reader:{nameof(ActiveTask)}:Capture -> {DeviceName} -> {ex.Message}".WriteLog('!', LogWriter); }
                        catch (Exception ex) { ex.WriteLog('!', $"Reader:{nameof(ActiveTask)}:Capture -> {DeviceName}", LogWriter).WriteStackTrace(); }
                        finally
                        {
                            $"{nameof(ActiveTask)} -> closed  device: {DeviceName}/{DeviceId}".WriteLog('v', LogWriter);

                            equalizer = default;
                            gain = default;
                            foreach (var a in clients)
                                a.Disable();
                            lock (lock__)
                                clients.Clear();

                            if ((mmd != default) && !mmd.IsDisposed)
                                try { mmd.Dispose(); } catch { }

                            try
                            {
                                CancellationTokenSafe cts = tokenEnd;
                                tokenEnd = default;
                                if (cts != default)
                                    cts.Dispose();
                            } catch { }
                        }
                    }, tokenEnd.Token);
                }
            }
            catch (OperationCanceledException ex) { $"Reader:{nameof(ActiveTask)}:End -> {DeviceName} -> {ex.Message}".WriteLog('!', LogWriter); }
            catch (Exception ex) { ex.WriteLog('!', $"Reader:{nameof(ActiveTask)}:End -> {DeviceName}", LogWriter).WriteStackTrace(); }
        }
        #endregion
    }
}
