﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

// #define WATCH_CHANGE

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CSCore.CoreAudioAPI;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Process.Report;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;
#if WATCH_CHANGE
using Tasks = System.Threading.Tasks;
#endif

namespace PlayListServiceStream
{
    public class AudioStreamManagerService : AudioStreamBase
    {
        public AudioStreamManagerService(Action<char, string, Exception> act, string cnfpath)
        {
            LogWriter = ProcessLog.Create(
                new FileLog(BasePath.GetAudioEngineLogPath(), "Audio"), new Reporter());

            logHandle = new WeakReference<Action<char, string, Exception>>(act);
            EventCb += SubscribeLog;
            xmlpath_ = cnfpath;

            channelssetting = new(cnfpath, LogWriter);
            AddMMDEvents(channelssetting);

#           if WATCH_CHANGE
            watchhandler = new FileSystemEventHandler(WatchChannelsOnChanged);
            channelwatch = new FileSystemWatcher
            {
                Path = Path.GetDirectoryName(xmlpath_),
                IncludeSubdirectories = true,
                NotifyFilter =
                    NotifyFilters.FileName |
                    NotifyFilters.Size |
                    NotifyFilters.LastWrite |
                    NotifyFilters.CreationTime,
                Filter = Path.GetFileName(xmlpath_)
            };
            channelwatch.EnableRaisingEvents = true;
            channelwatch.Changed += watchhandler;
            channelwatch.Created += watchhandler;
#           else
            watchhandler = default;
            channelwatch = default;
#           endif
        }

        ~AudioStreamManagerService()
        {
            Dispose();
        }

        private readonly object lock__ = new();
        private readonly string xmlpath_;
        private readonly FileSystemEventHandler watchhandler;
        private readonly AudioSettings channelssetting;
        public  readonly List<AudioStreamReader> AudioReaders = new();
        private FileSystemWatcher channelwatch;
#       if WATCH_CHANGE
        private bool isloading_ = false;
        private DateTime lastLoading_ = default;
#       endif

        public AudioSettings ASettings
        {
            get => channelssetting;
            set
            {
                if (value == default)
                    return;
                RemoveMMDEvents(channelssetting);
                InitSettings(value);
#               if WATCH_CHANGE
                IsLoading_ = isloading_ = false;
#               else
                IsLoading_ = false;
#               endif
            }
        }

        private bool IsLoading_ = false;
        public bool IsLoading { get => IsLoading_; }

        public int ReaderCount
        {
            get => channelssetting.Channels.Count;
        }

        public AudioStreamReader GetAudioReader(int idx)
        {
            if ((idx >= AudioReaders.Count) || (idx < 0))
                return default;
            lock (lock__)
                return AudioReaders[idx];
        }

        public AudioSettingsLite GetChannelsSettings()
        {
            try {
                int idx = 0;
                AudioSettingsLite asr = new();
                var list = (from i in channelssetting.Channels
                            select new AudioChannelLite(idx++, i));
                return asr.AddRange(list);

            } catch (Exception e) {
                string s = $"Manager:{nameof(GetChannelsSettings)} ->";
                e.WriteLog('!', s, LogWriter);
                ToLog(this, '!', s, e);
            }
            return default;
        }

        public List<Tuple<string, List<string>>> GetChannelsStat()
        {
            return (from i in AudioReaders
                    select new Tuple<string, List<string>>(i.DeviceName, i.GetClientsStats())).ToList();
        }

        public AudioChannelLite GetChannelSettings(int idx)
        {
            if ((idx >= channelssetting.Channels.Count) || (idx < 0))
                return default;
            lock (lock__)
                return new AudioChannelLite(idx, channelssetting.Channels[idx]);
        }

        public void SetChannelSettings(int idx, AudioChannelLite asr)
        {
            try {
                if ((asr == default) || (idx >= channelssetting.Channels.Count) || (idx < 0))
                    return;
                if (asr.Id != idx)
                    return;
                AudioStreamReader r = GetAudioReader(idx);
                if (r == default)
                    return;

                r.ChannelVolume = asr.Volume;
                r.ChannelMono = asr.IsMono;
                if (asr.Equalizer != default)
                    r.ChannelEqualizers = asr.Equalizer;
                if (asr.Atype != AudioStreamType.None)
                {
                    int[] arr = asr.Atype.Get();
                    if (arr != default)
                    {
                        r.ChannelType = asr.Atype;
                        r.ChannelNumber = arr[0];
                        r.ChannelBits = arr[1];
                        r.ChannelBytes = arr[2];
                    }
                }
            } catch (Exception e) {
                string s = $"Manager:{nameof(SetChannelSettings)} ->";
                e.WriteLog('!', s, LogWriter);
                ToLog(this, '!', s, e);
            }
        }

        public override void Dispose() => InternalDispose(true);
        private void InternalDispose(bool b)
        {
            lock (lock__)
            {
                try {
                    foreach (var a in AudioReaders)
                        try { a.Dispose(); } catch { }
                    AudioReaders.Clear();
                }
                catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(InternalDispose)} ->", LogWriter); }
            }

            if (!b)
                return;

            EventCb -= SubscribeLog;
            RemoveMMDEvents(channelssetting);

            try {
                FileSystemWatcher fsw = channelwatch;
                channelwatch = null;
                if (fsw != default)
                {
                    fsw.EnableRaisingEvents = false;
                    fsw.Changed -= watchhandler;
                    fsw.Created -= watchhandler;
                    fsw.Dispose();
                }
            }
            catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(InternalDispose)} ->", LogWriter); }

            LogWriter.Close();
            LogWriter = null;
            base.Dispose();
        }

#region Init/Event
#if WATCH_CHANGE
        private void WatchChannelsOnChanged(object sender, FileSystemEventArgs a)
        {
            try
            {
                do
                {
                    if (isloading_)
                        break;
                    if (!a.FullPath.Equals(xmlpath_))
                        break;
                    if (lastLoading_ != default)
                        if ((DateTime.Now - lastLoading_) <= TimeSpan.FromMinutes(3))
                        {
#if DEBUG
                            $"Manager:{nameof(WatchChannelsOnChanged)} -> skip change file settings -> {lastLoading_:dd-MM HH:mm:ss} <-> {DateTime.Now:dd-MM HH:mm:ss} -> {a.FullPath}".WriteLog('v', LogWriter);
#endif
                            break;
                        }

                    isloading_ = true;
                    lastLoading_ = DateTime.Now;
                    $"Manager:{nameof(WatchChannelsOnChanged)} -> change file settings -> {a.FullPath}".WriteLog('v', LogWriter);

                    ASettings.DisposeService();
                    Tasks.Task.Delay(1000);
                    lock (lock__)
                    {
                        foreach (AudioStreamReader r in AudioReaders)
                            try { r.Dispose(); } catch { }
                        AudioReaders.Clear();
                    }
                    ASettings = new AudioSettings(xmlpath_, LogWriter);

                } while (false);
            } catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(WatchChannelsOnChanged)} ->", LogWriter); }
        }
#endif

        private void State_EventCb(AudioStreamState s, bool b)
        {
            switch (s)
            {
                case AudioStreamState.Load:
                    {
                        if (b)
                        {
                            Init();
                            IsLoading_ = true;
                        }
                        else
                            IsLoading_ = false;
                        break;
                    }
            }
        }
        private void Init()
        {
            try {
                InternalDispose(false);

                lock (lock__)
                {
                    foreach (IAudioChannel a in channelssetting.Channels)
                    {
                        if (!channelssetting.MMDevice.IsDeviceFound(a.Tag)) {
                            $"Manager:{nameof(Init)} -> DEVICE NOT FOUND! -> {a.Tag}".WriteLog('!', LogWriter);
                            continue;
                        }
                        a.DeviceId = channelssetting.MMDevice.GetDeviceId(a.Tag);
                        if (string.IsNullOrWhiteSpace(a.DeviceId))
                        {
                            $"Manager:{nameof(Init)} -> ID NOT FOUND! -> {a.Tag}".WriteLog('!', LogWriter);
                            continue;
                        }
                        $"Manager:{nameof(Init)} -> NEW -> {a.Tag}/{a.DeviceId}".WriteLog('v', LogWriter);
                        AudioStreamReader asr = new(channelssetting.MMDevice, LogWriter);
                        asr.SetEventParent(SubscribeLog);
                        asr.SetEventCb();
                        asr.SetAudioChannel(a);
                        AudioReaders.Add(asr);
                    }
                }
                channelssetting.MMDevice.DisposeDevices();
            }
            catch (Exception e)
            {
                string s = $"Manager:{nameof(Init)} ->";
                e.WriteLog('!', s, LogWriter);
                ToLog(this, '!', s, e);
            }
        }

        private async void InitSettings(AudioSettings data)
        {
            if ((data == default) || (data.Channels.Count == 0))
                return;

            await Task.Run(() => {
                try { channelssetting.Copy(data); }
                catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(Init)}:Async", LogWriter); }

            }).ContinueWith((t) =>
            {
                try
                {
                    do {
                        if (t == default)
                        {
                            $"Manager:{nameof(Init)}:Async -> Task is NULL".WriteLog('!', LogWriter);
                            break;
                        }
                        if (t.IsFaulted || t.IsCanceled)
                        {
                            if (t.Exception != default)
                                t.Exception.WriteLog('!', $"Manager:{nameof(Init)}:Async -> Task abort ->", LogWriter);
                            break;
                        }
                        else if (!t.IsCompleted)
                            t.Wait();

                        AddMMDEvents(channelssetting);
                        Init();
                        State_EventCb(AudioStreamState.Load, true);

                    } while (false);
                } catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(Init)}:Async ->", LogWriter); }
#if WATCH_CHANGE
                isloading_ = false;
#endif
            });
        }

#region DeviceNotification Events
        private void AddMMDEvents(AudioSettings aset)
        {
            try {
                MMNotificationClient mme = aset.MMDevice.DeviceNotification;
                mme.DeviceAdded += MMD_DeviceAdded;
                mme.DevicePropertyChanged += MMD_DevicePropertyChanged;
                mme.DeviceRemoved += MMD_DeviceRemoved;
                aset.State.EventCb += State_EventCb;
            }
            catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(AddMMDEvents)} ->", LogWriter); }
        }
        private void RemoveMMDEvents(AudioSettings aset)
        {
            try
            {
                MMNotificationClient mme = aset.MMDevice.DeviceNotification;
                mme.DeviceAdded -= MMD_DeviceAdded;
                mme.DevicePropertyChanged -= MMD_DevicePropertyChanged;
                mme.DeviceRemoved -= MMD_DeviceRemoved;
                aset.State.EventCb -= State_EventCb;
            }
            catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(RemoveMMDEvents)} ->", LogWriter); }
        }

        private void MMD_DeviceRemoved(object sender, DeviceNotificationEventArgs a)
        {
            try {
                var reader = (from i in AudioReaders
                              where (i != default) && i.DeviceId.Equals(a.DeviceId)
                              select i).FirstOrDefault();

                if (reader != default) {
                    $"Manager:{nameof(AddMMDEvents)} -> removed device: {reader.DeviceName}/{a.DeviceId}".WriteLog('v', LogWriter);
                    reader.Dispose();
                    AudioReaders.Remove(reader);
                    State_EventCb(AudioStreamState.Remove, true);
                }
            } catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(AddMMDEvents)}:DeviceRemoved ->", LogWriter); }
        }

        private void MMD_DeviceAdded(object sender, DeviceNotificationEventArgs a)
        {
            MMDevice device = default;
            try {
                string name = string.Empty;
                if (a.TryGetDevice(out device))
                    name = device.FriendlyName;

                if (string.IsNullOrWhiteSpace(name))
                    return;

                $"Manager:{nameof(AddMMDEvents)} -> added device: {name}/{a.DeviceId}".WriteLog('v', LogWriter);

                var channel = (from i in channelssetting.Channels
                               where (i != default) && i.Tag.Equals(name)
                               select i).FirstOrDefault();
                if (channel != default)
                {
                    channel.DeviceId = device.DeviceID;
                    AudioStreamReader asr = new(channelssetting.MMDevice, LogWriter);
                    asr.SetEventParent(SubscribeLog);
                    asr.SetEventCb();
                    asr.SetAudioChannel(channel);
                    AudioReaders.Add(asr);
                    State_EventCb(AudioStreamState.Add, true);
                }
            }
            catch (Exception e) { e.WriteLog('!', $"Manager:{nameof(AddMMDEvents)}:DeviceAdded ->", LogWriter); }
            finally
            {
                if ((device != null) && !device.IsDisposed)
                    try { device.Dispose(); } catch { }
            }
        }

        private void MMD_DevicePropertyChanged(object sender, DevicePropertyChangedEventArgs a)
        {
            $"Manager:{nameof(AddMMDEvents)} -> changed device: {a.DeviceId}/{a.PropertyKey}".WriteLog('v', LogWriter);
            State_EventCb(AudioStreamState.Change, true);
        }
#endregion
#endregion
    }
}
