﻿/* Copyright (c) 2021-2022 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using PlayListServiceData.Utils;

namespace PlayListServiceStream
{
    public static class AudioStreamExtension
    {
        public static void CancellationCheckException(this CancellationTokenSafe token)
        {
            if ((token == default) || (!token.IsAlive) || token.IsDisposed)
                throw new OperationCanceledException($"token is END life");
            else if (token.IsCancellationRequested)
                throw new OperationCanceledException($"token is CANCELLED", token.Token);
            else
                token.Token.ThrowIfCancellationRequested();
        }
        public static bool CancellationCheckBoolean(this CancellationTokenSafe token)
        {
            if ((token == default) || (!token.IsAlive) || token.IsDisposed)
                return false;
            else if (token.IsCancellationRequested)
                return false;
            return true;
        }
        public static bool CheckDeviceString(string s) => !string.IsNullOrWhiteSpace(s) &&
                           !s.Equals("default", StringComparison.InvariantCultureIgnoreCase);
    }
}
