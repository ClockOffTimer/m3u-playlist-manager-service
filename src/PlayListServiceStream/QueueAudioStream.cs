﻿/* Copyright (c) 2021 PlayListServiceStream, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using PlayListServiceStream.Base;

namespace PlayListServiceStream
{
    public class QueueAudioStream : Stream
    {
#       pragma warning disable IDE1006
        private readonly ConcurrentQueue<byte[]> queue_ = new();
        private bool recordend_ = false;
        private long length_ { get; set; }  = 0L;
        private long position_ { get; set; } = 0L;
#       pragma warning restore IDE1006

        public int Count => queue_.Count;
        public override bool CanRead => true;
        public override bool CanSeek => true;
        public override bool CanWrite => true;
        public override long Length => length_;
        public override long Position { get => position_; set => position_ = value; }
        public readonly AudioStreamLogEvent<char, string, Exception> LogEvent = new();

        public override void Flush() { }
        public override long Seek(long offset, SeekOrigin origin) => offset;
        public override void SetLength(long value) { }
        public override int  Read(byte[] buffer, int offset, int count)
        {
            try
            {
                Debug.WriteLine($"{nameof(QueueAudioStream)}: !!Warning!! end send stream -> {nameof(Read)} =  {offset}/{count}");
                recordend_ = true;
                if (buffer == default)
                    return 0;
                for (int i = 0; i < buffer.Length; i++)
                    buffer[i] = 0;
                return (buffer.Length > count) ? count : 0;
            }
            catch (Exception e)
            {
                LogEvent.CallEvent('!', $"{nameof(QueueAudioStream)}: {nameof(Read)} =", e);
            }
            return 0;
        }

        private bool first_ = true;
        private int  header_size_ = 10; /* (421 bytes from defafault MS mp3 encoder) REMOVE default ID3 TAG on start.. */
        public override void Write(byte[] buffer, int offset, int count)
        {
            try
            {
                if (recordend_ || (count <= 0) || (buffer == default))
                    return;

                int pad = 0;
                if (header_size_ > 0)
                {
                    do {
                        if (first_)
                        {
                            if ((count >= 3) && ((buffer[0] != 0x49) || (buffer[1] != 0x44) || (buffer[2] != 0x33)))
                            {
                                header_size_ = 0;
                                break;
                            }
                            if (count < 10)
                            {
                                header_size_ = 0; // fuck..
                                return;
                            }
                            first_ = false;
                            header_size_ += buffer[6] << 21 | buffer[7] << 14 | buffer[8] << 7 | buffer[9];
                        }

                        if (count <= header_size_)
                        {
                            header_size_ -= count;
                            return;
                        }
                        count -= header_size_;
                        pad = header_size_;
                        header_size_ = 0;

                    } while (false);
                }

                length_ += count;
                position_ += count;

                byte [] b = new byte[count];
                Array.Copy(buffer, pad, b, 0, count);
                queue_.Enqueue(b);
            }
            catch (Exception e) { LogEvent.CallEvent('!', $"{nameof(QueueAudioStream)}: {nameof(Write)} ->", e); }
        }

        public new void Dispose()
        {
            try
            {
                while (!queue_.IsEmpty)
                    try { _ = queue_.TryDequeue(out byte[] _); } catch { }
                base.Dispose();
            }
            catch (Exception e)
            {
                LogEvent.CallEvent('!', $"{nameof(QueueAudioStream)}: {nameof(Dispose)} ->", e);
            }
        }

        public byte[] Read()
        {
            try
            {
                if (queue_.IsEmpty)
                    return default;
                if (queue_.TryDequeue(out byte[] buffer))
                    return buffer;
            }
            catch (Exception e) { LogEvent.CallEvent('!', $"{nameof(QueueAudioStream)}: {nameof(Read)} ->", e); }
            return default;
        }
    }
}
