﻿/* Copyright (c) 2021 WpfPluginAudio, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginAudio
	{
		public static readonly RoutedUICommand EqPreset = new
			(
				"",
				"EqPreset",
				typeof(CommandPluginAudio),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand OutMute = new
			(
				"",
				"OutMute",
				typeof(CommandPluginAudio),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand ReloadDevices = new
			(
				"",
				"ReloadDevices",
				typeof(CommandPluginAudio),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand SaveDevices = new
			(
				"",
				"SaveDevices",
				typeof(CommandPluginAudio),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand EmptyDevices = new
			(
				"",
				"EmptyDevices",
				typeof(CommandPluginAudio),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
	}
}
