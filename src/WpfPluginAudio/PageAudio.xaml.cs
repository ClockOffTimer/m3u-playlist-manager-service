﻿/* Copyright (c) 2021 WpfPluginAudio, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel.Composition;
using System.Windows.Controls;
using System.Windows.Input;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 8),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageAudio : Page, IPlugin
    {
        public int Id { get; set; } = 0;
        public new string Title  => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView   => this;

        private readonly PageAudioData pageAudioData;

        [ImportingConstructor]
        public PageAudio(IServiceControls icontrols)
        {
            InitializeComponent();
            pageAudioData = (PageAudioData)DataContext;
            pageAudioData.SetServiceCtrl(icontrols);
            icontrols.OnCloseEventCb += Icontrols_OnCloseEventCb;
        }

        private void Icontrols_OnCloseEventCb()
        {
            if (pageAudioData != default)
                pageAudioData.OnCloseEvent();
        }

        private void EqPreset_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if ((pageAudioData == default) || (e == default))
                return;
            if (int.TryParse(e.Parameter.ToString(), out int idx))
                pageAudioData.EqPresetCtrl(idx);
        }
        private void EqPreset_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (pageAudioData != default) && pageAudioData.IsPanelEnable;
        }

        private void OutMute_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageAudioData != default)
                pageAudioData.MuteCtrl();
        }
        private void OutMute_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (pageAudioData != default) && pageAudioData.IsMasterEnable;
        }

        private void ReloadDevices_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageAudioData != default)
                pageAudioData.ReloadCtrl();
        }

        private void SaveDevices_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageAudioData != default)
                pageAudioData.SaveCtrl();
        }
        private void SaveDevices_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (pageAudioData != default) && pageAudioData.AudioDevices.Count > 0;
        }

        private void EmptyDevices_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (pageAudioData != default)
                pageAudioData.DeleteCtrl();
        }
    }
}
