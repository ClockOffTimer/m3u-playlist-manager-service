﻿/* Copyright (c) 2021 WpfPluginAudio, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;
using PlayListServiceData.ServiceConfig;
using PlayListServiceStream;
using PlayListServiceStream.Base;
using PlayListServiceStream.Data;
using WpfPlugin.Properties;
using Tasks = System.Threading.Tasks;

namespace WpfPlugin.Data
{
    public class PageAudioData : PropertyChangedEvent
    {
        private AudioSettings ASettings { get; set; }
        public  AudioStreamManagerClient AManagerClient { get; set; }
        private readonly ILogManager LogWriter = new LocalLogManager();
        private IServiceControls _ServiceCtrl = default;
        public  IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }
        private IAudioChannel AudioDevice_ = new AudioChannel(-1);
        public IAudioChannel AudioDevice {
            get => AudioDevice_;
            set { AudioDevice_ = value; OnPropertyChanged(new string [] {nameof(AudioDevice), nameof(IsPanelEnable)}); }
        }

        public PageAudioData()
        {
            ASettings = new(BasePath.GetAudioStreamSettingsPath(), LogWriter, true);
            ASettings.State.EventCb += ASettings_EventCb;
            AManagerClient = new(ASettings.MMDevice, LogWriter);
            AManagerClient.PropertyChanged += AManagerClient_PropertyChanged;
            DeviceFormats = new ObservableCollection<string>(
                AudioStreamTypeString.GetList(ResourcesAudio.DeviceFormatOptions, ResourcesAudio.DeviceFormatEmpty));
            try
            {
                var mme = ASettings.MMDevice.DeviceNotification;
                mme.DevicePropertyChanged += (s, a) => { this.GetListDevices(); };
                mme.DeviceRemoved += (s, a) => { this.GetListDevices(); };
                mme.DeviceAdded += (s, a) => { this.GetListDevices(); };
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(PageAudioData), LogWriter); }
        }
        ~PageAudioData()
        {
            AManagerClient.Dispose();
            ASettings.Dispose();
        }

        #region Init Config/Channel/Audio
        public void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
        }

        private bool GetListDevices()
        {
            try
            {
                do
                {
                    List<Tuple<string, string>> list = ASettings.MMDevice.GetDevicesBase();
                    ASettings.MMDevice.DisposeDevices();

                    if ((list == default) || list.Count == 0)
                        break;

                    /* add system devices if not edited devices */
                    if (AudioDevices.Count == 0)
                    {
                        foreach (Tuple<string,string> t in list)
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                AudioChannel a = new(AudioDevices.Count);
                                _ = a.Default(t.Item1, t.Item2, false);
                                AudioDevices.Add(a);
                            });
                        break;
                    }
                    /* remove existing devices */
                    for (int i = AudioDevices.Count - 1; i >= 0; i--)
                    {
                        if ((from t in list
                             where !string.IsNullOrWhiteSpace(t.Item1) && t.Item1.Equals(AudioDevices[i].Tag)
                             select t).FirstOrDefault() == default)
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                AudioDevices.RemoveAt(i);
                            });
                        else if (AudioDevices[i].EqualizerChannels.Count != 10)
                            AudioDevices[i].DefaultEqChannels();
                    }
                    /* add system devices */
                    foreach (Tuple<string, string> t in list)
                    {
                        if (string.IsNullOrWhiteSpace(t.Item1))
                            continue;

                        var item = (from i in AudioDevices
                                    where t.Item1.Equals(i.Tag)
                                    select i).FirstOrDefault();
                        if (item == default)
                            Application.Current.Dispatcher.Invoke(() => {
                                AudioChannel a = new(AudioDevices.Count);
                                _ = a.Default(t.Item1, t.Item2, false, true);
                                AudioDevices.Add(a);
                            });
                        else
                            Application.Current.Dispatcher.Invoke(() => {
                                item.DeviceId = t.Item2;
                            });
                    }

                } while (false);

                Application.Current.Dispatcher.Invoke(() =>
                {
                    DeviceIndex = -1;
                    OnPropertyChanged(nameof(AudioDevices));
                });
                return true;

            } catch (Exception ex) { ex.WriteLog('!', nameof(GetListDevices), LogWriter); }
            return false;
        }
        #endregion

        #region Select/Start Audio channel
        private void SelectDevice()
        {
            try
            {
                int idx = DeviceIndex;
                if (!IsIndexDevices(idx))
                    return;

                if (AudioDevice != default)
                    AudioDevice.PropertyChanged -= local_PropertyChanged;

                AManagerClient.Stop();
                AudioDevice = AudioDevices[idx];
                AudioDevice.RefreshPropertyChanged();

                if (IsAudioDevice())
                {
                    if (AudioStreamTypeParse(AudioDevice.Atype))
                        DeviceFormatIndex = AudioDevice.Atype.GetStreamType();
                    AudioDevice.PropertyChanged += local_PropertyChanged;
                    AManagerClient.Start(AudioDevice);
                }
                else
                {
                    DeviceFormatIndex = -1;
                }
            } catch (Exception ex) { ex.WriteLog('!', nameof(SelectDevice), LogWriter); }
        }
        private void AutoStartDevice()
        {
            if (IsAudioDevice())
                AManagerClient.Start(AudioDevice);
            else
                AManagerClient.Stop();
        }
        private void AutoReStartDevice()
        {
            if (IsAudioDevice())
                AManagerClient.ReStart(AudioDevice);
            else
                AManagerClient.Stop();
        }
        private bool AudioStreamTypeParse(AudioStreamType type)
        {
            int[] arr = AudioStreamTypeString.Get(type);
            if ((arr != default) && (arr.Length == 3))
            {
                AudioDevice.Channels = arr[0];
                AudioDevice.BitsPerSample = arr[1];
                AudioDevice.BytesPerSecond = arr[2];
                return true;
            }
            return false;
        }
        #endregion

        private bool IsIndexDevices() => IsIndexDevices(DeviceIndex);
        private bool IsIndexDevices(int idx) => (idx >= 0) && (idx < AudioDevices.Count);
        private bool IsAudioDevice() => (AudioDevice != default) && AudioDevice.IsEnabled;

        public ObservableCollection<string> DeviceFormats { get; private set; }
        public ObservableCollection<AudioChannel> AudioDevices
        {
            get => ASettings.Channels;
            set { ASettings.Channels = value; OnPropertyChanged(nameof(AudioDevices)); }
        }
        private AudioStreamState DeviceState_ = AudioStreamState.None;
        public AudioStreamState DeviceState
        {
            get => DeviceState_;
            set
            {
                DeviceState_ = value;
                OnPropertyChanged(nameof(DeviceState));
            }
        }
        private int DeviceIndex_ = -1;
        public int DeviceIndex
        {
            get => DeviceIndex_;
            set {
                DeviceIndex_ = value;
                SelectDevice();
                OnPropertyChanged(nameof(DeviceIndex), nameof(IsMasterEnable));
            }
        }
        private int DeviceFormatIndex_ = -1;
        public int DeviceFormatIndex
        {
            get => DeviceFormatIndex_;
            set
            {
                DeviceFormatIndex_ = value;
                if (DeviceFormatIndex_ >= 0)
                {
                    AudioDevice.Atype = DeviceFormatIndex_.GetStreamType();
                    _ = AudioStreamTypeParse(AudioDevice.Atype);
                }
                OnPropertyChanged(nameof(DeviceFormatIndex));
                AutoReStartDevice();
            }
        }
        public bool IsPanelEnable
        {
            get => DeviceIndex != -1 && IsAudioDevice();
            set
            {
                if ((DeviceIndex == -1) || (AudioDevice == default))
                    return;
                AudioDevice.IsEnabled = value;
                if (value && AudioStreamTypeParse(AudioDevice.Atype))
                    DeviceFormatIndex = AudioDevice.Atype.GetStreamType();
                AutoStartDevice();
                OnPropertyChanged(nameof(IsPanelEnable));
            }
        }
        public bool IsMasterEnable
        {
            get => DeviceIndex > -1;
        }
        public bool IsMuted
        {
            get => AManagerClient.MasterVolume == 0.0f;
        }
        public void EqPresetCtrl(int idx)
        {
            if (!IsAudioDevice())
                return;

            AudioDevice.EqualizerChannels.SetEqualizerPreset(idx);
        }
        public void MuteCtrl()
        {
            if (IsMuted)
                AManagerClient.MasterVolume = AManagerClient.MasterVolumeMute;
            else
            {
                AManagerClient.MasterVolume = 0.0f;
            }
        }
        public void ReloadCtrl()
        {
            if (IsAudioDevice())
                AManagerClient.Stop();
            AudioDevices.Clear();
            GetListDevices();
            DeviceIndex = -1;
        }
        public void LoadCtrl() => ASettings.Load(default, true);
        public async void SaveCtrl() => await ASettings.Save();
        public void DeleteCtrl() => ASettings.Delete();

        //

        #region event control
        private bool IsAlive_ = true;
        public void OnCloseEvent()
        {
            IsAlive_ = false;
            try {
                Application.Current.Dispatcher.Invoke(async () => await ASettings.Save().ContinueWith((t) =>
                {
                    AManagerClient.Dispose();
                    ASettings.DisposeService();
                }).ConfigureAwait(false));
            } catch (Exception ex) {
                ex.WriteLog('!', nameof(OnCloseEvent), LogWriter);
                try { Application.Current.Dispatcher.Invoke(() => ASettings.DisposeService()); } catch { }
            }
        }
        private async void ASettings_EventCb(AudioStreamState s, bool b)
        {
            if (!IsAlive_)
                return;
            try
            {
                switch (s)
                {
                    case AudioStreamState.Load:
                        {
                            Application.Current.Dispatcher.Invoke(() => ASettings.Copy());
                            _ = await Tasks.Task.FromResult(GetListDevices()).ConfigureAwait(false);
                            break;
                        }
                }
                DeviceState = s;
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(ASettings_EventCb), LogWriter); }
        }

        private void local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (DeviceIndex == -1)
                return;

            switch (e.PropertyName)
            {
                case "IsMono":
                    {
                        if (IsAudioDevice())
                            AutoReStartDevice();
                        break;
                    }
                case "IsEnabled":
                    {
                        OnPropertyChanged(nameof(IsPanelEnable));
                        break;
                    }
            }
        }
        private void AManagerClient_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (DeviceIndex == -1)
                return;

            switch (e.PropertyName)
            {
                case "AudioRawDevice":
                    {
                        // TO-DO: FFT = AudioVisualiseDevice();
                        break;
                    }
            }
        }
        #endregion
    }
}
