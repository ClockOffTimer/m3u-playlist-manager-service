﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceScript
{
    public interface IScriptRunner<T>
    {
        IScriptRunner<T> Init(ScriptsManager<T> man, string rootPath);
        ScriptData BuildScriptData(string file, string content);
        ScriptData GetEngine(ScriptData sd);
        CONTINUESTATE RunDebug<T1>(ScriptData sd, out T1 obj);
        CONTINUESTATE Run<T1>(ScriptData sd, CALLSTATE s, int id, out T1 obj);
        CONTINUESTATE Check(ScriptData sd, CALLSTATE s, T m, out T obj);
    }
}