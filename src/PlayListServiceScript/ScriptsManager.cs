﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PlayListServiceData.Process.Log;

namespace PlayListServiceScript
{
    [Flags]
    public enum CALLSTATE : int
    {
        NONE = 0,
        BEGIN_SCAN = 1,
        BEGIN_LOCALSCAN = 2,
        BEGIN_CHECK = 3,
        BEGIN_LOCALCHECK = 4,
        END_CHECK = 5,
        BEGIN_DOWNLOAD = 6
    };
    [Flags]
    public enum CONTINUESTATE : int
    {
        NONE = 0,
        CONTINUE_CHANGE = 1,
        CONTINUE_NOT_CHANGE = 2,
        CONTINUE_ABORT = 3
    };

    public class ScriptsManager<T>
    {
        private bool IsInit = false;
        private readonly bool[] jsLoadOpt = new bool[6] { false, false, false, false, false, true };
        private readonly int[] scrType = new int[7] { 0, 0, 0, 0, 0, 0, 0 };
        private readonly object __lock = new();
        private string RootPath { get; set; } = default;
        private string contentScript { get; set; } = default;
        private  IScriptRunner<T> runner;
        internal List<ScriptData> data = new();

        public ILogManager WriteLog { get; private set; } = default;

        public int Count
        {
            get { return data.Count; }
        }

        public bool IsConfigJsLib
        {
            get { return jsLoadOpt[0]; }
            set { jsLoadOpt[0] = value; }
        }
        public bool IsConfigJsHost
        {
            get { return jsLoadOpt[1]; }
            set { jsLoadOpt[1] = value; }
        }
        public bool IsConfigJsXMLHttpRequest
        {
            get { return jsLoadOpt[2]; }
            set { jsLoadOpt[2] = value; }
        }
        public bool IsDebugMode
        {
            get { return jsLoadOpt[3]; }
            private set { jsLoadOpt[3] = value; }
        }
        public bool IsDirectoryMode
        {
            get { return jsLoadOpt[4]; }
            private set { jsLoadOpt[4] = value; }
        }
        public bool IsAsyncMode
        {
            get { return jsLoadOpt[5]; }
            private set { jsLoadOpt[5] = value; }
        }

        public ScriptsManager(string root, IScriptRunner<T> run, ILogManager log, bool isasync)
        {
            WriteLog = log;
            IsAsyncMode = isasync;
            IsDebugMode = false;
            contentScript = default;
            Init(root, run);
        }
        public ScriptsManager(string root, IScriptRunner<T> run, ILogManager log, string content = default)
        {
            WriteLog = log;
            IsAsyncMode = true;
            IsDebugMode = false;
            contentScript = content;
            Init(root, run);
        }
        public ScriptsManager(
            string root, IScriptRunner<T> run, ILogManager log, bool isDebugMode, string content = default)
        {
            WriteLog = log;
            IsAsyncMode = !isDebugMode;
            IsDebugMode = isDebugMode;
            contentScript = content;
            Init(root, run);
        }

        private async void InitFileSelector(FileInfo file, string s) { if (IsAsyncMode) await InitFileAsync(file, s); else InitFile(file, s); }
        private async void InitDirectorySelector(DirectoryInfo dir) { if (IsAsyncMode) await InitDirectoryAsync(dir); else InitDirectory(dir); }

        #region PUBLIC BASE methods
        public void ReInit(string content)
        {
            if (content != default)
                contentScript = content;
            if (data.Count > 0)
                Clear();
            if (IsDirectoryMode)
                InitDirectorySelector(new DirectoryInfo(RootPath));
            else
                InitFileSelector(new FileInfo(RootPath), contentScript);
        }

        public bool IsEnable(CALLSTATE cs)
        {
            int cnt = 0;
            lock (__lock)
            {
                cnt = data.Count;
            }
            return (cnt > 0) && (scrType[(int)cs] > 0);
        }

        public void Wait()
        {
            int cnt = 256000;
            while (!IsInit)
            {
                Task.Delay(500);
                Task.Yield();
                if (cnt-- <= 0)
                {
                    lock (__lock)
                    {
                        Clear();
                    }
                    break;
                }
            }
        }

        public void Clear()
        {
            lock (__lock)
            {
                for (int i = (data.Count - 1); i >= 0; i--)
                {
                    try
                    {
                        data[i].Dispose();
                        data.RemoveAt(i);
                    } catch (Exception e) { WriteLog.Error.Write($"Clear -> {e}"); }
                }
            }
        }
        #endregion

        #region Single Engine (DEBUG Mode)
        public void ClearSingleEngine(int idx = 0)
        {
            if (!IsDirectoryMode && (data.Count > idx))
            {
                try
                {
                    ScriptData sd = data[0];
                    data.RemoveAt(idx);
                    sd.Dispose();
                }
                catch { }
            }
        }

        public ScriptData GetSingleEngine(int idx = 0)
        {
            if (!IsDirectoryMode && (data.Count > idx))
                return runner.GetEngine(data[idx]);
            else
                return default;
        }

        public void SetSingleInterrupt(int idx = 0)
        {
            if (!IsDirectoryMode && (data.Count > idx))
                try
                {
                    ScriptData sd = data[idx];
                    if (sd.IsEnable)
                        sd.Interrupt();
                }
                catch { }
        }
        #endregion

        #region Init File/Directory to ScriptData
        private void Init(string root, IScriptRunner<T> run)
        {
            DirectoryInfo dir = new(root);
            if (dir.Exists)
            {
                IsDirectoryMode = true;
                RootPath = Path.Combine(dir.FullName, "scripts");
                dir = new DirectoryInfo(RootPath);
                if (dir == default)
                    return;
                if (!dir.Exists)
                    dir.Create();

                runner = run.Init(this, dir.FullName);
                InitDirectorySelector(dir);
                return;
            }

            if (!Path.HasExtension(root))
                throw new NotSupportedException("file type unknown");

            if (root.EndsWith(".ts", StringComparison.InvariantCultureIgnoreCase) ||
                root.EndsWith(".js", StringComparison.InvariantCultureIgnoreCase) ||
                root.EndsWith(".js.example", StringComparison.InvariantCultureIgnoreCase))
            {
                FileInfo file = new(root);
                if (file.Exists && (!file.IsReadOnly))
                {
                    RootPath = file.FullName;
                    runner = run.Init(this, Path.GetDirectoryName(file.FullName));
                    InitFileSelector(file, contentScript);
                    return;
                }
            }
            throw new Exception("bad path or file name");
        }

        internal async Task InitFileAsync(FileInfo file, string content)
        {
            await Task.Run(() => { InitFile(file, content); }).ConfigureAwait(false);
        }
        private void InitFile(FileInfo file, string content)
        {
            if (InitScriptData(file, content))
                IsInit = true;
        }

        internal async Task InitDirectoryAsync(DirectoryInfo dir)
        {
            await Task.Run(() => { InitDirectory(dir); }).ConfigureAwait(false);
        }
        internal IEnumerable<string> DirectoryEntry(string path, string s)
        {
            return Directory.EnumerateFiles(path, s, SearchOption.TopDirectoryOnly);
        }

        private void InitDirectory(DirectoryInfo dir)
        {
            try
            {
                List<string> list = DirectoryEntry(dir.FullName, "*.js").ToList();
                list.AddRange(DirectoryEntry(dir.FullName, "*.ts"));

                foreach (var f in list)
                {
                    FileInfo file = new(f);
                    if (file.Exists && (!file.IsReadOnly))
                        _ = InitScriptData(file);
                }
            } catch (Exception e) { WriteLog.Error.Write($"Manager> JS Init error -> {e}"); }

            WriteLog.Verbose.Write("Manager> JS init done, total script count:");
            WriteLog.Verbose.Write($"Manager> [SCAN:{scrType[1]}]/[LOCALSCAN:{scrType[2]}]/[CHECK:{scrType[3]}]/[LOCALCHECK:{scrType[4]}]/[END CHECK:{scrType[5]}]/[DOWNLOAD:{scrType[6]}]");
            if (scrType[0] > 0)
                WriteLog.Info.Write($"Manager> JS init warning, unknown type scripts: {scrType[0]}");
            IsInit = true;
        }
        private bool InitScriptData(FileInfo file, string content = default)
        {
            try
            {
                ScriptData sd = runner.BuildScriptData(file.FullName, content);
                if (sd != default)
                {
                    lock (__lock)
                        data.Add(sd);
                    scrType[(int)sd.ScriptType]++;
                    return true;
                }
            } catch (Exception e) { WriteLog.Error.Write($"Manager>, JS Init error -> Data -> {e}"); }
            return false;
        }
        #endregion

        public CONTINUESTATE RunDebug<T1>(ScriptData sd, out T1 obj)
        {
            return runner.RunDebug<T1>(sd, out obj);
        }

        private List<ScriptData> GetScripts(CALLSTATE s)
        {
            lock (__lock)
            {
                return (from sd in data
                        where sd.ScriptType == s && sd.IsEnable
                        select sd).ToList();
            }
        }

        public CONTINUESTATE Run<T1>(CALLSTATE s, out T1 obj)
        {
            obj = default;
            CONTINUESTATE cs = CONTINUESTATE.CONTINUE_NOT_CHANGE;
            try
            {
                List<ScriptData> list = GetScripts(s);
                if ((list == null) || (list.Count == 0))
                    return cs;

                for (int i = 0; i < list.Count; i++)
                {
                    ScriptData sd = list[i];
                    cs = runner.Run<T1>(sd, s, i, out obj);
                    if (cs != CONTINUESTATE.CONTINUE_CHANGE)
                        WriteLog.Verbose.Write($"Warning> JS Run script: {sd.FileName} = {cs}/{(obj != null)}");
                }
            }
            catch (Exception e) { WriteLog.Error.Write($"Manager> JS Run error: {e}"); }
            return cs;
        }

        public CONTINUESTATE Check(CALLSTATE s, T m, out T obj)
        {
            obj = default;
            CONTINUESTATE cs = CONTINUESTATE.CONTINUE_NOT_CHANGE;
            try
            {
                List<ScriptData> list = GetScripts(s);
                if ((list == null) || (list.Count == 0))
                    return cs;

                for (int i = 0; i < list.Count; i++)
                {
                    ScriptData sd = list[i];
                    cs = runner.Check(sd, s, m, out obj);
                    if ((cs == CONTINUESTATE.CONTINUE_CHANGE) ||
                        (cs == CONTINUESTATE.CONTINUE_ABORT))
                        break;
                }
            }
            catch (Exception e) { WriteLog.Error.Write($"Manager> Check error: {e}"); }
            return cs;
        }
    }
}
