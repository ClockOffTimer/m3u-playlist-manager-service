﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using Microsoft.ClearScript.V8;

namespace PlayListServiceScript
{
    public class ScriptRunnerDownload<T> : ScriptRunnerBase<T>, IScriptRunner<T>
    {
        private bool InitOnce = false;

        public ScriptRunnerDownload() { }
        ~ScriptRunnerDownload()
        {
            manager = default;
        }

        public IScriptRunner<T> Init(ScriptsManager<T> man, string rootPath)
        {
            if (InitOnce)
                return this;
            InitOnce = true;
            manager = man;
            return this;
        }

        public override CONTINUESTATE Run<T1>(ScriptData sd, CALLSTATE s, int id, out T1 obj)
        {
            obj = default;
            CONTINUESTATE cs = CONTINUESTATE.CONTINUE_NOT_CHANGE;
            try
            {
                V8ScriptEngine engine = PreLoad__(ref sd);
                lock (sd.engineLock)
                {
                    cs = (CONTINUESTATE)engine.Invoke("Download", new object[] { id });
                    if (cs == CONTINUESTATE.CONTINUE_CHANGE)
                        obj = (T1)engine.Invoke("GetMediaList", default);
                }
            }
            catch (Exception e) { manager.WriteLog.Error.Write($"Runner> Run downloader error: {id}/{sd.ScriptType} -> {e}"); }
            return cs;
        }
    }
}
