﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Log;

namespace PlayListServiceScript
{
    public class xJSON
    {
        public dynamic parse(string s)
        {
            throw new NotImplementedException();
        }
        public string stringify(dynamic obj)
        {
            throw new NotImplementedException();
        }
    }
    public class xPath
    {
        public string GetDirectory(string s)
        {
            return Path.GetDirectoryName(s);
        }
        public string GetFile(string s)
        {
            return Path.GetFileName(s);
        }
        public string GetFileShort(string s)
        {
            return Path.GetFileNameWithoutExtension(s);
        }
        public string GetFileExtension(string s)
        {
            return Path.GetExtension(s);
        }
        public string Combine(params string [] ss)
        {
            return Path.Combine(ss);
        }
        public string Combine(string s1, string s2)
        {
            return Path.Combine(s1, s2);
        }
        public string Combine(string s1, string s2, string s3)
        {
            return Path.Combine(s1, s2, s3);
        }
        public string Combine(string s1, string s2, string s3, string s4)
        {
            return Path.Combine(s1, s2, s3, s4);
        }
    }
    public class xHttp
    {
        public string Get(string url, int timeout = 10)
        {
            try
            {
                return HttpClientDefault.GetHttp(url, timeout);
            } catch (Exception e)
            {
                throw new Exception(e.ExceptionPrint());
            }
        }
        public string Post(string url, string body, int timeout = 10, string mime = default)
        {
            try
            {
                if (mime == default)
                    return HttpClientDefault.PostHttp(url, body, timeout);
                else
                    return HttpClientDefault.PostHttp(url, body, timeout, mime);
            }
            catch (Exception e)
            {
                throw new Exception(e.ExceptionPrint());
            }
        }
    }
    public class xFile
    {
        public string ReadAllText(string s)
        {
            return File.ReadAllText(s);
        }
        public string[] ReadAllLines(string s)
        {
            return File.ReadAllLines(s);
        }
        public void WriteAllText(string s, string txt)
        {
            File.WriteAllText(s, txt);
        }
        public FileInfo Info(string s)
        {
            return new FileInfo(s);
        }
    }

    public class ScriptExportUtils
    {
        public xPath PATH { get; private set; } = new();
        public xFile FILE { get; private set; } = new();
        public xHttp HTTP { get; private set; } = new();

        public Type GetType<T>()
        {
            return typeof(T);
        }

#if DEBUG
        public void TraceVsLog(string s)
        {
            System.Diagnostics.Trace.WriteLine(s);
        }
        public void TraceVsLog(string fmt, params object[] args)
        {
            System.Diagnostics.Trace.WriteLine(string.Format(fmt, args));
        }
#endif
    }
}
