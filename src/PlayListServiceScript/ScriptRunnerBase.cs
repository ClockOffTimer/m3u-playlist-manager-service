﻿using System;
using System.Threading;
using M3U;
using M3U.Model;
using Microsoft.ClearScript;
using Microsoft.ClearScript.V8;

namespace PlayListServiceScript
{
    public abstract class ScriptRunnerBase<T>
    {
        protected ScriptsManager<T> manager = default;
        public Action<V8ScriptEngine> OptExtendedAdd = (a) => { };

        public virtual ScriptData BuildScriptData(string file, string content)
        {
            ScriptData sd = new(manager.WriteLog, file, content);
            if (InitEngine__(ref sd))
                return sd;
            return default;
        }

        public virtual ScriptData GetEngine(ScriptData sd)
        {
            if (sd.GetV8Engine(Thread.CurrentThread.ManagedThreadId, OptLoad__, manager.IsDebugMode) == default)
                return default;
            return sd;
        }

        public virtual CONTINUESTATE RunDebug<T1>(ScriptData sd, out T1 obj)
        {
            throw new NotImplementedException();
        }

        public virtual CONTINUESTATE Run<T1>(ScriptData sd, CALLSTATE s, int id, out T1 obj)
        {
            throw new NotImplementedException();
        }

        public virtual CONTINUESTATE Check(ScriptData sd, CALLSTATE s, T m, out T obj)
        {
            throw new NotImplementedException();
        }

        #region INTERNAL LOAD INIT

        protected bool InitEngine__(ref ScriptData sd)
        {
            try
            {
                V8ScriptEngine engine = PreLoad__(ref sd);
                lock (sd.engineLock)
                {
                    CALLSTATE old = sd.ScriptType;
                    sd.ScriptType = engine.Script.Global.type;
                    if (old != sd.ScriptType)
                        manager.WriteLog.Verbose.Write($"+> JS script Type warning: from name = {old}, from id = {sd.ScriptType}/{sd.FileName}");
                }
                bool b = sd.ScriptType != CALLSTATE.NONE;
                manager.WriteLog.Verbose.Write($"+> JS initialize script Type: {sd.ScriptType}/{sd.FileName} = {b}");
                return b;
            }
            catch (Exception e) { manager?.WriteLog.Error.Write($"ScriptRunnerMedia, Init ->", e); }
            return false;
        }

        protected V8ScriptEngine PreLoad__(ref ScriptData sd)
        {
            if (!sd.IsEnable)
                throw new ArgumentNullException("script content empty");

            V8ScriptEngine engine = sd.GetV8Engine(Thread.CurrentThread.ManagedThreadId, OptLoad__, manager.IsDebugMode);
            if (engine == null)
                throw new NullReferenceException("compiled V8 engine is null");
            return engine;
        }

        protected void OptLoad__(V8ScriptEngine engine)
        {
            engine.AddHostType("ContState",  HostItemFlags.GlobalMembers, typeof(CONTINUESTATE));
            engine.AddHostType("CallState",  HostItemFlags.GlobalMembers, typeof(CALLSTATE));
            engine.AddHostType("xMedia",     HostItemFlags.GlobalMembers, typeof(M3U.Model.Media));
            engine.AddHostType("xAttributes",HostItemFlags.GlobalMembers, typeof(M3U.Model.Attributes));
            engine.AddHostType("xM3uWriter", HostItemFlags.GlobalMembers, typeof(M3U.M3uStreamWriter));
            engine.AddHostType("xMediaList", HostItemFlags.GlobalMembers, typeof(System.Collections.Generic.List<Media>));
            engine.AddHostType("xStringList",HostItemFlags.GlobalMembers, typeof(System.Collections.Generic.List<string>));
            engine.AddHostType("xFileInfo",  HostItemFlags.GlobalMembers, typeof(System.IO.FileInfo));
#if DEBUG
            engine.AddHostType("Console", HostItemFlags.GlobalMembers, typeof(Console));
#endif
            engine.AddHostObject("xUtils", HostItemFlags.GlobalMembers, new ScriptExportUtils());
            if (manager.IsConfigJsXMLHttpRequest)
                engine.AddCOMType("XMLHttpRequest", "MSXML2.XMLHTTP");
            if (manager.IsConfigJsHost)
                engine.AddHostObject("xHost", HostItemFlags.GlobalMembers, new HostFunctions());
            if (manager.IsConfigJsLib)
                engine.AddHostObject("xLib", HostItemFlags.GlobalMembers, new HostTypeCollection("mscorlib", "System", "System.Core"));

            OptExtendedAdd(engine);
        }
        #endregion
    }
}
