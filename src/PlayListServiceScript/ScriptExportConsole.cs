﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Text;
using PlayListServiceData.Process.Log;

namespace PlayListServiceScript
{
    public class ScriptExportConsole
    {
        private ILogManager WriteLog { get; set; } = default;

        public ScriptExportConsole(ILogManager lm)
        {
            WriteLog = lm;
        }

        public void log(string s)
        {
            WriteLog?.Info.Write(s);
        }
        public void log(params object[] args)
        {
            StringBuilder sb = new();
            foreach (var i in args)
                sb.Append($"{i}, ");
            WriteLog?.Info.Write(sb.ToString());
        }
        public void log(string fmt, params object[] args)
        {
            WriteLog?.Info.Write(string.Format(fmt, args));
        }
    }
}
