﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using Microsoft.ClearScript;
using Microsoft.ClearScript.JavaScript;
using Microsoft.ClearScript.V8;
using PlayListServiceData.Process.Log;
using PlayListServiceData.Utils;

namespace PlayListServiceScript
{
    public class ScriptData : IDisposable
    {
        public static string scriptInsertBody = default;
        private CancellationTokenSource tokenCancel = default;
        private V8ScriptEngine engineCompiled = default;
        private ILogManager WriteLog { get; set; } = default;
        private int engineCompiledId = -1;
        public readonly object engineLock = new();
        ///
        public V8Script ScriptCompiled { get; private set; } = default;
        public string FileContent { get; private set; } = default;
        public string FilePath { get; private set; } = default;
        public string FileName { get; private set; } = default;
        public string SharePath { get; private set; } = default;
        public CALLSTATE ScriptType { get; set; }
        public bool IsEnable
        {
            get { return !string.IsNullOrWhiteSpace(FileContent); }
        }
        public bool IsScriptCompiled
        {
            get { return !string.IsNullOrWhiteSpace(FileContent); }
        }
        public bool IsCancell
        {
            get { return tokenCancel != default && tokenCancel.Token.IsCancellationRequested; }
        }
        public V8ScriptEngine GetV8Engine(int id, Action<V8ScriptEngine> engineLoad, bool isdebug = false)
        {
            if (IsCancell)
                throw new ScriptAllreadyCanceledException("data cancelled, not run..");

            bool[] b = new bool[]
            {
                engineCompiledId != id,
                engineCompiled == null
            };
            if (b[0] || b[1])
            {
                if (b[0])
                    engineCompiledId = id;
                if (!b[1])
                    ClearCompiled();
                if (IsEnable)
                {
                    try
                    {
                        lock (engineLock)
                        {
                            V8ScriptEngineFlags flags =
                                   V8ScriptEngineFlags.EnableDynamicModuleImports
                                 | V8ScriptEngineFlags.EnableDateTimeConversion
                                 | V8ScriptEngineFlags.EnableTaskPromiseConversion
                                 | V8ScriptEngineFlags.EnableValueTaskPromiseConversion;
                            if (isdebug)
                                flags |= V8ScriptEngineFlags.EnableDebugging
                                      | V8ScriptEngineFlags.EnableRemoteDebugging
                                      | V8ScriptEngineFlags.AwaitDebuggerAndPauseOnStart;

                            if (isdebug)
                                engineCompiled = new V8ScriptEngine(FileName, flags, 9229);
                            else
                                engineCompiled = new V8ScriptEngine(flags);

                            engineLoad(engineCompiled);
                            engineCompiled.Script.Global = engineCompiled.Script;
                            engineCompiled.DocumentSettings.SearchPath = SharePath;
                            engineCompiled.DocumentSettings.AccessFlags =
                                DocumentAccessFlags.EnforceRelativePrefix | DocumentAccessFlags.EnableAllLoading;

                            if (isdebug)
                            {
                                if (ScriptCompiled != default)
                                    try { ((IDisposable)ScriptCompiled).Dispose(); } catch { }
                                ScriptCompiled = engineCompiled.Compile(
                                    new DocumentInfo() { Category = ModuleCategory.Standard }, FileContent);
                                engineCompiled.ContinuationCallback = () => { return !IsCancell; };
                            }
                            else
                                engineCompiled.Execute(new DocumentInfo() { Category = ModuleCategory.Standard }, FileContent);

                            if (ScriptType == CALLSTATE.NONE)
                                ScriptType = GetCALLSTATE(FileName);
                        }
                    }
#if DEBUG
                    catch (Exception e) { WriteLog.Error.Write($"Data> GetV8Engine -> {e.Message}"); return default; }
#else
                    catch { return default; }
#endif
                }
            }
            return engineCompiled;
        }
        private void ClearCompiled()
        {
            lock (engineLock)
            {
                V8Script script = ScriptCompiled;
                ScriptCompiled = default;
                if (script != default)
                    try { ((IDisposable)script).Dispose(); } catch { }

                V8ScriptEngine engine = engineCompiled;
                engineCompiled = default;
                if (engine != default)
                    try { ((IDisposable)engine).Dispose(); } catch { }

                CancellationTokenSource ts = tokenCancel;
                tokenCancel = default;
                if (ts != default)
                    try { ((IDisposable)ts).Dispose(); } catch { }
            }
        }

        public void Interrupt()
        {
            if ((engineCompiled != default) && (tokenCancel != default))
                tokenCancel.Cancel();
        }

        public void Dispose()
        {
            ClearCompiled();
            GC.SuppressFinalize(this);
        }

        public ScriptData(ILogManager log, string path, string content = default, CALLSTATE state = CALLSTATE.NONE)
        {
            var shared = Path.GetDirectoryName(path);
            WriteLog = log;
            FilePath = path;
            FileName = Path.GetFileNameWithoutExtension(path).ToUpperInvariant();
            SharePath = $"{shared};{Path.Combine(shared, "jslib")};";
            tokenCancel = new ();
            if (content == default)
                FileContent = (scriptInsertBody == default) ? File.ReadAllText(path) : $"{scriptInsertBody}{File.ReadAllText(path)}";
            else
                FileContent = (scriptInsertBody == default) ? content : $"{scriptInsertBody}{content}";

            if (state == CALLSTATE.NONE)
                ScriptType = GetCALLSTATE(FileName);
            else
                ScriptType = state;
        }
        ~ScriptData()
        {
            Dispose();
        }

        public static CALLSTATE GetCALLSTATE(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
                foreach (var i in Enum.GetValues(typeof(CALLSTATE)))
                    if (name.StartsWith(i.ToString(), StringComparison.InvariantCultureIgnoreCase))
                        return (CALLSTATE)i;
            return CALLSTATE.NONE;
        }

        public static bool LoadScriptLib(string s)
        {
            try
            {
                Assembly assembly = Assembly.GetExecutingAssembly();
                string name = $"{assembly.GetName().Name.Replace('\\', '.')}.{s}";
                if (assembly.GetManifestResourceNames().Contains(name))
                {
                    using Stream rs = assembly.GetManifestResourceStream(name);
                    if ((rs == null) || !rs.CanRead)
                        return false;
                    using (StreamReader sr = new(rs, Encoding.UTF8))
                        scriptInsertBody = sr.ReadToEnd();
                    return true;
                }
            }
            catch (Exception e) { System.Diagnostics.Trace.WriteLine($"Data> Load script library -> {e}"); }
            return false;
        }

    }
}
