﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceScript
{
    [Serializable]
    public class ScriptAllreadyCanceledException : Exception
    {
        public ScriptAllreadyCanceledException(string message) : base(message) { }
    }
}
