﻿/* Copyright (c) 2021 PlayListServiceScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using Microsoft.ClearScript.V8;

namespace PlayListServiceScript
{
    public class ScriptRunnerMedia<T> : ScriptRunnerBase<T>, IScriptRunner<T>
    {
        private bool InitOnce = false;

        public ScriptRunnerMedia() { }
        ~ScriptRunnerMedia()
        {
            manager = default;
        }

        public IScriptRunner<T> Init(ScriptsManager<T> man, string rootPath)
        {
            if (InitOnce)
                return this;
            InitOnce = true;
            manager = man;
            return this;
        }

        public override CONTINUESTATE Check(ScriptData sd, CALLSTATE s, T m, out T obj)
        {
            obj = default;
            CONTINUESTATE cs = CONTINUESTATE.CONTINUE_NOT_CHANGE;
            try
            {
                V8ScriptEngine engine = PreLoad__(ref sd);
                lock (sd.engineLock)
                {
                    cs = (CONTINUESTATE)engine.Invoke("CheckMedia", new object[] { s, m });
                    if (cs == CONTINUESTATE.CONTINUE_CHANGE)
                        obj = (T)engine.Invoke("GetMedia", default);
                }
                if ((cs == CONTINUESTATE.CONTINUE_CHANGE) || (cs == CONTINUESTATE.CONTINUE_ABORT))
                    manager.WriteLog.Verbose.Write($"Runner> JS Check script: {sd.FileName} = {cs}/{(obj != null)}");
            }
            catch (Exception e) { manager.WriteLog.Error.Write($"Runner> Check error -> {e}"); }
            return cs;
        }
    }
}
