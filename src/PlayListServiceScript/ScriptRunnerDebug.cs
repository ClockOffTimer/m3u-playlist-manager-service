﻿using System;
using System.IO;
using System.Threading;
using Microsoft.ClearScript.V8;

namespace PlayListServiceScript
{
    public class ScriptRunnerDebug<T> : ScriptRunnerBase<T>, IScriptRunner<T>
    {
        private static readonly string[] AppendScriptText = new string[]
        {
            "\r\nGlobal.result = CheckMedia(CallState.END_CHECK, MediaSource);\n\r",
            "\r\nGlobal.result = Download(1);\n\r"
        };
        public ScriptRunnerDebug() { }
        ~ScriptRunnerDebug()
        {
            manager = default;
        }

        public IScriptRunner<T> Init(ScriptsManager<T> man, string rootPath)
        {
            manager = man;
            return this;
        }

        public override ScriptData BuildScriptData(string file, string content)
        {
            try
            {
                string s = string.Empty;
                CALLSTATE state = ScriptData.GetCALLSTATE(
                    Path.GetFileNameWithoutExtension(file).ToUpperInvariant());
                switch (state)
                {
                    case CALLSTATE.NONE: break;
                    case CALLSTATE.BEGIN_SCAN:
                    case CALLSTATE.BEGIN_CHECK:
                    case CALLSTATE.BEGIN_LOCALSCAN:
                    case CALLSTATE.BEGIN_LOCALCHECK:
                    case CALLSTATE.END_CHECK: s = AppendScriptText[0]; break;
                    case CALLSTATE.BEGIN_DOWNLOAD: s = AppendScriptText[1]; break;
                }
                ScriptData sd = new(manager.WriteLog, file, (content != default) ? $"{content}{s}" : default, state);
                if (sd == default)
                {
                    manager.WriteLog.Error.Write($"Runner> not initialize V8 Engine: {sd.FileName}/{sd.ScriptType}, {sd.IsEnable}/{sd.IsScriptCompiled}");
                    return default;
                }
                V8ScriptEngine engine = PreLoad__(ref sd);
                manager.WriteLog.Verbose.Write($"Runner> initialize: {sd.FileName}/{sd.ScriptType}, {sd.IsEnable}/{sd.IsScriptCompiled}");
                return sd;
            }
            catch (Exception e) { manager?.WriteLog.Error.Write($"Runner> Init debug error ->", e); }
            return default;
        }

        public override CONTINUESTATE RunDebug<T1>(ScriptData sd, out T1 obj)
        {
            obj = default;
            CONTINUESTATE state = CONTINUESTATE.NONE;
            try
            {
                if (sd == default)
                {
                    manager?.WriteLog.Error.Write("runner, not run, data empty..");
                    return state;
                }
                if (!sd.IsEnable || !sd.IsScriptCompiled || sd.IsCancell)
                {
                    manager?.WriteLog.Error.Write($"runner, not run, state: {sd.IsEnable}/{sd.IsScriptCompiled}/{sd.IsCancell}");
                    return state;
                }

                lock (sd.engineLock)
                {
                    V8ScriptEngine engine = sd.GetV8Engine(Thread.CurrentThread.ManagedThreadId, OptLoad__, manager.IsDebugMode);
                    if (engine == default)
                    {
                        manager?.WriteLog.Error.Write("runner, engine is null..");
                        return state;
                    }

                    engine.Execute(sd.ScriptCompiled);

                    if (sd.IsCancell)
                    {
                        manager?.WriteLog.Error.Write("execute interrupt, not end..");
                        return state;
                    }
                    manager?.WriteLog.Error.Write("execute end.");

                    try
                    {
                        state = (CONTINUESTATE)engine.Script.Global.result;
                    }
                    catch (Exception e)
                    {
                        manager.WriteLog.Error.Write($"get result = {e.Message}");
                        try { manager.WriteLog.Error.Write($"reason = {engine.Script.Global.result}"); } catch { }
                        return state;
                    }
                    try
                    {
                        CALLSTATE byname = sd.ScriptType,
                                  bytype = engine.Script.Global.type;
                        if (byname != bytype)
                            manager.WriteLog.Verbose.Write($"type warning: from name = {byname}, from id = {bytype}");
                        else
                            manager.WriteLog.Verbose.Write($"type = {bytype}");
                    } catch { }
                    try
                    {
                        switch (sd.ScriptType)
                        {
                            case CALLSTATE.NONE: break;
                            case CALLSTATE.BEGIN_SCAN:
                            case CALLSTATE.BEGIN_CHECK:
                            case CALLSTATE.BEGIN_LOCALSCAN:
                            case CALLSTATE.BEGIN_LOCALCHECK:
                            case CALLSTATE.END_CHECK:
                                {
                                    obj = (T1)engine.Invoke("GetMedia", default);
                                    break;
                                }
                            case CALLSTATE.BEGIN_DOWNLOAD:
                                {
                                    obj = (T1)engine.Invoke("GetMediaList", default);
                                    break;
                                }
                        }
                    } catch { }
                }
            }
            catch (Exception e) { manager?.WriteLog.Error.Write("Runner> Run debug error ->", e); }
            return state;
        }
    }
}
