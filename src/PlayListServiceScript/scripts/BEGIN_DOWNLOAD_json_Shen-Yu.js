
import {Meta as metaj} from "jsonp.js";
import {Meta as metaf} from 'filedatelib.js';

Global.type = CallState.BEGIN_DOWNLOAD;
Global.countries = ['ru', 'by'];
Global.listm3u = null;
Global.srcurl = "https://github.com/Shen-Yu/iptv-list/raw/gh-pages/channels.json";

Global.Download = function (id) {
    if ((typeof (srcurl) == undefined) || (srcurl == null))
        return ContState.CONTINUE_NOT_CHANGE;

    try {
        let path = xUtils.PATH.Combine(
            xSettings.ConfigM3uPath, 'auto', 'channels_ShenYu.m3u');

        let d = new Date();
        let edays = d.daysFileExpire(path, 3);
        if (edays != 0) {
            xConsole.log("file not expire, left " + edays + " days, (" + d.shortDate() + ") channels [ShenYu]");
            return ContState.CONTINUE_NOT_CHANGE;
        }

        let json = xUtils.HTTP.Get(srcurl);
        if (json == null)
            return ContState.CONTINUE_ABORT;

        let obj = JSON.parse(json);
        if ((!Array.isArray(obj)) || (!obj.hasOwnProperty('length')) || (obj.length == 0))
            return ContState.CONTINUE_NOT_CHANGE;

        if ((typeof(listm3u) == undefined) || (listm3u == null))
            listm3u = new xMediaList();
        else
            listm3u.Clear();

        for (var ele of obj) {
            if (!CheckCountries(ele))
                continue;
            var g = (ele.hasOwnProperty('category')) ? ele.category : '';
            var l = (ele.hasOwnProperty('logo')) ? ele.logo : '';
            var n = (ele.hasOwnProperty('name')) ? ele.name : '';
            var u = (ele.hasOwnProperty('url')) ? ele.url : '';
            var t = (ele.hasOwnProperty('tvg')) ? ele.tvg : {};
            var m = new xMedia(u, n, g, l);
            if ((t.hasOwnProperty('id')) && (t.id != null))
                m.Attributes.JsSetTvgId(t.id);
            if ((t.hasOwnProperty('name')) && (t.name != null))
                m.Attributes.JsSetTvgName(t.id);
            if ((t.hasOwnProperty('url')) && (t.url != null))
                m.Attributes.JsSetTvgUrl(t.url);
            listm3u.Add(m);
        };
        if (listm3u.Count > 0) {
            new xM3uWriter()
                .SetLog(function(s_) { xConsole.log(id, s_); })
                .Write(path, listm3u);
        }

        /* listm3u.Clear(); Debug Only */
        return ContState.CONTINUE_CHANGE;

    } catch (error) {
        xConsole.log(id, error.stack);
        return ContState.CONTINUE_ABORT;
    }
}

Global.GetMediaList = function () {
    return listm3u;
}

Global.ClearMediaList = function () {
    if (listm3u != null)
        listm3u.Clear();
}

Global.CheckCountries = function (ele) {
    if ((countries == null) || (countries.length == 0))
        return false;

    if ((ele.hasOwnProperty('countries')) && (Array.isArray(ele.countries)) && (ele.countries.length > 0) && (countries.length > 0)) {
        for (var c of ele.countries) {
            if ((!c.hasOwnProperty('code')) || (c.code == null))
                continue;

            for (var cc of countries) {
                if (c.code.toUpperCase() == cc.toUpperCase())
                    return true;
            }
        }
    }
    return false;
}
