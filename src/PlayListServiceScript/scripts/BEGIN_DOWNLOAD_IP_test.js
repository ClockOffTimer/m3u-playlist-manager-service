
import  {Meta} from "jsonp.js";

Global.type = CallState.BEGIN_DOWNLOAD;
Global.listm3u = null;
Global.srcurl = 'http://httpbin.org/ip';

Global.Download = function (id) {
    try {
            let json = xUtils.HTTP.Get(srcurl);
            if (json == null)
                throw new Error('json == null');

            xUtils.FILE.WriteAllText("test.json", json);
            xConsole.log(json);

            return ContState.CONTINUE_CHANGE;

    } catch (error) {
        xConsole.log(id, error.stack);
        return ContState.CONTINUE_ABORT;
    }
}

Global.GetMediaList = function () {
    return listm3u;
}

Global.ClearMediaList = function () {
    if (listm3u != null)
        listm3u.Clear();
}
