﻿JS Script:

Input check event:

    enum CALLSTATE
    {
        NONE = 0,
        BEGIN_SCAN = 1,
        BEGIN_LOCALSCAN = 2,
        BEGIN_CHECK = 3,
        BEGIN_LOCALCHECK = 4,
        END_CHECK = 5
    };

Output check state:

    enum CONTINUESTATE
    {
        NONE = 0,
        CONTINUE_CHANGE = 1,
        CONTINUE_NOT_CHANGE = 2,
        CONTINUE_ABORT = 3
    };

Media object fields:

        Media.Duration - decimal
        Media.Title.InnerTitle - string
        Media.Title.RawTitle - string
        Media.MediaFile - string
        Media.MediaFileExt - string
        Media.MediaFileMime - string
        Media.HttpUA - string
        Media.IsStream - boolean
        Media.ParserId - number

        Media.Attributes.TvgShift - string
        Media.Attributes.TvgName - string
        Media.Attributes.TvgLogo - string
        Media.Attributes.TvgId - string
        Media.Attributes.UrlTvg - string
        Media.Attributes.GroupTitle - string
        Media.Attributes.AudioTrack - string
        Media.Attributes.AspectRatio - string
        Media.Attributes.M3UAutoLoad - number
        Media.Attributes.Cache - number
        Media.Attributes.Deinterlace - number
        Media.Attributes.Refresh - number
        Media.Attributes.ChannelNumber - number

Constructors:

        Media(string url, string name, long duration)
        Media(string url, string name, string group, string logo)

Attributes functions:

        JsSetTvgShift(string val)
        JsSetTvgName(string val)
        JsSetTvgLogo(string val)
        JsSetTvgId(string val)
        JsSetTvgUrl(string val)
        JsSetGroupTitle(string val)
        JsSetAudioTrack(string val)
        JsSetAspectRatio(string val)
        JsSetM3UAutoLoad(int val)
        JsSetCache(int val)
        JsSetDeinterlace(int val)
        JsSetRefresh(int val)
        JsSetChannelNumber(int val)

Example base script source:

    Global.type = CallState.BEGIN_CHECK;
    Global.m3u = null;

    Global.Download = function(id)
    {
        ...
    }
    Global.GetMediaList = function()
    {
        ...
    }

    Global.CheckMedia = function(state, media) {
        if ((media.Title.InnerTitle == "xxx") || (media.Attributes.GroupTitle == "XYX"))
        {
            m3u = media;
            m3u.Title.InnerTitle = "yyy";
            m3u.Attributes.GroupTitle = "YYY"

            // or -> m3u = new Media(url, title, duration)

            m3u = new Media(media.MediaFile, "yyy", media.Duration);
            m3u.Attributes.GroupTitle = "YYY"

            xConsole.log("JS M3U modify: {0}/{1} {2}", type, state, media.MediaFile);
            return CONTINUE_CHANGE;
        }
        else if (media.MediaFile == "")
        {
            return ContState.CONTINUE_ABORT;
        }
        return ContState.CONTINUE_NOT_CHANGE;
    }
    Global.GetMedia = function() {
        return m3u;
    }


Read more:
    https://github.com/microsoft/ClearScript/blob/master/ClearScriptTest/JavaScript/General.js
    https://github.com/microsoft/ClearScript/blob/master/ClearScriptTest/JScriptEngineTest.cs
