﻿Put JS library here.
Using in js sript:

                import {Meta} from 'jsonp.js';
                // Create default JSON instance
                let object = JSON.parse(string);
                let string = JSON.stringify(object);

                import {Meta, HTTP} from 'httplib.js';
                // Create default HTTP client
                let xhr1 = HTTP.getUrl(string);
                let xhr2 = HTTP.postUrl(string, postdata);

                // Next, you code attach:
                import {MyPrint} from 'MyLib.js';
                import {MyAdd} from 'MyLib.js';
                MyPrint(MyAdd(10, 20));

Example js library, file MyLib.js:

                export function MyPrint(txt) {
                    Utils.DebugLog('The text = {0}', txt);
                    }
                export function MyAdd(var1, var2) {
                    return var1 + var2;
                    }

Example XMLHttpRequest support:

        var xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://www.google.com', false);
        xhr.send();
        if (xhr.status == 200) {
            Utils.DebugLog("JS getUrl: {0}/{1}", xhr.status, xhr.responseText);
            return xhr.responseText;
        }
        throw new Error('Request failed: ' + xhr.status);

        xhr = new XMLHttpRequest();
        xhr.open('POST', 'http://myhost.org/post', true);
        xhr.send('Hello, world!');
        xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
            Utils.DebugLog("JS postUrl: {0}/{1}", xhr.status, JSON.parse(xhr.responseText).data);
        }};


Read more:
    https://developer.mozilla.org/ru/docs/Web/JavaScript/Guide/Modules
    https://github.com/microsoft/ClearScript/blob/master/ClearScriptTest/JavaScript/General.js
    https://github.com/microsoft/ClearScript/blob/master/ClearScriptTest/JScriptEngineTest.cs
