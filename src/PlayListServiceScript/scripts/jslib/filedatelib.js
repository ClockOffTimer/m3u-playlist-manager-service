
Date.prototype.checkFileExpire = function (s, i) {
    let f = new xFileInfo(s);
    if (!f.Exists)
        return true;

    let d = Math.round(((((this - f.CreationTime) / 1000) / 60) / 60) / 24, 1);
    if (d <= i)
        return false;
    f.Delete();
    return true;
};
Date.prototype.daysFileExpire = function (s, i) {
    let f = new xFileInfo(s);
    if (!f.Exists)
        return 0;

    let d = Math.round(((((this - f.CreationTime) / 1000) / 60) / 60) / 24, 1);
    if (d <= i)
        return (i - d);
    f.Delete();
    return 0;
};
Date.prototype.shortDate = function () {
    return this.toISOString().substring(0, 10);
};
export const Meta = import.meta;
