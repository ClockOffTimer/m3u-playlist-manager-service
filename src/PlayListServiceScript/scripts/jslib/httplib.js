

Global.HTTP = {
    getUrl: function (url) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.send();
        if (xhr.status == 200) {
            Utils.DebugLog("JS getUrl: {0}", xhr.status);
            return xhr.responseText;
        }
        throw new Error('Request failed: ' + xhr.status);
    },
    postUrl = function (url, data) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url, true);
        xhr.send(data);
        if (xhr.status == 200) {
            Utils.DebugLog("JS postUrl: {0}", xhr.status);
            return JSON.parse(xhr.responseText).data;
        }
        throw new Error('Request failed: ' + xhr.status);
    }
};

export const Meta = import.meta;
export const HTTP;

