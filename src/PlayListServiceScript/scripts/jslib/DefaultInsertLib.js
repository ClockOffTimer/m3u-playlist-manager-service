

const console = {
    log: function () {
        var args = Array.prototype.slice.call(arguments, 0);
        xConsole.log(args.join(', '));
    },
    fmt: function (text) {
        var args = Array.prototype.slice.call(arguments, 1);
        xConsole.log(text, args);
    },
    info: function (text) {
        xConsole.log(text);
    },
    warn: function (text) {
        xConsole.log(text);
    },
    error: function (text) {
        xConsole.log(text);
    }
};
