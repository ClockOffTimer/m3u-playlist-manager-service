
import {Meta} from 'filedatelib.js';

Global.type = CallState.BEGIN_DOWNLOAD;
Global.countries = ['ru', 'by'];
Global.languages = ['rus'];
Global.categories = ['xxx'];
Global.listm3u = null;
Global.srcurl = 'https://iptv-org.github.io/iptv/';

Global.DownloadUrl = function (id, tag, name, date) {
    try {

        do {
            if ((srcurl == null) || (tag == null) || (name == null))
                break;

            let path = xUtils.PATH.Combine(
                xSettings.ConfigM3uPath, 'auto', 'iptv_' + tag + '_' + name + '.m3u');

            let edays = date.daysFileExpire(path, 3);
            if (edays != 0) {
                xConsole.log("file not expire, left " + edays + " days, (" + date.shortDate() + ") " + tag + " [" + name + "]");
                break;
            }

            let m3u = xUtils.HTTP.Get(srcurl + tag + '/' + name + '.m3u');
            if (m3u == null)
                break;

            xUtils.FILE.WriteAllText(path, m3u);
            return ContState.CONTINUE_CHANGE;

        } while (false);
        return ContState.CONTINUE_NOT_CHANGE;

    } catch (error) {
        xConsole.log(id, error.stack);
        return ContState.CONTINUE_ABORT;
    }
}

Global.Download = function (id) {
    try {
        var b = ContState.CONTINUE_NOT_CHANGE;
        var d = new Date();

        if ((Array.isArray(countries)) && (countries.hasOwnProperty('length')) && (countries.length > 0)) {
            for (var ele of countries)
                b = (DownloadUrl(id, 'countries', ele, d) == ContState.CONTINUE_CHANGE) ? ContState.CONTINUE_CHANGE : b;
        }
        if ((Array.isArray(languages)) && (languages.hasOwnProperty('length')) && (languages.length > 0)) {
            for (var ele of languages)
                b = (DownloadUrl(id, 'languages', ele, d) == ContState.CONTINUE_CHANGE) ? ContState.CONTINUE_CHANGE : b;
        }
        if ((Array.isArray(categories)) && (categories.hasOwnProperty('length')) && (categories.length > 0)) {
            for (var ele of categories)
                b = (DownloadUrl(id, 'categories', ele, d) == ContState.CONTINUE_CHANGE) ? ContState.CONTINUE_CHANGE : b;
        }
        return b;

    } catch (error) {
        xConsole.log(id, error.stack);
        return ContState.CONTINUE_ABORT;
    }
}

Global.GetMediaList = function () {
    return listm3u;
}

Global.ClearMediaList = function () {
    if (listm3u != null)
        listm3u.Clear();
}
