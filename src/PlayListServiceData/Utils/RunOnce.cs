﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Utils
{
    public class RunOnce
    {
        private volatile bool IsRuning = false;
        public bool GetOnce()
        {
            if (!IsRuning)
            {
                IsRuning = true;
                return false;
            }
            return IsRuning;
        }
        public void Reset()
        {
            IsRuning = false;
        }
    }
}
