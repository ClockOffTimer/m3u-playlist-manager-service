﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using System.Reflection;

namespace PlayListServiceData.Utils
{
    public class CacheValue<T> : IDisposable where T : class
    {
        private volatile bool IsIDisposable = false;
        private Lazy<T> LazyObj { get; set; } = default;

        public T Value
        {
            get { return LazyObj.Value; }
        }

        public CacheValue() { }
        public CacheValue(Func<T> fun)
        {
            Set(fun);
        }
        public CacheValue(T val)
        {
            Set(val);
        }
        ~CacheValue()
        {
            Dispose();
        }
        public T Get()
        {
            if (LazyObj == default)
                throw new NullReferenceException("Lazy container is null");

            return LazyObj.Value;
        }
        public void Set(Func<T> fun)
        {
            Dispose();
            LazyObj = new Lazy<T>(() =>
            {
                T val = fun.Invoke();
                IsIDisposable = IsIDisposableObject(val);
                return val;
            }, true);
        }
        public void Set(T val)
        {
            if (val == default)
                return;

            Dispose();
            IsIDisposable = IsIDisposableObject(val);
            LazyObj = new Lazy<T>(() => val, true);
        }
        public void Dispose()
        {
            T val = ((LazyObj == default) || !LazyObj.IsValueCreated) ? default : LazyObj.Value;
            LazyObj = default;
            if (val != default)
            {
                if (IsIDisposable)
                    ((IDisposable)val).Dispose();
                IsIDisposable = false;
                GC.SuppressFinalize(this);
            }
        }
        [DebuggerNonUserCode]
        private bool IsIDisposableObject(T val)
        {
            try
            {
                if (val == default)
                    return false;

                Type t = typeof(T);
                InterfaceMapping m = t.GetInterfaceMap(typeof(IDisposable));
                MethodInfo mi = t.GetMethod("Dispose");
                if ((mi != default) && (m.TargetMethods.Length > 0) && (mi == m.TargetMethods[0]))
                    return true;
            } catch { }
            return false;
        }
    }
}
