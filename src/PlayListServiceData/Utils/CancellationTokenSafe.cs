﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;

namespace PlayListServiceData.Utils
{
    public class TokenSafe : IDisposable
    {
        private WaitHandle _whandle = default;
        private CancellationTokenSafe stoken_;
        private bool Check() => (stoken_ != default) && stoken_.IsAlive;
        public TokenSafe(CancellationTokenSafe _stoken) => stoken_ = _stoken;
        ~TokenSafe() => Dispose();

        public bool CanBeCanceled
        {
            get {
                do {
                    if (!Check())
                        break;
                    CancellationToken token = stoken_.Token;
                    if (token == null)
                        break;
                    return token.CanBeCanceled;

                } while (false);
                return false;
            }
        }
        public bool IsCancellationRequested
        {
            get {
                do {
                    if (!Check())
                        break;
                    CancellationToken token = stoken_.Token;
                    if (token == null)
                        break;
                    return token.IsCancellationRequested;

                } while (false);
                return true;
            }
        }
        public WaitHandle WaitHandle
        {
            get {
                do {
                    if (!Check())
                        break;

                    CancellationToken token = stoken_.Token;
                    if (token == null)
                        break;
                    if (token.IsCancellationRequested)
                        throw new OperationCanceledException(nameof(TokenSafe));

                    if (_whandle == default)
                        _whandle = Check() ? token.WaitHandle : default;

                } while (false);
                return _whandle;
            }
        }
        public void ThrowIfCancellationRequested()
        {
            try {
                do {
                    if (!Check())
                        break;
                    CancellationToken token = stoken_.Token;
                    if (token == null)
                        break;
                    token.ThrowIfCancellationRequested();

                } while (false);
            }
            catch { Dispose(true); throw; }
        }
        public CancellationTokenRegistration Register(Action<object> act, object state, bool sync = false)
        {
            do {
                if (!Check())
                    break;

                CancellationToken token = stoken_.Token;
                if (token == null)
                    break;
                if (token.IsCancellationRequested)
                    throw new OperationCanceledException(nameof(TokenSafe));

                return token.Register(act, state, sync);

            } while (false);
            return default;
        }
        public CancellationTokenRegistration Register(Action act, bool sync = false)
        {
            do {
                if (!Check())
                    break;
                CancellationToken token = stoken_.Token;
                if (token == null)
                    break;
                if (token.IsCancellationRequested)
                    throw new OperationCanceledException(nameof(TokenSafe));

                return token.Register(act, sync);

            } while (false);
            return default;
        }

        public void Dispose() => Dispose(false);
        public void Dispose(bool b)
        {
            WaitHandle wh = _whandle;
            _whandle = default;
            if (wh != null)
                try { wh.Dispose(); } catch { }
            if (!b) {
                stoken_ = default;
            }
            GC.SuppressFinalize(this);
        }
    }

    public class CancellationTokenSafe : IDisposable
    {
        private CancellationTokenSource stoken_;
        private bool Check(object obj) => obj != default;
        private bool Check() => Check(stoken_) && !IsDisposed && !stoken_.IsCancellationRequested;
        public TokenSafe SafeToken;
        public CancellationTokenSafe() { stoken_ = new(); SafeToken = new(this); }
        public CancellationTokenSafe(TimeSpan ts) { stoken_ = new(ts); SafeToken = new(this); }
        public CancellationTokenSafe(CancellationTokenSource st) { stoken_ = st; SafeToken = new(this); }
        ~CancellationTokenSafe() => Dispose();

        public void Dispose() => Dispose(false);
        public async void Dispose(bool b)
        {
            SafeToken?.Dispose();
            CancellationTokenSource st = stoken_;
            stoken_ = default;
            if (st != null)
            {
                if (b && !st.IsCancellationRequested)
                    try {
                        st.Cancel();
                        await System.Threading.Tasks.Task.Delay(1000).ConfigureAwait(false);
                    } catch { }
                try { st.Dispose(); } catch { }
                IsDisposed = true;
            }
            GC.SuppressFinalize(this);
        }
        public CancellationToken Token => Check() ? stoken_.Token : default;
        public bool IsActive => Check();
        public bool IsAlive => Check(stoken_) && !IsDisposed;
        public bool IsDisposed { get; private set; } = false;
        public bool IsCancellationRequested => !Check(stoken_) || stoken_.IsCancellationRequested;
        public void Cancel()
        {
            if (Check())
                stoken_?.Cancel();
        }
        public void Cancel(bool b) {
            if (Check())
                stoken_?.Cancel(b);
        }
        public void CancelAfter(TimeSpan ts = default)
        {
            if (Check())
            {
                ts = (ts == default) ? TimeSpan.Zero : ts;
                stoken_?.CancelAfter(ts);
            }
        }
        public CancellationTokenSafe Renew()
        {
            try
            {
                SafeToken?.Dispose();
                Dispose(true);
                stoken_ = new();
                SafeToken = new(this);
                IsDisposed = false;
            } catch { }
            return this;
        }
    }
}
