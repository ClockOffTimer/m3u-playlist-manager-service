﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Threading;

namespace PlayListServiceData.Utils
{
    public static class HumanizeExtension
    {
        public static string Humanize(this TimeSpan ts, string[] ss)
        {
            if ((ts == TimeSpan.Zero) || (ss == default))
                return "..";

            StringBuilder sb = new();
            if ((ts.Days > 0) && (ss.Length >= 1))
                sb.Append($"{ts.Days} {ss[0]} ");
            if ((ts.Hours > 0) && (ss.Length >= 2))
                sb.Append($"{ts.Hours} {ss[1]} ");
            if ((ts.Minutes > 0) && (ss.Length >= 3))
                sb.Append($"{ts.Minutes} {ss[2]}");
            if ((ts.Seconds > 0) && (ss.Length >= 4) && (ts.Days == 0) && (ts.Hours == 0) && (ts.Minutes == 0))
                sb.Append($"{ts.Seconds} {ss[3]}");
            return sb.ToString();
        }
        public static string HumanizeDayOfWeek(this TimeSpan ts)
        {
            do
            {
                if ((ts == TimeSpan.Zero) || (ts == Timeout.InfiniteTimeSpan))
                    break;
                DayOfWeek? day = ts.Days switch
                {
                    0 => DayOfWeek.Sunday,
                    1 => DayOfWeek.Monday,
                    2 => DayOfWeek.Tuesday,
                    3 => DayOfWeek.Wednesday,
                    4 => DayOfWeek.Thursday,
                    5 => DayOfWeek.Friday,
                    6 => DayOfWeek.Saturday,
                    7 => DayOfWeek.Sunday,
                    _ => null
                };
                if (day == null)
                    break;

                return day?.ToString();

            } while (false);
            return "-";
        }
        public static string Humanize(this int size) => HumanizeExtension.Humanize((long)size);
        public static string Humanize(this long size)
        {
            string[] tags = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
            if (size == 0)
                return "0" + tags[0];
            int idx = Convert.ToInt32(Math.Floor(Math.Log(size, 1024)));
            double num = Math.Round(size / Math.Pow(1024, idx), 1);
            return num + tags[idx];
        }
    }
}
