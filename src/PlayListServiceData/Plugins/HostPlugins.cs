﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PlayListServiceData.Plugins
{
    [Flags]
    public enum PluginType : int
    {
        PLUG_NONE = 0,
        PLUG_PLACE_TAB = 1,
        PLUG_PLACE_BTN = 2
    }
    public class HostPlugins<T>
    {
        private T exportValue = default;
        private CompositionContainer container = default;
        [ImportMany(typeof(IPlugin), AllowRecomposition = true)]
        private IEnumerable<Lazy<IPlugin, IPluginMetadata>> plugins = default;

        public delegate void ChangeCatalog(List<IPlugin> list, PluginGuiList plist);
        public event ChangeCatalog changePluginList;
        public string searchPattern { get; private set; } = default;
        public PluginGuiList GuiList { get; private set; } = default;

        public HostPlugins(ChangeCatalog ev, string pattern, T exval, PluginType type = PluginType.PLUG_PLACE_TAB, StringCollection sc = default)
        {
            exportValue = exval;
            searchPattern = pattern;
            changePluginList += ev;
            GuiList = new PluginGuiList();
            GuiList.Import(sc);
            Load(type);
        }
        ~HostPlugins()
        {
            Clear();
        }

        internal void Clear()
        {
            var cont = container;
            container = null;
            if (cont != null)
            {
                try
                {
                    if (plugins != null)
                    {
                        foreach (var i in plugins)
                            cont.ReleaseExport(i);
                    }
                    cont?.Dispose();
                }
                catch { }
            }
        }

        public void Load(PluginType type)
        {
            try
            {
                Clear();

                AggregateCatalog catalog = new AggregateCatalog();
                catalog.Catalogs.Add(new DirectoryCatalog(
                    Path.GetDirectoryName(Assembly.GetAssembly(typeof(HostPlugins<T>)).Location),
                    searchPattern
                    ));
                container = new CompositionContainer(catalog, true, null);
                container.ComposeExportedValue(exportValue);
                container.ComposeParts(this);

                try
                {
                    if ((changePluginList != null) && (plugins != null) && (plugins.Count() > 0))
                        changePluginList?.Invoke(GetPlugins(type), GuiList);
                }
#if DEBUG
                catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.ToString()); }
#else
                catch { }
#endif
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e.ToString()); }
#else
            catch { }
#endif
        }

        public int PluginsCount => plugins != null ? plugins.Count() : 0;
        public List<IPlugin> GetPlugins(PluginType type, int num = -1)
        {
            List<IPlugin> list = new List<IPlugin>();
            foreach (var i in plugins)
            {
                if ((i.Metadata != null) && (i.Metadata.pluginType == type))
                {
                    if ((num != -1) && (num != i.Metadata.Id))
                        continue;
                    if (i.Value == null)
                        continue;
                    i.Value.Id = i.Metadata.Id;
                    list.Add(i.Value);
                }
            }
            return list.OrderBy(o => o.Id).ToList();
        }
    }
}
