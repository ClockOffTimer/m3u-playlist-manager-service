﻿namespace PlayListServiceData.Plugins
{
    public class PluginGuiItem
    {
        public bool IsFound { get; set; } = false;
        public bool IsEnable { get; set; } = true;
        public int Id { get; set; } = -1;
        public string Title { get; set; } = default;
        public IPlugin Context { get; set; } = default;

        public PluginGuiItem(IPlugin plug)
        {
            Id = plug.Id;
            Title = plug.TitleShort.ToString().Trim();
            Context = plug;
        }
        public PluginGuiItem(int id, string title, bool isenable)
        {
            Id = id;
            Title = title.Trim();
            IsEnable = isenable;
        }
    }
}
