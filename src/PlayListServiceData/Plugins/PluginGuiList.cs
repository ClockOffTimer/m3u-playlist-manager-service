﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace PlayListServiceData.Plugins
{
    public class PluginGuiList : INotifyPropertyChanged
    {
        private readonly ObservableCollection<PluginGuiItem> plugList_ =
            new ObservableCollection<PluginGuiItem>();
        public ObservableCollection<PluginGuiItem> Plugins => plugList_;
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void Import(StringCollection c)
        {
            if ((c != default) && (c.Count > 0))
            {
                plugList_.Clear();

                foreach (var i in c)
                {
                    if (string.IsNullOrWhiteSpace(i))
                        continue;

                    string[] s = i.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    do
                    {
                        if (s.Length != 3)
                            break;

                        if (!int.TryParse(s[0], out int id))
                            break;

                        if (!bool.TryParse(s[2], out bool en))
                            break;

                        plugList_.Add(new PluginGuiItem(id, s[1].Trim(), en));

                    } while (false);
                }
                OnPropertyChanged(nameof(Plugins));
            }
        }
        public bool Update(IPlugin p)
        {
            var a = Get(p.Id);
            if (a == default)
                plugList_.Add(new PluginGuiItem(p));
            else
                a.Context = p;

            OnPropertyChanged(nameof(Plugins));
            return (a == default) || a.IsEnable;
        }
        public StringCollection Export()
        {
            StringCollection c = new StringCollection();
            if (plugList_.Count == 0)
                return c;

            foreach (var i in plugList_)
            {
                c.Add($"{i.Id}|{i.Title}|{i.IsEnable}");
            }
            return c;
        }
        public PluginGuiItem Get(string s)
        {
            return Get(plugList_, s);
        }
        public PluginGuiItem Get(int n)
        {
            return Get(plugList_, n);
        }
        public PluginGuiItem Get(IEnumerable<PluginGuiItem> i, string s)
        {
            return (from p in i
                    where p.Title.Equals(s)
                    select p).FirstOrDefault();
        }
        public PluginGuiItem Get(IEnumerable<PluginGuiItem> i, int n)
        {
            return (from p in i
                    where p.Id == n
                    select p).FirstOrDefault();
        }
    }
}
