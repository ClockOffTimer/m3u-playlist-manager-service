﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Plugins
{
    public interface IPlugin
    {
        int Id { get; set; }
        string Title { get; }
        string TitleShort { get; }
        IPlugin GetView { get; }
    }
}
