﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;

namespace PlayListServiceData.Plugins
{
    public interface IPluginMetadata
    {
        [DefaultValue(-1)]
        int Id { get; }
        [DefaultValue(0)]
        PluginType pluginType { get; }
    }
}
