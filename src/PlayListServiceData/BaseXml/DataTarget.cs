﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.BaseXml
{
    public enum DataTarget : int
    {
        None = 0,
        Id_GroupWildcardChannels,
        Id_GroupAlias,
        Id_EpgAlias,
        Id_EpgFavorite,
        Id_EpgChannels,
        Id_EpgPrograms
    };
}
