﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Process.Log;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceData.BaseXml
{
    public class DataBuilder<T2> : IDisposable where T2 : class, new()
    {
        private readonly ILogManager Log_;
        private readonly ConcurrentDictionary<DataTarget, Lazy<T2>> Base_;
        private readonly Dictionary<DataTarget, Tuple<DataTarget, string, Func<DataTarget, string, T2>>> BaseInit_;

        public DataBuilder(ILogManager lm)
        {
            Log_ = lm;
            BaseInit_ = new Dictionary<DataTarget, Tuple<DataTarget, string, Func<DataTarget, string, T2>>>();
            Base_ = new ConcurrentDictionary<DataTarget, Lazy<T2>>();
        }
        ~DataBuilder()
        {
            Dispose();
        }

        public void Dispose()
        {
            try { Base_.Clear(); } catch { }
            try { BaseInit_.Clear(); } catch { }
        }

        public void Clear(DataTarget target = DataTarget.None)
        {
            try
            {
                if ((target == DataTarget.None) || (Base_.Count == 0))
                {
                    if (BaseInit_.Count > 0)
                        LazyInit_();
                    return;
                }
                if (Base_.TryRemove(target, out Lazy<T2> _))
                {
                    if (BaseInit_.Count == 0)
                        return;
                    var kvp = (from i in BaseInit_
                               where i.Key == target
                               select i).FirstOrDefault();
                    if (kvp.Value == default)
                        return;

                    _ = Base_.TryAdd(
                        kvp.Key,
                        new Lazy<T2>(() => kvp.Value.Item3.Invoke(kvp.Value.Item1, kvp.Value.Item2)));
                }
            } catch (Exception e) { _ = e.WriteLog($"clear:", Log_); }
        }

        public void InitLazy()
        {
            try
            {
                LazyInit_();
            } catch (Exception e) { _ = e.WriteLog($"init lazy:", Log_); }
        }

        public async Tasks.Task InitAllAsync()
        {
            await Tasks.Task.Run(() =>
            {
                InitAll();
            }).ConfigureAwait(false);
        }

        public void InitAll()
        {
            try
            {
                LazyInit_();

                var t = new Thread(() =>
                {
                    foreach (var n in Base_)
                    {
                        try
                        {
                            if (!n.Value.IsValueCreated)
                                _ = n.Value.Value;
                        } catch (Exception e) { _ = e.WriteLog($"init base '{n.Key}':", Log_); }
                    }
                });
                t.Start();
                t.Join(TimeSpan.FromMinutes(5));

            } catch (Exception e) { _ = e.WriteLog($"init All:", Log_); }
        }

        public bool IsBaseEnable(DataTarget target)
        {
            if (Base_.TryGetValue(target, out Lazy<T2> val))
            {
                if ((val == default) || (val.Value == default))
                    return false;
                if (val.Value is ServiceDataItems clz)
                    return clz.Items.Count > 0;
            }
            return false;
        }

        public T2 Get(DataTarget target)
        {
            if (Base_.TryGetValue(target, out Lazy<T2> val))
                return (val == default) ? default : val.Value;
            return default;
        }

        public void Add(DataTarget target, string path, Func<DataTarget, string, T2> fun)
        {
            try
            {
                if (BaseInit_.ContainsKey(target))
                    return;
                BaseInit_.Add(
                    target,
                    new Tuple<DataTarget, string, Func<DataTarget, string, T2>>(target, path, fun));
            }
            catch (Exception e) { _ = e.WriteLog($"add init '{target}':", Log_); }
        }

        public T2 Load<T1>(DataTarget target, string path, DataTarget[] targets = default, string[] paths = default) where T1 : class
        {
            try
            {
                T2 clz = default;
                T1 d0 = Load_<T1>(path);
                if (d0 == default)
                    throw new DataBuilderException("serialize data is empty");

                if (d0 is IDataCopy idc)
                {
                    clz = idc.Copy<T2>(target, path, Log_);
                    if (clz == default)
                        throw new DataBuilderException("build data is empty");
                }
                if ((clz is IDataBuild idb) && (targets != default) && (paths != default))
                {
                    if (targets.Length != paths.Length)
                        throw new DataBuilderException("arrays length not equals");

                    for (int i = 0; i < paths.Length; i++)
                    {
                        try
                        {
                            T1 d1 = Load_<T1>(paths[i]);
                            if (d1 == default)
                                continue;
                            idb.Merge(targets[i], d1);
                        }
                        catch (Exception e) { _ = e.WriteLog($"merge xml:", Log_); }
                    }
                }
                return clz;
            }
            catch (Exception e) { _ = e.WriteLog($"load xml:", Log_); }
            return default(T2);
        }

        public async Tasks.Task Save<T1>(DataTarget target, string path = default) where T1 : class, new()
        {
            await Tasks.Task.Run(() =>
            {
                try
                {
                    T2 data = Get(target);
                    if (data == default)
                        throw new DataBuilderException($"base '{target}' is empty");

                    if (data is IDataBuild idb)
                    {
                        T1 val = idb.Save<T1>();
                        if (val == default)
                            throw new DataBuilderException("save data is empty");

                        Save_(string.IsNullOrWhiteSpace(path) ? idb.PathBase : path, val);
                    }
                }
                catch (Exception e) { _ = e.WriteLog($"save xml:", Log_); }
            }).ConfigureAwait(false);
        }

        #region Private
        private void LazyInit_()
        {
            try { Base_.Clear(); } catch { }
            if (BaseInit_.Count == 0)
                throw new DataBuilderException("init base is empty");

            foreach (var n in BaseInit_)
                if (!Base_.TryAdd(
                    n.Key,
                    new Lazy<T2>(() => n.Value.Item3.Invoke(n.Value.Item1, n.Value.Item2))
                    ))
                    throw new DataBuilderException($"add '{n.Key}' error");
        }

        private T1 Load_<T1>(string path) where T1 : class
        {
            try
            {
                using FileStream stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                return stream.DeserializeFromStream<T1>();
            }
            catch (Exception e) { _ = e.WriteLog($"load serialize xml:", Log_); }
            return default(T1);
        }

        private void Save_<T1>(string path, T1 clz) where T1 : class
        {
            try
            {
                using FileStream stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
                clz.SerializeToStream<T1>(stream);
            }
            catch (Exception e) { _ = e.WriteLog($"save serialize xml:", Log_); }
        }
        #endregion
    }
}
