﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.BaseXml.Bases
{
    public interface IDataBuild
    {
        string PathBase { get; }
        DataTarget  BaseId { get; }
        ILogManager LogWriter { get; }
        void Build<T1>(DataTarget target, string path, List<T1> list, ILogManager lm = default) where T1 : class;
        T1   Save<T1>() where T1 : class, new();
        void Merge<T1>(DataTarget t, T1 clz) where T1 : class;
    };

    public interface IDataCopy
    {
        T1 Copy<T1>(DataTarget target, string path, ILogManager lm = default) where T1 : class, new();
    };

    public abstract class SerializeDataBuild : IDataBuild
    {
        public DataTarget BaseId { get; private set; } = DataTarget.None;
        public ILogManager LogWriter { get; private set; } = default;
        public string PathBase { get; private set; } = default;

        protected virtual T2 GetDataList<T2>() where T2 : class, new()
        {
            return new T2();
        }
        protected virtual void SetDataList<T2>(T2 list) where T2 : class
        {
        }
        protected virtual void MergeData<T2>(DataTarget t, T2 clz) where T2 : class
        {
        }
        public void Build<T1>(DataTarget target, string path, List<T1> list, ILogManager lm) where T1 : class
        {
            BaseId = target;
            PathBase = path;
            LogWriter = lm;
            SetDataList(list);
        }
        public T1 Save<T1>() where T1 : class, new()
        {
            return GetDataList<T1>();
        }
        public void Merge<T1>(DataTarget t, T1 clz) where T1 : class
        {
            MergeData<T1>(t, clz);
        }
    }

    public abstract class SerializeDataCopy<T2> : IDataCopy where T2 : class
    {
        protected virtual List<T2> GetDataList()
        {
            return new List<T2>();
        }
        public T1 Copy<T1>(DataTarget target, string path, ILogManager lm = default) where T1 : class, new()
        {
            T1 t = new T1();
            if (t is IDataBuild clz)
            {
                clz.Build(target, path, GetDataList(), lm);
                return t;
            }
            throw new DataBuilderException("input class not in interface IDataBuild");
        }
    }
}
