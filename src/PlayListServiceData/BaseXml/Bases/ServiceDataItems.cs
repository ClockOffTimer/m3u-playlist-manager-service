﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.BaseXml.Bases
{
    public class ServiceDataItems : SerializeDataBuild
    {
        public ConcurrentBag<ServiceDataItem> Items { get; private set; } = new ConcurrentBag<ServiceDataItem>();

        #region Overwrite
        protected override void SetDataList<T>(T list)
        {
            try
            {
                if (list is List<SerializeDataChannelItem> lst)
                {
                    foreach (var a in lst)
                        Add(a.Id, a.Element.Value, a.Items.ToArray());
                }
            }
            catch (Exception e) { e.WriteLog($"Set Data List '{typeof(T)}':", LogWriter); }
        }
        protected override T GetDataList<T>()
        {
            try
            {
                T sdc = new T();
                if (Items.Count == 0)
                    return sdc;
                if (sdc is SerializeDataChannels sdcs)
                {
                    foreach (var n in Items)
                        sdcs.Add(n.IfTag, n.IfIcon, n.IfItems.ToArray());
                }
                return sdc;
            }
            catch (Exception e) { e.WriteLog($"Get Data List '{typeof(T)}':", LogWriter); }
            return default(T);
        }
        protected override void MergeData<T>(DataTarget t, T clz)
        {
            try
            {
                if (BaseId != DataTarget.Id_EpgChannels)
                    return;

                if ((t == DataTarget.Id_EpgFavorite) && (clz is SerializeDataChannels cls1))
                {
                    foreach (var n in cls1.Items)
                    {
                        ServiceDataItem item = (from i in Items
                                                where i.IfTag.Equals(n.Id)
                                                select i).FirstOrDefault();
                        if (item == default)
                            continue;
                        item.IfFavorite = true;
                        item.IfIcon = n.Element.Value;
                    }
                }
                else if ((t == DataTarget.Id_EpgAlias) && (clz is SerializeDataChannels cls2))
                {
                    foreach (var n in cls2.Items)
                    {
                        ServiceDataItem item = (from i in Items
                                                where i.IfTag.Equals(n.Id)
                                                select i).FirstOrDefault();
                        if (item == default)
                        {
                            Add(n.Id, n.Element.Value, n.Items.ToArray());
                            continue;
                        }
                        foreach (string s in n.Items)
                        {
                            var x = (from i in item.IfItems
                                     where i.Contains(s)
                                     select i).FirstOrDefault();
                            if (x == default)
                                item.IfItems.Add(s);
                        }
                    }
                }
            }
            catch (Exception e) { e.WriteLog($"Get Data List '{typeof(T)}':", LogWriter); }
        }
        #endregion

        #region public Methods
        public bool IsEnable()
        {
            return Items.Count > 0;
        }
        public void Clear()
        {
            Items = new ConcurrentBag<ServiceDataItem>();
        }
        public void Add(string tag, string icon, string [] items)
        {
            try
            {
                ServiceDataItem sdi = new ServiceDataItem()
                {
                    IfTag = tag,
                    IfIcon = icon
                };
                if ((items != default) && (items.Length > 0))
                {
                    foreach (var s in from s in items
                                      where !sdi.IfItems.Contains(s)
                                      select s)
                        try { sdi.IfItems.Add(s); } catch { }
                }
                Items.Add(sdi);
            }
            catch (Exception e) { e.WriteLog($"Add Data List '{tag}':", LogWriter); }
        }
        #endregion
    }
    public class ServiceDataItem : IServiceDataItem<HashSet<string>>
    {
        public bool   IfFavorite { get; set; } = false;
        public string IfTag { get; set; } = default;
        public string IfIcon { get; set; } = default;
        public HashSet<string> IfItems { get; set; } = new HashSet<string>();
    }
}
