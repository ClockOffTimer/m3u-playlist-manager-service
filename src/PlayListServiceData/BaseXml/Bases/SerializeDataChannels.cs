﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.BaseXml.Bases
{

    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "tv", Namespace = "", IsNullable = false)]
    public class SerializeDataChannels : SerializeDataCopy<SerializeDataChannelItem>
    {
        [XmlElement("channel")]
        public List<SerializeDataChannelItem> Items = new List<SerializeDataChannelItem>();

        #region Overwrite
        protected override List<SerializeDataChannelItem> GetDataList() => Items;
        #endregion

        #region public Methods
        public void Add(string id, string ico, string [] items)
        {
            SerializeDataChannelItem item = (from i in Items
                                             where i.Id.Equals(id)
                                             select i).FirstOrDefault();
            if (item == default)
            {
                item = new SerializeDataChannelItem() { Id = id };
                item.Element.Value = ico;
                if ((items != default) && (items.Length > 0))
                    item.Items.AddRange(items);
                Items.Add(item);
            }
            else
            {
                item.Element.Value = ico;
                item.Items.Clear();
                if ((items != default) && (items.Length > 0))
                    item.Items.AddRange(items);
            }
        }
        #endregion
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SerializeDataChannelItem
    {
        [XmlAttribute("id")]
        public string Id = default;
        [XmlElement("display-name")]
        public List<string> Items = new List<string>();
        [XmlElement("icon")]
        public SerializeDataAElement Element = new SerializeDataAElement();
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SerializeDataAElement
    {
        [XmlAttribute("src")]
        public string Value = default;
    }
}
