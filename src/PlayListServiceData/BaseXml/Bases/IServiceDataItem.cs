﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */


namespace PlayListServiceData.BaseXml.Bases
{
    public interface IServiceDataItem<T1> where T1 : class
    {
        bool   IfFavorite { get; set; }
        string IfTag { get; set; }
        string IfIcon { get; set; }
        T1     IfItems { get; set; }
    }
}