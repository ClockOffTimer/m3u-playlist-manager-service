﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.BaseXml
{
    public static class DataBuilderExtension
    {
        private static bool Check_(ServiceDataItems sdi, string name)
        {
            return !((sdi == default) || (sdi.Items.Count == 0) || string.IsNullOrWhiteSpace(name));
        }

        public static string GetContainsIdFromName(this ServiceDataItems sdi, string name, ILogManager lm = default, bool b = false)
        {
            if (!Check_(sdi, name))
                return default;
            try
            {
                name = name.ToLowerInvariant();
                return (from i in sdi.Items
                        from n in i.IfItems
                        where name.Contains(n)
                        select b ? i.IfTag.ToLowerInvariant() : i.IfTag.ToUpperInvariant()).FirstOrDefault();
            }
            catch (Exception e) { _ = e.WriteLog("Get Contains Id From Name:", lm); }
            return default;
        }
        public static string GetStartsWithIdFromName(this ServiceDataItems sdi, string name, ILogManager lm = default, bool b = false)
        {
            if (!Check_(sdi, name))
                return default;
            try
            {
                string [] ss = name.Split(' ');
                if ((ss.Length > 0) && (ss[0].Length > 0))
                    name = ss[0];

                return (from i in sdi.Items
                        from n in i.IfItems
                        where name.StartsWith(n, StringComparison.InvariantCultureIgnoreCase)
                        select b ? i.IfTag.ToLowerInvariant() : i.IfTag.ToUpperInvariant()).FirstOrDefault();
            }
            catch (Exception e) { _ = e.WriteLog("Get Starts with Id From Name:", lm); }
            return default;
        }
    }
}
