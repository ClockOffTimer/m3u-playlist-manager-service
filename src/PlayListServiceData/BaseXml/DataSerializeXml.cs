﻿/* Copyright (c) 2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace PlayListServiceData.BaseXml
{
    public static class DataSerializeXml
    {
        public static void SerializeToStream<T1>(this T1 src, Stream stream, Encoding enc = default)
        {
            if (src == null) return;
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamWriter sw = new(stream, enc);
            XmlSerializer xml = new(typeof(T1));
            xml.Serialize(sw, src);
        }
        public static string SerializeToString<T1>(this T1 src, Encoding enc = default)
        {
            if (src == null) return string.Empty;
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using MemoryStream ms = new();
            using StreamWriter sw = new(ms, enc);
            XmlSerializer xml = new(typeof(T1));
            xml.Serialize(sw, src);
            return enc.GetString(ms.ToArray());
        }
        public static void SerializeToFile<T1>(this T1 src, string path, Encoding enc = default)
        {
            if (src == null) return;
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamWriter sw = new(path, false, enc);
            XmlSerializer xml = new(typeof(T1));
            xml.Serialize(sw, src);
        }

        public static T1 DeserializeFromStream<T1>(this Stream ms, Encoding enc = default)
        {
            if (ms == null) return default(T1);
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamReader sr = new(ms, enc, false);
            XmlSerializer xml = new(typeof(T1));
            if (xml.Deserialize(sr) is T1 val)
                return val;
            return default(T1);
        }
        public static T1 DeserializeFromFile<T1>(this string path, Encoding enc = default)
        {
            if (string.IsNullOrWhiteSpace(path)) return default(T1);
            enc = (enc == default) ? new UTF8Encoding(false) : enc;
            using StreamReader sr = new(path, enc, true);
            XmlSerializer xml = new(typeof(T1));
            if (xml.Deserialize(sr) is T1 val)
                return val;
            return default(T1);
        }

    }
}
