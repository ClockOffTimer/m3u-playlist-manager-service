﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.BaseXml
{
    public class DataBuilderException : Exception
    {
        public DataBuilderException(string message) : base(message) { }
        public DataBuilderException(string message, Exception e) : base(message, e) { }
    }
}
