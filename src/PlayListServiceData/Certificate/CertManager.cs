﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using PlayListServiceData.Process.Run;
using PlayListServiceData.Base;
using System.Collections.Generic;
using PlayListServiceData.Utils;

namespace PlayListServiceData.Certificate
{
    public class CertManager : BaseEvent<string, Exception>
    {
        public int    Port { get; private set; }
        public string Store { get; private set; }
        public string Thumbprint { get; private set; }
        public string Domain { get; private set; }
        public StoreLocation Location { get; set; } = StoreLocation.LocalMachine;
        private CancellationTokenSafe token;

        public CertManager(string s, string t, string d, int p)
        {
            Store = s;
            Thumbprint = t;
            Port = p;
            {
                int x = Store.LastIndexOf('.');
                if (x >= 0)
                    StorePart = Store.Substring(x);
                else
                    StorePart = Store;
            }
            if (string.IsNullOrWhiteSpace(d))
                Domain = "0.0.0.0";
            else
                Domain = d;

            token = new ();
        }
        ~CertManager()
        {
            CancellationTokenSafe cts = token;
            token = default;
            if (cts != default)
                cts.Dispose();
        }

        private readonly string StorePart;
        private const string AppId = "69a37cb5-3cc9-4574-98c6-ef574ce618bf";
        private string GetNetshInstallCmd() => $"http add sslcert ipport={Domain}:{Port} certhash={Thumbprint} appid={{{AppId}}}";
        private string GetNetshInstallFromStoreCmd() => $"{GetNetshInstallCmd()} certstorename={StorePart}";
        private string GetNetshUnInstallCmd(string ip, int port) => $"http delete sslcert ipport={ip}:{port}";
        private string GetNetshCertificate(string ip, int port) => $"http show sslcert ipport={ip}:{port}";

        public void Stop()
        {
            try
            {
                if (!token.IsCancellationRequested)
                    token.Cancel();
            } catch (Exception e) { CallEvent(default, e); }
        }

        public X509Certificate2 Get()
        {
            return Get(Store);
        }

        public X509Certificate2 Get(string name)
        {
            if (!Enum.TryParse(name, out StoreName id))
                id = StoreName.My;
            return Get(id, false);
        }

        public X509Certificate2 Get(StoreName storename, bool isevent)
        {
            try
            {
                using (X509Store store = new(storename, Location))
                {
                    if (store == default)
                    {
                        if (isevent)
                            CallEvent(nameof(Get), new Exception($"Store {store.Location}.{store.Name} is null"));
                        return default;
                    }
                    store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly | OpenFlags.MaxAllowed);
                    if (store.Certificates.Count == 0)
                    {
                        if (isevent)
                            CallEvent(nameof(Get), new Exception($"Store {store.Location}.{store.Name} is empty"));
                        return default;
                    }
                    foreach (var crt in store.Certificates)
                    {
                        if (crt.Thumbprint.Equals(Thumbprint, StringComparison.InvariantCultureIgnoreCase))
                            return crt;
                        try { crt.Dispose(); } catch { }
                    }
                }
            } catch (Exception e) { CallEvent(nameof(Get), e); }
            if (isevent)
                CallEvent(nameof(Get), new Exception($"Certificate {Thumbprint} not found in Store {storename}.{Location}"));
            return default;
        }

        public Tuple<StoreName, X509Certificate2> Find()
        {
            try
            {
                foreach (StoreName n in Enum.GetValues(typeof(StoreName)))
                {
                    X509Certificate2 crt = Get(n, true);
                    if (crt != default)
                        return new Tuple<StoreName, X509Certificate2>(n, crt);
                }
            } catch (Exception e) { CallEvent(nameof(Find), e); }
            return default;
        }

        public async Task NetshAdd()
        {
            await NetshExec(GetNetshInstallCmd());
        }
        public async Task NetshRemove(string domain, int port)
        {
            await NetshExec(GetNetshUnInstallCmd(domain, port));
        }
        private async Task NetshExec(string args)
        {
            try
            {
                using (ProcessResults results = await ProcessEx.RunAsync("netsh.exe", args, token.Token))
                {
                    #region DEBUG print exec result
#if DEBUG
                    if (results.ExitCode != 0)
                        System.Diagnostics.Debug.WriteLine($"netsh return code: {results.ExitCode}");
#endif
                    if (results.StandardError.Length > 0)
                        foreach (var s in results.StandardError)
#if DEBUG
                            System.Diagnostics.Debug.WriteLine(s);
#else
                            CallEvent(nameof(NetshExec), new Exception(s));
#endif
                    #endregion
                }
            } catch (Exception e) { CallEvent(nameof(NetshExec), e); }
        }

        public async Task<string> NetshCertificateShow()
        {
            try
            {
                using (ProcessResults results = await ProcessEx.RunAsync("netsh.exe", GetNetshCertificate(Domain, Port), token.Token))
                {
                    #region DEBUG print exec result
#if DEBUG
                    if (results.ExitCode != 0)
                        System.Diagnostics.Debug.WriteLine($"netsh return code: {results.ExitCode}");
#endif
                    #endregion
                    List<string> list = new();
                    if (results.StandardError != default)
                        list.AddRange(results.StandardError);
                    if (results.StandardOutput != default)
                        list.AddRange(results.StandardOutput);

                    return string.Join(Environment.NewLine, list.ToArray());
                }
            }
            catch (Exception e) { return e.ToString(); }
        }
    }
}
