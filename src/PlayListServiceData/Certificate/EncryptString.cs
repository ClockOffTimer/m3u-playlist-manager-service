﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace PlayListServiceData.Certificate
{
    public static partial class EncryptString
    {
        public static string Encrypt(string s)
        {
            byte[] src = Encoding.Unicode.GetBytes(s);
            byte[] dst = Crypt_(src, true);
            if (dst == default)
                return string.Empty;
            return Convert.ToBase64String(dst);
        }

        public static string Decrypt(string s)
        {
            byte[] src = Convert.FromBase64String(s.Replace(" ", "+"));
            byte[] dst = Crypt_(src, false);
            if (dst == default)
                return string.Empty;
            return Encoding.Unicode.GetString(dst);
        }

        private static byte[] Crypt_(byte[] src, bool iscrypt = false)
        {
            byte[] dst = default;
            using (Aes aes = Aes.Create())
            {
                using Rfc2898DeriveBytes pdb = new(
                    StaticKey__, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                aes.Key = pdb.GetBytes(32);
                aes.IV = pdb.GetBytes(16);
                using MemoryStream ms = new();
                using (CryptoStream cs = new(
                    ms, iscrypt ? aes.CreateEncryptor() : aes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(src, 0, src.Length);
                    cs.Close();
                }
                dst = ms.ToArray();
            }
            return dst;
        }
    }
}
