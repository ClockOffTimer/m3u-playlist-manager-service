﻿namespace PlayListServiceSettings.Data
{
    public interface IServiceLanguage
    {
        string DialogMessage1 { get; }
        string DialogMessage2 { get; }
    }
}