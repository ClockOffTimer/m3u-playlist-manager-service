﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;
using PlayListServiceData.Base;

namespace PlayListServiceData.Settings
{
    public partial interface IServiceSettings
    {
        bool IsLoad { get; }
        int ConfigNetOldPort { get; }
        string ConfigNetOldSslDomain { get; }
        string ExeConfigName { get; }

        event PropertyChangedEventHandler PropertyChanged;
        event BaseEvent<IServiceSettings, bool>.DelegateEvent EventCb;

        void Load();
        void ReLoad();
        void Save();
        void WaitLoad();
        bool IsSSlChange();
    }
}
