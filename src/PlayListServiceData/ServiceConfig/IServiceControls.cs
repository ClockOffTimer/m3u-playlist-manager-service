﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Windows;
using PlayListServiceData.Settings;
using PlayListServiceSettings.Data;

namespace PlayListServiceData.ServiceConfig
{
    public interface IServiceControls
    {
        event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        event DelegateOnCloseEvent OnCloseEventCb;
        delegate void DelegateOnCloseEvent();

        void CheckServiceStatus();
        void ReadConfig();
        void WriteConfig(Action act = null);
        void ServiceStateChange(ServiceState state);

        Window ControlMainWindow { get; set; }
        IServiceTags ControlServiceTags { get; set; }
        IServiceSettings ControlConfig { get; set; }
        IServiceCommand ControlCommand { get; }
        IServiceLanguage ControlLanguage { get; set; }
        ServiceState ControlServiceState { get; set; }
        string ControlServiceStateText { get; }
        bool IsControlConfig { get; }
        bool IsControlServiceRun { get; }
        bool IsControlServiceStop { get; set; }
        bool IsControlServiceInstall { get; set; }
        bool IsControlAdminRun { get; set; }
        bool IsControlServicePath { get; }

        int IsPlatformRun { get; }
        string IconSaveExtension { get; }
        string FFMpegPath { get; }
        List<string> EpgHostGuiList { get; }
    }
}
