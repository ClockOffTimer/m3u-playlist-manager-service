
/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   AUTO-GENERATED! NOT MANUAL EDIT!
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.ComponentModel;
using PlayListServiceData.Base;

namespace PlayListServiceData.Settings
{
    public partial class ServiceSettings : BaseEvent<IServiceSettings, bool>, IServiceSettings, INotifyPropertyChanged
    {
        public bool FirstConfigure
        {
            get { return getBool(nameof(FirstConfigure)); }
            set { setBool(nameof(FirstConfigure), value); OnPropertyChanged(nameof(FirstConfigure)); }
        }
        public bool IsConfigAddTagsToLocalFiles
        {
            get { return getBool(nameof(IsConfigAddTagsToLocalFiles)); }
            set { setBool(nameof(IsConfigAddTagsToLocalFiles), value); OnPropertyChanged(nameof(IsConfigAddTagsToLocalFiles)); }
        }
        public bool IsConfigAudioCache
        {
            get { return getBool(nameof(IsConfigAudioCache)); }
            set { setBool(nameof(IsConfigAudioCache), value); OnPropertyChanged(nameof(IsConfigAudioCache)); }
        }
        public bool IsConfigCheckSkipDash
        {
            get { return getBool(nameof(IsConfigCheckSkipDash)); }
            set { setBool(nameof(IsConfigCheckSkipDash), value); OnPropertyChanged(nameof(IsConfigCheckSkipDash)); }
        }
        public bool IsConfigCheckSkipMime
        {
            get { return getBool(nameof(IsConfigCheckSkipMime)); }
            set { setBool(nameof(IsConfigCheckSkipMime), value); OnPropertyChanged(nameof(IsConfigCheckSkipMime)); }
        }
        public bool IsConfigCheckSkipProto
        {
            get { return getBool(nameof(IsConfigCheckSkipProto)); }
            set { setBool(nameof(IsConfigCheckSkipProto), value); OnPropertyChanged(nameof(IsConfigCheckSkipProto)); }
        }
        public bool IsConfigCheckSkipUdpProxy
        {
            get { return getBool(nameof(IsConfigCheckSkipUdpProxy)); }
            set { setBool(nameof(IsConfigCheckSkipUdpProxy), value); OnPropertyChanged(nameof(IsConfigCheckSkipUdpProxy)); }
        }
        public bool IsConfigDlnaEnable
        {
            get { return getBool(nameof(IsConfigDlnaEnable)); }
            set { setBool(nameof(IsConfigDlnaEnable), value); OnPropertyChanged(nameof(IsConfigDlnaEnable)); }
        }
        public bool IsConfigEpg
        {
            get { return getBool(nameof(IsConfigEpg)); }
            set { setBool(nameof(IsConfigEpg), value); OnPropertyChanged(nameof(IsConfigEpg)); }
        }
        public bool IsConfigJsHost
        {
            get { return getBool(nameof(IsConfigJsHost)); }
            set { setBool(nameof(IsConfigJsHost), value); OnPropertyChanged(nameof(IsConfigJsHost)); }
        }
        public bool IsConfigJsLib
        {
            get { return getBool(nameof(IsConfigJsLib)); }
            set { setBool(nameof(IsConfigJsLib), value); OnPropertyChanged(nameof(IsConfigJsLib)); }
        }
        public bool IsConfigJsXMLHttpRequest
        {
            get { return getBool(nameof(IsConfigJsXMLHttpRequest)); }
            set { setBool(nameof(IsConfigJsXMLHttpRequest), value); OnPropertyChanged(nameof(IsConfigJsXMLHttpRequest)); }
        }
        public bool IsConfigLogEnable
        {
            get { return getBool(nameof(IsConfigLogEnable)); }
            set { setBool(nameof(IsConfigLogEnable), value); OnPropertyChanged(nameof(IsConfigLogEnable)); }
        }
        public bool IsConfigLogHttpEnable
        {
            get { return getBool(nameof(IsConfigLogHttpEnable)); }
            set { setBool(nameof(IsConfigLogHttpEnable), value); OnPropertyChanged(nameof(IsConfigLogHttpEnable)); }
        }
        public bool IsConfigM3UCache
        {
            get { return getBool(nameof(IsConfigM3UCache)); }
            set { setBool(nameof(IsConfigM3UCache), value); OnPropertyChanged(nameof(IsConfigM3UCache)); }
        }
        public bool IsConfigNetHttpProxy
        {
            get { return getBool(nameof(IsConfigNetHttpProxy)); }
            set { setBool(nameof(IsConfigNetHttpProxy), value); OnPropertyChanged(nameof(IsConfigNetHttpProxy)); }
        }
        public bool IsConfigNetHttpSupport
        {
            get { return getBool(nameof(IsConfigNetHttpSupport)); }
            set { setBool(nameof(IsConfigNetHttpSupport), value); OnPropertyChanged(nameof(IsConfigNetHttpSupport)); }
        }
        public bool IsConfigNetSocks5Proxy
        {
            get { return getBool(nameof(IsConfigNetSocks5Proxy)); }
            set { setBool(nameof(IsConfigNetSocks5Proxy), value); OnPropertyChanged(nameof(IsConfigNetSocks5Proxy)); }
        }
        public bool IsConfigNetSslEnable
        {
            get { return getBool(nameof(IsConfigNetSslEnable)); }
            set { setBool(nameof(IsConfigNetSslEnable), value); OnPropertyChanged(nameof(IsConfigNetSslEnable)); }
        }
        public bool IsConfigRenameLocalFiles
        {
            get { return getBool(nameof(IsConfigRenameLocalFiles)); }
            set { setBool(nameof(IsConfigRenameLocalFiles), value); OnPropertyChanged(nameof(IsConfigRenameLocalFiles)); }
        }
        public bool IsConfigReport
        {
            get { return getBool(nameof(IsConfigReport)); }
            set { setBool(nameof(IsConfigReport), value); OnPropertyChanged(nameof(IsConfigReport)); }
        }
        public bool IsConfigSeparateUdpSource
        {
            get { return getBool(nameof(IsConfigSeparateUdpSource)); }
            set { setBool(nameof(IsConfigSeparateUdpSource), value); OnPropertyChanged(nameof(IsConfigSeparateUdpSource)); }
        }
        public bool IsConfigTimeOutRetry
        {
            get { return getBool(nameof(IsConfigTimeOutRetry)); }
            set { setBool(nameof(IsConfigTimeOutRetry), value); OnPropertyChanged(nameof(IsConfigTimeOutRetry)); }
        }
        public bool IsConfigTorrentEnable
        {
            get { return getBool(nameof(IsConfigTorrentEnable)); }
            set { setBool(nameof(IsConfigTorrentEnable), value); OnPropertyChanged(nameof(IsConfigTorrentEnable)); }
        }
        public bool IsConfigVideoCache
        {
            get { return getBool(nameof(IsConfigVideoCache)); }
            set { setBool(nameof(IsConfigVideoCache), value); OnPropertyChanged(nameof(IsConfigVideoCache)); }
        }
        public List<string> ConfigAudioDirExclude
        {
            get { return getCollection(nameof(ConfigAudioDirExclude)); }
            set { setCollection(nameof(ConfigAudioDirExclude), value); OnPropertyChanged(nameof(ConfigAudioDirExclude)); }
        }
        public List<string> ConfigM3uDirExclude
        {
            get { return getCollection(nameof(ConfigM3uDirExclude)); }
            set { setCollection(nameof(ConfigM3uDirExclude), value); OnPropertyChanged(nameof(ConfigM3uDirExclude)); }
        }
        public List<string> ConfigNetAccess
        {
            get { return getCollection(nameof(ConfigNetAccess)); }
            set { setCollection(nameof(ConfigNetAccess), value); OnPropertyChanged(nameof(ConfigNetAccess)); }
        }
        public List<string> ConfigTorrentTrackers
        {
            get { return getCollection(nameof(ConfigTorrentTrackers)); }
            set { setCollection(nameof(ConfigTorrentTrackers), value); OnPropertyChanged(nameof(ConfigTorrentTrackers)); }
        }
        public List<string> ConfigUserAuth
        {
            get { return getCollection(nameof(ConfigUserAuth)); }
            set { setCollection(nameof(ConfigUserAuth), value); OnPropertyChanged(nameof(ConfigUserAuth)); }
        }
        public List<string> ConfigVideoDirExclude
        {
            get { return getCollection(nameof(ConfigVideoDirExclude)); }
            set { setCollection(nameof(ConfigVideoDirExclude), value); OnPropertyChanged(nameof(ConfigVideoDirExclude)); }
        }
        public int ConfigAudioReadTimeout
        {
            get { return getInt(nameof(ConfigAudioReadTimeout)); }
            set { setInt(nameof(ConfigAudioReadTimeout), value); OnPropertyChanged(nameof(ConfigAudioReadTimeout)); }
        }
        public int ConfigCpuPriority
        {
            get { return getInt(nameof(ConfigCpuPriority)); }
            set { setInt(nameof(ConfigCpuPriority), value); OnPropertyChanged(nameof(ConfigCpuPriority)); }
        }
        public int ConfigLogLevel
        {
            get { return getInt(nameof(ConfigLogLevel)); }
            set { setInt(nameof(ConfigLogLevel), value); OnPropertyChanged(nameof(ConfigLogLevel)); }
        }
        public int ConfigNetPort
        {
            get { return getInt(nameof(ConfigNetPort)); }
            set { setInt(nameof(ConfigNetPort), value); OnPropertyChanged(nameof(ConfigNetPort)); }
        }
        public int ConfigRunCheckPeriodDays
        {
            get { return getInt(nameof(ConfigRunCheckPeriodDays)); }
            set { setInt(nameof(ConfigRunCheckPeriodDays), value); OnPropertyChanged(nameof(ConfigRunCheckPeriodDays)); }
        }
        public int ConfigRunDownloadPeriodDays
        {
            get { return getInt(nameof(ConfigRunDownloadPeriodDays)); }
            set { setInt(nameof(ConfigRunDownloadPeriodDays), value); OnPropertyChanged(nameof(ConfigRunDownloadPeriodDays)); }
        }
        public int ConfigTimeOutRequest
        {
            get { return getInt(nameof(ConfigTimeOutRequest)); }
            set { setInt(nameof(ConfigTimeOutRequest), value); OnPropertyChanged(nameof(ConfigTimeOutRequest)); }
        }
        public string ConfigAudioDirFormat
        {
            get { return getString(nameof(ConfigAudioDirFormat)); }
            set { setString(nameof(ConfigAudioDirFormat), value); OnPropertyChanged(nameof(ConfigAudioDirFormat)); }
        }
        public string ConfigAudioFileFormat
        {
            get { return getString(nameof(ConfigAudioFileFormat)); }
            set { setString(nameof(ConfigAudioFileFormat), value); OnPropertyChanged(nameof(ConfigAudioFileFormat)); }
        }
        public string ConfigAudioPath
        {
            get { return getString(nameof(ConfigAudioPath)); }
            set { setString(nameof(ConfigAudioPath), value); OnPropertyChanged(nameof(ConfigAudioPath)); }
        }
        public string ConfigAudioPrefix
        {
            get { return getString(nameof(ConfigAudioPrefix)); }
            set { setString(nameof(ConfigAudioPrefix), value); OnPropertyChanged(nameof(ConfigAudioPrefix)); }
        }
        public string ConfigAudioTranslateM3u
        {
            get { return getString(nameof(ConfigAudioTranslateM3u)); }
            set { setString(nameof(ConfigAudioTranslateM3u), value); OnPropertyChanged(nameof(ConfigAudioTranslateM3u)); }
        }
        public string ConfigConfiguratorExe
        {
            get { return getString(nameof(ConfigConfiguratorExe)); }
            set { setString(nameof(ConfigConfiguratorExe), value); OnPropertyChanged(nameof(ConfigConfiguratorExe)); }
        }
        public string ConfigEpgUrl
        {
            get { return getString(nameof(ConfigEpgUrl)); }
            set { setString(nameof(ConfigEpgUrl), value); OnPropertyChanged(nameof(ConfigEpgUrl)); }
        }
        public string ConfigLanguage
        {
            get { return getString(nameof(ConfigLanguage)); }
            set { setString(nameof(ConfigLanguage), value); OnPropertyChanged(nameof(ConfigLanguage)); }
        }
        public string ConfigLogName
        {
            get { return getString(nameof(ConfigLogName)); }
            set { setString(nameof(ConfigLogName), value); OnPropertyChanged(nameof(ConfigLogName)); }
        }
        public string ConfigLogSource
        {
            get { return getString(nameof(ConfigLogSource)); }
            set { setString(nameof(ConfigLogSource), value); OnPropertyChanged(nameof(ConfigLogSource)); }
        }
        public string ConfigM3uFavoriteName
        {
            get { return getString(nameof(ConfigM3uFavoriteName)); }
            set { setString(nameof(ConfigM3uFavoriteName), value); OnPropertyChanged(nameof(ConfigM3uFavoriteName)); }
        }
        public string ConfigM3uPath
        {
            get { return getString(nameof(ConfigM3uPath)); }
            set { setString(nameof(ConfigM3uPath), value); OnPropertyChanged(nameof(ConfigM3uPath)); }
        }
        public string ConfigNetAddr
        {
            get { return getString(nameof(ConfigNetAddr)); }
            set { setString(nameof(ConfigNetAddr), value); OnPropertyChanged(nameof(ConfigNetAddr)); }
        }
        public string ConfigNetSslCertStore
        {
            get { return getString(nameof(ConfigNetSslCertStore)); }
            set { setString(nameof(ConfigNetSslCertStore), value); OnPropertyChanged(nameof(ConfigNetSslCertStore)); }
        }
        public string ConfigNetSslCertThumbprint
        {
            get { return getString(nameof(ConfigNetSslCertThumbprint)); }
            set { setString(nameof(ConfigNetSslCertThumbprint), value); OnPropertyChanged(nameof(ConfigNetSslCertThumbprint)); }
        }
        public string ConfigNetSslDomain
        {
            get { return getString(nameof(ConfigNetSslDomain)); }
            set { setString(nameof(ConfigNetSslDomain), value); OnPropertyChanged(nameof(ConfigNetSslDomain)); }
        }
        public string ConfigTorrentsPath
        {
            get { return getString(nameof(ConfigTorrentsPath)); }
            set { setString(nameof(ConfigTorrentsPath), value); OnPropertyChanged(nameof(ConfigTorrentsPath)); }
        }
        public string ConfigUdpPrefix
        {
            get { return getString(nameof(ConfigUdpPrefix)); }
            set { setString(nameof(ConfigUdpPrefix), value); OnPropertyChanged(nameof(ConfigUdpPrefix)); }
        }
        public string ConfigVideoPath
        {
            get { return getString(nameof(ConfigVideoPath)); }
            set { setString(nameof(ConfigVideoPath), value); OnPropertyChanged(nameof(ConfigVideoPath)); }
        }
        public string ConfigVideoPrefix
        {
            get { return getString(nameof(ConfigVideoPrefix)); }
            set { setString(nameof(ConfigVideoPrefix), value); OnPropertyChanged(nameof(ConfigVideoPrefix)); }
        }
    }
}

