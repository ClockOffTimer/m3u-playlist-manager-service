﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayListServiceData.Base;

namespace PlayListServiceData.Settings
{
    public class ServiceSettingsSSL
    {
        public int ConfigNetPort { get; private set; }
        public bool IsConfigNetSslEnable { get; private set; }
        public string ConfigNetSslDomain { get; private set; }
        public string ConfigNetSslCertStore { get; private set; }
        public string ConfigNetSslCertThumbprint { get; private set; }

        public ServiceSettingsSSL(string d, string s, string t, bool b, int p)
        {
            ConfigNetSslDomain = d;
            ConfigNetSslCertStore = s;
            ConfigNetSslCertThumbprint = t;
            ConfigNetPort = p;
            IsConfigNetSslEnable = b;
        }
        public bool Equals(string d, string s, string t, bool b, int p)
        {
            return ConfigNetSslDomain.Equals(d) &&
                   ConfigNetSslCertStore.Equals(s) &&
                   ConfigNetSslCertThumbprint.Equals(t) &&
                   (ConfigNetPort == p) &&
                   (IsConfigNetSslEnable == b);
        }
    }

    public partial class ServiceSettings : BaseEvent<IServiceSettings, bool>, IServiceSettings, INotifyPropertyChanged
    {
        private Configuration configuration = default;
        private ClientSettingsSection clientSection = default;
        private ServiceSettingsSSL SettingsSSL = default;
        public string ExeConfigName { get; private set; } = default;
        public bool IsLoad { get; private set; } = default;

        #region init Settings
        private async void initSettings()
        {
            IsLoad = false;
            configuration = null;
            clientSection = null;

            await Task.Run(() =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(ExeConfigName))
                        return;

                    ExeConfigurationFileMap map = new()
                    {
                        ExeConfigFilename =
                            $"{Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}{Path.DirectorySeparatorChar}{ExeConfigName}.exe.config"
                    };
                    configuration = ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
                    if (configuration == null)
                        return;

                    ConfigurationSectionGroup appSettings = configuration.SectionGroups["applicationSettings"];
                    if (appSettings == null)
                        return;

                    clientSection = appSettings.Sections[$"{ExeConfigName}.Properties.Settings"] as ClientSettingsSection;
                    if (clientSection == null)
                        return;

                    IsLoad = true;
                    OnPropertyChanged("ServiceSettingsLoad");

                    foreach (SettingElement ele in clientSection.Settings)
                        OnPropertyChanged(ele.Name);

                    SettingsSSL = new ServiceSettingsSSL(
                        ConfigNetSslDomain, ConfigNetSslCertStore, ConfigNetSslCertThumbprint, IsConfigNetSslEnable, ConfigNetPort);

                }
                catch { }
            });
        }
        #endregion

        public ServiceSettings(string name)
        {
            ExeConfigName = name;
            initSettings();
        }

        public void ReLoad() => initSettings();
        public void Load() => initSettings();
        public void Save()
        {
            if (configuration == null)
                return;
            configuration.Save();
        }
        public void WaitLoad()
        {
            int cnt = 256000;
            while (!IsLoad)
            {
                Task.Delay(500);
                Task.Yield();
                if (cnt-- <= 0)
                    break;
            }
        }
        public bool IsSSlChange()
        {
            try
            {
                return !SettingsSSL.Equals(
                    ConfigNetSslDomain, ConfigNetSslCertStore, ConfigNetSslCertThumbprint, IsConfigNetSslEnable, ConfigNetPort);
            }
            catch { return false; }
        }
        public int ConfigNetOldPort
        {
            get { return SettingsSSL.ConfigNetPort; }
        }
        public string ConfigNetOldSslDomain
        {
            get { return SettingsSSL.ConfigNetSslDomain; }
        }

        #region notify
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region Get
        private string getString(string name)
        {
            do
            {
                if (clientSection == null)
                    break;
                SettingElement sec = clientSection.Settings.Get(name);
                if ((sec == null) || (sec.Value == null) || (sec.Value.ValueXml == null))
                    break;
                return sec.Value.ValueXml.InnerText;
            }
            while (false);
            return default;
        }
        private bool getBool(string name)
        {
            do
            {
                string s = getString(name);
                if (string.IsNullOrWhiteSpace(s))
                    break;

                if (!bool.TryParse(s, out bool b))
                    break;
                return b;
            }
            while (false);
            return default;
        }
        private int getInt(string name)
        {
            do
            {
                string s = getString(name);
                if (string.IsNullOrWhiteSpace(s))
                    break;

                if (!int.TryParse(s, out int i))
                    break;
                return i;
            }
            while (false);
            return default;
        }
        private List<string> getCollection(string name)
        {
            try
            {
                string s = getString(name);
                if (!string.IsNullOrWhiteSpace(s))
                {
                    List<string> list = new();
                    string[] str = s.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < str.Length; i++)
                    {
                        var sout = str[i].Trim();
                        if (string.IsNullOrWhiteSpace(sout))
                            continue;
                        list.Add(sout);
                    }
                    return list;
                }
            }
            catch { }
            return default;
        }

        #endregion

        #region Set
        private void setString(string name, string val, bool isxml = false)
        {
            if (val == default)
            {
                val = string.Empty;
                isxml = false;
            }
            if (clientSection == null)
                return;
            try
            {
                SettingElement sec = clientSection.Settings.Get(name);
                if (sec == null)
                    sec = new SettingElement(name, SettingsSerializeAs.String);
                else
                    clientSection.Settings.Remove(sec);

                if (sec.Value == default)
                    sec.Value = new SettingValueElement();
                if (sec.Value.ValueXml == default)
                {
                    if (string.IsNullOrWhiteSpace(val))
                        return;
                    System.Xml.XmlDocument doc = new();
                    sec.Value.ValueXml = doc.CreateElement("value");
                }
                if (isxml)
                    sec.Value.ValueXml.InnerXml = val;
                else
                    sec.Value.ValueXml.InnerText = val;
                clientSection.Settings.Add(sec);
                CallEvent(this, true);
            } catch { }
        }
        private void setBool(string name, bool val)
        {
            setString(name, val.ToString());
        }
        private void setInt(string name, int val)
        {
            setString(name, val.ToString());
        }
        private void setCollection(string name, List<string> val)
        {
            try
            {
                if ((val == default) || (val.Count == 0))
                {
                    setString(name, string.Empty);
                    return;
                }
                StringBuilder sb = new(
                    $@"<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">{Environment.NewLine}"
                    );
                sb.Append(string.Join($"{Environment.NewLine}", val.Select(s => $"\t<string>{s}</string>").ToArray()));
                sb.AppendLine("</ArrayOfString>");
                setString(name, sb.ToString(), true);
            } catch { }
        }
        #endregion

        #region Interface Setter/Getter
        /* from partial class ServiceSettingsXetter.cs */
        #endregion
    }
}
