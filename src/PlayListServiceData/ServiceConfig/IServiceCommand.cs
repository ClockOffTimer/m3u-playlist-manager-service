﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.ServiceConfig
{
    public interface IServiceCommand
    {
        ServiceState GetServiceStatus();
        ServiceState SetUserCommand(int cmd);
        void SetServiceName(string s);
        void StartService();
        void StopService();
        void UninstallService();
        bool IsServiceInstalled();
        void InstallAndStartService(string displayName, string displayDesc, string fileName, bool isDelay = false);
        void InstallService(string displayName, string displayDesc, string fileName, bool isDelay = false);

    }
}
