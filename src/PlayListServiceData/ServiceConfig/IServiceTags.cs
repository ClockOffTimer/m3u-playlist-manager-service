﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.ServiceConfig
{
    public interface IServiceTags
    {
        string Name { get; }
        string Display { get; }
        string Description { get; }
        string SysLogName { get; }
    }
}
