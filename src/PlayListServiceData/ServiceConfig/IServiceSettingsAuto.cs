
/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   AUTO-GENERATED! NOT MANUAL EDIT!
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;

namespace PlayListServiceData.Settings
{
    public partial interface IServiceSettings
    {
        bool FirstConfigure { get; set; }
        bool IsConfigAddTagsToLocalFiles { get; set; }
        bool IsConfigAudioCache { get; set; }
        bool IsConfigCheckSkipDash { get; set; }
        bool IsConfigCheckSkipMime { get; set; }
        bool IsConfigCheckSkipProto { get; set; }
        bool IsConfigCheckSkipUdpProxy { get; set; }
        bool IsConfigDlnaEnable { get; set; }
        bool IsConfigEpg { get; set; }
        bool IsConfigJsHost { get; set; }
        bool IsConfigJsLib { get; set; }
        bool IsConfigJsXMLHttpRequest { get; set; }
        bool IsConfigLogEnable { get; set; }
        bool IsConfigLogHttpEnable { get; set; }
        bool IsConfigM3UCache { get; set; }
        bool IsConfigNetHttpProxy { get; set; }
        bool IsConfigNetHttpSupport { get; set; }
        bool IsConfigNetSocks5Proxy { get; set; }
        bool IsConfigNetSslEnable { get; set; }
        bool IsConfigRenameLocalFiles { get; set; }
        bool IsConfigReport { get; set; }
        bool IsConfigSeparateUdpSource { get; set; }
        bool IsConfigTimeOutRetry { get; set; }
        bool IsConfigTorrentEnable { get; set; }
        bool IsConfigVideoCache { get; set; }
        List<string> ConfigAudioDirExclude { get; set; }
        List<string> ConfigM3uDirExclude { get; set; }
        List<string> ConfigNetAccess { get; set; }
        List<string> ConfigTorrentTrackers { get; set; }
        List<string> ConfigUserAuth { get; set; }
        List<string> ConfigVideoDirExclude { get; set; }
        int ConfigAudioReadTimeout { get; set; }
        int ConfigCpuPriority { get; set; }
        int ConfigLogLevel { get; set; }
        int ConfigNetPort { get; set; }
        int ConfigRunCheckPeriodDays { get; set; }
        int ConfigRunDownloadPeriodDays { get; set; }
        int ConfigTimeOutRequest { get; set; }
        string ConfigAudioDirFormat { get; set; }
        string ConfigAudioFileFormat { get; set; }
        string ConfigAudioPath { get; set; }
        string ConfigAudioPrefix { get; set; }
        string ConfigAudioTranslateM3u { get; set; }
        string ConfigConfiguratorExe { get; set; }
        string ConfigEpgUrl { get; set; }
        string ConfigLanguage { get; set; }
        string ConfigLogName { get; set; }
        string ConfigLogSource { get; set; }
        string ConfigM3uFavoriteName { get; set; }
        string ConfigM3uPath { get; set; }
        string ConfigNetAddr { get; set; }
        string ConfigNetSslCertStore { get; set; }
        string ConfigNetSslCertThumbprint { get; set; }
        string ConfigNetSslDomain { get; set; }
        string ConfigTorrentsPath { get; set; }
        string ConfigUdpPrefix { get; set; }
        string ConfigVideoPath { get; set; }
        string ConfigVideoPrefix { get; set; }
    }
}

