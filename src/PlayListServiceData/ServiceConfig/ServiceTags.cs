﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Windows;

namespace PlayListServiceData.ServiceConfig
{
    public class ServiceTags : IServiceTags, INotifyPropertyChanged
    {
        private readonly string Name_;
        private readonly string Display_;
        private readonly string Description_;

        public string Name
        {
            get { return Name_; }
        }
        public string Display
        {
            get { return Display_; }
        }
        public string Description
        {
            get { return Description_; }
        }
        public string SysLogName
        {
            get { return "Application"; }
        }

        #region notify
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        public ServiceTags(ResourceDictionary r)
        {
            try
            {
                if (r == null)
                    return;

                Name_ = r["ServiceName"] as string;
                OnPropertyChanged(nameof(Name));
                Display_ = r["DisplayName"] as string;
                OnPropertyChanged(nameof(Display));
                Description_ = r["Description"] as string;
                OnPropertyChanged(nameof(Description));
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }
    }
}
