﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Process
{
    public static class ProcessStat
    {
        public enum Id : long
        {
            Scan = 0,
            ScanLocalVideo = 1,
            ScanLocalAudio = 2,
            ScanTranslateAudio = 3,
            Check = 4,
            Move = 5,
            None = 6
        };
        public static Id[] GetIdAsArray() => new Id[] {
            Id.Scan,
            Id.ScanLocalVideo,
            Id.ScanLocalAudio,
            Id.ScanTranslateAudio,
            Id.Check,
            Id.Move,
        };
    }
}
