﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.Process.CmdLine
{
    public class CmdOptionException : Exception
    {
        public bool IsCallHelp { get; set; } = false;
        private static string GetMessage(Exception e) => $"{e.GetType()}: {e.Message}";

        public static CmdOptionException Create(Exception e) => new CmdOptionException(GetMessage(e), e);
        public static CmdOptionException Create(string s, Exception e) => new CmdOptionException(s, e);
        public static CmdOptionException Create(Exception e, bool b) => new CmdOptionException(GetMessage(e), e, b);
        private CmdOptionException(string s, Exception e) : base(s, e) { }
        private CmdOptionException(string s, Exception e, bool b) : base(s, e) { IsCallHelp = b; }
    }
}
