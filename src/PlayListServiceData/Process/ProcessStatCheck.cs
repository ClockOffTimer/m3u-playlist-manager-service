﻿using System;
using System.ComponentModel;
using System.Text;
using System.Threading;
using System.Xml.Serialization;

namespace PlayListServiceData.Process
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class ProcessStatCheck
    {
        #region CHECK Stat
        public enum StatsId : long
        {
            All = 0,
            Error = 1,
            Reject = 2,
            Good = 3,
            Bad = 4,
            Dup = 5
        }
        [XmlIgnore]
        public ProcessStat.Id Id {
            get => Id_;
            set { Id_ = value; Ids = Id_.ToString(); }
        }
        private ProcessStat.Id Id_ = ProcessStat.Id.None;
        [XmlElement("id")]
        public string Ids { get; set; } = string.Empty;
        [XmlElement("stats")]
        public long[] Stats { get; set; } = new long[6] { 0L, 0L, 0L, 0L, 0L, 0L };
        [XmlElement("last")]
        public DateTime LastDate { get; set; } = DateTime.MinValue;

        private long GetId(StatsId id) => (long)id;
        public  long GetValue(StatsId id) => Stats[GetId(id)];

        public void SetHostTotal(long x) => Interlocked.Add(ref Stats[GetId(StatsId.All)], x);
        public void SetHostDuplicate(long x) => Interlocked.Add(ref Stats[GetId(StatsId.Dup)], x);
        public void SetHostDuplicate() => Interlocked.Increment(ref Stats[GetId(StatsId.Dup)]);
        public void SetHostError() => Interlocked.Increment(ref Stats[GetId(StatsId.Error)]);
        public void SetHostReject() => Interlocked.Increment(ref Stats[GetId(StatsId.Reject)]);
        public void SetHostGood() => Interlocked.Increment(ref Stats[GetId(StatsId.Good)]);
        public void SetHostBad() => Interlocked.Increment(ref Stats[GetId(StatsId.Bad)]);

        public override string ToString()
        {
            StringBuilder sb = new();
            sb.AppendLine($"\r\n{Ids}:");
            sb.AppendLine($"\r\n\t{StatsId.All}:\t\t{StatsId.Error}:\t\t{StatsId.Reject}:\t\t{StatsId.Good}:\t\t{StatsId.Bad}:\t\t{StatsId.Dup}:\r\n");
            sb.AppendLine($"\t{Stats[0]}\t\t{Stats[1]}\t\t{Stats[2]}\t\t{Stats[3]}\t\t{Stats[4]}\t\t{Stats[5]}");
            return sb.ToString();
        }
        #endregion
    }
}
