﻿using System;
using System.Diagnostics;

namespace PlayListServiceData.Process.Log.Formater
{
    public interface ILogFormater
    {
        string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e);
        string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = default);
        Tuple<TraceEventType, object[,]> GetFormat(ILogManager p, DateTime dt, char c, string text, Exception e, string member, string path, int line, object clz = default);
    }
}