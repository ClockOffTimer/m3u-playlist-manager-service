﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace PlayListServiceData.Process.Log.Formater
{
    public class LogFormaterFile : LogFormaterBase, ILogFormater
    {
        private readonly string dateFmt = "dd H:mm";

        public string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e)
        {
            StringBuilder sb = new StringBuilder();

            if (p.IsDatePrint)
                DateFormatInternal(sb, dt, dateFmt);
            if (p.IsThreadIdPrint || p.IsThreadNamePrint)
                ThreadFormatInternal(sb, p.IsThreadNamePrint ? thname : default, p.IsThreadIdPrint ? thid : 0);

            TypeFormatInternal(sb, t);

            if (!string.IsNullOrWhiteSpace(text))
                sb.AppendFormat(" {0}{1}", text, (e == default) ? "" : " ->");
            _ = ExceptionFormatInternal(sb, e);

            return sb.ToString();
        }

        public string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = default)
        {
            StringBuilder sb = new StringBuilder();

            if (p.IsDatePrint)
                DateFormatInternal(sb, dt, dateFmt);
            if (p.IsThreadIdPrint || p.IsThreadNamePrint)
                ThreadFormatInternal(sb, p.IsThreadNamePrint ? thname : default, p.IsThreadIdPrint ? thid : 0);

            TypeFormatInternal(sb, t);

            if (!string.IsNullOrWhiteSpace(text))
                sb.AppendFormat(" {0}{1}", text, (e == default) ? "" : " ->");
            if (p.IsSourceLocationPrint)
                SourceInfoFormatInternal(sb, member, path, line);
            if (ExceptionFormatInternal(sb, e) && (clz != default))
                sb.Append(Environment.NewLine);
            if (clz != default)
                AttributeInspectFormatInternal(sb, clz);

            return sb.ToString();
        }
        public Tuple<TraceEventType, object[,]> GetFormat(ILogManager p, DateTime dt, char c, string text, Exception e, string member, string path, int line, object clz = default)
        {
            TraceEventType t = TypeFormatInternal(c);
            string s = GetFormat(p, t, dt, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, text, e, member, path, line, clz);
            return new Tuple<TraceEventType, object[,]>(t, new object[1, 2] {{ "", s }});
        }
    }
}
