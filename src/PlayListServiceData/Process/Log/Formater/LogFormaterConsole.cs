﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using System.Text;

namespace PlayListServiceData.Process.Log.Formater
{
    public class LogFormaterConsole : LogFormaterBase, ILogFormater
    {
        private readonly string dateFmt = "HH:mm:ss";

        public string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e)
        {
            StringBuilder sb = new StringBuilder();
            if (p.IsDatePrint)
                DateFormatInternal(sb, dt, dateFmt);
            if (p.IsThreadIdPrint || p.IsThreadNamePrint)
                ThreadFormatInternal(sb, p.IsThreadNamePrint ? thname : default, p.IsThreadIdPrint ? thid : 0);
            TypeFormatInternal(sb, t);
            if (!string.IsNullOrWhiteSpace(text))
                sb.AppendFormat(" {0}{1}", text, (e == default) ? "" : " ->");
            _ = ExceptionFormatInternal(sb, e);
            return sb.ToString();
        }

        public string GetFormat(ILogManager p, TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = default)
        {
            StringBuilder sb = new StringBuilder();
            if (p.IsDatePrint)
                DateFormatInternal(sb, dt, dateFmt);
            if (p.IsThreadIdPrint || p.IsThreadNamePrint)
                ThreadFormatInternal(sb, p.IsThreadNamePrint ? thname : default, p.IsThreadIdPrint ? thid : 0);
            TypeFormatInternal(sb, t);
            if (!string.IsNullOrWhiteSpace(text))
                sb.AppendFormat(" {0}{1}", text, (e == default) ? "" : " ->");
            if (p.IsSourceLocationPrint)
                SourceInfoFormatInternal(sb, member, path, line);
            _ = ExceptionFormatInternal(sb, e);
#if DEBUG
            if ((e != default) && (clz != default))
                sb.Append(Environment.NewLine);
            if (clz != default)
                AttributeInspectFormatInternal(sb, clz);
#endif
            return sb.ToString();
        }

        public Tuple<TraceEventType, object[,]> GetFormat(ILogManager p, DateTime dt, char c, string text, Exception e, string member, string path, int line, object clz = default)
        {
            object[,] obj = new object[4, 2];
            TraceEventType t = TypeFormatInternal(c);

            obj[0, 0] = TypeColorInternal(c);
            obj[0, 1] = $"[{c}/{t}]";

            obj[1, 0] = ConsoleColor.DarkGray;
            obj[1, 1] = $"({dt.ToString(dateFmt)}) ";

            obj[2, 0] = Console.ForegroundColor;
            obj[2, 1] = string.IsNullOrWhiteSpace(text) ? "" : $"{text} ";

            obj[3, 0] = Console.ForegroundColor;
            obj[3, 1] = (e == default) ? "" : e?.ExceptionPrint(true);

            return new Tuple<TraceEventType, object[,]>(t, obj);
        }
    }
}
