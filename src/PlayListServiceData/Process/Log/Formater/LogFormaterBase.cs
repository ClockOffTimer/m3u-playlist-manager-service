﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;

namespace PlayListServiceData.Process.Log.Formater
{
    public abstract class LogFormaterBase
    {

        #region protected

        protected void DateFormatInternal(StringBuilder sb, DateTime dt, string datefmt) => sb.Append(dt.ToString(datefmt));
        protected void TypeFormatInternal(StringBuilder sb, char c) => sb.AppendFormat(" [{0}]", TypeFormatInternal(c).ToString().ToUpperInvariant());
        protected void TypeFormatInternal(StringBuilder sb, TraceEventType t)
        {
            switch (t)
            {
                case TraceEventType.Critical:
                case TraceEventType.Error:
                case TraceEventType.Warning:
                case TraceEventType.Information:
                case TraceEventType.Verbose:
                    {
                        sb.AppendFormat(" [{0}]", t.ToString().ToUpperInvariant());
                        break;
                    }
            }
        }

        protected TraceEventType TypeFormatInternal(char c)
        {
            switch (c)
            {
                case '~': return TraceEventType.Critical;
                case '!': return TraceEventType.Error;
                case '?':
                case 'w': return TraceEventType.Warning;
                case '*':
                case '=':
                case 'i': return TraceEventType.Information;
                case '+':
                case '-':
                case 'v':
                case '$':
                case '&': return TraceEventType.Verbose;
                default: return TraceEventType.Error;
            }
        }

        protected ConsoleColor TypeColorInternal(char c)
        {
            switch(c)
            {
                case '~':
                case '!': return ConsoleColor.Red;
                case '*': return ConsoleColor.Yellow;
                case '=': return ConsoleColor.Yellow;
                case '+': return ConsoleColor.Green;
                case '-': return ConsoleColor.DarkGreen;
                case 'i': return ConsoleColor.DarkCyan;
                case '?': return ConsoleColor.Cyan;
                case 'v':
                case '&': return ConsoleColor.DarkMagenta;
                case '$': return ConsoleColor.Magenta;
                default: return Console.ForegroundColor;
            }
        }

        protected void ThreadFormatInternal(StringBuilder sb, string name, int id)
        {
            bool[] b = new bool[]
            {
                !(id <= 0),
                !(name == default)
            };
            if (!b[0] && !b[1])
                return;

            sb.Append(" [");
            if (b[1])
                sb.Append($"{name}");
            if (b[0] && b[1])
                sb.Append("/");
            if (b[0])
                sb.Append($"{id}");
            sb.Append($"] ->");
        }

        protected bool ExceptionFormatInternal(StringBuilder sb, Exception e)
        {
            if (e != default)
            {
                sb.AppendLine(e?.ExceptionPrint(true));
                return true;
            }
            return false;
        }

        protected void SourceInfoFormatInternal(StringBuilder sb, string member, string path, int line)
        {
            bool[] b = new bool[]
            {
                !string.IsNullOrWhiteSpace(member),
                !string.IsNullOrWhiteSpace(path),
                line > 0
            };
            if (!b[0] && !b[1])
                return;

            sb.Append(" [");
            if (b[0])
                sb.Append(member);
            if (b[1])
            {
                string s = Path.GetDirectoryName(path);
                s = (s.Length > 10) ? s.Substring(s.Length - 10) : s;
                sb.AppendFormat("://..{0}/{1}", s, Path.GetFileName(path));
                if (b[2])
                    sb.AppendFormat(":{0}", line);
            }
            sb.Append("]");
        }
        #endregion

        #region Attribute / Reflection (LogInspectAttribute)
        private PropertyInfo GetPropertyInfo(PropertyInfo p, string s)
        {
            return p.PropertyType.GetProperty(
                s, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        }

        protected void AttributeInspectFormatInternal(StringBuilder sb, Object clz)
        {
            Type t = typeof(LogInspectAttribute);
            foreach (var (prop, attr) in from PropertyInfo prop in clz.GetType().GetProperties()
                                         from LogInspectAttribute attr in prop.GetCustomAttributes(t, false)
                                         select (prop, attr)) {
                try
                {
                    object obj = prop.GetValue(clz, null);
                    string name = prop.Name;
                    string val = attr.Converter.Invoke(obj);
                    if (val == default)
                    {
                        PropertyInfo pi;
                        if (((pi = GetPropertyInfo(prop, "Count")) != default) ||
                            (((pi = GetPropertyInfo(prop, "Length")) != default) && (prop.PropertyType != typeof(string))))
                            val = pi.GetValue(obj, null).ToString();
                        else
                            val = obj.ToString();
                    }
                    sb.AppendFormat("\t\t[Inspect: {0} = {1}, {2}]{3}", name, val, prop.PropertyType.ToString(), Environment.NewLine);
                }
#if DEBUG
                catch (Exception e) { Trace.WriteLine($"{nameof(LogFormaterBase)} -> {e}"); }
#else
                catch {}
#endif
            }
        }
        #endregion
    }
}
