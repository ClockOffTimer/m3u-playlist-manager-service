﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;

namespace PlayListServiceData.Process.Log
{
    public interface ILogLevel
    {
        void SetLogLevel();
        TraceEventType GetEventType();
        ILogManager AssertIf(bool b, Exception e, string member, string path, int line);
        ILogManager AssertIf(bool b, string text, string member, string path, int line);
        ILogManager Write(Exception e);
        ILogManager Write(Exception e, string member, string path, int line);
        ILogManager Write(string text);
        ILogManager Write(string text, Exception e);
        ILogManager Write(string text, Exception e, string member, string path, int line, object clz = default);
        ILogManager Write(string text, string member, string path, int line);
    }
}
