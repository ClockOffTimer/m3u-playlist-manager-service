﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using System.Threading;

namespace PlayListServiceData.Process.Log
{
    public class LogLevel : ILogLevel
    {
        private readonly TraceEventType level;
        private readonly ILogManager root;

        public LogLevel(ILogManager root_, TraceEventType level_)
        {
            root = root_;
            level = level_;
        }
        public TraceEventType GetEventType() => level;
        public void SetLogLevel() => root.SetLogLevel(level);

        public ILogManager Write(Exception e) => Write(default, e);
        public ILogManager Write(string text) => Write(text, default);
        public ILogManager Write(Exception e, string member, string path, int line) => Write(default, e, member, path, line);
        public ILogManager Write(string text, string member, string path, int line) => Write(text, default, member, path, line);
        public ILogManager Write(string text, Exception e)
        {
            return root.Write(level, DateTime.Now, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, text, e);
        }
        public ILogManager Write(string text, Exception e, string member, string path, int line, object clz = default)
        {
            return root.Write(level, DateTime.Now, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, text, e, member, path, line, clz);
        }

        ///

        public ILogManager AssertIf(bool b, string text, string member, string path, int line)
        {
            if (b)
                return root.Write(level, DateTime.Now, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, text, default, member, path, line);
            return root;
        }
        public ILogManager AssertIf(bool b, Exception e, string member, string path, int line)
        {
            if (b)
                return root.Write(level, DateTime.Now, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, default, e, member, path, line);
            return root;
        }
    }
}
