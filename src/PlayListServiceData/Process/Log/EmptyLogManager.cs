﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PlayListServiceData.Process.Log
{
    public abstract class EmptyLevel : ILogLevel
    {
        protected readonly ILogManager m = default;
        protected readonly TraceEventType t = TraceEventType.Verbose;

        protected EmptyLevel(ILogManager m_, TraceEventType t_)
        {
            m = m_;
            t = t_;
        }
        public virtual void SetLogLevel() { }
        public virtual TraceEventType GetEventType() { return t; }
        public virtual ILogManager AssertIf(bool b, Exception e, string member, string path, int line) { return m; }
        public virtual ILogManager AssertIf(bool b, string text, string member, string path, int line) { return m; }
        public virtual ILogManager Write(Exception e) { return m; }
        public virtual ILogManager Write(Exception e, string member, string path, int line) { return m; }
        public virtual ILogManager Write(string text) { return m; }
        public virtual ILogManager Write(string text, Exception e) { return m; }
        public virtual ILogManager Write(string text, Exception e, string member, string path, int line, object clz = null) { return m; }
        public virtual ILogManager Write(string text, string member, string path, int line) { return m; }
    }

    public abstract class EmptyLogManager : ILogManager
    {
        public virtual bool IsDatePrint { get; set; } = false;
        public virtual bool IsSourceLocationPrint { get; set; } = false;
        public virtual bool IsThreadIdPrint { get; set; } = false;
        public virtual bool IsThreadNamePrint { get; set; } = false;
        public virtual TraceEventType MinEventType { get; set; } = TraceEventType.Verbose;

        public virtual ILogLevel Error => default;
        public virtual ILogLevel Warning => default;
        public virtual ILogLevel Info => default;
        public virtual ILogLevel Verbose => default;

        public virtual void Close() { }

        public virtual string GetAppName() { return string.Empty; }
        public virtual string GetLogName() { return string.Empty; }
        public virtual ILogManager Report() { return this; }
        public virtual ILogManager Report(Exception x) { return this; }
        public virtual ILogManager SetLogLevel(TraceEventType t) { return this; }
        public virtual void UnhandledLogging(object o, UnhandledExceptionEventArgs e) { }
        public virtual void UnobservedTaskLogging(object o, UnobservedTaskExceptionEventArgs e) { }
        public virtual ILogManager WriteStackTrace() { return this; }
        public virtual ILogManager WriteStackTrace(Exception e) { return this; }
        public virtual ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e) { return this; }
        public virtual ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = null) { return this; }
        public virtual ILogManager Write(TraceEventType t, string s) { return this; }
        public ILogManager Write(char c, string text, Exception e, string member, string path, int line, object clz = default) { return this; }
        public virtual ILogManager Write(string s) { return this; }
        public virtual ILogManager Write(Tuple<TraceEventType, object[,]> tuple) { return this; }
    }
}
