﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;

namespace PlayListServiceData.Process.Log
{
    public interface ILog
    {
        string GetAppName();
        string GetLogName();

        void SetLogLevels(SourceLevels sl);
        void SetLogLevels(TraceEventType te);
        void Open(string s, string l = default);
        void Write(string s);
        void Write(TraceEventType t, string s);
        void Write(Tuple<TraceEventType, object[,]> tuple);
        void Close();
    }
}
