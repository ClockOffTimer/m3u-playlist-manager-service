﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.Process.Log
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter | AttributeTargets.GenericParameter)]
    public class LogInspectAttribute : Attribute
    {
        public Func<object, string> Converter = (a) => { return default; };
    }
}
