﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;

namespace PlayListServiceData.Process.Log
{
    public static class LogUtils
    {
        public class FakeLogManager : EmptyLogManager { }

        #region WriteLocation
        [Description("<ILogManager>.<ILogLevel>.WriteLocation('text', || Exception)")]
        public static ILogManager WriteLocation(this ILogLevel ll, string s,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(s, cmn, cfp, cln);
        }
        public static ILogManager WriteLocation(this ILogLevel ll, Exception e,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(e, cmn, cfp, cln);
        }
        public static ILogManager WriteLocation(this ILogLevel ll, string s, Exception e,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(s, e, cmn, cfp, cln);
        }
        #endregion

        #region WriteInspect
        [Description("<ILogManager>.<ILogLevel>.WriteInspect(this, 'text', || Exception) - using LogInspectAttribute, object this_ == using class")]
        public static ILogManager WriteInspect(this ILogLevel ll, object this_, string s,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(s, default, cmn, cfp, cln, this_);
        }
        public static ILogManager WriteInspect(this ILogLevel ll, object this_, Exception e,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(default, e, cmn, cfp, cln, this_);
        }
        public static ILogManager WriteInspect(this ILogLevel ll, object this_, string s, Exception e,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.Write(s, e, cmn, cfp, cln, this_);
        }
        #endregion

        #region AssertIf
        [Description("<ILogManager>.<ILogLevel>.AssertIf((bool) a > b, 'text' || Exception)")]
        public static ILogManager AssertIf(this ILogLevel ll, bool b, string s,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.AssertIf(b, s, cmn, cfp, cln);
        }
        public static ILogManager AssertIf(this ILogLevel ll, bool b, Exception e,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            return ll.AssertIf(b, e, cmn, cfp, cln);
        }
        #endregion

        #region WriteLog (short error log)
        public static ILogManager WriteLog(this Exception e, char c, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Write(c, default, e, cmn, cfp, cln);
        }
        public static ILogManager WriteLog(this string s, char c, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Write(c, s, default, cmn, cfp, cln);
        }
        public static ILogManager WriteLog(this Exception e, char c, string s, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Write(c, s, e, cmn, cfp, cln);
        }
        public static ILogManager WriteLog(this Exception e, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Error.Write(e, cmn, cfp, cln);
        }
        public static ILogManager WriteLog(this string s, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Error.Write(s, cmn, cfp, cln);
        }
        public static ILogManager WriteLog(this Exception e, string s, ILogManager logm,
            [CallerMemberName] string cmn = null,
            [CallerFilePath] string cfp = null,
            [CallerLineNumber] int cln = 0)
        {
            if (logm == default)
                return new FakeLogManager();
            return logm.Error.Write(s, e, cmn, cfp, cln);
        }
        #endregion

        #region ExceptionPrint (prettyprint)
        public static string ExceptionPrint(this Exception e, bool istab = false)
        {
            if (e == default)
                return string.Empty;
            try
            {
                StringBuilder sb = new StringBuilder();
                Exception ex = e;
                while (ex != default)
                {
                    string type = string.Empty,
                           tab = istab ? "\t\t" : string.Empty;

                    if (ex is WebException wex)
                        sb.Append($"{Environment.NewLine}{tab}{wex.GetType().Name} ({wex.Status}) -> {wex.Message}");
                    else
                        sb.Append($"{Environment.NewLine}{tab}{ex.GetType().Name} -> {ex.Message}");
                    ex = ex.InnerException;
                }
                return sb.ToString();
            }
            catch { return string.Empty; }
        }
        public static T1 FindException<T1>(this Exception e) where T1 : class
        {
            try
            {
                Exception ex = e;
                while (ex != default)
                {
                    if (ex is T1 exc)
                        return exc;
                    ex = ex.InnerException;
                }
            }
            catch { }
            return default;
        }
        #endregion

    }
}
