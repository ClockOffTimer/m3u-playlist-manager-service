﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Process.Log.Formater;
using PlayListServiceData.Process.Report;

namespace PlayListServiceData.Process.Log
{
    public class ProcessLog : ILogManager
    {
        private readonly ILog ilog;
        private readonly IReporter ireporter;
        private readonly object __lock = new object();
        private ILogFormater logFormater { get; set; } = default;

        public TraceEventType MinEventType { get; set; } = TraceEventType.Verbose;
        public bool IsDatePrint { get; set; } = true;
        public bool IsThreadIdPrint { get; set; } = true;
        public bool IsThreadNamePrint { get; set; } = true;
        public bool IsSourceLocationPrint { get; set; } = true;

        private Exception exception { get; set; } = default;

        public ILogLevel Info { get; private set; }
        public ILogLevel Error { get; private set; }
        public ILogLevel Warning { get; private set; }
        public ILogLevel Verbose { get; private set; }

        public ILogManager SetLogLevel(TraceEventType t)
        {
            MinEventType = t;
            ilog.SetLogLevels(t);
            return this;
        }
        public static ILogManager Create(ILog ilog_)
        {
            return new ProcessLog(ilog_, new LogFormaterConsole(), default);
        }
        public static ILogManager Create(ILog ilog_, IReporter reporter_)
        {
            return new ProcessLog(ilog_, new LogFormaterConsole(), reporter_);
        }
        public static ILogManager Create(ILog ilog_, ILogFormater iformat_)
        {
            return new ProcessLog(ilog_, iformat_, default);
        }
        public static ILogManager Create(ILog ilog_, ILogFormater iformat_, IReporter reporter_)
        {
            return new ProcessLog(ilog_, iformat_, reporter_);
        }

        [Description("Don't use direct in code")]
        private ProcessLog(ILog ilog_, ILogFormater iformat_, IReporter reporter_)
        {
            ilog = ilog_;
            logFormater = iformat_;
            ireporter = reporter_;
            Info = new LogLevel(this, TraceEventType.Information);
            Error = new LogLevel(this, TraceEventType.Error);
            Warning = new LogLevel(this, TraceEventType.Warning);
            Verbose = new LogLevel(this, TraceEventType.Verbose);
        }
        ~ProcessLog()
        {
            Close();
            GC.Collect(0, GCCollectionMode.Forced);
            GC.WaitForPendingFinalizers();
        }

        private  bool GetTypeSelector(TraceEventType t) => ((int)MinEventType >= (int)t);
        public string GetAppName() => ilog.GetAppName();
        public string GetLogName() => ilog.GetLogName();

        public void Close()
        {
            if (ilog != null)
                ilog?.Close();
            if (ireporter != null)
                ireporter?.Close();
        }

        public ILogManager Report()
        {
            Exception x;
            lock (__lock)
            {
                x = exception;
                exception = default;
            }
            if ((x != default) && (ireporter != default))
                ireporter?.Send(x);
            return this;
        }

        public ILogManager Report(Exception x)
        {
            if (ireporter != default)
                ireporter?.Send(x);
            return this;
        }

        public ILogManager WriteStackTrace()
        {
            string s;
            lock (__lock)
            {
                if (exception == default)
                    return this;
                else
                    s = exception.StackTrace;
            }
            if (s != default)
                return Write(s);
            return this;
        }

        public ILogManager WriteStackTrace(Exception e)
        {
            lock (__lock)
                exception = e;
            var t = e.StackTrace;
            if (t != default)
                return Write(t);
            return this;
        }

        /* Global exceptions handle Write */
        public void UnobservedTaskLogging(object o, UnobservedTaskExceptionEventArgs e) =>
            HandleLogging_("*!Unobserved", e.Exception as Exception);
        public void UnhandledLogging(object o, UnhandledExceptionEventArgs e) =>
            HandleLogging_("*!Unhandled", e.ExceptionObject as Exception);
        private void HandleLogging_(string tag, Exception e)
        {
            if (e.GetType() == typeof(AggregateException))
                return;
            Write(TraceEventType.Critical, DateTime.Now, Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.Name, tag, e)
           .WriteStackTrace()
           .Report();
        }

        /* Begin Write */
        public ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e)
        {
            if (!GetTypeSelector(t))
                return this;
            lock (__lock)
                exception = e;
            return Write(
                logFormater.GetFormat(this, t, dt, thid, thname, text, e));
        }
        public ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = default)
        {
            if (!GetTypeSelector(t))
                return this;
            lock (__lock)
                exception = e;
            return Write(
                logFormater.GetFormat(this, t, dt, thid, thname, text, e, member, path, line, clz));
        }
        public ILogManager Write(char c, string text, Exception e, string member, string path, int line, object clz = default)
        {
            lock (__lock)
                exception = e;
            return Write(
                logFormater.GetFormat(this, DateTime.Now, c, text, e, member, path, line, clz));
        }
        public ILogManager Write(string s)
        {
            lock (__lock)
                ilog?.Write(s);
            return this;
        }
        public ILogManager Write(TraceEventType t, string s)
        {
            if (!GetTypeSelector(t))
                return this;

            lock (__lock)
                ilog?.Write(t, s);
            return this;
        }
        public ILogManager Write(Tuple<TraceEventType, object[,]> tuple)
        {
            lock (__lock)
                ilog?.Write(tuple);
            return this;
        }
        /* End Write */
    }
}
