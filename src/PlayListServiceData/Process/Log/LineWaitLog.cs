﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Threading;

namespace PlayListServiceData.Process.Log
{
    public class LineWaitLog
    {
        private readonly object lock_ = new object();
        private readonly StringBuilder sb_ = new StringBuilder();
        private ILogLevel log_ = default;
        private Timer timer_ = default;
        private bool iswrite_;
        private string header_ { get; set; } = default;

        private void TimerStop()
        {
            if (IsTimer())
                timer_.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        private void TimerStart(double sec)
        {
            if (IsTimer())
                timer_.Change(TimeSpan.FromSeconds(sec), Timeout.InfiniteTimeSpan);
        }

        public LineWaitLog AddCount(int x)   { lock (lock_) { iswrite_ = false; sb_.Clear(); sb_.Append($" [counter:{x}]"); TimerStart(2.0); } return this; }
        public LineWaitLog AddTime(string s) { lock (lock_) { iswrite_ = false; DateTime d = DateTime.Now; sb_.Append($" [{s}:{d.Hour}:{d.Minute}:{d.Second}:{d.Millisecond}]"); TimerStart(6.0); } return this; }
        public LineWaitLog AddTag(string s)  { if (s != default) lock (lock_) { iswrite_ = false; header_ += $" [tag:{s}]"; } return this; }
        public LineWaitLog Add(string s)     { lock (lock_) { iswrite_ = false; header_ = s; } return this; }
        public string Get()                  { lock (lock_) { iswrite_ = true; TimerStop(); sb_.Insert(0, header_); string s = sb_.ToString(); sb_.Clear(); return s; }}
        public void Write()                  { if (!IsLog()) return; string s = Get(); if (!string.IsNullOrWhiteSpace(s)) lock (lock_) { log_.Write(s); }}
        public void End()                    { AddTime("polling stop"); Write(); Close(); sb_.Clear(); }
        public bool IsLog() =>               log_ != default;
        public bool IsTimer() =>             timer_ != default;

        public LineWaitLog(ILogLevel log)
        {
            iswrite_ = false;
            log_ = log;
            timer_ = new Timer((o) =>
            {
                if (iswrite_)
                {
                    lock (lock_)
                    {
                        if (IsTimer())
                            TimerStop();
                        iswrite_ = false;
                    }
                    return;
                }
                Write();
            }, null, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        ~LineWaitLog()
        {
            Close();
        }
        private void Close()
        {
            lock (lock_)
            {
                log_ = default;
                Timer t = timer_;
                timer_ = default;
                if (t != default)
                {
                    try { t.Dispose(); } catch { }
                }
            }
        }
    }
}
