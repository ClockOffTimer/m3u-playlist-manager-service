﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Process.Log
{
    public static class LogContant
    {
        public static string msg1() => $" URL not found in base: ";
        public static string msg2(string s) => $" return bad {s} request";
        public static string msg3(string s) => $" return empty {s} request";
        public static string msg4() => $" -> check valid Mime: ";
        public static string msg5() => $" -> check DASH Mime: ";
        public static string msg6() =>  " -> check DASH URL part: ";
        public static string msg7() =>  " Wait All ->";
        public static string msg8() =>  " Wait Complette ->";
        public static string msg9() =>  " Tasks Wait: ";
        public static string msg10() => " Bad tasks: ";
        public static string msg11() => " Run retry ->";
        public static string msg12() => " Build Task ->";
        public static string msg13() => " Duplicate exception from tasks: ";
        public static string msg14() => " Task Complette ->";
    }
}
