﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;

namespace PlayListServiceData.Process.Log.Handler
{
    public class ConsoleLog : ILog
    {
        private readonly object lock__ = new object();

        public void Close() { }
        public string GetAppName() => default;
        public string GetLogName() => nameof(ConsoleLog);
        public void Open(string _, string __ = null) { }
        public void SetLogLevels(SourceLevels _) { }
        public void SetLogLevels(TraceEventType _) { }
        public void Write(string s)
        {
            lock (lock__)
                Console.WriteLine(s);
        }
        public void Write(TraceEventType t, string s)
        {
            ConsoleColor clr = t switch
            {
                TraceEventType.Error => ConsoleColor.Red,
                TraceEventType.Warning => ConsoleColor.DarkRed,
                TraceEventType.Verbose => ConsoleColor.DarkCyan,
                TraceEventType.Information => ConsoleColor.Cyan,
                _ => Console.ForegroundColor,
            };
            lock (lock__)
            {
                Write_($"[{t}] ", clr);
                Write_(s, Console.ForegroundColor);
                Console.Write(Environment.NewLine);
            }
        }
        public void Write(Tuple<TraceEventType, object[,]> tuple)
        {
            lock (lock__)
            {
                for (int i = 0; i < tuple.Item2.GetLength(0); i++)
                {
                    ConsoleColor clr = (tuple.Item2[i, 0] is ConsoleColor c) ? c : Console.ForegroundColor;
                    if ((tuple.Item2[i, 1] is string s) && !string.IsNullOrWhiteSpace(s))
                        Write_(s, clr);
                }
                Console.Write(Environment.NewLine);
            }
        }

        private static void Write_(string s, ConsoleColor clr)
        {
            Console.ForegroundColor = clr;
            Console.Write(s);
            Console.ResetColor();
        }
    }
}
