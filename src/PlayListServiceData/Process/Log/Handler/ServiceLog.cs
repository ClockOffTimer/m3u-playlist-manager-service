﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace PlayListServiceData.Process.Log
{
    public class ServiceLog : ILog
    {
        private readonly object __lock = new();
        private EventLog listener_ = default;

        private EventLog Getlistener() => listener_;
        private void Setlistener(EventLog value) => listener_ = value;

        private string AppName_ { get; set; } = default;
        private string LogName_ { get; set; } = default;

        [Description("use ServiceBase.EventLog from Service class, before init custom log instance")]
        public ServiceLog(EventLog evlog, string src = default, string log = default)
        {
            Setlistener(evlog);
            LogName_ = evlog.Log = (log == default) ? evlog.Log : log;
            AppName_ = evlog.Source = (src == default) ? evlog.Source : src;
        }

        public string GetAppName() => AppName_;
        public string GetLogName() => LogName_;
        public void SetLogLevels(SourceLevels sl) { }
        public void SetLogLevels(TraceEventType te) { }
        public void Open(string x, string y = default) {
            try {
                Getlistener().Clear();
                System.Diagnostics.Eventing.Reader.EventLogSession.GlobalSession.ClearLog(Getlistener().Log);
                Debug.WriteLine($"EventLog Open: {Getlistener().MachineName}/{Getlistener().LogDisplayName} = {Getlistener().Log}/{Getlistener().Source}");
            } catch {}
        }
        public void Close() { }

        public void Write(string s)
        {
            Write(TraceEventType.Information, s);
        }

        public void Write(TraceEventType t, string s)
        {
            lock (__lock)
            {
                var elt = t switch
                {
                    TraceEventType.Error => EventLogEntryType.Error,
                    TraceEventType.Warning => EventLogEntryType.Warning,
                    TraceEventType.Verbose => EventLogEntryType.Information,
                    TraceEventType.Information => EventLogEntryType.Information,
                    _ => EventLogEntryType.Information,
                };
                Getlistener().WriteEntry(s, elt);
            }
        }

        public void Write(Tuple<TraceEventType, object[,]> tuple)
        {
            StringBuilder sb = new();
            for (int i = 0; i < tuple.Item2.GetLength(0); i++)
                if ((tuple.Item2[i, 1] is string s) && !string.IsNullOrWhiteSpace(s))
                    sb.Append(s);
            if (sb.Length > 0)
                Write(tuple.Item1, sb.ToString());
        }
    }
}
