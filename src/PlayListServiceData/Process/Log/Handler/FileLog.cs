﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define TRACE

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;

namespace PlayListServiceData.Process.Log
{
    public class FileLog : ILog
    {
        private readonly object lock_ = new object();
        private string runId { get; set; } = default;
        private Timer closeTimer;
        private FileStream logStream { get; set; } = default;
        private TextWriter logWriter { get; set; } = default;
        private string logName { get; set; } = default;
        private bool isOpened = false;

        public string GetAppName() => default;
        public string GetLogName() => logName;
        public void SetLogLevels(SourceLevels sl) { }
        public void SetLogLevels(TraceEventType te) { }

        public FileLog(string path, string id = default)
        {
            runId = id;
            Open(path);
            closeTimer = new Timer((o) =>
            {
                if (!isOpened)
                    lock (lock_)
                    {
                        Close_();
                    }
                closeTimer.Dispose();
                closeTimer = default;
            }, null, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        ~FileLog()
        {
            isOpened = false;
            Close_();
        }

        public void Open(string s, string n = default)
        {
            try
            {
                FileInfo file = new FileInfo(s);
                if ((file == null) || (file.Exists && file.IsReadOnly))
                    throw new Exception($"create logging file error -> {s}");

                if (file.Exists)
                    file.Delete();

                lock (lock_)
                {
                    Close();
                    logStream = File.Open(file.FullName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                    logWriter = new StreamWriter(logStream, new UTF8Encoding(false))
                    {
                        AutoFlush = true,
                        NewLine = "\n"
                    };
                    isOpened = true;
                }
                Trace.WriteLine($"Log Open: {runId} ({nameof(FileLog)})");
            }
            catch (Exception e) { Close(); Trace.WriteLine(e); }
        }

        public void Close()
        {
            isOpened = false;
            if (closeTimer != default)
                closeTimer.Change(TimeSpan.FromSeconds(5), Timeout.InfiniteTimeSpan);
            else
                Close_();
        }

        private void Close_()
        {
            lock (lock_)
            {
                var sw = logWriter;
                logWriter = null;
                if (sw != null)
                {
                    try
                    {
                        try { sw.Flush(); } catch { }
                        try { sw.Close(); } catch { }
                        try { sw.Dispose(); } catch { }
                    }
                    catch (Exception e) { Trace.WriteLine(e); }
                }
                var sl = logStream;
                logStream = null;
                if (sl != null)
                {
                    try
                    {
                        if (sl.CanWrite)
                            sl.Flush();
                        try { sl.Close(); } catch { }
                        try { sl.Dispose(); } catch { }
                    }
                    catch (Exception e) { Trace.WriteLine(e); }
                }
            }
            Trace.WriteLine($"Log Close: {runId} ({nameof(FileLog)})");
        }

        public void Write(TraceEventType t, string s)
        {
            if (!isOpened)
                return;
            Write_(s);
        }
        public void Write(string s)
        {
            if (!isOpened)
                return;
            Write_(s);
        }
        public void Write(Tuple<TraceEventType, object[,]> tuple)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < tuple.Item2.GetLength(0); i++)
                if ((tuple.Item2[i, 1] is string s) && !string.IsNullOrWhiteSpace(s))
                    sb.Append(s);
            if (sb.Length > 0)
                Write_(sb.ToString());
        }

        private void Write_(string s)
        {
            lock (lock_)
            {
                try { logWriter?.WriteLine(s); }
                catch (Exception e) { Trace.WriteLine($"{nameof(FileLog)}: {e}"); }
                /* logWriter.Flush(); */
            }
        }
    }
}
