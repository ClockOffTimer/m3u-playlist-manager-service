﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PlayListServiceData.Process.Log
{
    public interface ILogManager
    {
        bool IsDatePrint { get; set; }
        bool IsSourceLocationPrint { get; set; }
        bool IsThreadIdPrint { get; set; }
        bool IsThreadNamePrint { get; set; }
        TraceEventType MinEventType { get; set; }

        ILogLevel Error { get; }
        ILogLevel Warning { get; }
        ILogLevel Info { get; }
        ILogLevel Verbose { get; }

        void Close();
        string GetAppName();
        string GetLogName();
        ILogManager Report();
        ILogManager Report(Exception x);
        ILogManager SetLogLevel(TraceEventType t);
        void UnhandledLogging(object o, UnhandledExceptionEventArgs e);
        void UnobservedTaskLogging(object o, UnobservedTaskExceptionEventArgs e);
        ILogManager Write(char c, string text, Exception e, string member, string path, int line, object clz = default);
        ILogManager Write(Tuple<TraceEventType, object[,]> tuple);
        ILogManager Write(string s);
        ILogManager Write(TraceEventType t, string s);
        ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e);
        ILogManager Write(TraceEventType t, DateTime dt, int thid, string thname, string text, Exception e, string member, string path, int line, object clz = null);
        ILogManager WriteStackTrace();
        ILogManager WriteStackTrace(Exception e);
    }
}