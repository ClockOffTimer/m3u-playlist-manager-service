﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.Process.Http
{
    public class HttpClientException : Exception
    {
        public static HttpClientException Create(Exception ea)
        {
            return Create(ea, "");
        }
        public static HttpClientException Create(Exception ea, string url)
        {
            if (ea == default)
                return new HttpClientException(url);
            if (ea.GetType() == typeof(AggregateException))
            {
                Exception e = ea.InnerException;
                bool b = !string.IsNullOrWhiteSpace(url);
                if (e == default)
                    return new HttpClientException(b ? url : ea.Message, ea);
                else if (e.InnerException == default)
                    return new HttpClientException(b ? url : e.Message, e);
                else
                    return new HttpClientException(b ? url : e.Message, e?.InnerException);
            }
            else
                return new HttpClientException(ea);
        }
        private HttpClientException() : base() { }
        private HttpClientException(string message) : base(message) { }
        private HttpClientException(Exception e) : base(e.Message, e) { }
        private HttpClientException(string message, Exception e) : base(message, e) { }
    }
}
