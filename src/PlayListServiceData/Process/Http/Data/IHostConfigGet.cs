﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;

namespace PlayListServiceData.Process.Http.Data
{
    public interface IHostConfigGet
    {
        int ConfigNetPort { get; }

        bool IsConfigNetSslEnable { get; }
        bool IsConfigNetHttpSupport { get; }

        string ConfigNetSslDomain { get; }
        string ConfigNetSslCertStore { get; }
        string ConfigNetSslCertThumbprint { get; }
        string ConfigNetAddr { get; }

        List<string> ConfigUserAuth { get; }
    }
}
