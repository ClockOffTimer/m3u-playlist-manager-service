﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Security.Cryptography.X509Certificates;
using PlayListServiceData.Certificate;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.Process.Http.Data
{
    public class HostConfig
    {
        private readonly ILogManager LogWriter;
        public HostEntry HTTP { get; private set; }
        public HostEntry HTTPS { get; private set; }
        public AuthenticationHeaderValue AuthenticationHeader { get; private set; }

        public HostConfig(IHostConfigGet hostcnf, X509Certificate2 cert, ILogManager log)
        {
            LogWriter = log;
            BuildHostConfig(hostcnf, cert, false);
        }

        public HostConfig(IHostConfigGet hostcnf, ILogManager log)
        {
            LogWriter = log;
            X509Certificate2 cert = default;
            try
            {
                cert = BuildSSLCert(hostcnf);
                BuildHostConfig(hostcnf, cert, true);
            }
            catch { }
            finally
            {
                if (cert != default)
                    cert.Dispose();
            }
        }

        private void BuildHostConfig(IHostConfigGet hostcnf, X509Certificate2 cert, bool isBasicAuth)
        {
            string dnsHttp = string.Empty;
            string dnsHttps = string.Empty;
            int portHttp = 0;
            int portHttps = 0;
            do
            {
                if (!hostcnf.IsConfigNetSslEnable)
                    break;
                if ((cert != default) && cert.Verify())
                    dnsHttps = cert.GetNameInfo(X509NameType.DnsName, false);
                if (string.IsNullOrWhiteSpace(dnsHttps))
                    dnsHttps = hostcnf.ConfigNetSslDomain;
                else break;
                if (string.IsNullOrWhiteSpace(dnsHttps))
                    dnsHttps = hostcnf.ConfigNetAddr;
                else break;
                if (string.IsNullOrWhiteSpace(dnsHttps))
                    dnsHttps = "*";

            } while (false);

            do
            {
                if (!hostcnf.IsConfigNetSslEnable)
                    break;

                if (hostcnf.ConfigNetPort > 0)
                    portHttps = hostcnf.ConfigNetPort;
                else
                    portHttps = 8089;

            } while (false);

            do
            {
                if (hostcnf.IsConfigNetSslEnable && !hostcnf.IsConfigNetHttpSupport)
                    break;

                dnsHttp = hostcnf.ConfigNetAddr;
                if (string.IsNullOrWhiteSpace(dnsHttp))
                    dnsHttp = "*";

                if (hostcnf.ConfigNetPort > 0)
                {
                    if (hostcnf.IsConfigNetSslEnable)
                        portHttp = hostcnf.ConfigNetPort - 1;
                    else
                        portHttp = hostcnf.ConfigNetPort;
                }
                else
                {
                    if (hostcnf.IsConfigNetSslEnable)
                        portHttp = 8088;
                    else
                        portHttp = 8089;
                }

            } while (false);

            HTTP = new("http", dnsHttp, portHttp);
            HTTPS = new("https", dnsHttps, portHttps);

            if (isBasicAuth)
            {
                (string l, string p) = BuildBasicAuth(hostcnf);
                if (string.IsNullOrWhiteSpace(l) || string.IsNullOrWhiteSpace(p))
                    return;

                AuthenticationHeader = new(
                    "Basic", Convert.ToBase64String(
                        new UTF8Encoding(false).GetBytes($"{l}:{p}")));
            }
        }

        private X509Certificate2 BuildSSLCert(IHostConfigGet hostcnf)
        {
            if (!hostcnf.IsConfigNetSslEnable || string.IsNullOrWhiteSpace(hostcnf.ConfigNetSslCertThumbprint))
                return default;
            try {
                CertManager cm = new(
                    hostcnf.ConfigNetSslCertStore,
                    hostcnf.ConfigNetSslCertThumbprint,
                    hostcnf.ConfigNetSslDomain,
                    hostcnf.ConfigNetPort
                    );
                cm.EventCb += (s, e) => {
                    e.WriteLog('!', $"{this.GetType().Name} {nameof(BuildSSLCert)} EventCb:", LogWriter);
                };
                return cm.Get();
            }
            catch (Exception e) { e.WriteLog('!', $"{this.GetType().Name} {nameof(BuildSSLCert)}:", LogWriter); }
            return default;
        }

        private (string, string) BuildBasicAuth(IHostConfigGet hostcnf)
        {
            try
            {
                List<string> list = hostcnf.ConfigUserAuth;
                if ((list == default) || (list.Count == 0))
                    return default;

                foreach (string s in list)
                {
                    try
                    {
                        if (string.IsNullOrWhiteSpace(s))
                            continue;

                        string ss = EncryptString.Decrypt(s);
                        if (string.IsNullOrWhiteSpace(ss))
                            continue;

                        string[] arr = ss.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        if ((arr == default) || (arr.Length == 0))
                            continue;

                        if (arr.Length == 2)
                            return (arr[0], arr[1]);
                    }
                    catch (System.Security.Cryptography.CryptographicException se) {
                        $"{this.GetType().Name} {nameof(BuildBasicAuth)}: {se.Message}".WriteLog('!', LogWriter);
                    }
                    catch (Exception e) {
                        e.WriteLog('!', $"{this.GetType().Name} {nameof(BuildBasicAuth)}:", LogWriter);
                    }
                }
            }
            catch (Exception e) {
                e.WriteLog('!', $"{this.GetType().Name} {nameof(BuildBasicAuth)}:", LogWriter);
            }
            return default;
        }
    }
}
