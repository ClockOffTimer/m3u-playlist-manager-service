﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Process.Http.Data
{
    public class HostEntry
    {
        public bool   IsEmpty { get; private set; }
        public bool   IsValid { get; private set; }
        public int    Port { get; private set; }
        public string Name { get; private set; }
        public string Proto { get; private set; }

        public HostEntry(string proto, string name, int port)
        {
            Proto = proto;
            Port = port;
            Name = name;
            IsEmpty = (Port == 0) || string.IsNullOrWhiteSpace(Name);
            IsValid = !IsEmpty && !Name.Equals("*");
        }

        public override string ToString()
        {
            if (IsEmpty)
                return string.Empty;
            return $"{Proto}://{Name}:{Port}/";
        }
    }
}
