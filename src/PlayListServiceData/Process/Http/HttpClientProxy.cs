﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define ISEVENTLOG

using System;
using System.Net;

namespace PlayListServiceData.Process.Http
{
    public class HttpClientProxy : HttpClientLogEvent<char, string, Exception>, IWebProxy
    {
        private readonly Func<string> GetRandomProxy;
        private readonly Func<bool> IsProxy;
        private ICredentials CredentialsValue { get; set; } = default;
        private Uri UriValue { get; set; } = default;
        public Uri ActualUrl
        {
            get => UriValue;
        }
        public IWebProxy Proxy
        {
            get => throw new NotSupportedException(nameof(IWebProxy));
            set { }
        }
        public ICredentials Credentials
        {
            get => CredentialsValue;
            set { if (CredentialsValue != value) CredentialsValue = value; }
        }
        public Uri GetProxy(Uri u)
        {
            try
            {
                string uri = GetRandomProxy.Invoke();
                if (string.IsNullOrWhiteSpace(uri))
                {
                    if (UriValue == default)
                        return WebRequest.DefaultWebProxy.GetProxy(u);
                    return UriValue;
                }
                if ((UriValue == default) || (!uri.Equals(UriValue.AbsoluteUri)))
                    UriValue = new Uri(uri, UriKind.Absolute);

                #region Event print
#if ISEVENTLOG
                CallEvent('&', $"\t    Request proxy ->\t{UriValue}", default);
#endif
                #endregion

                return UriValue;
            }
            catch { return WebRequest.DefaultWebProxy.GetProxy(u); }
        }
        public bool IsBypassed(Uri u) => !IsProxy.Invoke();

        public HttpClientProxy(Func<string> proxyurl, Func<bool> isproxy, Action<char, string, Exception> act)
        {
            if (proxyurl != default)
                GetRandomProxy = proxyurl;
            else
                GetRandomProxy = () => string.Empty;
            if (isproxy != default)
                IsProxy = isproxy;
            else
                IsProxy = () => { return true; };
            EventCb += (c, s, a) => {
                act.Invoke(c, s, a);
            };
        }
    }
}
