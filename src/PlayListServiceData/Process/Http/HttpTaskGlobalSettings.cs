﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.Process.Http
{
    public class HttpTaskGlobalSettings
    {
        private bool[] chkOptArray { get; set; }
        public ProcessStatCheck Stat { get; private set; }
        public CancellationToken Token { get; set; }
        public SemaphoreSlim LockWaiter { get; set; }
        public ILogManager LogWriter { get; set; }

        public bool IsConfigCheckSkipProto    => chkOptArray[0];
        public bool IsConfigCheckSkipMime     => chkOptArray[1];
        public bool IsConfigCheckSkipDash     => chkOptArray[2];
        public bool IsConfigCheckSkipUdpProxy => chkOptArray[3];
        public bool[] RequestFilterOptions { get { return chkOptArray; }  set { chkOptArray = value; } }

        public HttpTaskGlobalSettings(
            ProcessStatCheck p, SemaphoreSlim s, CancellationToken t, ILogManager l, bool[] opt = default
            )
        {
            object[] obj = new object[]
            {
                p, t, s, l
            };
            for (int i = 0; i < obj.Length; i++)
                if (obj[i] == default)
                    throw new ArgumentNullException($"{i} object empty");

            Stat = p;
            Token = t;
            LockWaiter = s;
            LogWriter = l;
            chkOptArray = (opt == default) ? new bool[4] : opt;
        }
    }
}
