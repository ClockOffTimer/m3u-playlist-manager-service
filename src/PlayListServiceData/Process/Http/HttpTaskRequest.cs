﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using M3U.Model;
using PlayListServiceData.Process.Log;
using Tasks = System.Threading.Tasks;

namespace PlayListServiceData.Process.Http
{
    public partial class HttpTaskRequest : IDisposable
    {
        public enum RequestState : long
        {
            None = 0,
            WaitSlim,
            RunRequest,
            CancellRequest,
            ErrorRequest,
            ErrorEmptyRequest,
            EndOk
        };

        private HttpTaskGlobalSettings GlobalSettings { get; set; } = default;
        private long rstate = (long)RequestState.None;
        public Media media { get; private set; } = default;
        public HttpResponseMessage hmsg { get; private set; } = default;
        public RequestState State
        {
            get {
                return (RequestState)Interlocked.Read(ref rstate);
            }
        }

        public HttpTaskRequest(Media m, HttpTaskGlobalSettings gs)
        {
            media = m;
            GlobalSettings = gs;
        }
        ~HttpTaskRequest()
        {
            Dispose();
        }

        public void Dispose()
        {
            HttpResponseMessage hrm = hmsg;
            hmsg = default;
            if (hrm != default)
                try { hrm.Dispose(); } catch { }
        }

        public async Tasks.Task<HttpTaskRequest> BuildResult()
        {
            if (GlobalSettings == default)
                throw new NullReferenceException(nameof(HttpTaskGlobalSettings));

            return await Tasks.Task<HttpTaskRequest>.Run(async () =>
            {
                try
                {
                    StateChange(RequestState.WaitSlim);
                    await GlobalSettings?.LockWaiter.WaitAsync();
                    if (GlobalSettings.Token.IsCancellationRequested)
                    {
                        StateChange(RequestState.CancellRequest);
                        return this;
                    }
                    StateChange(RequestState.RunRequest);
                    Uri uri = new(media.MediaFile, UriKind.Absolute);
                    hmsg = await uri.NewHeaderRequest(GlobalSettings.Token).ConfigureAwait(false);
                    StateChange(RequestState.EndOk);
                }
                catch (Exception e)
                {
                    if ((e is OperationCanceledException) || (e is Tasks.TaskCanceledException))
                        StateChange(RequestState.CancellRequest);
                    else
                        StateChange(RequestState.ErrorRequest);

                    WebException ex = e.FindException<WebException>();
                    if (ex != default)
                        switch (ex.Status)
                        {
                            case WebExceptionStatus.ProxyNameResolutionFailure:
                            case WebExceptionStatus.RequestProhibitedByProxy:
                                {
                                    $"{nameof(HttpTaskRequest)} Task -> BAD proxy: {ex.Status}".WriteLog('!', GlobalSettings?.LogWriter);
                                    break;
                                }
                        }

                    e.WriteLog('!', $"{nameof(HttpTaskRequest)} Task ->", GlobalSettings?.LogWriter);
                    GlobalSettings?.Stat.SetHostError();
                }
                finally
                {
                    GlobalSettings?.LockWaiter.Release();
                }
                return this;
            });
        }

        public async Tasks.Task<RequestState> StateCheck()
        {
            return await Tasks.Task.FromResult(StateCheckResult());
        }

        private RequestState StateCheckResult()
        {
            if ((GlobalSettings == default) || (hmsg == default))
                return RequestState.ErrorEmptyRequest;

            switch (State)
            {
                case RequestState.EndOk:
                case RequestState.ErrorRequest:
                case RequestState.ErrorEmptyRequest: break;
                default: return State;
            }

            string uri, mime, ext;
            try
            {
                do
                {
                    if (!hmsg.IsSuccessStatusCode)
                        break;
                    if ((hmsg.RequestMessage == default) ||
                        (hmsg.RequestMessage.RequestUri == default))
                        break;
                    if (!(bool)(hmsg?.RequestMessage?.RequestUri?.IsAbsoluteUri))
                        break;
                    if ((hmsg.Content == null) ||
                        (hmsg.Content.Headers == null) ||
                        (hmsg.Content.Headers.ContentType == null))
                        break;

                    mime = hmsg.Content.Headers.ContentType.MediaType;
                    if (string.IsNullOrEmpty(mime))
                        break;

                    media.MediaFileMime = mime;
                    uri = hmsg.RequestMessage.RequestUri.AbsoluteUri.Normalize(NormalizationForm.FormC);
                    if (string.IsNullOrWhiteSpace(uri))
                    {
                        if (media.MediaFile.Contains("?"))
                        {
                            string[] str = media.MediaFile.Split(new char[] { '?' });
                            if (str.Length > 0)
                                uri = str[0].Trim();
                        }
                        if (string.IsNullOrWhiteSpace(uri))
                            uri = media.MediaFile.Trim();
                    }
                    if (!media.MediaFile.Equals(uri))
                        media.MediaFiles[1] = uri;

                    ext = getIsMediaExtension(uri);
                    if (!string.IsNullOrWhiteSpace(ext))
                        media.MediaFileExt = ext;

                    if ((hmsg.Content?.Headers?.ContentLength != null) &&
                            (hmsg.Content?.Headers?.ContentLength > 0))
                        media.Duration = (long)hmsg.Content?.Headers?.ContentLength;

                    if (GlobalSettings.IsConfigCheckSkipDash)
                    {
                        if (CheckIsDashMime(media.MediaFileMime, media.GetParserId) ||
                            CheckIsDashUrlPart(uri, media.GetParserId))
                        {
                            GlobalSettings?.Stat.SetHostReject();
                            break;
                        }
                    }
                    if (GlobalSettings.IsConfigCheckSkipMime)
                    {
                        if (!CheckValidMime(media.MediaFileMime, media.GetParserId))
                        {
                            GlobalSettings?.Stat.SetHostReject();
                            break;
                        }
                    }
                    if (GlobalSettings.IsConfigCheckSkipDash)
                    {
                        if (CheckIsDashMime(media.MediaFileMime, media.GetParserId) ||
                            CheckIsDashUrlPart(uri, media.GetParserId))
                        {
                            GlobalSettings?.Stat.SetHostReject();
                            break;
                        }
                    }
                    if (GlobalSettings.IsConfigCheckSkipMime)
                    {
                        if (!CheckValidMime(media.MediaFileMime, media.GetParserId))
                        {
                            GlobalSettings?.Stat.SetHostReject();
                            break;
                        }
                    }

                    GlobalSettings?.Stat.SetHostGood();
                    return RequestState.EndOk;

                } while (false);

                GlobalSettings?.Stat.SetHostBad();
                return RequestState.ErrorRequest;
            }
            catch (Exception e)
            {
                HttpClientException.Create(e)
                                   .WriteLog('!', $"{nameof(HttpTaskRequest)} Result -> {e.Message}", GlobalSettings?.LogWriter);
                GlobalSettings?.Stat.SetHostError();
                return RequestState.ErrorRequest;
            }
            finally
            {
                Dispose();
            }
        }

        private bool StateChange(RequestState val)
        {
            return (RequestState)Interlocked.Exchange(ref rstate, (long)val) == val;
        }

        public static List<Media> TasksCheckResult(List<Tasks.Task<HttpTaskRequest>> list, ILogManager LogWriter, CancellationToken token)
        {
            List<Media> medias = new();
            CancellationTokenSource cancellation = new(TimeSpan.FromMinutes(180));
            try
            {
                while (list.Count > 0)
                {
                    if (token.IsCancellationRequested)
                        break;

                    List<Media> lm = TasksCheckResult(list, LogWriter);
                    if (lm.Count > 0)
                        medias.AddRange(lm.ToArray());

                    if (cancellation.Token.IsCancellationRequested)
                    {
                        $"{nameof(TasksCheckResult)} -> Timeout clear: {list.Count}".WriteLog('i', LogWriter);
                        list.Clear();
                        break;
                    }

                    #region DEBUG job checked status
#if DEBUG
                    for (int i = list.Count - 1; i >= 0; i--)
                        $"CheckResult: ({i}) {list[i].Status}".WriteLog('v', LogWriter);
#endif
                    #endregion
                }
            }
            catch (Exception e) { e.WriteLog('!', $"{nameof(TasksCheckResult)} All -> {e.Message}", LogWriter); }
            finally
            {
                try { cancellation.Dispose(); } catch { }
            }
            return medias;
        }

        public static List<Media> TasksCheckResult(List<Tasks.Task<HttpTaskRequest>> list, ILogManager LogWriter)
        {
            List<Media> medias = new();
            try
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    try
                    {
                        HttpTaskRequest r = list[i].ConfigureAwait(false).GetAwaiter().GetResult();
                        if (r == default)
                            continue;

                        if ((r.State == RequestState.EndOk) && (r.StateCheckResult() == RequestState.EndOk))
                            medias.Add(r.media);

                        switch (r.State)
                        {
                            case RequestState.EndOk:
                            case RequestState.ErrorRequest:
                            case RequestState.ErrorEmptyRequest:
                            case RequestState.CancellRequest:
                                {
                                    r.Dispose();
                                    list.RemoveAt(i);
                                    break;
                                }
                        }
                    }
                    catch (Exception e) { e.WriteLog('!', $"{nameof(TasksCheckResult)} media -> {e.Message}", LogWriter); }
                }
            } catch (Exception e) { e.WriteLog('!', $"{nameof(TasksCheckResult)} -> {e.Message}", LogWriter); }
            return medias;
        }
    }
}
