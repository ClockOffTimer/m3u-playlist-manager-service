﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Linq;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.Process.Http
{
    public partial class HttpTaskRequest
    {
        public bool CheckValidMime(string mime, int id = -1)
        {
            bool b = checkValidMime(mime);
            if (!b)
                $"ID:{id} {nameof(CheckValidMime)} -> {mime} = {b}, {id}, {mime}".WriteLog('!', GlobalSettings?.LogWriter);
            return b;
        }
        public bool CheckIsDashMime(string mime, int id = -1)
        {
            bool b = checkIsDashMime(mime);
            if (b)
                $"ID:{id} {nameof(CheckIsDashMime)} -> {mime} = {b}, {id}, {mime}".WriteLog('!', GlobalSettings?.LogWriter);
            return b;
        }
        public bool CheckIsDashUrlPart(string url, int id = -1)
        {
            bool b = checkIsDashUrlPart(url);
            if (b)
                $"ID:{id} {nameof(CheckIsDashUrlPart)} -> {url} = {b}, {id}".WriteLog('!', GlobalSettings?.LogWriter);
            return b;
        }
        ///
        private bool checkValidMime(string mime)
        {
            return (from i in BaseConstant.videoMimes
                    where i.Equals(mime)
                    select i).FirstOrDefault() != default;
        }
        private bool checkIsDashMime(string mime)
        {
            return (from i in BaseConstant.dashMimes
                    where i.Equals(mime)
                    select i).FirstOrDefault() != default;
        }
        private bool checkIsDashUrlPart(string url)
        {
            string s = url.ToLowerInvariant();
            return (from i in BaseConstant.dashPartUri
                    where i.Contains(s)
                    select i).FirstOrDefault() != default;
        }
        private string getIsMediaExtension(string url)
        {
            string s = url.ToLowerInvariant();
            if (s.Contains("?"))
            {
                string[] str = s.Split(new char[] { '?' });
                if (str.Length > 0)
                    s = str[0].Trim();
            }
            return (from i in BaseConstant.dashPartUri
                    where s.EndsWith(i, System.StringComparison.InvariantCultureIgnoreCase)
                    select i).FirstOrDefault();
        }
    }
}
