﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;

namespace PlayListServiceData.Process.Http
{
    public class HttpClientProxyHandler : HttpClientHandler
    {
        public override bool SupportsProxy => true;
        private readonly Func<bool> IsProxy;

        public HttpClientProxyHandler(Func<string> proxyurl, Func<bool> isproxy, Action<char, string, Exception> log)
        {
            if (isproxy != default)
                IsProxy = isproxy;
            else
                IsProxy = () => { return false; };

            base.PreAuthenticate = false;
            base.UseDefaultCredentials = false;
            base.Credentials = new NetworkCredential();
            base.Proxy = new HttpClientProxy(proxyurl, isproxy, log)
            {
                Credentials = base.Credentials
            };
            base.UseProxy = IsProxy.Invoke();

            base.AllowAutoRedirect = true;
            base.UseCookies = false;
            /* base.CookieContainer = new CookieContainer(); */
            base.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
            base.MaxAutomaticRedirections = 12;
            base.MaxResponseHeadersLength = 64;
            base.MaxConnectionsPerServer = int.MaxValue;
            base.CheckCertificateRevocationList = false;
            base.ClientCertificateOptions = ClientCertificateOption.Automatic;
            base.SslProtocols = SslProtocols.Tls13 | SslProtocols.Tls12 | SslProtocols.Tls11 | SslProtocols.Tls;
        }
        ~HttpClientProxyHandler()
        {
            base.Dispose();
        }
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken token)
        {
            if (request != default)
            {
                var spm = ServicePointManager.FindServicePoint(request.RequestUri);
                spm.ConnectionLimit = 10;
                spm.ConnectionLeaseTimeout = 60 * 1000;
            }
            return await base.SendAsync(request, token).ConfigureAwait(false);
        }
    }
}
