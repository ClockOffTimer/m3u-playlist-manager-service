﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Utils;

namespace PlayListServiceData.Process.Http
{
    public enum HttpProxyType : int
    {
        None = 0,
        Http,
        Socks5
    };

    /*
     * List method:
     *
     *  [string] HttpClientDefault.GetHttp(string url, int timeout = 10)
     *  [string] HttpClientDefault.PostHttp(string url, string body, int timeout = 10, string mime = "application/json")
     *  [Task]   HttpClientDefault.DownloadFileAsync(this Uri uri, string filename)
     *  [Task]   HttpClientDefault.DownloadFileAsync(this Uri uri, FileInfo fileinfo)
     *
     *  [HttpClient]                HttpClientDefault.GetHttpClient()
     *  [Task<HttpResponseMessage>] HttpClientDefault.NewRequest(this Uri uri, HttpCompletionOption opt, CancellationToken token)
     *  [Task<HttpResponseMessage>] HttpClientDefault.NewHeaderRequest(this Uri uri, CancellationToken token)
     *  [Task<HttpResponseMessage>] HttpClientDefault.NewContentRequest(this Uri uri, CancellationToken token)
     *  [Task<Stream>]              HttpClientDefault.NewReadStreamRequest(this Uri uri)
     *
     */
    public static class HttpClientDefault
    {
        private const string JsonType = "application/json";
        private const string XmlType = "text/xml";
        public static HttpClientManager HTTP { get; } = new HttpClientManager(10, 1);

        public static async Task<HttpResponseMessage> NewRequest(this Uri uri, HttpCompletionOption opt, CancellationToken token) =>
            await HttpClientDefault.HTTP.Client.GetAsync(uri, opt, token);

        public static async Task<HttpResponseMessage> NewHeaderRequest(this Uri uri, CancellationToken token) =>
            await HttpClientDefault.NewRequest(uri, HttpCompletionOption.ResponseHeadersRead, token);

        public static async Task<HttpResponseMessage> NewContentRequest(this Uri uri, CancellationToken token) =>
            await HttpClientDefault.NewRequest(uri, HttpCompletionOption.ResponseContentRead, token);

        public static async Task<Stream> NewReadStreamRequest(this Uri uri) =>
            await HttpClientDefault.HTTP.Client.GetStreamAsync(uri);

        public static async Task<HttpResponseMessage> NewContentRequestBasicAuth(this Uri uri, string login, string pass, CancellationToken token)
        {
            AuthenticationHeaderValue auth = new(
                "Basic", Convert.ToBase64String(
                    new UTF8Encoding(false).GetBytes($"{login}:{pass}")));
            return await HttpClientDefault.NewContentRequestBasicAuth(uri, auth, token);
        }

        public static async Task<HttpResponseMessage> NewContentRequestBasicAuth(this Uri uri, AuthenticationHeaderValue auth, CancellationToken token)
        {
            HttpClient client = HttpClientDefault.HTTP.Client;
            client.DefaultRequestHeaders.Authorization = auth;
            return await client.GetAsync(uri, HttpCompletionOption.ResponseContentRead, token);
        }

        public static async Task<bool> PostContentBasicAuth(this Uri uri, AuthenticationHeaderValue auth, string body, int timeout = 10, string mime = XmlType)
        {
            using CancellationTokenSource c = new(TimeSpan.FromSeconds(timeout));
            using StringContent context = new(body, Encoding.UTF8, mime);
            HttpClient client = HttpClientDefault.HTTP.Client;
            client.DefaultRequestHeaders.Authorization = auth;
            using HttpResponseMessage r = await client.PostAsync(uri, context, c.Token)
                                                      .ConfigureAwait(false);
            return r.IsSuccessStatusCode;
        }

        public static async Task DownloadFileAsync(this Uri uri, string filename)
        {
            if (string.IsNullOrWhiteSpace(filename))
                throw new FileLoadException(nameof(filename));

            using Stream sa = await uri.NewReadStreamRequest().ConfigureAwait(false);
            using FileStream fs = new(filename, FileMode.CreateNew);
            await sa.CopyToAsync(fs).ConfigureAwait(false);
        }

        public static async Task DownloadFileAsync(this Uri uri, FileInfo fileinfo)
        {
            if ((fileinfo == null) || (fileinfo.Exists && fileinfo.IsReadOnly))
                throw new FileLoadException(nameof(fileinfo));

            using Stream sa = await uri.NewReadStreamRequest().ConfigureAwait(false);
            using FileStream fs = fileinfo.OpenWrite();
            await sa.CopyToAsync(fs).ConfigureAwait(false);
        }

        public static async Task<string> DownloadStringAsync(this string url, int timeout = 10)
        {
            using CancellationTokenSource c = new (TimeSpan.FromSeconds(timeout));
            using HttpResponseMessage r = await HttpClientDefault.NewContentRequest(new Uri(url), c.Token)
                                                                 .ConfigureAwait(false);
            if (!r.IsSuccessStatusCode)
                return default;
            return await r.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        public static async Task<string> DownloadStringAsync(this Uri uri, int timeout = 10)
        {
            using CancellationTokenSource c = new (TimeSpan.FromSeconds(timeout));
            using HttpResponseMessage r = await HttpClientDefault.NewContentRequest(uri, c.Token)
                                                                 .ConfigureAwait(false);
            if (!r.IsSuccessStatusCode)
                return default;
            return await r.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        public static async Task<string> PostContentAsync(this string url, string body, int timeout = 10, string mime = JsonType)
        {
            using CancellationTokenSource c = new (TimeSpan.FromSeconds(timeout));
            using StringContent context = new(body, Encoding.UTF8, mime);
            using HttpResponseMessage r = await HttpClientDefault.HTTP.Client.PostAsync(url, context, c.Token)
                                                                             .ConfigureAwait(false);
            if (!r.IsSuccessStatusCode)
                return default;
            return await r.Content.ReadAsStringAsync().ConfigureAwait(false);
        }

        #region short method
        public static string GetHttp(this string url, int timeout = 10)
        {
            using Task<string> t = HttpClientDefault.DownloadStringAsync(url, timeout);
            return t.Result;
        }

        public static string PostHttp(this string url, string body, int timeout = 10, string mime = JsonType)
        {
            using Task<string> t = HttpClientDefault.PostContentAsync(url, body, timeout, mime);
            return t.Result;
        }
        #endregion
    }
}
