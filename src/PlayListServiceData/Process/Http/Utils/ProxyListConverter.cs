﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */


#define REGEXTRACE_
#define ISEVENTLOG

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace PlayListServiceData.Process.Http.Utils
{
    public class ProxyListConverter : HttpClientLogEvent<char, string, Exception>
    {
        private readonly Uri uri_;
        private readonly FileInfo fsrc_,
                                  fdst_;

        public ProxyListConverter(Uri u, FileInfo dst)
        {
            uri_ = u;
            if (uri_ == default)
                throw new ArgumentNullException("source Uri");

            fdst_ = dst;
            if (fdst_ == default)
                throw new ArgumentNullException("destination file name");
            if (fdst_.Exists && fdst_.IsReadOnly)
                throw new FileLoadException($"file '{fdst_.FullName}' is read only");
        }

        public ProxyListConverter(FileInfo src, FileInfo dst)
        {
            fsrc_ = src;
            if (fsrc_ == default)
                throw new ArgumentNullException("source file name");
            if (!fsrc_.Exists)
                throw new FileNotFoundException(fsrc_.FullName);

            fdst_ = dst;
            if (fdst_ == default)
                throw new ArgumentNullException("destination file name");
            if (fsrc_.Equals(fdst_))
            {
                fdst_ = new FileInfo($"{fsrc_.FullName}.out");
                if (fdst_ == default)
                    throw new ArgumentNullException("destination file name");
            }
            if (fdst_.Exists && fdst_.IsReadOnly)
                throw new FileLoadException($"file '{fdst_.FullName}' is read only");
        }

        public ProxyListConverter(string src, string dst)
        {
            if (string.IsNullOrWhiteSpace(src))
                throw new ArgumentNullException("source file name");

            fsrc_ = new FileInfo(src);
            if ((fsrc_ == default) || !fsrc_.Exists)
                throw new FileNotFoundException(src);

            if (dst != default)
                fdst_ = new FileInfo(dst);
            else
                fdst_ = new FileInfo($"{fsrc_.FullName}.tmp");

            if (fdst_ == default)
                throw new ArgumentNullException("destination file name");
            if (fdst_.Exists && fdst_.IsReadOnly)
                throw new FileLoadException($"file '{fdst_.FullName}' is read only");
        }

        public Task<bool> UrlConvertAsync() => Task.FromResult(UrlConvert());
        public Task<bool> FileConvertAsync() => Task.FromResult(FileConvert());

        #region UrlConvert https://hidemy.name/ru/proxy-list/
        public bool FileConvert()
        {
            try
            {
                Regex r1 = new Regex(@"^((\S+)\s(\d+)).*$",
                    RegexOptions.IgnoreCase | RegexOptions.Singleline |
                    RegexOptions.IgnorePatternWhitespace | RegexOptions.CultureInvariant
                    );
                string [] ain = File.ReadAllLines(fsrc_.FullName);
                List<string> list = new List<string>();

                #region Event print
#if ISEVENTLOG
                CallEvent('+', $"{nameof(FileConvert)}: {fsrc_.FullName} -> {fsrc_.Length}",
                    default);
#endif
                #endregion

                foreach (string s in ain)
                {
                    if (s.StartsWith("HTTP"))
                        continue;

                    MatchCollection m = r1.Matches(s);
                    if ((m.Count > 0) && (m[0].Groups.Count == 4))
                        list.Add($"{m[0].Groups[2].Value}:{m[0].Groups[3].Value}");

                    #region Event Regex print
#if ISEVENTLOG && REGEXTRACE
                    for (int i = 0; i < m.Count; i++)
                        for (int n = 0; n < m[i].Groups.Count; n++)
                            CallEvent('R', $"= {i}/{n}) [{m[i].Groups[n].Value}]", default);
#endif
                    #endregion
                }

                string[] aout = list.Distinct().OrderBy(q => q).ToArray();
                if (aout.Length > 0)
                    File.WriteAllText(fdst_.FullName, string.Join(Environment.NewLine, aout));

                #region Event print
#if ISEVENTLOG
                CallEvent('=', $"{nameof(FileConvert)}: {Path.GetFileName(fsrc_.FullName)} -> {Path.GetFileName(fdst_.FullName)} ->  ({aout.Length}) End OK",
                    default);
#endif
                #endregion

                return true;
            }
            catch (Exception e) { CallEvent('!', $"{nameof(FileConvert)} -> {e.Message}", e); }
            return false;
        }
        #endregion

        #region UrlConvert https://spys.me/proxy.txt
        public bool UrlConvert()
        {
            try
            {
                string data = default;
                Regex r1 = new Regex(@"^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\:(\d+)\s(\S{2})\-(\S)\-.*$",
                    RegexOptions.IgnoreCase | RegexOptions.Singleline |
                    RegexOptions.IgnorePatternWhitespace | RegexOptions.CultureInvariant
                    );

                using (HttpClient client = new HttpClient())
                    data = client.GetStringAsync(uri_).ConfigureAwait(false).GetAwaiter().GetResult();
                if (data == default)
                    return false;


                string[] ain = data.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                List<string> list = new List<string>();

                #region Event print
#if ISEVENTLOG
                CallEvent('+', $"{nameof(FileConvert)}: {uri_.AbsoluteUri} -> {fdst_.FullName}",
                    default);
#endif
                #endregion

                foreach (string s in ain)
                {
                    MatchCollection m = r1.Matches(s.Trim());
                    if ((m.Count > 0) && (m[0].Groups.Count == 5))
                    {
                        string tag = m[0].Groups[4].Value;
                        if (tag.Equals("H") || tag.Equals("A"))
                            list.Add($"{m[0].Groups[1].Value}:{m[0].Groups[2].Value}");
                    }

                    #region Event Regex print
#if ISEVENTLOG && REGEXTRACE
                    for (int i = 0; i < m.Count; i++)
                        for (int n = 0; n < m[i].Groups.Count; n++)
                            CallEvent('R', $"= {i}/{n}) [{m[i].Groups[n].Value}]", default);
#endif
                    #endregion
                }

                string[] aout = list.Distinct().OrderBy(q => q).ToArray();
                if (aout.Length > 0)
                    File.WriteAllText(fdst_.FullName, string.Join(Environment.NewLine, aout));

                #region Event print
#if ISEVENTLOG
                CallEvent('=', $"{nameof(FileConvert)}: {uri_.AbsoluteUri} -> {Path.GetFileName(fdst_.FullName)} ->  ({aout.Length}) End OK",
                    default);
#endif
                #endregion

                return true;
            }
            catch (Exception e) { CallEvent('!', $"{nameof(UrlConvert)} -> {e.Message}", e); }
            return false;
        }
        #endregion
    }
}
