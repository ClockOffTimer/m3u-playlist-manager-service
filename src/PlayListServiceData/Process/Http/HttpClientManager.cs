﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

#define ISEVENTLOG

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Base;

namespace PlayListServiceData.Process.Http
{
    public class HttpClientManager : HttpClientLogEvent<char, string, Exception>, IDisposable
    {
        #region STATIC
        public  static bool IsDisposed { get; set; } = false;
        public  static TimeSpan TimeOut { get; private set; } = Timeout.InfiniteTimeSpan;
        private static HttpClientManager This_ = default;
        private static readonly object lockHttpClient__ = new();
        private static readonly string[,] RequestHeaders_ = new string[,]
        {
            { "User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36" },
            { "Accept-Language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7" },
            { "Accept-Encoding", "deflate,gzip;q=1.0,*;q=0.5" },
            { "sec-ch-ua", @"""Google Chrome"";v=""95"", ""Chromium"";v=""95"", ""; Not A Brand"";v=""99""" },
            { "sec-ch-ua-mobile", "?0" },
            { "sec-ch-ua-platform", "Windows" },
            { "DNT", "1" }
        };

        private static readonly Lazy<HttpClientProxyHandler> Handler_ =
            new(() => CreateHttpClientHandler_());
        private static readonly Lazy<HttpClient> Client_ =
            new(() => CreateHttpClient_());

        private static HttpClientProxyHandler CreateHttpClientHandler_()
        {
            if (HttpClientManager.This_ == default)
                return default;
            return new HttpClientProxyHandler(
                () => HttpClientManager.This_.GetProxyUrl(),
                () => HttpClientManager.This_.IsProxy(),
                LogEventBroadcast_
            );
        }
        private static HttpClient CreateHttpClient_()
        {
            lock (lockHttpClient__)
            {
                HttpClient client = new(Handler_.Value, false)
                {
                    Timeout = TimeOut
                };
                client.DefaultRequestHeaders.ConnectionClose = true;
                try
                {
                    for (int i = 0; i < (HttpClientManager.RequestHeaders_.Length / 2); i++)
                        client.DefaultRequestHeaders.Add(
                            HttpClientManager.RequestHeaders_[i, 0],
                            HttpClientManager.RequestHeaders_[i, 1]
                            );
                } catch { }
                return client;
            }
        }
        private static void LogEventBroadcast_(char c, string s, Exception e)
        {
            HttpClientManager.This_.CallEvent(c, s, e);
        }
        #endregion

        #region PUBLIC Getter/Setter
        public HttpClient Client
        {
            get
            {
                lock (lockHttpClient__)
                    return HttpClientManager.Client_.Value;
            }
        }
        public HttpClientProxyHandler ClientHandler
        {
            get => HttpClientManager.Handler_.Value;
        }
        public int RequestTimeout
        {
            set
            {
                if (Client_.IsValueCreated)
                    return;

                lock (lockHttpClient__)
                {
                    if (value > 0)
                        TimeOut = TimeSpan.FromMinutes(value);
                    else
                        TimeOut = Timeout.InfiniteTimeSpan;
                }
            }
        }
        public HttpProxyType ProxyType
        {
            get => (!ProxyEnableStatus) ? HttpProxyType.None :
                    ((ProxyHttpList.Count > 0) ? HttpProxyType.Http :
                     ((ProxySocks5List.Count > 0) ? HttpProxyType.Socks5 : HttpProxyType.None));
        }
        public SemaphoreSlim HttpLocker
        {
            get => HttpSemaphore;
        }
        public int ProxyCount
        {
            get
            {
                List<string> list = SelectSourceProxy();
                return (list == default) ? 0 : list.Count;
            }
        }
        public bool EnableProxy
        {
            get => ProxyEnableStatus;
            set => ProxyEnableStatus = value;
        }

        public async Task ProxyHttpAddRange(IEnumerable<string> list)
        {
            ProxyHttpList.Clear();
            ProxyHttpList.AddRange(list.Distinct().OrderBy(q => q));
            await CheckProxies().ConfigureAwait(false);
        }
        public async Task ProxySocks5AddRange(IEnumerable<string> list)
        {
            ProxySocks5List.Clear();
            ProxySocks5List.AddRange(list.Distinct().OrderBy(q => q));
            await CheckProxies().ConfigureAwait(false);
        }

        public readonly Queue<string> ProxyCheckQueue = new();
        public readonly List<string>  ProxyHttpList = new();
        public readonly List<string>  ProxySocks5List = new();
        public FileInfo CheckFileOut { get; set; } = default;
        #endregion

        #region PRIVATE
        private bool ProxyEnableStatus { get; set; } = true;
        private bool ProxyCheckStatus { get; set; } = false;
        private SemaphoreSlim HttpSemaphore { get; set; }
        private bool IsProxy() => ProxyEnableStatus && (SelectSourceProxy()?.Count > 0);
        private string GetProxyPrefix() =>
            (ProxyType == HttpProxyType.Http) ? "http://" :
                ((ProxyType == HttpProxyType.Socks5) ? "socks5://" : string.Empty);
        private List<string> SelectSourceProxy()
        {
            return ProxyType switch
            {
                HttpProxyType.Http => ProxyHttpList,
                HttpProxyType.Socks5 => ProxySocks5List,
                _ => new List<string>()
            };
        }
        private string GetProxyUrl()
        {
            if (ProxyCheckStatus)
            {
                if (ProxyCheckQueue.Count > 0)
                    return ProxyCheckQueue.Dequeue();
                return string.Empty;
            }

            if (!ProxyEnableStatus || (ProxyType == HttpProxyType.None))
                return string.Empty;

            string prefix = GetProxyPrefix();
            List<string> list = SelectSourceProxy();
            if ((list == default) || (list.Count == 0))
                return string.Empty;

            int idx = new Random().Next(list.Count);
            return $"{prefix}{list[idx]}";
        }
        #endregion

        public void Dispose()
        {
            if (HttpClientManager.IsDisposed)
                return;
            HttpClientManager.IsDisposed = true;
            if (HttpClientManager.Client_.IsValueCreated)
                HttpClientManager.Client_.Value.Dispose();
            if (HttpClientManager.Handler_.IsValueCreated)
                HttpClientManager.Handler_.Value.Dispose();

            SemaphoreSlim ss = HttpSemaphore;
            HttpSemaphore = default;
            if (ss != default)
                ss.Dispose();
        }

        #region CheckProxies
        public async Task CheckProxies()
        {
            await Task.Run(async () =>
            {
                SemaphoreSlim lockqueue = default;

                try
                {
                    string prefix = GetProxyPrefix();
                    List<string> clist = new(),
                                 ilist = SelectSourceProxy();
                    DateTime dt = DateTime.Now;
                    int begincount = ilist.Count;
                    lockqueue = new SemaphoreSlim(4);

                    #region Event print
#if ISEVENTLOG
                    {
                        CallEvent(
                            '*', $"{nameof(CheckProxies)}: begin check, input proxy count: {ilist.Count}",
                            default);
                        CallEvent(
                            '*', $"{nameof(CheckProxies)}: settings -> UseDefaultCredentials:{ClientHandler.UseDefaultCredentials}, PreAuthenticate:{ClientHandler.PreAuthenticate}, UseProxy:{ClientHandler.UseProxy}",
                            default);
                    }
#endif
                    #endregion

                    ProxyCheckStatus = true;
                    ProxyCheckQueue.Clear();

                    for (int i = ilist.Count - 1; i >= 0; i--)
                    {
                        #region Event print
                        {
#if ISEVENTLOG
                            CallEvent(
                                '-', $"{nameof(CheckProxies)}: {i}{((i < 10) ? " " : "")} go ->\t[{ilist[i]}]",
                                default);
#endif
                        }
                        #endregion

                        await lockqueue.WaitAsync();
                        HttpResponseMessage result = default;

                        try
                        {
                            ProxyCheckQueue.Enqueue($"{prefix}{ilist[i]}");

                            result = Client.SendAsync(
                                new HttpRequestMessage(HttpMethod.Get, "http://httpbin.org/ip"))
                                   .ConfigureAwait(false).GetAwaiter().GetResult();

                            if ((result == default) || !result.IsSuccessStatusCode)
                                continue;

                            HttpStatusCode code = result.StatusCode;
                            Uri uri = ((HttpClientProxy)Handler_.Value.Proxy).ActualUrl;
                            string s = result.Content.ReadAsStringAsync()
                                                     .ConfigureAwait(false).GetAwaiter().GetResult();
                            #region Event print
#if ISEVENTLOG
                            CallEvent('$', $"\t   Response proxy ->\t{uri}, code: {code}",
                                default);
#endif
                            #endregion

                            if (!string.IsNullOrWhiteSpace(s))
                            {
                                s = s.Replace("\n", "")
                                     .Replace("\r", "")
                                     .Replace(" ", "")
                                     .Replace("{", "")
                                     .Replace("}", "")
                                     .Replace("\"", "");

                                if (!s.Contains("origin:") || (uri == default))
                                    continue;

                                string ss = $"{uri.Host}:{uri.Port}";
                                if (!clist.Contains(ss))
                                    clist.Add(ss);

                                #region Event print
#if ISEVENTLOG
                                CallEvent(
                                    '+', $"{nameof(CheckProxies)}: {i}/{((i < 10) ? " " : "")}{code} ->\t[{ilist[i]} = {ss}] -> {s.Substring(7)}",
                                    default);
#endif
                                #endregion
                            }
                        }
                        catch (Exception e)
                        {
                            #region Event print
#if ISEVENTLOG
                            /* CallEvent('!', e.ExceptionPrint(true), e); */
                            CallEvent('!', default, e);
#endif
                            #endregion
                        }
                        finally
                        {
                            if (result != default)
                                result.Dispose();
                            lockqueue.Release();
                        }
                    }
                    if (clist.Count > 0)
                    {
                        try
                        {
                            string[] arr = clist.Distinct().ToArray();
                            ilist.Clear();
                            ilist.AddRange(arr);
                            File.WriteAllText(
                                (CheckFileOut != default) ? CheckFileOut.FullName :
                                    ((ProxyType == HttpProxyType.Socks5) ? BasePath.GetSocks5ProxyListPath() : BasePath.GetHttpProxyListPath()),
                                string.Join(Environment.NewLine, arr));
                        }
                        catch (Exception e) { CallEvent('!', $"{nameof(CheckProxies)}: -> {e.Message}", e); }
                    }

                    #region Event print
#if ISEVENTLOG
                    {
                        TimeSpan dd = DateTime.Now - dt;
                        CallEvent(
                            '=', $"{nameof(CheckProxies)}: end check, total time -> [{dd.ToString(@"hh\:mm\:ss")}], out proxy count: {clist.Count}/{ilist.Count}/{begincount}",
                            default);
                    }
#endif
                    #endregion
                }
                catch (Exception e)
                {
                    #region Event print
#if ISEVENTLOG
                    CallEvent('!', $"{nameof(CheckProxies)}: {e.Message}", e);
#endif
                    #endregion
                }
                finally
                {
                    ProxyCheckStatus = false;
                    if (lockqueue != default)
                        lockqueue.Dispose();
                }
            });
        }
        #endregion

        public HttpClientManager(int timeout = 3, int proc = 1)
        {
            HttpClientManager.This_ = this;
            RequestTimeout = timeout;
            int limit = proc switch
            {
                0 => 4,
                1 => 8,
                3 => 12,
                4 => 16,
                _ => 10
            };
            HttpSemaphore = new SemaphoreSlim(limit);
            try
            {
                if (File.Exists(BasePath.GetHttpProxyListPath()))
                {
                    string[] proxys = File.ReadAllLines(BasePath.GetHttpProxyListPath());
                    IEnumerable<string> plist = proxys.Distinct();
                    ProxyHttpList.AddRange(plist);
                }
            } catch (Exception e) { CallEvent('!', $"{nameof(HttpClientManager)}: -> {e.Message}", e); }
        }
        ~HttpClientManager()
        {
            Dispose();
        }
    }
}
