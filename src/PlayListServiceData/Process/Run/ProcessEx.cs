using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace PlayListServiceData.Process.Run
{
    public static partial class ProcessEx
    {
        public static async Task<ProcessResults> RunAsync(
            ProcessStartInfo pi, List<string> so, List<string> se, CancellationToken token, bool iscmd = false)
        {
            pi.UseShellExecute = iscmd;
            /* pi.CreateNoWindow = iscmd; */
            pi.RedirectStandardOutput = true;
            pi.RedirectStandardError = true;

            var tcs = new TaskCompletionSource<ProcessResults>();

            var proc = new System.Diagnostics.Process
            {
                StartInfo = pi,
                EnableRaisingEvents = true,
            };

            var standardOutputResults = new TaskCompletionSource<string[]>();
            proc.OutputDataReceived += (sender, args) =>
            {
                if (args.Data != null)
                    so.Add(args.Data);
                else
                    standardOutputResults.SetResult(so.ToArray());
            };

            var standardErrorResults = new TaskCompletionSource<string[]>();
            proc.ErrorDataReceived += (sender, args) =>
            {
                if (args.Data != null)
                    se.Add(args.Data);
                else
                    standardErrorResults.SetResult(se.ToArray());
            };

            var processStartTime = new TaskCompletionSource<DateTime>();

            proc.Exited += async (sender, args) =>
            {
                tcs.TrySetResult(
                    new ProcessResults(
                        proc,
                        await processStartTime.Task.ConfigureAwait(false),
                        await standardOutputResults.Task.ConfigureAwait(false),
                        await standardErrorResults.Task.ConfigureAwait(false)
                    )
                );
            };

            using (token.Register(
                () =>
                {
                    tcs.TrySetCanceled();
                    try
                    {
                        if (!proc.HasExited)
                            proc.Kill();
                    }
                    catch (InvalidOperationException) { }
                }))
            {
                token.ThrowIfCancellationRequested();

                var startTime = DateTime.Now;
                if (proc.Start() == false)
                {
                    tcs.TrySetException(new InvalidOperationException("Failed to start process"));
                }
                else
                {
                    try
                    {
                        startTime = proc.StartTime;
                    }
                    catch (Exception)
                    {
                    }
                    processStartTime.SetResult(startTime);

                    proc.BeginOutputReadLine();
                    proc.BeginErrorReadLine();
                }

                return await tcs.Task.ConfigureAwait(false);
            }
        }
    }
}
