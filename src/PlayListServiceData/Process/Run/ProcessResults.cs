﻿using System;

namespace PlayListServiceData.Process.Run
{
    public sealed class ProcessResults : IDisposable
    {
        public ProcessResults(System.Diagnostics.Process process, DateTime processStartTime, string[] standardOutput, string[] standardError)
        {
            Process = process;
            ExitCode = process.ExitCode;
            RunTime = process.ExitTime - processStartTime;
            StandardOutput = standardOutput;
            StandardError = standardError;
        }

        public System.Diagnostics.Process Process { get; }
        public int ExitCode { get; }
        public TimeSpan RunTime { get; }
        public string[] StandardOutput { get; }
        public string[] StandardError { get; }
        public void Dispose() { Process.Dispose(); }
    }
}
