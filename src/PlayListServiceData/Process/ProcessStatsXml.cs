﻿/* Copyright (c) 2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.Process
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class ProcessStatsXml
    {
        [XmlElement("items")]
        public List<ProcessStatCheck> Items { get; set; } = new();

        public ProcessStatsXml() { }
        public ProcessStatsXml(ProcessStatCheck[] stats)
        {
            if (stats != null)
                    for (int i = 0; i < stats.Length; i++)
                        if ((stats[i] != null) && (stats[i].Id != ProcessStat.Id.None))
                            Items.Add(stats[i]);
        }
    }
}
