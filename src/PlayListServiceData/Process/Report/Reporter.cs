﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Reflection;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using PlayListServiceData.DoctorDump;

namespace PlayListServiceData.Process.Report
{
    public class Reporter : IReporter
    {
        private static Application rapp = default;
        private static ClientLib rclib = default;

        public bool IsInit { get; private set; } = default;
        private IdolSoftwareDoctorDumpCrashReporterGateCrashReporterReportUploaderClient exec = default;

        public Reporter(Exception e)
        {
            if ((IsInit = Init()))
                Send_(GetExceptionDescription(e));
            Close();
        }
        public Reporter()
        {
            IsInit = Init();
        }
        ~Reporter()
        {
            Close();
        }

        public void Close()
        {
            IsInit = false;
            var ex_ = exec;
            exec = default;
            if (ex_ != default)
                try { ex_.Close(); } catch { }
        }

        public void Send(Exception e)
        {
            if (!IsInit)
                return;
            Send_(GetExceptionDescription(e));
        }

        private async void Send_(ExceptionDescription ed)
        {
            await exec.SendAnonymousReportAsync(rclib, rapp, ed);
        }
        internal bool Init()
        {
            if ((rapp == default) || (rclib == default))
            {
                try
                {
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    if (assembly == default)
                        assembly = Assembly.GetEntryAssembly();
                    Version appv = assembly.GetName().Version;
                    rapp = Reporter.GetApplication(assembly, appv);
                    rclib = Reporter.GetClientLib(appv);
                }
                catch { return false; }
            }
            try
            {
                exec = new IdolSoftwareDoctorDumpCrashReporterGateCrashReporterReportUploaderClient(
                        GetBinding(), new EndpointAddress(ReportConfig.Endpoint));
                return true;
            }
            catch { return false; }
        }
        internal CustomBinding GetBinding()
        {
            CustomBinding binding = new CustomBinding();
            TextMessageEncodingBindingElement textBinding = new TextMessageEncodingBindingElement
            {
                MessageVersion = MessageVersion.CreateVersion(EnvelopeVersion.Soap12, AddressingVersion.None),
                WriteEncoding = Encoding.UTF8,
                ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max
            };
            HttpsTransportBindingElement httpsBinding = new HttpsTransportBindingElement
            {
                AllowCookies = false,
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                TransferMode = TransferMode.Buffered
            };
            binding.Elements.Add(textBinding);
            binding.Elements.Add(httpsBinding);
            return binding;
        }
        internal static Application GetApplication(Assembly assembly, Version appv)
        {
            return new Application
            {
                ApplicationGUID = new Guid(ReportConfig.Api),
                AppName = assembly.GetName().Name,
                CompanyName = assembly.GetName().FullName,
                V1 = (ushort)appv.Major,
                V2 = (ushort)appv.Minor,
                V3 = (ushort)appv.Build,
                V4 = (ushort)appv.Revision,
                MainModule = assembly.GetName().CodeBase
            };
        }
        internal static ClientLib GetClientLib(Version appv)
        {
            return new ClientLib
            {
                V1 = (ushort)appv.Major,
                V2 = (ushort)appv.Minor,
                V3 = (ushort)appv.Build,
                V4 = (ushort)appv.Revision
            };
        }
        internal static ExceptionDescription GetExceptionDescription(Exception e)
        {
            if (e == default)
                return default;
            return new ExceptionDescription
            {
                ClrVersion = Environment.Version.ToString(),
                OS = Environment.OSVersion.VersionString,
                CrashDate = DateTime.UtcNow,
                PCID = BitConverter.ToInt32(
                    value: SHA256.Create().ComputeHash(
                        Encoding.UTF8.GetBytes(Environment.MachineName)), 0),
                Exception = GetExceptionInfo(e),
                ExceptionString = e.ToString()
            };
        }
        internal static ExceptionInfo GetExceptionInfo(Exception e)
        {
            return new ExceptionInfo
            {
                Type = e.GetType().ToString(),
                HResult = System.Runtime.InteropServices.Marshal.GetHRForException(e),
                StackTrace = e.StackTrace,
                Source = e.Source,
                Message = e.Message,
                InnerException = GetExceptionInfo(e.InnerException)
            };
        }
    }
}
