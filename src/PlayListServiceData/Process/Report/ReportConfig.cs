﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Process.Report
{
    /* https://drdump.com/SearchResult.aspx?ApplicationID=8250 */

    public static class ReportConfig
    {
        public static string Api { get { return "b2958c4a-1e3f-4bea-a380-d0ffe4d9426a"; } }
        public static string Endpoint { get { return @"https://drdump.com/Service/CrashReporterReportUploader.svc"; } }
        /* public static string AppName { get { return @"PlayListService"; } } */
        /* public static string CompanyName { get { return @"PlayListService Reporter"; } } */
    }
}
