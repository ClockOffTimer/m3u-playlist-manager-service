﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.Process.Report
{
    public interface IReporter
    {
        void Send(Exception e);
        void Close();
    }
}
