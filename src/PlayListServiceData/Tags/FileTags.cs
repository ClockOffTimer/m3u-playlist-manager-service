﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using M3U.Model;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.Tags
{
    [System.Diagnostics.DebuggerNonUserCode]
    public class FileTags : IDisposable
    {
        private static readonly Regex r1_ = new(@"^.*\s\((\d{4})\)$",
            RegexOptions.IgnoreCase |
            RegexOptions.Singleline |
            RegexOptions.CultureInvariant |
            RegexOptions.IgnorePatternWhitespace
        );
        private readonly FileInfo Info_;
        private readonly ILogManager Log_;
        private readonly Media Media_;
        private TagLib.File TFile_;

        public string Name { get; private set; } = default;
        public string TypeTags { get; private set; } = default;
        public TimeSpan Duration { get; private set; } = default;
        public Media M3uMedia { get { return Media_; }}
        public TagLib.File TFile { get { return TFile_; }}

        public FileTags(string s, ILogManager lm = default)
        {
            Log_ = lm;
            Info_ = new FileInfo(s);
            Media_ = default;
            Init();
        }
        public FileTags(string s, Media m, ILogManager lm = default)
        {
            Log_ = lm;
            Info_ = new FileInfo(s);
            Media_ = m;
            Init();
        }
        public FileTags(FileInfo fi, ILogManager lm = default)
        {
            Log_ = lm;
            Info_ = fi;
            Media_ = default;
            Init();
        }
        public FileTags(FileInfo fi, Media m, ILogManager lm = default)
        {
            Log_ = lm;
            Info_ = fi;
            Media_ = m;
            Init();
        }
        ~FileTags()
        {
            Dispose();
        }

        private void Init()
        {
            if (Info_ == default)
                throw new ArgumentNullException("FileInfo");
            if (!Info_.Exists)
                throw new FileNotFoundException(Info_.FullName);

            switch (Info_.Extension)
            {
                case ".avi":
                case ".m4a":
                case ".m4p":
                case ".m4v":
                case ".mkv":
                case ".mp3":
                case ".mp4":
                case ".mpg":
                case ".mpeg":
                case ".mpeg2":
                case ".mpeg-2":
                case ".wav":
                case ".wma":
                case ".wmv":
                case ".webm": break;
                default: throw new NotSupportedException(Info_.Extension);
            }

            TFile_ = TagLib.File.Create(Info_.FullName);
            if (TFile_ == default)
                throw new ArgumentNullException("TagLib.File");
        }

        private string FormatString(string s) => $" - {s}";
        private string FormatString(string s, bool b) => $"{(b ? ", " : "")}{s}";
        private string FormatString(string t, string s, bool b = false) => $"{(b ? ", " : "")}{t}: {s}";
        private string CheckString(string s) =>
            string.IsNullOrWhiteSpace(s) ? default :
                ((Encoding.UTF8.GetByteCount(s) == s.Length) ? s : ConvertString(s));
        private string ConvertString(string s)
        {
            try
            {
                byte[] bx = Encoding.Unicode.GetBytes(s);

                try
                {
                    UTF8Encoding utf8 = new(true, true);
                    int p = utf8.GetPreamble().Length;
                    _ = utf8.GetString(bx, p, bx.Length - p);
                    return s;
                }
                catch (DecoderFallbackException)
                {
                    try
                    {
                        byte[] b0 = Encoding.Convert(
                            Encoding.Unicode,
                            Encoding.GetEncoding("latin1"),
                            bx); // *cp1251
                        byte[] b1 = Encoding.Convert(
                            Encoding.GetEncoding("windows-1251"),
                            Encoding.UTF8,
                            b0); // *utf8
                        return Encoding.UTF8.GetString(b1);
                    }
                    catch (Exception e) { e.WriteLog($"{nameof(ConvertString)} Fallback: -> {e.Message}", Log_); }
                    return default;
                }
            }
            catch (Exception e) { e.WriteLog($"{nameof(ConvertString)}: -> {e.Message}", Log_); }
            return default;
        }

        public void Dispose()
        {
            TagLib.File tf = TFile_;
            TFile_ = default;
            if (tf != default)
                try { tf.Dispose(); } catch { }
        }

        #region Public Tag async wrapper
        public async Task<bool> ReadTagsAsync()
        {
            return await Task.FromResult(ReadTags()).ConfigureAwait(false);
        }
        public async Task<bool> WriteTagsAsync(Media m = default)
        {
            return await Task.FromResult(WriteTags(m)).ConfigureAwait(false);
        }
        public async Task<bool> ImagesFromTagsAsync(bool islocal = false, Media m = default)
        {
            return await Task.FromResult(ImagesFromTags(islocal, m)).ConfigureAwait(false);
        }
        #endregion

        #region Public Tag method
        public bool ReadTags()
        {
            try
            {
                do
                {
                    if ((TFile_ == default) || TFile_.Tag.IsEmpty)
                        break;
                    if (!Info_.Exists)
                        break;

                    StringBuilder sb = new();
                    string s;

                    if ((s = CheckString(TFile_.Tag.FirstPerformer)) != default)
                        sb.Append(s);
                    if ((s = CheckString(TFile_.Tag.FirstAlbumArtist)) != default)
                        sb.Append((sb.Length > 0) ? FormatString(s) : s);
                    if ((s = CheckString(TFile_.Tag.FirstGenre)) != default)
                        sb.Append((sb.Length > 0) ? FormatString(s) : s);
                    if ((s = CheckString(TFile_.Tag.Title)) != default)
                    {
                        string ss = s.TryStringName();
                        if (!string.IsNullOrWhiteSpace(ss))
                            sb.Append((sb.Length > 0) ? FormatString(ss) : ss);
                        else
                            sb.Append((sb.Length > 0) ? FormatString(s) : s);
                    }
                    if (TFile_.Tag.Year > 0)
                    {
                        if (!FileTags.r1_.IsMatch(sb.ToString()))
                            sb.Append($" ({TFile_.Tag.Year})");
                    }

                    TypeTags = TFile_.Tag.TagTypes.ToString();
                    Duration = TFile_.Properties.Duration;

                    if (sb.Length == 0)
                        break;

                    Name = sb.ToString()
                        .Replace("/", "")
                        .Replace("\\", "")
                        .Replace(":", "")
                        .Replace("#", "")
                        .Replace("'", "")
                        .Replace("\"", "");

                    return true;

                } while (false);
            }
            catch (Exception e) { e.WriteLog($"{nameof(ReadTags)}: {TFile_.Name} -> {e.Message}", Log_); }
            return false;
        }

        public MediaTags DetailsTags()
        {
            MediaTags mt = new();
            try
            {
                do
                {
                    if (TFile_ == default)
                    {
                        mt.Error = "Tags file instance error";
                        break;
                    }
                    if (TFile_.Tag.IsEmpty)
                    {
                        mt.Error = "Tags empty";
                        break;
                    }
                    if (!Info_.Exists)
                    {
                        mt.Error = "File not found";
                        break;
                    }

                    string s;
                    StringBuilder sb = new();
                    int[] disks = new int[] { -1, -1, -1 };

                    if (TFile_.Tag.Disc > 0)
                        disks[0] = (int)TFile_.Tag.Disc;
                    if (TFile_.Tag.DiscCount > 0)
                        disks[1] = (int)TFile_.Tag.DiscCount;
                    if (TFile_.Tag.Track > 0)
                        disks[2] = (int)TFile_.Tag.Track;
                    if ((disks[0] > 0) || (disks[1] > 0) || (disks[2] > 0))
                        mt.Disc.AddRange(disks);

                    if ((s = CheckString(TFile_.Tag.Title)) != default)
                        mt.Title = s;
                    else
                        mt.Title = Path.GetFileNameWithoutExtension(TFile_.Name)
                            .Replace('_', ' ')
                            .Replace('-', ' ')
                            .Replace('/', ' ')
                            .Replace('\\', ' ');

                    mt.Tags = TFile_.Tag.TagTypes.ToString();

                    if (TFile_.Properties.Duration != default)
                        mt.DurationTS = TFile_.Properties.Duration;

                    if ((s = CheckString(TFile_.Tag.Description)) != default)
                        sb.Append(s);
                    if ((s = CheckString(TFile_.Tag.Comment)) != default)
                        sb.Append(FormatString(s, (sb.Length > 0)));
                    if (sb.Length > 0)
                    {
                        mt.Description = sb.ToString();
                        sb.Clear();
                    }

                    if ((s = CheckString(TFile_.Tag.Publisher)) != default)
                        sb.Append(FormatString("Publisher", s));
                    if ((s = CheckString(TFile_.Tag.RemixedBy)) != default)
                        sb.Append(FormatString("RemixedBy", s, (sb.Length > 0)));
                    if (sb.Length > 0)
                    {
                        mt.Publisher = sb.ToString();
                        sb.Clear();
                    }

                    if (TFile_.Tag.DateTagged != default)
                        mt.DateDT = (DateTime)TFile_.Tag.DateTagged;
                    else if (TFile_.Tag.Year > 0)
                        mt.DateDT = new DateTime((int)TFile_.Tag.Year, 1, 1);

                    if ((s = CheckString(TFile_.Tag.Album)) != default)
                        mt.Albums.Add(s);
                    if ((TFile_.Tag.AlbumArtists != default) && (TFile_.Tag.AlbumArtists.Length > 0))
                        foreach (var ss in TFile_.Tag.AlbumArtists)
                            if (((s = CheckString(ss)) != default) && (!mt.Albums.Contains(s)))
                                mt.Albums.Add(s);

                    if ((TFile_.Tag.Performers != default) && (TFile_.Tag.Performers.Length > 0))
                        foreach (var ss in TFile_.Tag.Performers)
                            if (((s = CheckString(ss)) != default) && (!mt.Artists.Contains(s)))
                                mt.Artists.Add(s);

                    if ((TFile_.Tag.Composers != default) && (TFile_.Tag.Composers.Length > 0))
                        foreach (var ss in TFile_.Tag.Composers)
                            if (((s = CheckString(ss)) != default) && (!mt.Composers.Contains(s)))
                                mt.Composers.Add(s);

                    if ((TFile_.Tag.Genres != default) && (TFile_.Tag.Genres.Length > 0))
                        foreach (var ss in TFile_.Tag.Genres)
                            if (((s = CheckString(ss)) != default) && (!mt.Genres.Contains(s)))
                                mt.Genres.Add(s);

                    return mt;

                } while (false);
            }
            catch (Exception e) {
                e.WriteLog($"{nameof(DetailsTags)}: {TFile_.Name} -> {e.Message}", Log_);
                mt.Error = e.Message;
            }
            return mt;
        }

        public bool WriteTags(Media m = default)
        {
            try
            {
                do
                {
                    if (TFile_ == default)
                        break;
                    if (!Info_.Exists)
                        break;

                    Media media = (m != default) ? m : Media_;
                    bool b = media != default;

                    if (CheckString(TFile_.Tag.Title) == default)
                    {
                        if (b && !string.IsNullOrWhiteSpace(media.Title.InnerTitle))
                            TFile_.Tag.Title = media.Title.InnerTitle;
                        else if (b && !string.IsNullOrWhiteSpace(media.Title.RawTitle))
                            TFile_.Tag.Title = media.Title.RawTitle;
                        else
                        {
                            string s = Path.GetFileNameWithoutExtension(Info_.FullName),
                                   ss = s.TryStringName();
                            if (!string.IsNullOrWhiteSpace(ss))
                                TFile_.Tag.Title = ss;
                            else
                                TFile_.Tag.Title = s;
                        }
                    }

                    if ((TFile_.Tag.AlbumArtists == default) || (TFile_.Tag.AlbumArtists.Length == 0))
                        TFile_.Tag.AlbumArtists = new string[]
                        {
                            Path.GetFileNameWithoutExtension(Path.GetDirectoryName(Info_.FullName))
                        };

                    if ((TFile_.Tag.Performers == default) || (TFile_.Tag.Performers.Length == 0))
                        if (b && !string.IsNullOrWhiteSpace(media.Attributes.GroupTitle))
                            TFile_.Tag.Performers = new string[]
                            {
                                media.Attributes.GroupTitle
                            };

                    if ((TFile_.Tag.Performers != default) &&
                        (TFile_.Tag.Performers.Length > 0) &&
                        (TFile_.Tag.AlbumArtists != default) &&
                        (TFile_.Tag.AlbumArtists.Length > 0))
                        if (TFile_.Tag.AlbumArtists[0].Equals(TFile_.Tag.Performers[0]))
                            TFile_.Tag.Performers = new string[]
                            {
                                Info_.CreationTime.ToString("MMMM yyyy")
                            };

                    if (TFile_.Tag.Year == 0)
                        TFile_.Tag.Year = (uint)Info_.CreationTime.Year;

                    TFile_.Save();
                    return true;

                } while (false);
            }
            catch (Exception e) { e.WriteLog($"{nameof(WriteTags)}: {TFile_.Name} -> {e.Message}", Log_); }
            return false;
        }

        public bool ImagesFromTags(bool islocal = false, Media m = default)
        {
            try
            {
                do
                {
                    if (TFile_ == default)
                        break;
                    if (!Info_.Exists)
                        break;
                    if (TFile_.Tag.Pictures.Length == 0)
                        break;

                    Media media = (m != default) ? m : Media_;
                    bool x = false,
                         b = media != default;
                    string fullpath = Path.Combine(
                        Path.GetDirectoryName(Info_.FullName), Path.GetFileNameWithoutExtension(Info_.FullName));

                    for (int i = 0; i < TFile_.Tag.Pictures.Length; i++)
                    {
                        try
                        {
                            if ((TFile_.Tag.Pictures[i] == default) ||
                                (TFile_.Tag.Pictures[i].Data == default) ||
                                (TFile_.Tag.Pictures[i].Data.Data == default))
                                continue;

                            byte[] data = TFile_.Tag.Pictures[i].Data.Data;
                            string path = islocal ? Path.GetFileName($"{fullpath}_{i}.png") : $"{fullpath}_{i}.png";

                            FileInfo fi = new(path);
                            if ((fi == default) || fi.Exists)
                                continue;
                            try
                            {
                                BitmapEncoder encoder = new PngBitmapEncoder();
                                {
                                    BitmapImage img = new();
                                    img.BeginInit();
                                    img.StreamSource = new MemoryStream(data);
                                    img.EndInit();
                                    encoder.Frames.Add(BitmapFrame.Create(img));
                                }
                                using (FileStream stream = new(fi.FullName, FileMode.Create, FileAccess.Write, FileShare.Read))
                                    encoder.Save(stream);

                                if (b && string.IsNullOrWhiteSpace(media.Attributes.TvgLogo) && !string.IsNullOrWhiteSpace(media.MediaFile))
                                {
                                    int k = Path.GetExtension(media.MediaFile).Length;
                                    media.Attributes.SetTvgLogo($"{media.MediaFile.Substring(0, media.MediaFile.Length - k)}_{i}.png");
                                }
                                x = true;
                            }
                            catch (Exception e) { e.WriteLog($"{nameof(ImagesFromTags)} -> bitmap: {TFile_.Name} -> {e.Message}", Log_); break; }
                        }
                        catch (Exception e) { e.WriteLog($"{nameof(ImagesFromTags)} -> for: {TFile_.Name} -> {e.Message}", Log_); break; }
                    }
                    return x;

                } while (false);
            }
            catch (Exception e) { e.WriteLog($"{nameof(ImagesFromTags)}: {TFile_.Name} -> {e.Message}", Log_); }
            return false;
        }
        #endregion
    }
}
