﻿/* Copyright (c) 2021 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;


namespace PlayListServiceData.Tags
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class MediaTags
    {
        [XmlElement("error")]
        public string Error { get; set; } = string.Empty;
        [XmlElement("title")]
        public string Title { get; set; } = default;
        [XmlElement("desc")]
        public string Description { get; set; } = default;
        [XmlElement("copy")]
        public string Copyright { get; set; } = default;
        [XmlElement("pub")]
        public string Publisher { get; set; } = default;
        [XmlElement("tags")]
        public string Tags { get; set; } = default;
        [XmlElement("date", IsNullable = false)]
        public string Date { get; set; } = default;
        [XmlIgnore()]
        public DateTime DateDT
        {
            get { return DateDT_; }
            set
            {
                DateDT_ = value;
                Date = DateDT_.ToString(@"yyyy-MM-dd");
            }
        }
        private DateTime DateDT_ = default;

        [XmlElement("duration")]
        public string Duration { get; set; } = default;
        [XmlIgnore()]
        public TimeSpan DurationTS
        {
            get { return DurationTS_; }
            set
            {
                DurationTS_ = value;
                Duration = DurationTS_.ToString(@"hh\:mm\:ss");
            }
        }
        private TimeSpan DurationTS_ = default;

        [XmlElement("disk")]
        public List<int> Disc { get; set; } = new List<int>();
        [XmlElement("albums")]
        public List<string> Albums { get; set; } = new List<string>();
        [XmlElement("artists")]
        public List<string> Artists { get; set; } = new List<string>();
        [XmlElement("composers")]
        public List<string> Composers { get; set; } = new List<string>();
        [XmlElement("genres")]
        public List<string> Genres { get; set; } = new List<string>();
    }
}
