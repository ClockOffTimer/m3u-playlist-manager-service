﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */


namespace PlayListServiceData.Container
{
    public class EventsContainer
    {
        private readonly bool[] IsCommandOnce = new bool[6] { default, default, default, default, default, default };
        public delegate void ActionEvents(ConstContainer.State state);
        public event ActionEvents MainAction;
        public event ActionEvents ErrorAction;
        public event ActionEvents LastAction;
        public string IdName { get; set; } = default;

        public void BaseEventReset()
        {
            for (int i = 0; i < IsCommandOnce.Length; i++)
                IsCommandOnce[i] = false;
        }

#if DEBUG
        public void BaseDebugEvent(ConstContainer.State state, ConstContainer.State invoke, int num)
        {
            System.Diagnostics.Debug.WriteLine($"\tBase send event -> {state} : {IdName} -> {invoke} -> {num}");
        }
#else
        public void BaseDebugEvent(ConstContainer.State state, ConstContainer.State invoke, int num) {}
#endif

        public void BaseEventCallBackOnce(ConstContainer.State state)
        {
            switch (state)
            {
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.SaveOk:
                    {
                        if (IsCommandOnce[0])
                            break;
                        IsCommandOnce[0] = true;
                        MainAction?.Invoke(state);
                        BaseDebugEvent(state, state, 0);
                        break;
                    }
                case ConstContainer.State.DataReady:
                    {
                        if (IsCommandOnce[5])
                            break;
                        IsCommandOnce[0] = true;
                        MainAction?.Invoke(state);
                        BaseDebugEvent(state, state, 0);
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.SaveEnd:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        if (IsCommandOnce[1])
                            break;
                        IsCommandOnce[1] = true;
                        MainAction?.Invoke(state);
                        BaseDebugEvent(state, state, 1);
                        break;
                    }
                case ConstContainer.State.LoadError:
                case ConstContainer.State.SaveError:
                    {
                        if (IsCommandOnce[2])
                            break;
                        IsCommandOnce[2] = true;
                        ErrorAction?.Invoke(state);
                        BaseEventCallBackOnce(ConstContainer.State.ErrorEnd);
                        BaseDebugEvent(state, ConstContainer.State.ErrorEnd, 2);
                        break;
                    }
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        if (IsCommandOnce[3])
                            break;
                        IsCommandOnce[3] = true;
                        BaseDebugEvent(state, state, 3);
                        break;
                    }
                case ConstContainer.State.LastActionLoad:
                case ConstContainer.State.LastActionSave:
                    {
                        if (IsCommandOnce[4])
                            break;
                        IsCommandOnce[4] = true;
                        LastAction?.Invoke(state);
                        BaseDebugEvent(state, state, 4);
                        break;
                    }
            }
        }
    }
}
