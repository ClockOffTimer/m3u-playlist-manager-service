﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;

namespace PlayListServiceData.Container
{
    public class DataContainerItem<T> where T : class, new()
    {
        public bool IsComplette { get; set; } = default;
        public string SourcePath { get; private set; } = default;
        public EventContainerItem Event;
        public T Data { get; set; } = default;
        public ConstContainer.Actions Id { get; private set; } = ConstContainer.Actions.NONE;

        public DataContainerItem(string path, ConstContainer.Actions id, EventsContainer.ActionEvents dataCb)
        {
            Id = id;
            SourcePath = path;
            Event = new EventContainerItem(dataCb);
#if DEBUG
            Event.EventStatus.IdName = Path.GetFileNameWithoutExtension(path);
#endif
        }
        public override string ToString()
        {
            return $"\n\tDataContainer Item -> {Id}, {IsComplette}, {typeof(T)}\n\t\t{SourcePath}";
        }
        public void Clear()
        {
            Data = null;
            GC.SuppressFinalize(this);
        }
    }
}
