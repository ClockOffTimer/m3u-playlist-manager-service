﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using M3U.Model;

namespace PlayListServiceData.Container.Items
{
    /// <summary>
    /// using in -> ListViewEpgDataItem
    /// using in -> ListViewM3uDataItem
    /// using in -> TreeViewDataItem
    /// using in -> ViewItemT
    /// </summary>
    public interface IViewItem<T>
    {
        string IfTag { get; set; }
        string IfIcon { get; set; }
        string IfChannelName { get; set; }
        bool IfFavorite { get; set; }
        Media IfMedia { get; set; }
        T IfListItems { get; set; }
    }
}
