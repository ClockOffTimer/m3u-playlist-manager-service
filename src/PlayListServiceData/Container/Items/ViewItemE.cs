﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using M3U.Model;

namespace PlayListServiceData.Container.Items
{
    public class ViewSubItemE : INotifyPropertyChanged
    {
        #region get/set
        public DateTime Begin_ = default;
        public DateTime Begin
        {
            get { return Begin_; }
            set
            {
                if (Begin_ == value)
                    return;
                Begin_ = value;
                OnPropertyChanged(nameof(Begin));
            }
        }
        public DateTime End_ = default;
        public DateTime End
        {
            get { return End_; }
            set
            {
                if (End_ == value)
                    return;
                End_ = value;
                OnPropertyChanged(nameof(End));
            }
        }
        public string Title_ = default;
        public string Title
        {
            get { return Title_; }
            set
            {
                if (Title_ == value)
                    return;
                Title_ = value;
                OnPropertyChanged(nameof(Title));
            }
        }
        public string Desc_ = default;
        public string Desc
        {
            get { return Desc_; }
            set
            {
                if (Desc_ == value)
                    return;
                Desc_ = value;
                OnPropertyChanged(nameof(Desc));
            }
        }
        private bool IsExpanded_ = default;
        public bool IsExpanded
        {
            get { return IsExpanded_; }
            set
            {
                if (IsExpanded_ == value)
                    return;
                IsExpanded_ = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ViewSubItemE() { }
        public ViewSubItemE(string begin_, string end_, string title_, string desc_ = default)
        {
            Begin = parseTime(begin_);
            End = parseTime(end_);
            Title = title_;
            Desc = desc_;

            if ((Begin == default) || (End == default) || (Title == default))
                throw new ArgumentNullException("View SubItem EPG = Begin, End or Title is null");
        }
        private DateTime parseTime(string s)
        {
            try
            {
                if (s == null)
                    return default;
                return DateTime.ParseExact(s, "yyyyMMddHHmmss zzz", DateTimeFormatInfo.CurrentInfo);
            }
            catch { return default; }
        }
    }

    public class ViewHeaderItemE : INotifyPropertyChanged
    {
        public ObservableCollection<ViewSubItemE> IfListItems { get; set; } = new ObservableCollection<ViewSubItemE>();
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ViewHeaderItemE() { }
        public ViewHeaderItemE(DateTime dt)
        {
            DayTime = dt.ToString("dddd, dd MMMM");
        }

        #region get/set
        private string DayTime_ = default;
        public string DayTime
        {
            get { return DayTime_; }
            set
            {
                if (DayTime_ == value)
                    return;
                DayTime_ = value;
                OnPropertyChanged(nameof(DayTime));
            }
        }
        private bool IsExpanded_ = default;
        public bool IsExpanded
        {
            get { return IsExpanded_; }
            set
            {
                if (IsExpanded_ == value)
                    return;
                IsExpanded_ = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }
        #endregion

        public void Add(ViewSubItemE item)
        {
            IfListItems.Add(item);
            OnPropertyChanged(nameof(IfListItems));
        }
    }

    public class ViewItemE<T> : IViewItem<ObservableCollection<T>>, INotifyPropertyChanged
        where T : class, new()
    {
        #region get/set
        private string IfTag_ = default;
        public string IfTag
        {
            get { return IfTag_; }
            set
            {
                if (IfTag_ == value)
                    return;
                IfTag_ = value;
                OnPropertyChanged(nameof(IfTag));
                OnPropertyChanged(nameof(ChannelName));
            }
        }
        private string IfIcon_ = default;
        public string IfIcon
        {
            get { return IfIcon_; }
            set
            {
                if (IfIcon_ == value)
                    return;
                IfIcon_ = value;
                OnPropertyChanged(nameof(IfIcon));
            }
        }
        private string IfChannelName_ = default;
        public string IfChannelName
        {
            get { return IfChannelName_; }
            set
            {
                if (IfChannelName_ == value)
                    return;
                IfChannelName_ = value;
                OnPropertyChanged(nameof(IfChannelName));
                OnPropertyChanged(nameof(ChannelName));
            }
        }
        private bool IfFavorite_ = default;
        public bool IfFavorite
        {
            get { return IfFavorite_; }
            set
            {
                if (IfFavorite_ == value)
                    return;
                IfFavorite_ = value;
                OnPropertyChanged(nameof(IfFavorite));
            }
        }
        private Media IfMedia_ = default;
        public Media IfMedia
        {
            get { return IfMedia_; }
            set
            {
                if (IfMedia_ == value)
                    return;
                IfMedia_ = value;
                OnPropertyChanged(nameof(IfMedia));
            }
        }
        private bool IsExpanded_ = default;
        public bool IsExpanded
        {
            get { return IsExpanded_; }
            set
            {
                if (IsExpanded_ == value)
                    return;
                IsExpanded_ = value;
                OnPropertyChanged(nameof(IsExpanded));
            }
        }
        public string ChannelName
        {
            get { return (IfChannelName == default) ? IfTag : IfChannelName; }
        }
        public ObservableCollection<T> IfListItems { get; set; } = new ObservableCollection<T>();
        #endregion

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ViewItemE(string tag_, string icon_ = default, string name_ = default)
        {
            IfTag = tag_;
            IfIcon = icon_;
            IfFavorite = true;
            IfChannelName = name_;
        }
        public ViewItemE(ViewItemE<ViewSubItemE> item)
        {
            IfTag = item.IfTag;
            IfIcon = item.IfIcon;
            IfFavorite = item.IfFavorite;
            IfChannelName = item.IfChannelName;
            IfMedia = item.IfMedia;
        }
        public ViewItemE(ViewItemE<ViewHeaderItemE> item)
        {
            IfTag = item.IfTag;
            IfIcon = item.IfIcon;
            IfFavorite = item.IfFavorite;
            IfChannelName = item.IfChannelName;
            IfMedia = item.IfMedia;
        }
        public void Add(T item)
        {
            IfListItems.Add(item);
            OnPropertyChanged(nameof(IfListItems));
        }
        public void Add(string begin_, string end_, string title_)
        {
            try
            {
                if (typeof(T).IsAssignableFrom(typeof(ViewSubItemE)))
                    IfListItems.Add(new ViewSubItemE(begin_, end_, title_) as T);
                OnPropertyChanged(nameof(IfListItems));
            }
            catch { }
        }
    }
}
