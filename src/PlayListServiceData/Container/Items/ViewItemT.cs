﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using System.Linq;
using M3U.Model;

namespace PlayListServiceData.Container.Items
{
    public class ViewItemT : IViewItem<HashSet<string>>
    {
        public string IfTag { get; set; } = default;
        public string IfIcon { get; set; } = default;
        public string IfChannelName { get; set; } = default;
        public bool   IfFavorite { get; set; } = default;
        public Media  IfMedia { get; set; } = default;
        public HashSet<string> IfListItems { get; set; } = new HashSet<string>();

        public ViewItemT(string[] names_, string icon_ = default, bool isfav_ = false)
        {
            IfIcon = icon_;
            IfFavorite = isfav_;
            Init(names_);
        }
        private void Init(string[] names_)
        {
            try
            {
                foreach (var i in from i in names_
                                  where !IfListItems.Contains(i)
                                  select i)
                    IfListItems.Add(i);
            }
            catch { }
        }
    }
}
