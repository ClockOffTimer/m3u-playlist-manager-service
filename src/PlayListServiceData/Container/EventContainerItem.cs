﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Container
{
    public class EventContainerItem
    {
        private volatile ConstContainer.State actionState = ConstContainer.State.None;
        private volatile ConstContainer.State lastState = ConstContainer.State.None;
        public event EventsContainer.ActionEvents eventStatus = delegate { };
        public EventsContainer EventStatus { get; private set; } = new EventsContainer();
        public ConstContainer.State State
        {
            get { return actionState; }
            set
            {
                if (actionState == value)
                    return;
                actionState = value;
                eventStatus?.Invoke(actionState);
            }
        }

        public ConstContainer.State Last
        {
            get { return lastState; }
            set
            {
                if (lastState == value)
                    return;
                lastState = value;
                eventStatus?.Invoke(lastState);
            }
        }

        public EventContainerItem(EventsContainer.ActionEvents dataCb)
        {
            eventStatus += EventStatus.BaseEventCallBackOnce;
            EventStatus.MainAction += dataCb;
        }
    }
}
