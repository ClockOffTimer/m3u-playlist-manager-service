﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Container.Builder
{
    public interface IBuilders<in T1, out T2>
    {
        T2 Get();
        void Set(T1 ienumerable);
    }
}
