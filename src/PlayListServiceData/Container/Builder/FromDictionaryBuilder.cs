﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using PlayListServiceData.Base;
using PlayListServiceData.Container.Items;

namespace PlayListServiceData.Container.Builder
{
    /* Dictionary<string, ViewItemT> ->
     * to Save as BaseSerializableChannelData,
     * for Serialize and Write */
    public class FromDictionaryBuilder : IBuilders<Dictionary<string, ViewItemT>, BaseSerializableChannelData>
    {
        private Dictionary<string, ViewItemT> data = default;
        private BaseSerializableChannelData dout = default;

        ~FromDictionaryBuilder()
        {
            data = null;
        }

        private BaseSerializableChannelData Build()
        {
            try
            {
                List<BaseSerializableGroup> list = new List<BaseSerializableGroup>();
                foreach (var i in data.Distinct())
                {
                    try
                    {
                        BaseSerializableGroup g = new BaseSerializableGroup(true);
                        do
                        {
                            g.id = i.Value.IfTag;
                            g.icon = new BaseSerializableIcon(true);

                            if (string.IsNullOrWhiteSpace(i.Value.IfIcon))
                                g.icon.src = default;
                            else
                                g.icon.src = i.Value.IfIcon;

                            if (i.Value.IfListItems.Count == 0)
                            {
                                g.displayname = new string[0];
                                list.Add(g);
                                break;
                            }
                            if ((g.displayname == default) || (g.displayname.Length == 0))
                                g.displayname = i.Value.IfListItems.ToArray();
                            else if ((g.displayname != default) && (g.displayname.Length > 0))
                            {
                                var l = g.displayname.ToList();
                                l.AddRange(i.Value.IfListItems);
                                g.displayname = l.ToArray();
                            }

                        } while (false);
                        list.Add(g);
                    }
                    catch { }
                }
                if (list.Count > 0)
                {
                    BaseSerializableChannelData d = new BaseSerializableChannelData(true, list.Count);
                    d.channel = list.ToArray();
                    return d;
                }
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return default;
        }

        public void Set(Dictionary<string, ViewItemT> dict)
        {
            data = dict;
        }

        public BaseSerializableChannelData Get()
        {
            if (data == default)
                return default;
            if (dout != default)
                return dout;
            dout = Build();
            return dout;
        }
    }
}
