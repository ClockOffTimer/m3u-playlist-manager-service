﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Container.Builder;

namespace PlayListServiceData.Container
{
    public partial class DataContainer<T> where T : class, new()
    {
        private readonly List<DataContainerItem<T>> Bases = new List<DataContainerItem<T>>();
        private readonly Queue<DataContainerItem<T>> QueueLoad = new Queue<DataContainerItem<T>>();
        private CancellationToken cancellation = default;

        #region public DataContainer constructor

        public DataContainer(CancellationToken token)
        {
            cancellation = token;
        }

        public DataContainerItem<T> Open(
            string path, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id = ConstContainer.Actions.NONE)
        {
            if (!IsId(id))
                return default;
            return CheckDataContainerItem(path, dataCb, id);
        }
        public DataContainerItem<T> OpenQueueRead(
            string path, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id = ConstContainer.Actions.NONE)
        {
            if (!IsId(id))
                return default;
            var dc = CheckDataContainerItem(path, dataCb, id);
            QueueLoad.Enqueue(dc);
            return dc;
        }
        public DataContainerItem<T> OpenAndRead(
            string path, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id = ConstContainer.Actions.NONE)
        {
            if (!IsId(id))
                return default;
            var dc = CheckDataContainerItem(path, dataCb, id);
            ReadData(id);
            return dc;
        }
        public DataContainerItem<T> OpenAndWrite(
            string path, Func<T> func, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id = ConstContainer.Actions.NONE)
        {
            if (!IsId(id))
                return default;
            var dc = CheckDataContainerItem(path, dataCb, id);
            if (dc == default)
                return default;

            dc.Event.State = ConstContainer.State.SaveBegin;
            WriteData(dc, func);
            return dc;
        }
        public DataContainerItem<T> OpenAndWrite<T1>(
            string path, IBuilders<T1, T> build, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id = ConstContainer.Actions.NONE)
        {
            if (!IsId(id))
                return default;
            var dc = CheckDataContainerItem(path, dataCb, id);
            if (dc == default)
                return default;

            dc.Event.State = ConstContainer.State.SaveBegin;
            WriteData(dc, build);
            return dc;
        }
        #endregion

        #region public method

        public async void QueueStart()
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        List<DataContainerItem<T>> clr =
                            new List<DataContainerItem<T>>(QueueLoad);

                        while (QueueLoad.Count > 0)
                        {
                            var dc = QueueLoad.Dequeue();
                            if (dc == default)
                                break;
                            ReadData_(dc);
                        }

                        QueueLoad.Clear();

                        foreach (var i in clr)
                            i.Clear();
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                });
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
        }

        public void Clear()
        {
            Bases.Clear();
        }

        public void Clear(ConstContainer.Actions id)
        {
            SetBaseFromId(id, null);
        }

        public T GetBaseFromId(ConstContainer.Actions id)
        {
            var dc = GetFromId(id);
            return (dc == null) ? default : dc.Data;
        }

        public DataContainerItem<T> GetContainerItemFromId(ConstContainer.Actions id)
        {
            return GetFromId(id);
        }

        public void SetBaseFromId(ConstContainer.Actions id, T data)
        {
            var dc = GetFromId(id);
            if (dc != null)
                dc.Data = data;
        }

        #endregion

        #region private method
        private DataContainerItem<T> GetFromId(ConstContainer.Actions id)
        {
            return (from i in Bases
                    where i.Id == id
                    select i).FirstOrDefault();
        }
        private DataContainerItem<T> CheckDataContainerItem(string path, EventsContainer.ActionEvents dataCb, ConstContainer.Actions id)
        {
            var dc = GetFromId(id);
            if (dc == default)
            {
                Bases.Add(new DataContainerItem<T>(path, id, dataCb));
                return GetFromId(id);
            }
            return dc;
        }
        private bool IsId(ConstContainer.Actions id)
        {
            return id != ConstContainer.Actions.NONE;
        }
        #endregion

        #region Read Data
        private void ReadData_(DataContainerItem<T> dc)
        {
            if ((dc == null) || (dc.Event.State == ConstContainer.State.Loading) || (dc.Event.State == ConstContainer.State.Saving))
                return;
#if DEBUG
            System.Diagnostics.Debug.WriteLine(dc);
#endif

            dc.Event.EventStatus.BaseEventReset();
            dc.Event.State = ConstContainer.State.Loading;
            dc.IsComplette = false;

            try
            {
                dc.Data = null;
                ConstContainer.State bs;
                dc.Event.Last = ConstContainer.State.LastActionLoad;
                bs = dc.SourcePath.ReadXmlData<T>(cancellation, out T data);
                switch (bs)
                {
                    case ConstContainer.State.LoadDataFound:
                        {
                            if (data == null)
                            {
                                dc.Event.State = bs;
                                dc.Event.State = ConstContainer.State.LoadError;
                            }
                            else
                            {
                                dc.Data = data;
                                dc.Event.State = bs;
                                dc.Event.State = ConstContainer.State.LoadEnd;
                            }
                            break;
                        }
                    case ConstContainer.State.CancelledEnd:
                    case ConstContainer.State.LoadDataEmpty:
                    case ConstContainer.State.LoadError:
                        {
                            dc.Event.State = ConstContainer.State.LoadError;
                            break;
                        }
                }
            }
#if DEBUG
            catch (Exception e) { dc.Event.State = ConstContainer.State.LoadError; System.Diagnostics.Debug.WriteLine(e.Message); }
#else
            catch { dc.Event.State = ConstContainer.State.LoadError; }
#endif
            finally
            {
                dc.Event.Last = ConstContainer.State.LastActionLoad;
                if (dc.Event.State == ConstContainer.State.LoadError)
                    dc.Event.State = ConstContainer.State.ErrorEnd;
                else
                    dc.Event.State = ConstContainer.State.LoadEnd;
                if (dc.Data != null)
                    dc.Event.State = ConstContainer.State.DataReady;
                else
                    dc.Event.State = ConstContainer.State.ErrorEnd;
                dc.Event.EventStatus.BaseEventReset();
                dc.IsComplette = true;
            }
        }

        public async void ReadData(ConstContainer.Actions id)
        {
            var dc = GetFromId(id);
            if (dc == default)
                return;

            await ReadDataAsync(dc);
        }

        public async Task ReadDataAsync(DataContainerItem<T> dc)
        {
            try
            {
                await Task.Run(() =>
                {
                    ReadData_(dc);
                });
            }
#if DEBUG
            catch (Exception e) { dc.Event.State = ConstContainer.State.LoadError; System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { dc.Event.State = ConstContainer.State.LoadError; }
#endif
        }

        #endregion

        #region Write Data
        private void WriteData_(DataContainerItem<T> dc)
        {
            try
            {
                ConstContainer.State bs;
                dc.Event.Last = ConstContainer.State.LastActionSave;
                bs = dc.SourcePath.WriteXmlData<T>(cancellation, dc.Data);
                dc.Data = null;
                switch (bs)
                {
                    case ConstContainer.State.SaveOk:
                    case ConstContainer.State.SaveEnd:
                        {
                            dc.Event.State = bs;
                            dc.Event.State = ConstContainer.State.DataReady;
                            break;
                        }
                    case ConstContainer.State.CancelledEnd:
                    case ConstContainer.State.SaveError:
                        {
                            dc.Event.State = bs;
                            dc.Event.State = ConstContainer.State.ErrorEnd;
                            break;
                        }
                }
            }
#if DEBUG
            catch (Exception e) { dc.Event.State = ConstContainer.State.SaveError; System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { dc.Event.State = ConstContainer.State.SaveError; }
#endif
            finally
            {
                if (dc.Event.State == ConstContainer.State.SaveError)
                    dc.Event.State = ConstContainer.State.ErrorEnd;
            }
        }

        public async void WriteData(DataContainerItem<T> dc, Func<T> func)
        {
            await WriteDataAsync<int>(dc, func, null);
        }
        public async void WriteData<T1>(DataContainerItem<T> dc, IBuilders<T1, T> build)
        {
            await WriteDataAsync<T1>(dc, null, build);
        }
        public async void WriteData(DataContainerItem<T> dc)
        {
            await WriteDataAsync<int>(dc, null, null);
        }
        public async Task WriteDataAsync<T1>(DataContainerItem<T> dc, Func<T> func, IBuilders<T1, T> build)
        {
            if ((dc.Event.State == ConstContainer.State.Loading) || (dc.Event.State == ConstContainer.State.Saving))
                return;

            dc.Event.EventStatus.BaseEventReset();
            dc.Event.State = ConstContainer.State.Saving;

            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        #region Write Data Selector
                        do
                        {
                            if ((func == default) && (build == default) && (dc.Data == default))
                            {
                                dc.Event.State = ConstContainer.State.SaveError;
                                return;
                            }
                            if (dc.Data == default)
                            {
                                if (build != default)
                                    if ((dc.Data = build.Get()) != default)
                                        break;
                                if (func != default)
                                    if ((dc.Data = func.Invoke()) != default)
                                        break;

                                dc.Event.State = ConstContainer.State.SaveError;
                                return;
                            }
                        } while (false);
                        #endregion

                        WriteData_(dc);
                    }
#if DEBUG
                    catch (Exception e) { dc.Event.State = ConstContainer.State.SaveError; System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { dc.Event.State = ConstContainer.State.SaveError; }
#endif
                });
            }
#if DEBUG
            catch (Exception e) { dc.Event.State = ConstContainer.State.SaveError; System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { dc.Event.State = ConstContainer.State.SaveError; }
#endif
        }
        #endregion
    }
}
