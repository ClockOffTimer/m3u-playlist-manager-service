﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;

namespace PlayListServiceData.Container.Provider
{
    public class DataEpgItem
    {
        public string id { get; private set; } = default;
        public string ico { get; private set; } = default;
        public HashSet<string> names { get; } = new HashSet<string>();
        public bool IsFavorite { get; set; } = default;

        public DataEpgItem(string n, string i, string[] arr)
        {
            id = n;
            ico = i;
            AddRange(arr);
        }

        public void AddRange(string[] arr)
        {
            if (arr != null)
                foreach (var k in arr)
                    if (!names.Contains(k))
                        names.Add(k);
        }

        public void AddIcon(string icon)
        {
            if (string.IsNullOrWhiteSpace(ico))
                ico = icon;
        }
    }
}
