﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Threading;
using PlayListServiceData.BaseXml;

namespace PlayListServiceData.Container
{
    public static class IOContainer
    {
        internal static ConstContainer.State ReadXmlData<T1>(this string path, CancellationToken token, out T1 data) where T1 : class, new()
        {
            data = default(T1);

            try
            {
                FileInfo fxml = new(path);
                if ((fxml != null) && fxml.Exists)
                {
                    if (token.IsCancellationRequested)
                        return ConstContainer.State.CancelledEnd;

                    data = fxml.FullName.DeserializeFromFile<T1>();
                    if (data != null)
                        return ConstContainer.State.LoadDataFound;
                    else
                        return ConstContainer.State.LoadDataEmpty;
                }
                else
                    return ConstContainer.State.LoadError;
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return ConstContainer.State.LoadError;
        }

        internal static ConstContainer.State WriteXmlData<T1>(this string path, CancellationToken token, T1 data) where T1 : class, new()
        {
            try
            {
                FileInfo fxml = new(path);
                if (fxml == null)
                    return ConstContainer.State.SaveError;
                else if (fxml.Exists && fxml.IsReadOnly)
                    return ConstContainer.State.SaveError;
                else if (fxml.Exists)
                    fxml.Delete();

                if (token.IsCancellationRequested)
                    return ConstContainer.State.CancelledEnd;

                data.SerializeToFile(fxml.FullName);
                return ConstContainer.State.SaveOk;
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            return ConstContainer.State.SaveError;
        }
    }
}
