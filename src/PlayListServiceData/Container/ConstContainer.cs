﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using PlayListServiceData.Container.Items;

namespace PlayListServiceData.Container
{
    public static class ConstContainer
    {
        [Flags]
        public enum State : int
        {
            None,
            LastActionLoad,
            LastActionSave,
            Loading,
            LoadEnd,
            LoadError,
            LoadDataFound,
            LoadDataEmpty,
            Saving,
            SaveEnd,
            SaveOk,
            SaveError,
            SaveBegin,
            CancelledEnd,
            ErrorEnd,
            DataReady
        };
        [Flags]
        public enum Actions : int
        {
            LOAD_M3U = 0,           /* *.m3u */
            LOAD_GROUP_ALIAS,       /* GroupAliasPath.xml */
            LOAD_GROUP_WILD,        /* GroupWildcardChannels.xml */
            LOAD_EPG_BASE,          /* ../epg/<name?>.xml */
            LOAD_EPG_ALIAS,         /* EpgAliasPath.xml */
            LOAD_EPG_FAV,           /* EpgFavoritePath.xml */
            SAVE_M3U_AS,            /* ??.m3u */
            SAVE_GROUP_ALIAS,       /* GroupAliasPath.xml */
            SAVE_GROUP_WILD,        /* GroupWildcardChannels.xml */
            SAVE_EPG_ALIAS,         /* EpgAliasPath.xml */
            SAVE_EPG_FAV,           /* EpgFavoritePath.xml */
            NONE
        };
        public static string GetContainsIdFromName(this Dictionary<string, ViewItemT> b, string name)
        {
            if ((b.Count == 0) || string.IsNullOrWhiteSpace(name))
                return default;
            try
            {
                name = name.ToLowerInvariant();
                return (from i in b
                        from n in i.Value.IfListItems
                        where name.Contains(n)
                        select i.Key.ToUpper()).FirstOrDefault();
            } catch { }
            return default;
        }
        public static string GetStartsWithIdFromName(this Dictionary<string, ViewItemT> b, string name)
        {
            if ((b.Count == 0) || string.IsNullOrWhiteSpace(name))
                return default;
            try
            {
                string[] ss = name.Split(' ');
                if ((ss.Length > 0) && (ss[0].Length > 0))
                    name = ss[0];

                return (from i in b
                        from n in i.Value.IfListItems
                        where name.StartsWith(n, StringComparison.OrdinalIgnoreCase)
                        select i.Key.ToUpper()).FirstOrDefault();
            } catch { }
            return default;
        }
    }
}
