﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Text.RegularExpressions;
using PlayListServiceData.Tags;

namespace PlayListServiceData.Base
{
    public static partial class BasePathRename
    {
        public static string FileNamePattern =
            "^((?<Title>.+?)[\\. _-]?)" + // get title by reading from start to a field (whichever field comes first)
            "?(" +
                "([\\(]?(?<Year>(19|20)[0-9]{2})[\\)]?)|" + // year - extract for use in searching
                "([0-9]{3,4}(p|i))|" + // resolution (e.g. 1080p, 720i)
                "((?:PPV\\.)?[HPS]DTV|[. ](?:HD)?CAM[| ]|B[DR]Rip|[.| ](?:HD-?)?TS[.| ]|(?:PPV )?WEB-?DL(?: DVDRip)?|HDRip|DVDRip|CamRip|W[EB]Rip|BluRay|DvDScr|hdtv|REMUX|3D|Half-(OU|SBS)+|4K|NF|AMZN)|" + // rip type
                "(xvid|[hx]\\.?26[45]|AVC)|" + // video codec
                "(MP3|DD5\\.?1|Dual[\\- ]Audio|LiNE|DTS[-HD]+|AAC[.-]LC|AAC(?:\\.?2\\.0)?|AC3(?:\\.5\\.1)?|7\\.1|DDP5.1)|" + // audio codec
                "(REPACK|INTERNAL|PROPER)|" + // scene tags
                "\\.(mp4|m4v|mkv|mpg|avi|wmv|srt)$" + // file extensions
            ")";

        public static string TryFileRename(this string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return default;

            return new FileInfo(s).TryFileRename();
        }
        public static string TryFileRename(this FileInfo fi)
        {
            try
            {
                if ((fi == default) || !fi.Exists)
                    return default;

                FileTags tags = default;
                string dir = $"{Path.GetDirectoryName(fi.FullName)}{Path.DirectorySeparatorChar}";
                string p = Path.GetFileNameWithoutExtension(fi.FullName).TryStringName();
                if (!string.IsNullOrWhiteSpace(p))
                    return $"{dir}{p}{fi.Extension}";

                try
                {
                    tags = new FileTags(fi.FullName);
                    if (tags.ReadTags())
                        return $"{dir}{tags.Name}{fi.Extension}";
                }
                finally { if (tags != default) tags.Dispose(); }
            }
            catch { }
            return default;
        }

        public static string TryStringName(this string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                    return default;

                int x = -1;
                int[] index = { -1, -1 };

                Match m = Regex.Match(
                    name,
                    BasePathRename.FileNamePattern,
                    RegexOptions.CultureInvariant |
                    RegexOptions.IgnoreCase);

                if ((!m.Success) || (m.Groups.Count == 0))
                    return default;

                do
                {
                    if ((index[0] = m.CheckAndSet_(13)) != -1)
                        break;
                    if ((index[0] = m.CheckAndSet_(1)) != -1)
                        break;
                    if ((index[0] = m.CheckAndSet_(0)) != -1)
                        break;

                } while (false);

                if (index[0] == -1)
                    return default;

                do
                {
                    if ((index[1] = m.CheckAndSet_(14)) != -1)
                        break;
                    if ((index[1] = m.CheckAndSet_(2)) != -1)
                        break;
                    if ((index[1] = m.CheckAndSet_(3)) != -1)
                        break;

                } while (false);

                if (index[1] > -1)
                    x = m.IsInteger_(index[1]);

                if ((index[0] == 0) || (index[0] == 1) || (index[0] == 13))
                {
                    if (x == -1)
                        return m.Groups[index[0]].Value.RemoveDot_();
                    else
                        return $"{m.Groups[index[0]].Value.RemoveDot_()} ({x})";
                }
            }
            catch { }
            return default;
        }

        static internal int CheckAndSet_(this Match m, int id)
        {
            return ((m.Groups.Count > id) && !string.IsNullOrWhiteSpace(m.Groups[id].Value)) ? id : -1;
        }
        static internal int IsInteger_(this Match m, int id)
        {
            if (int.TryParse(m.Groups[id].Value, out int x))
                return x;
            return -1;
        }
        static internal string RemoveDot_(this string s)
        {
            return s.Replace('.', ' ').Trim();
        }
    }
}
