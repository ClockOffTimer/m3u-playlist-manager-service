﻿namespace PlayListServiceData.Base
{
    public class BaseEvent<T1,T2>
    {
        public delegate void DelegateEvent(T1 t1, T2 t2);
        public event DelegateEvent EventCb = delegate { };
        protected void CallEvent(T1 t1, T2 t2)
        {
            EventCb?.Invoke(t1, t2);
        }
    }
}
