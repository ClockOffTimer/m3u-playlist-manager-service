﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.IO;
using System.Net;
using System.Text;

namespace PlayListServiceData.Base
{
    public class BasePathMappingItem : IBasePathMappingItem
    {
        private readonly string _Value;
        public string Value { get { return _Value; } }
        public int? Size { get { return _Value?.Length; } }
        public bool IsEmpty { get; private set; }

        public BasePathMappingItem(string val)
        {
            _Value = val;
            IsEmpty = string.IsNullOrWhiteSpace(val);
        }
    }

    /* Path <-> Url converter */
    public class BasePathMapping : IBasePathMapping
    {
        public BasePathMappingItem UrlPathHttp { get; private set; }
        public BasePathMappingItem UrlPathHttps { get; private set; }
        public BasePathMappingItem FilePath { get; private set; }

        public BasePathMapping(string urlHttp, string urlHttps, string path)
        {
            UrlPathHttp = new BasePathMappingItem(urlHttp);
            UrlPathHttps = new BasePathMappingItem(urlHttps);
            FilePath = new BasePathMappingItem(path);
        }

        public FileInfo GetFileInfoFromUrl(string url)
        {
            return getFileInfoFromUrl(WebUtility.UrlDecode(url));
        }

        public string GetPathFromUrl(string url)
        {
            return getPathFromUrl(WebUtility.UrlDecode(url));
        }

        public string GetUrlFromPath(string path, bool ishttps = true)
        {
            return getUrlFromPath(path, ishttps);
        }

        public bool IsFileExistFromUrl(string url)
        {
            FileInfo f = getFileInfoFromUrl(WebUtility.UrlDecode(url));
            if (f == default)
                return false;
            return f.Exists;
        }

        #region private
        private string NormalizeSlash(string v, string s) => v.EndsWith("/") ? $"{v}{s}" : $"{v}/{s}";
        private string getPathFromUrl(string url)
        {
            do
            {
                string tmp;

                if (string.IsNullOrWhiteSpace(url))
                    break;
                if (!UrlPathHttp.IsEmpty && (url.Length > UrlPathHttp.Size) && url.StartsWith(UrlPathHttp.Value))
                    tmp = url.Substring((int)UrlPathHttp.Size);
                else if (!UrlPathHttps.IsEmpty && (url.Length > UrlPathHttps.Size) && url.StartsWith(UrlPathHttps.Value))
                    tmp = url.Substring((int)UrlPathHttps.Size);
                else
                    break;

                if (tmp.StartsWith("\\"))
                    tmp = tmp.Substring(1, tmp.Length);

                string name = WebUtility.UrlDecode(tmp
                        .Replace("%2F", "/")
                        .Replace("%20", " ")
                        .Replace("%23", "#")
                    ).Replace('/', '\\');

                return Path.Combine(FilePath.Value, name)
                    .Normalize(NormalizationForm.FormC);

            } while (false);
            return default;
        }

        private string getUrlFromPath(string path, bool ishttps)
        {
            do
            {
                if (string.IsNullOrWhiteSpace(path))
                    break;
                if (path.Length <= FilePath.Size)
                    break;
                if (!path.StartsWith(FilePath.Value))
                    break;
                string name = WebUtility.UrlEncode(path.Substring((int)FilePath.Size)
                    .Replace('\\', '/'))
                    .Replace("%2F", "/")
                    .Replace("+", "%20");

                if (ishttps && !UrlPathHttps.IsEmpty)
                    return NormalizeSlash(UrlPathHttps.Value, name);
                else if (!UrlPathHttp.IsEmpty)
                    return NormalizeSlash(UrlPathHttp.Value, name);
                else if (!UrlPathHttps.IsEmpty)
                    return NormalizeSlash(UrlPathHttps.Value, name);

            } while (false);
            return default;
        }

        private FileInfo getFileInfoFromUrl(string url)
        {
            string path = getPathFromUrl(url);
            if (path == default)
                return default;
            return new FileInfo(path.Normalize(NormalizationForm.FormC));
        }
        #endregion
    }
}
