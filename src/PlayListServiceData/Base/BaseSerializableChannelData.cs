﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Base
{
    #region BaseSerializableChannelData

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "tv", Namespace = "", IsNullable = false)]
    public partial class BaseSerializableChannelData
    {
        public BaseSerializableChannelData() { }
        public BaseSerializableChannelData(bool b, int cnt)
        {
            channelField = new BaseSerializableGroup[cnt];
            generatorinfonameField = default;
            generatorinfourlField = default;
        }

        private BaseSerializableGroup[] channelField;
        private string generatorinfonameField;
        private string generatorinfourlField;

        [System.Xml.Serialization.XmlElementAttribute("channel")]
        public BaseSerializableGroup[] channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute("generator-info-name")]
        public string generatorinfoname
        {
            get
            {
                return this.generatorinfonameField;
            }
            set
            {
                this.generatorinfonameField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute("generator-info-url")]
        public string generatorinfourl
        {
            get
            {
                return this.generatorinfourlField;
            }
            set
            {
                this.generatorinfourlField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "tvChannel", Namespace = "", IsNullable = false)]
    public partial class BaseSerializableGroup
    {
        public BaseSerializableGroup() { }
        public BaseSerializableGroup(bool b)
        {
            displaynameField = new string[0];
            iconField = new BaseSerializableIcon(true);
            idField = default;
        }

        private string[] displaynameField;
        private BaseSerializableIcon iconField;
        private string idField;

        [System.Xml.Serialization.XmlElementAttribute("display-name")]
        public string[] displayname
        {
            get
            {
                return this.displaynameField;
            }
            set
            {
                this.displaynameField = value;
            }
        }

        public BaseSerializableIcon icon
        {
            get
            {
                return this.iconField;
            }
            set
            {
                this.iconField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(ElementName = "tvChannelIcon", Namespace = "", IsNullable = false)]
    public partial class BaseSerializableIcon
    {
        public BaseSerializableIcon() { }
        public BaseSerializableIcon(bool b)
        {
            srcField = default;
        }

        private string srcField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string src
        {
            get
            {
                return this.srcField;
            }
            set
            {
                this.srcField = value;
            }
        }
    }
    #endregion
}
