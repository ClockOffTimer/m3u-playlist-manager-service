﻿/* Copyright (c) 2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.Base
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BaseExceptionInnerXml
    {
        [XmlElement("errortype")]
        public Type TypeException { get; set; } = default;
        [XmlElement("error")]
        public string Message { get; set; } = default;
        [XmlElement("errortrace")]
        public string StackTrace { get; set; } = default;

        public BaseExceptionInnerXml() { }
        public BaseExceptionInnerXml(Type t, string m, string s)
        {
            TypeException = t;
            Message = m;
            StackTrace = s;
        }
    }
}
