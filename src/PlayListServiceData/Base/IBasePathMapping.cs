﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.IO;

namespace PlayListServiceData.Base
{
    public interface IBasePathMappingItem
    {
        string Value { get; }
        int? Size { get; }
        bool IsEmpty { get; }
    }
    public interface IBasePathMapping
    {
        FileInfo GetFileInfoFromUrl(string url);
        string GetPathFromUrl(string url);
        string GetUrlFromPath(string path, bool ishttps = true);
        bool IsFileExistFromUrl(string url);
    }
}
