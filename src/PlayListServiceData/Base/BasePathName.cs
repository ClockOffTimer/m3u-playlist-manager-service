﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using M3U.Model;

namespace PlayListServiceData.Base
{
    public static partial class BasePathRename
    {
        public static readonly Regex __HostOrDnsPlusPortRegex =
            new(@"((http|https)://)+(([a-zA-Z0-9\._-]+\.[a-zA-Z]{2,15})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})):([0-9]{1,5})(:?\/)?",
                RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        public static string IconNameFromM3uMedia(this Media m, string group, string ext, int? id = 0)
        {
            if (m == null)
                return default;

            group = string.IsNullOrWhiteSpace(group) ? m.Attributes.GroupTitle : group;
            id = ((id == null) || (id == 0)) ? m.Attributes.ChannelNumber : id;

            if (!string.IsNullOrWhiteSpace(m.Attributes.TvgId))
                return createIconName(m.Attributes.TvgId, group, ext, id);
            else if (!string.IsNullOrWhiteSpace(m.Title.InnerTitle))
                return createIconName(m.Title.InnerTitle, group, ext, id);
            else if (!string.IsNullOrWhiteSpace(m.Title.RawTitle))
                return createIconName(m.Title.RawTitle, group, ext, id);
            else if (!string.IsNullOrWhiteSpace(m.Attributes.TvgName))
                return createIconName(m.Attributes.TvgName, group, ext, id);
            else if (!string.IsNullOrWhiteSpace(m.Attributes.GroupTitle))
                return createIconName(m.Attributes.GroupTitle, group, ext, id);
            else
                return createIconName(default, group, ext, id);
        }
        public static string IconUrlFromString(this string name, string ip, int port)
        {
            return $"http://{ip}:{port}/icon/{WebUtility.UrlEncode(name)}";
        }
        public static string EpgArchFromString(this string name, string ip, int port)
        {
            return $"http://{ip}:{port}/epg/{WebUtility.UrlEncode(Path.GetFileName(name))}";
        }
        public static string IconsPathFromDirectoryString(this string dir)
        {
            return Path.Combine(dir, "icon");
        }

        public static bool IsPlayableMediaUrl(this string url)
        {
            return url.isUrlPlayableByType(BaseConstant.extVideoUri);
        }

        public static bool IsPlayableMediaUrl(this Uri uri)
        {
            return uri.AbsoluteUri.isUrlPlayableByType(BaseConstant.extVideoUri);
        }

        public static bool IsPlayableM3uUrl(this string url)
        {
            return url.isUrlPlayableByType(BaseConstant.extM3uUri);
        }

        public static bool IsPlayableM3uUrl(this Uri uri)
        {
            return uri.AbsoluteUri.isUrlPlayableByType(BaseConstant.extM3uUri);
        }

        public static string GetUrlExtension(this string url)
        {
            var s = url.urlExtensionByType(BaseConstant.extVideoUri);
            if (s == default)
                return url.urlExtensionByType(BaseConstant.extM3uUri);
            return s;
        }

        public static string GetUrlExtension(this Uri uri)
        {
            var s = uri.AbsoluteUri.urlExtensionByType(BaseConstant.extVideoUri);
            if (s == default)
                return uri.AbsoluteUri.urlExtensionByType(BaseConstant.extM3uUri);
            return s;
        }

        public static string FileNameFromUrlString(this string s)
        {
            string str = Path.GetFileName(s);
            if (str.Contains("?"))
            {
                string[] arr = str.Split(new char[] { '?' });
                if (arr.Length > 0)
                    str = arr[0];
            }
            return str;
        }

        #region internal

        internal static bool isUrlPlayableByType(this string s, string[] arr)
        {
            if (string.IsNullOrWhiteSpace(s))
                return false;
            string name = s.FileNameFromUrlString();
            return (from i in arr
                    where name.EndsWith(i)
                    select i).FirstOrDefault() != null;
        }

        internal static string urlExtensionByType(this string s, string[] arr)
        {
            if (string.IsNullOrWhiteSpace(s))
                return default;
            string name = s.FileNameFromUrlString();
            return (from i in arr
                    where name.EndsWith(i)
                    select i).FirstOrDefault();
        }

        internal static string createIconName(string s, string g, string ext, int? id = 0)
        {
            if ((s == default) && (g == default))
            {
                DateTime d = DateTime.Now;
                return $"{d.Year}{d.Month}{d.Day}_{d.Hour}{d.Minute}{d.Second}_{id}.{ext}";
            }
            else if (s == default)
            {
                DateTime d = DateTime.Now;
                return $"{g}_{d.Year}{d.Month}{d.Day}_{d.Hour}{d.Minute}{d.Second}_{id}.{ext}";
            }
            string[] arr = s.Split(' ');
            if (arr.Length > 0)
                return $"{g}_{arr[0]}_{id}.{ext}";
            return $"{g}_{s}_{id}.{ext}";
        }

        #endregion
    }
}
