﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

namespace PlayListServiceData.Base
{
    public static class BaseConstant
    {
        public static readonly string[] videoMimes = new string[]
        {
            "application/vnd.apple.mpegurl","application/x-mpegurl", "application/ogg", "application/mp4",
            "video/mp4", "video/mp2t", "video/mpeg", "video/ogg", "video/webm", "video/quicktime", "video/x-ms-wmv",
            "video/3gpp", "video/3gpp2", "video/x-msvideo", "video/x-flv", "video/mp", "video/iso.segment", "audio/ogg",
            "audio/aac","audio/mpeg", "audio/mp4", "audio/mpegurl", "audio/x-mpegurl", "audio/aacp", "audio/x-scpls"
        };
        public static readonly string[] dashMimes = new string[]
        {
            "application/dash+xml", "video/iso.segment"
        };
        public static readonly string[] dashPartUri = new string[]
        {
            "dash/", "/dash", ".mpd"
        };
        public static readonly string[] extM3uUri = new string[]
        {
            ".m3u8", ".m3u"
        };
        public static readonly string[] extVideoUri = new string[]
        {
            ".avi", ".mpg", ".mpeg", ".mp4", ".m4p", ".m4v", ".mp2", ".mpe", ".mpv", ".m2v", ".m4v", ".svi",".3gpp", ".3gp", ".3g2", ".mov", ".qt",
            ".webm", ".wmv", ".ogg", ".ogv", ".aac", ".mp3", ".wav", ".mkv", ".mpeg-2", ".mpeg2", ".flv", ".f4v", ".f4p", ".f4a", ".f4b", ".swf",
            ".avchd", ".vob", ".ts", ".mts", ".m2ts", ".yuv", ".rm", ".rmvb", ".viv", ".asf", ".amv", ".mxf", ".roq", ".nsv"
        };
        public static readonly string[] extAudioUri = new string[]
        {
            ".mp3", ".opus", ".wav", ".aac"
        };
        public static readonly string[] extArch = new string[]
        {
            ".zip", ".gz"
        };
        public static string RepoBaseUrl() => "https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service/";
        public static string RepoBaseRawUrl() => $"{BaseConstant.RepoBaseUrl()}-/raw/main/";
        public static string RepoDownloadUrl(string s) => $"{BaseConstant.RepoBaseRawUrl()}{s}";
    }
}
