﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Reflection;

namespace PlayListServiceData.Base
{
    public static class BasePath
    {
        public static readonly string NameAudioBroadcastM3u = "AudioBroadcastLocal";
        public static readonly string NameFavoritesM3u      = "FavoritesLocal";

        public static readonly string RepoLastVersion       = $"config{Path.DirectorySeparatorChar}RepoPlayListServiceVersion.xml";
        public static readonly string GroupWildcardChannels = $"config{Path.DirectorySeparatorChar}GroupWildcardChannels.xml";
        public static readonly string GroupAliasPath        = $"config{Path.DirectorySeparatorChar}GroupAliasPath.xml";
        public static readonly string EpgAliasPath          = $"config{Path.DirectorySeparatorChar}EpgAliasPath.xml";
        public static readonly string EpgFavoritePath       = $"config{Path.DirectorySeparatorChar}EpgFavoritePath.xml";
        public static readonly string M3uDownloadPath       = $"config{Path.DirectorySeparatorChar}M3UDownloadList.xml";
        public static readonly string HistoryBasePath       = $"config{Path.DirectorySeparatorChar}HistoryBase.xml";
        public static readonly string HttpProxyList         = $"config{Path.DirectorySeparatorChar}HttpClientProxy.list";
        public static readonly string Socks5ProxyList       = $"config{Path.DirectorySeparatorChar}Socks5ClientProxy.list";
        public static readonly string TorrentTrackersList   = $"config{Path.DirectorySeparatorChar}TTrackers.list";
        public static readonly string AudioStreamSettings   = $"config{Path.DirectorySeparatorChar}AudioStreamSettingsList.xml";
        public static readonly string MainLog               = $"log{Path.DirectorySeparatorChar}PlayListService.log";
        public static readonly string AudioEngineLog        = $"log{Path.DirectorySeparatorChar}AudioEngine.log";
        public static readonly string ScanLog               = $"log{Path.DirectorySeparatorChar}ServiceScan.log";
        public static readonly string ScanLocalVideoLog     = $"log{Path.DirectorySeparatorChar}ServiceScanLocalVideo.log";
        public static readonly string ScanLocalAudioLog     = $"log{Path.DirectorySeparatorChar}ServiceScanLocalAudio.log";
        public static readonly string ScanTranslateAudioLog = $"log{Path.DirectorySeparatorChar}ServiceScanTranslateAudioLog.log";
        public static readonly string CheckLog              = $"log{Path.DirectorySeparatorChar}ServiceCheck.log";
        public static readonly string HttpLog               = $"log{Path.DirectorySeparatorChar}ServiceHttp.log";
        public static readonly string ServiceInstallLog     = "PlayListService.InstallLog";
        public static readonly string UtilInstallLog        = "InstallUtil.InstallLog";

        public static string GetEpgBasePath(this string root, string url)
        {
            return Path.Combine(root, "epg", Path.GetFileNameWithoutExtension(url));
        }
        public static string GetEpgSavePath(this string root, string url)
        {
            return Path.Combine(root, "epg", Path.GetFileName(url));
        }
        public static string GetAutoSavePath(this string root, string url)
        {
            return Path.Combine(root, "auto", Path.GetFileName(url));
        }
        public static string GetEpgFavoritePath()
        {
            return Path.Combine(GetBaseDirectoryPath(), EpgFavoritePath);
        }
        public static string GetEpgAliasPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), EpgAliasPath);
        }
        public static string GetGroupAliasPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), GroupAliasPath);
        }
        public static string GetGroupWildcardPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), GroupWildcardChannels);
        }
        public static string GetM3uDownloadPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), M3uDownloadPath);
        }
        public static string GetHistoryBasePath()
        {
            return Path.Combine(GetBaseDirectoryPath(), HistoryBasePath);
        }
        public static string GetHttpProxyListPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), HttpProxyList);
        }
        public static string GetSocks5ProxyListPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), Socks5ProxyList);
        }
        public static string GetTorrentTrackersListPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), TorrentTrackersList);
        }
        public static string GetAudioStreamSettingsPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), AudioStreamSettings);
        }
        public static string GetMainLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), MainLog);
        }
        public static string GetAudioEngineLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), AudioEngineLog);
        }
        public static string GetScanLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), ScanLog);
        }
        public static string GetScanLocalVideoLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), ScanLocalVideoLog);
        }
        public static string GetScanLocalAudioLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), ScanLocalAudioLog);
        }
        public static string GetScanTranslateAudioLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), ScanTranslateAudioLog);
        }
        public static string GetCheckLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), CheckLog);
        }
        public static string GetHttpLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), HttpLog);
        }
        public static string GetServiceInstallLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), ServiceInstallLog);
        }
        public static string GetUtilInstallLogPath()
        {
            return Path.Combine(GetBaseDirectoryPath(), UtilInstallLog);
        }
        public static string GetBaseDirectoryPath()
        {
            return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        }
    }
}