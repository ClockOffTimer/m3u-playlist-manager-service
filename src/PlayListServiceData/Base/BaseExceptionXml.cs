﻿/* Copyright (c) 2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.Base
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class BaseExceptionXml : IBaseExceptionXml
    {
        [XmlElement("errortype")]
        public Type TypeException { get; set; } = default;
        [XmlElement("error")]
        public string Message { get; set; } = default;
        [XmlElement("errortrace")]
        public string StackTrace { get; set; } = default;
        [XmlElement("errorinner")]
        public List<BaseExceptionInnerXml> InnerException { get; set; } = new();
        [XmlElement("errorcode")]
        public int Code { get; set; } = -1;

        public BaseExceptionXml() { }
        public BaseExceptionXml(Exception ex) => Copy(ex);

        public void Copy(Exception ex)
        {
            if (ex == default)
                return;

            TypeException = ex.GetType();
            Message = ex.Message;
            StackTrace = ex.StackTrace;
            Exception exc = ex.InnerException;

            while (exc != default)
            {
                InnerException.Add(new BaseExceptionInnerXml(exc.GetType(), exc.Message, exc.StackTrace));
                exc = exc.InnerException;
            }
        }

        public Exception Get()
        {
            if (TypeException == default)
                return new Exception();

            if ((InnerException != default) && (InnerException.Count > 0))
            {
                Exception exc = default;
                foreach (BaseExceptionInnerXml t in InnerException)
                {
                    if (exc == default)
                        exc = (Exception)Activator.CreateInstance(t.TypeException, new object[] { t.Message });
                    else
                        exc = (Exception)Activator.CreateInstance(t.TypeException, new object[] { t.Message, exc });
                }
                return (Exception)Activator.CreateInstance(TypeException, new object[] { Message, exc });
            }
            return (Exception)Activator.CreateInstance(TypeException, new object[] { Message });
        }
    }
}
