﻿/* Copyright (c) 2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;

namespace PlayListServiceData.Base
{
    public interface IBaseExceptionXml
    {
        List<BaseExceptionInnerXml> InnerException { get; set; }
        string Message { get; set; }
        string StackTrace { get; set; }
        Type TypeException { get; set; }

        void Copy(Exception ex);
        Exception Get();
    }
}