﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using M3U.Model;
using PlayListServiceData.Process.Http;

namespace M3U
{
    /// <summary>
    /// https://en.wikipedia.org/wiki/M3UP
    /// https://github.com/sprache/Sprache
    /// https://tools.ietf.org/html/draft-pantos-http-live-streaming-23#section-4.2
    /// http://ss-iptv.com/en/users/documents/m3u
    /// https://developer.apple.com/library/content/technotes/tn2288/_index.html
    /// https://developer.apple.com/documentation/http_live_streaming/example_playlists_for_http_live_streaming/event_playlist_construction
    /// https://tools.ietf.org/html/draft-pantos-http-live-streaming-23#page-12
    /// https://wiki.tvip.ru/en/m3u
    /// https://pastebin.com/KNYEZNun
    /// https://github.com/AlexanderSofronov/iptv.example
    /// </summary>
    public static class M3UP
    {
        public static Extm3u Parse(string content)
        {
            return new Extm3u(content);
        }

        public static Extm3u ParseBytes(byte[] byteArr)
        {
            return Parse(Encoding.Default.GetString(byteArr));
        }

        public static Extm3u ParseFromFile(string file)
        {
            return Parse(System.IO.File.ReadAllText(file));
        }

        public static async Task<Extm3u> ParseFromUrlAsync(string requestUri)
        {
            return await ParseFromUrlAsync(requestUri, HttpClientDefault.HTTP.Client);
        }

        public static async Task<Extm3u> ParseFromUrlAsync(string requestUri, HttpClient client)
        {
            var get = await client.GetAsync(requestUri);
            var content = await get.Content.ReadAsStringAsync();
            return Parse(content);
        }
    }
}