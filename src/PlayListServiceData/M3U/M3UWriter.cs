﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using M3U.Model;
using PlayListServiceData.BaseXml;
using PlayListServiceData.BaseXml.Bases;
using PlayListServiceData.Container;

namespace M3U
{
    public static class M3UWriter
    {
        private static readonly Regex __udpProxyRegex =
            new (@"^http(?s)://(\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}):(\d{2,8})/udp/(\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}):(\d{2,8})",
                RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

        public static void WriteHeader(this StreamWriter sw, string ConfigEpgFullUrl = default)
        {
            if (sw != null)
            {
                if (ConfigEpgFullUrl != null)
                    sw.WriteLine($@"#EXTM3U url-tvg=""{ConfigEpgFullUrl}""");
                else
                    sw.WriteLine(@"#EXTM3U");
            }
        }

        public static void WriteItem(this StreamWriter sw, Media m, ServiceDataItems epg = null)
        {
            if (sw == null)
                return;
            try
            {
                bool _TvgId = !string.IsNullOrEmpty(m.Attributes.TvgId),
                     _TvgName = !string.IsNullOrEmpty(m.Attributes.TvgName),
                     _TvgShift = !string.IsNullOrEmpty(m.Attributes.TvgShift),
                     _TvgLogo = !string.IsNullOrEmpty(m.Attributes.TvgLogo),
                     _InnerTitle = !string.IsNullOrEmpty(m.Title.InnerTitle),
                     _RawTitle = !string.IsNullOrEmpty(m.Title.RawTitle),
                     _GroupTitle = !string.IsNullOrEmpty(m.Attributes.GroupTitle),
                     _TvgMime = !string.IsNullOrEmpty(m.MediaFileMime),
                     _TvgExt = !string.IsNullOrEmpty(m.MediaFileExt),
                     _UrlTvg = !string.IsNullOrEmpty(m.Attributes.TvgUrl),
                     _AudioTrack = !string.IsNullOrEmpty(m.Attributes.AudioTrack),
                     _AspectRatio = !string.IsNullOrEmpty(m.Attributes.AspectRatio),
                     _TvgHttpUA = !string.IsNullOrEmpty(m.HttpUA),
                     _AutoLoad = m.Attributes?.AutoLoad > 0,
                     _Cache = m.Attributes?.Cache > 0,
                     _Deinterlace = m.Attributes?.Deinterlace > 0,
                     _Refresh = m.Attributes?.Refresh > 0,
                     _ChannelNumber = m.Attributes?.ChannelNumber > 0,
                     _ParserId = m.ParserId > 0;

                if (_TvgHttpUA)
                {
                    sw.Write($@"#EXTVLCOPT:http-user-agent={m.HttpUA}");
                    sw.Write(sw.NewLine);
                }

                sw.Write($@"#EXTINF:{m.Duration}");

                if (_ChannelNumber)
                    sw.Write($@" channel-id=""{m.Attributes.ChannelNumber}""");

                if (_AspectRatio)
                    sw.Write($@" aspect-ratio=""{m.Attributes.AspectRatio}""");

                if (_AudioTrack)
                    sw.Write($@" audio-track=""{m.Attributes.AudioTrack}""");

                if (_Deinterlace)
                    sw.Write($@" deinterlace=""{m.Attributes.Deinterlace}""");

                if (_AutoLoad)
                    sw.Write($@" m3uautoload=""{m.Attributes.AutoLoad}""");

                if (_Cache)
                    sw.Write($@" cache=""{m.Attributes.Cache}""");

                if (_Refresh)
                    sw.Write($@" refresh=""{m.Attributes.Refresh}""");

                if (_ParserId)
                    sw.Write($@" parser-id=""{m.ParserId}""");

                if (_GroupTitle)
                    sw.Write($@" group-title=""{m.Attributes.GroupTitle}""");

                if (_UrlTvg)
                    sw.Write($@" url-tvg=""{m.Attributes.TvgUrl}""");

                if (_TvgId)
                    sw.Write($@" tvg-id=""{m.Attributes.TvgId}""");
                else if (epg != default)
                {
                    string s = default;
                    if (_InnerTitle)
                        s = epg.GetStartsWithIdFromName(m.Title.InnerTitle, null, true);
                    if (_RawTitle && string.IsNullOrEmpty(s))
                        s = epg.GetStartsWithIdFromName(m.Title.RawTitle, null, true);
                    if (_TvgName && string.IsNullOrEmpty(s))
                        s = epg.GetStartsWithIdFromName(m.Attributes.TvgName, null, true);
                    if (!string.IsNullOrEmpty(s))
                        sw.Write($@" tvg-id=""{s}""");
                }

                if (_TvgShift)
                    sw.Write($@" tvg-shift=""{m.Attributes.TvgShift}""");

                if (_TvgLogo && (!m.Attributes.TvgLogo.StartsWith(@"http://127.0", StringComparison.OrdinalIgnoreCase)))
                    sw.Write($@" tvg-logo=""{m.Attributes.TvgLogo}""");

                if (_TvgMime)
                    sw.Write($@" tvg-mime=""{m.MediaFileMime}""");

                if (_TvgExt)
                    sw.Write($@" tvg-ext=""{m.MediaFileExt}""");

                if (_TvgName)
                    sw.Write($@" tvg-name=""{m.Attributes.TvgName}""");

                foreach (var i in m.Attributes.AttributeList.Where(i => i.Key.Equals("type")))
                    sw.Write($@" type=""{i.Value}""");

                if (_InnerTitle)
                    sw.Write($@",{m.Title.InnerTitle}");
                else if (_RawTitle)
                    sw.Write($@",{m.Title.RawTitle}");
                else
                    sw.Write(@",");

                sw.Write(sw.NewLine);
                sw.WriteLine($"{m.MediaFile}");
            }
            catch { }
        }

        ///

        public static bool IsM3u8FileName(this string s)
        {
            if (s.EndsWith(@".m3u8", StringComparison.OrdinalIgnoreCase) ||
                s.ToLowerInvariant().Contains(@".m3u8?"))
                return true;
            return false;
        }

        public static bool IsM3uFileName(this string s)
        {
            if (s.EndsWith(@".m3u", StringComparison.OrdinalIgnoreCase) ||
                s.ToLowerInvariant().Contains(@".m3u?"))
                return true;
            return false;
        }

        public static bool IsM3uXFileName(this string s)
        {
            return IsM3u8FileName(s) || IsM3uFileName(s);
        }

        public static bool IsUdpUrl(this string url)
        {
            if (url.StartsWith("udp://", StringComparison.InvariantCultureIgnoreCase))
                return true;
            var match = __udpProxyRegex.Match(url);
            return match.Success;
        }
    }
}
