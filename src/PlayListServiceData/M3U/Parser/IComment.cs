﻿namespace M3U.Parser
{
    /// <summary>
    /// Represents a customizable comment parser.
    /// </summary>
    public interface IComment
    {
        ///<summary>
        /// Single-line comment header.
        ///</summary>
        string Single { get; set; }

        ///<summary>
        /// Newline character preference.
        ///</summary>
        string NewLine { get; set; }

        ///<summary>
        /// Multi-line comment opener.
        ///</summary>
        string MultiOpen { get; set; }

        ///<summary>
        /// Multi-line comment closer.
        ///</summary>
        string MultiClose { get; set; }

        ///<summary>
        /// Import a single-line comment.
        ///</summary>
        Parser<string> SingleLineComment { get; }

        ///<summary>
        /// Import a multi-line comment.
        ///</summary>
        Parser<string> MultiLineComment { get; }

        ///<summary>
        /// Import a comment.
        ///</summary>
        Parser<string> AnyComment { get; }
    }
}
