﻿using System.Collections.Generic;

namespace PlayListServiceData.M3U.Model
{
    public interface IAttributes
    {
        IEnumerable<KeyValuePair<string, string>> AttributeList { get; set; }
        string TvgShift { get; set; }
        string TvgName { get; set; }
        string TvgLogo { get; set; }
        string TvgId { get; set; }
        string UrlTvg { get; set; }
        string GroupTitle { get; set; }
        string AudioTrack { get; set; }
        string AspectRatio { get; set; }
        int? AutoLoad { get; set; }
        int? Cache { get; set; }
        int? Deinterlace { get; set; }
        int? Refresh { get; set; }
        int? ChannelNumber { get; set; }

        void SetTvgShift<T>(T val);
        void SetTvgName<T>(T val);
        void SetTvgLogo<T>(T val);
        void SetTvgId<T>(T val);
        void SetUrlTvg<T>(T val);
        void SetGroupTitle<T>(T val);
        void SetAudioTrack<T>(T val);
        void SetAspectRatio<T>(T val);
        void SetM3UAutoLoad<T>(T val);
        void SetCache<T>(T val);
        void SetDeinterlace<T>(T val);
        void SetRefresh<T>(T val);
        void SetChannelNumber<T>(T val);
    }
}
