﻿using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace M3U.Model
{
    public class Attributes
    {
        public Attributes()
        {
            AttributeList = new Dictionary<string, string>();
        }

        public void Build(Media media)
        {
            if (media == default)
                return;

            media.ParserId = ConvertIntOrNull(GetOrNull("parser-id"));
            media.MediaFileExt = GetOrNull("tvg-ext");
            media.MediaFileMime = GetOrNull("tvg-mime");
        }

        internal Attributes(IEnumerable<KeyValuePair<string, string>> attributes)
        {
            AttributeList = attributes;
            GroupTitle = GetOrNull(nameof(GroupTitle))?.Normalize(NormalizationForm.FormC);
            TvgShift = GetOrNull(nameof(TvgShift));
            TvgName = GetOrNull(nameof(TvgName))?.Normalize(NormalizationForm.FormC);
            TvgLogo = GetOrNull(nameof(TvgLogo));
            AudioTrack = GetOrNull(nameof(AudioTrack));
            AspectRatio = GetOrNull(nameof(AspectRatio));
            TvgId = GetOrNull(nameof(TvgId))?.Normalize(NormalizationForm.FormC);
            TvgUrl = GetOrNull(nameof(TvgUrl));
            Cache = ConvertIntOrNull(GetOrNull(nameof(Cache)));
            Deinterlace = ConvertIntOrNull(GetOrNull(nameof(Deinterlace)));
            Refresh = ConvertIntOrNull(GetOrNull(nameof(Refresh)));

            ChannelNumber = ConvertIntOrNull(GetOrNull(nameof(ChannelNumber)));
            if (ChannelNumber == default)
                ChannelNumber = ConvertIntOrNull(GetOrNull("channel-id"));

            AutoLoad = ConvertIntOrNull(GetOrNull(nameof(AutoLoad)));
            do
            {
                if (AutoLoad != default)
                    break;
                AutoLoad = ConvertIntOrNull(GetOrNull("m3u-autoload"));
                if (AutoLoad != default)
                    break;
                AutoLoad = ConvertIntOrNull(GetOrNull("autoload"));
            } while (false);
        }

        internal string GetOrNull(string name)
        {
            return AttributeList?
                .FirstOrDefault(w => w.Key?.ToLower()?.Replace("-", string.Empty) == name.ToLower())
                .Value;
        }

        internal int? ConvertIntOrNull(string value)
        {
            if (int.TryParse(value, out int num))
                return num;

            return null;
        }

        internal void Set<T>(string name, T val)
        {
            try
            {
                var pi = GetType().GetProperty(name);
                if (pi != null)
                    pi.SetValue(this, val, null);
            }
            catch { }
        }

        public void JsSetTvgShift(string val)   => SetTvgShift<string>(val);
        public void JsSetTvgName(string val)    => SetTvgName<string>(val);
        public void JsSetTvgLogo(string val)    => SetTvgLogo<string>(val);
        public void JsSetTvgId(string val)      => SetTvgId<string>(val);
        public void JsSetTvgUrl(string val)     => SetTvgUrl<string>(val);
        public void JsSetGroupTitle(string val) => SetGroupTitle<string>(val);
        public void JsSetAudioTrack(string val) => SetAudioTrack<string>(val);
        public void JsSetAspectRatio(string val) => SetAspectRatio<string>(val);
        public void JsSetM3UAutoLoad(int val)   => SetM3UAutoLoad<int>(val);
        public void JsSetCache(int val)         => SetCache<int>(val);
        public void JsSetDeinterlace(int val)   => SetDeinterlace<int>(val);
        public void JsSetRefresh(int val)       => SetRefresh<int>(val);
        public void JsSetChannelNumber(int val) => SetChannelNumber<int>(val);

        public void SetTvgShift<T>(T val) { Set(nameof(TvgShift), val); }
        public void SetTvgName<T>(T val) { Set(nameof(TvgName), val); }
        public void SetTvgLogo<T>(T val) { Set(nameof(TvgLogo), val); }
        public void SetTvgId<T>(T val) { Set(nameof(TvgId), (val as string)?.ToLowerInvariant()); }
        public void SetTvgUrl<T>(T val) { Set(nameof(TvgUrl), val); }
        public void SetGroupTitle<T>(T val) { Set(nameof(GroupTitle), val); }
        public void SetAudioTrack<T>(T val) { Set(nameof(AudioTrack), val); }
        public void SetAspectRatio<T>(T val) { Set(nameof(AspectRatio), val); }
        public void SetM3UAutoLoad<T>(T val) { Set(nameof(AutoLoad), val); }
        public void SetCache<T>(T val) { Set(nameof(Cache), val); }
        public void SetDeinterlace<T>(T val) { Set(nameof(Deinterlace), val); }
        public void SetRefresh<T>(T val) { Set(nameof(Refresh), val); }
        public void SetChannelNumber<T>(T val) { Set(nameof(ChannelNumber), val); }

        public IEnumerable<KeyValuePair<string, string>> AttributeList { get; private set; }
        public string TvgShift { get; private set; } = default;
        public string TvgName { get; private set; } = default;
        public string TvgLogo { get; private set; } = default;
        public string TvgId { get; private set; } = default;
        public string TvgUrl { get; private set; } = default;
        public string GroupTitle { get; private set; } = default;
        public string AudioTrack { get; private set; } = default;
        public string AspectRatio { get; private set; } = default;
        public int? AutoLoad { get; private set; } = default;
        public int? Cache { get; private set; } = default;
        public int? Deinterlace { get; private set; } = default;
        public int? Refresh { get; private set; } = default;
        public int? ChannelNumber { get; private set; } = default;
    }
}