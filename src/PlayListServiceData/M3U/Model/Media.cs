﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using M3U.Parser;

namespace M3U.Model
{
    public class Media
    {
        private volatile string Cache_HostWithoutQuery__ = default;
        private volatile Uri Cache_Uri__ = default;
        public decimal Duration { get; set; }

        public Title Title { get; set; }
        public string GetShortUrl
        {
            get { SetComparableUrl(); return Cache_HostWithoutQuery__; }
        }
        public string HttpUA { get; set; } = default;
        public string MediaFileExt { get; set; } = default;
        public string MediaFileMime { get; set; } = default;
        public readonly string [] MediaFiles = new string[3] { default, default, default };
        public string MediaFile
        {
            get { return MediaFiles[0]; }
            set { MediaFiles[0] = NormalizeUrl(value); }
        }
        public int MediaFileCount
        {
            get
            {
                int n = 0;
                foreach (var i in MediaFiles)
                    n += string.IsNullOrEmpty(i) ? 0 : 1;
                return n;
            }
        }
        public Uri MediaUrl
        {
            get {
                if (Cache_Uri__ == default)
                    Cache_Uri__ = new Uri(MediaFile, UriKind.Absolute);
                return Cache_Uri__;
            }
            set {
                if (value == default)
                    return;
                MediaFile = NormalizeUrl(value.AbsoluteUri);
            }
        }
        public List<string> GetMediaFiles()
        {
            return MediaFiles.ToList();
        }
        public Media MediaFileSwap()
        {
            string s = MediaFiles[1];
            MediaFiles[1] = MediaFiles[0];
            MediaFiles[0] = s;
            return this;
        }

        public bool IsStream { get { return Duration <= 0; } }
        public int? ParserId { get; set; } = -1;
        public int GetParserId => (int)ParserId;

        public Attributes Attributes { get; set; }

        public Media(string url, string name, long duration) => Init(url, name, duration);
        public Media(Uri uri, string name, long duration) => Init(uri.AbsoluteUri, name, duration);
        public Media(string url, string name, string group, string logo) => Init(url, name, group, logo);
        public Media(Uri uri, string name, string group, string logo) => Init(uri.AbsoluteUri, name, group, logo);

        internal void Init(string url, string name, long duration)
        {
            MediaFile = NormalizeUrl(url);
            Duration = duration;
            Title = new Title(name.Normalize(NormalizationForm.FormC), default);
            Attributes = new Attributes();
        }
        internal void Init(string url, string name, string group, string logo)
        {
            Duration = -1;
            MediaFile = NormalizeUrl(url);
            Title = new Title(name.Normalize(NormalizationForm.FormC), default);
            Attributes = new Attributes();
            Attributes.SetTvgLogo(logo);
            Attributes.SetGroupTitle(group);
        }

        internal Media(string source, string httpua = default)
        {
            var media = LinesSpecification.Extinf.Parse(source);

            HttpUA = httpua;

            Duration = media.Duration;
            Title = NormalizeTitle(media.Title);
            MediaFile = NormalizeUrl(media.MediaFile);
            Attributes = media.Attributes;
            Attributes.Build(this);
        }
        internal Media(decimal duration, IEnumerable<KeyValuePair<string, string>> attributes, Title title, string mediafile)
        {
            Duration = duration;
            Title = NormalizeTitle(title);
            MediaFile = NormalizeUrl(mediafile);
            Attributes = new Attributes(attributes);
            Attributes.Build(this);
        }

        internal Title NormalizeTitle(Title title)
        {
            return new Title(
                title.InnerTitle?.Normalize(NormalizationForm.FormC),
                title.RawTitle?.Normalize(NormalizationForm.FormC));
        }
        internal string NormalizeUrl(string uri)
        {
            if (string.IsNullOrWhiteSpace(uri))
                return string.Empty;

            StringBuilder sb = new();
            bool isslash = false;
            int i = uri.IndexOf("://");
            i = (i < 0) ? 0 : (i + 3);

            for (int n = 0; n < i; n++)
                _ = sb.Append(uri.ElementAt(n));

            for (; i < uri.Length; i++)
            {
                char c = uri.ElementAt(i);
                if ((c == '/') || (c == '\\'))
                {
                    if (isslash) continue;
                    isslash = true;
                }
                else
                {
                    isslash = false;
                }
                _ = sb.Append(c);
            }
            return sb.ToString().Trim().Normalize(NormalizationForm.FormC);
        }
        public override bool Equals(object obj)
        {
            SetComparableUrl();
            if (obj is Media m)
                return m.MediaFile.StartsWith(
                    Cache_HostWithoutQuery__, StringComparison.OrdinalIgnoreCase);
            return false;
        }
        public override int GetHashCode()
        {
            SetComparableUrl();
            return Cache_HostWithoutQuery__.GetHashCode();
        }
        private void SetComparableUrl()
        {
            if (Cache_HostWithoutQuery__ == default)
            {
                string [] arr = MediaFile.Split('?');
                if ((arr != null) && (arr.Length > 0))
                    Cache_HostWithoutQuery__ = arr[0].Trim().Normalize(NormalizationForm.FormC);
                else
                    Cache_HostWithoutQuery__ = MediaFile.Trim().Normalize(NormalizationForm.FormC);
            }
        }
    }
}