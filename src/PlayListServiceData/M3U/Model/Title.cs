﻿namespace M3U.Model
{
    public class Title
    {
        public string RawTitle { get; set; }
        public string InnerTitle { get; set; }

        internal Title(string title, string innerTitle)
        {
            RawTitle = title;
            InnerTitle = innerTitle;
        }
    }
}