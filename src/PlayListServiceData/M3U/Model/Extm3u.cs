﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using M3U.Parser;

namespace M3U.Model
{
    public class Extm3u
    {
        private readonly static string M3uStartTag = "#EXTM3U";
        private readonly static string HttpUserAgentTag = "http-user-agent=";

        public string PlayListType { get; set; }
        public string HttpUA { get; set; }
        public bool HasEndList { get; set; }
        public int? TargetDuration { get; set; }
        public int? Version { get; set; }
        public int? MediaSequence { get; set; }
        public Attributes Attributes { get; set; }
        public IEnumerable<Media> Medias { get; set; }
        public IEnumerable<string> Warnings { get; set; }

        internal Extm3u(string content)
        {
            Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

            var segments = SegmentSpecification.SegmentCollection.Parse(content);

            if (segments == null || segments.Count() == 0)
                throw new Exception("The content cannot be parsed");

            if (!segments.First().StartsWith(M3uStartTag, StringComparison.CurrentCultureIgnoreCase))
                throw new Exception("The content do not has extm3u header");
            else
                Attributes = new Attributes(LinesSpecification.HeaderLine.Parse(segments.First()));

            IList<string> warnings = new List<string>();
            IList<Media> medias = new List<Media>();

            foreach (var item in segments.Skip(1))
            {
                var tag = PairsSpecification.Tag.Parse(item);

                switch (tag.Key)
                {
                    case "EXT-X-PLAYLIST-TYPEOK":
                        PlayListType = tag.Value;
                        break;

                    case "EXT-X-TARGETDURATION":
                        TargetDuration = int.Parse(tag.Value);
                        break;

                    case "EXT-X-VERSION":
                        Version = int.Parse(tag.Value);
                        break;

                    case "EXT-X-MEDIA-SEQUENCE":
                        MediaSequence = int.Parse(tag.Value);
                        break;

                    case "EXTINF":
                        try
                        {
                            medias.Add(new Media(tag.Value, HttpUA));
                            HttpUA = default;
                        }
                        catch
                        {
                            warnings.Add($"Can't parse media #{tag.Key}{(string.IsNullOrEmpty(tag.Value) ? string.Empty : ":")}{tag.Value}");
                        }
                        break;

                    case "EXT-X-ENDLIST":
                        HasEndList = true;
                        break;

                    case "EXT-VLC-OPT":
                    case "EXTVLCOPT":
                        {
                            if (tag.Value.StartsWith(HttpUserAgentTag, StringComparison.OrdinalIgnoreCase))
                                HttpUA = tag.Value.Substring(HttpUserAgentTag.Length);
                            break;
                        }

                    default:
                        warnings.Add($"Can't parse content #{tag.Key}{(string.IsNullOrEmpty(tag.Value) ? string.Empty : ":")}{tag.Value}");
                        break;
                }
            }

            Warnings = warnings.AsEnumerable();
            Medias = medias.AsEnumerable();
        }

        public List<Media> GetDuplicate(Extm3u mext)
        {
            List<Media> list = new List<Media>();
            foreach (var i in this.Medias)
            {
                var lst = (from n in mext.Medias
                            where n.Equals(i)
                            select n);
                if (lst == default)
                    continue;
                list.AddRange(lst);
            }
            return list;
        }
    }
}
