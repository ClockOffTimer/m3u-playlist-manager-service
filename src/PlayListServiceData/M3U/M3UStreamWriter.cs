﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using M3U.Model;

namespace M3U
{
    public class M3uStreamWriter
    {
        private Action<string> act = (a) => { };

        public M3uStreamWriter SetLog(Action<string> act_)
        {
            act = act_;
            return this;
        }
        public M3uStreamWriter SetLog(dynamic fun)
        {
            return SetLog(arg => fun(arg));
        }

        public void Write(string path, List<Media> list)
        {
            try
            {
                FileInfo f = new FileInfo(path);
                if ((f == default) || (f.Exists && f.IsReadOnly))
                    return;
                if (f.Exists)
                    f.Delete();

                using (FileStream s = File.Open(f.FullName, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                using (StreamWriter w = new StreamWriter(s, new UTF8Encoding(false))
                {
                    AutoFlush = true,
                    NewLine = "\n"
                })
                {
                    M3UWriter.WriteHeader(w);
                    foreach (var m in list)
                        try { M3UWriter.WriteItem(w, m); } catch (Exception e) { act.Invoke(e.Message); }
                    w.Flush(); w.Close();
                }
            } catch (Exception e) { act.Invoke(e.Message); }
        }

    }
}
