﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Collections.Generic;
using M3U.Model;

namespace M3U
{
    public class MediaComparer : IEqualityComparer<Media>
    {
        public bool Equals(Media x, Media y)
        {
            if ((x == null) || (y == null) || (x.MediaFile == null) || (y.MediaFile == null))
                return false;
            return x.Equals(y);
        }
        public int GetHashCode(Media x)
        {
            return x.GetHashCode();
        }
    }

}
