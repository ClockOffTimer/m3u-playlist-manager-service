﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Net;

namespace PlayListServiceData.UPNP
{
    [Serializable]
    public class DLNAException : Exception
    {
        static private readonly string shead = "Return code:";
        static private readonly string head = "Request return code:";
        public HttpStatusCode Code { get; private set; } = HttpStatusCode.Unused;
        public string ShortMessage { get; private set; } = default;

        public DLNAException() { }
        public DLNAException(string s)
            : base(s)
        { ShortMessage = String.Empty; }
        public DLNAException(string s, Exception e)
            : base(s, e)
        { ShortMessage = e.Message; }
        public DLNAException(HttpStatusCode code)
            : base($@"{head} ""{code}""")
        { Code = code; ShortMessage = $@"{shead} ""{code}"""; }
        public DLNAException(string s, HttpStatusCode code)
            : base($@"{head} ""{code}"", {s}")
        { Code = code; ShortMessage = $@"{shead} ""{code}"""; }
        public DLNAException(HttpStatusCode code, Exception e)
            : base($@"{head} ""{code}"", {e.Message}", e)
        { Code = code; ShortMessage = $@"{code} ""{e.Message}"""; }
    }
}
