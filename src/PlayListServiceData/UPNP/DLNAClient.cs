﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace PlayListServiceData.UPNP
{
    public class DLNAClient
    {
        private string RootUriFromDictionary() => $"http://{Device["IP"]}:{Device["PORT"]}/{Device["URL"]}";
        private string ControlUriFromDictionary() => $"http://{Device["IP"]}:{Device["PORT"]}/{Device["CONTROLURL"]}";
        private string[] PlayItemTags() => new string[] { "TrackId", "TrackURI", "TrackDuration", "RelTime", "AbsTime", "RelCount", "AbsCount" };
        private bool IsControlLoadEnable(DLNA.ActionType a) => (a == DLNA.ActionType.STOP) || (a == DLNA.ActionType.SETAV);
        private bool IsControlPlayEnable(DLNA.ActionType a) => (a == DLNA.ActionType.PLAY) || (a == DLNA.ActionType.PAUSE);
        private bool IsControlPauseEnable(DLNA.ActionType a) => (a == DLNA.ActionType.PLAY) || (a == DLNA.ActionType.NONE);
        private bool IsControlActionEnable(DLNA.ActionType a) => (a == DLNA.ActionType.PLAY) || (a == DLNA.ActionType.PAUSE) ||
                                                                  (a == DLNA.ActionType.STOP) || (a == DLNA.ActionType.SETAV) ||
                                                                   (a == DLNA.ActionType.NONE);
        private Timer timer = default;
        private DLNA.ActionType CurentAction_ = DLNA.ActionType.NONE;

        public int ClientId  { get; private set; } = -1;
        public bool IsBusy   { get; private set; } = false;
        public bool IsEnable { get; private set; } = false;
        public string ContentFeatures { get; private set; } = default;
        public DLNAPlayTrack PlayTrack { get; private set; } = new DLNAPlayTrack();
        public Dictionary<string, string> Device = new Dictionary<string, string>();
        public DLNAEvent<DLNA.ActionType, DLNAPlayTrack> EventTrack = new DLNAEvent<DLNA.ActionType, DLNAPlayTrack>();
        public DLNAEvent<string, Exception> EventError = new DLNAEvent<string, Exception>();
        public DLNAEvent<DLNA.ActionType, bool> EventAction = new DLNAEvent<DLNA.ActionType, bool>();
        public DLNA.ActionType CurentAction
        {
            get { return CurentAction_;  }
            set
            {
                if (CurentAction_ == value)
                    return;
                CurentAction_ = value;
                EventAction.EvSend(ClientId, new Tuple<DLNA.ActionType, bool>(CurentAction_, true));
            }
        }

        public DLNAClient(Tuple<string, string, string, string, string> data_)
        {
            Device.Add("IP", data_.Item1);
            Device.Add("PORT", data_.Item2);
            Device.Add("DESC", data_.Item4);
            Device.Add("ST", data_.Item5);
            if (data_.Item3.Equals("/"))
                Device.Add("URL", "");
            else
                Device.Add("URL", data_.Item3);

            timer = new Timer((a) =>
            {
                try { CmdInfo(0); }
                catch (Exception e) { EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(Timer), e)); }
            }, null, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        ~DLNAClient()
        {
            Timer t = timer;
            timer = default;
            if (t != default)
                try { t.Dispose(); } catch { }
        }

        public void SetClientId(int id) => ClientId = id;

        public Tuple<string, string, string> ToDisplay()
        {
            if (!IsEnable)
                return default;

            if (Device.ContainsKey("TITLE"))
                return new Tuple<string, string, string>(
                    this.GetDeviceString("TITLE"),
                    this.GetDeviceString("DESC"),
                    this.GetDeviceString("IP"));
            else
                return new Tuple<string, string, string>(
                    default,
                    this.GetDeviceString("DESC"),
                    this.GetDeviceString("IP"));
        }

        public DLNADeviceData ToXmlList()
        {
            if (!IsEnable)
                return default;

            DLNADeviceData data = new DLNADeviceData();
            data.Name = this.GetDeviceString("TITLE");
            data.Desc = this.GetDeviceString("DESC");
            data.Ip   = this.GetDeviceString("IP");
            data.Port = this.GetDeviceString("PORT");
            data.ActionType = CurentAction;
            data.Track = PlayTrack;
            data.Id = ClientId;
            return data;
        }

        public async Task Begin()
        {
            if (IsBusy)
            {
                EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(Begin), new DLNAException("Command busy, wait..")));
                return;
            }
            IsBusy = true;

            await Task.Run(async () => {
                try
                {
                    string s = RootUriFromDictionary().DlnaRequest(
                        "GET", default, default, default,
                        (t, a) => DlnaErrorCb(t, a),
                        (arr) => DlnaFeaturesCb(arr));
                    ParseResponce(s);
                    EventTrack.EvSend(ClientId, new Tuple<DLNA.ActionType, DLNAPlayTrack>(DLNA.ActionType.SCANEND, PlayTrack));
                    #region Debug
#if DEBUG
                    File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Begin-res.txt"), s);
#endif
                    #endregion
                    if (CurentAction == DLNA.ActionType.NONE)
                    {
                        await Task.Delay(750);
                        try { CmdInfo(0, true); CurentAction = DLNA.ActionType.SETAV; }
                        catch (System.Net.WebException) { }
                        catch (DLNAException) { }
                    }
                }
                catch (Exception e) { IsEnable = false; EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(Begin), e)); }
                finally
                {
                    IsBusy = false;
                }
            }).ConfigureAwait(false);
        }

        public async Task Command(DLNA.ActionType action, int id = -1, string url = default, string title = default)
        {
            if (IsBusy)
            {
                EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(Command), new DLNAException("Command busy, wait..")));
                return;
            }
            IsBusy = true;

            await Task.Run(async() => {

                try
                {
                    switch (action)
                    {
                        case DLNA.ActionType.PLAY:
                            {
                                do
                                {
                                    bool b = string.IsNullOrWhiteSpace(url);

                                    if (IsControlLoadEnable(CurentAction) && b)
                                        break;
                                    if (IsControlPlayEnable(CurentAction))
                                    {
                                        CmdStop(0);
                                        await Task.Delay(1750);
                                        CmdStop(0);
                                        await Task.Delay(1750);
                                    }
                                    if ((CurentAction != DLNA.ActionType.SETAV) && b)
                                    {
                                        LoadUriToDevice(url, title);
                                        await Task.Delay(1750);
                                    }

                                } while(false);

                                CmdPlay(0);
                                CurentAction = action;
                                try { timer.Change(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1)); } catch { }
                                break;
                            }
                        case DLNA.ActionType.SETAV:
                            {
                                LoadUriToDevice(url, title);
                                if (CurentAction == DLNA.ActionType.NONE)
                                    CurentAction = DLNA.ActionType.SETAV;
                                break;
                            }
                        case DLNA.ActionType.PAUSE:
                            {
                                if (!IsControlPauseEnable(CurentAction))
                                    break;

                                CmdPause(0);
                                CurentAction = action;
                                break;
                            }
                        case DLNA.ActionType.STOP:
                            {
                                if (!IsControlActionEnable(CurentAction))
                                    break;

                                CmdStop(0);
                                CurentAction = action;
                                try { timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan); } catch { }
                                break;
                            }
                        case DLNA.ActionType.GETINFO:
                            {
                                if (!IsControlActionEnable(CurentAction))
                                    break;

                                CmdInfo(0);
                                break;
                            }
                    }
                }
                catch (Exception e) { EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(Command), e)); }
                finally
                {
                    IsBusy = false;
                }
            }).ConfigureAwait(false);
        }

        private void LoadUriToDevice(string url, string title)
        {
            if (string.IsNullOrWhiteSpace(url))
                return;

            if (string.IsNullOrWhiteSpace(title))
                title = Path.GetFileNameWithoutExtension(url);
            
            ContentFeatures = url.GetUrlMime();
            CmdUploadUri(url, title, 0);
        }

        private void DlnaFeaturesCb(string [] arr)
        {
            try
            {
                if ((arr != default) && (arr.Length > 0))
                    ContentFeatures = string.Join(";", arr);
            } catch { }
        }

        private void DlnaErrorCb(string t, Exception a)
        {
            try { EventError.EvSend(ClientId, new Tuple<string, Exception>(t, a)); } catch { }
        }

        private void CmdUploadUri(string url, string title, int id)
        {
            string s = ControlUriFromDictionary().DlnaRequest(
                "POST",
                ContentFeatures,
                this.GetDeviceString("SERVICETYPE").AVTransportUri(),
                UTF8Encoding.UTF8.GetBytes(
                    url.MakeUploadFile(title, id, this.GetDeviceString("SERVICETYPE"))),
                (t, a) => DlnaErrorCb(t, a),
                (arr) => DlnaFeaturesCb(arr));
            #region Debug
#if DEBUG
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CmdUploadUri-res.txt"), s);
#endif
            #endregion
        }

        private void CmdPlay(int id)
        {
            string s = ControlUriFromDictionary().DlnaRequest(
                "POST",
                ContentFeatures,
                this.GetDeviceString("SERVICETYPE").AVTransportPlay(),
                UTF8Encoding.UTF8.GetBytes(
                    this.GetDeviceString("SERVICETYPE").MakeDlnaPlay(id)),
                (t, a) => DlnaErrorCb(t, a),
                (arr) => DlnaFeaturesCb(arr));
            #region Debug
#if DEBUG
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CmdPlay-res.txt"), s);
#endif
            #endregion
        }

        private void CmdPause(int id)
        {
            string s = ControlUriFromDictionary().DlnaRequest(
                "POST",
                ContentFeatures,
                this.GetDeviceString("SERVICETYPE").AVTransportPause(),
                UTF8Encoding.UTF8.GetBytes(
                    this.GetDeviceString("SERVICETYPE").MakeDlnaPause(id)),
                (t, a) => DlnaErrorCb(t, a),
                (arr) => DlnaFeaturesCb(arr));
            #region Debug
#if DEBUG
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CmdPause-res.txt"), s);
#endif
            #endregion
        }

        private void CmdStop(int id)
        {
            string s = ControlUriFromDictionary().DlnaRequest(
                "POST",
                ContentFeatures,
                this.GetDeviceString("SERVICETYPE").AVTransportStop(),
                UTF8Encoding.UTF8.GetBytes(
                    this.GetDeviceString("SERVICETYPE").MakeDlnaStop(id)),
                (t, a) => DlnaErrorCb(t, a),
                (arr) => DlnaFeaturesCb(arr));
            #region Debug
#if DEBUG
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CmdStop-res.txt"), s);
#endif
            #endregion
        }

        private void CmdInfo(int id, bool isthrow = false)
        {
            string s = ControlUriFromDictionary().DlnaRequest(
                "POST",
                ContentFeatures,
                this.GetDeviceString("SERVICETYPE").AVTransportInfo(),
                UTF8Encoding.UTF8.GetBytes(
                    this.GetDeviceString("SERVICETYPE").MakeDlnaPlayPosition(id)),
                (t, a) => {
                    DlnaErrorCb(t, a);
                    if (isthrow)
                        throw a;
                },
                (arr) => DlnaFeaturesCb(arr));
            ParsePlayTrack(s);
            EventTrack.EvSend(ClientId, new Tuple<DLNA.ActionType, DLNAPlayTrack>(CurentAction, PlayTrack));
            #region Debug
#if DEBUG
            File.WriteAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CmdInfo-res.txt"), s);
#endif
            #endregion
        }

        private void ParseResponce(string src)
        {
            if (src == default)
                return;
            try
            {
                XDocument xdoc = XDocument.Parse(src);
                try
                {
                    XElement el = xdoc.Descendants().Where(r => r.Name.LocalName == "friendlyName").FirstOrDefault();
                    if ((el != default) && (el.Value != default))
                        Device.Add("TITLE", el.Value);
                    else
                    {
                        var s = this.GetDeviceString("DESC").Split(' ');
                        Device.Add("TITLE", (s.Length > 0) ? s[0] : this.GetDeviceString("DESC"));
                    }
                } catch { }
                foreach (XElement ele in xdoc.Descendants().Where(r => r.Name.LocalName == "service"))
                {
                    var dic = new Dictionary<string, string>();
                    foreach (XElement el in ele.Elements())
                        try { dic.Add(el.Name.LocalName.ToUpperInvariant(), el.Value); } catch { }
                    if (dic.Count > 0)
                        if (dic.ContainsKey("SERVICETYPE"))
                            if (dic["SERVICETYPE"].Contains("AVTransport"))
                            {
                                foreach (var i in dic)
                                    if (!Device.ContainsKey(i.Key))
                                        Device.Add(i.Key, i.Value);
                                IsEnable = true;
                                break;
                            }
                }
            }
            catch (Exception e) { IsEnable = false; EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(ParseResponce), e)); }
        }
        private void ParsePlayTrack(string src)
        {
            if (src == default)
                return;
            try
            {
                string[] tags = PlayItemTags();
                string[] arr = new string[tags.Length];
                XDocument xdoc = XDocument.Parse(src);

                for (int i = 0; i < tags.Length; i++)
                {
                    XElement el = xdoc.Descendants().Where(r => r.Name.LocalName == tags[i]).FirstOrDefault();
                    if ((el != default) && (el.Value != default))
                        arr[i] = el.Value;
                    else
                        arr[i] = default;
                }
                PlayTrack.Clear();
                PlayTrack.Parse(arr);
            }
            catch (Exception e) { IsEnable = false; EventError.EvSend(ClientId, new Tuple<string, Exception>(nameof(ParsePlayTrack), e)); }
        }
    }
}
