﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.BaseXml;

namespace PlayListServiceData.UPNP
{
    public class DLNA
    {
        public enum ActionType : int
        {
            NONE,
            PLAY,
            PAUSE,
            STOP,
            SETAV,
            GETINFO,
            SCANBEGIN,
            SCANEND,
            SSDP
        };

        private static readonly string xmlDefault = @"﻿<?xml version=""1.0"" encoding=""utf-8""?><root xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema""/><Devices></Devices>";

        private Timer timer = default;
        private readonly SSDP ssdp = SSDP.Create();
        private readonly List<DLNAClient> clients = new();
        private DLNAClient selected = default;

        public bool IsScanWait   { get { return ssdp.IsScanWait; }}
        public int UdpPortSSDP   { get { return ssdp.UdpPortSSDP; }}
        public int UdpPortServer { get { return ssdp.UdpPortServer; } set { ssdp.UdpPortServer = value; }}
        public int WebPortServer { get { return ssdp.WebPortServer; } set { ssdp.WebPortServer = value; }}

        public string UdpIpSSDP   { get { return ssdp.UdpIpSSDP; } set { ssdp.UdpIpSSDP = value; }}
        public string UdpIpServer { get { return ssdp.UdpIpServer; } set { ssdp.UdpIpServer = value; }}
        public string WebServer   { get { return ssdp.WebServer; } set { ssdp.WebServer = value; }}

        public DLNAEvent<ActionType, DLNAPlayTrack> EventTrack = new();
        public DLNAEvent<ActionType, bool> EventAction = new();
        public DLNAEvent<string, Exception> EventError = new();

        public DLNA()
        {
            ssdp.EventError.EvCb  += ErrorEventReceive;
            ssdp.EventDevice.EvCb += UpdateEventReceive;
            ssdp.EventAction.EvCb += ActionEventReceive;
            timer = new Timer((a) => {
                try { ssdp.StartOnce(); }
                catch (Exception e) { EventError.EvSend(-2, new Tuple<string, Exception>(nameof(DLNA), e)); }
            }, null, TimeSpan.FromSeconds(5.0), TimeSpan.FromMinutes(5.0));
        }
        ~DLNA()
        {
            Timer tm = timer;
            timer = default;
            if (tm != default)
                try { tm.Dispose(); } catch (Exception e) { EventError.EvSend(-2, new Tuple<string, Exception>(nameof(DLNA), e)); }
        }

        #region TimerDeviceStart
        public void TimerDeviceStart()
        {
            timer.Change(TimeSpan.FromSeconds(5.0), TimeSpan.FromMinutes(5.0));
        }
        #endregion

        #region TimerDeviceStop
        public void TimerDeviceStop()
        {
            timer.Change(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
        }
        #endregion

        #region CountDevice
        public int CountDevice()
        {
            return clients.Count;
        }
        #endregion

        #region ListDevices
        public List<Tuple<string, string, string>> ListDevices()
        {
            try
            {
                List<Tuple<string, string, string>> list = new();
                foreach (var i in clients)
                    list.Add(i.ToDisplay());
                return list;
            }
            catch (Exception e) { EventError.EvSend(-2, new Tuple<string, Exception>(nameof(ListDevices), e)); }
            return default;
        }
        #endregion

        #region ListXmlDevices
        public string ListXmlDevices()
        {
            try
            {
                DLNADevices dev = new();
                foreach (var i in clients)
                    dev.Add(i.ToXmlList());

                return dev.SerializeToString();
            }
            catch (Exception e) { EventError.EvSend(-2, new Tuple<string, Exception>(nameof(ListXmlDevices), e)); }
            return xmlDefault;
        }
        #endregion

        #region ContainsDevice
        public bool ContainsDevice(string ip, string port, string desc)
        {
            return GetDevice(ip, port, desc) != default;
        }
        #endregion

        #region GetDevice
        public DLNAClient GetDevice(string ip, string port, string desc)
        {
            return (from i in clients
                     where ip.Equals(i.GetDeviceString("IP")) &&
                           port.Equals(i.GetDeviceString("PORT")) &&
                           desc.Equals(i.GetDeviceString("DESC"))
                     select i).FirstOrDefault();
        }
        #endregion

        #region SelectDevice
        public void SelectDevice(int id)
        {
            if ((id >= 0) && (id < clients.Count))
                selected = clients[id];
            else
                selected = default;
        }
        #endregion

        #region PlayToDevice
        public async void PlayToDevice(string url, string title, int id = -1)
        {
            if (!CheckDeviceId(id))
                return;
            await selected.Command(
                DLNA.ActionType.PLAY,
                -1,
                url.Replace('\\', '/').Replace(" ", "%20").Replace("#", "%23"),
                title);
        }
        #endregion

        #region LoadToDevice
        public async void LoadToDevice(string url, string title, int id = -1)
        {
            if (!CheckDeviceId(id))
                return;
            await selected.Command(
                ActionType.SETAV,
                -1,
                url.Replace('\\', '/').Replace(" ", "%20").Replace("#", "%23"),
                title);
        }
        #endregion

        #region PauseToDevice
        public async void PauseToDevice(int id = -1)
        {
            if (!CheckDeviceId(id))
                return;
            await selected.Command(DLNA.ActionType.PAUSE);
        }
        #endregion

        #region StopToDevice
        public async void StopToDevice(int id = -1)
        {
            if (!CheckDeviceId(id))
                return;
            await selected.Command(DLNA.ActionType.STOP);
        }
        #endregion

        #region TrackFromDevice
        public async void TrackFromDevice(int id = -1)
        {
            if (!CheckDeviceId(id))
                return;
            await selected.Command(DLNA.ActionType.GETINFO);
        }
        #endregion

        #region private
        private bool CheckDeviceId(int id)
        {
            do
            {
                if (selected != default)
                    return true;
                if (id < 0)
                    break;
                SelectDevice(id);
                if (selected == default)
                    break;
                return true;
            } while (false);
            return false;
        }
        private async void UpdateEventReceive(int count, Tuple<DLNA.ActionType, Queue<Tuple<string, string, string, string, string>>> data)
        {
            try
            {
                if ((count <= 0) || (data.Item1 != ActionType.SSDP))
                    return;

                clients.Clear();
                while (data.Item2.Count > 0)
                {
                    var tuple = data.Item2.Dequeue();
                    if (ContainsDevice(tuple.Item1, tuple.Item2, tuple.Item4))
                        continue;
                    var c = new DLNAClient(tuple);
                    await c.Begin();
                    if (c.IsEnable)
                    {
                        c.SetClientId(
                            $"{c.GetDeviceString("DESC")}.{c.GetDeviceString("TITLE")}.{clients.Count}".GetHashCode());
                        c.EventError.EvCb += ErrorEventReceive;
                        c.EventTrack.EvCb += TrackEventReceive;
                        c.EventAction.EvCb += ActionEventReceive;
                        clients.Add(c);
                    }
                }
                try
                {
                    while ((selected != default) && selected.IsBusy) { await Task.Delay(250); }
                    if (selected != default)
                        selected = GetDevice(
                            selected.GetDeviceString("IP"),
                            selected.GetDeviceString("PORT"),
                            selected.GetDeviceString("DESC"));
                }
                catch { }
                EventAction.EvSend(-2, new Tuple<ActionType, bool>(ActionType.SCANEND, clients.Count > 0));
            }
            catch (Exception e) { EventError.EvSend(-2, new Tuple<string, Exception>(nameof(DLNA), e)); }
        }
        private void ActionEventReceive(int id, Tuple<ActionType, bool> t)
        {
            if (t.Item1 == ActionType.SCANEND)
                t = new Tuple<ActionType, bool>(t.Item1, clients.Count > 0);
            EventAction.EvSend(id, t);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"ActionEvent, {id} : {t.Item1} -> {t.Item2}");
#endif
        }
        private void ErrorEventReceive(int id, Tuple<string, Exception> err)
        {
            EventError.EvSend(id, err);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"ErrorEvent, {id} : {err.Item1} -> {err.Item2}");
#endif
        }
        private void TrackEventReceive(int id, Tuple<DLNA.ActionType, DLNAPlayTrack> track)
        {
            EventTrack.EvSend(id, track);
#if DEBUG
            System.Diagnostics.Debug.WriteLine($"TrackEvent, {id} -> {track.Item1}");
#endif
        }
        #endregion
    }
}
