﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.UPNP
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "root", Namespace = "", IsNullable = false)]
    public class DLNADevices
    {
        [XmlElement()]
        public List<DLNADeviceData> Devices = new List<DLNADeviceData>();

        public DLNADevices() { }
        public void Add(DLNADeviceData x)
        {
            if (x == default)
                return;
            if (!Devices.Contains(x))
                Devices.Add(x);
        }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "device", Namespace = "", IsNullable = false)]
    public class DLNADeviceData
    {
        public int    Id   { get; set; } = -1;
        public string Name { get; set; } = default;
        public string Desc { get; set; } = default;
        public string Ip   { get; set; } = default;
        public string Port { get; set; } = default;
        public DLNAPlayTrack Track { get; set; } = default;
        public DLNA.ActionType ActionType { get; set; } = DLNA.ActionType.NONE;

        public DLNADeviceData() { }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "status", Namespace = "", IsNullable = false)]
    public class DLNAPlayTrackStat
    {
        public int Count { get; set; } = 0;
        public TimeSpan Time { get; set; } = TimeSpan.Zero;

        public DLNAPlayTrackStat() { }
        public DLNAPlayTrackStat(string tm, string cnt)
        {
            if (int.TryParse(cnt, out int t))
                Count = t;
            if (TimeSpan.TryParse(tm, out TimeSpan d))
                Time = d;
        }
        public override string ToString()
        {
            return $@"Time:{Time}, Count:{Count}";
        }
    }
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(ElementName = "track", Namespace = "", IsNullable = false)]
    public class DLNAPlayTrack
    {
        public int TrackId { get; set; } = -1;
        public string Url { get; set; } = string.Empty;
        public TimeSpan Duration { get; set; } = TimeSpan.Zero;
        public DLNAPlayTrackStat Rel { get; set; } = default;
        public DLNAPlayTrackStat Abs { get; set; } = default;

        public DLNAPlayTrack() { }
        public DLNAPlayTrack(string [] arr) { Parse(arr); }
        
        public void Parse(string[] arr)
        {
            for (int i = 0; i < arr.Length && i < 3; i++)
            {
                switch (i)
                {
                    case 0:
                        {
                            if (int.TryParse(arr[i], out int t))
                                TrackId = t;
                            break;
                        }
                    case 1:
                        {
                            Url = arr[i];
                            break;
                        }
                    case 2:
                        {
                            if (TimeSpan.TryParse(arr[i], out TimeSpan d))
                                Duration = d;
                            break;
                        }
                    default: break;
                }
                if (arr.Length > 5)
                    Rel = new DLNAPlayTrackStat(arr[3], arr[5]);
                if (arr.Length > 6)
                    Abs = new DLNAPlayTrackStat(arr[4], arr[6]);
            }
        }
        public void Clear()
        {
            TrackId = -1;
            Url = string.Empty;
            Duration = TimeSpan.Zero;
            Rel = default;
            Abs = default;
        }
        public DLNAPlayTrack Clone()
        {
            var n = new DLNAPlayTrack();
            n.TrackId = TrackId;
            n.Url = Url;
            n.Duration = Duration;
            n.Rel = Rel;
            n.Abs = Abs;
            return n;
        }
        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(Url))
                return "DLNAPlayTrack is empty";
            return $@"TrackId:{TrackId}, Duration:{Duration}, Url:""{Url}"", Rel:{Rel.ToString()}, Abs:{Abs.ToString()}";
        }
    }
}
