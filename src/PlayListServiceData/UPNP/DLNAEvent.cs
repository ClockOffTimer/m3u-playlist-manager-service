﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;

namespace PlayListServiceData.UPNP
{
    public class DLNAEvent<T1,T2>
    {
        public delegate void Ev(int id, Tuple<T1, T2> item);
        public event Ev EvCb = delegate { };
        public virtual void EvSend(int id, Tuple<T1, T2> item)
        {
            EvCb?.Invoke(id, item);
        }
    }
}
