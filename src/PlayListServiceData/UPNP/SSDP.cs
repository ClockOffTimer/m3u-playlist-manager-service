﻿/* Copyright (c) 2021 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Utils;

namespace PlayListServiceData.UPNP
{
    public class SSDP
    {
        private static readonly Regex udpresponse =
            new(@"^HTTP/1.1\s(\d+)\s|^SERVER:\s(\S.+)\s|^ST:\s(\S.+)|^LOCATION:\s((http|https)://)(\d{0,3}\.\d{0,3}.\d{0,3}\.\d{0,3})(:?\:)(:?\d{2,8})(:?\/)?(:?\S.+)$",
                    RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant | RegexOptions.IgnorePatternWhitespace);
        private string UpnpSearchString() => "M-SEARCH * HTTP/1.1\r\nHOST:239.255.255.250:1900\r\nMAN:\"ssdp:discover\"\r\nST:urn:schemas-upnp-org:service:AVTransport:1\r\nMX:3\r\n\r\n";

        private static SSDP instance = default;
        private CancellationTokenSafe token = default;

        public int UdpPortSSDP   { get; private set; } = 1900;
        public int UdpPortServer { get; set; } = 8088;
        public int WebPortServer { get; set; } = 8089;

        public bool   IsScanWait  { get; private set; } = default;
        public string UdpIpSSDP   { get; set; } = "239.255.255.250";
        public string UdpIpServer { get; set; } = "192.168.0.105";
        public string WebServer   { get; set; } = "192.168.0.105";
        public Queue<Tuple<string, string, string, string, string>> Clients { get; private set; } =
            new Queue<Tuple<string, string, string, string, string>>();

        public DLNAEvent<DLNA.ActionType, Queue<Tuple<string, string, string, string, string>>> EventDevice =
            new();
        public DLNAEvent<string, Exception> EventError = new();
        public DLNAEvent<DLNA.ActionType, bool> EventAction = new();

        private SSDP()
        {
            SSDP.instance = this;
        }
        ~SSDP()
        {
            Clear();
        }

        public static SSDP Create()
        {
            if (SSDP.instance == default)
                SSDP.instance = new SSDP();
            return SSDP.instance;
        }

        public async void Start()
        {
            if (IsScanWait)
                return;
            IsScanWait = true;
            await SendRequest();
        }
        public async void StartOnce()
        {
            if (IsScanWait)
                return;
            IsScanWait = true;
            await SendRequest(true);
        }
        public void Stop()
        {
            if (!IsScanWait)
                return;
            Clear();
        }
        public async void Clear()
        {
            CancellationTokenSafe cts = token;
            token = default;
            if (cts != default)
            {
                try { cts.Cancel(); } catch (Exception e)  { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(Clear) + "1", e)); }
                await Task.Delay(750);
                try { cts.Dispose(); } catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(Clear) + "2", e)); }
            }
        }
        private async Task SendRequest(bool IsOnce = false)
        {
            try
            {
                Clear();
                token = new CancellationTokenSafe();
                CancellationToken cancel = token.Token;
                EventAction.EvSend(-2, new Tuple<DLNA.ActionType, bool>(DLNA.ActionType.SCANBEGIN, true));
                await Task.Run(() => {
                    try
                    {
                        while (!cancel.IsCancellationRequested)
                        {
                            MakeRequest(cancel);
                            if (IsOnce)
                                break;
                        }
                    }
                    catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(CloseSocket) + "2", e)); }
                    finally
                    {
                        EventAction.EvSend(-2, new Tuple<DLNA.ActionType, bool>(DLNA.ActionType.SCANEND, false));
                        IsScanWait = false;
                    }
                }, cancel).ConfigureAwait(false);
            }
            catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(SendRequest), e)); }
        }
        private Tuple<Socket, IPEndPoint> MakeSocket()
        {
            Socket UdpSocket;
            IPEndPoint lep = new(IPAddress.Parse(UdpIpServer), UdpPortServer);
            IPEndPoint mep = new(IPAddress.Parse(UdpIpSSDP), UdpPortSSDP);
            UdpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            UdpSocket.Bind(lep);
            UdpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(mep.Address, IPAddress.Any));
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 2);
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastLoopback, true);
            return new Tuple<Socket, IPEndPoint>(UdpSocket, mep);
        }
        private void CloseSocket(Tuple<Socket, IPEndPoint> sock)
        {
            if (sock == default)
                return;

            try { sock.Item1.Shutdown(SocketShutdown.Both); } catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(CloseSocket) + "1", e)); }
            try { sock.Item1.Close(); } catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(CloseSocket) + "2", e)); }
            try { sock.Item1.Dispose(); } catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(CloseSocket) + "3", e)); }
        }
        private async void MakeRequest(CancellationToken cancel)
        {
            Tuple<Socket, IPEndPoint> sock = default;
            try
            {
                bool b = false;
                sock = MakeSocket();
                sock.Item1.SendTo(Encoding.UTF8.GetBytes(UpnpSearchString()), SocketFlags.None, sock.Item2);
                DateTime dt = DateTime.Now + TimeSpan.FromSeconds(15);
                HashSet<string> sdup = new();

                Clients.Clear();
                cancel.ThrowIfCancellationRequested();

                while (dt >= DateTime.Now)
                {
                    try
                    {
                        if (sock.Item1.Available <= 0)
                        {
                            await Task.Delay(750);
                            continue;
                        }
                        byte[] rb = new byte[4000];
                        ArraySegment<byte> aseg = new(rb);
                        int sz = await sock.Item1.ReceiveAsync(aseg, SocketFlags.None);
                        if (sz > 0)
                        {
                            string str = Encoding.UTF8.GetString(rb, 0, sz).Trim();
                            Tuple<string, string, string, string, string> data = GetDiscoveryUrl(str);
                            if ((data == default) || sdup.Contains(data.Item5))
                            {
                                await Task.Delay(750);
                                continue;
                            }
                            sdup.Add(data.Item5);

                            if (!Clients.Contains(data))
                                Clients.Enqueue(data);
                        }
                        if (cancel.IsCancellationRequested)
                            break;
                        await Task.Delay(750);

                        if (b) sock.Item1.SendTo(Encoding.UTF8.GetBytes(UpnpSearchString()), SocketFlags.None, sock.Item2);
                        if (!b) b = true;
                    }
                    catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(MakeRequest), e)); }
                }
            }
            catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(MakeRequest), e)); }
            finally
            {
                if (Clients.Count > 0)
                    EventDevice.EvSend(
                        Clients.Count,
                        new Tuple<DLNA.ActionType, Queue<Tuple<string, string, string, string, string>>>(DLNA.ActionType.SSDP, Clients));
                CloseSocket(sock);
            }
        }

        private Tuple<string, string, string, string, string> GetDiscoveryUrl(string src)
        {
            try
            {
                MatchCollection m = SSDP.udpresponse.Matches(src);
                string[] arr = new string[6];

                for (int i = 0; i < m.Count; i++)
                {
                    int[] idx = new int[3];
                    switch (i)
                    {
                        case 0:
                            {
                                idx[0] = i; idx[1] = 1; idx[2] = 0;
                                arr[0] = m.GetMatched(idx);
                                break;
                            }
                        case 1:
                            {
                                idx[0] = i; idx[1] = 6; idx[2] = 0;
                                arr[1] = m.GetMatched(idx);
                                idx[0] = i; idx[1] = 8; idx[2] = 0;
                                arr[2] = m.GetMatched(idx);
                                idx[0] = i; idx[1] = 10; idx[2] = 0;
                                arr[3] = m.GetMatched(idx);
                                break;
                            }
                        case 2:
                            {
                                idx[0] = i; idx[1] = 2; idx[2] = 0;
                                arr[4] = m.GetMatched(idx);
                                break;
                            }
                        case 3:
                            {
                                idx[0] = i; idx[1] = 3; idx[2] = 0;
                                arr[5] = m.GetMatched(idx);
                                break;
                            }
                        default: continue;
                    }
                }
                if ((arr[5] == default) || (!arr[5].ToUpper().Contains("AVTRANSPORT")))
                    return default;
                if ((arr[1] == default) || (arr[2] == default))
                    return default;
                if ((arr[0] == default) || (!int.TryParse(arr[0], out int code)) || (code != 200))
                    return default;
                if ((arr[2] == default) || (!int.TryParse(arr[2], out int port)))
                    return default;

                return new Tuple<string, string, string, string, string>(
                    arr[1], arr[2], arr[3], arr[4], arr[5]);
            }
            catch (Exception e) { EventError.EvSend(-1, new Tuple<string, Exception>(nameof(GetDiscoveryUrl), e)); }
            return default;
        }
    }
}

