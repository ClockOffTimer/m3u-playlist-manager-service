﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Threading;
using System.Xml.Serialization;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.RunManager
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SheduleItemLite : ISheduleItemValues
    {
        private readonly ILogManager LogWriter;

        [XmlElement("id")]
        public object Id { get; set; } = default;
        [XmlElement("state")]
        public string Status { get; set; } = default;
        [XmlElement("enable")]
        public bool IsEnable { get; set; } = false;
        [XmlElement("active")]
        public bool IsActive { get; set; } = false;
        [XmlElement("runing")]
        public bool IsRuning { get; set; } = false;
        [XmlElement("timerlastchange")]
        public bool IsChange { get; set; } = true;
        [XmlElement("runtype")]
        public SheduleManager.ActionType RunType { get; set; } = SheduleManager.ActionType.NONE;
        [XmlElement("periodrun")]
        public long PeriodTimeLong { get; set; } = -1;
        [XmlElement("duerun")]
        public long DueTimeLong { get; set; } = -1;
        [XmlElement("dayofweekrun")]
        public long DayOfWeekTimeLong { get; set; } = -1;
        [XmlElement("lastrun")]
        public DateTime LastRunTime { get; set; } = DateTime.MinValue;
        [XmlElement("lasterror")]
        public BaseExceptionXml LastError { get; set; } = default;

        [XmlIgnore]
        public Exception LastErrorEx
        {
            get => default(Exception);
            set { if (value != null) { if (LastError == null) LastError = new(value); else LastError.Copy(value); }}
        }

        [XmlIgnore]
        public TimeSpan PeriodTime
        {
            get => TimeSpan.FromTicks(PeriodTimeLong);
            set { if (value != null) PeriodTimeLong = value.Ticks; }
        }
        [XmlIgnore]
        public TimeSpan DueTime
        {
            get => TimeSpan.FromTicks(DueTimeLong);
            set { if (value != null) DueTimeLong = value.Ticks; }
        }
        [XmlIgnore]
        public TimeSpan DayOfWeekTime
        {
            get => TimeSpan.FromTicks(DayOfWeekTimeLong);
            set { if (value != null) DayOfWeekTimeLong = value.Ticks; }
        }

        public SheduleItemLite() { }
        public SheduleItemLite(ISheduleItemValues shi) { Copy(shi); LogWriter = default; }
        public SheduleItemLite(ISheduleItemValues shi, ILogManager log) { Copy(shi); LogWriter = log; }

        public void Copy(ISheduleItemValues shi)
        {
            if (shi == null)
                throw new ArgumentNullException(nameof(ISheduleItemValues));

            Id = shi.Id.ToString();
            IsEnable = shi.IsEnable;
            IsActive = shi.IsActive;
            IsRuning = shi.IsRuning;
            IsChange = shi.IsChange;
            RunType = shi.RunType;
            LastRunTime = shi.LastRunTime;
            PeriodTime = (shi.PeriodTime == null) ? Timeout.InfiniteTimeSpan : shi.PeriodTime;
            DueTime = (shi.DueTime == null) ? Timeout.InfiniteTimeSpan : shi.DueTime;
            DayOfWeekTime = (shi.DayOfWeekTime == null) ? Timeout.InfiniteTimeSpan : shi.DayOfWeekTime;
            LastError = shi.LastError;

            if (!IsEnable)
                Status = "job disabled";
            else if (!IsActive)
                Status = "timer is null";
            else if (IsRuning)
                Status = "already running";
            else if (!IsRuning)
                Status = "wait time event";
            else if (!IsChange)
                Status = "last change timer produce error";
            else
                Status = string.IsNullOrWhiteSpace(Status) ? string.Empty : Status;

#           if DEBUG
            string dump = $"\tOrig: {shi.FormatedText()}\n\tThis: {this.FormatedText()}";
            if (LogWriter != default)
                LogWriter?.Verbose.Write(dump);
            else
                System.Diagnostics.Debug.WriteLine(dump);
#           endif
        }

        public override string ToString() => this.FormatedText();
    }
}
