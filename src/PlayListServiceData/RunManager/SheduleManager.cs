﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.RunManager
{
    #region SheduleManager
    public class SheduleManager
    {
        public enum ActionType : int
        {
            NONE = 0,
            RUN_ONCE,
            RUN_HOLD,
            RUN_DAYOFWEEK,
            INT_SECONDS,
            INT_MINUTES,
            INT_HOURS,
            INT_DAYS
        };

        private SheduleManager(ILogManager log)
        {
            LogWriter = log;
            token = new ();
            Add(-1, ActionType.INT_MINUTES, TimeSpan.FromSeconds(60.0), TimeSpan.Zero, (s, a) => { checkSheduled(); });
        }
        ~SheduleManager()
        {
            CancellationTokenSource cts = token;
            token = default;
            if (cts != default)
            {
                if (!cts.IsCancellationRequested)
                    try { cts.Cancel(); } catch { }
                try { cts.Dispose(); } catch (Exception ex) { ex.WriteLog('!', $"{nameof(SheduleManager)} -> Cancellation Token ->", LogWriter); }
            }
        }
        public static SheduleManager Create(ILogManager log) => new(log);
        public Action ManageQueueCb = () => { };

        private CancellationTokenSource token;
        private readonly List<SheduleItem> shedules = new();
        private readonly ILogManager LogWriter;

        public void Pause()
        {
            allEnabled(false);
        }
        public void Resume()
        {
            allEnabled(true);
        }
        public async void Clear()
        {
            try
            {
                if (shedules != default)
                    shedules.Clear();

                if (token != default)
                {
                    if (!token.IsCancellationRequested)
                    {
                        try { token.Cancel(); } catch { }
                        await Task.Delay(1000).ConfigureAwait(false);
                    }
                    try { token.Dispose(); token = default; } catch { }
                    token = new ();
                }
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(Clear), LogWriter); }
        }
        public void Remove(object id)
        {
            try
            {
                var a = findById(id);
                if (a == default)
                    return;
                shedules.Remove(a);
            }
            catch (Exception ex) { ex.WriteLog('!', $"{nameof(Remove)} -> ID:{id} ->", LogWriter); }
        }
        public bool Contains(object id)
        {
            if (findById(id) == default)
                return false;
            return true;
        }
        public SheduleItem GetById(object id)
        {
            return findById(id);
        }
        public bool RunTask(object id)
        {
            try
            {
                SheduleItem item = findById(id);
                if (item == default)
                {
                    $"{item.Id}, {item.RunType} = job not found, dont run..".WriteLog('v', LogWriter);
                    return false;
                }
                if (!item.IsEnable)
                {
                    $"{item.Id}, {item.RunType} = job disabled, dont run..".WriteLog('v', LogWriter);
                    return false;
                }
                if (!item.IsActive)
                {
                    $"{item.Id}, {item.RunType} = timer is null, dont run..".WriteLog('v', LogWriter);
                    return false;
                }
                if (item.IsRuning)
                {
                    $"{item.Id}, {item.RunType} = already running, dont run..".WriteLog('v', LogWriter);
                    return false;
                }
                $"{item.Id} -> Call -> Begin {item.RunType} job started.. (Found:{(item != null)})".WriteLog('v', LogWriter);
                item.Run();
                return true;
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(RunTask), LogWriter); }
            return false;
        }
        public override string ToString()
        {
            StringBuilder sb = new();
            for (int i = 0; i < shedules.Count; i++)
                sb.AppendLine($"[{i}] {shedules[i]}");
            return sb.ToString();
        }

        public SheduleItemsLite GetAllTasks()
        {
            SheduleItemsLite items = new();
            try
            {
                foreach (SheduleItem shi in shedules)
                    items.Add(new SheduleItemLite(shi, LogWriter));
                return items;
            } catch (Exception ex) { ex.WriteLog('!', nameof(GetAllTasks), LogWriter); }
            return default;
        }

        public void SetAllTasks(SheduleItemsLite items)
        {
            if ((items == default) || (items.Items.Count == 0))
                return;

            try {
                foreach (SheduleItemLite shi in items.Items)
                {
                    SheduleItem item = (from i in shedules
                                        where i.Id.ToString() == shi.Id.ToString()
                                        select i).FirstOrDefault();
                    if (item == default)
                        continue;
                    item.Copy(shi);
                }
            } catch (Exception ex) { ex.WriteLog('!', nameof(SetAllTasks), LogWriter); }
        }

        [Conditional("DEBUG")]
        public void PrintList()
        {
            try {
                for (int i = shedules.Count - 1; i >= 0; i--)
                    LogWriter?.Write($"\t[{i}] - {shedules[i].FormatedText()}");

            } catch (Exception ex) { ex.WriteLog('!', nameof(PrintList), LogWriter); }
        }

        [Description("* DayOfWeek type")]
        public void Add(object id, int minutes, DayOfWeek day, Action<ISheduleItem, CancellationToken> task) =>
            shedules.Add(new SheduleItem(LogWriter, ActionType.RUN_DAYOFWEEK, id, TimeSpan.FromDays((double)((day == DayOfWeek.Sunday) ? 7 : (int)day)), minutes.GetDueTime(), token.Token, task));

        [Description("* HOLD type")]
        public void Add(object id, Action<ISheduleItem, CancellationToken> task) =>
            shedules.Add(new SheduleItem(LogWriter, ActionType.RUN_HOLD, id, Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan, token.Token, task));

        [Description("+ all time type + interval")]
        public void Add(object id, ActionType type, TimeSpan period, int minutes, Action<ISheduleItem, CancellationToken> task) =>
            shedules.Add(new SheduleItem(LogWriter, type, id, period, minutes.GetDueTime(), token.Token, task));

        [Description("+ all time type + interval + start TimeSpan")]
        public void Add(object id, ActionType type, TimeSpan period, TimeSpan due, Action<ISheduleItem, CancellationToken> task) =>
            shedules.Add(new SheduleItem(LogWriter, type, id, period, due, token.Token, task));

        private void checkSheduled()
        {
            try
            {
                for (int i = shedules.Count - 1; i >= 0; i--)
                    if (!shedules[i].IsActive && !shedules[i].IsRuning)
                        shedules.RemoveAt(i);

                try { ManageQueueCb.Invoke(); }
                catch (Exception ex) { ex.WriteLog('!', $"{nameof(checkSheduled)} -> Manage Queue ->", LogWriter); }
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(checkSheduled), LogWriter); }
        }
        private void allEnabled(bool val)
        {
            try
            {
                for (int i = shedules.Count - 1; i >= 0; i--)
                    shedules[i].Enable(val);
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(allEnabled), LogWriter); }
        }
        private SheduleItem findById(object id)
        {
            try
            {
                for (int i = shedules.Count - 1; i >= 0; i--)
                    if (shedules[i].Id.Equals(id))
                        return shedules[i];
            }
            catch (Exception ex) { ex.WriteLog('!', nameof(findById), LogWriter); }
            return default;
        }
    }
    #endregion
}
