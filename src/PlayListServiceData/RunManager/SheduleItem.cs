﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;
using System.Threading.Tasks;
using PlayListServiceData.Base;
using PlayListServiceData.Process.Log;

namespace PlayListServiceData.RunManager
{
    #region SheduleItem
    public class SheduleItem : ISheduleItem, ISheduleItemValues, IDisposable
    {
        private readonly SheduleItem Instance;
        private readonly ILogManager LogWriter;
        private CancellationToken token_ = default;
        private Timer timer_ = default;
        private bool isEnable_ = true,
                     isRuning_ = false;

        public Action<ISheduleItem, CancellationToken> Exec { get; private set; } = default;
        public SheduleManager.ActionType RunType { get; private set; } = SheduleManager.ActionType.NONE;
        public object Id { get; private set; } = default;
        public string Status { get; private set; } = string.Empty; /* not used */

        #region last error
        public BaseExceptionXml LastError { get; set; } = new();
        public Exception LastErrorEx
        {
            get => (LastError == default) ? default : LastError.Get();
            private set {
                LastError = (LastError == default) ? new() : LastError;
                LastError.Copy(value);
            }
        }
        #endregion

        public bool IsChange { get; private set; } = true;
        public bool IsEnable { get => isEnable_; }
        public bool IsActive { get => timer_ != default; }
        public bool IsRuning
        {
            get => isRuning_;
            private set
            {
                if (isRuning_ == value)
                    return;
                isRuning_ = value;
                LastRunTime = DateTime.Now;
            }
        }
        public DateTime LastRunTime { get; private set; } = DateTime.MinValue;
        public TimeSpan PeriodTime { get; private set; } = Timeout.InfiniteTimeSpan;
        public TimeSpan DueTime { get; private set; } = Timeout.InfiniteTimeSpan;
        public TimeSpan DayOfWeekTime { get; private set; } = Timeout.InfiniteTimeSpan;

        private TimeSpan PeriodTimeCalc(SheduleManager.ActionType type, TimeSpan ts) =>
            type switch
            {
                SheduleManager.ActionType.RUN_HOLD => Timeout.InfiniteTimeSpan,
                SheduleManager.ActionType.RUN_ONCE => Timeout.InfiniteTimeSpan,
                _ => ts
            };
        private TimeSpan DueTimeCalc(SheduleManager.ActionType type, TimeSpan ts) =>
            (type == SheduleManager.ActionType.RUN_HOLD) ?
                Timeout.InfiniteTimeSpan : ts;

        private TimeSpan DayOfWeekTimeCalc(TimeSpan ts) {
            DayOfWeek dx = DateTime.Today.DayOfWeek;
            int current = (dx == DayOfWeek.Sunday) ? 7 : (int)dx,
                from = ts.Days;
            from = (from < current) ? (current - from) : ((from == current) ? 7 : (from - current));
            return TimeSpan.FromDays((double)from);
        }

        public SheduleItem(
            ILogManager log,
            SheduleManager.ActionType type,
            object id,
            TimeSpan tsPeriodTime,
            TimeSpan tsDueTime,
            CancellationToken token,
            Action<ISheduleItem, CancellationToken> task)
        {
            Instance = this;
            RunType = type;
            Id = id;
            Exec = task;
            token_ = token;
            LogWriter = log;
            DueTime = DueTimeCalc(RunType, tsDueTime);
            isEnable_ = true;

            if (RunType == SheduleManager.ActionType.RUN_DAYOFWEEK)
            {
                DayOfWeekTime = tsPeriodTime;
                PeriodTime = DayOfWeekTimeCalc(DayOfWeekTime);
            }
            else
            {
                PeriodTime = PeriodTimeCalc(RunType, tsPeriodTime);
                DayOfWeekTime = Timeout.InfiniteTimeSpan;
            }

            timer_ = new Timer(x =>
            {
                try {
                    if (token_.IsCancellationRequested)
                        return;

                    if (!IsEnable || IsRuning || (RunType == SheduleManager.ActionType.RUN_HOLD))
                        return;

                    runAsync(task);

                    if (RunType == SheduleManager.ActionType.RUN_ONCE)
                        try { timer_.Dispose(); timer_ = default; }
                        catch (Exception ex) { ex.WriteLog('!', nameof(Timer), LogWriter); }

                } catch (Exception ex) { ex.WriteLog('!', nameof(Timer), LogWriter); }

            }, null, DueTime, PeriodTime);
        }
        ~SheduleItem()
        {
            Dispose();
        }
        ///
        public void Dispose()
        {
            Timer tm = timer_;
            timer_ = default;
            if (tm != default)
                try { tm.Dispose(); }
                catch (Exception ex) { ex.WriteLog('!', nameof(Dispose), LogWriter); }
            isEnable_ = false;
        }
        public void Enable(bool b)
        {
            if (timer_ == default)
                return;
            isEnable_ = b;
        }
        public void Run()
        {
            if (!isEnable_ || IsRuning)
                return;
            runAsync(Exec);
        }
        public Timer GetTimer()
        {
            return timer_;
        }
        public void ChangeTimer(TimeSpan periodTime, TimeSpan dueTime)
        {
            if (!IsActive || !IsEnable)
                return;

            changeTimer(periodTime, dueTime);
        }
        public override string ToString() => this.FormatedText();
        public void Copy(ISheduleItemValues shi)
        {
            if (shi == default)
                return;

            switch (RunType)
            {
                case SheduleManager.ActionType.NONE:
                case SheduleManager.ActionType.RUN_HOLD:
                case SheduleManager.ActionType.RUN_ONCE: break;
                case SheduleManager.ActionType.RUN_DAYOFWEEK:
                    {
                        DayOfWeekTime = shi.DayOfWeekTime;
                        PeriodTime = DayOfWeekTimeCalc(DayOfWeekTime);
                        break;
                    }
                default:
                    {
                        PeriodTime = shi.PeriodTime;
                        break;
                    }
            }
            DueTime = shi.DueTime;

            if (!IsRuning)
                isEnable_ = shi.IsEnable;
            changeTimer(PeriodTime, DueTime);
        }
        ///
        private void changeTimer(TimeSpan periodTime, TimeSpan dueTime)
        {
            DueTime = (RunType == SheduleManager.ActionType.RUN_HOLD) ? Timeout.InfiniteTimeSpan :
                ((dueTime != default) ? dueTime : DueTime);

            PeriodTime = (RunType == SheduleManager.ActionType.RUN_ONCE) ? Timeout.InfiniteTimeSpan :
                ((periodTime != default) ? periodTime : PeriodTime);

            try { IsChange = timer_.Change(DueTime, PeriodTime); LastError.Message = default; }
            catch (Exception ex) {
                ex.WriteLog('!', nameof(changeTimer), LogWriter);
                LastError.Copy(ex);
                IsChange = false;
            }
        }
        private void setNextTimer()
        {
            switch (RunType)
            {
                case SheduleManager.ActionType.RUN_HOLD:
                case SheduleManager.ActionType.RUN_ONCE:
                    {
                        changeTimer(Timeout.InfiniteTimeSpan, Timeout.InfiniteTimeSpan);
                        break;
                    }
                case SheduleManager.ActionType.RUN_DAYOFWEEK:
                    {
                        /* DueTime = LastRunTime.GetDueTime(); */
                        PeriodTime = DayOfWeekTimeCalc(DayOfWeekTime);
                        changeTimer(PeriodTime, default);
                        break;
                    }
                case SheduleManager.ActionType.INT_DAYS:
                case SheduleManager.ActionType.INT_HOURS:
                case SheduleManager.ActionType.INT_MINUTES:
                case SheduleManager.ActionType.INT_SECONDS:
                    {
                        /* DueTime = LastRunTime.GetDueTime(); */
                        changeTimer(PeriodTime, DueTime);
                        break;
                    }
            }
        }
        private async void runAsync(Action<ISheduleItem, CancellationToken> task)
        {
            if (IsRuning)
                return;
            IsRuning = true;

            if (token_.IsCancellationRequested)
                return;

            setNextTimer();

            await Task.Run(() =>
            {
                try { task?.Invoke(Instance, token_); }
                catch (Exception ex) {
                    ex.WriteLog('!', nameof(runAsync), LogWriter);
                    LastError.Copy(ex);
                }
                finally { IsRuning = false; }
            });
        }
    }
    #endregion
}
