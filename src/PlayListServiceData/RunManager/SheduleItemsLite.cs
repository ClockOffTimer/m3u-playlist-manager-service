﻿/* Copyright (c) 2021-2022 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace PlayListServiceData.RunManager
{
    [Serializable]
    [DesignerCategory("code")]
    [XmlType(AnonymousType = true)]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class SheduleItemsLite
    {
        [XmlElement("items")]
        public List<SheduleItemLite> Items { get; set; } = new();
        public void Add(SheduleItemLite shi) => Items.Add(shi);
    }
}
