﻿/* Copyright (c) 2021-2020 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using PlayListServiceData.Base;

namespace PlayListServiceData.RunManager
{
    public interface ISheduleItemValues
    {
        #region Getter
        SheduleManager.ActionType RunType { get; }
        BaseExceptionXml LastError { get; set; }
        string Status { get; }
        object Id { get; }
        bool IsEnable { get; }
        bool IsActive { get; }
        bool IsRuning { get; }
        bool IsChange { get; }
        DateTime LastRunTime { get; }
        TimeSpan PeriodTime { get; }
        TimeSpan DueTime { get; }
        TimeSpan DayOfWeekTime { get; }
        #endregion
    }
}
