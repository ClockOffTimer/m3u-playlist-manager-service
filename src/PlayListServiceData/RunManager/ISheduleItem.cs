﻿/* Copyright (c) 2021-2020 PlayListServiceData, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Threading;

namespace PlayListServiceData.RunManager
{
    public interface ISheduleItem
    {
        Action<ISheduleItem, CancellationToken> Exec { get; }

        void Dispose();
        void Enable(bool b);
        void Run();
        void ChangeTimer(TimeSpan ta, TimeSpan tb);
        Timer GetTimer();
    }
}
