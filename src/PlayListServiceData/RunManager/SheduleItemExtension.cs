﻿/* Copyright (c) 2021-2022 PlayListService, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Threading;

namespace PlayListServiceData.RunManager
{
    public static class SheduleItemExtension
    {
        public static string FormatedText(this ISheduleItemValues @this)
        {
            StringBuilder sb = new();
            string last = (@this.LastRunTime == DateTime.MinValue) ? "00.00:00" : @this.LastRunTime.ToString("dd.HH:mm");

            sb.Append($"[{@this.Id}] - {@this.RunType} = [{@this.IsActive}/{@this.IsEnable}/{@this.IsRuning}]");
            sb.Append($"\n\t\tPeriod:{@this.PeriodTime:dd\\.hh\\:mm} <-> Due:{@this.DueTime:dd\\.hh\\:mm} -> Last:{last}");
            if (@this.DayOfWeekTime != Timeout.InfiniteTimeSpan)
                sb.Append($"\n\t\t RunDay:{@this.DayOfWeekTime:dd}");
            if (!string.IsNullOrWhiteSpace(@this.Status))
                sb.Append($"\n\t\t Status:{@this.Status}");
            if ((@this.LastError != default) && (!string.IsNullOrWhiteSpace(@this.LastError.Message)))
                sb.Append($"\n\t\tLast error:{@this.LastError.Message}");
            return sb.ToString();
        }
        public static TimeSpan GetDueTime(this int minutes)
        {
            DateTime dt = DateTime.Now.AddMinutes(minutes);
            return new TimeSpan(dt.Hour, dt.Minute, 0);
        }
        public static TimeSpan GetDueTime(this DateTime tlast)
        {
            DateTime dt = ((tlast != DateTime.MinValue) ? tlast : DateTime.Now).AddMinutes(-1);
            return new TimeSpan(dt.Hour, dt.Minute, 0);
        }

    }
}
