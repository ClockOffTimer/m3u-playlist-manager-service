﻿/* Copyright (c) 2021 WpfPluginEpg, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Windows.Data;
using PlayListServiceData.Container.Items;

namespace WpfPlugin.Data
{
    [ValueConversion(typeof(string), typeof(string))]
    public class TimeLineBeginConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ViewSubItemE item)
                return item.Begin.ToString("HH:mm");
            return "00:00";
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
