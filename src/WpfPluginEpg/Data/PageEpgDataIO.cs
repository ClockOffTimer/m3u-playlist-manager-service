﻿/* Copyright (c) 2021 WpfPluginEpg, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using PlayListServiceData.Base;
using PlayListServiceData.Utils;
using PlayListServiceData.Container;
using PlayListServiceData.Container.Items;

namespace WpfPlugin.Data
{
    public partial class PageEpgData : INotifyPropertyChanged
    {
        private readonly RunOnce onceEpgFav = new RunOnce();
        private readonly RunOnce onceEpgBase = new RunOnce();
        private DataContainer<BaseSerializableChannelData> XmlDataContainerT = default;
        private DataContainer<BaseSerializableProgrammData> XmlDataContainerE = default;

        private void LoadEpgData()
        {
            try
            {
                string fav = BasePath.GetEpgFavoritePath();
                FileInfo file = new FileInfo(fav);
                if ((file == null) || (!file.Exists))
                    return;
                string xml = ServiceCtrl.ControlConfig.ConfigM3uPath.GetEpgBasePath(ServiceCtrl.ControlConfig.ConfigEpgUrl);
                FileInfo fxml = new FileInfo(xml);
                if ((fxml == null) || (!fxml.Exists))
                    return;

                XmlDataContainerE = default;
                XmlDataContainerT = default;

                Application.Current.Dispatcher.Invoke(() =>
                {
                    EpgItems.Clear();
                    OnPropertyChanged(nameof(EpgItems));
                    OnPropertyChanged(nameof(IsEpgActions));
                    OnPropertyChanged(nameof(PanelHVisibility));
                    OnPropertyChanged(nameof(PanelVVisibility));
                });

                XmlDataContainerT =
                    new DataContainer<BaseSerializableChannelData>(tokenSourceCancel.Token);
                XmlDataContainerE =
                    new DataContainer<BaseSerializableProgrammData>(tokenSourceCancel.Token);

                onceEpgFav.Reset();
                onceEpgBase.Reset();

                _ = XmlDataContainerE.OpenQueueRead(
                    xml,
                    eventEpgBaseLoadCb_,
                    ConstContainer.Actions.LOAD_EPG_BASE);
                XmlDataContainerE.QueueStart();

                _ = XmlDataContainerT.OpenQueueRead(
                    fav,
                    eventEpgFavLoadCb_,
                    ConstContainer.Actions.LOAD_EPG_FAV);
                XmlDataContainerT.QueueStart();
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); IsLoadingEpgData = false; }
#else
            catch { IsLoadingEpgData = false; }
#endif
        }

        /* - LOAD EPG Base callback - */
        #region - EPG Base LOAD callback = eventEpgBaseLoadCb_ (call loadEpgBaseParse_)
        private void eventEpgBaseLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceEpgBase.GetOnce())
                            return;
                        loadEpgBaseParse_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        /* - LOAD EPG Base method - */
        #region - EPG Base LOAD = loadEpgBaseParse_
        private void loadEpgBaseParse_(CancellationToken tokenCancel)
        {
            DataContainerItem<BaseSerializableProgrammData> dc = default;
            try
            {
                var parent = XmlDataContainerT.GetContainerItemFromId(ConstContainer.Actions.LOAD_EPG_FAV);
                if (parent == null)
                    return;

                int cnt = 1500000;
                while (!parent.IsComplette)
                {
                    Task.Delay(750);
                    Task.Yield();
                    if (cnt-- <= 0)
                        return;
                    if (tokenCancel.IsCancellationRequested)
                        return;
                }

                dc = XmlDataContainerE.GetContainerItemFromId(ConstContainer.Actions.LOAD_EPG_BASE);
                if ((dc == null) || (dc.Data == null))
                    return;

                dc.IsComplette = false;

                DateTime dt = DateTime.Now;
                dt = dt.AddHours(-1);

                foreach (var i in EpgItems)
                {
                    try
                    {
                        var a = (from n in dc.Data.programme
                                 where i.IfTag.Equals(n.channel)
                                 select n).ToList();
                        if (a == null)
                            continue;

                        foreach (var k in a)
                        {
                            if (tokenCancel.IsCancellationRequested)
                                break;
                            try
                            {
                                ViewSubItemE item = new ViewSubItemE(k.start, k.stop, k.title?.Value, k.desc?.Value);
                                do
                                {
                                    if ((item.Begin == null) || (item.End == null))
                                        break;
                                    if (dt >= item.Begin)
                                        break;
                                    if (item.Title == null)
                                        break;

                                    Application.Current.Dispatcher.Invoke(() => { i.Add(item); });

                                } while (false);
                            }
#if DEBUG
                            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                            catch { }
#endif
                        }
                    }
#if DEBUG
                    catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
                    catch { }
#endif
                }
                dc.Clear();
                for (int m = EpgItems.Count - 1; m >= 0; m--)
                    if (EpgItems[m].IfListItems.Count == 0)
                        Application.Current.Dispatcher.Invoke(() => { EpgItems.Remove(EpgItems[m]); });
            }
#if DEBUG
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
            }
#else
            catch { Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; }); }
#endif
            finally
            {
                if (dc != null)
                    dc.IsComplette = true;
                Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                LoadSelector();
#if DEBUG
                System.Diagnostics.Debug.WriteLine("EPG *loadEpgBaseParse_ = IsComplette");
#endif
            }
        }
        #endregion


        /* - LOAD EPG Favorites callback - */
        #region - EPG Favorites LOAD callback = eventEpgFavLoadCb_ (call loadedEpgFavAsync_)
        private void eventEpgFavLoadCb_(ConstContainer.State s)
        {
            switch (s)
            {
                case ConstContainer.State.DataReady:
                    {
                        if (onceEpgFav.GetOnce())
                            return;
                        loadEpgFavParse_(tokenSourceCancel.Token);
                        break;
                    }
                case ConstContainer.State.LoadDataEmpty:
                case ConstContainer.State.LoadDataFound:
                case ConstContainer.State.Loading:
                case ConstContainer.State.Saving:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = true; });
                        break;
                    }
                case ConstContainer.State.LoadEnd:
                case ConstContainer.State.LoadError:
                case ConstContainer.State.ErrorEnd:
                case ConstContainer.State.CancelledEnd:
                    {
                        Application.Current.Dispatcher.Invoke(() => { IsLoadingEpgData = false; });
                        break;
                    }
            }
        }
        #endregion

        /* - LOAD EPG Favorites method - */
        #region - EPG Favorites LOAD = loadEpgFavParse_
        private void loadEpgFavParse_(CancellationToken tokenCancel)
        {
            DataContainerItem<BaseSerializableChannelData> dc = default;
            try
            {
                dc = XmlDataContainerT.GetContainerItemFromId(ConstContainer.Actions.LOAD_EPG_FAV);
                if ((dc == null) || (dc.Data == null))
                    return;

                dc.IsComplette = false;
                for (int i = dc.Data.channel.Length - 1; i >= 0; i--)
                {
                    if (tokenCancel.IsCancellationRequested)
                        break;
                    var s = ((dc.Data.channel[i].icon == null) || (dc.Data.channel[i].icon.src == null)) ? default : dc.Data.channel[i].icon.src;
                    var t = ((dc.Data.channel[i].displayname == null) || (dc.Data.channel[i].displayname.Length == 0)) ? default : dc.Data.channel[i].displayname[0];
                    Application.Current.Dispatcher.Invoke(() => { EpgItems.Add(new ViewItemE<ViewSubItemE>(dc.Data.channel[i].id, s, t)); });
                }
                dc.Clear();
            }
#if DEBUG
            catch (Exception e) { System.Diagnostics.Debug.WriteLine(e); }
#else
            catch { }
#endif
            finally
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (dc != null)
                        dc.IsComplette = true;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        IsLoadingEpgData = false;
                        if (EpgItems.Count > 0)
                            OnPropertyChanged(nameof(EpgItems));
                    });
                });
#if DEBUG
                System.Diagnostics.Debug.WriteLine("EPG *loadEpgFavParse = IsComplette");
#endif
            }
        }
        #endregion
    }
}
