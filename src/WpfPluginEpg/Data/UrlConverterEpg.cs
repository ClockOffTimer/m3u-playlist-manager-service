﻿/* Copyright (c) 2021 WpfPluginEpg, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Globalization;
using System.Windows.Data;

namespace WpfPlugin.Data
{
    [ValueConversion(typeof(string), typeof(string))]
    public class UrlConverterEpg : IValueConverter
    {
        public readonly static string DefaultImageUri = "pack://application:,,,/WpfPluginEpg;component/Resources/notfound.png";
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            do
            {
                if (value == null)
                    break;

                var u = new Uri(value as string, UriKind.Absolute);
                if (u == null)
                    break;
                return u.AbsoluteUri;
            } while (false);
            return new Uri(DefaultImageUri, UriKind.Absolute);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }


}
