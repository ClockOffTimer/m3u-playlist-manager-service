﻿/* Copyright (c) 2021 WpfPluginEpg, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Speech.Synthesis;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using PlayListServiceData.Container.Items;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public partial class PageEpgData : INotifyPropertyChanged
    {
        public enum TypeActions : int
        {
            NONE,
            LOAD_EPG,
            PANEL_BTN,
            EXPAND_CLOSE_PROG,
            EXPAND_CLOSE_CHAN,
            SPEECH_ENABLE,
            SPEECH_STOP,
            SPEECH_CONTINUE,
            SPEECH_ERASE,
            SPEECH_VOLUME,
            SPEECH_SETTINGS,
            PANEL_TYPE_V,
            PANEL_TYPE_H
        };
        private CancellationTokenSource tokenSourceCancel = default;
        private CancellationTokenSource tokenSpeechCancel = default; // TO-DO
        private EpgPanelVertical panelVertical = default;
        private EpgPanelHorizontal panelHorizontal = default;
        private readonly Queue speechQueue = new Queue();
        private SpeechSynthesizer speechSynth = default;

        private IServiceControls _ServiceCtrl = default;
        public IServiceControls ServiceCtrl
        {
            get { return _ServiceCtrl; }
            private set { _ServiceCtrl = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageEpgData()
        {
            tokenSourceCancel = new CancellationTokenSource();
        }
        ~PageEpgData()
        {
            TokenDispose();
            if (speechSynth != default)
                try { speechSynth.Pause(); speechSynth.Dispose(); speechSynth = null; } catch { }
        }
        public void SetServiceCtrl(IServiceControls controls, EpgPanelVertical pv, EpgPanelHorizontal ph)
        {
            ServiceCtrl = controls;
            panelVertical = pv;
            panelHorizontal = ph;
        }

        #region get/set
        private ObservableCollection<ViewItemE<ViewSubItemE>> EpgItems_ = new ObservableCollection<ViewItemE<ViewSubItemE>>();
        public ObservableCollection<ViewItemE<ViewSubItemE>> EpgItems
        {
            get { return EpgItems_; }
            set
            {
                if (EpgItems_ == value)
                    return;
                EpgItems_ = value;
                OnPropertyChanged(nameof(IsEpgActions));
            }
        }

        private bool IsInfoExpanded_ = true;
        public bool IsInfoExpanded
        {
            get { return IsInfoExpanded_; }
            set
            {
                if (IsInfoExpanded_ == value)
                    return;
                IsInfoExpanded_ = value;
                OnPropertyChanged(nameof(IsInfoExpanded));
            }
        }
        private bool GuiLoadVisible_ = false;
        public bool GuiLoadVisible
        {
            get { return GuiLoadVisible_; }
            set
            {
                if (GuiLoadVisible_ == value)
                    return;
                GuiLoadVisible_ = value;
                OnPropertyChanged(nameof(GuiLoadVisible));
                OnPropertyChanged(nameof(GuiNoLoadVisible));
            }
        }
        public bool GuiNoLoadVisible
        {
            get { return !GuiLoadVisible_; }
        }
        private bool IsLoadingEpgData_ = false;
        public bool IsLoadingEpgData
        {
            get { return IsLoadingEpgData_; }
            set
            {
                if (IsLoadingEpgData_ == value)
                    return;
                IsLoadingEpgData_ = value;
                OnPropertyChanged(nameof(IsLoadingEpgData));
            }
        }
        private bool IsSpeechEnable_ = true;
        public bool IsSpeechEnable
        {
            get { return IsSpeechEnable_; }
            set
            {
                if (IsSpeechEnable_ == value)
                    return;
                IsSpeechEnable_ = value;
                OnPropertyChanged(nameof(IsSpeechEnable));
                OnPropertyChanged(nameof(IsSpeechQueue));
            }
        }
        private bool IsSpeechRun_ = false;
        public bool IsSpeechRun
        {
            get { return IsSpeechEnable_ && IsSpeechRun_; }
            set
            {
                if (IsSpeechRun_ == value)
                    return;
                IsSpeechRun_ = value;
                if (!IsSpeechRun_)
                    IsSpeechSettings = false;
                OnPropertyChanged(nameof(IsSpeechRun));
                OnPropertyChanged(nameof(IsSpeechNotRun));
                OnPropertyChanged(nameof(IsSpeechQueue));
            }
        }
        public bool IsSpeechNotRun
        {
            get { return !IsSpeechRun; }
        }
        private bool IsSpeechSettings_ = false;
        public bool IsSpeechSettings
        {
            get { return !IsSpeechRun && IsSpeechSettings_; }
            set
            {
                if (IsSpeechSettings_ == value)
                    return;
                IsSpeechSettings_ = value;
                if (IsSpeechSettings_)
                    IsInfoExpanded = false;
                OnPropertyChanged(nameof(IsSpeechSettings));
                OnPropertyChanged(nameof(SpeechSettingVisibility));
            }
        }
        private bool IsSpeechPlay_ = false;
        public bool IsSpeechPlay
        {
            get { return IsSpeechEnable_ && IsSpeechPlay_; }
            set
            {
                if (IsSpeechPlay_ == value)
                    return;
                IsSpeechPlay_ = value;
                if (IsSpeechPlay_)
                    IsSpeechSettings = false;
                OnPropertyChanged(nameof(IsSpeechPlay));
                OnPropertyChanged(nameof(SpeechHeaderVisibility));
            }
        }
        public bool IsSpeechQueue
        {
            get { return (speechQueue.Count > 0) && (!IsSpeechPlay) && IsSpeechEnable; }
        }
        private TypeActions TypePanel_ = TypeActions.PANEL_TYPE_V;
        public TypeActions TypePanel
        {
            get { return TypePanel_; }
            set
            {
                if (TypePanel_ == value)
                    return;
                TypePanel_ = value;
                OnPropertyChanged(nameof(TypePanel));
            }
        }
        public bool IsEpgActions
        {
            get { return EpgItems.Count > 0; }
        }
        private int SpeechProgressIdx_ = 0;
        public int SpeechProgressIdx
        {
            get { return SpeechProgressIdx_; }
            set
            {
                if (SpeechProgressIdx_ == value)
                    return;
                SpeechProgressIdx_ = value;
                OnPropertyChanged(nameof(SpeechProgressIdx));
            }
        }
        private int SpeechProgressMax_ = 0;
        public int SpeechProgressMax
        {
            get { return SpeechProgressMax_; }
            set
            {
                if (SpeechProgressMax_ == value)
                    return;
                SpeechProgressMax_ = value;
                OnPropertyChanged(nameof(SpeechProgressMax));
            }
        }
        private string SpeechSelectedItem_ = default;
        public string SpeechSelectedItem
        {
            get { return SpeechSelectedItem_; }
            set
            {
                if (SpeechSelectedItem_ == value)
                    return;
                SpeechSelectedItem_ = value;
                OnPropertyChanged(nameof(SpeechSelectedItem));
            }
        }
        private string SpeechTitle_ = default;
        public string SpeechTitle
        {
            get { return SpeechTitle_; }
            set
            {
                if (SpeechTitle_ == value)
                    return;
                SpeechTitle_ = value;
                OnPropertyChanged(nameof(SpeechTitle));
            }
        }
        private int SpeechVolume_ = 55;
        public int SpeechVolume
        {
            get { return SpeechVolume_; }
            set
            {
                if (SpeechVolume_ == value)
                    return;
                SpeechVolume_ = value;
                OnPropertyChanged(nameof(SpeechVolume));
                CmdActions(TypeActions.SPEECH_VOLUME, new Tuple<int>(SpeechVolume_));
            }
        }

        private ObservableCollection<string> SpeechVoiceItems_ = new ObservableCollection<string>();
        public ObservableCollection<string> SpeechVoiceItems
        {
            get { return SpeechVoiceItems_; }
            set
            {
                if (SpeechVoiceItems_ == value)
                    return;
                SpeechVoiceItems_ = value;
                OnPropertyChanged(nameof(SpeechVoiceItems));
            }
        }

        public Visibility PanelHVisibility
        {
            get { return (TypePanel == TypeActions.PANEL_TYPE_H) ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility PanelVVisibility
        {
            get { return (TypePanel == TypeActions.PANEL_TYPE_V) ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility SpeechHeaderVisibility
        {
            get { return IsSpeechPlay ? Visibility.Visible : Visibility.Collapsed; }
        }
        public Visibility SpeechSettingVisibility
        {
            get { return IsSpeechSettings ? Visibility.Visible : Visibility.Collapsed; }
        }
        #endregion

        #region Load base Selector
        private void LoadSelector()
        {
            do
            {
                if (EpgItems.Count > 0)
                    break;

                LoadEpgData();
                return;

            } while (false);

            if (TypePanel == TypeActions.PANEL_TYPE_V)
                Application.Current.Dispatcher.Invoke(() => { panelVertical.ItemsSource = EpgItems; });
            else if (TypePanel == TypeActions.PANEL_TYPE_H)
                Application.Current.Dispatcher.Invoke(() => { panelHorizontal.ItemsSource = EpgItems; });

            Application.Current.Dispatcher.Invoke(() =>
            {
                IsLoadingEpgData = false;
                OnPropertyChanged(nameof(EpgItems));
                OnPropertyChanged(nameof(IsEpgActions));
                OnPropertyChanged(nameof(PanelHVisibility));
                OnPropertyChanged(nameof(PanelVVisibility));
            });
        }
        #endregion

        #region Token Dispose
        private void TokenDispose()
        {
            CancellationTokenSource cs = tokenSourceCancel;
            tokenSourceCancel = default;
            if (cs != default)
            {
                try
                {
                    cs.Cancel();
                    cs.Dispose();
                }
                catch { }
            }
            cs = tokenSpeechCancel;
            tokenSpeechCancel = default;
            if (cs != default)
            {
                try
                {
                    cs.Cancel();
                    cs.Dispose();
                }
                catch { }
            }
        }
        #endregion

        #region Command Actions
        public void CmdActions<T1>(TypeActions t, T1 obj = default(T1))
            where T1 : class
        {
            switch (t)
            {
                case TypeActions.SPEECH_ENABLE:
                    {
                        IsSpeechEnable = !IsSpeechEnable;
                        break;
                    }
                case TypeActions.SPEECH_STOP:
                    {
                        if (!IsSpeechEnable)
                            break;

                        if (IsSpeechRun)
                        {
                            try
                            {
                                if (IsSpeechPlay && (speechSynth != default))
                                {
                                    speechSynth.Pause();
                                    IsSpeechPlay = false;
                                }
                                else if (!IsSpeechPlay && (speechSynth != default))
                                {
                                    speechSynth.Resume();
                                    IsSpeechPlay = true;
                                }
                                else if (tokenSpeechCancel != default)
                                    tokenSpeechCancel.Cancel();
                            }
                            catch { }
                        }
                        else
                        {
                            if (speechQueue.Count > 0)
                                SpeechFromText();
                        }
                        OnPropertyChanged(nameof(IsSpeechEnable));
                        OnPropertyChanged(nameof(IsSpeechQueue));
                        OnPropertyChanged(nameof(IsSpeechPlay));
                        OnPropertyChanged(nameof(IsSpeechRun));
                        break;
                    }
                case TypeActions.SPEECH_ERASE:
                    {
                        if (!IsSpeechEnable)
                            break;

                        if (speechQueue.Count > 0)
                        {
                            try
                            {
                                if (IsSpeechRun && (tokenSpeechCancel != default))
                                    tokenSpeechCancel.Cancel();
                                speechQueue.Clear();
                            }
                            catch { }
                        }
                        OnPropertyChanged(nameof(IsSpeechEnable));
                        OnPropertyChanged(nameof(IsSpeechQueue));
                        OnPropertyChanged(nameof(IsSpeechRun));
                        OnPropertyChanged(nameof(IsSpeechPlay));
                        break;
                    }
                case TypeActions.SPEECH_VOLUME:
                    {
                        if (IsSpeechEnable && IsSpeechRun && (speechSynth != default))
                        {
                            if (obj is Tuple<int> tpl)
                            {
                                try
                                {
                                    speechSynth.Volume = tpl.Item1;
                                }
                                catch { }
                            }
                        }
                        break;
                    }
                case TypeActions.SPEECH_SETTINGS:
                    {
                        try
                        {
                            if (SpeechVoiceItems.Count == 0)
                            {
                                using (SpeechSynthesizer s = new SpeechSynthesizer())
                                {
                                    foreach (var i in s.GetInstalledVoices())
                                        Application.Current.Dispatcher.Invoke(() => { SpeechVoiceItems.Add(i.VoiceInfo.Name); });
                                }
                            }
                        }
                        catch { }
                        if (IsSpeechEnable && !IsSpeechRun)
                            IsSpeechSettings = !IsSpeechSettings;
                        break;
                    }
                case TypeActions.LOAD_EPG:
                    {
                        if (ServiceCtrl != default)
                            LoadEpgData();
                        break;
                    }
                case TypeActions.PANEL_BTN:
                    {
                        switch (TypePanel)
                        {
                            case TypeActions.PANEL_TYPE_V:
                                {
                                    if (panelHorizontal != default)
                                    {
                                        if (EpgItems.Count > 0)
                                            panelHorizontal.ItemsSource = EpgItems;
                                        TypePanel = TypeActions.PANEL_TYPE_H;
                                    }
                                    break;
                                }
                            case TypeActions.PANEL_TYPE_H:
                                {
                                    if (panelVertical != default)
                                    {
                                        if (EpgItems.Count > 0)
                                            panelVertical.ItemsSource = EpgItems;
                                        TypePanel = TypeActions.PANEL_TYPE_V;
                                    }
                                    break;
                                }
                        }
                        OnPropertyChanged(nameof(PanelHVisibility));
                        OnPropertyChanged(nameof(PanelVVisibility));
                        OnPropertyChanged(nameof(IsEpgActions));
                        break;
                    }
                case TypeActions.EXPAND_CLOSE_CHAN:
                    {
                        if (TypePanel == TypeActions.PANEL_TYPE_H)
                            panelHorizontal.CloseChannel();
                        else if (TypePanel == TypeActions.PANEL_TYPE_V)
                            panelVertical.CloseChannel();
                        break;
                    }
                case TypeActions.EXPAND_CLOSE_PROG:
                    {
                        if (TypePanel == TypeActions.PANEL_TYPE_H)
                            panelHorizontal.CloseProgramm();
                        else if (TypePanel == TypeActions.PANEL_TYPE_V)
                            panelVertical.CloseProgramm();
                        break;
                    }
            }
        }
        #endregion

        #region event control
        public void local_EventLoad(bool b)
        {
            GuiLoadVisible = b;
        }
        #endregion

        #region event Speech to Queue
        public void local_EventSelector(object obj)
        {
            if (!IsSpeechEnable)
                return;

            try
            {
                if (obj is ViewItemE<ViewSubItemE> r3)
                    foreach (var i in r3.IfListItems)
                        speechQueue.Enqueue($"{i.Title}. {i.Desc}");
                else if (obj is ViewItemE<ViewHeaderItemE> r4)
                    foreach (var i in r4.IfListItems)
                    {
                        speechQueue.Enqueue($"{i.DayTime}");
                        foreach (var n in i.IfListItems)
                            speechQueue.Enqueue($"{n.Title}. {n.Desc}");
                    }
                else if (obj is ViewHeaderItemE r1)
                    foreach (var i in r1.IfListItems)
                        speechQueue.Enqueue($"{i.Title}. {i.Desc}");
                else if (obj is ViewSubItemE r2)
                    speechQueue.Enqueue($"{r2.Title}. {r2.Desc}");

            }
            catch { }

            if ((speechQueue.Count > 0) && !IsSpeechRun)
                SpeechFromText();
        }
        #endregion

        #region Speech from text Queue
        private async void SpeechFromText()
        {
            if (IsSpeechRun)
                return;
            IsSpeechRun = true;

            if (tokenSpeechCancel != null)
                try { tokenSpeechCancel.Dispose(); } catch { }
            tokenSpeechCancel = new CancellationTokenSource();
            IsInfoExpanded = false;

            await Task.Run(() =>
            {
                try
                {
                    CancellationToken token = tokenSpeechCancel.Token;
                    using (SpeechSynthesizer s = new SpeechSynthesizer())
                    {
                        int cnt = 0, vol = 0;
                        s.SetOutputToDefaultAudioDevice();
                        s.SpeakProgress += (a, b) =>
                        {
                            if (cnt == 0)
                                return;
                            SpeechProgressIdx = cnt - (cnt - b.CharacterPosition);
                            SpeechProgressMax = cnt;
                        };
                        speechSynth = s;
                        Application.Current.Dispatcher.Invoke(() => { IsSpeechPlay = true; });
                        while (speechQueue.Count > 0)
                        {
                            if (token.IsCancellationRequested)
                                break;
                            var text = speechQueue.Dequeue() as string;
                            if (text == default)
                                continue;

                            try
                            {
                                if (SpeechSelectedItem != default)
                                {
                                    speechSynth.SelectVoice(SpeechSelectedItem);
                                    Application.Current.Dispatcher.Invoke(() => { SpeechSelectedItem = default; });
                                }
                                if (vol != SpeechVolume)
                                {
                                    vol = s.Volume;
                                    s.Volume = SpeechVolume;
                                }
                                Application.Current.Dispatcher.Invoke(() => { SpeechTitle = SpeechTitleText(text); });
                            }
                            catch { }
                            cnt = text.Length;
                            s.Speak(text);
                        }
                        speechSynth = default;
                        Application.Current.Dispatcher.Invoke(() => { IsSpeechPlay = false; });
                    }
                }
#if DEBUG
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                catch { }
#endif
                finally
                {
                    CancellationTokenSource cts = tokenSpeechCancel;
                    tokenSpeechCancel = null;
                    if (cts != default)
                        try { cts.Dispose(); } catch { }
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        IsSpeechRun = false;
                        IsInfoExpanded = true;
                    });
                }
            });
        }
        #endregion

        #region Speech title formater
        private string SpeechTitleText(string s)
        {
            if (s.Length > 20)
                return $"{s.Substring(0, 20)}..";
            else
                return s;
        }
        #endregion
    }
}
