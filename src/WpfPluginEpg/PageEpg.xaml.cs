﻿/* Copyright (c) 2021 WpfPluginEpg, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel.Composition;
using System.Windows;
using System.Windows.Controls;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
       ExportMetadata("Id", 6),
       ExportMetadata("pluginType", PluginType.PLUG_PLACE_TAB)]
    public partial class PageEpg : Page, IPlugin
    {
        private PageEpgData pageEpgData { get; set; } = default;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageEpg(IServiceControls controls)
        {
            InitializeComponent();
            pageEpgData = (PageEpgData)DataContext;
            pageEpgData.SetServiceCtrl(controls, EPGPanelVertical, EPGPanelHorizontal);
            EPGPanelHorizontal.EventLoad += pageEpgData.local_EventLoad;
            EPGPanelHorizontal.EventSelect += pageEpgData.local_EventSelector;
            EPGPanelVertical.EventLoad += pageEpgData.local_EventLoad;
            EPGPanelVertical.EventSelect += pageEpgData.local_EventSelector;
        }

        private void Button_load_epg_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.GuiLoadVisible = true;
            pageEpgData.CmdActions(PageEpgData.TypeActions.LOAD_EPG, default(string));
        }
        private void Button_panel_type_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.PANEL_BTN, default(string));
        }
        private void Button_expand_close_chan_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.EXPAND_CLOSE_CHAN, default(string));
        }
        private void Button_expand_close_prog_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.EXPAND_CLOSE_PROG, default(string));
        }
        private void Button_speech_enable_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.SPEECH_ENABLE, default(string));
        }
        private void Button_speech_pause_play_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.SPEECH_STOP, default(string));
        }
        private void Button_speech_erase_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.SPEECH_ERASE, default(string));
        }

        private void Button_speech_settings_Click(object sender, RoutedEventArgs e)
        {
            pageEpgData.CmdActions(PageEpgData.TypeActions.SPEECH_SETTINGS, default(string));
        }
    }
}
