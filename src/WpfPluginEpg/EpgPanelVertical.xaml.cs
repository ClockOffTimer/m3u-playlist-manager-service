﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using PlayListServiceData.Container.Items;

namespace WpfPlugin
{
    public partial class EpgPanelVertical : UserControl, INotifyPropertyChanged
    {
        public EpgPanelVertical()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        #region DependencyProperty
        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); OnPropertyChanged(nameof(Width)); }
        }
        public static readonly new DependencyProperty WidthProperty = DependencyProperty.Register(
            "Width",
            typeof(double),
            typeof(EpgPanelVertical),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinWidth
        {
            get { return (double)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); OnPropertyChanged(nameof(MinWidth)); }
        }
        public static readonly new DependencyProperty MinWidthProperty = DependencyProperty.Register(
            "MinWidth",
            typeof(double),
            typeof(EpgPanelVertical),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); OnPropertyChanged(nameof(Height)); }
        }
        public static readonly new DependencyProperty HeightProperty = DependencyProperty.Register(
            "Height",
            typeof(double),
            typeof(EpgPanelVertical),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinHeight
        {
            get { return (double)GetValue(MinHeightProperty); }
            set { SetValue(MinHeightProperty, value); OnPropertyChanged(nameof(MinHeight)); }
        }
        public static readonly new DependencyProperty MinHeightProperty = DependencyProperty.Register(
            "MinHeight",
            typeof(double),
            typeof(EpgPanelVertical),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public new Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); OnPropertyChanged(nameof(Visibility)); }
        }
        public static readonly new DependencyProperty VisibilityProperty = DependencyProperty.Register(
            "Visibility",
            typeof(Visibility),
            typeof(EpgPanelVertical),
            new FrameworkPropertyMetadata(Visibility.Collapsed, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        #endregion

        #region setter callback
        private async void CollectionChanged(ObservableCollection<ViewItemE<ViewSubItemE>> items)
        {
            if ((items == default) || (items.Count == 0))
                return;

            Application.Current.Dispatcher.Invoke(() =>
            {
                EventLoad?.Invoke(true);
                viewItems.Clear();
            });

            await Task.Run(() =>
            {
                try
                {
                    DateTime dz, de, dt = DateTime.Now;
                    dz = de = dt.Subtract(new TimeSpan(dt.Hour, dt.Minute, dt.Second));
                    de = de.AddDays(1);

                    foreach (var i in items)
                    {
                        if (i.IfListItems.Count == 0)
                            continue;

                        try
                        {
                            DateTime dend = de, dday = dz;
                            ViewItemE<ViewHeaderItemE> item = new ViewItemE<ViewHeaderItemE>(i);
                            ViewHeaderItemE header = new ViewHeaderItemE(dday);

                            foreach (var n in i.IfListItems)
                            {
                                try
                                {
                                    if (n.Begin < dt)
                                        continue;

                                    if (n.End > dend)
                                    {
                                        if (header.IfListItems.Count > 0)
                                            item.Add(header);
                                        dend = dend.AddDays(1);
                                        dday = dday.AddDays(1);
                                        header = new ViewHeaderItemE(dday);
                                    }
                                    header.Add(n);
                                }
#if DEBUG
                                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                                catch { }
#endif
                            }
                            if (header.IfListItems.Count > 0)
                                item.Add(header);
                            if (item.IfListItems.Count > 0)
                                Application.Current.Dispatcher.Invoke(() => { viewItems.Add(item); });
                        }
#if DEBUG
                        catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                        catch { }
#endif
                    }
                }
#if DEBUG
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                catch { }
#endif
                finally
                {
                    if (viewItems.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EventLoad?.Invoke(false);
                            OnPropertyChanged(nameof(viewItems));
                            ItemsSource = default;
                        });
                    }
                }
            });
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public delegate void LoadEvents(bool isload);
        public event LoadEvents EventLoad = delegate { };

        public delegate void SelectEvents(object obj);
        public event SelectEvents EventSelect = delegate { };

        #region get/set
        private object SelectedItem_ = default;
        public object SelectedItem
        {
            get { return SelectedItem_; }
            set
            {
                if (SelectedItem_ == value)
                    return;
                SelectedItem_ = value;
                OnPropertyChanged(nameof(SelectedItem));
                EventSelect?.Invoke(SelectedItem_);
            }
        }
        private ObservableCollection<ViewItemE<ViewSubItemE>> ItemsSource_ = default;
        public ObservableCollection<ViewItemE<ViewSubItemE>> ItemsSource
        {
            get { return ItemsSource_; }
            set
            {
                if (ItemsSource_ == value)
                    return;
                ItemsSource_ = value;
                OnPropertyChanged(nameof(ItemsSource));
                CollectionChanged(ItemsSource);
            }
        }
        private int SelectedIndex_ = -1;
        public int SelectedIndex
        {
            get { return SelectedIndex_; }
            set
            {
                if (SelectedIndex_ == value)
                    return;
                SelectedIndex_ = value;
                OnPropertyChanged(nameof(SelectedIndex));
                OnPropertyChanged(nameof(viewDayItems));
            }
        }
        private ObservableCollection<ViewItemE<ViewHeaderItemE>> viewItems_ = new ObservableCollection<ViewItemE<ViewHeaderItemE>>();
        public ObservableCollection<ViewItemE<ViewHeaderItemE>> viewItems
        {
            get { return viewItems_; }
            set
            {
                if (viewItems_ == value)
                    return;
                viewItems_ = value;
                if (SelectedIndex != -1)
                    SelectedIndex = -1;

                OnPropertyChanged(nameof(viewItems));
                OnPropertyChanged(nameof(viewDayItems));
            }
        }
        public ObservableCollection<ViewHeaderItemE> viewDayItems
        {
            get { return ((viewItems.Count == 0) || (SelectedIndex < 0)) ? default : viewItems[SelectedIndex].IfListItems; }
        }

        private string viewDate_ = default;
        public string viewDate
        {
            get { return viewDate_; }
            set
            {
                if (viewDate_ == value)
                    return;
                viewDate_ = value;
                OnPropertyChanged(nameof(viewDate));
            }
        }
        #endregion

        public void CloseChannel()
        {
            if (viewDayItems == default)
                return;

            foreach (var i in viewDayItems)
                if (i.IsExpanded)
                    Application.Current.Dispatcher.Invoke(() => { i.IsExpanded = false; });
            Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(viewDayItems)); });
        }
        public void CloseProgramm()
        {
            if (viewDayItems == default)
                return;

            foreach (var i in viewDayItems)
                foreach (var n in i.IfListItems)
                    if (n.IsExpanded)
                        Application.Current.Dispatcher.Invoke(() => { n.IsExpanded = false; });
            Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(viewDayItems)); });
        }
    }
}
