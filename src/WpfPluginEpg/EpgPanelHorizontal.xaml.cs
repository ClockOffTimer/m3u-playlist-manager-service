﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using PlayListServiceData.Container.Items;

namespace WpfPlugin
{
    public partial class EpgPanelHorizontal : UserControl, INotifyPropertyChanged
    {
        public EpgPanelHorizontal()
        {
            InitializeComponent();
            LayoutRoot.DataContext = this;
        }

        #region DependencyProperty
        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set { SetValue(WidthProperty, value); OnPropertyChanged(nameof(Width)); }
        }
        public static readonly new DependencyProperty WidthProperty = DependencyProperty.Register(
            "Width",
            typeof(double),
            typeof(EpgPanelHorizontal),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinWidth
        {
            get { return (double)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); OnPropertyChanged(nameof(MinWidth)); }
        }
        public static readonly new DependencyProperty MinWidthProperty = DependencyProperty.Register(
            "MinWidth",
            typeof(double),
            typeof(EpgPanelHorizontal),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set { SetValue(HeightProperty, value); OnPropertyChanged(nameof(Height)); }
        }
        public static readonly new DependencyProperty HeightProperty = DependencyProperty.Register(
            "Height",
            typeof(double),
            typeof(EpgPanelHorizontal),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        [TypeConverterAttribute(typeof(LengthConverter))]
        public new double MinHeight
        {
            get { return (double)GetValue(MinHeightProperty); }
            set { SetValue(MinHeightProperty, value); OnPropertyChanged(nameof(MinHeight)); }
        }
        public static readonly new DependencyProperty MinHeightProperty = DependencyProperty.Register(
            "MinHeight",
            typeof(double),
            typeof(EpgPanelHorizontal),
            new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public new Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); OnPropertyChanged(nameof(Visibility)); }
        }
        public static readonly new DependencyProperty VisibilityProperty = DependencyProperty.Register(
            "Visibility",
            typeof(Visibility),
            typeof(EpgPanelHorizontal),
            new FrameworkPropertyMetadata(Visibility.Collapsed, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        #endregion

        #region setter callback
        private async void CollectionChanged(ObservableCollection<ViewItemE<ViewSubItemE>> items)
        {
            if ((items == default) || (items.Count == 0))
                return;

            Application.Current.Dispatcher.Invoke(() =>
            {
                viewItems.Clear();
                EventLoad?.Invoke(true);
                viewDate = DateTime.Now.ToString("dddd, dd MMMM");
            });

            await Task.Run(() =>
            {
                try
                {
                    DateTime dt = DateTime.Now;
                    DateTime de = dt.Subtract(new TimeSpan(dt.Hour, dt.Minute, dt.Second));
                    de = de.AddDays(1);
                    de = de.AddMinutes(30);

                    foreach (var i in items)
                    {
                        if (i.IfListItems.Count == 0)
                            continue;
                        try
                        {
                            var item = new ViewItemE<ViewSubItemE>(i);
                            foreach (var n in i.IfListItems)
                            {
                                if (n.End > de)
                                    break;
                                if (n.Begin >= dt)
                                    item.Add(n);
                            }
                            if (item.IfListItems.Count > 0)
                                Application.Current.Dispatcher.Invoke(() => { viewItems.Add(item); });
                        }
#if DEBUG
                        catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                        catch { }
#endif
                    }
                }
#if DEBUG
                catch (Exception ex) { System.Diagnostics.Debug.WriteLine(ex); }
#else
                catch { }
#endif
                finally
                {
                    if (viewItems.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            EventLoad?.Invoke(false);
                            OnPropertyChanged(nameof(viewItems));
                            ItemsSource = default;
                        });
                    }
                }
            });
        }
        #endregion

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public delegate void LoadEvents(bool isload);
        public event LoadEvents EventLoad = delegate { };

        public delegate void SelectEvents(object obj);
        public event SelectEvents EventSelect = delegate { };

        #region get/set
        private object SelectedItem_ = default;
        public object SelectedItem
        {
            get { return SelectedItem_; }
            set
            {
                if (SelectedItem_ == value)
                    return;
                SelectedItem_ = value;
                OnPropertyChanged(nameof(SelectedItem));
                EventSelect?.Invoke(SelectedItem_);
            }
        }
        private ObservableCollection<ViewItemE<ViewSubItemE>> ItemsSource_ = default;
        public ObservableCollection<ViewItemE<ViewSubItemE>> ItemsSource
        {
            get { return ItemsSource_; }
            set
            {
                if (ItemsSource_ == value)
                    return;
                ItemsSource_ = value;
                OnPropertyChanged(nameof(ItemsSource));
                CollectionChanged(ItemsSource);
            }
        }
        private ObservableCollection<ViewItemE<ViewSubItemE>> viewItems_ = new ObservableCollection<ViewItemE<ViewSubItemE>>();
        public ObservableCollection<ViewItemE<ViewSubItemE>> viewItems
        {
            get { return viewItems_; }
            set
            {
                if (viewItems_ == value)
                    return;
                viewItems_ = value;
                OnPropertyChanged(nameof(viewItems));
            }
        }
        public string viewDate_ = default;
        public string viewDate
        {
            get { return viewDate_; }
            set
            {
                if (viewDate_ == value)
                    return;
                viewDate_ = value;
                OnPropertyChanged(nameof(viewDate));
            }
        }
        #endregion

        public void CloseChannel()
        {
            if (viewItems == default)
                return;

            foreach (var i in viewItems)
                if (i.IsExpanded)
                    Application.Current.Dispatcher.Invoke(() => { i.IsExpanded = false; });
            Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(viewItems)); });
        }
        public void CloseProgramm()
        {
            if (viewItems == default)
                return;

            foreach (var i in viewItems)
                foreach (var n in i.IfListItems)
                    if (n.IsExpanded)
                        Application.Current.Dispatcher.Invoke(() => { n.IsExpanded = false; });
            Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(viewItems)); });
        }
    }
}
