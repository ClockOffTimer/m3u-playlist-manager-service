
const CACHEPREF = 'app';
const CACHESUF  = 'cache';
const CACHENAME = 'wcplm3u';
const CACHEREPO = `${CACHEPREF}-${CACHENAME}-${CACHESUF}`;
const OFFLINEURL = '/data/pwa/offline';
const ROOTURL = '/';
const FILEREVISION = '495677';

importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.3/workbox-sw.js');
//self.__WB_DISABLE_DEV_LOGS = true;

const authClearPlugin = {
    cacheWillUpdate: async ({ res }) => {
        if ((res.status === 401) || (res.status === 403) || (res.status === 407)) {
            await caches.open(CACHEREPO).then(cache => { cache.delete(res.url); });
            return null;
        }
        return res;
    }
};

const cacheUrls = [
    { url: '/', revision: FILEREVISION },
    { url: '/data/js', revision: FILEREVISION },
    { url: '/data/css', revision: FILEREVISION },
    { url: '/data/workboxjs', revision: FILEREVISION },
    { url: '/data/manifest', revision: FILEREVISION },
    { url: '/data/pwa/offline', revision: FILEREVISION },
    { url: '/data/images/notrunning/400', revision: FILEREVISION },
    { url: 'https://code.jquery.com/jquery-2.2.4.min.js', revision: null },
    { url: 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/fonts/bootstrap-icons.woff2', revision: null },
    { url: 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/fonts/bootstrap-icons.woff', revision: null },
    { url: 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js', revision: null },
    { url: 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css', revision: null },
    { url: 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css', revision: null },
    { url: 'https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.11/jsrender.min.js', revision: null },
    { url: 'https://cdnjs.cloudflare.com/ajax/libs/hls.js/1.0.10/hls.min.js', revision: null },
    { url: 'https://cdn.dashjs.org/latest/dash.all.min.js', revision: null }
];

if (workbox) {

    workbox.setConfig({
        debug: true
    });
    workbox.core.setCacheNameDetails({
        prefix: CACHEPREF,
        suffix: CACHESUF,
        precache: CACHENAME
    });
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/^\/$/),
        new workbox.strategies.NetworkFirst()
    );
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/^\/cmd\/.*$/),
        new workbox.strategies.CacheOnly()
    );
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/^\/cmdZzzzzzzzzzzz\/.*$/),
        new workbox.strategies.NetworkOnly({
            plugins: [authClearPlugin]
        })
    );
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/^\/(?:video|audio|m3u)$/),
        new workbox.strategies.CacheOnly()
    );
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/^\/data\/images\/(.*)\/(.*)/),
        new workbox.strategies.StaleWhileRevalidate()
    );
    workbox.routing.registerRoute(
        ({ url }) => url.pathname.match(/(.*)\.(?:png|gif|jpg|ico)/),
        new workbox.strategies.StaleWhileRevalidate()
    );

    self.addEventListener('install', (ev) => {
        /* console.log('[SW Install]'); */

        ev.waitUntil((async () => {
            await caches.open(CACHEREPO).then((cache) => {
                cache.addAll(
                    cacheUrls.map(u => new Request(u.url, { cache: 'reload', credentials: 'same-origin' }))
                );
            });
        })());

        self.skipWaiting();
    });
    self.addEventListener('activate', (ev) => {
        /* console.log('[SW Activate]'); */

        if ('navigationPreload' in self.registration)
            ev.waitUntil((async () => {
                await self.registration.navigationPreload.enable();
            })());

        self.clients.claim();
    });
    self.addEventListener('fetch', (ev) => {
        console.log('[SW] Fetch match:', ev.request.url);
        ev.respondWith(
            (async () => {
                let response = {};
                const request = ev.request;
                request.credentials = 'same-origin';

                try {
                    if (request.url.startsWith('/cmd/') || request.url.match(/^\/(?:video|audio|m3u)$/)) {
                        response = await fetch(request, { credentials: 'same-origin' });
                        console.log('[SW] Fetch (new request 1)', request.url, request, response);
                        //if (response.ok)
                            return response;
                    }
                    try {
                        response = await ev.preloadResponse;
                        if (response) {
                            console.log('[SW] Fetch (from cache)', request.url, request, response);
                            return response;
                        }
                    } catch (error) { console.log('[SW] Fetch (preload)', error); }

                    response = await fetch(request, { credentials: 'same-origin' });
                    console.log('[SW] Fetch (new request 2)', request.url, request, response);
                    //if (response.ok)
                        return response;

                    //response = await caches.open(CACHEREPO).then(cache => { return cache(request.url); });
                    return response;

                } catch (error) {
                    console.log('[SW] Fetch (fetch) error', error, response);
                    if ((response !== null) && response.hasOwnProperty('status') && (response.status === 401))
                        return new Response("Welcome to M3U service dash.", {
                            status: 401,
                            headers: {
                                'WWW-Authenticate': 'Basic realm="Log to M3U Playlist service"'
                            }
                        });
                    else
                        await self.clients.matchAll().then((clients) => {
                            clients.forEach((client) => {
                                client.postMessage(JSON.stringify({
                                    type: 'offline',
                                    url: ev.request.url,
                                    mode: ev.request.mode
                                }));
                            });
                        });
                    return false;
                }
            }) ());
    });
    self.addEventListener('message', (ev) => {
        /* console.log('[SW] Message'); */

        ev.waitUntil((async () => {
            if (ev.data === 'skipWaiting')
                self.skipWaiting();
            else if (ev.data.type === 'addChache') {
                caches.open(CACHEREPO)
                    .then((async (cache) => {
                        return cache.addAll(ev.data.payload);
                    })());
            }
            else {
                self.filesDir = ev.data.filesDir;
                await ev.ports[0].postMessage({ 'received': ev.data });
            }
        })());
    });

    /* Notification */
    if ((typeof (Notification) !== 'undefined') &&
        ('showNotification' in ServiceWorkerRegistration.prototype)) {

        self.addEventListener('notificationclick', (ev) => {
            /* console.log('[SW Notify click]', ev); */

            if (ev.action === 'close')
                ev.waitUntil((async () => {
                    await ev.notification.close();
                })());
        });

        self.addEventListener('push', (ev) => {
            /* console.log(`[SW Push] ${Notification.permission}`, ev); */

            if (Notification.permission !== 'granted')
                return;

            let opt = {};
            try {
                const msg = JSON.parse(ev.data.json());
                if (msg === null)
                    throw new Error('Json is null');
                opt = {
                    title: (msg.hasOwnProperty('title')) ? msg.title : 'M3U Playlist service',
                    body:  (msg.hasOwnProperty('body'))  ? msg.body : '..',
                    icon:  (msg.hasOwnProperty('icon'))  ? msg.icon : '/data/images/logo/144',
                    image: (msg.hasOwnProperty('image')) ? msg.image : '',
                    vibrate: [100, 50, 100],
                    data: {
                        dateOfArrival: Date.now(),
                        primaryKey: '2'
                    },
                };
            } catch (error) {
                console.log('[SW Push] (json) error', error);
                try {
                    opt = {
                        title: 'M3U Playlist service',
                        icon: '/data/images/logo/144',
                        body: ev.data.text(),
                        vibrate: [100, 50, 100],
                        data: {
                            dateOfArrival: Date.now(),
                            primaryKey: '2'
                        },
                    };
                } catch (error) {
                    console.log('[SW Push] (text) error', error);
                    return;
                }
            }
            ev.waitUntil((async () => {
                await self.registration.showNotification(opt);
            })());
        });
    }
}

