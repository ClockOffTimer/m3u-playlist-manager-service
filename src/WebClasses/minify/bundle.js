
const b = /^(b|B)$/,
	symbol = {
		iec: {
			bits: ["bit", "Kibit", "Mibit", "Gibit", "Tibit", "Pibit", "Eibit", "Zibit", "Yibit"],
			bytes: ["B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"]
		},
		jedec: {
			bits: ["bit", "Kbit", "Mbit", "Gbit", "Tbit", "Pbit", "Ebit", "Zbit", "Ybit"],
			bytes: ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"]
		}
	},
	fullform = {
		iec: ["", "kibi", "mebi", "gibi", "tebi", "pebi", "exbi", "zebi", "yobi"],
		jedec: ["", "kilo", "mega", "giga", "tera", "peta", "exa", "zetta", "yotta"]
	},
	roundingFuncs = {
		floor: Math.floor,
		ceil: Math.ceil
	};

function filesize (arg, descriptor = {}) {
	let result = [],
		val = 0,
		e, base, bits, ceil, full, fullforms, locale, localeOptions, neg, num, output, pad, round, u, unix, separator, spacer, standard, symbols, roundingFunc, precision;

	if (isNaN(arg)) {
		throw new TypeError("Invalid number");
	}

	bits = descriptor.bits === true;
	unix = descriptor.unix === true;
	pad = descriptor.pad === true;
	base = descriptor.base || 10;
	round = descriptor.round !== void 0 ? descriptor.round : unix ? 1 : 2;
	locale = descriptor.locale !== void 0 ? descriptor.locale : "";
	localeOptions = descriptor.localeOptions || {};
	separator = descriptor.separator !== void 0 ? descriptor.separator : "";
	spacer = descriptor.spacer !== void 0 ? descriptor.spacer : unix ? "" : " ";
	symbols = descriptor.symbols || {};
	standard = base === 2 ? descriptor.standard || "iec" : "jedec";
	output = descriptor.output || "string";
	full = descriptor.fullform === true;
	fullforms = descriptor.fullforms instanceof Array ? descriptor.fullforms : [];
	e = descriptor.exponent !== void 0 ? descriptor.exponent : -1;
	roundingFunc = roundingFuncs[descriptor.roundingMethod] || Math.round;
	num = Number(arg);
	neg = num < 0;
	ceil = base > 2 ? 1000 : 1024;
	precision = isNaN(descriptor.precision) === false ? parseInt(descriptor.precision, 10) : 0;

	if (neg) {
		num = -num;
	}
	if (e === -1 || isNaN(e)) {
		e = Math.floor(Math.log(num) / Math.log(ceil));

		if (e < 0) {
			e = 0;
		}
	}
	if (e > 8) {
		if (precision > 0) {
			precision += 8 - e;
		}

		e = 8;
	}
	if (output === "exponent") {
		return e;
	}
	if (num === 0) {
		result[0] = 0;
		u = result[1] = unix ? "" : symbol[standard][bits ? "bits" : "bytes"][e];
	} else {
		val = num / (base === 2 ? Math.pow(2, e * 10) : Math.pow(1000, e));

		if (bits) {
			val = val * 8;

			if (val >= ceil && e < 8) {
				val = val / ceil;
				e++;
			}
		}

		const p = Math.pow(10, e > 0 ? round : 0);
		result[0] = roundingFunc(val * p) / p;

		if (result[0] === ceil && e < 8 && descriptor.exponent === void 0) {
			result[0] = 1;
			e++;
		}

		u = result[1] = base === 10 && e === 1 ? bits ? "kbit" : "kB" : symbol[standard][bits ? "bits" : "bytes"][e];

		if (unix) {
			result[1] = result[1].charAt(0);

			if (b.test(result[1])) {
				result[0] = Math.floor(result[0]);
				result[1] = "";
			}
		}
	}

	if (neg) {
		result[0] = -result[0];
	}
	if (precision > 0) {
		result[0] = result[0].toPrecision(precision);
	}
	result[1] = symbols[result[1]] || result[1];

	if (locale === true) {
		result[0] = result[0].toLocaleString();
	} else if (locale.length > 0) {
		result[0] = result[0].toLocaleString(locale, localeOptions);
	} else if (separator.length > 0) {
		result[0] = result[0].toString().replace(".", separator);
	}
	if (pad && Number.isInteger(result[0]) === false && round > 0) {
		const x = separator || ".",
			tmp = result[0].toString().split(x),
			s = tmp[1] || "",
			l = s.length,
			n = round - l;

		result[0] = `${tmp[0]}${x}${s.padEnd(l + n, "0")}`;
	}
	if (full) {
		result[1] = fullforms[e] ? fullforms[e] : fullform[standard][e] + (bits ? "bit" : "byte") + (result[0] === 1 ? "" : "s");
	}
	return output === "array" ? result : output === "object" ? {value: result[0], symbol: result[1], exponent: e, unit: u} : result.join(spacer);
}

filesize.partial = opt => arg => filesize(arg, opt);



(function (root, factoryT) {
	if (typeof define === "function" && define.amd) {
		define([], factoryT);
	} else if (typeof exports === "object") {
		exports = factoryT(); //module.
	} else {
		root.X2JS = factoryT();
	}
}(this, function () {
	return function (config) {
		'use strict';

		var VERSION = "1.2.0";

		config = config || {};
		initConfigDefaults();
		initRequiredPolyfills();

		function initConfigDefaults() {
			if (config.escapeMode === undefined) {
				config.escapeMode = true;
			}

			config.attributePrefix = config.attributePrefix || "_";
			config.arrayAccessForm = config.arrayAccessForm || "none";
			config.emptyNodeForm = config.emptyNodeForm || "text";

			if (config.enableToStringFunc === undefined) {
				config.enableToStringFunc = true;
			}
			config.arrayAccessFormPaths = config.arrayAccessFormPaths || [];
			if (config.skipEmptyTextNodesForObj === undefined) {
				config.skipEmptyTextNodesForObj = true;
			}
			if (config.stripWhitespaces === undefined) {
				config.stripWhitespaces = true;
			}
			config.datetimeAccessFormPaths = config.datetimeAccessFormPaths || [];

			if (config.useDoubleQuotes === undefined) {
				config.useDoubleQuotes = false;
			}

			config.xmlElementsFilter = config.xmlElementsFilter || [];
			config.jsonPropertiesFilter = config.jsonPropertiesFilter || [];

			if (config.keepCData === undefined) {
				config.keepCData = false;
			}
		}

		var DOMNodeTypes = {
			ELEMENT_NODE: 1,
			TEXT_NODE: 3,
			CDATA_SECTION_NODE: 4,
			COMMENT_NODE: 8,
			DOCUMENT_NODE: 9
		};

		function initRequiredPolyfills() {
		}

		function getNodeLocalName(node) {
			var nodeLocalName = node.localName;
			if (nodeLocalName == null) // Yeah, this is IE!!
				nodeLocalName = node.baseName;
			if (nodeLocalName == null || nodeLocalName == "") // =="" is IE too
				nodeLocalName = node.nodeName;
			return nodeLocalName;
		}

		function getNodePrefix(node) {
			return node.prefix;
		}

		function escapeXmlChars(str) {
			if (typeof (str) == "string")
				return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos;');
			else
				return str;
		}

		function unescapeXmlChars(str) {
			return str.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"').replace(/&apos;/g, "'").replace(/&amp;/g, '&');
		}

		function checkInStdFiltersArrayForm(stdFiltersArrayForm, obj, name, path) {
			var idx = 0;
			for (; idx < stdFiltersArrayForm.length; idx++) {
				var filterPath = stdFiltersArrayForm[idx];
				if (typeof filterPath === "string") {
					if (filterPath == path)
						break;
				}
				else
					if (filterPath instanceof RegExp) {
						if (filterPath.test(path))
							break;
					}
					else
						if (typeof filterPath === "function") {
							if (filterPath(obj, name, path))
								break;
						}
			}
			return idx != stdFiltersArrayForm.length;
		}

		function toArrayAccessForm(obj, childName, path) {
			switch (config.arrayAccessForm) {
				case "property":
					if (!(obj[childName] instanceof Array))
						obj[childName + "_asArray"] = [obj[childName]];
					else
						obj[childName + "_asArray"] = obj[childName];
					break;
				/*case "none":
					break;*/
			}

			if (!(obj[childName] instanceof Array) && config.arrayAccessFormPaths.length > 0) {
				if (checkInStdFiltersArrayForm(config.arrayAccessFormPaths, obj, childName, path)) {
					obj[childName] = [obj[childName]];
				}
			}
		}

		function fromXmlDateTime(prop) {
			var bits = prop.split(/[-T:+Z]/g);

			var d = new Date(bits[0], bits[1] - 1, bits[2]);
			var secondBits = bits[5].split("\.");
			d.setHours(bits[3], bits[4], secondBits[0]);
			if (secondBits.length > 1)
				d.setMilliseconds(secondBits[1]);

			if (bits[6] && bits[7]) {
				var offsetMinutes = bits[6] * 60 + Number(bits[7]);
				var sign = /\d\d-\d\d:\d\d$/.test(prop) ? '-' : '+';
				offsetMinutes = 0 + (sign == '-' ? -1 * offsetMinutes : offsetMinutes);
				d.setMinutes(d.getMinutes() - offsetMinutes - d.getTimezoneOffset())
			}
			else
				if (prop.indexOf("Z", prop.length - 1) !== -1) {
					d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds()));
				}
			return d;
		}

		function checkFromXmlDateTimePaths(value, childName, fullPath) {
			if (config.datetimeAccessFormPaths.length > 0) {
				var path = fullPath.split("\.#")[0];
				if (checkInStdFiltersArrayForm(config.datetimeAccessFormPaths, value, childName, path)) {
					return fromXmlDateTime(value);
				}
				else
					return value;
			}
			else
				return value;
		}

		function checkXmlElementsFilter(obj, childType, childName, childPath) {
			if (childType == DOMNodeTypes.ELEMENT_NODE && config.xmlElementsFilter.length > 0) {
				return checkInStdFiltersArrayForm(config.xmlElementsFilter, obj, childName, childPath);
			}
			else
				return true;
		}

		function parseDOMChildren(node, path) {
			if (node.nodeType == DOMNodeTypes.DOCUMENT_NODE) {
				var result = new Object;
				var nodeChildren = node.childNodes;
				// Alternative for firstElementChild which is not supported in some environments
				for (var cidx = 0; cidx < nodeChildren.length; cidx++) {
					var child = nodeChildren.item(cidx);
					if (child.nodeType == DOMNodeTypes.ELEMENT_NODE) {
						var childName = getNodeLocalName(child);
						result[childName] = parseDOMChildren(child, childName);
					}
				}
				return result;
			}
			else
				if (node.nodeType == DOMNodeTypes.ELEMENT_NODE) {
					var result = new Object;
					result.__cnt = 0;

					var nodeChildren = node.childNodes;
					for (var cidx = 0; cidx < nodeChildren.length; cidx++) {
						var child = nodeChildren.item(cidx); // nodeChildren[cidx];
						var childName = getNodeLocalName(child);

						if (child.nodeType != DOMNodeTypes.COMMENT_NODE) {
							var childPath = path + "." + childName;
							if (checkXmlElementsFilter(result, child.nodeType, childName, childPath)) {
								result.__cnt++;
								if (result[childName] == null) {
									result[childName] = parseDOMChildren(child, childPath);
									toArrayAccessForm(result, childName, childPath);
								}
								else {
									if (result[childName] != null) {
										if (!(result[childName] instanceof Array)) {
											result[childName] = [result[childName]];
											toArrayAccessForm(result, childName, childPath);
										}
									}
									(result[childName])[result[childName].length] = parseDOMChildren(child, childPath);
								}
							}
						}
					}

					// Attributes
					for (var aidx = 0; aidx < node.attributes.length; aidx++) {
						var attr = node.attributes.item(aidx); // [aidx];
						result.__cnt++;
						result[config.attributePrefix + attr.name] = attr.value;
					}

					// Node namespace prefix
					var nodePrefix = getNodePrefix(node);
					if (nodePrefix != null && nodePrefix != "") {
						result.__cnt++;
						result.__prefix = nodePrefix;
					}

					if (result["#text"] != null) {
						result.__text = result["#text"];
						if (result.__text instanceof Array) {
							result.__text = result.__text.join("\n");
						}
						//if(config.escapeMode)
						//	result.__text = unescapeXmlChars(result.__text);
						if (config.stripWhitespaces)
							result.__text = result.__text.trim();
						delete result["#text"];
						if (config.arrayAccessForm == "property")
							delete result["#text_asArray"];
						result.__text = checkFromXmlDateTimePaths(result.__text, childName, path + "." + childName);
					}
					if (result["#cdata-section"] != null) {
						result.__cdata = result["#cdata-section"];
						delete result["#cdata-section"];
						if (config.arrayAccessForm == "property")
							delete result["#cdata-section_asArray"];
					}

					if (result.__cnt == 0 && config.emptyNodeForm == "text") {
						result = '';
					}
					else
						if (result.__cnt == 1 && result.__text != null) {
							result = result.__text;
						}
						else
							if (result.__cnt == 1 && result.__cdata != null && !config.keepCData) {
								result = result.__cdata;
							}
							else
								if (result.__cnt > 1 && result.__text != null && config.skipEmptyTextNodesForObj) {
									if ((config.stripWhitespaces && result.__text == "") || (result.__text.trim() == "")) {
										delete result.__text;
									}
								}
					delete result.__cnt;

					if (config.enableToStringFunc && (result.__text != null || result.__cdata != null)) {
						result.toString = function () {
							return (this.__text != null ? this.__text : '') + (this.__cdata != null ? this.__cdata : '');
						};
					}

					return result;
				}
				else
					if (node.nodeType == DOMNodeTypes.TEXT_NODE || node.nodeType == DOMNodeTypes.CDATA_SECTION_NODE) {
						return node.nodeValue;
					}
		}

		function startTag(jsonObj, element, attrList, closed) {
			var resultStr = "<" + ((jsonObj != null && jsonObj.__prefix != null) ? (jsonObj.__prefix + ":") : "") + element;
			if (attrList != null) {
				for (var aidx = 0; aidx < attrList.length; aidx++) {
					var attrName = attrList[aidx];
					var attrVal = jsonObj[attrName];
					if (config.escapeMode)
						attrVal = escapeXmlChars(attrVal);
					resultStr += " " + attrName.substr(config.attributePrefix.length) + "=";
					if (config.useDoubleQuotes)
						resultStr += '"' + attrVal + '"';
					else
						resultStr += "'" + attrVal + "'";
				}
			}
			if (!closed)
				resultStr += ">";
			else
				resultStr += "/>";
			return resultStr;
		}

		function endTag(jsonObj, elementName) {
			return "</" + (jsonObj.__prefix != null ? (jsonObj.__prefix + ":") : "") + elementName + ">";
		}

		function endsWith(str, suffix) {
			return str.indexOf(suffix, str.length - suffix.length) !== -1;
		}

		function jsonXmlSpecialElem(jsonObj, jsonObjField) {
			if ((config.arrayAccessForm == "property" && endsWith(jsonObjField.toString(), ("_asArray")))
				|| jsonObjField.toString().indexOf(config.attributePrefix) == 0
				|| jsonObjField.toString().indexOf("__") == 0
				|| (jsonObj[jsonObjField] instanceof Function))
				return true;
			else
				return false;
		}

		function jsonXmlElemCount(jsonObj) {
			var elementsCnt = 0;
			if (jsonObj instanceof Object) {
				for (var it in jsonObj) {
					if (jsonXmlSpecialElem(jsonObj, it))
						continue;
					elementsCnt++;
				}
			}
			return elementsCnt;
		}

		function checkJsonObjPropertiesFilter(jsonObj, propertyName, jsonObjPath) {
			return config.jsonPropertiesFilter.length == 0
				|| jsonObjPath == ""
				|| checkInStdFiltersArrayForm(config.jsonPropertiesFilter, jsonObj, propertyName, jsonObjPath);
		}

		function parseJSONAttributes(jsonObj) {
			var attrList = [];
			if (jsonObj instanceof Object) {
				for (var ait in jsonObj) {
					if (ait.toString().indexOf("__") == -1 && ait.toString().indexOf(config.attributePrefix) == 0) {
						attrList.push(ait);
					}
				}
			}
			return attrList;
		}

		function parseJSONTextAttrs(jsonTxtObj) {
			var result = "";

			if (jsonTxtObj.__cdata != null) {
				result += "<![CDATA[" + jsonTxtObj.__cdata + "]]>";
			}

			if (jsonTxtObj.__text != null) {
				if (config.escapeMode)
					result += escapeXmlChars(jsonTxtObj.__text);
				else
					result += jsonTxtObj.__text;
			}
			return result;
		}

		function parseJSONTextObject(jsonTxtObj) {
			var result = "";

			if (jsonTxtObj instanceof Object) {
				result += parseJSONTextAttrs(jsonTxtObj);
			}
			else
				if (jsonTxtObj != null) {
					if (config.escapeMode)
						result += escapeXmlChars(jsonTxtObj);
					else
						result += jsonTxtObj;
				}

			return result;
		}

		function getJsonPropertyPath(jsonObjPath, jsonPropName) {
			if (jsonObjPath === "") {
				return jsonPropName;
			}
			else
				return jsonObjPath + "." + jsonPropName;
		}

		function parseJSONArray(jsonArrRoot, jsonArrObj, attrList, jsonObjPath) {
			var result = "";
			if (jsonArrRoot.length == 0) {
				result += startTag(jsonArrRoot, jsonArrObj, attrList, true);
			}
			else {
				for (var arIdx = 0; arIdx < jsonArrRoot.length; arIdx++) {
					result += startTag(jsonArrRoot[arIdx], jsonArrObj, parseJSONAttributes(jsonArrRoot[arIdx]), false);
					result += parseJSONObject(jsonArrRoot[arIdx], getJsonPropertyPath(jsonObjPath, jsonArrObj));
					result += endTag(jsonArrRoot[arIdx], jsonArrObj);
				}
			}
			return result;
		}

		function parseJSONObject(jsonObj, jsonObjPath) {
			var result = "";

			var elementsCnt = jsonXmlElemCount(jsonObj);

			if (elementsCnt > 0) {
				for (var it in jsonObj) {

					if (jsonXmlSpecialElem(jsonObj, it) || (jsonObjPath != "" && !checkJsonObjPropertiesFilter(jsonObj, it, getJsonPropertyPath(jsonObjPath, it))))
						continue;

					var subObj = jsonObj[it];

					var attrList = parseJSONAttributes(subObj)

					if (subObj == null || subObj == undefined) {
						result += startTag(subObj, it, attrList, true);
					}
					else
						if (subObj instanceof Object) {

							if (subObj instanceof Array) {
								result += parseJSONArray(subObj, it, attrList, jsonObjPath);
							}
							else if (subObj instanceof Date) {
								result += startTag(subObj, it, attrList, false);
								result += subObj.toISOString();
								result += endTag(subObj, it);
							}
							else {
								var subObjElementsCnt = jsonXmlElemCount(subObj);
								if (subObjElementsCnt > 0 || subObj.__text != null || subObj.__cdata != null) {
									result += startTag(subObj, it, attrList, false);
									result += parseJSONObject(subObj, getJsonPropertyPath(jsonObjPath, it));
									result += endTag(subObj, it);
								}
								else {
									result += startTag(subObj, it, attrList, true);
								}
							}
						}
						else {
							result += startTag(subObj, it, attrList, false);
							result += parseJSONTextObject(subObj);
							result += endTag(subObj, it);
						}
				}
			}
			result += parseJSONTextObject(jsonObj);

			return result;
		}

		this.parseXmlString = function (xmlDocStr) {
			var isIEParser = window.ActiveXObject || "ActiveXObject" in window;
			if (xmlDocStr === undefined) {
				return null;
			}
			var xmlDoc;
			if (window.DOMParser) {
				var parser = new window.DOMParser();
				var parsererrorNS = null;
				// IE9+ now is here
				if (!isIEParser) {
					try {
						parsererrorNS = parser.parseFromString("INVALID", "text/xml").getElementsByTagName("parsererror")[0].namespaceURI;
					}
					catch (err) {
						parsererrorNS = null;
					}
				}
				try {
					xmlDoc = parser.parseFromString(xmlDocStr, "text/xml");
					if (parsererrorNS != null && xmlDoc.getElementsByTagNameNS(parsererrorNS, "parsererror").length > 0) {
						//throw new Error('Error parsing XML: '+xmlDocStr);
						xmlDoc = null;
					}
				}
				catch (err) {
					xmlDoc = null;
				}
			}
			else {
				// IE :(
				if (xmlDocStr.indexOf("<?") == 0) {
					xmlDocStr = xmlDocStr.substr(xmlDocStr.indexOf("?>") + 2);
				}
				xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async = "false";
				xmlDoc.loadXML(xmlDocStr);
			}
			return xmlDoc;
		};

		this.asArray = function (prop) {
			if (prop === undefined || prop == null)
				return [];
			else
				if (prop instanceof Array)
					return prop;
				else
					return [prop];
		};

		this.toXmlDateTime = function (dt) {
			if (dt instanceof Date)
				return dt.toISOString();
			else
				if (typeof (dt) === 'number')
					return new Date(dt).toISOString();
				else
					return null;
		};

		this.asDateTime = function (prop) {
			if (typeof (prop) == "string") {
				return fromXmlDateTime(prop);
			}
			else
				return prop;
		};

		this.xml2json = function (xmlDoc) {
			return parseDOMChildren(xmlDoc);
		};

		this.xml_str2json = function (xmlDocStr) {
			var xmlDoc = this.parseXmlString(xmlDocStr);
			if (xmlDoc != null)
				return this.xml2json(xmlDoc);
			else
				return null;
		};

		this.json2xml_str = function (jsonObj) {
			return parseJSONObject(jsonObj, "");
		};

		this.json2xml = function (jsonObj) {
			var xmlDocStr = this.json2xml_str(jsonObj);
			return this.parseXmlString(xmlDocStr);
		};

		this.getVersion = function () {
			return VERSION;
		};
	}
}));


/*!
 * Datepicker for Bootstrap v1.9.0 (https://github.com/uxsolutions/bootstrap-datepicker)
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */

(function(factory){
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function($, undefined){
	function UTCDate(){
		return new Date(Date.UTC.apply(Date, arguments));
	}
	function UTCToday(){
		var today = new Date();
		return UTCDate(today.getFullYear(), today.getMonth(), today.getDate());
	}
	function isUTCEquals(date1, date2) {
		return (
			date1.getUTCFullYear() === date2.getUTCFullYear() &&
			date1.getUTCMonth() === date2.getUTCMonth() &&
			date1.getUTCDate() === date2.getUTCDate()
		);
	}
	function alias(method, deprecationMsg){
		return function(){
			if (deprecationMsg !== undefined) {
				$.fn.datepicker.deprecated(deprecationMsg);
			}

			return this[method].apply(this, arguments);
		};
	}
	function isValidDate(d) {
		return d && !isNaN(d.getTime());
	}

	var DateArray = (function(){
		var extras = {
			get: function(i){
				return this.slice(i)[0];
			},
			contains: function(d){
				// Array.indexOf is not cross-browser;
				// $.inArray doesn't work with Dates
				var val = d && d.valueOf();
				for (var i=0, l=this.length; i < l; i++)
		  // Use date arithmetic to allow dates with different times to match
		  if (0 <= this[i].valueOf() - val && this[i].valueOf() - val < 1000*60*60*24)
						return i;
				return -1;
			},
			remove: function(i){
				this.splice(i,1);
			},
			replace: function(new_array){
				if (!new_array)
					return;
				if (!$.isArray(new_array))
					new_array = [new_array];
				this.clear();
				this.push.apply(this, new_array);
			},
			clear: function(){
				this.length = 0;
			},
			copy: function(){
				var a = new DateArray();
				a.replace(this);
				return a;
			}
		};

		return function(){
			var a = [];
			a.push.apply(a, arguments);
			$.extend(a, extras);
			return a;
		};
	})();


	// Picker object

	var Datepicker = function(element, options){
		$.data(element, 'datepicker', this);

		this._events = [];
		this._secondaryEvents = [];

		this._process_options(options);

		this.dates = new DateArray();
		this.viewDate = this.o.defaultViewDate;
		this.focusDate = null;

		this.element = $(element);
		this.isInput = this.element.is('input');
		this.inputField = this.isInput ? this.element : this.element.find('input');
		this.component = this.element.hasClass('date') ? this.element.find('.add-on, .input-group-addon, .input-group-append, .input-group-prepend, .btn') : false;
		if (this.component && this.component.length === 0)
			this.component = false;
		this.isInline = !this.component && this.element.is('div');

		this.picker = $(DPGlobal.template);

		// Checking templates and inserting
		if (this._check_template(this.o.templates.leftArrow)) {
			this.picker.find('.prev').html(this.o.templates.leftArrow);
		}

		if (this._check_template(this.o.templates.rightArrow)) {
			this.picker.find('.next').html(this.o.templates.rightArrow);
		}

		this._buildEvents();
		this._attachEvents();

		if (this.isInline){
			this.picker.addClass('datepicker-inline').appendTo(this.element);
		}
		else {
			this.picker.addClass('datepicker-dropdown dropdown-menu');
		}

		if (this.o.rtl){
			this.picker.addClass('datepicker-rtl');
		}

		if (this.o.calendarWeeks) {
			this.picker.find('.datepicker-days .datepicker-switch, thead .datepicker-title, tfoot .today, tfoot .clear')
				.attr('colspan', function(i, val){
					return Number(val) + 1;
				});
		}

		this._process_options({
			startDate: this._o.startDate,
			endDate: this._o.endDate,
			daysOfWeekDisabled: this.o.daysOfWeekDisabled,
			daysOfWeekHighlighted: this.o.daysOfWeekHighlighted,
			datesDisabled: this.o.datesDisabled
		});

		this._allow_update = false;
		this.setViewMode(this.o.startView);
		this._allow_update = true;

		this.fillDow();
		this.fillMonths();

		this.update();

		if (this.isInline){
			this.show();
		}
	};

	Datepicker.prototype = {
		constructor: Datepicker,

		_resolveViewName: function(view){
			$.each(DPGlobal.viewModes, function(i, viewMode){
				if (view === i || $.inArray(view, viewMode.names) !== -1){
					view = i;
					return false;
				}
			});

			return view;
		},

		_resolveDaysOfWeek: function(daysOfWeek){
			if (!$.isArray(daysOfWeek))
				daysOfWeek = daysOfWeek.split(/[,\s]*/);
			return $.map(daysOfWeek, Number);
		},

		_check_template: function(tmp){
			try {
				// If empty
				if (tmp === undefined || tmp === "") {
					return false;
				}
				// If no html, everything ok
				if ((tmp.match(/[<>]/g) || []).length <= 0) {
					return true;
				}
				// Checking if html is fine
				var jDom = $(tmp);
				return jDom.length > 0;
			}
			catch (ex) {
				return false;
			}
		},

		_process_options: function(opts){
			// Store raw options for reference
			this._o = $.extend({}, this._o, opts);
			// Processed options
			var o = this.o = $.extend({}, this._o);

			// Check if "de-DE" style date is available, if not language should
			// fallback to 2 letter code eg "de"
			var lang = o.language;
			if (!dates[lang]){
				lang = lang.split('-')[0];
				if (!dates[lang])
					lang = defaults.language;
			}
			o.language = lang;

			// Retrieve view index from any aliases
			o.startView = this._resolveViewName(o.startView);
			o.minViewMode = this._resolveViewName(o.minViewMode);
			o.maxViewMode = this._resolveViewName(o.maxViewMode);

			// Check view is between min and max
			o.startView = Math.max(this.o.minViewMode, Math.min(this.o.maxViewMode, o.startView));

			// true, false, or Number > 0
			if (o.multidate !== true){
				o.multidate = Number(o.multidate) || false;
				if (o.multidate !== false)
					o.multidate = Math.max(0, o.multidate);
			}
			o.multidateSeparator = String(o.multidateSeparator);

			o.weekStart %= 7;
			o.weekEnd = (o.weekStart + 6) % 7;

			var format = DPGlobal.parseFormat(o.format);
			if (o.startDate !== -Infinity){
				if (!!o.startDate){
					if (o.startDate instanceof Date)
						o.startDate = this._local_to_utc(this._zero_time(o.startDate));
					else
						o.startDate = DPGlobal.parseDate(o.startDate, format, o.language, o.assumeNearbyYear);
				}
				else {
					o.startDate = -Infinity;
				}
			}
			if (o.endDate !== Infinity){
				if (!!o.endDate){
					if (o.endDate instanceof Date)
						o.endDate = this._local_to_utc(this._zero_time(o.endDate));
					else
						o.endDate = DPGlobal.parseDate(o.endDate, format, o.language, o.assumeNearbyYear);
				}
				else {
					o.endDate = Infinity;
				}
			}

			o.daysOfWeekDisabled = this._resolveDaysOfWeek(o.daysOfWeekDisabled||[]);
			o.daysOfWeekHighlighted = this._resolveDaysOfWeek(o.daysOfWeekHighlighted||[]);

			o.datesDisabled = o.datesDisabled||[];
			if (!$.isArray(o.datesDisabled)) {
				o.datesDisabled = o.datesDisabled.split(',');
			}
			o.datesDisabled = $.map(o.datesDisabled, function(d){
				return DPGlobal.parseDate(d, format, o.language, o.assumeNearbyYear);
			});

			var plc = String(o.orientation).toLowerCase().split(/\s+/g),
				_plc = o.orientation.toLowerCase();
			plc = $.grep(plc, function(word){
				return /^auto|left|right|top|bottom$/.test(word);
			});
			o.orientation = {x: 'auto', y: 'auto'};
			if (!_plc || _plc === 'auto')
				; // no action
			else if (plc.length === 1){
				switch (plc[0]){
					case 'top':
					case 'bottom':
						o.orientation.y = plc[0];
						break;
					case 'left':
					case 'right':
						o.orientation.x = plc[0];
						break;
				}
			}
			else {
				_plc = $.grep(plc, function(word){
					return /^left|right$/.test(word);
				});
				o.orientation.x = _plc[0] || 'auto';

				_plc = $.grep(plc, function(word){
					return /^top|bottom$/.test(word);
				});
				o.orientation.y = _plc[0] || 'auto';
			}
			if (o.defaultViewDate instanceof Date || typeof o.defaultViewDate === 'string') {
				o.defaultViewDate = DPGlobal.parseDate(o.defaultViewDate, format, o.language, o.assumeNearbyYear);
			} else if (o.defaultViewDate) {
				var year = o.defaultViewDate.year || new Date().getFullYear();
				var month = o.defaultViewDate.month || 0;
				var day = o.defaultViewDate.day || 1;
				o.defaultViewDate = UTCDate(year, month, day);
			} else {
				o.defaultViewDate = UTCToday();
			}
		},
		_applyEvents: function(evs){
			for (var i=0, el, ch, ev; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				} else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.on(ev, ch);
			}
		},
		_unapplyEvents: function(evs){
			for (var i=0, el, ev, ch; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				} else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.off(ev, ch);
			}
		},
		_buildEvents: function(){
			var events = {
				keyup: $.proxy(function(e){
					if ($.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) === -1)
						this.update();
				}, this),
				keydown: $.proxy(this.keydown, this),
				paste: $.proxy(this.paste, this)
			};

			if (this.o.showOnFocus === true) {
				events.focus = $.proxy(this.show, this);
			}

			if (this.isInput) { // single input
				this._events = [
					[this.element, events]
				];
			}
			// component: input + button
			else if (this.component && this.inputField.length) {
				this._events = [
					// For components that are not readonly, allow keyboard nav
					[this.inputField, events],
					[this.component, {
						click: $.proxy(this.show, this)
					}]
				];
			}
			else {
				this._events = [
					[this.element, {
						click: $.proxy(this.show, this),
						keydown: $.proxy(this.keydown, this)
					}]
				];
			}
			this._events.push(
				// Component: listen for blur on element descendants
				[this.element, '*', {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}],
				// Input: listen for blur on element
				[this.element, {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}]
			);

			if (this.o.immediateUpdates) {
				// Trigger input updates immediately on changed year/month
				this._events.push([this.element, {
					'changeYear changeMonth': $.proxy(function(e){
						this.update(e.date);
					}, this)
				}]);
			}

			this._secondaryEvents = [
				[this.picker, {
					click: $.proxy(this.click, this)
				}],
				[this.picker, '.prev, .next', {
					click: $.proxy(this.navArrowsClick, this)
				}],
				[this.picker, '.day:not(.disabled)', {
					click: $.proxy(this.dayCellClick, this)
				}],
				[$(window), {
					resize: $.proxy(this.place, this)
				}],
				[$(document), {
					'mousedown touchstart': $.proxy(function(e){
						// Clicked outside the datepicker, hide it
						if (!(
							this.element.is(e.target) ||
							this.element.find(e.target).length ||
							this.picker.is(e.target) ||
							this.picker.find(e.target).length ||
							this.isInline
						)){
							this.hide();
						}
					}, this)
				}]
			];
		},
		_attachEvents: function(){
			this._detachEvents();
			this._applyEvents(this._events);
		},
		_detachEvents: function(){
			this._unapplyEvents(this._events);
		},
		_attachSecondaryEvents: function(){
			this._detachSecondaryEvents();
			this._applyEvents(this._secondaryEvents);
		},
		_detachSecondaryEvents: function(){
			this._unapplyEvents(this._secondaryEvents);
		},
		_trigger: function(event, altdate){
			var date = altdate || this.dates.get(-1),
				local_date = this._utc_to_local(date);

			this.element.trigger({
				type: event,
				date: local_date,
				viewMode: this.viewMode,
				dates: $.map(this.dates, this._utc_to_local),
				format: $.proxy(function(ix, format){
					if (arguments.length === 0){
						ix = this.dates.length - 1;
						format = this.o.format;
					} else if (typeof ix === 'string'){
						format = ix;
						ix = this.dates.length - 1;
					}
					format = format || this.o.format;
					var date = this.dates.get(ix);
					return DPGlobal.formatDate(date, format, this.o.language);
				}, this)
			});
		},

		show: function(){
			if (this.inputField.is(':disabled') || (this.inputField.prop('readonly') && this.o.enableOnReadonly === false))
				return;
			if (!this.isInline)
				this.picker.appendTo(this.o.container);
			this.place();
			this.picker.show();
			this._attachSecondaryEvents();
			this._trigger('show');
			if ((window.navigator.msMaxTouchPoints || 'ontouchstart' in document) && this.o.disableTouchKeyboard) {
				$(this.element).blur();
			}
			return this;
		},

		hide: function(){
			if (this.isInline || !this.picker.is(':visible'))
				return this;
			this.focusDate = null;
			this.picker.hide().detach();
			this._detachSecondaryEvents();
			this.setViewMode(this.o.startView);

			if (this.o.forceParse && this.inputField.val())
				this.setValue();
			this._trigger('hide');
			return this;
		},

		destroy: function(){
			this.hide();
			this._detachEvents();
			this._detachSecondaryEvents();
			this.picker.remove();
			delete this.element.data().datepicker;
			if (!this.isInput){
				delete this.element.data().date;
			}
			return this;
		},

		paste: function(e){
			var dateString;
			if (e.originalEvent.clipboardData && e.originalEvent.clipboardData.types
				&& $.inArray('text/plain', e.originalEvent.clipboardData.types) !== -1) {
				dateString = e.originalEvent.clipboardData.getData('text/plain');
			} else if (window.clipboardData) {
				dateString = window.clipboardData.getData('Text');
			} else {
				return;
			}
			this.setDate(dateString);
			this.update();
			e.preventDefault();
		},

		_utc_to_local: function(utc){
			if (!utc) {
				return utc;
			}

			var local = new Date(utc.getTime() + (utc.getTimezoneOffset() * 60000));

			if (local.getTimezoneOffset() !== utc.getTimezoneOffset()) {
				local = new Date(utc.getTime() + (local.getTimezoneOffset() * 60000));
			}

			return local;
		},
		_local_to_utc: function(local){
			return local && new Date(local.getTime() - (local.getTimezoneOffset()*60000));
		},
		_zero_time: function(local){
			return local && new Date(local.getFullYear(), local.getMonth(), local.getDate());
		},
		_zero_utc_time: function(utc){
			return utc && UTCDate(utc.getUTCFullYear(), utc.getUTCMonth(), utc.getUTCDate());
		},

		getDates: function(){
			return $.map(this.dates, this._utc_to_local);
		},

		getUTCDates: function(){
			return $.map(this.dates, function(d){
				return new Date(d);
			});
		},

		getDate: function(){
			return this._utc_to_local(this.getUTCDate());
		},

		getUTCDate: function(){
			var selected_date = this.dates.get(-1);
			if (selected_date !== undefined) {
				return new Date(selected_date);
			} else {
				return null;
			}
		},

		clearDates: function(){
			this.inputField.val('');
			this.update();
			this._trigger('changeDate');

			if (this.o.autoclose) {
				this.hide();
			}
		},

		setDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.update.apply(this, args);
			this._trigger('changeDate');
			this.setValue();
			return this;
		},

		setUTCDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.setDates.apply(this, $.map(args, this._utc_to_local));
			return this;
		},

		setDate: alias('setDates'),
		setUTCDate: alias('setUTCDates'),
		remove: alias('destroy', 'Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead'),

		setValue: function(){
			var formatted = this.getFormattedDate();
			this.inputField.val(formatted);
			return this;
		},

		getFormattedDate: function(format){
			if (format === undefined)
				format = this.o.format;

			var lang = this.o.language;
			return $.map(this.dates, function(d){
				return DPGlobal.formatDate(d, format, lang);
			}).join(this.o.multidateSeparator);
		},

		getStartDate: function(){
			return this.o.startDate;
		},

		setStartDate: function(startDate){
			this._process_options({startDate: startDate});
			this.update();
			this.updateNavArrows();
			return this;
		},

		getEndDate: function(){
			return this.o.endDate;
		},

		setEndDate: function(endDate){
			this._process_options({endDate: endDate});
			this.update();
			this.updateNavArrows();
			return this;
		},

		setDaysOfWeekDisabled: function(daysOfWeekDisabled){
			this._process_options({daysOfWeekDisabled: daysOfWeekDisabled});
			this.update();
			return this;
		},

		setDaysOfWeekHighlighted: function(daysOfWeekHighlighted){
			this._process_options({daysOfWeekHighlighted: daysOfWeekHighlighted});
			this.update();
			return this;
		},

		setDatesDisabled: function(datesDisabled){
			this._process_options({datesDisabled: datesDisabled});
			this.update();
			return this;
		},

		place: function(){
			if (this.isInline)
				return this;
			var calendarWidth = this.picker.outerWidth(),
				calendarHeight = this.picker.outerHeight(),
				visualPadding = 10,
				container = $(this.o.container),
				windowWidth = container.width(),
				scrollTop = this.o.container === 'body' ? $(document).scrollTop() : container.scrollTop(),
				appendOffset = container.offset();

			var parentsZindex = [0];
			this.element.parents().each(function(){
				var itemZIndex = $(this).css('z-index');
				if (itemZIndex !== 'auto' && Number(itemZIndex) !== 0) parentsZindex.push(Number(itemZIndex));
			});
			var zIndex = Math.max.apply(Math, parentsZindex) + this.o.zIndexOffset;
			var offset = this.component ? this.component.parent().offset() : this.element.offset();
			var height = this.component ? this.component.outerHeight(true) : this.element.outerHeight(false);
			var width = this.component ? this.component.outerWidth(true) : this.element.outerWidth(false);
			var left = offset.left - appendOffset.left;
			var top = offset.top - appendOffset.top;

			if (this.o.container !== 'body') {
				top += scrollTop;
			}

			this.picker.removeClass(
				'datepicker-orient-top datepicker-orient-bottom '+
				'datepicker-orient-right datepicker-orient-left'
			);

			if (this.o.orientation.x !== 'auto'){
				this.picker.addClass('datepicker-orient-' + this.o.orientation.x);
				if (this.o.orientation.x === 'right')
					left -= calendarWidth - width;
			}
			// auto x orientation is best-placement: if it crosses a window
			// edge, fudge it sideways
			else {
				if (offset.left < 0) {
					// component is outside the window on the left side. Move it into visible range
					this.picker.addClass('datepicker-orient-left');
					left -= offset.left - visualPadding;
				} else if (left + calendarWidth > windowWidth) {
					// the calendar passes the widow right edge. Align it to component right side
					this.picker.addClass('datepicker-orient-right');
					left += width - calendarWidth;
				} else {
					if (this.o.rtl) {
						// Default to right
						this.picker.addClass('datepicker-orient-right');
					} else {
						// Default to left
						this.picker.addClass('datepicker-orient-left');
					}
				}
			}

			// auto y orientation is best-situation: top or bottom, no fudging,
			// decision based on which shows more of the calendar
			var yorient = this.o.orientation.y,
				top_overflow;
			if (yorient === 'auto'){
				top_overflow = -scrollTop + top - calendarHeight;
				yorient = top_overflow < 0 ? 'bottom' : 'top';
			}

			this.picker.addClass('datepicker-orient-' + yorient);
			if (yorient === 'top')
				top -= calendarHeight + parseInt(this.picker.css('padding-top'));
			else
				top += height;

			if (this.o.rtl) {
				var right = windowWidth - (left + width);
				this.picker.css({
					top: top,
					right: right,
					zIndex: zIndex
				});
			} else {
				this.picker.css({
					top: top,
					left: left,
					zIndex: zIndex
				});
			}
			return this;
		},

		_allow_update: true,
		update: function(){
			if (!this._allow_update)
				return this;

			var oldDates = this.dates.copy(),
				dates = [],
				fromArgs = false;
			if (arguments.length){
				$.each(arguments, $.proxy(function(i, date){
					if (date instanceof Date)
						date = this._local_to_utc(date);
					dates.push(date);
				}, this));
				fromArgs = true;
			} else {
				dates = this.isInput
						? this.element.val()
						: this.element.data('date') || this.inputField.val();
				if (dates && this.o.multidate)
					dates = dates.split(this.o.multidateSeparator);
				else
					dates = [dates];
				delete this.element.data().date;
			}

			dates = $.map(dates, $.proxy(function(date){
				return DPGlobal.parseDate(date, this.o.format, this.o.language, this.o.assumeNearbyYear);
			}, this));
			dates = $.grep(dates, $.proxy(function(date){
				return (
					!this.dateWithinRange(date) ||
					!date
				);
			}, this), true);
			this.dates.replace(dates);

			if (this.o.updateViewDate) {
				if (this.dates.length)
					this.viewDate = new Date(this.dates.get(-1));
				else if (this.viewDate < this.o.startDate)
					this.viewDate = new Date(this.o.startDate);
				else if (this.viewDate > this.o.endDate)
					this.viewDate = new Date(this.o.endDate);
				else
					this.viewDate = this.o.defaultViewDate;
			}

			if (fromArgs){
				// setting date by clicking
				this.setValue();
				this.element.change();
			}
			else if (this.dates.length){
				// setting date by typing
				if (String(oldDates) !== String(this.dates) && fromArgs) {
					this._trigger('changeDate');
					this.element.change();
				}
			}
			if (!this.dates.length && oldDates.length) {
				this._trigger('clearDate');
				this.element.change();
			}

			this.fill();
			return this;
		},

		fillDow: function(){
	  if (this.o.showWeekDays) {
			var dowCnt = this.o.weekStart,
				html = '<tr>';
			if (this.o.calendarWeeks){
				html += '<th class="cw">&#160;</th>';
			}
			while (dowCnt < this.o.weekStart + 7){
				html += '<th class="dow';
		if ($.inArray(dowCnt, this.o.daysOfWeekDisabled) !== -1)
		  html += ' disabled';
		html += '">'+dates[this.o.language].daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
	  }
		},

		fillMonths: function(){
	  var localDate = this._utc_to_local(this.viewDate);
			var html = '';
			var focused;
			for (var i = 0; i < 12; i++){
				focused = localDate && localDate.getMonth() === i ? ' focused' : '';
				html += '<span class="month' + focused + '">' + dates[this.o.language].monthsShort[i] + '</span>';
			}
			this.picker.find('.datepicker-months td').html(html);
		},

		setRange: function(range){
			if (!range || !range.length)
				delete this.range;
			else
				this.range = $.map(range, function(d){
					return d.valueOf();
				});
			this.fill();
		},

		getClassNames: function(date){
			var cls = [],
				year = this.viewDate.getUTCFullYear(),
				month = this.viewDate.getUTCMonth(),
				today = UTCToday();
			if (date.getUTCFullYear() < year || (date.getUTCFullYear() === year && date.getUTCMonth() < month)){
				cls.push('old');
			} else if (date.getUTCFullYear() > year || (date.getUTCFullYear() === year && date.getUTCMonth() > month)){
				cls.push('new');
			}
			if (this.focusDate && date.valueOf() === this.focusDate.valueOf())
				cls.push('focused');
			// Compare internal UTC date with UTC today, not local today
			if (this.o.todayHighlight && isUTCEquals(date, today)) {
				cls.push('today');
			}
			if (this.dates.contains(date) !== -1)
				cls.push('active');
			if (!this.dateWithinRange(date)){
				cls.push('disabled');
			}
			if (this.dateIsDisabled(date)){
				cls.push('disabled', 'disabled-date');
			}
			if ($.inArray(date.getUTCDay(), this.o.daysOfWeekHighlighted) !== -1){
				cls.push('highlighted');
			}

			if (this.range){
				if (date > this.range[0] && date < this.range[this.range.length-1]){
					cls.push('range');
				}
				if ($.inArray(date.valueOf(), this.range) !== -1){
					cls.push('selected');
				}
				if (date.valueOf() === this.range[0]){
		  cls.push('range-start');
		}
		if (date.valueOf() === this.range[this.range.length-1]){
		  cls.push('range-end');
		}
			}
			return cls;
		},

		_fill_yearsView: function(selector, cssClass, factor, year, startYear, endYear, beforeFn){
			var html = '';
			var step = factor / 10;
			var view = this.picker.find(selector);
			var startVal = Math.floor(year / factor) * factor;
			var endVal = startVal + step * 9;
			var focusedVal = Math.floor(this.viewDate.getFullYear() / step) * step;
			var selected = $.map(this.dates, function(d){
				return Math.floor(d.getUTCFullYear() / step) * step;
			});

			var classes, tooltip, before;
			for (var currVal = startVal - step; currVal <= endVal + step; currVal += step) {
				classes = [cssClass];
				tooltip = null;

				if (currVal === startVal - step) {
					classes.push('old');
				} else if (currVal === endVal + step) {
					classes.push('new');
				}
				if ($.inArray(currVal, selected) !== -1) {
					classes.push('active');
				}
				if (currVal < startYear || currVal > endYear) {
					classes.push('disabled');
				}
				if (currVal === focusedVal) {
				  classes.push('focused');
		}

				if (beforeFn !== $.noop) {
					before = beforeFn(new Date(currVal, 0, 1));
					if (before === undefined) {
						before = {};
					} else if (typeof before === 'boolean') {
						before = {enabled: before};
					} else if (typeof before === 'string') {
						before = {classes: before};
					}
					if (before.enabled === false) {
						classes.push('disabled');
					}
					if (before.classes) {
						classes = classes.concat(before.classes.split(/\s+/));
					}
					if (before.tooltip) {
						tooltip = before.tooltip;
					}
				}

				html += '<span class="' + classes.join(' ') + '"' + (tooltip ? ' title="' + tooltip + '"' : '') + '>' + currVal + '</span>';
			}

			view.find('.datepicker-switch').text(startVal + '-' + endVal);
			view.find('td').html(html);
		},

		fill: function(){
			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth(),
				startYear = this.o.startDate !== -Infinity ? this.o.startDate.getUTCFullYear() : -Infinity,
				startMonth = this.o.startDate !== -Infinity ? this.o.startDate.getUTCMonth() : -Infinity,
				endYear = this.o.endDate !== Infinity ? this.o.endDate.getUTCFullYear() : Infinity,
				endMonth = this.o.endDate !== Infinity ? this.o.endDate.getUTCMonth() : Infinity,
				todaytxt = dates[this.o.language].today || dates['en'].today || '',
				cleartxt = dates[this.o.language].clear || dates['en'].clear || '',
		titleFormat = dates[this.o.language].titleFormat || dates['en'].titleFormat,
		todayDate = UTCToday(),
		titleBtnVisible = (this.o.todayBtn === true || this.o.todayBtn === 'linked') && todayDate >= this.o.startDate && todayDate <= this.o.endDate && !this.weekOfDateIsDisabled(todayDate),
				tooltip,
				before;
			if (isNaN(year) || isNaN(month))
				return;
			this.picker.find('.datepicker-days .datepicker-switch')
						.text(DPGlobal.formatDate(d, titleFormat, this.o.language));
			this.picker.find('tfoot .today')
						.text(todaytxt)
			.css('display', titleBtnVisible ? 'table-cell' : 'none');
			this.picker.find('tfoot .clear')
						.text(cleartxt)
						.css('display', this.o.clearBtn === true ? 'table-cell' : 'none');
			this.picker.find('thead .datepicker-title')
						.text(this.o.title)
						.css('display', typeof this.o.title === 'string' && this.o.title !== '' ? 'table-cell' : 'none');
			this.updateNavArrows();
			this.fillMonths();
			var prevMonth = UTCDate(year, month, 0),
				day = prevMonth.getUTCDate();
			prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.o.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			if (prevMonth.getUTCFullYear() < 100){
		nextMonth.setUTCFullYear(prevMonth.getUTCFullYear());
	  }
			nextMonth.setUTCDate(nextMonth.getUTCDate() + 42);
			nextMonth = nextMonth.valueOf();
			var html = [];
			var weekDay, clsName;
			while (prevMonth.valueOf() < nextMonth){
				weekDay = prevMonth.getUTCDay();
				if (weekDay === this.o.weekStart){
					html.push('<tr>');
					if (this.o.calendarWeeks){
						// ISO 8601: First week contains first thursday.
						// ISO also states week starts on Monday, but we can be more abstract here.
						var
							// Start of current week: based on weekstart/current date
							ws = new Date(+prevMonth + (this.o.weekStart - weekDay - 7) % 7 * 864e5),
							// Thursday of this week
							th = new Date(Number(ws) + (7 + 4 - ws.getUTCDay()) % 7 * 864e5),
							// First Thursday of year, year from thursday
							yth = new Date(Number(yth = UTCDate(th.getUTCFullYear(), 0, 1)) + (7 + 4 - yth.getUTCDay()) % 7 * 864e5),
							// Calendar week: ms between thursdays, div ms per day, div 7 days
							calWeek = (th - yth) / 864e5 / 7 + 1;
						html.push('<td class="cw">'+ calWeek +'</td>');
					}
				}
				clsName = this.getClassNames(prevMonth);
				clsName.push('day');

				var content = prevMonth.getUTCDate();

				if (this.o.beforeShowDay !== $.noop){
					before = this.o.beforeShowDay(this._utc_to_local(prevMonth));
					if (before === undefined)
						before = {};
					else if (typeof before === 'boolean')
						before = {enabled: before};
					else if (typeof before === 'string')
						before = {classes: before};
					if (before.enabled === false)
						clsName.push('disabled');
					if (before.classes)
						clsName = clsName.concat(before.classes.split(/\s+/));
					if (before.tooltip)
						tooltip = before.tooltip;
					if (before.content)
						content = before.content;
				}

				//Check if uniqueSort exists (supported by jquery >=1.12 and >=2.2)
				//Fallback to unique function for older jquery versions
				if ($.isFunction($.uniqueSort)) {
					clsName = $.uniqueSort(clsName);
				} else {
					clsName = $.unique(clsName);
				}

				html.push('<td class="'+clsName.join(' ')+'"' + (tooltip ? ' title="'+tooltip+'"' : '') + ' data-date="' + prevMonth.getTime().toString() + '">' + content + '</td>');
				tooltip = null;
				if (weekDay === this.o.weekEnd){
					html.push('</tr>');
				}
				prevMonth.setUTCDate(prevMonth.getUTCDate() + 1);
			}
			this.picker.find('.datepicker-days tbody').html(html.join(''));

			var monthsTitle = dates[this.o.language].monthsTitle || dates['en'].monthsTitle || 'Months';
			var months = this.picker.find('.datepicker-months')
						.find('.datepicker-switch')
							.text(this.o.maxViewMode < 2 ? monthsTitle : year)
							.end()
						.find('tbody span').removeClass('active');

			$.each(this.dates, function(i, d){
				if (d.getUTCFullYear() === year)
					months.eq(d.getUTCMonth()).addClass('active');
			});

			if (year < startYear || year > endYear){
				months.addClass('disabled');
			}
			if (year === startYear){
				months.slice(0, startMonth).addClass('disabled');
			}
			if (year === endYear){
				months.slice(endMonth+1).addClass('disabled');
			}

			if (this.o.beforeShowMonth !== $.noop){
				var that = this;
				$.each(months, function(i, month){
		  var moDate = new Date(year, i, 1);
		  var before = that.o.beforeShowMonth(moDate);
					if (before === undefined)
						before = {};
					else if (typeof before === 'boolean')
						before = {enabled: before};
					else if (typeof before === 'string')
						before = {classes: before};
					if (before.enabled === false && !$(month).hasClass('disabled'))
						$(month).addClass('disabled');
					if (before.classes)
						$(month).addClass(before.classes);
					if (before.tooltip)
						$(month).prop('title', before.tooltip);
				});
			}

			// Generating decade/years picker
			this._fill_yearsView(
				'.datepicker-years',
				'year',
				10,
				year,
				startYear,
				endYear,
				this.o.beforeShowYear
			);

			// Generating century/decades picker
			this._fill_yearsView(
				'.datepicker-decades',
				'decade',
				100,
				year,
				startYear,
				endYear,
				this.o.beforeShowDecade
			);

			// Generating millennium/centuries picker
			this._fill_yearsView(
				'.datepicker-centuries',
				'century',
				1000,
				year,
				startYear,
				endYear,
				this.o.beforeShowCentury
			);
		},

		updateNavArrows: function(){
			if (!this._allow_update)
				return;

			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth(),
				startYear = this.o.startDate !== -Infinity ? this.o.startDate.getUTCFullYear() : -Infinity,
				startMonth = this.o.startDate !== -Infinity ? this.o.startDate.getUTCMonth() : -Infinity,
				endYear = this.o.endDate !== Infinity ? this.o.endDate.getUTCFullYear() : Infinity,
				endMonth = this.o.endDate !== Infinity ? this.o.endDate.getUTCMonth() : Infinity,
				prevIsDisabled,
				nextIsDisabled,
				factor = 1;
			switch (this.viewMode){
				case 4:
					factor *= 10;
					/* falls through */
				case 3:
					factor *= 10;
					/* falls through */
				case 2:
					factor *= 10;
					/* falls through */
				case 1:
					prevIsDisabled = Math.floor(year / factor) * factor <= startYear;
					nextIsDisabled = Math.floor(year / factor) * factor + factor > endYear;
					break;
				case 0:
					prevIsDisabled = year <= startYear && month <= startMonth;
					nextIsDisabled = year >= endYear && month >= endMonth;
					break;
			}

			this.picker.find('.prev').toggleClass('disabled', prevIsDisabled);
			this.picker.find('.next').toggleClass('disabled', nextIsDisabled);
		},

		click: function(e){
			e.preventDefault();
			e.stopPropagation();

			var target, dir, day, year, month;
			target = $(e.target);

			// Clicked on the switch
			if (target.hasClass('datepicker-switch') && this.viewMode !== this.o.maxViewMode){
				this.setViewMode(this.viewMode + 1);
			}

			// Clicked on today button
			if (target.hasClass('today') && !target.hasClass('day')){
				this.setViewMode(0);
				this._setDate(UTCToday(), this.o.todayBtn === 'linked' ? null : 'view');
			}

			// Clicked on clear button
			if (target.hasClass('clear')){
				this.clearDates();
			}

			if (!target.hasClass('disabled')){
				// Clicked on a month, year, decade, century
				if (target.hasClass('month')
						|| target.hasClass('year')
						|| target.hasClass('decade')
						|| target.hasClass('century')) {
					this.viewDate.setUTCDate(1);

					day = 1;
					if (this.viewMode === 1){
						month = target.parent().find('span').index(target);
						year = this.viewDate.getUTCFullYear();
						this.viewDate.setUTCMonth(month);
					} else {
						month = 0;
						year = Number(target.text());
						this.viewDate.setUTCFullYear(year);
					}

					this._trigger(DPGlobal.viewModes[this.viewMode - 1].e, this.viewDate);

					if (this.viewMode === this.o.minViewMode){
						this._setDate(UTCDate(year, month, day));
					} else {
						this.setViewMode(this.viewMode - 1);
						this.fill();
					}
				}
			}

			if (this.picker.is(':visible') && this._focused_from){
				this._focused_from.focus();
			}
			delete this._focused_from;
		},

		dayCellClick: function(e){
			var $target = $(e.currentTarget);
			var timestamp = $target.data('date');
			var date = new Date(timestamp);

			if (this.o.updateViewDate) {
				if (date.getUTCFullYear() !== this.viewDate.getUTCFullYear()) {
					this._trigger('changeYear', this.viewDate);
				}

				if (date.getUTCMonth() !== this.viewDate.getUTCMonth()) {
					this._trigger('changeMonth', this.viewDate);
				}
			}
			this._setDate(date);
		},

		// Clicked on prev or next
		navArrowsClick: function(e){
			var $target = $(e.currentTarget);
			var dir = $target.hasClass('prev') ? -1 : 1;
			if (this.viewMode !== 0){
				dir *= DPGlobal.viewModes[this.viewMode].navStep * 12;
			}
			this.viewDate = this.moveMonth(this.viewDate, dir);
			this._trigger(DPGlobal.viewModes[this.viewMode].e, this.viewDate);
			this.fill();
		},

		_toggle_multidate: function(date){
			var ix = this.dates.contains(date);
			if (!date){
				this.dates.clear();
			}

			if (ix !== -1){
				if (this.o.multidate === true || this.o.multidate > 1 || this.o.toggleActive){
					this.dates.remove(ix);
				}
			} else if (this.o.multidate === false) {
				this.dates.clear();
				this.dates.push(date);
			}
			else {
				this.dates.push(date);
			}

			if (typeof this.o.multidate === 'number')
				while (this.dates.length > this.o.multidate)
					this.dates.remove(0);
		},

		_setDate: function(date, which){
			if (!which || which === 'date')
				this._toggle_multidate(date && new Date(date));
			if ((!which && this.o.updateViewDate) || which === 'view')
				this.viewDate = date && new Date(date);

			this.fill();
			this.setValue();
			if (!which || which !== 'view') {
				this._trigger('changeDate');
			}
			this.inputField.trigger('change');
			if (this.o.autoclose && (!which || which === 'date')){
				this.hide();
			}
		},

		moveDay: function(date, dir){
			var newDate = new Date(date);
			newDate.setUTCDate(date.getUTCDate() + dir);

			return newDate;
		},

		moveWeek: function(date, dir){
			return this.moveDay(date, dir * 7);
		},

		moveMonth: function(date, dir){
			if (!isValidDate(date))
				return this.o.defaultViewDate;
			if (!dir)
				return date;
			var new_date = new Date(date.valueOf()),
				day = new_date.getUTCDate(),
				month = new_date.getUTCMonth(),
				mag = Math.abs(dir),
				new_month, test;
			dir = dir > 0 ? 1 : -1;
			if (mag === 1){
				test = dir === -1
					// If going back one month, make sure month is not current month
					// (eg, Mar 31 -> Feb 31 == Feb 28, not Mar 02)
					? function(){
						return new_date.getUTCMonth() === month;
					}
					// If going forward one month, make sure month is as expected
					// (eg, Jan 31 -> Feb 31 == Feb 28, not Mar 02)
					: function(){
						return new_date.getUTCMonth() !== new_month;
					};
				new_month = month + dir;
				new_date.setUTCMonth(new_month);
				// Dec -> Jan (12) or Jan -> Dec (-1) -- limit expected date to 0-11
				new_month = (new_month + 12) % 12;
			}
			else {
				// For magnitudes >1, move one month at a time...
				for (var i=0; i < mag; i++)
					// ...which might decrease the day (eg, Jan 31 to Feb 28, etc)...
					new_date = this.moveMonth(new_date, dir);
				// ...then reset the day, keeping it in the new month
				new_month = new_date.getUTCMonth();
				new_date.setUTCDate(day);
				test = function(){
					return new_month !== new_date.getUTCMonth();
				};
			}
			// Common date-resetting loop -- if date is beyond end of month, make it
			// end of month
			while (test()){
				new_date.setUTCDate(--day);
				new_date.setUTCMonth(new_month);
			}
			return new_date;
		},

		moveYear: function(date, dir){
			return this.moveMonth(date, dir*12);
		},

		moveAvailableDate: function(date, dir, fn){
			do {
				date = this[fn](date, dir);

				if (!this.dateWithinRange(date))
					return false;

				fn = 'moveDay';
			}
			while (this.dateIsDisabled(date));

			return date;
		},

		weekOfDateIsDisabled: function(date){
			return $.inArray(date.getUTCDay(), this.o.daysOfWeekDisabled) !== -1;
		},

		dateIsDisabled: function(date){
			return (
				this.weekOfDateIsDisabled(date) ||
				$.grep(this.o.datesDisabled, function(d){
					return isUTCEquals(date, d);
				}).length > 0
			);
		},

		dateWithinRange: function(date){
			return date >= this.o.startDate && date <= this.o.endDate;
		},

		keydown: function(e){
			if (!this.picker.is(':visible')){
				if (e.keyCode === 40 || e.keyCode === 27) { // allow down to re-show picker
					this.show();
					e.stopPropagation();
		}
				return;
			}
			var dateChanged = false,
				dir, newViewDate,
				focusDate = this.focusDate || this.viewDate;
			switch (e.keyCode){
				case 27: // escape
					if (this.focusDate){
						this.focusDate = null;
						this.viewDate = this.dates.get(-1) || this.viewDate;
						this.fill();
					}
					else
						this.hide();
					e.preventDefault();
					e.stopPropagation();
					break;
				case 37: // left
				case 38: // up
				case 39: // right
				case 40: // down
					if (!this.o.keyboardNavigation || this.o.daysOfWeekDisabled.length === 7)
						break;
					dir = e.keyCode === 37 || e.keyCode === 38 ? -1 : 1;
		  if (this.viewMode === 0) {
					if (e.ctrlKey){
						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveYear');

						if (newViewDate)
							this._trigger('changeYear', this.viewDate);
					} else if (e.shiftKey){
						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveMonth');

						if (newViewDate)
							this._trigger('changeMonth', this.viewDate);
					} else if (e.keyCode === 37 || e.keyCode === 39){
						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveDay');
					} else if (!this.weekOfDateIsDisabled(focusDate)){
						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveWeek');
					}
		  } else if (this.viewMode === 1) {
			if (e.keyCode === 38 || e.keyCode === 40) {
			  dir = dir * 4;
			}
			newViewDate = this.moveAvailableDate(focusDate, dir, 'moveMonth');
		  } else if (this.viewMode === 2) {
			if (e.keyCode === 38 || e.keyCode === 40) {
			  dir = dir * 4;
			}
			newViewDate = this.moveAvailableDate(focusDate, dir, 'moveYear');
		  }
					if (newViewDate){
						this.focusDate = this.viewDate = newViewDate;
						this.setValue();
						this.fill();
						e.preventDefault();
					}
					break;
				case 13: // enter
					if (!this.o.forceParse)
						break;
					focusDate = this.focusDate || this.dates.get(-1) || this.viewDate;
					if (this.o.keyboardNavigation) {
						this._toggle_multidate(focusDate);
						dateChanged = true;
					}
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.setValue();
					this.fill();
					if (this.picker.is(':visible')){
						e.preventDefault();
						e.stopPropagation();
						if (this.o.autoclose)
							this.hide();
					}
					break;
				case 9: // tab
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.fill();
					this.hide();
					break;
			}
			if (dateChanged){
				if (this.dates.length)
					this._trigger('changeDate');
				else
					this._trigger('clearDate');
				this.inputField.trigger('change');
			}
		},

		setViewMode: function(viewMode){
			this.viewMode = viewMode;
			this.picker
				.children('div')
				.hide()
				.filter('.datepicker-' + DPGlobal.viewModes[this.viewMode].clsName)
					.show();
			this.updateNavArrows();
	  this._trigger('changeViewMode', new Date(this.viewDate));
		}
	};

	var DateRangePicker = function(element, options){
		$.data(element, 'datepicker', this);
		this.element = $(element);
		this.inputs = $.map(options.inputs, function(i){
			return i.jquery ? i[0] : i;
		});
		delete options.inputs;

		this.keepEmptyValues = options.keepEmptyValues;
		delete options.keepEmptyValues;

		datepickerPlugin.call($(this.inputs), options)
			.on('changeDate', $.proxy(this.dateUpdated, this));

		this.pickers = $.map(this.inputs, function(i){
			return $.data(i, 'datepicker');
		});
		this.updateDates();
	};
	DateRangePicker.prototype = {
		updateDates: function(){
			this.dates = $.map(this.pickers, function(i){
				return i.getUTCDate();
			});
			this.updateRanges();
		},
		updateRanges: function(){
			var range = $.map(this.dates, function(d){
				return d.valueOf();
			});
			$.each(this.pickers, function(i, p){
				p.setRange(range);
			});
		},
		clearDates: function(){
			$.each(this.pickers, function(i, p){
				p.clearDates();
			});
		},
		dateUpdated: function(e){
			// `this.updating` is a workaround for preventing infinite recursion
			// between `changeDate` triggering and `setUTCDate` calling.  Until
			// there is a better mechanism.
			if (this.updating)
				return;
			this.updating = true;

			var dp = $.data(e.target, 'datepicker');

			if (dp === undefined) {
				return;
			}

			var new_date = dp.getUTCDate(),
				keep_empty_values = this.keepEmptyValues,
				i = $.inArray(e.target, this.inputs),
				j = i - 1,
				k = i + 1,
				l = this.inputs.length;
			if (i === -1)
				return;

			$.each(this.pickers, function(i, p){
				if (!p.getUTCDate() && (p === dp || !keep_empty_values))
					p.setUTCDate(new_date);
			});

			if (new_date < this.dates[j]){
				// Date being moved earlier/left
				while (j >= 0 && new_date < this.dates[j]){
					this.pickers[j--].setUTCDate(new_date);
				}
			} else if (new_date > this.dates[k]){
				// Date being moved later/right
				while (k < l && new_date > this.dates[k]){
					this.pickers[k++].setUTCDate(new_date);
				}
			}
			this.updateDates();

			delete this.updating;
		},
		destroy: function(){
			$.map(this.pickers, function(p){ p.destroy(); });
			$(this.inputs).off('changeDate', this.dateUpdated);
			delete this.element.data().datepicker;
		},
		remove: alias('destroy', 'Method `remove` is deprecated and will be removed in version 2.0. Use `destroy` instead')
	};

	function opts_from_el(el, prefix){
		// Derive options from element data-attrs
		var data = $(el).data(),
			out = {}, inkey,
			replace = new RegExp('^' + prefix.toLowerCase() + '([A-Z])');
		prefix = new RegExp('^' + prefix.toLowerCase());
		function re_lower(_,a){
			return a.toLowerCase();
		}
		for (var key in data)
			if (prefix.test(key)){
				inkey = key.replace(replace, re_lower);
				out[inkey] = data[key];
			}
		return out;
	}

	function opts_from_locale(lang){
		// Derive options from locale plugins
		var out = {};
		// Check if "de-DE" style date is available, if not language should
		// fallback to 2 letter code eg "de"
		if (!dates[lang]){
			lang = lang.split('-')[0];
			if (!dates[lang])
				return;
		}
		var d = dates[lang];
		$.each(locale_opts, function(i,k){
			if (k in d)
				out[k] = d[k];
		});
		return out;
	}

	var old = $.fn.datepicker;
	var datepickerPlugin = function(option){
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function(){
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data){
				var elopts = opts_from_el(this, 'date'),
					// Preliminary otions
					xopts = $.extend({}, defaults, elopts, options),
					locopts = opts_from_locale(xopts.language),
					// Options priority: js args, data-attrs, locales, defaults
					opts = $.extend({}, defaults, locopts, elopts, options);
				if ($this.hasClass('input-daterange') || opts.inputs){
					$.extend(opts, {
						inputs: opts.inputs || $this.find('input').toArray()
					});
					data = new DateRangePicker(this, opts);
				}
				else {
					data = new Datepicker(this, opts);
				}
				$this.data('datepicker', data);
			}
			if (typeof option === 'string' && typeof data[option] === 'function'){
				internal_return = data[option].apply(data, args);
			}
		});

		if (
			internal_return === undefined ||
			internal_return instanceof Datepicker ||
			internal_return instanceof DateRangePicker
		)
			return this;

		if (this.length > 1)
			throw new Error('Using only allowed for the collection of a single element (' + option + ' function)');
		else
			return internal_return;
	};
	$.fn.datepicker = datepickerPlugin;

	var defaults = $.fn.datepicker.defaults = {
		assumeNearbyYear: false,
		autoclose: false,
		beforeShowDay: $.noop,
		beforeShowMonth: $.noop,
		beforeShowYear: $.noop,
		beforeShowDecade: $.noop,
		beforeShowCentury: $.noop,
		calendarWeeks: false,
		clearBtn: false,
		toggleActive: false,
		daysOfWeekDisabled: [],
		daysOfWeekHighlighted: [],
		datesDisabled: [],
		endDate: Infinity,
		forceParse: true,
		format: 'mm/dd/yyyy',
		keepEmptyValues: false,
		keyboardNavigation: true,
		language: 'en',
		minViewMode: 0,
		maxViewMode: 4,
		multidate: false,
		multidateSeparator: ',',
		orientation: "auto",
		rtl: false,
		startDate: -Infinity,
		startView: 0,
		todayBtn: false,
		todayHighlight: false,
		updateViewDate: true,
		weekStart: 0,
		disableTouchKeyboard: false,
		enableOnReadonly: true,
		showOnFocus: true,
		zIndexOffset: 10,
		container: 'body',
		immediateUpdates: false,
		title: '',
		templates: {
			leftArrow: '&#x00AB;',
			rightArrow: '&#x00BB;'
		},
	showWeekDays: true
	};
	var locale_opts = $.fn.datepicker.locale_opts = [
		'format',
		'rtl',
		'weekStart'
	];
	$.fn.datepicker.Constructor = Datepicker;
	var dates = $.fn.datepicker.dates = {
		en: {
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			today: "Today",
			clear: "Clear",
			titleFormat: "MM yyyy"
		},
		ru: {
			days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
			daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
			daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
			months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
			monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
			today: "Сегодня",
			clear: "Очистить",
			format: "dd.mm.yyyy",
			weekStart: 1,
			monthsTitle: 'Месяцы'
		}
	};

	var DPGlobal = {
		viewModes: [
			{
				names: ['days', 'month'],
				clsName: 'days',
				e: 'changeMonth'
			},
			{
				names: ['months', 'year'],
				clsName: 'months',
				e: 'changeYear',
				navStep: 1
			},
			{
				names: ['years', 'decade'],
				clsName: 'years',
				e: 'changeDecade',
				navStep: 10
			},
			{
				names: ['decades', 'century'],
				clsName: 'decades',
				e: 'changeCentury',
				navStep: 100
			},
			{
				names: ['centuries', 'millennium'],
				clsName: 'centuries',
				e: 'changeMillennium',
				navStep: 1000
			}
		],
		validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
		nonpunctuation: /[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,
		parseFormat: function(format){
			if (typeof format.toValue === 'function' && typeof format.toDisplay === 'function')
				return format;
			// IE treats \0 as a string end in inputs (truncating the value),
			// so it's a bad format delimiter, anyway
			var separators = format.replace(this.validParts, '\0').split('\0'),
				parts = format.match(this.validParts);
			if (!separators || !separators.length || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separators: separators, parts: parts};
		},
		parseDate: function(date, format, language, assumeNearby){
			if (!date)
				return undefined;
			if (date instanceof Date)
				return date;
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			if (format.toValue)
				return format.toValue(date, format, language);
			var fn_map = {
					d: 'moveDay',
					m: 'moveMonth',
					w: 'moveWeek',
					y: 'moveYear'
				},
				dateAliases = {
					yesterday: '-1d',
					today: '+0d',
					tomorrow: '+1d'
				},
				parts, part, dir, i, fn;
			if (date in dateAliases){
				date = dateAliases[date];
			}
			if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/i.test(date)){
				parts = date.match(/([\-+]\d+)([dmwy])/gi);
				date = new Date();
				for (i=0; i < parts.length; i++){
					part = parts[i].match(/([\-+]\d+)([dmwy])/i);
					dir = Number(part[1]);
					fn = fn_map[part[2].toLowerCase()];
					date = Datepicker.prototype[fn](date, dir);
				}
				return Datepicker.prototype._zero_utc_time(date);
			}

			parts = date && date.match(this.nonpunctuation) || [];

			function applyNearbyYear(year, threshold){
				if (threshold === true)
					threshold = 10;

				// if year is 2 digits or less, than the user most likely is trying to get a recent century
				if (year < 100){
					year += 2000;
					// if the new year is more than threshold years in advance, use last century
					if (year > ((new Date()).getFullYear()+threshold)){
						year -= 100;
					}
				}

				return year;
			}

			var parsed = {},
				setters_order = ['yyyy', 'yy', 'M', 'MM', 'm', 'mm', 'd', 'dd'],
				setters_map = {
					yyyy: function(d,v){
						return d.setUTCFullYear(assumeNearby ? applyNearbyYear(v, assumeNearby) : v);
					},
					m: function(d,v){
						if (isNaN(d))
							return d;
						v -= 1;
						while (v < 0) v += 12;
						v %= 12;
						d.setUTCMonth(v);
						while (d.getUTCMonth() !== v)
							d.setUTCDate(d.getUTCDate()-1);
						return d;
					},
					d: function(d,v){
						return d.setUTCDate(v);
					}
				},
				val, filtered;
			setters_map['yy'] = setters_map['yyyy'];
			setters_map['M'] = setters_map['MM'] = setters_map['mm'] = setters_map['m'];
			setters_map['dd'] = setters_map['d'];
			date = UTCToday();
			var fparts = format.parts.slice();
			// Remove noop parts
			if (parts.length !== fparts.length){
				fparts = $(fparts).filter(function(i,p){
					return $.inArray(p, setters_order) !== -1;
				}).toArray();
			}
			// Process remainder
			function match_part(){
				var m = this.slice(0, parts[i].length),
					p = parts[i].slice(0, m.length);
				return m.toLowerCase() === p.toLowerCase();
			}
			if (parts.length === fparts.length){
				var cnt;
				for (i=0, cnt = fparts.length; i < cnt; i++){
					val = parseInt(parts[i], 10);
					part = fparts[i];
					if (isNaN(val)){
						switch (part){
							case 'MM':
								filtered = $(dates[language].months).filter(match_part);
								val = $.inArray(filtered[0], dates[language].months) + 1;
								break;
							case 'M':
								filtered = $(dates[language].monthsShort).filter(match_part);
								val = $.inArray(filtered[0], dates[language].monthsShort) + 1;
								break;
						}
					}
					parsed[part] = val;
				}
				var _date, s;
				for (i=0; i < setters_order.length; i++){
					s = setters_order[i];
					if (s in parsed && !isNaN(parsed[s])){
						_date = new Date(date);
						setters_map[s](_date, parsed[s]);
						if (!isNaN(_date))
							date = _date;
					}
				}
			}
			return date;
		},
		formatDate: function(date, format, language){
			if (!date)
				return '';
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			if ((format !== null) && format.hasOwnProperty('toDisplay') && format.toDisplay)
				return format.toDisplay(date, format, language);
			var val = {
				d: date.getUTCDate(),
				D: dates[language].daysShort[date.getUTCDay()],
				DD: dates[language].days[date.getUTCDay()],
				m: date.getUTCMonth() + 1,
				M: dates[language].monthsShort[date.getUTCMonth()],
				MM: dates[language].months[date.getUTCMonth()],
				yy: date.getUTCFullYear().toString().substring(2),
				yyyy: date.getUTCFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			date = [];
			var seps = $.extend([], format.separators);
			for (var i=0, cnt = format.parts.length; i <= cnt; i++){
				if (seps.length)
					date.push(seps.shift());
				date.push(val[format.parts[i]]);
			}
			return date.join('');
		},
		headTemplate: '<thead>'+
						  '<tr>'+
							'<th colspan="7" class="datepicker-title"></th>'+
						  '</tr>'+
							'<tr>'+
								'<th class="prev">'+defaults.templates.leftArrow+'</th>'+
								'<th colspan="5" class="datepicker-switch"></th>'+
								'<th class="next">'+defaults.templates.rightArrow+'</th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
		footTemplate: '<tfoot>'+
							'<tr>'+
								'<th colspan="7" class="today"></th>'+
							'</tr>'+
							'<tr>'+
								'<th colspan="7" class="clear"></th>'+
							'</tr>'+
						'</tfoot>'
	};
	DPGlobal.template = '<div class="datepicker">'+
							'<div class="datepicker-days">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-decades">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-centuries">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
						'</div>';

	$.fn.datepicker.DPGlobal = DPGlobal;


	/* DATEPICKER NO CONFLICT
	* =================== */

	$.fn.datepicker.noConflict = function(){
		$.fn.datepicker = old;
		return this;
	};

	/* DATEPICKER VERSION
	 * =================== */
	$.fn.datepicker.version = '1.9.0';

	$.fn.datepicker.deprecated = function(msg){
		var console = window.console;
		if (console && console.warn) {
			console.warn('DEPRECATED: ' + msg);
		}
	};


	/* DATEPICKER DATA-API
	* ================== */

	$(document).on(
		'focus.datepicker.data-api click.datepicker.data-api',
		'[data-provide="datepicker"]',
		function(e){
			var $this = $(this);
			if ($this.data('datepicker'))
				return;
			e.preventDefault();
			// component click requires us to explicitly show it
			datepickerPlugin.call($this, 'show');
		}
	);
	$(function(){
		datepickerPlugin.call($('[data-provide="datepicker-inline"]'));
	});

}));

class AppServiceWorker {
	static isSW_ = false;
	static isPM_ = false;
	static isNotify_ = false;
	static isInstall_ = false;
	static handleInstall_ = null;
	static eleId_ = [];

	static PwaMode_() {
		const isStandalone = window.matchMedia('(display-mode: standalone)').matches;
		if (document.referrer.startsWith('android-app://'))
			return false;
		else if (navigator.standalone || isStandalone)
			return false;
		return true;
	};
	static InstallSelector_(b, h) {
		AppServiceWorker.isInstall_ = b;
		AppServiceWorker.handleInstall_ = h;
		if (AppServiceWorker.eleId_.length > 0) {
			if (b)
				$(AppServiceWorker.eleId_[0]).show();
			else
				$(AppServiceWorker.eleId_[0]).hide();
		}
	};
	static InstallChoice_(ev) {
		if (ev === null)
			return;

		ev.userChoice.then(function (res) {
			if ((res.outcome == 'dismissed') && AppServiceWorker.PwaMode_()) {
				AppServiceWorker.InstallSelector_(true, ev);
			} else if (AppServiceWorker.isInstall_) {
				AppServiceWorker.InstallSelector_(false, null);
			}
		})
		.catch((error) => {
			console.log('App InstallChoice', error);
		});
	};
	static NotifySend(data) {
		try {
			if (!AppServiceWorker.isNotify_)
				return;
			new Notification(JSON.stringify(data));
		} catch (error) { console.log('App NotifySend', error); }
	};
	static Init(cls) {
		try {
			if (window.location.protocol === 'http:')
				return;

			AppServiceWorker.eleId_ = cls;
			AppServiceWorker.isSW_ = ('serviceWorker' in navigator);
			AppServiceWorker.isPM_ = ('PushManager' in window);
			AppServiceWorker.isNotify_ = ((typeof (self.Notification) !== 'undefined') &&
				('showNotification' in ServiceWorkerRegistration.prototype));

			if (!AppServiceWorker.isSW_)
				return;

			window.addEventListener('load', () => {
				navigator.serviceWorker.register('/data/workboxjs', { scope: '/' })
					.then(function () {
						navigator.serviceWorker.onmessage = function (evt) {
							if (evt.data !== null) {
								let msg = JSON.parse(evt.data);
								console.log('on message:', msg);

								if (msg.hasOwnProperty('type')) {
									switch (msg.type) {
										case 'offline':
											document.location.replace('/data/pwa/offline'); break;
										default: break;
									}
								}
							}
						};

						if (AppServiceWorker.isPM_ && AppServiceWorker.isNotify_) {
							switch (Notification.permission) {
								case 'blocked':
								case 'granted': break;
								default: {
									Notification.requestPermission();
									break;
								}
							}
						}
					})
					.catch((error) => { console.log('PWA ERR:', error); });
			});
			window.addEventListener('beforeinstallprompt', (ev) => {
				ev.preventDefault();
				AppServiceWorker.InstallChoice_(ev);
			});
			window.addEventListener('appinstalled', () => {
				AppServiceWorker.InstallSelector_(false, null);
			});
			window.addEventListener('online', () => {
				window.location.reload();
			});

			$(document).on('click', AppServiceWorker.eleId_[0], function () {
				try {
					if (AppServiceWorker.isInstall_ && (AppServiceWorker.handleInstall_ !== null)) {
						AppServiceWorker.handleInstall_.prompt();
						AppServiceWorker.InstallChoice_(ev);

						if (!AppServiceWorker.isInstall_)
							AppServiceWorker.NotifySend({
								title: 'M3U Плей-лист Сервис - Web приложение',
								body: 'Приложение успешно установленно',
								icon: '/data/images/logo/144'
							});
					}
				} catch (error) { console.log('App Service Worker click', error); }
			});
		} catch (error) { console.log('App Service Worker init', error); }
	};
};



class AppLanguage {
    constructor() {
        this.LocaleDateFmtWYMDHM = null;
        this.LocaleDateFmtWYMD = null;
        this.LocaleDateFmtDHM = null;
        this.LocaleLang = 'ru';
    };
    getLocaleLang() {
        return this.LocaleLang;
    };
    setLocaleLang(l) {
        this.LocaleLang = l;
        this.LocaleDateFmtWYMDHM = null;
        this.LocaleDateFmtWYMD = null;
    };
    static SetLocale() {
        try {
            if (typeof (Intl) != 'undefined')
                this.LocaleLang = Intl.NumberFormat().resolvedOptions().locale;
            else if (window.navigator.languages) {
                this.LocaleLang = window.navigator.languages[0];
            } else {
                this.LocaleLang = window.navigator.userLanguage || window.navigator.language;
            }
        } catch (error) {
            console.log(error);
            this.LocaleLang = 'ru';
        }
        AppLanguage.Languages.default = eval('AppLanguage.Languages.' + this.LocaleLang);
    };
    static GetDateFormaterWYMDHM() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtWYMDHM == null) {
            this.LocaleDateFmtWYMDHM =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour: '2-digit',
                        minute: '2-digit'
                });
        }
        return this.LocaleDateFmtWYMDHM;
    };
    static GetDateFormaterWYMD() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtWYMD == null) {
            this.LocaleDateFmtWYMD =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                });
        }
        return this.LocaleDateFmtWYMD;
    };
    static GetDateFormaterDHM() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtDHM == null) {
            this.LocaleDateFmtDHM =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                    day: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                });
        }
        return this.LocaleDateFmtWYMDHM;
    };
    static GetLanguage() {
        try {
            if (this.LocaleLang == '')
                return this.Languages.default;
            else
                return eval('AppLanguage.Languages.' + this.LocaleLang);
        } catch (error) {
            console.log(error);
            return AppLanguage.Languages.default;
        }
    };
    static GetLanguageKey(key) {
        try {
            if (this.LocaleLang == '')
                return eval('AppLanguage.Languages.default.' + key);
            else
                return eval('AppLanguage.Languages.' + this.LocaleLang + '.' + key);
        } catch (error) {
            console.log(error);
            return AppLanguage.Languages.default;
        }
    };
    static Languages = {
        en: {
            constant: {
                on: "on",
                off: "off",
                error: "error",
                notrun: "did not start"
            },
            selector: {
                opt0: "Open on this page",
                opt1: "Open a new page and play",
                opt2: "Play with built-in player",
                opt3: "Open To ..",
                opt4: "Other options are currently not available .."
            },
            warnings: {
                msg1: "You are already in the root directory",
                msg2: "Connection error",
                msg3: "Service returned an error",
                msg4: "Command sending error",
                msg5: "Limit is exceeded",
                msg6: "Your browsing history is empty",
                msg7: "Feature not supported by browser",
                msg8: "You have successfully signed out until you can use advanced commands and manage the service.",
                msg9: "File upload",
                msg10_1: "A new version of the service is available, you can download",
                msg10_2: "this version from the link.",
                msg11: "Your browser not support the speech synthesis",
                msg12: "To be, or not to be, that is the question: Whether tis nobler in the mind to suffer the slings and arrows of outrageous fortune, or to take arms against a sea of troubles and by opposing end them.",
                msg13: "Root folder not defined, no further execution possible",
                msg14: "Failure to copy. Check permissions for clipboard"
            },
            title: {
                msg0: "PlayList service: ",
                msg1: "M3U playlist",
                msg2: "Video library",
                msg3: "Audio library",
                msg4: "Torrents",
                msg5: "View History"
            },
            body: {
                lang: "en",
                name: "M3U Play list service",
                msg1: "Update service configuration",
                msg2: "Refresh M3U playlists",
                msg3: "Refresh Playlists of Video Archive",
                msg4: "Run playlist check",
                msg5: "Update TV EPG Program",
                msg6: "Play to",
                msg7: "Loading...",
                msg8: "Prev",
                msg9: "Next",
                msg10: "Select target",
                msg11: "Playlists",
                msg12: "path in area",
                msg13: "Video",
                msg14: "Audio",
                msg15: "Play to",
                msg16: "M3U",
                msg17: "History",
                footer1: "This program does not store video files. The base of links to videos and the video fragments themselves are created by the user independently.<br/>As a rule, links to public URLs of video streams are deliberately distributed",
                footer2: "by copyright holders and operators providing access to video streams, the latter do it for advertising purposes hoping to increase the paid audience.<br/>Most operators providing access services officially publish their own playlists in M3U format among its paid subscribers.",
                footer3: "The creation of links is not a direct violation of copyright, as it is a description of the resource and not the author's material itself.",
                footer4: "If you think that any links in your playlists violate the rights of the copyright holder or access operator - remove them! :)",
                footer5: "Install as app"
            },
            nfo: {
                none: "NONE",
                minutes: "min",
                seconds: "sec",
                season: "SEASON",
                episode: "EPISODE",
                close: "Close",
                actors: "Actors",
                directors: "Directors",
                credits: "Credits",
                tags: "Tags",
                video: "VIDEO",
                aspect: "ASPECT",
                audio: "AUDIO",
                channels: "CHANNELS"
            },
            audiobox: {
                title: "Title:",
                disk: "Track/Disk:",
                desc: "Description:",
                copy: "Copyright:",
                pub: "Publisher:",
                date: "Date:",
                dur: "Duration:",
                albums: "Albums:",
                artists: "Artists:",
                comps: "Composers:",
                genres: "Genres:",
                tags: "Tags:"
            },
            torrent: {
                submit: "Create torrent",
                close: "Close",
                ext: "file mask",
                trackers: "tracker list",
                onedir: "only root directory",
                alldirs: "add all sub-directories",
                basedir: "creating a torrent from the contents of a directory",
                copied: "Magnet link copied",
                magnet: "Copy magnet link",
                download: "Download torrent"
            },
            stat: {
                submit: "Show service statistics",
                close:  "Hide service statistics",
                swstat: "file processing statistics",
                swtask: "running task statistics",
                id: "Id",
                last: "Update",
                all: "All",
                error: "Error",
                reject: "Reject",
                good: "Good",
                bad: "Bad",
                dup: "Dup",
                scan: "Scan M3U files",
                scanvideo: "Scan Video files",
                scanaudio: "Scan Audio files",
                chanaudio: "Audio broadcast channel",
                check: "Check all file source",
                download: "Playlist update",
                version: "Version check",
                epg: "EPG update",
                history: "Save history",
                move: "Create a savepoint",
                monitor: "State",
                type: "Task type",
                period: "Launch period",
                due: "Last run time",
                status: "Status"
            },
            default: null
        },
        ru: {
            constant: {
                on: "включено",
                off: "выключено",
                error: "ошибка",
                notrun: "не запускался"
            },
            selector: {
                opt0: "Открывать на этой странице",
                opt1: "Открыть новую страницу и воспроизвести",
                opt2: "Воспроизвести с помощью встроенного плеера",
                opt3: "Открыть в ..",
                opt4: "Другие варианты сейчас не доступны.."
            },
            warnings: {
                msg1: "Вы уже находитесь в корневой директории",
                msg2: "Ошибка соединения",
                msg3: "Сервис вернул ошибку",
                msg4: "Ошибка отправки команды",
                msg5: "Превышен лимит",
                msg6: "История просмотров пуста",
                msg7: "Возможность не поддерживается браузером",
                msg8: "Вы успешно вышли из аккаунта, пока вы не сможете использовать расширенные команды и управлять сервисом.",
                msg9: "Отправка файла",
                msg10_1: "Доступна новая версия сервиса, вы можете загрузить версию",
                msg10_2: "по ссылке.",
                msg11: "Ваш браузер не поддерживает синтез речи",
                msg12: "Как скучно мы живем! В нас пропал дух авантюризма! Мы перестали лазить в окна к любимым женщинам. Мы перестали делать большие хорошие глупости.",
                msg13: "Корневая папка не определена, дальнейшее выполнение невозможно",
                msg14: "Невозможно скопировать, проверьте права на запись буфера обмена"
            },
            title: {
                msg0: "Плей-лист сервис: ",
                msg1: "M3U плей-листы",
                msg2: "Видео архив",
                msg3: "Аудио архив",
                msg4: "Торренты",
                msg5: "История просмотров"
            },
            body: {
                lang: "ru",
                name: "Сервис плей-листов M3U",
                msg1: "Обновить конфигурацию сервиса",
                msg2: "Обновить плей-листы M3U",
                msg3: "Обновить плей-листы Видео архива",
                msg4: "Запустиь проверку плей-листов",
                msg5: "Обновить ТВ программу EPG",
                msg6: "Воспроизвести на",
                msg7: "Загружаем...",
                msg8: "Туда",
                msg9: "Сюда",
                msg10: "Выбор назначения",
                msg11: "Плей-лист",
                msg12: "путь в категории",
                msg13: "Видео",
                msg14: "Аудио",
                msg15: "Воспроизвести на",
                msg16: "M3U",
                msg17: "История",
                footer1: "В этой программе не хранятся видеофайлы. База ссылок на видео и сами видео-фрагменты создаются пользователем самостоятельно.<br/>Как правило, ссылки на общедоступные URL-адреса видеопотоков, распространяется намеренно",
                footer2: "правообладателями и операторами предоставляющими услуги доступа к видео потокам, последние делают это в рекламных целях надеясь на увеличение платной аудитории.<br/>Большинство операторов предоставляющих услуги доступа официально рспостроняют собственные плей- листы в формате M3U среди своих платных подписчиков.",
                footer3: "Создание ссылок не является прямым нарушением авторских прав, так-как это является описанием ресурса а не самим материалом автора.",
                footer4: "Если вам кажется что какие-либо ссылки в ваших плейлистах нарушают права правообладателя или оператора доступа - удалите их! :)",
                footer5: "Установить как приложение"
            },
            nfo: {
                none: "НЕИЗВЕСТНО",
                minutes: "мин",
                seconds: "сек",
                season: "Сезон",
                episode: "Серия",
                close: "Закрыть",
                actors: "Актеры",
                directors: "Директора",
                credits: "Помогали",
                tags: "Тэги",
                video: "ВИДЕО",
                aspect: "АСПЕКТ",
                audio: "АУДИО",
                channels: "КАНАЛЫ"
            },
            audiobox: {
                title: "Название:",
                disk: "Трек/Диск:",
                desc: "Описание:",
                copy: "Авторское право:",
                pub: "Издательство:",
                date: "Дата:",
                dur: "Длительность:",
                albums: "Альбомы:",
                artists: "Артисты:",
                comps: "Композиторы:",
                genres: "Жанры:",
                tags: "Теги:"
            },
            torrent: {
                submit: "Создать торрент",
                close: "Закрыть",
                ext: "маска файлов",
                trackers: "список трекеров",
                onedir: "только корневая директория",
                alldirs: "добавить все под-директории",
                basedir: "создание торрента из содержимого директории",
                copied: "Magnet link скопирован",
                magnet: "Копировать магнет ссылку",
                download: "Скачать торрент"
            },
            stat: {
                submit: "Показать статистику сервиса",
                close:  "Скрыть статистику сервиса",
                swstat: "статистика обработки файлов",
                swtask: "статистика запущенных задач",
                id: "№",
                last: "Обновлено",
                all: "Всего",
                error: "Ошибки",
                reject: "Отклонено",
                good: "Удачно",
                bad: "Плохие",
                dup: "Дупликаты",
                scan: "Обработка M3U файлов",
                scanvideo: "Обработка видео файлов",
                scanaudio: "Обработка аудио файлов",
                chanaudio: "Создание аудио трансляций",
                check: "Проверка всех файлов",
                download: "Обновление плей-листов",
                version: "Проверка версии",
                epg: "Обновление EPG",
                history: "Сохронение истории",
                move: "Создание точки сохранения",
                monitor: "Состояние",
                type: "Тип задачи",
                period: "Период запуска",
                due: "Время последнего запуска",
                status: "Статус"
            },
            default: null
        },
        default: null
    };
};

jQuery.fn.localize = function () {
    return this.each(function () {
        let this_ = $(this);
        let tag = this_.data('localize');
        this_.html(AppLanguage.GetLanguageKey(tag));
    });
};
if (!String.prototype.isEmpty) {
    String.prototype.isEmpty = function () {
        'use strict';
        return (!this || this.length === 0 || !this.trim());
    };
}
if (!String.prototype.searchLowerCase) {
    String.prototype.searchLowerCase = function (tag) {
        'use strict';
        if (tag.length > this.length) {
            return false;
        } else {
            return this.toLowerCase().indexOf(tag, 0) !== -1;
        }
    };
}

class AppTaskContainer {
	constructor() {
		this.cb = $.Callbacks();
		this.data = {};
	};
	get Data() {
		return this.data;
	};
	buildCb(fun) {
		if ((fun === null) || (typeof (fun) !== 'function'))
			return;
		this.cb.add(fun);
	};
	buildsCb(funs) {
		if ((funs === null) || !$.isArray(funs))
			return;
		for (let f of funs)
			this.cb.add(f);
	};
	build(dataX) {
		try {
			this.data = dataX;
			if ("Promise" in window) {
				const this_ = this;
				const p = new window.Promise((resolve, reject) => {
					this.cb.fire(this_.data);
					resolve();
				});
				p.then(() => { });
				p.catch((error) => console.log(error));
			} else {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg7);
				this.cb.fire(this.data);
			}
		} catch (a) { console.log(a); }
	};
};


class AppConnector {
	static getRequest(url, farr) {
		try {
			$('#spinnerTop').show();

			let xhr = new XMLHttpRequest();
			xhr.overrideMimeType("text/xml");
			xhr.responseType = 'document';
			xhr.withCredentials = false;
			xhr.open('GET', url, true)
			xhr.onload = function () {
				AppConnector.RequestOnLoad_(this, farr);
			};
			xhr.onerror = function () {
				AppConnector.RequestOnError_(
					this,
					AppLanguage.GetLanguage().warnings.msg2,
					AppLanguage.GetLanguage().warnings.msg2
				);
			};
			xhr.onloadend = function () {
				$('#spinnerTop').hide();
			};
			xhr.send();

		} catch (a) { console.log(a); }
	};
	static RequestOnError_(xhr, s1, s2) {
		if ((typeof (xhr.status) === 'undefined') && (typeof (xhr.statusText) === 'undefined')) {
			if (s1 !== null)
				AppConnector.showError(s1);
		}
		else if (typeof (xhr.status) === 'undefined')
			AppConnector.showError(`${s2} = ${xhr.statusText}`);
		else if (typeof (xhr.statusText) === 'undefined')
			AppConnector.showError(`${s2} = ${xhr.status}`);
		else
			AppConnector.showError(`${s2} = ${xhr.status}: ${xhr.statusText}`);
	};
	static RequestOnLoad_(xhr, farr) {
		try {
			if (xhr.response == null)
				return;
			let x = new X2JS().xml2json(xhr.response);
			/* console.log('-X-', x); */
			if ((typeof (x) != 'undefined') && (x != null))
				try {
					for (let f of farr) {
						if ((typeof (f) != 'undefined') && (f != null)) {

							if (x.hasOwnProperty('error') &&
								(x.error != null) &&
								(typeof (x.error) === 'string') &&
								(!x.error.isEmpty())) { AppConnector.showError(x.error); }

							else if (x.hasOwnProperty('status') && (x.status != null)) {
								if (x.status.hasOwnProperty('error') &&
									(x.status.error != null) &&
									(typeof (x.status.error) === 'string') &&
									(!x.status.error.isEmpty())) {
									AppConnector.showError(`${x.status.errorcode} = ${x.status.error}`); }}

							if (x.hasOwnProperty('root'))
								f.build(x.root);
							else
								f.build(x);
						}
					}
				} catch (a) { console.log(a); }
		}
		catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2} = ${a.message}`); }
		finally { $('#spinnerTop').hide(); }
	};
	static showError(str) {
		if ((typeof (str) === 'undefined') || str.isEmpty())
			return;
		$('.text-error-container').html(str);
		$('#ctrlNotifyBox').fadeIn();
	};
	static reloadDefault(farr) {
		if ((typeof (farr) != 'undefined') && (farr != null))
			try {
				for (let f of farr) {
					if ((typeof (f) != 'undefined') && (f != null))
						f.defaultSet();
				}
			} catch (a) { console.log(a); }
	};
	static reloadBegin(url, farr) {
		AppConnector.reloadDefault(farr);
		AppConnector.getRequest(url, farr);
	};
	static saveSettings(s, farr) {
		if ((typeof (farr) === 'undefined') || (farr === null))
			return;

		if (farr[1].State === farr[1].states.listHistory)
			return;

		try {
			window.localStorage.setItem(AppPlaySelector.Tag, JSON.stringify(
				{
					ustate: farr[0].State,
					sstate: farr[1].State,
					speechenable: farr[3].IsSpeak,
					speechvoice: farr[2].VoiceIndex,
					speechrate: farr[2].rate,
					speechpitch: farr[2].pitch
				}
			));
			let data = ((typeof (s) === 'undefined') || (s === null)) ? farr[0].Data : s;
			if ((data === null) || (typeof (data) !== 'string'))
				return;

			window.localStorage.setItem(AppUrl.Tag, JSON.stringify(
				{ saveurl: data }
			));
		} catch (error) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2}: ${error.message}`);
		}
	};
	static loadSettings(farr) {
		if ((typeof (farr) === 'undefined') || (farr === null))
			return null;

		try {
			var json = window.localStorage.getItem(AppPlaySelector.Tag);
			if (json !== null) {
				let obj = JSON.parse(json);
				if (obj.hasOwnProperty('ustate'))
					farr[0].State = obj.ustate;
				if (obj.hasOwnProperty('sstate'))
					farr[1].State = obj.sstate;
				if (obj.hasOwnProperty('speechrate'))
					farr[2].rate = obj.speechrate;
				if (obj.hasOwnProperty('speechpitch'))
					farr[2].pitch = obj.speechpitch;
				if (obj.hasOwnProperty('speechvoice')) {
					const t = typeof (obj.speechvoice);
					if ((t === 'number') && (obj.speechvoice >= 0))
						farr[2].VoiceIndex = obj.speechvoice;
					else if ((t === 'string') && (!obj.speechvoice.isEmpty()))
						farr[2].VoiceIndex = parseInt(obj.speechvoice);
				}
				if (obj.hasOwnProperty('speechenable'))
					farr[3].IsSpeak = obj.speechenable;
			}
			json = window.localStorage.getItem(AppUrl.Tag);
			if (json !== null) {
				let obj = JSON.parse(json);
				if (obj.hasOwnProperty('saveurl') && ((obj.saveurl !== null) && (typeof (obj.saveurl) === 'string') && !obj.saveurl.isEmpty()))
					return obj.saveurl;
			}
			console.log('loadSettings', json);
		} catch (error) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2}: ${error.message}`);
		}
		return null;
	};
};


class AppUtils {
	constructor() {
		this.mime = {
			ext: {
				nfo:   ['nfo'],
				m3u:   ['m3u', 'm3u8'],
				xspf:  ['xspf'],
				image: ['jpg', 'jpeg', 'png', 'gif'],
				video: ['mpeg', 'mpg', 'mp4', 'avi', 'wmv', 'webm', 'mov', 'mkv', 'm2v', 'm4v', 'm4p', 'mpeg2', 'mpeg-2'],
				audio: ['mp3','wav','aac','wma','opus']
			}
		};
		this.items = {
			dir: ['папка', 'папки', 'папок'],
			file: ['файл', 'файла', 'файлов'],
			day: ['день', 'дня', 'дней'],
			hour: ['час', 'часа', 'часов'],
			min: ['минута', 'минуты', 'минут'],
			sec: ['секунда', 'секунды', 'секунд']
		};
	};
	calcDateString__(n) {
		switch ((n % 100) % 10) {
			case 0:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9: return 2;
			case 2:
			case 3:
			case 4: return (n > 9 && 20 > n) ? 2 : 1;
			case 1: return 0;
			default: return 0;
		}
	};
	stringDirectory(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.dir[idx]}`;
	};
	stringFiles(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.file[idx]}`;
	};
	stringDay(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.day[idx]}`;
	};
	stringHour(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.hour[idx]}`;
	};
	stringMinute(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.min[idx]}`;
	};
	stringSeconds(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.sec[idx]}`;
	};
	stringEmpty(val) {
		return ((typeof (val) !== 'string') || (val === null) || val.isEmpty());
	};
	checkObject(root, name) {
		return ((typeof (root) != 'undefined') &&
				(root != null) &&
				root.hasOwnProperty(name));
	};
	checkFileIsImage(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.image.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsVideo(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.video.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsAudio(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.audio.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsNfo(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.nfo.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsM3u(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.m3u.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsXspf(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.xspf.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	getBool(val) {
		if (val === null)
			return false;

		let t = typeof (val);
		if (t === 'boolean')
			return val;
		else if (t === 'number')
			return (val > 0);
		else if (t === 'string')
			return (val === 'true');
		return false;
	}
	getProgressPosition(ele, duration, ev) {
		let b = ele.getBoundingClientRect();
		let x = ev.clientX - b.left;
		let p = Math.floor(x / (ele.clientWidth / 100));
		return [parseFloat(p * (duration / 100)), p];
	};
	StringToDateConverter(val) {
		const regex = /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/s;
		const result = val.match(regex);
		return ((result !== null) && (result.length > 0)) ? result[0] : val;
	};
	TimeSpanTicksConverter(val) {
		let str = '';
		const dates = this.convertDayTimeFromTick_(val);
		const found = [false, false];
		if (dates[0].getDate() !== dates[1].getDate()) str += this.stringDay(dates[1].getDate());
		if (dates[0].getHours() !== dates[1].getHours()) found[0] = true;
		if (dates[0].getMinutes() !== dates[1].getMinutes()) found[1] = true;

		if (found[0] && found[1])
			str += `${this.convertTo2Digit_(dates[1].getHours())}:${this.convertTo2Digit_(dates[1].getMinutes())}`;
		else if (found[0] && !found[1])
			str += `${this.convertTo2Digit_(dates[1].getHours())}:00`;
		else if (!found[0] && found[1])
			str += `00:${this.convertTo2Digit_(dates[1].getMinutes())}`;
		return (str.length == 0) ? '-' : str;
	};
	convertDayTimeFromTick_(val) {
		let utc0 = Date.UTC(0, 0, 0, 0, 0, 0, 0);
		utc0 = (utc0 - ((Math.abs(new Date(utc0).getTimezoneOffset()) * 60) * 1000));
		let arr = [];
		arr.push(new Date(utc0));
		arr.push(new Date(utc0 + (val / 10000)));
		return arr;
	};
	convertTo2Digit_(val) {
		return (val > 9) ? `${val}` : `0${val}`;
	};
};

class AppUrl extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.eles = { tmpl: tmpln, ele: elen };
		this.data = {
			paths: []
		};
		this.template = {
			paths: []
		};
		this.states = { listM3u: 0, listVideo: 1, listAudio: 2, listHistory: 3, listTorrents: 4, None: 5 };
		this.state = this.states.listVideo;
	};
	static get Tag() {
		return 'urlState';
	};
	static get IsHttps() {
		return window.location.protocol.startsWith('https:');
	};
	get Protocol() {
		return (AppUrl.IsHttps) ? 'https://' : 'http://';
	};
	get Count() {
		return this.data.paths.length;
	};
	get Data() {
		if (this.Count === 0)
			return null;
		return this.dataEncodeUrl(this.data.paths.join('/'));
	};
	get Prefix() {
		if (this.Count === 0)
			return '';
		return this.data.paths[0];
	};
	get State() {
		return this.state;
	};
	set State(s) {
		let t = typeof (s);
		if ((t == typeof (this.states)) || (t === 'number'))
			this.state = s;
	};
	convertUrlToHost(url) {
		return this.convertUrlTo_(url, ['://', '/'], false);
	};
	convertUrlToName(url) {
		return this.convertUrlTo_(url, ['/','.'], true);
	};
	convertFileToUrl(file) {
		try {
			let url = decodeURI(file);
			let idx = url.indexOf('file:///');
			if (idx < 0)
				return;
			url = url.substring(idx + 8);

			for (let s of this.template.paths) {
				if (!url.startsWith(s.path))
					continue;
				idx = url.indexOf(s.path);
				if (idx < 0)
					continue;

				url = url.substring(idx + s.path.length);
				return this.dataEncodeUrl(`${s.tag}/${url}`, '');
			}

		} catch (a) { console.log(a); }
		return file;
	};
	convertUrlTo_(url, tags, b) {
		try {
			let unpack = decodeURI(url);
			let off = (b) ? unpack.lastIndexOf(tags[0]) : unpack.indexOf(tags[0]);
			if (off > 0) {
				off += tags[0].length;
				return unpack.substring(off, unpack.indexOf(tags[1], off));
			}
		} catch (a) { console.log(a); }
		return null;
	};
	dataRawUrl(s) {
		return this.dataBaseUrl() + s;
	};
	dataEncodeUrl(s, f) {
		return this.dataBaseUrl() + this.dataEncodePartUrl(s) + f;
	};
	dataEncodePartUrl(s) {
		return encodeURI(s)
			.replace(/['()]/g, escape)
			.replace(/\*/g, '%2A')
			.replace(/%(?:7C|60|5E)/g, unescape);
	};
	dataBaseUrl() {
		return this.Protocol + window.location.hostname + ':' + window.location.port + '/';
	};
	dataNextUrl(part, b) {
		var url = '';
		for (let a of this.data.paths)
			url += a + '/';
		return this.dataEncodeUrl(url + part, + (b) ? '/' : '');
	};
	dataFragmentUrl(tag) {
		var url = '';
		for (let a of this.data.paths) {
			url += a + '/';
			if (tag.localeCompare(a) === 0)
				break;
		};
		return this.dataEncodeUrl(url, '');
	};
	dataFragmentRawUrl(part) {
		var url = '';
		for (let a of this.data.paths) {
			url += a + '/';
		};
		if (typeof (part) !== 'undefined')
			url += part;
		return url;
	};
	dataGroupUrl(s) {
		this.state = s;
		switch (s) {
			case this.states.listM3u: return this.dataBaseUrl() + 'm3u/';
			case this.states.listVideo: return this.dataBaseUrl() + 'video/';
			case this.states.listAudio: return this.dataBaseUrl() + 'audio/';
			case this.states.listTorrents: return this.dataBaseUrl() + 'torrents/';
			case this.states.listHistory: return this.dataBaseUrl() + 'history/';
			default: return this.dataBaseUrl();
		};
	};
	pageTitle() {
		const title = AppLanguage.GetLanguage().title.msg0;
		switch (this.state) {
			case this.states.listM3u:   return title + AppLanguage.GetLanguage().title.msg1;
			case this.states.listVideo: return title + AppLanguage.GetLanguage().title.msg2;
			case this.states.listAudio: return title + AppLanguage.GetLanguage().title.msg3;
			case this.states.listTorrents: return title + AppLanguage.GetLanguage().title.msg4;
			case this.states.listHistory: return title + AppLanguage.GetLanguage().title.msg5;
			default: return title;
		};
	};
	defaultSet() {
		this.data = { paths: [] };
		this.refresh();
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data));
	};
	build(dataX) {
		try {
			this.data.paths = [];
			if (super.checkObject(dataX, 'url') && !super.stringEmpty(dataX.url)) {
				for (let s of dataX.url.split('/')) {
					if (super.stringEmpty(s))
						continue;
					this.data.paths.push(s);
				}
			}
			if (super.checkObject(dataX, 'paths') && $.isArray(dataX.paths))
				for (let x of dataX.paths) {
					let s = x.name.replace(/\\/g, '/');
					let a = { tag: x.tag, path: s };
					this.template.paths.push(a);
				}

			this.refresh();
		} catch (a) { console.log(a); }
	};
};

class AudioVoiceSpeak extends AppUtils {
	constructor(inst, arractrl, clz) {
		super();
		this.$ = inst;
		this.states = { Disabled: 0, Play: 1, Pause: 2, None: 3 };
		this.state = this.states.None;
		this.handle = window.speechSynthesis || null;
		this.clz = clz;
		this.actrl = [];
		this.voices = [];
		this.voice = -1;
		this.rate = 1.0;
		this.pitch = 1.0;
		this.isenable = (this.handle !== null);
		const speakBind = this.speak.bind(this);
		const stopBind = this.stop.bind(this);
		const this_ = this;
		for (let i of arractrl) {
			this.actrl.push(this.$(i));
		}
		if (this.isenable) {

			this.handle.onvoiceschanged = () => {
				try {
					this.voices = this.handle.getVoices();
					for (let i = 0; i < this.voices.length; i++) {
						const v = this.voices[i];
						let sdef = (v.default) ? '+' : '';
						this.actrl[0].append(
							this.buildOption_(i, `${v.name} (${sdef}${v.lang})`)
						);
					}
					if ((this.voices.length > 0) && (this.voice >= 0) && (this.voice < this.voices.length))
						this.actrl[0][0].selectedIndex = this.voice;

				} catch (a) { console.log(a); }
			};
		} else {
			this.actrl[0].append(
				this.buildOption_(-1, AppLanguage.GetLanguage().warnings.msg11)
			);
		}
		this.actrl[0].change(() => { this.voice = this.actrl[0].val(); });
		this.actrl[1].change(() => { this.rate  = this.actrl[1].val(); });
		this.actrl[2].change(() => { this.pitch = this.actrl[2].val(); });
		this.actrl[3].on('click', () => {
			try {
				if (this.handle.speaking)
					stopBind();
				else
					speakBind(AppLanguage.GetLanguage().warnings.msg12, () => { });

			} catch (a) { console.log(a); }
		});
	};
	get Enable() {
		return this.isenable;
	};
	get State() {
		return this.state;
	};
	get VoiceIndex() {
		return this.voice;
	};
	set VoiceIndex(i) {
		try {
			if ((typeof (i) === 'number') && (i >= 0)) {
				if (this.voices.length > 0) {
					if ((i >= 0) && (i < this.voices.length))
						this.voice = i;
				} else {
					this.voice = i;
				}
			}
		} catch (a) { console.log(a); }
	};
	buildOption_(id, txt) {
		return this.$('<option>', {
				value: id,
				text: txt
			});
	};
	speak(txt, cb) {
		if (!this.isenable)
			return this.states.Disabled;

		try {
			if (this.handle.speaking)
				this.handle.cancel();

			const ut = new SpeechSynthesisUtterance(txt);
			ut.onend = function () {
				try { cb(); } catch (a) { console.log(a); }
			};
			ut.onerror = function (event) {
				AppConnector.showError(`${event.error}`);
			};
			if ((this.voices.length > 0) && (this.voice >= 0))
				ut.voice = this.voices[this.voice];
			ut.rate = isNaN(this.rate) ? 1.0 : this.rate;
			ut.pitch = isNaN(this.pitch) ? 1.0 : this.pitch;
			this.handle.speak(ut);
			this.state = this.states.Play;
			return this.state;

		} catch (a) { console.log(a); this.state = this.states.None; }
		return this.states.None;
	};
	stop() {
		try {
			if (this.isenable) {
				if (this.handle.paused)
					this.handle.resume();
				if (this.handle.speaking)
					this.handle.cancel();
				this.state = this.states.None;
			}
		} catch (a) { console.log(a); }
	}
	pause() {
		try {
			if (this.isenable) {
				let idx = ((this.handle.speaking) * 1) + ((this.handle.paused) * 2);
				switch (idx) {
					case 1: {
						this.handle.pause();
						this.state = this.states.Pause;
						break;
					}
					case 2:
					case 3: {
						this.handle.resume();
						this.state = this.states.Play;
						break;
					}
					default: {
						this.state = this.states.None;
						break;
					}
				}
				return this.state;
			}
		} catch (a) { console.log(a); }
		return this.states.Disabled;
	};
	/* empty call API */
	defaultSet() {};
	build(dataX) {};
};


class AppPlayFileList extends AppUtils {
	constructor(ins, tmpldir, tmplfiles, elen, sctrl, appurl) {
		super();
		this.$ = ins;
		this.index = 0;
		this.isreload = false;
		this.eles = { tmpld: tmpldir, tmplf: tmplfiles, ele: elen, hidehisory: sctrl[2] };
		this.data = {
			dirs: [],
			files: [],
			url: ''
		};
		this.datas = {
			dirs: [],
			files: [],
			url: ''
		};
		this.appurl = appurl;
		const this_ = this;
		this.m3utag = ['#EXTM3U', '#EXTINF', '#EXTVLCOPT'];
		this.lastSort = [false, false, false, false, false, false];
		this.TemplateHelpers = {
			isfilevideo: function (val) {
				return this_.checkFileIsVideo(val);
			},
			isfileaudio: function (val) {
				return this_.checkFileIsAudio(val);
			},
			isfilenfo: function (val) {
				return this_.checkFileIsNfo(val);
			},
			isfilem3u: function (val) {
				return this_.checkFileIsM3u(val);
			},
			isfileimage: function (val) {
				return this_.checkFileIsImage(val);
			},
			isfilexspf: function (val) {
				return this_.checkFileIsXspf(val);
			},
			isstringnotempty: function (val) {
				return !this_.stringEmpty(val);
			},
			filsize: function (val) {
				return filesize(val, { locale: 'ru' });
			},
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			}
		};
		this.$(sctrl[0]).on('click', function () {
			this_.setDataDefault();
		}).on('keyup', function () {
			const txt = $(this).val();
			if (txt.length >= 3)
				this_.search(txt);
			else if (txt.length == 0)
				this_.setDataDefault();
		});
		this.$(sctrl[1]).on('click', function () {
			const txt = this_.$(sctrl[0]).val();
			if (!this_.stringEmpty(txt))
				this_.search(txt);
			else
				this_.setDataDefault();
		});
		$(document).on('click', sctrl[3], function () {
			this_.clearHistory();
		});
	};
	static get Tag() {
		return 'playFileList';
	};
	get DataDirs() {
		return this.data.dirs;
	};
	get DataFiles() {
		return this.data.files;
	};
	get DataUrl() {
		return this.data.url;
	};
	setDataDefault() {
		this.datas.dirs = [];
		this.datas.files = [];
		this.refresh();
	};
	getImages() {
		const arr = [];
		for (let i of this.data.files) {
			if (super.checkFileIsImage(i.uri))
				arr.push(i);
		}
		return arr;
	};
	getTrackById(id) {
		id -= 1;
		if ((id >= this.data.files.length) || (id < 0))
			return ['', ''];
		this.index = id;
		let item = this.data.files[this.index++];
		return [item.uri, item.name];
	};
	getPrevTrack() {
		if (0 > this.index)
			this.index = (this.data.files.length - 1);
		let item = this.data.files[this.index--];
		return [item.uri, item.name];
	};
	getNextTrack() {
		if (this.data.files.length <= this.index)
			this.index = 0;
		let item = this.data.files[this.index++];
		return [item.uri, item.name];
	};
	sortOnce(num) {
		if (this.lastSort[num]) return false;
		for (var i = 0; i < this.lastSort.length; i++)
			if (i === num)
				this.lastSort[i] = true;
			else
				this.lastSort[i] = false;
		return true;
	};
	sort1Up() {
		if (!this.sortOnce(0))
			return;
		this.data.files = this.data.files.sort((a, b) => a.uri.localeCompare(b.uri));
		this.refresh();
	};
	sort1Down() {
		if (!this.sortOnce(1))
			return;
		this.data.files = this.data.files.sort((a, b) => b.uri.localeCompare(a.uri));
		this.refresh();
	};
	sort2Up() {
		if (!this.sortOnce(2))
			return;
		this.data.files = this.data.files.sort((a, b) => a.time !== b.time ? a.time < b.time ? -1 : 1 : 0);
		this.refresh();
	};
	sort2Down() {
		if (!this.sortOnce(3))
			return;
		this.data.files = this.data.files.sort((a, b) => b.time !== a.time ? b.time < a.time ? -1 : 1 : 0);
		this.refresh();
	};
	sort3Up() {
		if (!this.sortOnce(4))
			return;
		this.data.files = this.data.files.sort((a, b) => { let ai = parseInt(a.size), bi = parseInt(b.size); return ai !== bi ? ai < bi ? -1 : 1 : 0; });
		this.refresh();
	};
	sort3Down() {
		if (!this.sortOnce(5))
			return;
		this.data.files = this.data.files.sort((a, b) => { let ai = parseInt(a.size), bi = parseInt(b.size); return bi !== ai ? bi < ai ? -1 : 1 : 0; });
		this.refresh();
	};
	checkM3u(url) {
		return super.checkFileIsM3u(url);
	};
	checkXspf(url) {
		return super.checkFileIsXspf(url);
	};
	defaultSet() {
		this.data = { dirs: [], files: [], url: '' };
		this.refresh();
	};
	refresh() {
		var inst = this.$(this.eles.ele);
		var tmpld = this.$.templates(this.eles.tmpld);
		var tmplf = this.$.templates(this.eles.tmplf);
		const b = ((this.datas.dirs.length > 0) || (this.datas.files.length > 0));

		inst.html(tmpld.render(b ? this.datas : this.data, this.TemplateHelpers));
		inst.append(tmplf.render(b ? this.datas : this.data, this.TemplateHelpers));

		$(this.eles.hidehisory).hide();
	};
	search(tag) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (x) {

				const d = {
					dirs: [],
					files: []
				};
				const txt = tag.toLowerCase();
				for (let t of this_.data.dirs.filter(pr => pr.name.searchLowerCase(txt)))
					d.dirs.push(t);

				for (let t of this_.data.files.filter(pr => pr.name.searchLowerCase(txt)))
					d.files.push(t);

				this_.datas.dirs = d.dirs;
				this_.datas.files = d.files;
				this_.datas.dirscount = d.dirs.length;
				this_.datas.filescount = d.files.length;
				this_.datas.url = this_.data.url;
				this_.refresh();
			});
			atc.build([]);

		} catch (a) { console.log(a); }
	};
	findImageByUrl(url) {
		try {
			const d = {
				files: []
			};
			let txt = decodeURI(url);
			txt = txt.substring(txt.lastIndexOf('/') + 1);
			txt = txt.substring(0, txt.indexOf('.'));

			for (let t of this.data.files.filter(pr => pr.uri.includes(txt)))
				if (!super.stringEmpty(t.uri) && super.checkFileIsImage(t.uri))
					d.files.push(t);

			let f;
			if (d.files.length === 0)
				return null;
			else if (d.files.length === 1)
				f = d.files[0].uri;
			else
				f = d.files[Math.floor(Math.random() * d.files.length)].uri;
			return this.appurl.dataNextUrl(f.trim(), false);

		} catch (a) { console.log(a); return null; }
	};
	getEmptyItem_() {
		return {
			id: 0,
			size: 0,
			name: '',
			uri: '',
			time: '',
			ip: '',
			imgurl: '',
			isdfmt: false,
			issfmt: false,
			isip: false,
			ishistory: false
		};
	};
	setBool_(arr) {
		for (let ele of arr) {
			if (super.checkObject(ele, 'isdfmt'))
				ele.isdfmt = super.getBool(ele.isdfmt);
			else
				ele.isdfmt = false;
			if (super.checkObject(ele, 'issfmt'))
				ele.issfmt = super.getBool(ele.issfmt);
			else
				ele.issfmt = false;
			if (super.checkObject(ele, 'isip'))
				ele.isip = super.getBool(ele.isip);
			else
				ele.isip = false;
			if (super.checkObject(ele, 'ishistory'))
				ele.ishistory = super.getBool(ele.ishistory);
			else
				ele.ishistory = false;
		}
	};
	getHistory_() {
		try {
			const Store = window.localStorage;
			let json = Store.getItem(AppPlayFileList.Tag);
			if (json === null) {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);
				return null;
			}
			let arr = JSON.parse(json);
			if (!$.isArray(arr) || (arr.length === 0)) {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);
				return null;
			}
			return arr;

		} catch (a) { console.log(a); }
		return null;
	};
	clearHistory() {
		try {
			const Store = window.localStorage;
			Store.setItem(AppPlayFileList.Tag, JSON.stringify([]));
			this.data.dirs = [];
			this.data.files = [];
			this.datas.dirscount = 1;
			this.datas.filescount = 0;
			this.isreload = true;
			this.refresh();

		} catch (a) { console.log(a); }
	};
	addHistory(id) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (x) {

				let item = null;
				const Store = window.localStorage;
				for (let t of this_.data.files.filter(pr => pr.id == id)) {
					item = t;
					break;
				}
				if (item == null)
					return;
				if (this_.checkObject(item, 'ishistory') && item.ishistory)
					return;
				if (!this_.checkObject(item, 'uri') ||
					(!this_.checkFileIsVideo(item.uri) && !this_.checkFileIsAudio(item.uri)))
					return;

				let arr, idx = 0;
				let json = Store.getItem(AppPlayFileList.Tag);
				if (json !== null) {
					arr = JSON.parse(json);
					if (!$.isArray(arr)) {
						arr = [];
					} else {
						if (arr.length > 99) {
							idx = arr[0].id;
							arr.shift();
						} else {
							idx = arr.length;
						}
					}
				}
				else {
					arr = [];
				}

				let a = this_.getEmptyItem_();
				a.id = idx;
				a.ishistory = true;
				a.isdfmt = true;
				a.issfmt = item.issfmt;
				a.name = item.name;
				a.size = item.size;
				a.time = new Date().toISOString();
				a.uri = item.uri.includes('://') ? item.uri : this_.appurl.dataNextUrl(item.uri, false);
				arr.push(a);
				Store.setItem(AppPlayFileList.Tag, JSON.stringify(arr));
			});
			atc.build([]);

		} catch (a) { console.log(a); }
	};
	filterHistory(dt) {
		try {
			if (!super.checkObject(dt, 'date')) {
				this.buildHistory();
				return;
			}
			const arr = this.getHistory_();
			if ((arr === null) || !$.isArray(arr) || (arr.length === 0))
				return;

			let d = [];
			let h = (dt.date.getTimezoneOffset() < 0) ?
						(Math.abs(dt.date.getTimezoneOffset()) / 60) : (-Math.abs(dt.date.getTimezoneOffset()) / 60);
			let date = new Date(
				dt.date.getFullYear(),
				dt.date.getMonth(),
				dt.date.getDate(),
				h, 1).toISOString().split('T')[0];
			for (let t of arr.filter(pr => pr.time.startsWith(date)))
				d.push(t);

			this.data.dirs = [];
			this.data.files = d;
			this.data.dirscount = 1;
			this.data.filescount = d.length;
			this.refresh();

			if (d.length === 0)
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);

		} catch (a) { console.log(a); }
	};
	buildHistory() {
		try {
			const arr = this.getHistory_();
			if ((arr === null) || !$.isArray(arr) || (arr.length === 0))
				return;

			this.data.dirs = [];
			this.data.files = arr.reverse();
			this.datas.dirscount = 1;
			this.datas.filescount = arr.length;
			this.isreload = true;
			this.refresh();

		} catch (a) { console.log(a); }
	};
	updateHistory(url, duration) {
		try {
			const json = Store.getItem(AppPlayFileList.Tag);
			if (json === null)
				return;

			const arr = JSON.parse(json);
			if (!$.isArray(arr) || (arr.length === 0))
				return;

			for (let i of arr.reverse()) {
				if (url.localeCompare(i.uri) === 0) {
					i.duration = duration;
					Store.setItem(AppPlayFileList.Tag, JSON.stringify(arr));
					return;
				}
			}
		} catch (a) { console.log(a); }
	};
	buildxspfItem_(id, obj, titles, this_) {

		if (!this_.checkObject(obj, 'location'))
			return null;

		let ids = id + 1;
		let title = (titles === null) ? '' : titles;
		let album = this_.checkObject(obj, 'album') ? obj.album : `#${ids}`;
		let duration = this_.checkObject(obj, 'duration') ? Math.round((parseInt(obj.duration) / 1000) / 60) : 0;

		if (!this_.checkObject(obj, 'title')) {
			let name = this_.appurl.convertUrlToName(obj.location);
			if (name !== null)
				title += `${ids} - ${name}`;
			else
				title += (this_.checkObject(obj, 'creator') ? `${ids} - ${obj.creator}` : `# ${ids}`);
		} else {
			title += obj.title;
		}

		let a = this_.getEmptyItem_();
		a.id = id;
		a.name = title;
		a.time = album;
		a.size = `${duration} ${AppLanguage.GetLanguage().nfo.minutes}`;

		let url = obj.location.trim();
		if (url.startsWith('file://'))
			a.uri = this_.appurl.convertFileToUrl(url);
		else
			a.uri = url;
		return a;
	};
	buildxspf(url) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (dataX) {

				if (!this_.checkObject(dataX, 'playlist'))
					return;
				if (!this_.checkObject(dataX.playlist, 'trackList') ||
					!this_.checkObject(dataX.playlist.trackList, 'track') ||
					!$.isArray(dataX.playlist.trackList.track) ||
					(dataX.playlist.trackList.track.length == 0))
					return;
				if (!this_.checkObject(dataX.playlist, 'extension'))
					return;

				const d = [];

				if (this_.checkObject(dataX.playlist.extension, 'items') &&
					$.isArray(dataX.playlist.extension.items) && (dataX.playlist.extension.items.length > 0)) {
					for (let i of dataX.playlist.extension.items) {
						try {
							let id = parseInt(i._tid);
							id = (id < 0) ? 0 : id;
							if (id >= dataX.playlist.trackList.track.length)
								continue;
							const x = dataX.playlist.trackList.track[id];
							if (x == null)
								continue;
							let a = this_.buildxspfItem_(id, x, null, this_);
							if (a != null)
								d.push(a);
						} catch (a) { console.log(a); }
					}
				}
				if (this_.checkObject(dataX.playlist.extension, 'node')) {

					let arr;
					if (this_.checkObject(dataX.playlist.extension.node, 'node') &&
						$.isArray(dataX.playlist.extension.node.node) && (dataX.playlist.extension.node.node.length > 0))
						arr = dataX.playlist.extension.node.node;
					else if ($.isArray(dataX.playlist.extension.node) && (dataX.playlist.extension.node.length > 0))
						arr = dataX.playlist.extension.node;
					else
						arr = [];

					for (let n of arr) {
						let title = (!this_.checkObject(n, '_title') || this_.stringEmpty(n._title)) ? '' : n._title + '/';

						if (this_.checkObject(n, 'item') && $.isArray(n.item) && (n.item.length > 0))
							for (let i of n.item) {
								try {
									let id = parseInt(i._tid);
									id = (id < 0) ? 0 : id;
									if (id >= dataX.playlist.trackList.track.length)
										continue;
									const x = dataX.playlist.trackList.track[id];
									if (x == null)
										continue;
									let a = this_.buildxspfItem_(id, x, title, this_);
									if (a != null)
										d.push(a);
								} catch (a) { console.log(a); }
							}
					}
				}
				if (d.length === 0) {
					for (let i = 0; i < dataX.playlist.trackList.track.length; i++) {
						try {
							const x = dataX.playlist.trackList.track[i];
							if (x == null)
								continue;
							let a = this_.buildxspfItem_(i, x, null, this_);
							if (a != null)
								d.push(a);
						} catch (a) { console.log(a); }
					}
				}
				this_.data.dirs = [];
				this_.data.files = d;
				this_.data.dirscount = 1;
				this_.data.filescount = d.length;
				this_.isreload = true;
				this_.refresh();
			});
			AppConnector.getRequest(url, [atc]);

		} catch (a) { console.log(a); }
	};
	buildm3u(url) {
		try {

			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (arr) {

				const d = [];

				if (arr[0].includes(this_.m3utag[0])) {
					arr.shift();
				}
				for (let i = 0; i < arr.length; i++) {
					if (arr[i].startsWith(this_.m3utag[2]))
						continue;

					let result = arr[i].match(/:(?:(-?\d+))(?:\s(.+)|)(?:\s|),(.+)(?:\r\n|\n)(\S+:\/\/.+)(?:\r\n|\n)/m);
					let a = this_.getEmptyItem_();
					var url = '';
					var group = '';

					a.id = i;
					a.time = '';
					a.name = '';
					a.issfmt = true;

					if (result && result.length == 5) {
						url = result[4].trim() || '';
						a.size = parseInt(result[1]) || 0;
						a.name = result[3].trim() || '';

						try {
							const bregexp = /(?<tag>[a-z\-]+)\=\"(?<val>[^\"]+)\"/gs;
							let grp = bregexp.exec(result[2]);
							do {
								if (grp.groups.tag === 'group-title') {
									group = grp.groups.val || '';
								}
								if (grp.groups.tag === 'tvg-logo') {
									a.imgurl = grp.groups.val || '';
								}
							} while ((grp = bregexp.exec(result[2])) !== null);
						} catch (a) { console.log(a); }
					}
					else if (result && result.length == 4) {
						url = result[3].trim() || '';
						a.size = parseInt(result[1]) || 0;
						a.name = result[2].trim() || '';
					}
					else if (result && result.length == 3) {
						url = result[2].trim() || '';
						a.size = parseInt(result[1]) || 0;
					}
					else
						continue;

					if (this_.stringEmpty(url))
						continue;

					if (url.startsWith('file://'))
						a.uri = this_.appurl.convertFileToUrl(url);
					else
						a.uri = url;

					a.size = (a.size < 0) ? 0 : a.size;

					if (this_.stringEmpty(a.name)) {
						if (!this_.stringEmpty(group)) {
							a.name = group;
						} else {
							let name = this_.appurl.convertUrlToName(a.uri);
							if (name !== null)
								a.name = name;
							else
								a.name = '-?-';
						}
					} else {
						if (!this_.stringEmpty(group)) {
							a.time = group;
						} else {
							a.time = this_.appurl.convertUrlToHost(a.uri) || '';
						}
					}
					d.push(a);
				}
				this_.data.dirs = [];
				this_.data.files = d;
				this_.data.dirscount = 1;
				this_.data.filescount = d.length;
				this_.isreload = true;
				this_.refresh();
			});
			const xhr = $.get(url, function (rawdata) {
				if (rawdata == null)
					return;
				const arr = rawdata.split(this_.m3utag[1]);
				if (arr.length == 0)
					return;

				atc.build(arr);
			})
			.fail(function () {
				AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2} = ${this.status}: ${this.statusText}`);
			});

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (super.checkObject(dataX, 'dirs')) {
				if ((dataX.dirs.items != null) && $.isArray(dataX.dirs.items)) {
					dataX.dirs = dataX.dirs.items;
					for (let i of dataX.dirs) {
						if (!super.checkObject(i, 'imgurl'))
							i.imgurl = '';
					}
				} else {
					const items = dataX.dirs.items;
					dataX.dirs = [];
					if (super.checkObject(items, 'uri'))
						if (items.uri != null)
							dataX.dirs.push(items);
				}
			}
			if (super.checkObject(dataX, 'files')) {
				if ((dataX.files.items != null) && $.isArray(dataX.files.items)) {
					dataX.files = dataX.files.items;
					for (let i of dataX.files) {
						if (!super.checkObject(i, 'imgurl'))
							i.imgurl = '';
					}
				} else {
					const items = dataX.files.items;
					dataX.files = [];
					if (super.checkObject(items, 'uri'))
						if (items.uri != null)
							dataX.files.push(items);
				}
			}
			this.data = dataX;
			this.isreload = false;
			this.setBool_(this.data.files);
			this.$('#dirscount').text(super.stringDirectory(dataX.dirscount));
			this.$('#filescount').text(super.stringFiles(dataX.filescount));
			this.refresh();
		} catch (a) { console.log(a); }
	};
};


class AppPlaySelector extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.states = { playCurrentWindow: 0, playNewWindows: 1, playEmbed: 2, sendTo: 3, playRemote: 4 };
		this.state = this.states.playEmbed;
		this.eles = { tmpl: tmpln, ele: elen };
		this.data = {
			items: []
		};
		this.dataT = {
			items: [
				{ name: AppLanguage.GetLanguage().selector.opt0, id: 0, lineb: false },
				{ name: AppLanguage.GetLanguage().selector.opt1, id: 1, lineb: false },
				{ name: AppLanguage.GetLanguage().selector.opt2, id: 2, lineb: !AppUrl.IsHttps }
			]
		};
		const this_ = this;
		if (AppUrl.IsHttps)
			this.dataT.items.push({ name: AppLanguage.GetLanguage().selector.opt3, id: 3, lineb: AppUrl.IsHttps });
		this.dataT.items.push({ name: AppLanguage.GetLanguage().selector.opt4, id: 50, lineb: false });
		this.$(elen).on('click', '.dropdown-item', function () {
			try {
				const n = parseInt($(this).data('id'));
				switch (n) {
					case 0:
					case 1:
					case 2:
					case 3: {
						this_.state = n;
						break;
					}
					case 50: {
						AppConnector.showError(this_.dataT.items[4].name);
						break;
					}
				}
			} catch (a) { console.log(a); }
		});
	};
	static get Tag() {
		return 'selectorState';
	};
	get State() {
		return this.state;
	};
	set State(s) {
		let t = typeof (s);
		if ((t == typeof (this.states)) || (t === 'number')) {
			if (!AppUrl.IsHttps && (s === this.states.sendTo))
				this.state = this.states.playEmbed;
			this.state = s;
		}
	};
	defaultSet() {
		this.data = this.dataT;
		this.refresh();
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data));
	};
	build(dataX) {
		try {
			if (!super.checkObject(dataX, 'items')) {
				this.refresh();
				return;
			}
			var datanew = { items: [] };
			datanew.items = this.dataT.items.slice(0, (AppUrl.IsHttps) ? 3 : 2);
			dataX.items.forEach(function (item) {
				datanew.items.push(item);
			});
			this.data = datanew;
			this.refresh();
		} catch (a) { console.log(a); }
	};
};


class AppServiceControl extends AppUtils {
	constructor(ins, bctrl, clz) {
		super();
		this.$ = ins;
		this.clz = clz;
		const sendCmdBind = this.sendcmd.bind(this);
		const sendSysBind = this.sendsys.bind(this);
		const uploadBind  = this.upload_.bind(this);
		this.$(bctrl[0]).on('click', () => { sendCmdBind(130); });
		this.$(bctrl[1]).on('click', () => { sendCmdBind(131); });
		this.$(bctrl[2]).on('click', () => { sendCmdBind(132); });
		this.$(bctrl[3]).on('click', () => { sendCmdBind(134); }); // 133 Audio scan
		this.$(bctrl[4]).on('click', () => { sendCmdBind(135); });
		this.$(bctrl[5]).on('click', () => { sendSysBind('cmd/login'); });
		this.$(bctrl[6]).on('click', () => { sendSysBind('cmd/unlogin'); });
		this.$(bctrl[7]).on('submit', (ev) => {
			ev.preventDefault();
			try {
				try {
					let cat = this.$(`${bctrl[7]} #path`);
					cat.val(cat.val().trim());
				} catch {}
				const form = this.$(bctrl[7])[0];
				const arr = form.upfile.files;
				if ((arr !== null) && (arr.length > 0))
					uploadBind(form);
				else
					form.reset();
			} catch (a) { AppConnector.showError(`Upload: ${a}`); }
			return false;
		});
	};
	sendcmd(command) {
		try {
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType("text/xml");
			xhr.responseType = 'document';
			xhr.withCredentials = false;
			xhr.open('GET', this.clz[1].dataBaseUrl() + 'cmd/command/' + command, true);
			xhr.onload = () => {
				if (xhr.status != 200)
					AppConnector.RequestOnError_(
						this, null,
						AppLanguage.GetLanguage().warnings.msg3
					);
			};
			xhr.onerror = () => {
				AppConnector.RequestOnError_(
					this, null,
					AppLanguage.GetLanguage().warnings.msg2
				);
			};
			xhr.onloadend = () => { };
			xhr.send();

		} catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`); }
	};
	sendsys(url) {
		try {
			$.get(this.clz[1].dataBaseUrl() + url, (rawdata) => {
				if (rawdata == null)
					return;
				AppConnector.showError(rawdata);
			})
			.fail(() => {
				AppConnector.RequestOnError_(
					this,
					AppLanguage.GetLanguage().warnings.msg8,
					AppLanguage.GetLanguage().warnings.msg2
				);
			});
		} catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`); }
	};
	upload_(form) {
		const jq_ = this.$;
		try {
			let isStart = false,
				isEnd = false,
				isProgress = false;
			const data = new FormData(form);
			const xhr = new XMLHttpRequest();
			xhr.responseType = 'text';
			xhr.withCredentials = false;
			xhr.open('POST', '/cmd/upload', true);

			const onprogress_ = (ev) => {
				if (isProgress || !ev.hasOwnProperty('loaded') || !ev.hasOwnProperty('total'))
				isProgress = true;
				const val = Math.floor(ev.loaded / ev.total * 100);
				const vals = `${val}%`;
				jq_('#progressUploadBar').css('width', vals).attr('aria-valuenow', val.toString()).html(vals);
				isProgress = false;
			};
			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200))
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
				if ((ev.response !== null) && (typeof (ev.response) === 'string') && !ev.response.isEmpty())
					AppConnector.showError(ev.response);
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
			};
			const onloadstart_ = (ev) => {
				if (isStart) return;
				isStart = true;
				jq_('#progressUploadBar').css('width', '0%').attr('aria-valuenow', '0').html('0%');
				jq_('#progressUploadContainer').fadeIn();
			};
			const onloadend_ = (ev) => {
				if (isEnd) return;
				isEnd = true;
				jq_('#progressUploadContainer').fadeOut();
				form.reset();
			};

			xhr.onprogress = (ev) => { onprogress_(ev); };
			xhr.onload = (ev) => { onload_(xhr); };
			xhr.onerror = (ev) => { onerror_(xhr); };
			xhr.onloadstart = (ev) => { onloadstart_(ev); };
			xhr.onloadend = (ev) => { onloadend_(ev); }

			xhr.upload.onprogress = (ev) => { onprogress_(ev); };
			xhr.upload.onload = (ev) => { onload_(xhr); };
			xhr.upload.onerror = (ev) => { onerror_(xhr); };
			xhr.upload.onloadstart = (ev) => { onloadstart_(ev); };
			xhr.upload.onloadend = (ev) => { onloadend_(ev); }

			xhr.send(data);

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			jq_('#progressUploadContainer').fadeOut();
		}
	};
};



class ImageView extends AppUtils {
	constructor(ins, itmpl, ictrl, clz) {
		super();
		this.$ = ins;
		this.index = 0;
		this.activeid = 0;
		this.clz = clz;
		this.ele = { tmpls: itmpl, eles: [] };
		this.data = { files: [] };
		const this_ = this;
		this.TemplateHelpers = {
			getid: function (val) {
				return this_.activeid == parseInt(val);
			},
			getcount: function () {
				return this_.index++;
			},
			geturl: function (val) {
				return this_.clz[1].dataNextUrl(val, false);
			}
		};
		for (let i of ictrl) {
			this.ele.eles.push(this.$(i));
		}
		this.ele.eles[3].on('click', function () {
			this_.defaultSet();
		});
	};
	check(val) {
		return super.checkFileIsImage(val);
	};
	defaultSet() {
		try {
			this.ele.eles[2].fadeOut();
			this.data = { files: [] };
			this.index = 0;
			this.activeid = 0;
		} catch (a) { console.log(a); }
	};
	refresh() {
		var tmpla = this.$.templates(this.ele.tmpls[0]);
		var tmplb = this.$.templates(this.ele.tmpls[1]);
		this.ele.eles[0].html(tmpla.render(this.data, this.TemplateHelpers));
		this.ele.eles[1].html(tmplb.render(this.data, this.TemplateHelpers));
		this.ele.eles[2].fadeIn();
	};
	build(ids) {
		try {
			this.index = 0;
			this.activeid = parseInt(ids);
			this.data.files = this.clz[0].getImages();
			if (this.data.files.length > 0)
				this.refresh();
		} catch (a) { console.log(a); }
	};
};

class NfoView extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.eles = { tmpl: tmpln, ele: elen[0], root: elen[1], closed: elen[2] };
		this.states = { typeNone: 0, typeShow: 1, typeEpisode: 2, typeMovie: 3 };
		this.state;
		this.data;
		this.defaultSet();
		const this_ = this;
		this.TemplateHelpers = {
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMD();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			},
			isint: function(i) {
				return (i !== null) && i > 0;
			},
			isint2: function(i,n) {
				return ((i !== null) && i > 0) && ((n !== null) && n > 0);
			},
			isnotlast: function(i, n) {
				return i < (n - 1);
			},
			isstr: function(str) {
				return !((str === null) || (typeof (str) != 'string') || (str.length === 0));
			},
			isarray: function(arr) {
				return !((arr === null) || !($.isArray(arr)) || (arr.length === 0));
			},
			isobject: function(obj, field) {
				return (obj != null) && obj.hasOwnProperty(field);
			},
			upercase: function(str) {
				return str.toUpperCase();
			},
			dburl: function(name, id) {
				if (this_.state === this_.states.typeNone)
					return '#';

				switch (name) {
					case 'imdb': {
						return `https://www.imdb.com/title/${id}/`;
					}
					case 'tmdb': {
						if (this_.state === this_.states.typeMovie)
							return `https://www.themoviedb.org/movie/${id}/`;
						else
							return `https://www.themoviedb.org/tv/${id}/`;
					}
					case 'trakt': {
						if (this_.state === this_.states.typeMovie)
							return `https://trakt.tv/movies/${id}/`;
						else
							return `https://trakt.tv/shows/${id}/`;
					}
					case 'tvdb': {
						return `https://thetvdb.com/search?query=${id}`;
					}
					case 'tvrage': {
						return `https://www.tvrage.com/all/`;
					}
					default: return '#';
				}
			}
		};
		$(document).on('click', this.eles.closed, function () {
			this_.close();
		});
	};
	defaultSet() {
		this.data = {
			actors: [],
			credits: [],
			directors: [],
			studios: [],
			genre: [],
			tags: [],
			imgs: [],
			baseid: [],
			audio: [],
			video: [],
			media: {
				titles: [],
				desc: '',
				premiered: '',
				limitation: '',
				file: '',
				runtime: 0
			},
			ratings: {
				rating: 0.0,
				userrating: 0.0,
				votes: 0
			},
			series: {
				season: 0,
				episode: 0
			}
		};
		this.state = this.states.typeNone;
	};
	get Data() {
		return this.data;
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data, this.TemplateHelpers));
		try {
			this.eles.ele.find('[data-localize]').localize();
		} catch (a) {
			console.log(a);
		}
		this.show();
	};
	show() {
		this.$(this.eles.root).fadeIn();
	};
	hide() {
		this.$(this.eles.root).fadeOut();
	};
	close() {
		this.hide();
		this.defaultSet();
	};
	check(val) {
		return super.checkFileIsNfo(val);
	};
	parseNormalize() {

		if (this.data.media.titles.length > 0) {
			this.data.media.titles = Array.from(new Set(this.data.media.titles));
		}
		if (this.data.imgs.length > 0) {
			this.data.imgs = Array.from(new Set(this.data.imgs));
		}
		if (this.data.video.length > 0) {
			for (let v of this.data.video) {
				if (!super.checkObject(v, 'codec'))
					v.codec = AppLanguage.GetLanguage().nfo.none;
				if (!super.checkObject(v, 'width'))
					v.width = '0';
				if (!super.checkObject(v, 'height'))
					v.height = '0';
				if (!super.checkObject(v, 'aspect'))
					v.aspect = '0.0';
				if (!super.checkObject(v, 'durationinseconds'))
					v.durationinseconds = '0';
			}
		}
		if (this.data.audio.length > 0) {
			for (let v of this.data.audio) {
				if (!super.checkObject(v, 'codec'))
					v.codec = AppLanguage.GetLanguage().nfo.none;
				if (!super.checkObject(v, 'channels'))
					v.channels = '0';
			}
		}
	}
	parseShow(dataX) {
		try {
			this.state = this.states.typeShow;
			this.parseBase(dataX);

			if (super.checkObject(dataX, 'showtitle'))
				if (!super.stringEmpty(dataX.showtitle))
					this.data.media.titles.push(dataX.showtitle);

			if (super.checkObject(dataX, 'year'))
				if (!super.stringEmpty(dataX.year))
					this.data.media.premiered = dataX.year;

			if (super.checkObject(dataX, 'tag')) {
				if ($.isArray(dataX.tag))
					this.data.tags = dataX.tag;
				else if (!super.stringEmpty(dataX.tag))
					this.data.tags.push(dataX.tag);
			}

			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	}
	parseEpisode(dataX) {
		try {
			this.state = this.states.typeEpisode;
			this.parseBase(dataX);

			if (super.checkObject(dataX, 'season'))
				if (!super.stringEmpty(dataX.season))
					this.data.series.season = parseInt(dataX.season);

			if (super.checkObject(dataX, 'episode'))
				if (!super.stringEmpty(dataX.episode))
					this.data.series.episode = parseInt(dataX.episode);

			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	};
	parseMovie(dataX) {
		try {
			this.state = this.states.typeMovie;
			this.parseBase(dataX);
			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	};
	parseBase(dataX) {

		if (super.checkObject(dataX, 'actor')) {
			if ($.isArray(dataX.actor))
				this.data.actors = dataX.actor;
			else if (dataX.actor != null)
				this.data.actors.push(dataX.actor);
		}

		if (super.checkObject(dataX, 'credits')) {
			if ($.isArray(dataX.credits))
				this.data.credits = dataX.credits;
			else if (!super.stringEmpty(dataX.credits))
				this.data.credits.push(dataX.credits);
		}

		if (super.checkObject(dataX, 'director')) {
			if ($.isArray(dataX.director))
				this.data.directors = dataX.director;
			else if (!super.stringEmpty(dataX.director))
				this.data.directors.push(dataX.director);
		}

		if (super.checkObject(dataX, 'genre')) {
			if ($.isArray(dataX.genre))
				this.data.genre = dataX.genre;
			else if (!super.stringEmpty(dataX.genre))
				this.data.genre.push(dataX.genre);
		}

		if (super.checkObject(dataX, 'fileinfo') && super.checkObject(dataX.fileinfo, 'streamdetails')) {
			if (super.checkObject(dataX.fileinfo.streamdetails, 'video')) {
				if ($.isArray(dataX.fileinfo.streamdetails.video))
					this.data.video = dataX.fileinfo.streamdetails.video;
				else
					this.data.video.push(dataX.fileinfo.streamdetails.video);
			}
			if (super.checkObject(dataX.fileinfo.streamdetails, 'audio')) {
				if ($.isArray(dataX.fileinfo.streamdetails.audio))
					this.data.audio = dataX.fileinfo.streamdetails.audio;
				else
					this.data.audio.push(dataX.fileinfo.streamdetails.audio);
			}
		}

		if (super.checkObject(dataX, 'studio')) {
			if ($.isArray(dataX.studio))
				this.data.studios = dataX.studio;
			else if (!super.stringEmpty(dataX.studio))
				this.data.studios.push(dataX.studio);
		}

		if (super.checkObject(dataX, 'uniqueid')) {
			if ($.isArray(dataX.uniqueid)) {
				for (let s of dataX.uniqueid)
					this.data.baseid.push({ name: s._type, id: s.toString() });
			}
			else if (!super.stringEmpty(dataX.uniqueid.toString()))
				this.data.baseid.push({ name: dataX.uniqueid._type, id: dataX.uniqueid.toString() });
		}

		if (super.checkObject(dataX, 'fanart') && super.checkObject(dataX.fanart, 'thumb'))
			if (!super.stringEmpty(dataX.fanart.thumb))
				this.data.imgs.push({ url: dataX.fanart.thumb, isart: dataX.fanart.thumb.includes('artworks.') });

		if (super.checkObject(dataX, 'thumb')) {
			if ($.isArray(dataX.thumb)) {
				for (let s of dataX.thumb)
					this.data.imgs.push({ url: s.__text, isart: s.__text.includes('artworks.') });
			} else {
				if (!super.stringEmpty(dataX.thumb.__text))
					this.data.imgs.push({ url: dataX.thumb.__text, isart: dataX.thumb.__text.includes('artworks.') });
			}
		}

		if (super.checkObject(dataX, 'title'))
			if (!super.stringEmpty(dataX.title))
				this.data.media.titles.push(dataX.title);

		if (super.checkObject(dataX, 'originaltitle'))
			if (!super.stringEmpty(dataX.originaltitle))
				this.data.media.titles.push(dataX.originaltitle);

		if (super.checkObject(dataX, 'original_filename'))
			if (!super.stringEmpty(dataX.original_filename))
				this.data.media.file = dataX.original_filename;

		if (super.checkObject(dataX, 'plot'))
			if (!super.stringEmpty(dataX.plot))
				this.data.media.desc = dataX.plot;

		if (super.checkObject(dataX, 'premiered'))
			if (!super.stringEmpty(dataX.premiered))
				this.data.media.premiered = dataX.premiered;

		if (super.checkObject(dataX, 'runtime'))
			if (!super.stringEmpty(dataX.runtime))
				this.data.media.runtime = parseInt(dataX.runtime);

		if (super.checkObject(dataX, 'rating'))
			if (!super.stringEmpty(dataX.rating))
				this.data.ratings.rating = parseFloat(dataX.rating);

		if (super.checkObject(dataX, 'userrating'))
			if (!super.stringEmpty(dataX.userrating))
				this.data.ratings.userrating = parseFloat(dataX.userrating);

		if (super.checkObject(dataX, 'votes'))
			if (!super.stringEmpty(dataX.votes))
				this.data.ratings.votes = parseInt(dataX.votes);

		if (super.checkObject(dataX, 'mpaa')) {
			if (!super.stringEmpty(dataX.mpaa))
				this.data.media.limitation = dataX.mpaa;
		} else {
			if (!super.stringEmpty(dataX.certification))
				this.data.media.limitation = dataX.certification;
		}

	};
	build(dataX) {
		try {

			this.defaultSet();

			if (super.checkObject(dataX, 'movie'))
				this.parseMovie(dataX.movie);
			else if (super.checkObject(dataX, 'episodedetails'))
				this.parseEpisode(dataX.episodedetails);
			else if (super.checkObject(dataX, 'tvshow'))
				this.parseShow(dataX.tvshow);

		} catch (a) { console.log(a); }
	};
};


class AudioPlayer extends AppUtils {
	constructor(inst, player, arractrl, arrpctrl, clz) {
		super();
		this.$ = inst;
		this.playerjq = this.$(player);
		this.clz = clz;
		this.eles = { tmpld: arrpctrl[8], tmplw: arrpctrl[9], ele: null };
		this.actrl = [];
		this.pctrl = [];
		this.playurl = null;
		this.playname = '';
		this.isplay = false;
		this.isspeech = true;
		this.isinfobox = false;
		const playNextBind = this.playNextTrack.bind(this);
		const playPrevBind = this.playPrevTrack.bind(this);
		const playPauseBind = this.pause.bind(this);
		const playStopBind = this.stop.bind(this);
		const playRepeatOneBind = this.repeatOne.bind(this);
		const playRepeatEventBind = this.repeatEvent.bind(this);
		const playEventBind = this.ctrlEvents.bind(this);
		const playDurationBind = this.ctrlDuration.bind(this);
		const infoBoxBuildBind = this.infoBoxBuild.bind(this);
		const this_ = this;
		for (let i of arractrl) {
			this.actrl.push(this.$(i));
		}
		for (let i of arrpctrl) {
			this.pctrl.push(this.$(i));
		}
		this.eles.ele = this.pctrl[7];
		this.TemplateHelpers = {
			isfilevideo: function (val) {
				return this_.checkFileIsVideo(val);
			},
			isfileaudio: function (val) {
				return this_.checkFileIsAudio(val);
			},
			isint: function (val) {
				return ((typeof (val) === 'number') && (val > -1));
			},
			isstring: function (val) {
				return ((typeof (val) === 'string') && !this_.stringEmpty(val));
			},
			isarray: function (val) {
				return ((this_.$.isArray(val)) && (val.length > 0));
			},
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			}
		};
		this.playerjq.on('ended', () => {
			playRepeatEventBind();
		});
		this.playerjq.bind('timeupdate', () => {
			new Promise(resolve => {
				playEventBind(
					isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration),
					isNaN(this_.playerjs.currentTime) ? -1 : parseInt(this_.playerjs.currentTime)
				);
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.playerjq.bind('durationchange progress', () => {
			new Promise(resolve => {
				this_.pctrl[2].text(
					playDurationBind(
						isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration)));
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.pctrl[10].on('click', (ev) => {
			new Promise(resolve => {
				const [n, p] = this_.getProgressPosition(
					this_.pctrl[10].get(0),
					this_.playerjs.duration,
					ev);
				this_.pctrl[11].css('width', `${p}%`).attr('aria-valuenow', p.toString());
				if (!isNaN(n))
					this_.playerjs.currentTime = n;
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.$(document).on('click', arrpctrl[6], () => {

			if (this.isinfobox) {
				this.infoBoxView(false);
				return;
			} else {
				if (this.playurl === null)
					return;
				let tmplw = this.$.templates(this.eles.tmplw);
				this.eles.ele.html(tmplw.render({}, this.TemplateHelpers));
				this.infoBoxView(true);
				infoBoxBuildBind();
			}
		});
		this.pctrl[4].on('click', () => { playNextBind(); });
		this.pctrl[5].on('click', () => { playPrevBind(); });
		this.actrl[0].on('click', () => { playNextBind(); });
		this.actrl[1].on('click', () => { playPauseBind(); });
		this.actrl[2].on('click', () => { playStopBind(); });
		this.actrl[3].on('click', () => { playStopBind(); });
		this.actrl[4].on('click', () => { playPauseBind(); });
		this.actrl[5].on('click', () => { this.actrl[5].toggleClass('btn-ison'); });
		this.actrl[6].on('click', () => { this.actrl[6].toggleClass('btn-ison'); playRepeatOneBind(); });
		this.actrl[7].on('click', () => { this.IsSpeak = !this.IsSpeak; });
		this.IsSpeak = this.clz[2].Enable;
	};
	get playerjs() {
		return this.playerjq.get(0);
	};
	get Title() {
		return this.playname;
	};
	set Title(s) {
		if ((s === null) || (typeof (s) !== 'string'))
			return;

		let i = s.lastIndexOf('.');
		if (i > 0)
			this.playname = s.substring(0, i);
		else
			this.playname = s;

		if (super.stringEmpty(this.playname))
			this.pctrl[1].empty();
		else
			this.pctrl[1].html(this.playname);
	};
	get IsSpeak() {
		return this.isspeech;
	};
	set IsSpeak(s) {
		this.isspeech = super.getBool(s);
		if (this.isspeech)
			this.actrl[7].addClass('btn-ison');
		else
			this.actrl[7].removeClass('btn-ison');
	};
	ctrlButtonVisible(isvisible) {
		for (let i = 5; i < this.actrl.length; i++) {
			if (isvisible)
				this.actrl[i].removeClass('btndisable');
			else
				this.actrl[i].addClass('btndisable');
		}
	};
	ctrlBoxIcons_(isplays) {
		if (isplays) {
			this.actrl[4].removeClass('bi-pause-circle');
			this.actrl[4].addClass('bi-play-circle');
		} else {
			this.actrl[4].removeClass('bi-play-circle');
			this.actrl[4].addClass('bi-pause-circle');
		}
	};
	ctrlDuration(d) {
		try {
			if (d <= 0)
				return;

			const dd = new Date(d * 1000);
			let str = '';
			if (dd.getUTCHours() > 0)
				str += super.stringHour(dd.getUTCHours()) + ' ';
			if (dd.getUTCMinutes() > 0)
				str += super.stringMinute(dd.getUTCMinutes()) + ' ';
			if (dd.getUTCSeconds() > 0)
				str += super.stringSeconds(dd.getUTCSeconds());
			return str;

		} catch (a) { console.log(a); }
	};
	ctrlEvents(d, c) {
		try {
			if ((d <= -1) || (c <= -1))
				return;

			const p = Math.ceil((c / d) * 100);
			this.pctrl[11].css('width', `${p}%`).attr('aria-valuenow', p.toString());
			this.pctrl[3].text(this.ctrlDuration(c));

		} catch (a) { console.log(a); }
	};
	getEmptyInfoBox_() {
		return {
			title: '',
			desc: '',
			copy: '',
			pub: '',
			tags: '',
			date: '',
			duration: '',
			image: '',
			disk: {
				disk: -1,
				total: -1,
				track: -1,
				enable: false
			},
			albums: [],
			artists: [],
			composers: [],
			genres: []
		};
	};
	infoBoxBuild() {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb((x) => {
				try {

					if (x === null)
						return;

					const d = this_.getEmptyInfoBox_();

					if (this_.checkObject(x, 'title'))
						d.title = x.title;
					if (this_.checkObject(x, 'desc'))
						d.desc = x.desc;
					if (this_.checkObject(x, 'copy'))
						d.copy = x.copy;
					if (this_.checkObject(x, 'pub'))
						d.pub = x.pub;
					if (this_.checkObject(x, 'tags'))
						d.tags = x.tags;
					if (this_.checkObject(x, 'date'))
						d.date = x.date;
					if (this_.checkObject(x, 'duration'))
						d.duration = x.duration;

					if (this_.checkObject(x, 'disk') && (this_.$.isArray(x.disk))) {
						d.disk.disk  = x.disk[0];
						d.disk.total = x.disk[1];
						d.disk.track = x.disk[2];
						d.disk.enable = ((x.disk[0] > -1) || (x.disk[1] > -1) || (x.disk[2] > -1));
					}
					if (this_.checkObject(x, 'albums')) {
						if (this_.$.isArray(x.albums))
							d.albums = x.albums;
						else
							d.albums.push(x.albums);
					}
					if (this_.checkObject(x, 'artists')) {
						if (this_.$.isArray(x.artists))
							d.artists = x.artists;
						else
							d.artists.push(x.artists);
					}
					if (this_.checkObject(x, 'composers')) {
						if (this_.$.isArray(x.composers))
							d.composers = x.composers;
						else
							d.composers.push(x.composers);
					}
					if (this_.checkObject(x, 'genres')) {
						if (this_.$.isArray(x.genres))
							d.genres = x.genres;
						else
							d.genres.push(x.genres);
					}
					const img = this_.clz[0].findImageByUrl(this_.playurl);
					if (img !== null)
						d.image = img;

					let tmpld = this_.$.templates(this_.eles.tmpld);
					this_.eles.ele.html(tmpld.render(d, this_.TemplateHelpers));

					try {
						this_.eles.ele.find('[data-localize]').localize();
					} catch (a) {
						console.log(a);
					}

				} catch (a) { console.log(a); }
			});
			if (this.playurl != null)
				AppConnector.getRequest(
					this.clz[1].dataRawUrl(`data/tag/?src="${encodeURI(decodeURI(this.playurl))}"`), [atc]);

		} catch (a) { console.log(a); }
	};
	eventStartPlay(name) {
		try {
			try {
				let namex = name;
				if (!super.stringEmpty(namex)) {
					const arr = namex.split('/');
					if (arr.length > 0)
						namex = arr[arr.length - 1].trim();
				}
				try {
					if (!super.stringEmpty(namex))
						this.Title = decodeURI(namex);
					else if (this.Title !== name)
						this.Title = name;

				} catch (a) { console.log(a); this.Title = namex; }
			} catch (a) { console.log(a); this.Title = name; }

			this.pctrl[1].html(this.Title);
			this.pctrl[2].text(0);
			this.pctrl[11].width(0);
			this.pctrl[0].show();

		} catch (a) { console.log(a); }
	};
	repeatEvent() {
		try {
			if (!this.actrl[5].hasClass('btn-ison')) {
				this.playname = '';
				this.pctrl[0].hide();
				return;
			}
			this.playNextTrack();
		} catch (a) { console.log(a); }
	};
	repeatOne() {
		try {
			const loop = this.actrl[6].hasClass('btn-ison');
			this.playerjq.attr({
				'loop': loop
			});
		} catch (a) { console.log(a); }
	};
	stop() {
		if (this.isspeech && this.clz[2].Enable)
			this.clz[2].stop();

		this.playerjs.pause();
		this.isplay = false;
		this.playname = '';
		this.playurl = null;
		this.clz[0].index = 0;
		this.infoBoxView(false);
		this.actrl[5].removeClass('btn-ison');
		this.actrl[6].removeClass('btn-ison');
		this.pctrl[1].empty();
		this.pctrl[0].hide();
		this.ctrlBoxIcons_(true);
	}
	pause() {
		if (this.isspeech && this.clz[2].Enable) {
			const state = this.clz[2].pause();
			if (state === this.clz[2].states.Play) {
				this.ctrlBoxIcons_(true);
				return;
			}
			if (state === this.clz[2].states.Pause) {
				this.ctrlBoxIcons_(false);
				return;
			}
		}

		if (this.isplay)
			this.playerjs.pause();
		else
			this.playerjs.play();

		this.isplay = !this.isplay;
		this.ctrlBoxIcons_(this.isplay);
	};
	playNextTrack() {
		const [uri, name] = this.clz[0].getNextTrack();
		this.Title = name;
		this.playTrack(uri);
	};
	playPrevTrack() {
		const [uri, name] = this.clz[0].getPrevTrack();
		this.Title = name;
		this.playTrack(uri);
	};
	playTrack(uri) {
		if (super.stringEmpty(uri))
			return;
		const url = this.clz[1].dataNextUrl(uri, false);
		if (super.stringEmpty(url))
			return;
		this.playTrackext(url, 0);
	};
	playTrackext(url, id) {
		try {
			if (super.stringEmpty(url))
				return;

			if (id > 0) {
				const [uri, name] = this.clz[0].getTrackById(id);
				if (!super.stringEmpty(uri)) {
					if (url.endsWith(uri) && !super.stringEmpty(name))
						this.Title = name;
					else if (super.stringEmpty(this.Title))
						this.Title = uri;
				}
			}
			this.eventStartPlay((super.stringEmpty(this.Title)) ? url : this.Title);

			do {
				if (!this.isspeech || !this.clz[2].Enable)
					break;

				const state = this.clz[2].speak(this.Title, () => {
					if (!this.stringEmpty(this.Title))
						this.playTrackUrl_(url);
				});
				if (state !== this.clz[2].states.Play)
					break;
				return;
			} while (false);

			this.playTrackUrl_(url);

		} catch (a) { console.log(a); }
	};
	playTrackUrl_(url) {
		try {
			const loop = this.actrl[6].hasClass('btn-ison');
			this.playerjq.attr({
				'src': url,
				'poster': '',
				'autoplay': 'autoplay',
				'loop': loop
			});
			this.isplay = true;
			this.playurl = url;

		} catch (a) { console.log(a); this.isplay = false; }
	};
	hide() {
		this.ctrlButtonVisible(false);
		this.stop();
	};
	infoBoxView(b) {
		if (b)
			this.eles.ele.fadeIn();
		else
			this.eles.ele.fadeOut();
		this.isinfobox = b;
	};
};


class VideoPlayer extends AppUtils {
	constructor(inst, pctrl, clz) {
		super();
		this.$ = inst;
		this.clz = clz;
		this.container = this.$(pctrl[0]);
		this.playerjq  = this.$(pctrl[1]);
		this.playstatus = false;
		this.playstream = false;
		this.playpromise = false;
		this.playname = '';
		this.btn = [];
		const playStopBind = this.stop.bind(this);
		const play2BtnBind = this.playpause.bind(this);
		const playFullBind = this.fullscreen.bind(this);
		const playEventBind = this.ctrlEvents.bind(this);
		const this_ = this;
		this.btn.push(this.$(pctrl[8]));
		this.btn.push(this.$(pctrl[4]));
		this.btn.push(this.$(pctrl[5]));

		this.playerjq.on('play', () => {
			this_.container.show();
		});
		this.playerjq.on('ended', () => {
			this_.playpromise = false;
			playStopBind();
		});
		this.playerjq.on('click', () => {
			play2BtnBind();
		});
		this.playerjq.on('loadedmetadata', () => {
			this_.playerjs.volume = (this_.$(pctrl[9]).val() / 100);
		});
		this.playerjq.bind('timeupdate', () => {
			if (!this_.playstream)
				playEventBind(
					isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration),
					isNaN(this_.playerjs.currentTime) ? -1 : parseInt(this_.playerjs.currentTime)
				);
		});
		this.$(pctrl[2]).on('click', () => { playStopBind(); });
		this.$(pctrl[3]).on('click', () => { play2BtnBind(); });
		this.$(pctrl[6]).on('click', () => { playFullBind(); });
		this.$(pctrl[7]).on('click', (ev) => {
			try {
				if (this_.playstream)
					return;
				const [n, p] = this_.getProgressPosition(
					this_.$(pctrl[7]).get(0),
					this_.playerjs.duration,
					ev);
				let vals = `${p}%`;
				this_.btn[0].css('width', `${p}%`).attr('aria-valuenow', p.toString()).html(vals);
				this_.playerjs.currentTime = n;

			} catch (a) { console.log(a); }
		});
		this.$(pctrl[9]).on('change', (e) => { this_.playerjs.volume = (e.target.value / 100); });
	};
	get playerjs() {
		return this.playerjq.get(0);
	};
	fullscreen() {
		try {
			if (this.playerjs.requestFullscreen)
				this.playerjs.requestFullscreen();
			else if (this.playerjs.webkitRequestFullscreen) /* Safari */
				this.playerjs.webkitRequestFullscreen();
			else if (this.playerjs.msRequestFullscreen) /* IE11 */
				this.playerjs.msRequestFullscreen();
		} catch (a) { console.log(a); }
	};
	ctrlEvents(d, c) {
		try {
			if ((d < 0) || (c < 0))
				return;

			if (this.playstream)
				return;

			const val = Math.ceil((c / d) * 100);
			this.btn[0].width(val + '%');
			if (val >= 5)
				this.btn[0].text(val + '%');

		} catch (a) { console.log(a); }
	};
	ctrltoggle(b) {
		const clsname = 'd-none';
		this.playstatus = b;

		if (b) {
			this.btn[1].removeClass(clsname);
			this.btn[2].addClass(clsname);
		} else {
			this.btn[1].addClass(clsname);
			this.btn[2].removeClass(clsname);
			this.btn[0].width(0).empty();
		}
	};
	ctrlPromise(promise) {

		const this_ = this;
		this.playpromise = false;
		if (typeof(promise) === 'undefined')
			return;

		promise.then(_ => { this.playpromise = true; })
			.catch(error => {
				AppConnector.showError(`promise: ${error}`);
				if (error.message.includes('no supported source'))
					this_.stop();
			});
	};
	stop() {
		this.pause();
		this.playerjs.src = '';
		this.container.hide();
		this.playstatus = this.playpromise = false;
	};
	pause() {
		try {
			if ((!this.playstatus) && (!this.playpromise))
				return;
			this.playerjs.pause();
			this.ctrltoggle(false);
		} catch (a) { AppConnector.showError(`pause: ${a}`); }
	};
	play() {
		try {
			if ((this.playstatus) && (this.playpromise))
				return;
			this.ctrlPromise(this.playerjs.play());
			this.ctrltoggle(true);
		} catch (a) { AppConnector.showError(`play: ${a}`); }
	};
	playpause() {
		if (this.playstatus)
			this.pause();
		else
			this.play();
	};
	playext(url) {
		try {
			if (super.stringEmpty(url))
				return;

			this.playstream = false;
			let ism = url.includes('.m3u8'),
				isd = (!ism) ? url.includes('.mpd') : false;

			if (ism || isd) {
				if (ism && this.playerjs.canPlayType('application/vnd.apple.mpegurl')) {
					this.playerjs.src = url;

				} else if (ism && Hls.isSupported()) {
					const hls = new Hls();
					hls.loadSource(url);
					hls.attachMedia(this.playerjs);
				} else if (isd) {
					const dash = dashjs.MediaPlayer().create();
					dash.initialize(this.playerjs, url, true);
				} else {
					return;
				}
				this.playstream = true;
				this.play();
				return;
			}
			this.playerjq.attr({
				'src': url,
				'poster': '',
				'preload': 'none',
				'autoplay': 'none'
			});
			this.play();
			this.ctrltoggle(true);

		} catch (a) { console.log(a); }
	};
	playlocal(name) {
		if (super.stringEmpty(name))
			return;
		this.playext(this.clz[1].dataNextUrl(name, false));
	};
	hide() {
		this.stop();
	};
};


class AppVersion extends AppUtils {
	constructor(ins, el) {
		super();
		this.$ = ins;
		this.ele = el;
		this.versionc = '1.0.0.x';
		this.versionr = null;
		this.url = null;
		this.isshow = true;
	};
	defaultSet() {
		this.$(this.ele).empty();
	};
	refresh() {
	};
	build(dataX) {
		try {
			this.url = null;
			this.versionr = null;
			do {
				if (!super.checkObject(dataX, 'versionc') ||
					!super.checkObject(dataX.versionc, 'appversion') ||
					 super.stringEmpty(dataX.versionc.appversion))
					break;
				this.versionc = dataX.versionc.appversion;

				if (!super.checkObject(dataX.versionc, 'appbaseurl') ||
					 super.stringEmpty(dataX.versionc.appbaseurl))
					break;
				this.url = dataX.versionc.appbaseurl;

			} while (false);

			this.$(this.ele).text(this.versionc);

			do {
				if (this.url === null)
					break;

				if (!super.checkObject(dataX, 'versionr') ||
					!super.checkObject(dataX.versionr, 'appversion') ||
					 super.stringEmpty(dataX.versionr.appversion))
					break;
				this.versionr = dataX.versionr.appversion;

				if (!super.checkObject(dataX.versionr, 'appfile') ||
					 super.stringEmpty(dataX.versionr.appfile))
					break;
				this.url += dataX.versionr.appfile;

				if (this.isshow && (this.versionc.localeCompare(this.versionr) !== 0)) {
					AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg10_1} <a target=_blank href='${this.url}'><b>${this.versionr}</b></a> ${AppLanguage.GetLanguage().warnings.msg10_2}`);
					this.isshow = false;
				}

			} while (false);

		} catch (a) { console.log(a); }
	};
};


class AppTorrent extends AppUtils {
	constructor(ins, el, ctrls, clz) {
		super();
		this.$ = ins;
		this.ele = this.$(el[0]);
		this.form = this.$(el[1]);
		this.btn = this.$(ctrls[0]);
		this.eles = [this.$(ctrls[4]), this.$(ctrls[5]), this.$(ctrls[6]), this.$(ctrls[7])];
		this.clz = clz;
		this.data = {};
		const this_ = this;

		this.$(document).on('click', ctrls[0], () => {
			try {
				this_.sendRequest();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[1], () => {
			try {
				this_.hide();
				this_.defaultSet();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[2], () => {
			try {
				if ((this_.data === null) || (this_.data.magnetlink === null))
					return;
				if ("navigator" in window) {
					navigator.clipboard.writeText(this_.data.magnetlink)
						.then(function () {
							AppConnector.showError(`${AppLanguage.GetLanguage().torrent.copied}: ${this_.data.torrentname}`)
						}, function () {
							AppConnector.showError(AppLanguage.GetLanguage().warnings.msg14)
						});
				}
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[3], () => {
			try {
				if ((this_.data === null) || (this_.data.torrenturl === null))
					return;
				window.open(this_.data.torrenturl, '_blank');
			} catch (a) { console.log(a); }
		});
	};
	sendRequest() {
		try {
			this.eles[3].show();
			this.btn.prop('disabled', true);

			const this_ = this;
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType('text/xml');
			xhr.responseType = 'document';
			xhr.withCredentials = true;
			xhr.open('POST', this.form.attr('action'), true);

			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200)) {
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
					this_.eles[3].hide();
					this_.btn.prop('disabled', false);
				}
				else {
					AppConnector.RequestOnLoad_(xhr, [this]);
				}
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
				this_.eles[3].hide();
				this_.btn.prop('disabled', false);
			};

			xhr.onload = () => { onload_(xhr); };
			xhr.onerror = () => { onerror_(xhr); };
			xhr.send(new FormData(this.form[0]));

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			this.eles[3].hide();
			this.hide();
			this.defaultSet();
		}
	};
	defaultSet() {
		this.data = {};
		this.form.trigger('reset');
		this.btn.prop('disabled', false);
		this.eles[0].val('');
		this.eles[1].show();
		this.eles[2].hide();
		this.eles[3].hide();
	};
	refresh() {
	};
	show() {
		this.ele.fadeIn();
	};
	hide() {
		this.ele.fadeOut();
	};
	open(path) {
		try {
			do {
				if (this.stringEmpty(path))
					break;

				this.defaultSet();
				this.eles[0].val(path);
				this.show();
				return;

			} while (false);

			AppConnector.showError(AppLanguage.GetLanguage().warnings.msg13);
			this.hide();

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (!super.checkObject(dataX, 'TorrentWebResponse'))
				return;
			this.data = dataX.TorrentWebResponse;
			this.btn.prop('disabled', true);
			this.eles[1].fadeOut();
			this.eles[2].fadeIn();
		} catch (a) { console.log(a); }
	};
};


class AppServiceStat extends AppUtils {
	constructor(ins, ele, ctrls, ids, clz) {
		super();
		this.$ = ins;
		this.box = this.$(ele[0]);
		this.form = this.$(ele[1]);
		this.btn = [ctrls[0], ctrls[1]];
		this.eles = {
			stat: {
				id: ids[0],
				template: ctrls[4],
				table: this.$(ctrls[2]),
				target: `${ctrls[2]} tbody`,
				url: '/cmd/command/tasks/stat'
			},
			task: {
				id: ids[1],
				template: ctrls[5],
				table: this.$(ctrls[3]),
				target: `${ctrls[3]} tbody`,
				url: '/cmd/command/tasks'
			}
		};
		this.clz = clz;
		this.isopen = false;
		const this_ = this;

		this.$(document).on('click', this_.btn[0], () => {
			try {
				if (this_.isopen)
					this_.hide();
				else
					this_.open();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', this_.btn[1], () => {
			try {
				this_.hide();
			} catch (a) { console.log(a); }
		});
		this.TemplateHelpers = {
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';
					if (val === '0001-01-01T00:00:00')
						return AppLanguage.GetLanguage().constant.notrun;

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(this_.StringToDateConverter(val)));
				} catch (a) {
					console.log(a);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			},
			tasktickformat(val) {
				try {
					if (typeof (val) !== 'number')
						return val;
					if (val === -10000)
						return AppLanguage.GetLanguage().constant.off;
					if (val === 0)
						return AppLanguage.GetLanguage().constant.on;

					return this_.TimeSpanTicksConverter(val);
				} catch (a) {
					console.log(a);
					return AppLanguage.GetLanguage().constant.error;
				}
			},
			tasktypeformat(val) {
				if (this_.stringEmpty(val))
					return '?';
				return val.replaceAll('_', ' ');
			},
			taskiserror(val) {
				if (typeof (val) !== 'number')
					return false;
				return (val !== -1);
			},
			taskidformat: function (val) {
				if (this_.stringEmpty(val))
					return 'x';
				else if (val === '-1')
					return 'SYSTEM';
				else if (val === 'ID_SCAN')
					return AppLanguage.GetLanguage().stat.scan;
				else if (val === 'ID_SCAN_LOCAL_VIDEO')
					return AppLanguage.GetLanguage().stat.scanvideo;
				else if (val === 'ID_SCAN_LOCAL_AUDIO')
					return AppLanguage.GetLanguage().stat.scanaudio;
				else if (val === 'ID_SCAN_TRANSLATE_AUDIO')
					return AppLanguage.GetLanguage().stat.chanaudio;
				else if (val === 'ID_CHECK_MEDIA')
					return AppLanguage.GetLanguage().stat.check;
				else if (val === 'ID_STAGE_DOWNLOAD')
					return AppLanguage.GetLanguage().stat.download;
				else if (val === 'ID_STAGE_VERSION')
					return AppLanguage.GetLanguage().stat.version;
				else if (val === 'ID_RENEW_EPG')
					return AppLanguage.GetLanguage().stat.epg;
				else if (val === 'ID_SAVE_HISTORY')
					return AppLanguage.GetLanguage().stat.history;
				else if (val === 'ID_STAGE_MOVE')
					return AppLanguage.GetLanguage().stat.move;
				else
					return val;
			},
			stateidformat: function (val) {
				if (this_.stringEmpty(val))
					return 'x';
				else if (val === 'Scan')
					return AppLanguage.GetLanguage().stat.scan;
				else if (val === 'ScanLocalVideo')
					return AppLanguage.GetLanguage().stat.scanvideo;
				else if (val === 'ScanLocalAudio')
					return AppLanguage.GetLanguage().stat.scanaudio;
				else if (val === 'ScanTranslateAudio')
					return AppLanguage.GetLanguage().stat.chanaudio;
				else if (val === 'Check')
					return AppLanguage.GetLanguage().stat.check;
				else
					return val;
			},
			statevalueformat: function (item, index, items) {
				if (item === null)
					return 'x';
				else if (typeof (item) === 'number') {
					return (item == 0) ? '-' : `${item}`;
				}
				return item;
			}
		};
	};
	sendRequest_(url) {
		try {
			const this_ = this;
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType('text/xml');
			xhr.responseType = 'document';
			xhr.withCredentials = true;
			xhr.open('GET', url, true);

			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200)) {
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
					this_.hide();
				}
				else {
					AppConnector.RequestOnLoad_(xhr, [this]);
				}
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
				this_.hide();
			};

			xhr.onload = () => { onload_(xhr); };
			xhr.onerror = () => { onerror_(xhr); };
			xhr.send();

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			this.hide();
		}
	};
	btnState_() {
		const ele = this.$(this.btn[0]);
		ele.find('.btn-text').html(
			this.isopen ? AppLanguage.GetLanguage().stat.close :
				AppLanguage.GetLanguage().stat.submit);
		ele.toggleClass('btn-outline-success btn-warning');
	};
	castStatItems_(data) {
		for (let ele of data.items) {
			if (super.checkObject(ele, 'stats')) {
				let values = [];
				for (let i of ele.stats) {
					values.push(parseInt(i));
				}
				ele.stats = values;
			} else {
				ele.stats = [ 0, 0, 0, 0, 0, 0 ];
			}
		}
		return data;
	};
	castTaskItems_(data) {
		for (let ele of data.items) {
			if (super.checkObject(ele, 'enable'))
				ele.enable = super.getBool(ele.enable);
			if (super.checkObject(ele, 'active'))
				ele.active = super.getBool(ele.active);
			if (super.checkObject(ele, 'runing'))
				ele.runing = super.getBool(ele.runing);
			if (super.checkObject(ele, 'timerlastchange'))
				ele.timerlastchange = super.getBool(ele.timerlastchange);

			if (super.checkObject(ele, 'id'))
				ele.id = ele.id.toString();

			if (super.checkObject(ele, 'periodrun'))
				ele.periodrun = parseInt(ele.periodrun);
			if (super.checkObject(ele, 'duerun'))
				ele.duerun = parseInt(ele.duerun);
			if (super.checkObject(ele, 'dayofweekrun'))
				ele.dayofweekrun = parseInt(ele.dayofweekrun);
			if (super.checkObject(ele, 'lasterror') && super.checkObject(ele.lasterror, 'errorcode'))
				ele.lasterror.errorcode = parseInt(ele.lasterror.errorcode);
		}
		return data;
	};
	defaultSet() {
		this.$(this.eles.stat.target).empty();
		this.$(this.eles.task.target).empty();
		this.eles.stat.table.hide();
		this.eles.task.table.hide();
	};
	refresh() {
	};
	show() {
		this.isopen = true;
		this.box.fadeIn();
		this.btnState_();
	};
	hide() {
		this.isopen = false;
		this.box.fadeOut();
		this.btnState_();
	};
	open() {
		try {
			const this_ = this;
			this.defaultSet();
			this.form.find('input, checkbox, checked').each(function () {
				const ele = $(this);
				if (ele.is(':checked')) {
					const val = ele.prop('id');

					if (!this_.stringEmpty(val)) {
						if (val == this_.eles.stat.id) {
							this_.eles.stat.table.show();
							this_.sendRequest_(this_.clz[1].dataRawUrl(this_.eles.stat.url));
						}
						else if (val == this_.eles.task.id) {
							this_.eles.task.table.show();
							this_.sendRequest_(this_.clz[1].dataRawUrl(this_.eles.task.url));
						}
					}
				}
			});
			this.show();

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (super.checkObject(dataX, 'ProcessStatsXml') &&
				super.checkObject(dataX.ProcessStatsXml, 'items')) {

				let data = { items: [] };
				if (this.$.isArray(dataX.ProcessStatsXml.items))
					data.items = dataX.ProcessStatsXml.items;
				else
					data.items = [dataX.ProcessStatsXml.items];

				if (data.items.length > 0) {
					const tmpl = this.$.templates(this.eles.stat.template);
					this.$(this.eles.stat.target).html(tmpl.render(this.castStatItems_(data), this.TemplateHelpers));
				}
			}
			else if (super.checkObject(dataX, 'SheduleItemsLite') &&
					 super.checkObject(dataX.SheduleItemsLite, 'items')) {

				let data = { items: [] };
				if (this.$.isArray(dataX.SheduleItemsLite.items))
					data.items = dataX.SheduleItemsLite.items;
				else
					data.items = [dataX.SheduleItemsLite.items];

				if (data.items.length > 0) {
					const tmpl = this.$.templates(this.eles.task.template);
					this.$(this.eles.task.target).html(tmpl.render(this.castTaskItems_(data), this.TemplateHelpers));
				}
			}

		} catch (a) { console.log(a); }
	};
};


(function ($) {
	$(document).ready(function ($) {

		AppLanguage.SetLocale();
		$('head').attr({ lang: AppLanguage.GetLanguage().body.lang });
		$('#path').attr('placeholder', AppLanguage.GetLanguage().body.msg12);

		var appVideoPlayer = {};
		var appAudioPlayer = {};
		const appUrl = new AppUrl($, '#breadcrumbTmpl', '#breadcrumb');
		const appFileList = new AppPlayFileList($, '#dirListTmpl', '#fileListTmpl', '#filetable tbody',
			['#searchField', '#searchBtn', '#listLocalHistoryView', '#listLocalHistoryClear'], appUrl);
		const appVoiceSpeak = new AudioVoiceSpeak($, [
			'#ctrlAudioSettingsVoice', '#ctrlAudioSettingsRate', '#ctrlAudioSettingsPitch', '#ctrlAudioSettingsTest'
		]);
		const clz = [appFileList, appUrl, appVoiceSpeak];
		const appSelector = new AppPlaySelector($, '#playToTmpl', '#playtolist');
		const clzs = [appUrl, appSelector, appVoiceSpeak];
		const appServiceControl = new AppServiceControl($, [
			'#ID_CONFIG_RELOAD', '#ID_SCAN', '#ID_SCAN_LOCAL', '#ID_CHECK_MEDIA', '#ID_RENEW_EPG',
			'#LOGIN', '#UNLOGIN', "form[name='upload']"	],
		clz);
		const appViewImages = new ImageView($,
			['#imagesCtrlTmpl', '#imagesListTmpl'],
			['.carousel-indicators', '.carousel-inner', '#ctrlImagesBox', '.btnclose-carusel'],
		clz);
		const appViewNfo = new NfoView($, '#tableNfoTmpl', ['#nfotable tbody', '#viewNfo', '#closeNfo']);
		const appVersion = new AppVersion($, '#versionText');
		const appTorrent = new AppTorrent($, ['#ctrlTorrentBox', '#formnewtorrent'], [
			'#ctrlTorrentSubmit', '#ctrlTorrentClose', '#ctrlTorrentMagnetCopy', '#ctrlTorrentDownloadFile',
			'#torrentDirBox', '#torrentOptionBox', '#torrentResponseBox', '#torrentOptionBoxWait' ],
		clz);
		const appServiceStat = new AppServiceStat($, ['#ctrlStatBox', '#formservicestat'], [
			'#ctrlSrvStatSubmit', '#ctrlSrvStatClose', '#stattable', '#tasktable', '#statListTmpl', '#taskListTmpl'], [
			'srvStatSwitch', 'srvTaskSwitch'],
		clz);

		const initMedia = () => {
			const vctrl = [
				'#ctrlVideoPlayBox', '#ctrlVideoEle', '#ctrlVideoStop', '#ctrlVideoPlayPause', '#ctrlVideoPlay', '#ctrlVideoPause',
				'#ctrlVideoFullScreen', '#progressVideoContainer', '#progressVideoBar', '.form-range-volume'
			];
			const actrl = [
				'#ctrlAudioPlay', '#ctrlAudioPause', '#ctrlAudioStop', '#ctrlAudioStop2', '#ctrlAudioPlayIcon', '#ctrlAudioRepeat',
				'#ctrlAudioRepeatAll','#ctrlAudioSpeech'
			];
			const pctrl = [
				'#ctrlAudioPlayBox', '#ctrlAudioPlayName', '#ctrlAudioPlayDuration', '#ctrlAudioPlayCurrent', '#ctrlAudioBackward', '#ctrlAudioForward',
				'#ctrlAudioInfoBtn', '#ctrlAudioInfoBox', '#audioInfoBoxTmpl','#waitInfoBoxTmpl',
				'#progressAudioContainer', '#progressAudioBar'
			];
			appVideoPlayer = new VideoPlayer($, vctrl, clz);
			appAudioPlayer = new AudioPlayer($, '#audioPlayer', actrl, pctrl, clz);
			clzs.push(appAudioPlayer);
		};
		const loadEvent = function (oldstate) {
			const ele = [$('#loadM3u'), $('#loadVideo'), $('#loadAudio'), $('#loadHistory')];
			const ctrl = $('#ctrlAudioMediaShow');

			for (let el of ele) {
				el.removeClass('active');
			}
			if (oldstate === appUrl.states.listAudio)
				appAudioPlayer.hide();
			else if (oldstate === appUrl.states.listVideo)
				appVideoPlayer.hide();

			appSelector.State = (appSelector.State === appSelector.states.playRemote) ?
				appSelector.states.playEmbed : appSelector.State;

			switch (appUrl.State) {
				case appUrl.states.listM3u: {
					ctrl.hide();
					ele[0].addClass('active');
					break;
				}
				case appUrl.states.listVideo: {
					ctrl.hide();
					ele[1].addClass('active');
					break;
				}
				case appUrl.states.listAudio: {
					ctrl.show();
					ele[2].addClass('active');
					appAudioPlayer.ctrlButtonVisible(true);
					break;
				}
				case appUrl.states.listHistory: {
					ctrl.hide();
					ele[3].addClass('active');
					break;
				}
			};

			$(document).attr('title', appUrl.pageTitle());
			AppConnector.saveSettings(null, clzs);
		};
		const loadSelectorElement = function (obj, s) {
			try {
				let ele = $(obj).parent().parent().parent().find(s);
				if (typeof (ele) === 'undefined')
					return null;
				return ele;
			} catch (error) {
				console.log(error);
				return null;
			}
		};
		const loadTableDirElement = function (obj) {
			return loadSelectorElement(obj, '#listMediaDirs');
		};
		const loadTableFileElement = function (obj) {
			return loadSelectorElement(obj, '#listMediaFile');
		};
		const buildUrl = function (url) {
			const b = (url.includes('://'));
			const uri = b ? url.trim() :
				((appUrl.State === appUrl.states.listHistory) ?
					`${appUrl.dataRawUrl(url.substring(1).trim())}` : appUrl.dataNextUrl(url.trim(), false));
			return [uri, b];
		};
		const selectUrl = function (url) {
			const [uri, b] = buildUrl(url);
			return uri;
		};

		try {
			$('[data-localize]').localize();
		} catch (error) {
			console.log(error);
		}

		$(document).on('focus', '#datePicker', function () {
			$(this).datepicker({
				language: AppLanguage.GetLanguage().body.lang,
				format: "yyyy-mm-dd",
				forceParse: false,
				autoclose: true,
				clearBtn: true,
				todayHighlight: true
			});
		});
		$(document).on('changeDate', '#datePicker', function (a) {
			appFileList.filterHistory(a);
		});
		$('#sort1Up').on('click', function () { appFileList.sort1Up(); });
		$('#sort1Down').on('click', function () { appFileList.sort1Down(); });
		$('#sort2Up').on('click', function () { appFileList.sort2Up(); });
		$('#sort2Down').on('click', function () { appFileList.sort2Down(); });
		$('#sort3Up').on('click', function () { appFileList.sort3Up(); });
		$('#sort3Down').on('click', function () { appFileList.sort3Down(); });
		$('#ctrlNotifyLink').on('click', function () { $('#ctrlNotifyBox').fadeOut(); });
		$('#ctrlAudioPlayLink').on('click', function () { $('#ctrlAudioPlayBox').fadeOut(); });
		$(document).on('click', '#loadM3u', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listM3u), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadVideo', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listVideo), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadAudio', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listAudio), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadHistory', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listHistory), clz);
			loadEvent(state);
		});
		$(document).on('click', '#filevideo', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appVideoPlayer.playext(
					selectUrl(ele.data('uri').trim()));
				appFileList.addHistory(parseInt(ele.data('id')));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#fileaudio', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				let id = parseInt(ele.data('id'));
				appAudioPlayer.playTrackext(
					selectUrl(ele.data('uri').trim()), id);
				appFileList.addHistory(id);
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filenfo', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				AppConnector.reloadBegin(
					selectUrl(ele.data('uri').trim()), [appViewNfo]);
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filem3u', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appFileList.buildm3u(
					selectUrl(ele.data('uri').trim()));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filexspf', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appFileList.buildxspf(
					selectUrl(ele.data('uri').trim()));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#fileimg', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appViewImages.build(ele.data('id'));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#dirtotorrent', function () {
			try {
				let ele = loadTableDirElement(this);
				if (ele == null)
					return;
				let part = ele.data('uri');
				if (typeof (part) !== 'undefined') {
					appTorrent.open(appUrl.dataFragmentRawUrl(part.trim()));
				};
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '.breadcrumb-item', function () {
			let url = appUrl.dataFragmentUrl($(this).find('a').text().trim());
			AppConnector.reloadBegin(url, clz);
		});
		$(document).on('click', '#listLocalHistory', function () {
			appFileList.buildHistory();
			$('#listLocalHistoryView').show();
		});
		$(document).on('click', '.icon-image-border-style', function (e) {
			e.preventDefault();
			$('.icon-image-preview-style').attr('src', $(this).attr('src'));
			const m = $('#ctrlImageModal');
			m.find('.modal-title').text($(this).attr('alt'))
			m.modal('show');
			return false;
		});
		$(document).on('click', '#ctrlImageModal', function (e) {
			e.preventDefault();
			$(this).modal('hide');
			return false;
		});
		$(document).on('click', '#listMediaDirs', function () {
			const uri = $(this).data('uri').toString();
			if ((uri === null) || uri.isEmpty())
				return;

			let url;
			if (uri.includes('../') && (appFileList.isreload || (appUrl.Count === 1))) {
				if (!appFileList.isreload) {
					AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg1}, ${appUrl.pageTitle()}`);
					return;
				}
				url = appUrl.dataNextUrl('', false);
			} else {
				url = appUrl.dataNextUrl(uri.trim(), true);
			}
			AppConnector.reloadBegin(url, clz);
			AppConnector.saveSettings(url, clzs);
		});
		$(document).on('click', '#listMediaFile', function () {
			const uri = $(this).data('uri').toString();
			if ((uri === null) || uri.isEmpty())
				return;

			const [url, b] = buildUrl(uri);
			const id = parseInt($(this).data('id'));

			/* console.log('listMediaFile:', uri, url); */

			switch (appSelector.State) {
				case appSelector.states.playCurrentWindow: {
					$(location).attr('href', url);
					appFileList.addHistory(id);
					break;
				}
				case appSelector.states.playNewWindows: {
					window.open(url, '_blank');
					appFileList.addHistory(id);
					break;
				}
				case appSelector.states.sendTo: {
					if (!b && (appUrl.State === appUrl.states.listM3u)) {
						appFileList.buildm3u(url);
						break;
					}
					try {
						const obj = { title: $(this).children().first().text(), url: decodeURI(url) };
						if (window.navigator.canShare(obj)) {
							window.navigator.share(obj)
								.then(() => {})
								.catch((error) => AppConnector.showError(`${error}`));
							appFileList.addHistory(id);
						}
					} catch (a) {
						console.log(a);
					}
					break;
				}
				case appSelector.states.playEmbed: {

					/* console.log('playEmbed:', uri, url, id); */
					appFileList.addHistory(id);

					try {
						let found = false;
						do {
							const appUtils = new AppUtils();
							if (appUtils.checkFileIsImage(uri)) {
								appViewImages.build(id);
								found = true;
								break;
							}
							if (appUtils.checkFileIsVideo(uri)) {
								appVideoPlayer.playext(url);
								found = true;
								break;
							}
							if (appUtils.checkFileIsAudio(uri)) {
								appAudioPlayer.playTrackext(url, b ? 0 : id);
								found = true;
								break;
							}
							if (appUtils.checkFileIsNfo(uri)) {
								AppConnector.reloadBegin(url, [appViewNfo]);
								found = true;
								break;
							}
							if (appUtils.checkFileIsM3u(uri)) {
								appFileList.buildm3u(url);
								found = true;
								break;
							}
							if (appUtils.checkFileIsXspf(uri)) {
								appFileList.buildxspf(url);
								found = true;
								break;
							}
						} while (false);

						if (found)
							break;

					} catch (a) { console.log(a); }

					switch (appUrl.State) {
						case appUrl.states.listM3u: {
							if (b)
								appVideoPlayer.playext(url);
							else
								appFileList.buildm3u(url);
							break;
						}
						case appUrl.states.listVideo: {
							appVideoPlayer.playext(url);
							break;
						}
						case appUrl.states.listAudio: {
							appAudioPlayer.playTrackext(url, b ? 0 : id);
							break;
						}
						case appUrl.states.listHistory: {
							window.open(url, '_blank');
							break;
						}
					};
					break;
				}
				case appSelector.states.playRemote: {
					appFileList.addHistory(id);
					break;
				}
			};
		});
		appSelector.defaultSet();
		initMedia();
		{
			let saveurl = AppConnector.loadSettings(clzs);
			if (appUrl.State == appUrl.states.listHistory) {
				appUrl.State == appUrl.states.listM3u;
				saveurl = appUrl.dataGroupUrl(appUrl.State);
			} else if ((saveurl === null) || saveurl.isEmpty())
				saveurl = appUrl.dataGroupUrl(appUrl.State);
			AppConnector.getRequest(saveurl, [appFileList, appUrl, appVersion]);
		}
		loadEvent(appUrl.states.None);
		/* AppServiceWorker.Init(['#pwaInstall']); */
		// AppServiceWorker.Init(['#pwaInstall']);
	});
})(jQuery);
