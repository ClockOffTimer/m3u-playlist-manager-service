﻿
class AppPlaySelector extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.states = { playCurrentWindow: 0, playNewWindows: 1, playEmbed: 2, sendTo: 3, playRemote: 4 };
		this.state = this.states.playEmbed;
		this.eles = { tmpl: tmpln, ele: elen };
		this.data = {
			items: []
		};
		this.dataT = {
			items: [
				{ name: AppLanguage.GetLanguage().selector.opt0, id: 0, lineb: false },
				{ name: AppLanguage.GetLanguage().selector.opt1, id: 1, lineb: false },
				{ name: AppLanguage.GetLanguage().selector.opt2, id: 2, lineb: !AppUrl.IsHttps }
			]
		};
		const this_ = this;
		if (AppUrl.IsHttps)
			this.dataT.items.push({ name: AppLanguage.GetLanguage().selector.opt3, id: 3, lineb: AppUrl.IsHttps });
		this.dataT.items.push({ name: AppLanguage.GetLanguage().selector.opt4, id: 50, lineb: false });
		this.$(elen).on('click', '.dropdown-item', function () {
			try {
				const n = parseInt($(this).data('id'));
				switch (n) {
					case 0:
					case 1:
					case 2:
					case 3: {
						this_.state = n;
						break;
					}
					case 50: {
						AppConnector.showError(this_.dataT.items[4].name);
						break;
					}
				}
			} catch (a) { console.log(a); }
		});
	};
	static get Tag() {
		return 'selectorState';
	};
	get State() {
		return this.state;
	};
	set State(s) {
		let t = typeof (s);
		if ((t == typeof (this.states)) || (t === 'number')) {
			if (!AppUrl.IsHttps && (s === this.states.sendTo))
				this.state = this.states.playEmbed;
			this.state = s;
		}
	};
	defaultSet() {
		this.data = this.dataT;
		this.refresh();
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data));
	};
	build(dataX) {
		try {
			if (!super.checkObject(dataX, 'items')) {
				this.refresh();
				return;
			}
			var datanew = { items: [] };
			datanew.items = this.dataT.items.slice(0, (AppUrl.IsHttps) ? 3 : 2);
			dataX.items.forEach(function (item) {
				datanew.items.push(item);
			});
			this.data = datanew;
			this.refresh();
		} catch (a) { console.log(a); }
	};
};
