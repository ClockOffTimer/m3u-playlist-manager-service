﻿
class AppTorrent extends AppUtils {
	constructor(ins, el, ctrls, clz) {
		super();
		this.$ = ins;
		this.ele = this.$(el[0]);
		this.form = this.$(el[1]);
		this.btn = this.$(ctrls[0]);
		this.eles = [this.$(ctrls[4]), this.$(ctrls[5]), this.$(ctrls[6]), this.$(ctrls[7])];
		this.clz = clz;
		this.data = {};
		const this_ = this;

		this.$(document).on('click', ctrls[0], () => {
			try {
				this_.sendRequest();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[1], () => {
			try {
				this_.hide();
				this_.defaultSet();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[2], () => {
			try {
				if ((this_.data === null) || (this_.data.magnetlink === null))
					return;
				if ("navigator" in window) {
					navigator.clipboard.writeText(this_.data.magnetlink)
						.then(function () {
							AppConnector.showError(`${AppLanguage.GetLanguage().torrent.copied}: ${this_.data.torrentname}`)
						}, function () {
							AppConnector.showError(AppLanguage.GetLanguage().warnings.msg14)
						});
				}
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', ctrls[3], () => {
			try {
				if ((this_.data === null) || (this_.data.torrenturl === null))
					return;
				window.open(this_.data.torrenturl, '_blank');
			} catch (a) { console.log(a); }
		});
	};
	sendRequest() {
		try {
			this.eles[3].show();
			this.btn.prop('disabled', true);

			const this_ = this;
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType('text/xml');
			xhr.responseType = 'document';
			xhr.withCredentials = true;
			xhr.open('POST', this.form.attr('action'), true);

			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200)) {
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
					this_.eles[3].hide();
					this_.btn.prop('disabled', false);
				}
				else {
					AppConnector.RequestOnLoad_(xhr, [this]);
				}
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
				this_.eles[3].hide();
				this_.btn.prop('disabled', false);
			};

			xhr.onload = () => { onload_(xhr); };
			xhr.onerror = () => { onerror_(xhr); };
			xhr.send(new FormData(this.form[0]));

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			this.eles[3].hide();
			this.hide();
			this.defaultSet();
		}
	};
	defaultSet() {
		this.data = {};
		this.form.trigger('reset');
		this.btn.prop('disabled', false);
		this.eles[0].val('');
		this.eles[1].show();
		this.eles[2].hide();
		this.eles[3].hide();
	};
	refresh() {
	};
	show() {
		this.ele.fadeIn();
	};
	hide() {
		this.ele.fadeOut();
	};
	open(path) {
		try {
			do {
				if (this.stringEmpty(path))
					break;

				this.defaultSet();
				this.eles[0].val(path);
				this.show();
				return;

			} while (false);

			AppConnector.showError(AppLanguage.GetLanguage().warnings.msg13);
			this.hide();

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (!super.checkObject(dataX, 'TorrentWebResponse'))
				return;
			this.data = dataX.TorrentWebResponse;
			this.btn.prop('disabled', true);
			this.eles[1].fadeOut();
			this.eles[2].fadeIn();
		} catch (a) { console.log(a); }
	};
};
