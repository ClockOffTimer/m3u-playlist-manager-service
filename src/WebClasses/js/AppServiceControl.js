﻿
class AppServiceControl extends AppUtils {
	constructor(ins, bctrl, clz) {
		super();
		this.$ = ins;
		this.clz = clz;
		const sendCmdBind = this.sendcmd.bind(this);
		const sendSysBind = this.sendsys.bind(this);
		const uploadBind  = this.upload_.bind(this);
		this.$(bctrl[0]).on('click', () => { sendCmdBind(130); });
		this.$(bctrl[1]).on('click', () => { sendCmdBind(131); });
		this.$(bctrl[2]).on('click', () => { sendCmdBind(132); });
		this.$(bctrl[3]).on('click', () => { sendCmdBind(134); }); // 133 Audio scan
		this.$(bctrl[4]).on('click', () => { sendCmdBind(135); });
		this.$(bctrl[5]).on('click', () => { sendSysBind('cmd/login'); });
		this.$(bctrl[6]).on('click', () => { sendSysBind('cmd/unlogin'); });
		this.$(bctrl[7]).on('submit', (ev) => {
			ev.preventDefault();
			try {
				try {
					let cat = this.$(`${bctrl[7]} #path`);
					cat.val(cat.val().trim());
				} catch {}
				const form = this.$(bctrl[7])[0];
				const arr = form.upfile.files;
				if ((arr !== null) && (arr.length > 0))
					uploadBind(form);
				else
					form.reset();
			} catch (a) { AppConnector.showError(`Upload: ${a}`); }
			return false;
		});
	};
	sendcmd(command) {
		try {
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType("text/xml");
			xhr.responseType = 'document';
			xhr.withCredentials = false;
			xhr.open('GET', this.clz[1].dataBaseUrl() + 'cmd/command/' + command, true);
			xhr.onload = () => {
				if (xhr.status != 200)
					AppConnector.RequestOnError_(
						this, null,
						AppLanguage.GetLanguage().warnings.msg3
					);
			};
			xhr.onerror = () => {
				AppConnector.RequestOnError_(
					this, null,
					AppLanguage.GetLanguage().warnings.msg2
				);
			};
			xhr.onloadend = () => { };
			xhr.send();

		} catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`); }
	};
	sendsys(url) {
		try {
			$.get(this.clz[1].dataBaseUrl() + url, (rawdata) => {
				if (rawdata == null)
					return;
				AppConnector.showError(rawdata);
			})
			.fail(() => {
				AppConnector.RequestOnError_(
					this,
					AppLanguage.GetLanguage().warnings.msg8,
					AppLanguage.GetLanguage().warnings.msg2
				);
			});
		} catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`); }
	};
	upload_(form) {
		const jq_ = this.$;
		try {
			let isStart = false,
				isEnd = false,
				isProgress = false;
			const data = new FormData(form);
			const xhr = new XMLHttpRequest();
			xhr.responseType = 'text';
			xhr.withCredentials = false;
			xhr.open('POST', '/cmd/upload', true);

			const onprogress_ = (ev) => {
				if (isProgress || !ev.hasOwnProperty('loaded') || !ev.hasOwnProperty('total'))
				isProgress = true;
				const val = Math.floor(ev.loaded / ev.total * 100);
				const vals = `${val}%`;
				jq_('#progressUploadBar').css('width', vals).attr('aria-valuenow', val.toString()).html(vals);
				isProgress = false;
			};
			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200))
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
				if ((ev.response !== null) && (typeof (ev.response) === 'string') && !ev.response.isEmpty())
					AppConnector.showError(ev.response);
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
			};
			const onloadstart_ = (ev) => {
				if (isStart) return;
				isStart = true;
				jq_('#progressUploadBar').css('width', '0%').attr('aria-valuenow', '0').html('0%');
				jq_('#progressUploadContainer').fadeIn();
			};
			const onloadend_ = (ev) => {
				if (isEnd) return;
				isEnd = true;
				jq_('#progressUploadContainer').fadeOut();
				form.reset();
			};

			xhr.onprogress = (ev) => { onprogress_(ev); };
			xhr.onload = (ev) => { onload_(xhr); };
			xhr.onerror = (ev) => { onerror_(xhr); };
			xhr.onloadstart = (ev) => { onloadstart_(ev); };
			xhr.onloadend = (ev) => { onloadend_(ev); }

			xhr.upload.onprogress = (ev) => { onprogress_(ev); };
			xhr.upload.onload = (ev) => { onload_(xhr); };
			xhr.upload.onerror = (ev) => { onerror_(xhr); };
			xhr.upload.onloadstart = (ev) => { onloadstart_(ev); };
			xhr.upload.onloadend = (ev) => { onloadend_(ev); }

			xhr.send(data);

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			jq_('#progressUploadContainer').fadeOut();
		}
	};
};

