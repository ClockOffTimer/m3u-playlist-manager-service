﻿
class AppVersion extends AppUtils {
	constructor(ins, el) {
		super();
		this.$ = ins;
		this.ele = el;
		this.versionc = '1.0.0.x';
		this.versionr = null;
		this.url = null;
		this.isshow = true;
	};
	defaultSet() {
		this.$(this.ele).empty();
	};
	refresh() {
	};
	build(dataX) {
		try {
			this.url = null;
			this.versionr = null;
			do {
				if (!super.checkObject(dataX, 'versionc') ||
					!super.checkObject(dataX.versionc, 'appversion') ||
					 super.stringEmpty(dataX.versionc.appversion))
					break;
				this.versionc = dataX.versionc.appversion;

				if (!super.checkObject(dataX.versionc, 'appbaseurl') ||
					 super.stringEmpty(dataX.versionc.appbaseurl))
					break;
				this.url = dataX.versionc.appbaseurl;

			} while (false);

			this.$(this.ele).text(this.versionc);

			do {
				if (this.url === null)
					break;

				if (!super.checkObject(dataX, 'versionr') ||
					!super.checkObject(dataX.versionr, 'appversion') ||
					 super.stringEmpty(dataX.versionr.appversion))
					break;
				this.versionr = dataX.versionr.appversion;

				if (!super.checkObject(dataX.versionr, 'appfile') ||
					 super.stringEmpty(dataX.versionr.appfile))
					break;
				this.url += dataX.versionr.appfile;

				if (this.isshow && (this.versionc.localeCompare(this.versionr) !== 0)) {
					AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg10_1} <a target=_blank href='${this.url}'><b>${this.versionr}</b></a> ${AppLanguage.GetLanguage().warnings.msg10_2}`);
					this.isshow = false;
				}

			} while (false);

		} catch (a) { console.log(a); }
	};
};
