﻿class AudioVoiceSpeak extends AppUtils {
	constructor(inst, arractrl, clz) {
		super();
		this.$ = inst;
		this.states = { Disabled: 0, Play: 1, Pause: 2, None: 3 };
		this.state = this.states.None;
		this.handle = window.speechSynthesis || null;
		this.clz = clz;
		this.actrl = [];
		this.voices = [];
		this.voice = -1;
		this.rate = 1.0;
		this.pitch = 1.0;
		this.isenable = (this.handle !== null);
		const speakBind = this.speak.bind(this);
		const stopBind = this.stop.bind(this);
		const this_ = this;
		for (let i of arractrl) {
			this.actrl.push(this.$(i));
		}
		if (this.isenable) {

			this.handle.onvoiceschanged = () => {
				try {
					this.voices = this.handle.getVoices();
					for (let i = 0; i < this.voices.length; i++) {
						const v = this.voices[i];
						let sdef = (v.default) ? '+' : '';
						this.actrl[0].append(
							this.buildOption_(i, `${v.name} (${sdef}${v.lang})`)
						);
					}
					if ((this.voices.length > 0) && (this.voice >= 0) && (this.voice < this.voices.length))
						this.actrl[0][0].selectedIndex = this.voice;

				} catch (a) { console.log(a); }
			};
		} else {
			this.actrl[0].append(
				this.buildOption_(-1, AppLanguage.GetLanguage().warnings.msg11)
			);
		}
		this.actrl[0].change(() => { this.voice = this.actrl[0].val(); });
		this.actrl[1].change(() => { this.rate  = this.actrl[1].val(); });
		this.actrl[2].change(() => { this.pitch = this.actrl[2].val(); });
		this.actrl[3].on('click', () => {
			try {
				if (this.handle.speaking)
					stopBind();
				else
					speakBind(AppLanguage.GetLanguage().warnings.msg12, () => { });

			} catch (a) { console.log(a); }
		});
	};
	get Enable() {
		return this.isenable;
	};
	get State() {
		return this.state;
	};
	get VoiceIndex() {
		return this.voice;
	};
	set VoiceIndex(i) {
		try {
			if ((typeof (i) === 'number') && (i >= 0)) {
				if (this.voices.length > 0) {
					if ((i >= 0) && (i < this.voices.length))
						this.voice = i;
				} else {
					this.voice = i;
				}
			}
		} catch (a) { console.log(a); }
	};
	buildOption_(id, txt) {
		return this.$('<option>', {
				value: id,
				text: txt
			});
	};
	speak(txt, cb) {
		if (!this.isenable)
			return this.states.Disabled;

		try {
			if (this.handle.speaking)
				this.handle.cancel();

			const ut = new SpeechSynthesisUtterance(txt);
			ut.onend = function () {
				try { cb(); } catch (a) { console.log(a); }
			};
			ut.onerror = function (event) {
				AppConnector.showError(`${event.error}`);
			};
			if ((this.voices.length > 0) && (this.voice >= 0))
				ut.voice = this.voices[this.voice];
			ut.rate = isNaN(this.rate) ? 1.0 : this.rate;
			ut.pitch = isNaN(this.pitch) ? 1.0 : this.pitch;
			this.handle.speak(ut);
			this.state = this.states.Play;
			return this.state;

		} catch (a) { console.log(a); this.state = this.states.None; }
		return this.states.None;
	};
	stop() {
		try {
			if (this.isenable) {
				if (this.handle.paused)
					this.handle.resume();
				if (this.handle.speaking)
					this.handle.cancel();
				this.state = this.states.None;
			}
		} catch (a) { console.log(a); }
	}
	pause() {
		try {
			if (this.isenable) {
				let idx = ((this.handle.speaking) * 1) + ((this.handle.paused) * 2);
				switch (idx) {
					case 1: {
						this.handle.pause();
						this.state = this.states.Pause;
						break;
					}
					case 2:
					case 3: {
						this.handle.resume();
						this.state = this.states.Play;
						break;
					}
					default: {
						this.state = this.states.None;
						break;
					}
				}
				return this.state;
			}
		} catch (a) { console.log(a); }
		return this.states.Disabled;
	};
	/* empty call API */
	defaultSet() {};
	build(dataX) {};
};
