﻿
class AppUtils {
	constructor() {
		this.mime = {
			ext: {
				nfo:   ['nfo'],
				m3u:   ['m3u', 'm3u8'],
				xspf:  ['xspf'],
				image: ['jpg', 'jpeg', 'png', 'gif'],
				video: ['mpeg', 'mpg', 'mp4', 'avi', 'wmv', 'webm', 'mov', 'mkv', 'm2v', 'm4v', 'm4p', 'mpeg2', 'mpeg-2'],
				audio: ['mp3','wav','aac','wma','opus']
			}
		};
		this.items = {
			dir: ['папка', 'папки', 'папок'],
			file: ['файл', 'файла', 'файлов'],
			day: ['день', 'дня', 'дней'],
			hour: ['час', 'часа', 'часов'],
			min: ['минута', 'минуты', 'минут'],
			sec: ['секунда', 'секунды', 'секунд']
		};
	};
	calcDateString__(n) {
		switch ((n % 100) % 10) {
			case 0:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9: return 2;
			case 2:
			case 3:
			case 4: return (n > 9 && 20 > n) ? 2 : 1;
			case 1: return 0;
			default: return 0;
		}
	};
	stringDirectory(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.dir[idx]}`;
	};
	stringFiles(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.file[idx]}`;
	};
	stringDay(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.day[idx]}`;
	};
	stringHour(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.hour[idx]}`;
	};
	stringMinute(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.min[idx]}`;
	};
	stringSeconds(n) {
		let idx = this.calcDateString__(n);
		return `${n} ${this.items.sec[idx]}`;
	};
	stringEmpty(val) {
		return ((typeof (val) !== 'string') || (val === null) || val.isEmpty());
	};
	checkObject(root, name) {
		return ((typeof (root) != 'undefined') &&
				(root != null) &&
				root.hasOwnProperty(name));
	};
	checkFileIsImage(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.image.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsVideo(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.video.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsAudio(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.audio.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsNfo(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.nfo.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsM3u(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.m3u.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	checkFileIsXspf(val) {
		var file = val.split('.').pop();
		if (this.mime.ext.xspf.indexOf(file) == -1) {
			return false;
		}
		return true;
	};
	getBool(val) {
		if (val === null)
			return false;

		let t = typeof (val);
		if (t === 'boolean')
			return val;
		else if (t === 'number')
			return (val > 0);
		else if (t === 'string')
			return (val === 'true');
		return false;
	}
	getProgressPosition(ele, duration, ev) {
		let b = ele.getBoundingClientRect();
		let x = ev.clientX - b.left;
		let p = Math.floor(x / (ele.clientWidth / 100));
		return [parseFloat(p * (duration / 100)), p];
	};
	StringToDateConverter(val) {
		const regex = /(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/s;
		const result = val.match(regex);
		return ((result !== null) && (result.length > 0)) ? result[0] : val;
	};
	TimeSpanTicksConverter(val) {
		let str = '';
		const dates = this.convertDayTimeFromTick_(val);
		const found = [false, false];
		if (dates[0].getDate() !== dates[1].getDate()) str += this.stringDay(dates[1].getDate());
		if (dates[0].getHours() !== dates[1].getHours()) found[0] = true;
		if (dates[0].getMinutes() !== dates[1].getMinutes()) found[1] = true;

		if (found[0] && found[1])
			str += `${this.convertTo2Digit_(dates[1].getHours())}:${this.convertTo2Digit_(dates[1].getMinutes())}`;
		else if (found[0] && !found[1])
			str += `${this.convertTo2Digit_(dates[1].getHours())}:00`;
		else if (!found[0] && found[1])
			str += `00:${this.convertTo2Digit_(dates[1].getMinutes())}`;
		return (str.length == 0) ? '-' : str;
	};
	convertDayTimeFromTick_(val) {
		let utc0 = Date.UTC(0, 0, 0, 0, 0, 0, 0);
		utc0 = (utc0 - ((Math.abs(new Date(utc0).getTimezoneOffset()) * 60) * 1000));
		let arr = [];
		arr.push(new Date(utc0));
		arr.push(new Date(utc0 + (val / 10000)));
		return arr;
	};
	convertTo2Digit_(val) {
		return (val > 9) ? `${val}` : `0${val}`;
	};
};
