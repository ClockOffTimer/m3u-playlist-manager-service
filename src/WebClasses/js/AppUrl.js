﻿class AppUrl extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.eles = { tmpl: tmpln, ele: elen };
		this.data = {
			paths: []
		};
		this.template = {
			paths: []
		};
		this.states = { listM3u: 0, listVideo: 1, listAudio: 2, listHistory: 3, listTorrents: 4, None: 5 };
		this.state = this.states.listVideo;
	};
	static get Tag() {
		return 'urlState';
	};
	static get IsHttps() {
		return window.location.protocol.startsWith('https:');
	};
	get Protocol() {
		return (AppUrl.IsHttps) ? 'https://' : 'http://';
	};
	get Count() {
		return this.data.paths.length;
	};
	get Data() {
		if (this.Count === 0)
			return null;
		return this.dataEncodeUrl(this.data.paths.join('/'));
	};
	get Prefix() {
		if (this.Count === 0)
			return '';
		return this.data.paths[0];
	};
	get State() {
		return this.state;
	};
	set State(s) {
		let t = typeof (s);
		if ((t == typeof (this.states)) || (t === 'number'))
			this.state = s;
	};
	convertUrlToHost(url) {
		return this.convertUrlTo_(url, ['://', '/'], false);
	};
	convertUrlToName(url) {
		return this.convertUrlTo_(url, ['/','.'], true);
	};
	convertFileToUrl(file) {
		try {
			let url = decodeURI(file);
			let idx = url.indexOf('file:///');
			if (idx < 0)
				return;
			url = url.substring(idx + 8);

			for (let s of this.template.paths) {
				if (!url.startsWith(s.path))
					continue;
				idx = url.indexOf(s.path);
				if (idx < 0)
					continue;

				url = url.substring(idx + s.path.length);
				return this.dataEncodeUrl(`${s.tag}/${url}`, '');
			}

		} catch (a) { console.log(a); }
		return file;
	};
	convertUrlTo_(url, tags, b) {
		try {
			let unpack = decodeURI(url);
			let off = (b) ? unpack.lastIndexOf(tags[0]) : unpack.indexOf(tags[0]);
			if (off > 0) {
				off += tags[0].length;
				return unpack.substring(off, unpack.indexOf(tags[1], off));
			}
		} catch (a) { console.log(a); }
		return null;
	};
	dataRawUrl(s) {
		return this.dataBaseUrl() + s;
	};
	dataEncodeUrl(s, f) {
		return this.dataBaseUrl() + this.dataEncodePartUrl(s) + f;
	};
	dataEncodePartUrl(s) {
		return encodeURI(s)
			.replace(/['()]/g, escape)
			.replace(/\*/g, '%2A')
			.replace(/%(?:7C|60|5E)/g, unescape);
	};
	dataBaseUrl() {
		return this.Protocol + window.location.hostname + ':' + window.location.port + '/';
	};
	dataNextUrl(part, b) {
		var url = '';
		for (let a of this.data.paths)
			url += a + '/';
		return this.dataEncodeUrl(url + part, + (b) ? '/' : '');
	};
	dataFragmentUrl(tag) {
		var url = '';
		for (let a of this.data.paths) {
			url += a + '/';
			if (tag.localeCompare(a) === 0)
				break;
		};
		return this.dataEncodeUrl(url, '');
	};
	dataFragmentRawUrl(part) {
		var url = '';
		for (let a of this.data.paths) {
			url += a + '/';
		};
		if (typeof (part) !== 'undefined')
			url += part;
		return url;
	};
	dataGroupUrl(s) {
		this.state = s;
		switch (s) {
			case this.states.listM3u: return this.dataBaseUrl() + 'm3u/';
			case this.states.listVideo: return this.dataBaseUrl() + 'video/';
			case this.states.listAudio: return this.dataBaseUrl() + 'audio/';
			case this.states.listTorrents: return this.dataBaseUrl() + 'torrents/';
			case this.states.listHistory: return this.dataBaseUrl() + 'history/';
			default: return this.dataBaseUrl();
		};
	};
	pageTitle() {
		const title = AppLanguage.GetLanguage().title.msg0;
		switch (this.state) {
			case this.states.listM3u:   return title + AppLanguage.GetLanguage().title.msg1;
			case this.states.listVideo: return title + AppLanguage.GetLanguage().title.msg2;
			case this.states.listAudio: return title + AppLanguage.GetLanguage().title.msg3;
			case this.states.listTorrents: return title + AppLanguage.GetLanguage().title.msg4;
			case this.states.listHistory: return title + AppLanguage.GetLanguage().title.msg5;
			default: return title;
		};
	};
	defaultSet() {
		this.data = { paths: [] };
		this.refresh();
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data));
	};
	build(dataX) {
		try {
			this.data.paths = [];
			if (super.checkObject(dataX, 'url') && !super.stringEmpty(dataX.url)) {
				for (let s of dataX.url.split('/')) {
					if (super.stringEmpty(s))
						continue;
					this.data.paths.push(s);
				}
			}
			if (super.checkObject(dataX, 'paths') && $.isArray(dataX.paths))
				for (let x of dataX.paths) {
					let s = x.name.replace(/\\/g, '/');
					let a = { tag: x.tag, path: s };
					this.template.paths.push(a);
				}

			this.refresh();
		} catch (a) { console.log(a); }
	};
};
