﻿
(function ($) {
	$(document).ready(function ($) {

		AppLanguage.SetLocale();
		$('head').attr({ lang: AppLanguage.GetLanguage().body.lang });
		$('#path').attr('placeholder', AppLanguage.GetLanguage().body.msg12);

		var appVideoPlayer = {};
		var appAudioPlayer = {};
		const appUrl = new AppUrl($, '#breadcrumbTmpl', '#breadcrumb');
		const appFileList = new AppPlayFileList($, '#dirListTmpl', '#fileListTmpl', '#filetable tbody',
			['#searchField', '#searchBtn', '#listLocalHistoryView', '#listLocalHistoryClear'], appUrl);
		const appVoiceSpeak = new AudioVoiceSpeak($, [
			'#ctrlAudioSettingsVoice', '#ctrlAudioSettingsRate', '#ctrlAudioSettingsPitch', '#ctrlAudioSettingsTest'
		]);
		const clz = [appFileList, appUrl, appVoiceSpeak];
		const appSelector = new AppPlaySelector($, '#playToTmpl', '#playtolist');
		const clzs = [appUrl, appSelector, appVoiceSpeak];
		const appServiceControl = new AppServiceControl($, [
			'#ID_CONFIG_RELOAD', '#ID_SCAN', '#ID_SCAN_LOCAL', '#ID_CHECK_MEDIA', '#ID_RENEW_EPG',
			'#LOGIN', '#UNLOGIN', "form[name='upload']"	],
		clz);
		const appViewImages = new ImageView($,
			['#imagesCtrlTmpl', '#imagesListTmpl'],
			['.carousel-indicators', '.carousel-inner', '#ctrlImagesBox', '.btnclose-carusel'],
		clz);
		const appViewNfo = new NfoView($, '#tableNfoTmpl', ['#nfotable tbody', '#viewNfo', '#closeNfo']);
		const appVersion = new AppVersion($, '#versionText');
		const appTorrent = new AppTorrent($, ['#ctrlTorrentBox', '#formnewtorrent'], [
			'#ctrlTorrentSubmit', '#ctrlTorrentClose', '#ctrlTorrentMagnetCopy', '#ctrlTorrentDownloadFile',
			'#torrentDirBox', '#torrentOptionBox', '#torrentResponseBox', '#torrentOptionBoxWait' ],
		clz);
		const appServiceStat = new AppServiceStat($, ['#ctrlStatBox', '#formservicestat'], [
			'#ctrlSrvStatSubmit', '#ctrlSrvStatClose', '#stattable', '#tasktable', '#statListTmpl', '#taskListTmpl'], [
			'srvStatSwitch', 'srvTaskSwitch'],
		clz);

		const initMedia = () => {
			const vctrl = [
				'#ctrlVideoPlayBox', '#ctrlVideoEle', '#ctrlVideoStop', '#ctrlVideoPlayPause', '#ctrlVideoPlay', '#ctrlVideoPause',
				'#ctrlVideoFullScreen', '#progressVideoContainer', '#progressVideoBar', '.form-range-volume'
			];
			const actrl = [
				'#ctrlAudioPlay', '#ctrlAudioPause', '#ctrlAudioStop', '#ctrlAudioStop2', '#ctrlAudioPlayIcon', '#ctrlAudioRepeat',
				'#ctrlAudioRepeatAll','#ctrlAudioSpeech'
			];
			const pctrl = [
				'#ctrlAudioPlayBox', '#ctrlAudioPlayName', '#ctrlAudioPlayDuration', '#ctrlAudioPlayCurrent', '#ctrlAudioBackward', '#ctrlAudioForward',
				'#ctrlAudioInfoBtn', '#ctrlAudioInfoBox', '#audioInfoBoxTmpl','#waitInfoBoxTmpl',
				'#progressAudioContainer', '#progressAudioBar'
			];
			appVideoPlayer = new VideoPlayer($, vctrl, clz);
			appAudioPlayer = new AudioPlayer($, '#audioPlayer', actrl, pctrl, clz);
			clzs.push(appAudioPlayer);
		};
		const loadEvent = function (oldstate) {
			const ele = [$('#loadM3u'), $('#loadVideo'), $('#loadAudio'), $('#loadHistory')];
			const ctrl = $('#ctrlAudioMediaShow');

			for (let el of ele) {
				el.removeClass('active');
			}
			if (oldstate === appUrl.states.listAudio)
				appAudioPlayer.hide();
			else if (oldstate === appUrl.states.listVideo)
				appVideoPlayer.hide();

			appSelector.State = (appSelector.State === appSelector.states.playRemote) ?
				appSelector.states.playEmbed : appSelector.State;

			switch (appUrl.State) {
				case appUrl.states.listM3u: {
					ctrl.hide();
					ele[0].addClass('active');
					break;
				}
				case appUrl.states.listVideo: {
					ctrl.hide();
					ele[1].addClass('active');
					break;
				}
				case appUrl.states.listAudio: {
					ctrl.show();
					ele[2].addClass('active');
					appAudioPlayer.ctrlButtonVisible(true);
					break;
				}
				case appUrl.states.listHistory: {
					ctrl.hide();
					ele[3].addClass('active');
					break;
				}
			};

			$(document).attr('title', appUrl.pageTitle());
			AppConnector.saveSettings(null, clzs);
		};
		const loadSelectorElement = function (obj, s) {
			try {
				let ele = $(obj).parent().parent().parent().find(s);
				if (typeof (ele) === 'undefined')
					return null;
				return ele;
			} catch (error) {
				console.log(error);
				return null;
			}
		};
		const loadTableDirElement = function (obj) {
			return loadSelectorElement(obj, '#listMediaDirs');
		};
		const loadTableFileElement = function (obj) {
			return loadSelectorElement(obj, '#listMediaFile');
		};
		const buildUrl = function (url) {
			const b = (url.includes('://'));
			const uri = b ? url.trim() :
				((appUrl.State === appUrl.states.listHistory) ?
					`${appUrl.dataRawUrl(url.substring(1).trim())}` : appUrl.dataNextUrl(url.trim(), false));
			return [uri, b];
		};
		const selectUrl = function (url) {
			const [uri, b] = buildUrl(url);
			return uri;
		};

		try {
			$('[data-localize]').localize();
		} catch (error) {
			console.log(error);
		}

		$(document).on('focus', '#datePicker', function () {
			$(this).datepicker({
				language: AppLanguage.GetLanguage().body.lang,
				format: "yyyy-mm-dd",
				forceParse: false,
				autoclose: true,
				clearBtn: true,
				todayHighlight: true
			});
		});
		$(document).on('changeDate', '#datePicker', function (a) {
			appFileList.filterHistory(a);
		});
		$('#sort1Up').on('click', function () { appFileList.sort1Up(); });
		$('#sort1Down').on('click', function () { appFileList.sort1Down(); });
		$('#sort2Up').on('click', function () { appFileList.sort2Up(); });
		$('#sort2Down').on('click', function () { appFileList.sort2Down(); });
		$('#sort3Up').on('click', function () { appFileList.sort3Up(); });
		$('#sort3Down').on('click', function () { appFileList.sort3Down(); });
		$('#ctrlNotifyLink').on('click', function () { $('#ctrlNotifyBox').fadeOut(); });
		$('#ctrlAudioPlayLink').on('click', function () { $('#ctrlAudioPlayBox').fadeOut(); });
		$(document).on('click', '#loadM3u', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listM3u), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadVideo', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listVideo), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadAudio', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listAudio), clz);
			loadEvent(state);
		});
		$(document).on('click', '#loadHistory', function () {
			const state = appUrl.State;
			AppConnector.reloadBegin(appUrl.dataGroupUrl(appUrl.states.listHistory), clz);
			loadEvent(state);
		});
		$(document).on('click', '#filevideo', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appVideoPlayer.playext(
					selectUrl(ele.data('uri').trim()));
				appFileList.addHistory(parseInt(ele.data('id')));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#fileaudio', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				let id = parseInt(ele.data('id'));
				appAudioPlayer.playTrackext(
					selectUrl(ele.data('uri').trim()), id);
				appFileList.addHistory(id);
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filenfo', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				AppConnector.reloadBegin(
					selectUrl(ele.data('uri').trim()), [appViewNfo]);
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filem3u', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appFileList.buildm3u(
					selectUrl(ele.data('uri').trim()));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#filexspf', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appFileList.buildxspf(
					selectUrl(ele.data('uri').trim()));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#fileimg', function () {
			try {
				let ele = loadTableFileElement(this);
				if (ele == null)
					return;
				appViewImages.build(ele.data('id'));
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '#dirtotorrent', function () {
			try {
				let ele = loadTableDirElement(this);
				if (ele == null)
					return;
				let part = ele.data('uri');
				if (typeof (part) !== 'undefined') {
					appTorrent.open(appUrl.dataFragmentRawUrl(part.trim()));
				};
			} catch (error) {
				console.log(error);
			}
		});
		$(document).on('click', '.breadcrumb-item', function () {
			let url = appUrl.dataFragmentUrl($(this).find('a').text().trim());
			AppConnector.reloadBegin(url, clz);
		});
		$(document).on('click', '#listLocalHistory', function () {
			appFileList.buildHistory();
			$('#listLocalHistoryView').show();
		});
		$(document).on('click', '.icon-image-border-style', function (e) {
			e.preventDefault();
			$('.icon-image-preview-style').attr('src', $(this).attr('src'));
			const m = $('#ctrlImageModal');
			m.find('.modal-title').text($(this).attr('alt'))
			m.modal('show');
			return false;
		});
		$(document).on('click', '#ctrlImageModal', function (e) {
			e.preventDefault();
			$(this).modal('hide');
			return false;
		});
		$(document).on('click', '#listMediaDirs', function () {
			const uri = $(this).data('uri').toString();
			if ((uri === null) || uri.isEmpty())
				return;

			let url;
			if (uri.includes('../') && (appFileList.isreload || (appUrl.Count === 1))) {
				if (!appFileList.isreload) {
					AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg1}, ${appUrl.pageTitle()}`);
					return;
				}
				url = appUrl.dataNextUrl('', false);
			} else {
				url = appUrl.dataNextUrl(uri.trim(), true);
			}
			AppConnector.reloadBegin(url, clz);
			AppConnector.saveSettings(url, clzs);
		});
		$(document).on('click', '#listMediaFile', function () {
			const uri = $(this).data('uri').toString();
			if ((uri === null) || uri.isEmpty())
				return;

			const [url, b] = buildUrl(uri);
			const id = parseInt($(this).data('id'));

			/* console.log('listMediaFile:', uri, url); */

			switch (appSelector.State) {
				case appSelector.states.playCurrentWindow: {
					$(location).attr('href', url);
					appFileList.addHistory(id);
					break;
				}
				case appSelector.states.playNewWindows: {
					window.open(url, '_blank');
					appFileList.addHistory(id);
					break;
				}
				case appSelector.states.sendTo: {
					if (!b && (appUrl.State === appUrl.states.listM3u)) {
						appFileList.buildm3u(url);
						break;
					}
					try {
						const obj = { title: $(this).children().first().text(), url: decodeURI(url) };
						if (window.navigator.canShare(obj)) {
							window.navigator.share(obj)
								.then(() => {})
								.catch((error) => AppConnector.showError(`${error}`));
							appFileList.addHistory(id);
						}
					} catch (a) {
						console.log(a);
					}
					break;
				}
				case appSelector.states.playEmbed: {

					/* console.log('playEmbed:', uri, url, id); */
					appFileList.addHistory(id);

					try {
						let found = false;
						do {
							const appUtils = new AppUtils();
							if (appUtils.checkFileIsImage(uri)) {
								appViewImages.build(id);
								found = true;
								break;
							}
							if (appUtils.checkFileIsVideo(uri)) {
								appVideoPlayer.playext(url);
								found = true;
								break;
							}
							if (appUtils.checkFileIsAudio(uri)) {
								appAudioPlayer.playTrackext(url, b ? 0 : id);
								found = true;
								break;
							}
							if (appUtils.checkFileIsNfo(uri)) {
								AppConnector.reloadBegin(url, [appViewNfo]);
								found = true;
								break;
							}
							if (appUtils.checkFileIsM3u(uri)) {
								appFileList.buildm3u(url);
								found = true;
								break;
							}
							if (appUtils.checkFileIsXspf(uri)) {
								appFileList.buildxspf(url);
								found = true;
								break;
							}
						} while (false);

						if (found)
							break;

					} catch (a) { console.log(a); }

					switch (appUrl.State) {
						case appUrl.states.listM3u: {
							if (b)
								appVideoPlayer.playext(url);
							else
								appFileList.buildm3u(url);
							break;
						}
						case appUrl.states.listVideo: {
							appVideoPlayer.playext(url);
							break;
						}
						case appUrl.states.listAudio: {
							appAudioPlayer.playTrackext(url, b ? 0 : id);
							break;
						}
						case appUrl.states.listHistory: {
							window.open(url, '_blank');
							break;
						}
					};
					break;
				}
				case appSelector.states.playRemote: {
					appFileList.addHistory(id);
					break;
				}
			};
		});
		appSelector.defaultSet();
		initMedia();
		{
			let saveurl = AppConnector.loadSettings(clzs);
			if (appUrl.State == appUrl.states.listHistory) {
				appUrl.State == appUrl.states.listM3u;
				saveurl = appUrl.dataGroupUrl(appUrl.State);
			} else if ((saveurl === null) || saveurl.isEmpty())
				saveurl = appUrl.dataGroupUrl(appUrl.State);
			AppConnector.getRequest(saveurl, [appFileList, appUrl, appVersion]);
		}
		loadEvent(appUrl.states.None);
		/* AppServiceWorker.Init(['#pwaInstall']); */
		// AppServiceWorker.Init(['#pwaInstall']);
	});
})(jQuery);
