﻿
class AppConnector {
	static getRequest(url, farr) {
		try {
			$('#spinnerTop').show();

			let xhr = new XMLHttpRequest();
			xhr.overrideMimeType("text/xml");
			xhr.responseType = 'document';
			xhr.withCredentials = false;
			xhr.open('GET', url, true)
			xhr.onload = function () {
				AppConnector.RequestOnLoad_(this, farr);
			};
			xhr.onerror = function () {
				AppConnector.RequestOnError_(
					this,
					AppLanguage.GetLanguage().warnings.msg2,
					AppLanguage.GetLanguage().warnings.msg2
				);
			};
			xhr.onloadend = function () {
				$('#spinnerTop').hide();
			};
			xhr.send();

		} catch (a) { console.log(a); }
	};
	static RequestOnError_(xhr, s1, s2) {
		if ((typeof (xhr.status) === 'undefined') && (typeof (xhr.statusText) === 'undefined')) {
			if (s1 !== null)
				AppConnector.showError(s1);
		}
		else if (typeof (xhr.status) === 'undefined')
			AppConnector.showError(`${s2} = ${xhr.statusText}`);
		else if (typeof (xhr.statusText) === 'undefined')
			AppConnector.showError(`${s2} = ${xhr.status}`);
		else
			AppConnector.showError(`${s2} = ${xhr.status}: ${xhr.statusText}`);
	};
	static RequestOnLoad_(xhr, farr) {
		try {
			if (xhr.response == null)
				return;
			let x = new X2JS().xml2json(xhr.response);
			/* console.log('-X-', x); */
			if ((typeof (x) != 'undefined') && (x != null))
				try {
					for (let f of farr) {
						if ((typeof (f) != 'undefined') && (f != null)) {

							if (x.hasOwnProperty('error') &&
								(x.error != null) &&
								(typeof (x.error) === 'string') &&
								(!x.error.isEmpty())) { AppConnector.showError(x.error); }

							else if (x.hasOwnProperty('status') && (x.status != null)) {
								if (x.status.hasOwnProperty('error') &&
									(x.status.error != null) &&
									(typeof (x.status.error) === 'string') &&
									(!x.status.error.isEmpty())) {
									AppConnector.showError(`${x.status.errorcode} = ${x.status.error}`); }}

							if (x.hasOwnProperty('root'))
								f.build(x.root);
							else
								f.build(x);
						}
					}
				} catch (a) { console.log(a); }
		}
		catch (a) { AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2} = ${a.message}`); }
		finally { $('#spinnerTop').hide(); }
	};
	static showError(str) {
		if ((typeof (str) === 'undefined') || str.isEmpty())
			return;
		$('.text-error-container').html(str);
		$('#ctrlNotifyBox').fadeIn();
	};
	static reloadDefault(farr) {
		if ((typeof (farr) != 'undefined') && (farr != null))
			try {
				for (let f of farr) {
					if ((typeof (f) != 'undefined') && (f != null))
						f.defaultSet();
				}
			} catch (a) { console.log(a); }
	};
	static reloadBegin(url, farr) {
		AppConnector.reloadDefault(farr);
		AppConnector.getRequest(url, farr);
	};
	static saveSettings(s, farr) {
		if ((typeof (farr) === 'undefined') || (farr === null))
			return;

		if (farr[1].State === farr[1].states.listHistory)
			return;

		try {
			window.localStorage.setItem(AppPlaySelector.Tag, JSON.stringify(
				{
					ustate: farr[0].State,
					sstate: farr[1].State,
					speechenable: farr[3].IsSpeak,
					speechvoice: farr[2].VoiceIndex,
					speechrate: farr[2].rate,
					speechpitch: farr[2].pitch
				}
			));
			let data = ((typeof (s) === 'undefined') || (s === null)) ? farr[0].Data : s;
			if ((data === null) || (typeof (data) !== 'string'))
				return;

			window.localStorage.setItem(AppUrl.Tag, JSON.stringify(
				{ saveurl: data }
			));
		} catch (error) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2}: ${error.message}`);
		}
	};
	static loadSettings(farr) {
		if ((typeof (farr) === 'undefined') || (farr === null))
			return null;

		try {
			var json = window.localStorage.getItem(AppPlaySelector.Tag);
			if (json !== null) {
				let obj = JSON.parse(json);
				if (obj.hasOwnProperty('ustate'))
					farr[0].State = obj.ustate;
				if (obj.hasOwnProperty('sstate'))
					farr[1].State = obj.sstate;
				if (obj.hasOwnProperty('speechrate'))
					farr[2].rate = obj.speechrate;
				if (obj.hasOwnProperty('speechpitch'))
					farr[2].pitch = obj.speechpitch;
				if (obj.hasOwnProperty('speechvoice')) {
					const t = typeof (obj.speechvoice);
					if ((t === 'number') && (obj.speechvoice >= 0))
						farr[2].VoiceIndex = obj.speechvoice;
					else if ((t === 'string') && (!obj.speechvoice.isEmpty()))
						farr[2].VoiceIndex = parseInt(obj.speechvoice);
				}
				if (obj.hasOwnProperty('speechenable'))
					farr[3].IsSpeak = obj.speechenable;
			}
			json = window.localStorage.getItem(AppUrl.Tag);
			if (json !== null) {
				let obj = JSON.parse(json);
				if (obj.hasOwnProperty('saveurl') && ((obj.saveurl !== null) && (typeof (obj.saveurl) === 'string') && !obj.saveurl.isEmpty()))
					return obj.saveurl;
			}
			console.log('loadSettings', json);
		} catch (error) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2}: ${error.message}`);
		}
		return null;
	};
};
