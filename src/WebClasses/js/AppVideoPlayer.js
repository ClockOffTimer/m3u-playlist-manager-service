﻿
class VideoPlayer extends AppUtils {
	constructor(inst, pctrl, clz) {
		super();
		this.$ = inst;
		this.clz = clz;
		this.container = this.$(pctrl[0]);
		this.playerjq  = this.$(pctrl[1]);
		this.playstatus = false;
		this.playstream = false;
		this.playpromise = false;
		this.playname = '';
		this.btn = [];
		const playStopBind = this.stop.bind(this);
		const play2BtnBind = this.playpause.bind(this);
		const playFullBind = this.fullscreen.bind(this);
		const playEventBind = this.ctrlEvents.bind(this);
		const this_ = this;
		this.btn.push(this.$(pctrl[8]));
		this.btn.push(this.$(pctrl[4]));
		this.btn.push(this.$(pctrl[5]));

		this.playerjq.on('play', () => {
			this_.container.show();
		});
		this.playerjq.on('ended', () => {
			this_.playpromise = false;
			playStopBind();
		});
		this.playerjq.on('click', () => {
			play2BtnBind();
		});
		this.playerjq.on('loadedmetadata', () => {
			this_.playerjs.volume = (this_.$(pctrl[9]).val() / 100);
		});
		this.playerjq.bind('timeupdate', () => {
			if (!this_.playstream)
				playEventBind(
					isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration),
					isNaN(this_.playerjs.currentTime) ? -1 : parseInt(this_.playerjs.currentTime)
				);
		});
		this.$(pctrl[2]).on('click', () => { playStopBind(); });
		this.$(pctrl[3]).on('click', () => { play2BtnBind(); });
		this.$(pctrl[6]).on('click', () => { playFullBind(); });
		this.$(pctrl[7]).on('click', (ev) => {
			try {
				if (this_.playstream)
					return;
				const [n, p] = this_.getProgressPosition(
					this_.$(pctrl[7]).get(0),
					this_.playerjs.duration,
					ev);
				let vals = `${p}%`;
				this_.btn[0].css('width', `${p}%`).attr('aria-valuenow', p.toString()).html(vals);
				this_.playerjs.currentTime = n;

			} catch (a) { console.log(a); }
		});
		this.$(pctrl[9]).on('change', (e) => { this_.playerjs.volume = (e.target.value / 100); });
	};
	get playerjs() {
		return this.playerjq.get(0);
	};
	fullscreen() {
		try {
			if (this.playerjs.requestFullscreen)
				this.playerjs.requestFullscreen();
			else if (this.playerjs.webkitRequestFullscreen) /* Safari */
				this.playerjs.webkitRequestFullscreen();
			else if (this.playerjs.msRequestFullscreen) /* IE11 */
				this.playerjs.msRequestFullscreen();
		} catch (a) { console.log(a); }
	};
	ctrlEvents(d, c) {
		try {
			if ((d < 0) || (c < 0))
				return;

			if (this.playstream)
				return;

			const val = Math.ceil((c / d) * 100);
			this.btn[0].width(val + '%');
			if (val >= 5)
				this.btn[0].text(val + '%');

		} catch (a) { console.log(a); }
	};
	ctrltoggle(b) {
		const clsname = 'd-none';
		this.playstatus = b;

		if (b) {
			this.btn[1].removeClass(clsname);
			this.btn[2].addClass(clsname);
		} else {
			this.btn[1].addClass(clsname);
			this.btn[2].removeClass(clsname);
			this.btn[0].width(0).empty();
		}
	};
	ctrlPromise(promise) {

		const this_ = this;
		this.playpromise = false;
		if (typeof(promise) === 'undefined')
			return;

		promise.then(_ => { this.playpromise = true; })
			.catch(error => {
				AppConnector.showError(`promise: ${error}`);
				if (error.message.includes('no supported source'))
					this_.stop();
			});
	};
	stop() {
		this.pause();
		this.playerjs.src = '';
		this.container.hide();
		this.playstatus = this.playpromise = false;
	};
	pause() {
		try {
			if ((!this.playstatus) && (!this.playpromise))
				return;
			this.playerjs.pause();
			this.ctrltoggle(false);
		} catch (a) { AppConnector.showError(`pause: ${a}`); }
	};
	play() {
		try {
			if ((this.playstatus) && (this.playpromise))
				return;
			this.ctrlPromise(this.playerjs.play());
			this.ctrltoggle(true);
		} catch (a) { AppConnector.showError(`play: ${a}`); }
	};
	playpause() {
		if (this.playstatus)
			this.pause();
		else
			this.play();
	};
	playext(url) {
		try {
			if (super.stringEmpty(url))
				return;

			this.playstream = false;
			let ism = url.includes('.m3u8'),
				isd = (!ism) ? url.includes('.mpd') : false;

			if (ism || isd) {
				if (ism && this.playerjs.canPlayType('application/vnd.apple.mpegurl')) {
					this.playerjs.src = url;

				} else if (ism && Hls.isSupported()) {
					const hls = new Hls();
					hls.loadSource(url);
					hls.attachMedia(this.playerjs);
				} else if (isd) {
					const dash = dashjs.MediaPlayer().create();
					dash.initialize(this.playerjs, url, true);
				} else {
					return;
				}
				this.playstream = true;
				this.play();
				return;
			}
			this.playerjq.attr({
				'src': url,
				'poster': '',
				'preload': 'none',
				'autoplay': 'none'
			});
			this.play();
			this.ctrltoggle(true);

		} catch (a) { console.log(a); }
	};
	playlocal(name) {
		if (super.stringEmpty(name))
			return;
		this.playext(this.clz[1].dataNextUrl(name, false));
	};
	hide() {
		this.stop();
	};
};
