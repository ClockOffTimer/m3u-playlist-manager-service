﻿
class AppLanguage {
    constructor() {
        this.LocaleDateFmtWYMDHM = null;
        this.LocaleDateFmtWYMD = null;
        this.LocaleDateFmtDHM = null;
        this.LocaleLang = 'ru';
    };
    getLocaleLang() {
        return this.LocaleLang;
    };
    setLocaleLang(l) {
        this.LocaleLang = l;
        this.LocaleDateFmtWYMDHM = null;
        this.LocaleDateFmtWYMD = null;
    };
    static SetLocale() {
        try {
            if (typeof (Intl) != 'undefined')
                this.LocaleLang = Intl.NumberFormat().resolvedOptions().locale;
            else if (window.navigator.languages) {
                this.LocaleLang = window.navigator.languages[0];
            } else {
                this.LocaleLang = window.navigator.userLanguage || window.navigator.language;
            }
        } catch (error) {
            console.log(error);
            this.LocaleLang = 'ru';
        }
        AppLanguage.Languages.default = eval('AppLanguage.Languages.' + this.LocaleLang);
    };
    static GetDateFormaterWYMDHM() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtWYMDHM == null) {
            this.LocaleDateFmtWYMDHM =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                        weekday: 'long',
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                        hour: '2-digit',
                        minute: '2-digit'
                });
        }
        return this.LocaleDateFmtWYMDHM;
    };
    static GetDateFormaterWYMD() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtWYMD == null) {
            this.LocaleDateFmtWYMD =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                });
        }
        return this.LocaleDateFmtWYMD;
    };
    static GetDateFormaterDHM() {
        if (typeof (Intl) == 'undefined')
            return {};
        if (this.LocaleDateFmtDHM == null) {
            this.LocaleDateFmtDHM =
                new Intl.DateTimeFormat(
                    this.LocaleLang, {
                    day: 'numeric',
                    hour: '2-digit',
                    minute: '2-digit'
                });
        }
        return this.LocaleDateFmtWYMDHM;
    };
    static GetLanguage() {
        try {
            if (this.LocaleLang == '')
                return this.Languages.default;
            else
                return eval('AppLanguage.Languages.' + this.LocaleLang);
        } catch (error) {
            console.log(error);
            return AppLanguage.Languages.default;
        }
    };
    static GetLanguageKey(key) {
        try {
            if (this.LocaleLang == '')
                return eval('AppLanguage.Languages.default.' + key);
            else
                return eval('AppLanguage.Languages.' + this.LocaleLang + '.' + key);
        } catch (error) {
            console.log(error);
            return AppLanguage.Languages.default;
        }
    };
    static Languages = {
        en: {
            constant: {
                on: "on",
                off: "off",
                error: "error",
                notrun: "did not start"
            },
            selector: {
                opt0: "Open on this page",
                opt1: "Open a new page and play",
                opt2: "Play with built-in player",
                opt3: "Open To ..",
                opt4: "Other options are currently not available .."
            },
            warnings: {
                msg1: "You are already in the root directory",
                msg2: "Connection error",
                msg3: "Service returned an error",
                msg4: "Command sending error",
                msg5: "Limit is exceeded",
                msg6: "Your browsing history is empty",
                msg7: "Feature not supported by browser",
                msg8: "You have successfully signed out until you can use advanced commands and manage the service.",
                msg9: "File upload",
                msg10_1: "A new version of the service is available, you can download",
                msg10_2: "this version from the link.",
                msg11: "Your browser not support the speech synthesis",
                msg12: "To be, or not to be, that is the question: Whether tis nobler in the mind to suffer the slings and arrows of outrageous fortune, or to take arms against a sea of troubles and by opposing end them.",
                msg13: "Root folder not defined, no further execution possible",
                msg14: "Failure to copy. Check permissions for clipboard"
            },
            title: {
                msg0: "PlayList service: ",
                msg1: "M3U playlist",
                msg2: "Video library",
                msg3: "Audio library",
                msg4: "Torrents",
                msg5: "View History"
            },
            body: {
                lang: "en",
                name: "M3U Play list service",
                msg1: "Update service configuration",
                msg2: "Refresh M3U playlists",
                msg3: "Refresh Playlists of Video Archive",
                msg4: "Run playlist check",
                msg5: "Update TV EPG Program",
                msg6: "Play to",
                msg7: "Loading...",
                msg8: "Prev",
                msg9: "Next",
                msg10: "Select target",
                msg11: "Playlists",
                msg12: "path in area",
                msg13: "Video",
                msg14: "Audio",
                msg15: "Play to",
                msg16: "M3U",
                msg17: "History",
                footer1: "This program does not store video files. The base of links to videos and the video fragments themselves are created by the user independently.<br/>As a rule, links to public URLs of video streams are deliberately distributed",
                footer2: "by copyright holders and operators providing access to video streams, the latter do it for advertising purposes hoping to increase the paid audience.<br/>Most operators providing access services officially publish their own playlists in M3U format among its paid subscribers.",
                footer3: "The creation of links is not a direct violation of copyright, as it is a description of the resource and not the author's material itself.",
                footer4: "If you think that any links in your playlists violate the rights of the copyright holder or access operator - remove them! :)",
                footer5: "Install as app"
            },
            nfo: {
                none: "NONE",
                minutes: "min",
                seconds: "sec",
                season: "SEASON",
                episode: "EPISODE",
                close: "Close",
                actors: "Actors",
                directors: "Directors",
                credits: "Credits",
                tags: "Tags",
                video: "VIDEO",
                aspect: "ASPECT",
                audio: "AUDIO",
                channels: "CHANNELS"
            },
            audiobox: {
                title: "Title:",
                disk: "Track/Disk:",
                desc: "Description:",
                copy: "Copyright:",
                pub: "Publisher:",
                date: "Date:",
                dur: "Duration:",
                albums: "Albums:",
                artists: "Artists:",
                comps: "Composers:",
                genres: "Genres:",
                tags: "Tags:"
            },
            torrent: {
                submit: "Create torrent",
                close: "Close",
                ext: "file mask",
                trackers: "tracker list",
                onedir: "only root directory",
                alldirs: "add all sub-directories",
                basedir: "creating a torrent from the contents of a directory",
                copied: "Magnet link copied",
                magnet: "Copy magnet link",
                download: "Download torrent"
            },
            stat: {
                submit: "Show service statistics",
                close:  "Hide service statistics",
                swstat: "file processing statistics",
                swtask: "running task statistics",
                id: "Id",
                last: "Update",
                all: "All",
                error: "Error",
                reject: "Reject",
                good: "Good",
                bad: "Bad",
                dup: "Dup",
                scan: "Scan M3U files",
                scanvideo: "Scan Video files",
                scanaudio: "Scan Audio files",
                chanaudio: "Audio broadcast channel",
                check: "Check all file source",
                download: "Playlist update",
                version: "Version check",
                epg: "EPG update",
                history: "Save history",
                move: "Create a savepoint",
                monitor: "State",
                type: "Task type",
                period: "Launch period",
                due: "Last run time",
                status: "Status"
            },
            default: null
        },
        ru: {
            constant: {
                on: "включено",
                off: "выключено",
                error: "ошибка",
                notrun: "не запускался"
            },
            selector: {
                opt0: "Открывать на этой странице",
                opt1: "Открыть новую страницу и воспроизвести",
                opt2: "Воспроизвести с помощью встроенного плеера",
                opt3: "Открыть в ..",
                opt4: "Другие варианты сейчас не доступны.."
            },
            warnings: {
                msg1: "Вы уже находитесь в корневой директории",
                msg2: "Ошибка соединения",
                msg3: "Сервис вернул ошибку",
                msg4: "Ошибка отправки команды",
                msg5: "Превышен лимит",
                msg6: "История просмотров пуста",
                msg7: "Возможность не поддерживается браузером",
                msg8: "Вы успешно вышли из аккаунта, пока вы не сможете использовать расширенные команды и управлять сервисом.",
                msg9: "Отправка файла",
                msg10_1: "Доступна новая версия сервиса, вы можете загрузить версию",
                msg10_2: "по ссылке.",
                msg11: "Ваш браузер не поддерживает синтез речи",
                msg12: "Как скучно мы живем! В нас пропал дух авантюризма! Мы перестали лазить в окна к любимым женщинам. Мы перестали делать большие хорошие глупости.",
                msg13: "Корневая папка не определена, дальнейшее выполнение невозможно",
                msg14: "Невозможно скопировать, проверьте права на запись буфера обмена"
            },
            title: {
                msg0: "Плей-лист сервис: ",
                msg1: "M3U плей-листы",
                msg2: "Видео архив",
                msg3: "Аудио архив",
                msg4: "Торренты",
                msg5: "История просмотров"
            },
            body: {
                lang: "ru",
                name: "Сервис плей-листов M3U",
                msg1: "Обновить конфигурацию сервиса",
                msg2: "Обновить плей-листы M3U",
                msg3: "Обновить плей-листы Видео архива",
                msg4: "Запустиь проверку плей-листов",
                msg5: "Обновить ТВ программу EPG",
                msg6: "Воспроизвести на",
                msg7: "Загружаем...",
                msg8: "Туда",
                msg9: "Сюда",
                msg10: "Выбор назначения",
                msg11: "Плей-лист",
                msg12: "путь в категории",
                msg13: "Видео",
                msg14: "Аудио",
                msg15: "Воспроизвести на",
                msg16: "M3U",
                msg17: "История",
                footer1: "В этой программе не хранятся видеофайлы. База ссылок на видео и сами видео-фрагменты создаются пользователем самостоятельно.<br/>Как правило, ссылки на общедоступные URL-адреса видеопотоков, распространяется намеренно",
                footer2: "правообладателями и операторами предоставляющими услуги доступа к видео потокам, последние делают это в рекламных целях надеясь на увеличение платной аудитории.<br/>Большинство операторов предоставляющих услуги доступа официально рспостроняют собственные плей- листы в формате M3U среди своих платных подписчиков.",
                footer3: "Создание ссылок не является прямым нарушением авторских прав, так-как это является описанием ресурса а не самим материалом автора.",
                footer4: "Если вам кажется что какие-либо ссылки в ваших плейлистах нарушают права правообладателя или оператора доступа - удалите их! :)",
                footer5: "Установить как приложение"
            },
            nfo: {
                none: "НЕИЗВЕСТНО",
                minutes: "мин",
                seconds: "сек",
                season: "Сезон",
                episode: "Серия",
                close: "Закрыть",
                actors: "Актеры",
                directors: "Директора",
                credits: "Помогали",
                tags: "Тэги",
                video: "ВИДЕО",
                aspect: "АСПЕКТ",
                audio: "АУДИО",
                channels: "КАНАЛЫ"
            },
            audiobox: {
                title: "Название:",
                disk: "Трек/Диск:",
                desc: "Описание:",
                copy: "Авторское право:",
                pub: "Издательство:",
                date: "Дата:",
                dur: "Длительность:",
                albums: "Альбомы:",
                artists: "Артисты:",
                comps: "Композиторы:",
                genres: "Жанры:",
                tags: "Теги:"
            },
            torrent: {
                submit: "Создать торрент",
                close: "Закрыть",
                ext: "маска файлов",
                trackers: "список трекеров",
                onedir: "только корневая директория",
                alldirs: "добавить все под-директории",
                basedir: "создание торрента из содержимого директории",
                copied: "Magnet link скопирован",
                magnet: "Копировать магнет ссылку",
                download: "Скачать торрент"
            },
            stat: {
                submit: "Показать статистику сервиса",
                close:  "Скрыть статистику сервиса",
                swstat: "статистика обработки файлов",
                swtask: "статистика запущенных задач",
                id: "№",
                last: "Обновлено",
                all: "Всего",
                error: "Ошибки",
                reject: "Отклонено",
                good: "Удачно",
                bad: "Плохие",
                dup: "Дупликаты",
                scan: "Обработка M3U файлов",
                scanvideo: "Обработка видео файлов",
                scanaudio: "Обработка аудио файлов",
                chanaudio: "Создание аудио трансляций",
                check: "Проверка всех файлов",
                download: "Обновление плей-листов",
                version: "Проверка версии",
                epg: "Обновление EPG",
                history: "Сохронение истории",
                move: "Создание точки сохранения",
                monitor: "Состояние",
                type: "Тип задачи",
                period: "Период запуска",
                due: "Время последнего запуска",
                status: "Статус"
            },
            default: null
        },
        default: null
    };
};

jQuery.fn.localize = function () {
    return this.each(function () {
        let this_ = $(this);
        let tag = this_.data('localize');
        this_.html(AppLanguage.GetLanguageKey(tag));
    });
};
if (!String.prototype.isEmpty) {
    String.prototype.isEmpty = function () {
        'use strict';
        return (!this || this.length === 0 || !this.trim());
    };
}
if (!String.prototype.searchLowerCase) {
    String.prototype.searchLowerCase = function (tag) {
        'use strict';
        if (tag.length > this.length) {
            return false;
        } else {
            return this.toLowerCase().indexOf(tag, 0) !== -1;
        }
    };
}
