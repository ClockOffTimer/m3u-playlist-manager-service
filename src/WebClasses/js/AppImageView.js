﻿
class ImageView extends AppUtils {
	constructor(ins, itmpl, ictrl, clz) {
		super();
		this.$ = ins;
		this.index = 0;
		this.activeid = 0;
		this.clz = clz;
		this.ele = { tmpls: itmpl, eles: [] };
		this.data = { files: [] };
		const this_ = this;
		this.TemplateHelpers = {
			getid: function (val) {
				return this_.activeid == parseInt(val);
			},
			getcount: function () {
				return this_.index++;
			},
			geturl: function (val) {
				return this_.clz[1].dataNextUrl(val, false);
			}
		};
		for (let i of ictrl) {
			this.ele.eles.push(this.$(i));
		}
		this.ele.eles[3].on('click', function () {
			this_.defaultSet();
		});
	};
	check(val) {
		return super.checkFileIsImage(val);
	};
	defaultSet() {
		try {
			this.ele.eles[2].fadeOut();
			this.data = { files: [] };
			this.index = 0;
			this.activeid = 0;
		} catch (a) { console.log(a); }
	};
	refresh() {
		var tmpla = this.$.templates(this.ele.tmpls[0]);
		var tmplb = this.$.templates(this.ele.tmpls[1]);
		this.ele.eles[0].html(tmpla.render(this.data, this.TemplateHelpers));
		this.ele.eles[1].html(tmplb.render(this.data, this.TemplateHelpers));
		this.ele.eles[2].fadeIn();
	};
	build(ids) {
		try {
			this.index = 0;
			this.activeid = parseInt(ids);
			this.data.files = this.clz[0].getImages();
			if (this.data.files.length > 0)
				this.refresh();
		} catch (a) { console.log(a); }
	};
};
