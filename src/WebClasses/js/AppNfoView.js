﻿class NfoView extends AppUtils {
	constructor(ins, tmpln, elen) {
		super();
		this.$ = ins;
		this.eles = { tmpl: tmpln, ele: elen[0], root: elen[1], closed: elen[2] };
		this.states = { typeNone: 0, typeShow: 1, typeEpisode: 2, typeMovie: 3 };
		this.state;
		this.data;
		this.defaultSet();
		const this_ = this;
		this.TemplateHelpers = {
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMD();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			},
			isint: function(i) {
				return (i !== null) && i > 0;
			},
			isint2: function(i,n) {
				return ((i !== null) && i > 0) && ((n !== null) && n > 0);
			},
			isnotlast: function(i, n) {
				return i < (n - 1);
			},
			isstr: function(str) {
				return !((str === null) || (typeof (str) != 'string') || (str.length === 0));
			},
			isarray: function(arr) {
				return !((arr === null) || !($.isArray(arr)) || (arr.length === 0));
			},
			isobject: function(obj, field) {
				return (obj != null) && obj.hasOwnProperty(field);
			},
			upercase: function(str) {
				return str.toUpperCase();
			},
			dburl: function(name, id) {
				if (this_.state === this_.states.typeNone)
					return '#';

				switch (name) {
					case 'imdb': {
						return `https://www.imdb.com/title/${id}/`;
					}
					case 'tmdb': {
						if (this_.state === this_.states.typeMovie)
							return `https://www.themoviedb.org/movie/${id}/`;
						else
							return `https://www.themoviedb.org/tv/${id}/`;
					}
					case 'trakt': {
						if (this_.state === this_.states.typeMovie)
							return `https://trakt.tv/movies/${id}/`;
						else
							return `https://trakt.tv/shows/${id}/`;
					}
					case 'tvdb': {
						return `https://thetvdb.com/search?query=${id}`;
					}
					case 'tvrage': {
						return `https://www.tvrage.com/all/`;
					}
					default: return '#';
				}
			}
		};
		$(document).on('click', this.eles.closed, function () {
			this_.close();
		});
	};
	defaultSet() {
		this.data = {
			actors: [],
			credits: [],
			directors: [],
			studios: [],
			genre: [],
			tags: [],
			imgs: [],
			baseid: [],
			audio: [],
			video: [],
			media: {
				titles: [],
				desc: '',
				premiered: '',
				limitation: '',
				file: '',
				runtime: 0
			},
			ratings: {
				rating: 0.0,
				userrating: 0.0,
				votes: 0
			},
			series: {
				season: 0,
				episode: 0
			}
		};
		this.state = this.states.typeNone;
	};
	get Data() {
		return this.data;
	};
	refresh() {
		var tmpl = this.$.templates(this.eles.tmpl);
		this.$(this.eles.ele).html(tmpl.render(this.data, this.TemplateHelpers));
		try {
			this.eles.ele.find('[data-localize]').localize();
		} catch (a) {
			console.log(a);
		}
		this.show();
	};
	show() {
		this.$(this.eles.root).fadeIn();
	};
	hide() {
		this.$(this.eles.root).fadeOut();
	};
	close() {
		this.hide();
		this.defaultSet();
	};
	check(val) {
		return super.checkFileIsNfo(val);
	};
	parseNormalize() {

		if (this.data.media.titles.length > 0) {
			this.data.media.titles = Array.from(new Set(this.data.media.titles));
		}
		if (this.data.imgs.length > 0) {
			this.data.imgs = Array.from(new Set(this.data.imgs));
		}
		if (this.data.video.length > 0) {
			for (let v of this.data.video) {
				if (!super.checkObject(v, 'codec'))
					v.codec = AppLanguage.GetLanguage().nfo.none;
				if (!super.checkObject(v, 'width'))
					v.width = '0';
				if (!super.checkObject(v, 'height'))
					v.height = '0';
				if (!super.checkObject(v, 'aspect'))
					v.aspect = '0.0';
				if (!super.checkObject(v, 'durationinseconds'))
					v.durationinseconds = '0';
			}
		}
		if (this.data.audio.length > 0) {
			for (let v of this.data.audio) {
				if (!super.checkObject(v, 'codec'))
					v.codec = AppLanguage.GetLanguage().nfo.none;
				if (!super.checkObject(v, 'channels'))
					v.channels = '0';
			}
		}
	}
	parseShow(dataX) {
		try {
			this.state = this.states.typeShow;
			this.parseBase(dataX);

			if (super.checkObject(dataX, 'showtitle'))
				if (!super.stringEmpty(dataX.showtitle))
					this.data.media.titles.push(dataX.showtitle);

			if (super.checkObject(dataX, 'year'))
				if (!super.stringEmpty(dataX.year))
					this.data.media.premiered = dataX.year;

			if (super.checkObject(dataX, 'tag')) {
				if ($.isArray(dataX.tag))
					this.data.tags = dataX.tag;
				else if (!super.stringEmpty(dataX.tag))
					this.data.tags.push(dataX.tag);
			}

			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	}
	parseEpisode(dataX) {
		try {
			this.state = this.states.typeEpisode;
			this.parseBase(dataX);

			if (super.checkObject(dataX, 'season'))
				if (!super.stringEmpty(dataX.season))
					this.data.series.season = parseInt(dataX.season);

			if (super.checkObject(dataX, 'episode'))
				if (!super.stringEmpty(dataX.episode))
					this.data.series.episode = parseInt(dataX.episode);

			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	};
	parseMovie(dataX) {
		try {
			this.state = this.states.typeMovie;
			this.parseBase(dataX);
			this.parseNormalize();
			this.refresh();

		} catch (a) { console.log(a); }
	};
	parseBase(dataX) {

		if (super.checkObject(dataX, 'actor')) {
			if ($.isArray(dataX.actor))
				this.data.actors = dataX.actor;
			else if (dataX.actor != null)
				this.data.actors.push(dataX.actor);
		}

		if (super.checkObject(dataX, 'credits')) {
			if ($.isArray(dataX.credits))
				this.data.credits = dataX.credits;
			else if (!super.stringEmpty(dataX.credits))
				this.data.credits.push(dataX.credits);
		}

		if (super.checkObject(dataX, 'director')) {
			if ($.isArray(dataX.director))
				this.data.directors = dataX.director;
			else if (!super.stringEmpty(dataX.director))
				this.data.directors.push(dataX.director);
		}

		if (super.checkObject(dataX, 'genre')) {
			if ($.isArray(dataX.genre))
				this.data.genre = dataX.genre;
			else if (!super.stringEmpty(dataX.genre))
				this.data.genre.push(dataX.genre);
		}

		if (super.checkObject(dataX, 'fileinfo') && super.checkObject(dataX.fileinfo, 'streamdetails')) {
			if (super.checkObject(dataX.fileinfo.streamdetails, 'video')) {
				if ($.isArray(dataX.fileinfo.streamdetails.video))
					this.data.video = dataX.fileinfo.streamdetails.video;
				else
					this.data.video.push(dataX.fileinfo.streamdetails.video);
			}
			if (super.checkObject(dataX.fileinfo.streamdetails, 'audio')) {
				if ($.isArray(dataX.fileinfo.streamdetails.audio))
					this.data.audio = dataX.fileinfo.streamdetails.audio;
				else
					this.data.audio.push(dataX.fileinfo.streamdetails.audio);
			}
		}

		if (super.checkObject(dataX, 'studio')) {
			if ($.isArray(dataX.studio))
				this.data.studios = dataX.studio;
			else if (!super.stringEmpty(dataX.studio))
				this.data.studios.push(dataX.studio);
		}

		if (super.checkObject(dataX, 'uniqueid')) {
			if ($.isArray(dataX.uniqueid)) {
				for (let s of dataX.uniqueid)
					this.data.baseid.push({ name: s._type, id: s.toString() });
			}
			else if (!super.stringEmpty(dataX.uniqueid.toString()))
				this.data.baseid.push({ name: dataX.uniqueid._type, id: dataX.uniqueid.toString() });
		}

		if (super.checkObject(dataX, 'fanart') && super.checkObject(dataX.fanart, 'thumb'))
			if (!super.stringEmpty(dataX.fanart.thumb))
				this.data.imgs.push({ url: dataX.fanart.thumb, isart: dataX.fanart.thumb.includes('artworks.') });

		if (super.checkObject(dataX, 'thumb')) {
			if ($.isArray(dataX.thumb)) {
				for (let s of dataX.thumb)
					this.data.imgs.push({ url: s.__text, isart: s.__text.includes('artworks.') });
			} else {
				if (!super.stringEmpty(dataX.thumb.__text))
					this.data.imgs.push({ url: dataX.thumb.__text, isart: dataX.thumb.__text.includes('artworks.') });
			}
		}

		if (super.checkObject(dataX, 'title'))
			if (!super.stringEmpty(dataX.title))
				this.data.media.titles.push(dataX.title);

		if (super.checkObject(dataX, 'originaltitle'))
			if (!super.stringEmpty(dataX.originaltitle))
				this.data.media.titles.push(dataX.originaltitle);

		if (super.checkObject(dataX, 'original_filename'))
			if (!super.stringEmpty(dataX.original_filename))
				this.data.media.file = dataX.original_filename;

		if (super.checkObject(dataX, 'plot'))
			if (!super.stringEmpty(dataX.plot))
				this.data.media.desc = dataX.plot;

		if (super.checkObject(dataX, 'premiered'))
			if (!super.stringEmpty(dataX.premiered))
				this.data.media.premiered = dataX.premiered;

		if (super.checkObject(dataX, 'runtime'))
			if (!super.stringEmpty(dataX.runtime))
				this.data.media.runtime = parseInt(dataX.runtime);

		if (super.checkObject(dataX, 'rating'))
			if (!super.stringEmpty(dataX.rating))
				this.data.ratings.rating = parseFloat(dataX.rating);

		if (super.checkObject(dataX, 'userrating'))
			if (!super.stringEmpty(dataX.userrating))
				this.data.ratings.userrating = parseFloat(dataX.userrating);

		if (super.checkObject(dataX, 'votes'))
			if (!super.stringEmpty(dataX.votes))
				this.data.ratings.votes = parseInt(dataX.votes);

		if (super.checkObject(dataX, 'mpaa')) {
			if (!super.stringEmpty(dataX.mpaa))
				this.data.media.limitation = dataX.mpaa;
		} else {
			if (!super.stringEmpty(dataX.certification))
				this.data.media.limitation = dataX.certification;
		}

	};
	build(dataX) {
		try {

			this.defaultSet();

			if (super.checkObject(dataX, 'movie'))
				this.parseMovie(dataX.movie);
			else if (super.checkObject(dataX, 'episodedetails'))
				this.parseEpisode(dataX.episodedetails);
			else if (super.checkObject(dataX, 'tvshow'))
				this.parseShow(dataX.tvshow);

		} catch (a) { console.log(a); }
	};
};
