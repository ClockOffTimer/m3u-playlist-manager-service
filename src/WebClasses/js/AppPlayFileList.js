﻿
class AppPlayFileList extends AppUtils {
	constructor(ins, tmpldir, tmplfiles, elen, sctrl, appurl) {
		super();
		this.$ = ins;
		this.index = 0;
		this.isreload = false;
		this.eles = { tmpld: tmpldir, tmplf: tmplfiles, ele: elen, hidehisory: sctrl[2] };
		this.data = {
			dirs: [],
			files: [],
			url: ''
		};
		this.datas = {
			dirs: [],
			files: [],
			url: ''
		};
		this.appurl = appurl;
		const this_ = this;
		this.m3utag = ['#EXTM3U', '#EXTINF', '#EXTVLCOPT'];
		this.lastSort = [false, false, false, false, false, false];
		this.TemplateHelpers = {
			isfilevideo: function (val) {
				return this_.checkFileIsVideo(val);
			},
			isfileaudio: function (val) {
				return this_.checkFileIsAudio(val);
			},
			isfilenfo: function (val) {
				return this_.checkFileIsNfo(val);
			},
			isfilem3u: function (val) {
				return this_.checkFileIsM3u(val);
			},
			isfileimage: function (val) {
				return this_.checkFileIsImage(val);
			},
			isfilexspf: function (val) {
				return this_.checkFileIsXspf(val);
			},
			isstringnotempty: function (val) {
				return !this_.stringEmpty(val);
			},
			filsize: function (val) {
				return filesize(val, { locale: 'ru' });
			},
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			}
		};
		this.$(sctrl[0]).on('click', function () {
			this_.setDataDefault();
		}).on('keyup', function () {
			const txt = $(this).val();
			if (txt.length >= 3)
				this_.search(txt);
			else if (txt.length == 0)
				this_.setDataDefault();
		});
		this.$(sctrl[1]).on('click', function () {
			const txt = this_.$(sctrl[0]).val();
			if (!this_.stringEmpty(txt))
				this_.search(txt);
			else
				this_.setDataDefault();
		});
		$(document).on('click', sctrl[3], function () {
			this_.clearHistory();
		});
	};
	static get Tag() {
		return 'playFileList';
	};
	get DataDirs() {
		return this.data.dirs;
	};
	get DataFiles() {
		return this.data.files;
	};
	get DataUrl() {
		return this.data.url;
	};
	setDataDefault() {
		this.datas.dirs = [];
		this.datas.files = [];
		this.refresh();
	};
	getImages() {
		const arr = [];
		for (let i of this.data.files) {
			if (super.checkFileIsImage(i.uri))
				arr.push(i);
		}
		return arr;
	};
	getTrackById(id) {
		id -= 1;
		if ((id >= this.data.files.length) || (id < 0))
			return ['', ''];
		this.index = id;
		let item = this.data.files[this.index++];
		return [item.uri, item.name];
	};
	getPrevTrack() {
		if (0 > this.index)
			this.index = (this.data.files.length - 1);
		let item = this.data.files[this.index--];
		return [item.uri, item.name];
	};
	getNextTrack() {
		if (this.data.files.length <= this.index)
			this.index = 0;
		let item = this.data.files[this.index++];
		return [item.uri, item.name];
	};
	sortOnce(num) {
		if (this.lastSort[num]) return false;
		for (var i = 0; i < this.lastSort.length; i++)
			if (i === num)
				this.lastSort[i] = true;
			else
				this.lastSort[i] = false;
		return true;
	};
	sort1Up() {
		if (!this.sortOnce(0))
			return;
		this.data.files = this.data.files.sort((a, b) => a.uri.localeCompare(b.uri));
		this.refresh();
	};
	sort1Down() {
		if (!this.sortOnce(1))
			return;
		this.data.files = this.data.files.sort((a, b) => b.uri.localeCompare(a.uri));
		this.refresh();
	};
	sort2Up() {
		if (!this.sortOnce(2))
			return;
		this.data.files = this.data.files.sort((a, b) => a.time !== b.time ? a.time < b.time ? -1 : 1 : 0);
		this.refresh();
	};
	sort2Down() {
		if (!this.sortOnce(3))
			return;
		this.data.files = this.data.files.sort((a, b) => b.time !== a.time ? b.time < a.time ? -1 : 1 : 0);
		this.refresh();
	};
	sort3Up() {
		if (!this.sortOnce(4))
			return;
		this.data.files = this.data.files.sort((a, b) => { let ai = parseInt(a.size), bi = parseInt(b.size); return ai !== bi ? ai < bi ? -1 : 1 : 0; });
		this.refresh();
	};
	sort3Down() {
		if (!this.sortOnce(5))
			return;
		this.data.files = this.data.files.sort((a, b) => { let ai = parseInt(a.size), bi = parseInt(b.size); return bi !== ai ? bi < ai ? -1 : 1 : 0; });
		this.refresh();
	};
	checkM3u(url) {
		return super.checkFileIsM3u(url);
	};
	checkXspf(url) {
		return super.checkFileIsXspf(url);
	};
	defaultSet() {
		this.data = { dirs: [], files: [], url: '' };
		this.refresh();
	};
	refresh() {
		var inst = this.$(this.eles.ele);
		var tmpld = this.$.templates(this.eles.tmpld);
		var tmplf = this.$.templates(this.eles.tmplf);
		const b = ((this.datas.dirs.length > 0) || (this.datas.files.length > 0));

		inst.html(tmpld.render(b ? this.datas : this.data, this.TemplateHelpers));
		inst.append(tmplf.render(b ? this.datas : this.data, this.TemplateHelpers));

		$(this.eles.hidehisory).hide();
	};
	search(tag) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (x) {

				const d = {
					dirs: [],
					files: []
				};
				const txt = tag.toLowerCase();
				for (let t of this_.data.dirs.filter(pr => pr.name.searchLowerCase(txt)))
					d.dirs.push(t);

				for (let t of this_.data.files.filter(pr => pr.name.searchLowerCase(txt)))
					d.files.push(t);

				this_.datas.dirs = d.dirs;
				this_.datas.files = d.files;
				this_.datas.dirscount = d.dirs.length;
				this_.datas.filescount = d.files.length;
				this_.datas.url = this_.data.url;
				this_.refresh();
			});
			atc.build([]);

		} catch (a) { console.log(a); }
	};
	findImageByUrl(url) {
		try {
			const d = {
				files: []
			};
			let txt = decodeURI(url);
			txt = txt.substring(txt.lastIndexOf('/') + 1);
			txt = txt.substring(0, txt.indexOf('.'));

			for (let t of this.data.files.filter(pr => pr.uri.includes(txt)))
				if (!super.stringEmpty(t.uri) && super.checkFileIsImage(t.uri))
					d.files.push(t);

			let f;
			if (d.files.length === 0)
				return null;
			else if (d.files.length === 1)
				f = d.files[0].uri;
			else
				f = d.files[Math.floor(Math.random() * d.files.length)].uri;
			return this.appurl.dataNextUrl(f.trim(), false);

		} catch (a) { console.log(a); return null; }
	};
	getEmptyItem_() {
		return {
			id: 0,
			size: 0,
			name: '',
			uri: '',
			time: '',
			ip: '',
			imgurl: '',
			isdfmt: false,
			issfmt: false,
			isip: false,
			ishistory: false
		};
	};
	setBool_(arr) {
		for (let ele of arr) {
			if (super.checkObject(ele, 'isdfmt'))
				ele.isdfmt = super.getBool(ele.isdfmt);
			else
				ele.isdfmt = false;
			if (super.checkObject(ele, 'issfmt'))
				ele.issfmt = super.getBool(ele.issfmt);
			else
				ele.issfmt = false;
			if (super.checkObject(ele, 'isip'))
				ele.isip = super.getBool(ele.isip);
			else
				ele.isip = false;
			if (super.checkObject(ele, 'ishistory'))
				ele.ishistory = super.getBool(ele.ishistory);
			else
				ele.ishistory = false;
		}
	};
	getHistory_() {
		try {
			const Store = window.localStorage;
			let json = Store.getItem(AppPlayFileList.Tag);
			if (json === null) {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);
				return null;
			}
			let arr = JSON.parse(json);
			if (!$.isArray(arr) || (arr.length === 0)) {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);
				return null;
			}
			return arr;

		} catch (a) { console.log(a); }
		return null;
	};
	clearHistory() {
		try {
			const Store = window.localStorage;
			Store.setItem(AppPlayFileList.Tag, JSON.stringify([]));
			this.data.dirs = [];
			this.data.files = [];
			this.datas.dirscount = 1;
			this.datas.filescount = 0;
			this.isreload = true;
			this.refresh();

		} catch (a) { console.log(a); }
	};
	addHistory(id) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (x) {

				let item = null;
				const Store = window.localStorage;
				for (let t of this_.data.files.filter(pr => pr.id == id)) {
					item = t;
					break;
				}
				if (item == null)
					return;
				if (this_.checkObject(item, 'ishistory') && item.ishistory)
					return;
				if (!this_.checkObject(item, 'uri') ||
					(!this_.checkFileIsVideo(item.uri) && !this_.checkFileIsAudio(item.uri)))
					return;

				let arr, idx = 0;
				let json = Store.getItem(AppPlayFileList.Tag);
				if (json !== null) {
					arr = JSON.parse(json);
					if (!$.isArray(arr)) {
						arr = [];
					} else {
						if (arr.length > 99) {
							idx = arr[0].id;
							arr.shift();
						} else {
							idx = arr.length;
						}
					}
				}
				else {
					arr = [];
				}

				let a = this_.getEmptyItem_();
				a.id = idx;
				a.ishistory = true;
				a.isdfmt = true;
				a.issfmt = item.issfmt;
				a.name = item.name;
				a.size = item.size;
				a.time = new Date().toISOString();
				a.uri = item.uri.includes('://') ? item.uri : this_.appurl.dataNextUrl(item.uri, false);
				arr.push(a);
				Store.setItem(AppPlayFileList.Tag, JSON.stringify(arr));
			});
			atc.build([]);

		} catch (a) { console.log(a); }
	};
	filterHistory(dt) {
		try {
			if (!super.checkObject(dt, 'date')) {
				this.buildHistory();
				return;
			}
			const arr = this.getHistory_();
			if ((arr === null) || !$.isArray(arr) || (arr.length === 0))
				return;

			let d = [];
			let h = (dt.date.getTimezoneOffset() < 0) ?
						(Math.abs(dt.date.getTimezoneOffset()) / 60) : (-Math.abs(dt.date.getTimezoneOffset()) / 60);
			let date = new Date(
				dt.date.getFullYear(),
				dt.date.getMonth(),
				dt.date.getDate(),
				h, 1).toISOString().split('T')[0];
			for (let t of arr.filter(pr => pr.time.startsWith(date)))
				d.push(t);

			this.data.dirs = [];
			this.data.files = d;
			this.data.dirscount = 1;
			this.data.filescount = d.length;
			this.refresh();

			if (d.length === 0)
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg6);

		} catch (a) { console.log(a); }
	};
	buildHistory() {
		try {
			const arr = this.getHistory_();
			if ((arr === null) || !$.isArray(arr) || (arr.length === 0))
				return;

			this.data.dirs = [];
			this.data.files = arr.reverse();
			this.datas.dirscount = 1;
			this.datas.filescount = arr.length;
			this.isreload = true;
			this.refresh();

		} catch (a) { console.log(a); }
	};
	updateHistory(url, duration) {
		try {
			const json = Store.getItem(AppPlayFileList.Tag);
			if (json === null)
				return;

			const arr = JSON.parse(json);
			if (!$.isArray(arr) || (arr.length === 0))
				return;

			for (let i of arr.reverse()) {
				if (url.localeCompare(i.uri) === 0) {
					i.duration = duration;
					Store.setItem(AppPlayFileList.Tag, JSON.stringify(arr));
					return;
				}
			}
		} catch (a) { console.log(a); }
	};
	buildxspfItem_(id, obj, titles, this_) {

		if (!this_.checkObject(obj, 'location'))
			return null;

		let ids = id + 1;
		let title = (titles === null) ? '' : titles;
		let album = this_.checkObject(obj, 'album') ? obj.album : `#${ids}`;
		let duration = this_.checkObject(obj, 'duration') ? Math.round((parseInt(obj.duration) / 1000) / 60) : 0;

		if (!this_.checkObject(obj, 'title')) {
			let name = this_.appurl.convertUrlToName(obj.location);
			if (name !== null)
				title += `${ids} - ${name}`;
			else
				title += (this_.checkObject(obj, 'creator') ? `${ids} - ${obj.creator}` : `# ${ids}`);
		} else {
			title += obj.title;
		}

		let a = this_.getEmptyItem_();
		a.id = id;
		a.name = title;
		a.time = album;
		a.size = `${duration} ${AppLanguage.GetLanguage().nfo.minutes}`;

		let url = obj.location.trim();
		if (url.startsWith('file://'))
			a.uri = this_.appurl.convertFileToUrl(url);
		else
			a.uri = url;
		return a;
	};
	buildxspf(url) {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (dataX) {

				if (!this_.checkObject(dataX, 'playlist'))
					return;
				if (!this_.checkObject(dataX.playlist, 'trackList') ||
					!this_.checkObject(dataX.playlist.trackList, 'track') ||
					!$.isArray(dataX.playlist.trackList.track) ||
					(dataX.playlist.trackList.track.length == 0))
					return;
				if (!this_.checkObject(dataX.playlist, 'extension'))
					return;

				const d = [];

				if (this_.checkObject(dataX.playlist.extension, 'items') &&
					$.isArray(dataX.playlist.extension.items) && (dataX.playlist.extension.items.length > 0)) {
					for (let i of dataX.playlist.extension.items) {
						try {
							let id = parseInt(i._tid);
							id = (id < 0) ? 0 : id;
							if (id >= dataX.playlist.trackList.track.length)
								continue;
							const x = dataX.playlist.trackList.track[id];
							if (x == null)
								continue;
							let a = this_.buildxspfItem_(id, x, null, this_);
							if (a != null)
								d.push(a);
						} catch (a) { console.log(a); }
					}
				}
				if (this_.checkObject(dataX.playlist.extension, 'node')) {

					let arr;
					if (this_.checkObject(dataX.playlist.extension.node, 'node') &&
						$.isArray(dataX.playlist.extension.node.node) && (dataX.playlist.extension.node.node.length > 0))
						arr = dataX.playlist.extension.node.node;
					else if ($.isArray(dataX.playlist.extension.node) && (dataX.playlist.extension.node.length > 0))
						arr = dataX.playlist.extension.node;
					else
						arr = [];

					for (let n of arr) {
						let title = (!this_.checkObject(n, '_title') || this_.stringEmpty(n._title)) ? '' : n._title + '/';

						if (this_.checkObject(n, 'item') && $.isArray(n.item) && (n.item.length > 0))
							for (let i of n.item) {
								try {
									let id = parseInt(i._tid);
									id = (id < 0) ? 0 : id;
									if (id >= dataX.playlist.trackList.track.length)
										continue;
									const x = dataX.playlist.trackList.track[id];
									if (x == null)
										continue;
									let a = this_.buildxspfItem_(id, x, title, this_);
									if (a != null)
										d.push(a);
								} catch (a) { console.log(a); }
							}
					}
				}
				if (d.length === 0) {
					for (let i = 0; i < dataX.playlist.trackList.track.length; i++) {
						try {
							const x = dataX.playlist.trackList.track[i];
							if (x == null)
								continue;
							let a = this_.buildxspfItem_(i, x, null, this_);
							if (a != null)
								d.push(a);
						} catch (a) { console.log(a); }
					}
				}
				this_.data.dirs = [];
				this_.data.files = d;
				this_.data.dirscount = 1;
				this_.data.filescount = d.length;
				this_.isreload = true;
				this_.refresh();
			});
			AppConnector.getRequest(url, [atc]);

		} catch (a) { console.log(a); }
	};
	buildm3u(url) {
		try {

			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb(function (arr) {

				const d = [];

				if (arr[0].includes(this_.m3utag[0])) {
					arr.shift();
				}
				for (let i = 0; i < arr.length; i++) {
					if (arr[i].startsWith(this_.m3utag[2]))
						continue;

					let result = arr[i].match(/:(?:(-?\d+))(?:\s(.+)|)(?:\s|),(.+)(?:\r\n|\n)(\S+:\/\/.+)(?:\r\n|\n)/m);
					let a = this_.getEmptyItem_();
					var url = '';
					var group = '';

					a.id = i;
					a.time = '';
					a.name = '';
					a.issfmt = true;

					if (result && result.length == 5) {
						url = result[4].trim() || '';
						a.size = parseInt(result[1]) || 0;
						a.name = result[3].trim() || '';

						try {
							const bregexp = /(?<tag>[a-z\-]+)\=\"(?<val>[^\"]+)\"/gs;
							let grp = bregexp.exec(result[2]);
							do {
								if (grp.groups.tag === 'group-title') {
									group = grp.groups.val || '';
								}
								if (grp.groups.tag === 'tvg-logo') {
									a.imgurl = grp.groups.val || '';
								}
							} while ((grp = bregexp.exec(result[2])) !== null);
						} catch (a) { console.log(a); }
					}
					else if (result && result.length == 4) {
						url = result[3].trim() || '';
						a.size = parseInt(result[1]) || 0;
						a.name = result[2].trim() || '';
					}
					else if (result && result.length == 3) {
						url = result[2].trim() || '';
						a.size = parseInt(result[1]) || 0;
					}
					else
						continue;

					if (this_.stringEmpty(url))
						continue;

					if (url.startsWith('file://'))
						a.uri = this_.appurl.convertFileToUrl(url);
					else
						a.uri = url;

					a.size = (a.size < 0) ? 0 : a.size;

					if (this_.stringEmpty(a.name)) {
						if (!this_.stringEmpty(group)) {
							a.name = group;
						} else {
							let name = this_.appurl.convertUrlToName(a.uri);
							if (name !== null)
								a.name = name;
							else
								a.name = '-?-';
						}
					} else {
						if (!this_.stringEmpty(group)) {
							a.time = group;
						} else {
							a.time = this_.appurl.convertUrlToHost(a.uri) || '';
						}
					}
					d.push(a);
				}
				this_.data.dirs = [];
				this_.data.files = d;
				this_.data.dirscount = 1;
				this_.data.filescount = d.length;
				this_.isreload = true;
				this_.refresh();
			});
			const xhr = $.get(url, function (rawdata) {
				if (rawdata == null)
					return;
				const arr = rawdata.split(this_.m3utag[1]);
				if (arr.length == 0)
					return;

				atc.build(arr);
			})
			.fail(function () {
				AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg2} = ${this.status}: ${this.statusText}`);
			});

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (super.checkObject(dataX, 'dirs')) {
				if ((dataX.dirs.items != null) && $.isArray(dataX.dirs.items)) {
					dataX.dirs = dataX.dirs.items;
					for (let i of dataX.dirs) {
						if (!super.checkObject(i, 'imgurl'))
							i.imgurl = '';
					}
				} else {
					const items = dataX.dirs.items;
					dataX.dirs = [];
					if (super.checkObject(items, 'uri'))
						if (items.uri != null)
							dataX.dirs.push(items);
				}
			}
			if (super.checkObject(dataX, 'files')) {
				if ((dataX.files.items != null) && $.isArray(dataX.files.items)) {
					dataX.files = dataX.files.items;
					for (let i of dataX.files) {
						if (!super.checkObject(i, 'imgurl'))
							i.imgurl = '';
					}
				} else {
					const items = dataX.files.items;
					dataX.files = [];
					if (super.checkObject(items, 'uri'))
						if (items.uri != null)
							dataX.files.push(items);
				}
			}
			this.data = dataX;
			this.isreload = false;
			this.setBool_(this.data.files);
			this.$('#dirscount').text(super.stringDirectory(dataX.dirscount));
			this.$('#filescount').text(super.stringFiles(dataX.filescount));
			this.refresh();
		} catch (a) { console.log(a); }
	};
};
