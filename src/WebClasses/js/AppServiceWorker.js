﻿class AppServiceWorker {
	static isSW_ = false;
	static isPM_ = false;
	static isNotify_ = false;
	static isInstall_ = false;
	static handleInstall_ = null;
	static eleId_ = [];

	static PwaMode_() {
		const isStandalone = window.matchMedia('(display-mode: standalone)').matches;
		if (document.referrer.startsWith('android-app://'))
			return false;
		else if (navigator.standalone || isStandalone)
			return false;
		return true;
	};
	static InstallSelector_(b, h) {
		AppServiceWorker.isInstall_ = b;
		AppServiceWorker.handleInstall_ = h;
		if (AppServiceWorker.eleId_.length > 0) {
			if (b)
				$(AppServiceWorker.eleId_[0]).show();
			else
				$(AppServiceWorker.eleId_[0]).hide();
		}
	};
	static InstallChoice_(ev) {
		if (ev === null)
			return;

		ev.userChoice.then(function (res) {
			if ((res.outcome == 'dismissed') && AppServiceWorker.PwaMode_()) {
				AppServiceWorker.InstallSelector_(true, ev);
			} else if (AppServiceWorker.isInstall_) {
				AppServiceWorker.InstallSelector_(false, null);
			}
		})
		.catch((error) => {
			console.log('App InstallChoice', error);
		});
	};
	static NotifySend(data) {
		try {
			if (!AppServiceWorker.isNotify_)
				return;
			new Notification(JSON.stringify(data));
		} catch (error) { console.log('App NotifySend', error); }
	};
	static Init(cls) {
		try {
			if (window.location.protocol === 'http:')
				return;

			AppServiceWorker.eleId_ = cls;
			AppServiceWorker.isSW_ = ('serviceWorker' in navigator);
			AppServiceWorker.isPM_ = ('PushManager' in window);
			AppServiceWorker.isNotify_ = ((typeof (self.Notification) !== 'undefined') &&
				('showNotification' in ServiceWorkerRegistration.prototype));

			if (!AppServiceWorker.isSW_)
				return;

			window.addEventListener('load', () => {
				navigator.serviceWorker.register('/data/workboxjs', { scope: '/' })
					.then(function () {
						navigator.serviceWorker.onmessage = function (evt) {
							if (evt.data !== null) {
								let msg = JSON.parse(evt.data);
								console.log('on message:', msg);

								if (msg.hasOwnProperty('type')) {
									switch (msg.type) {
										case 'offline':
											document.location.replace('/data/pwa/offline'); break;
										default: break;
									}
								}
							}
						};

						if (AppServiceWorker.isPM_ && AppServiceWorker.isNotify_) {
							switch (Notification.permission) {
								case 'blocked':
								case 'granted': break;
								default: {
									Notification.requestPermission();
									break;
								}
							}
						}
					})
					.catch((error) => { console.log('PWA ERR:', error); });
			});
			window.addEventListener('beforeinstallprompt', (ev) => {
				ev.preventDefault();
				AppServiceWorker.InstallChoice_(ev);
			});
			window.addEventListener('appinstalled', () => {
				AppServiceWorker.InstallSelector_(false, null);
			});
			window.addEventListener('online', () => {
				window.location.reload();
			});

			$(document).on('click', AppServiceWorker.eleId_[0], function () {
				try {
					if (AppServiceWorker.isInstall_ && (AppServiceWorker.handleInstall_ !== null)) {
						AppServiceWorker.handleInstall_.prompt();
						AppServiceWorker.InstallChoice_(ev);

						if (!AppServiceWorker.isInstall_)
							AppServiceWorker.NotifySend({
								title: 'M3U Плей-лист Сервис - Web приложение',
								body: 'Приложение успешно установленно',
								icon: '/data/images/logo/144'
							});
					}
				} catch (error) { console.log('App Service Worker click', error); }
			});
		} catch (error) { console.log('App Service Worker init', error); }
	};
};

