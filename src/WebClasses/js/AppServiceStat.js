﻿
class AppServiceStat extends AppUtils {
	constructor(ins, ele, ctrls, ids, clz) {
		super();
		this.$ = ins;
		this.box = this.$(ele[0]);
		this.form = this.$(ele[1]);
		this.btn = [ctrls[0], ctrls[1]];
		this.eles = {
			stat: {
				id: ids[0],
				template: ctrls[4],
				table: this.$(ctrls[2]),
				target: `${ctrls[2]} tbody`,
				url: '/cmd/command/tasks/stat'
			},
			task: {
				id: ids[1],
				template: ctrls[5],
				table: this.$(ctrls[3]),
				target: `${ctrls[3]} tbody`,
				url: '/cmd/command/tasks'
			}
		};
		this.clz = clz;
		this.isopen = false;
		const this_ = this;

		this.$(document).on('click', this_.btn[0], () => {
			try {
				if (this_.isopen)
					this_.hide();
				else
					this_.open();
			} catch (a) { console.log(a); }
		});
		this.$(document).on('click', this_.btn[1], () => {
			try {
				this_.hide();
			} catch (a) { console.log(a); }
		});
		this.TemplateHelpers = {
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';
					if (val === '0001-01-01T00:00:00')
						return AppLanguage.GetLanguage().constant.notrun;

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(this_.StringToDateConverter(val)));
				} catch (a) {
					console.log(a);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			},
			tasktickformat(val) {
				try {
					if (typeof (val) !== 'number')
						return val;
					if (val === -10000)
						return AppLanguage.GetLanguage().constant.off;
					if (val === 0)
						return AppLanguage.GetLanguage().constant.on;

					return this_.TimeSpanTicksConverter(val);
				} catch (a) {
					console.log(a);
					return AppLanguage.GetLanguage().constant.error;
				}
			},
			tasktypeformat(val) {
				if (this_.stringEmpty(val))
					return '?';
				return val.replaceAll('_', ' ');
			},
			taskiserror(val) {
				if (typeof (val) !== 'number')
					return false;
				return (val !== -1);
			},
			taskidformat: function (val) {
				if (this_.stringEmpty(val))
					return 'x';
				else if (val === '-1')
					return 'SYSTEM';
				else if (val === 'ID_SCAN')
					return AppLanguage.GetLanguage().stat.scan;
				else if (val === 'ID_SCAN_LOCAL_VIDEO')
					return AppLanguage.GetLanguage().stat.scanvideo;
				else if (val === 'ID_SCAN_LOCAL_AUDIO')
					return AppLanguage.GetLanguage().stat.scanaudio;
				else if (val === 'ID_SCAN_TRANSLATE_AUDIO')
					return AppLanguage.GetLanguage().stat.chanaudio;
				else if (val === 'ID_CHECK_MEDIA')
					return AppLanguage.GetLanguage().stat.check;
				else if (val === 'ID_STAGE_DOWNLOAD')
					return AppLanguage.GetLanguage().stat.download;
				else if (val === 'ID_STAGE_VERSION')
					return AppLanguage.GetLanguage().stat.version;
				else if (val === 'ID_RENEW_EPG')
					return AppLanguage.GetLanguage().stat.epg;
				else if (val === 'ID_SAVE_HISTORY')
					return AppLanguage.GetLanguage().stat.history;
				else if (val === 'ID_STAGE_MOVE')
					return AppLanguage.GetLanguage().stat.move;
				else
					return val;
			},
			stateidformat: function (val) {
				if (this_.stringEmpty(val))
					return 'x';
				else if (val === 'Scan')
					return AppLanguage.GetLanguage().stat.scan;
				else if (val === 'ScanLocalVideo')
					return AppLanguage.GetLanguage().stat.scanvideo;
				else if (val === 'ScanLocalAudio')
					return AppLanguage.GetLanguage().stat.scanaudio;
				else if (val === 'ScanTranslateAudio')
					return AppLanguage.GetLanguage().stat.chanaudio;
				else if (val === 'Check')
					return AppLanguage.GetLanguage().stat.check;
				else
					return val;
			},
			statevalueformat: function (item, index, items) {
				if (item === null)
					return 'x';
				else if (typeof (item) === 'number') {
					return (item == 0) ? '-' : `${item}`;
				}
				return item;
			}
		};
	};
	sendRequest_(url) {
		try {
			const this_ = this;
			const xhr = new XMLHttpRequest();
			xhr.overrideMimeType('text/xml');
			xhr.responseType = 'document';
			xhr.withCredentials = true;
			xhr.open('GET', url, true);

			const onload_ = (ev) => {
				if ((ev.status != 0) && (ev.status != 200)) {
					AppConnector.RequestOnError_(
						ev,
						AppLanguage.GetLanguage().warnings.msg2,
						AppLanguage.GetLanguage().warnings.msg9
					);
					this_.hide();
				}
				else {
					AppConnector.RequestOnLoad_(xhr, [this]);
				}
			};
			const onerror_ = (ev) => {
				AppConnector.RequestOnError_(
					ev,
					null,
					AppLanguage.GetLanguage().warnings.msg9
				);
				this_.hide();
			};

			xhr.onload = () => { onload_(xhr); };
			xhr.onerror = () => { onerror_(xhr); };
			xhr.send();

		} catch (a) {
			AppConnector.showError(`${AppLanguage.GetLanguage().warnings.msg4}: ${a}`);
			this.hide();
		}
	};
	btnState_() {
		const ele = this.$(this.btn[0]);
		ele.find('.btn-text').html(
			this.isopen ? AppLanguage.GetLanguage().stat.close :
				AppLanguage.GetLanguage().stat.submit);
		ele.toggleClass('btn-outline-success btn-warning');
	};
	castStatItems_(data) {
		for (let ele of data.items) {
			if (super.checkObject(ele, 'stats')) {
				let values = [];
				for (let i of ele.stats) {
					values.push(parseInt(i));
				}
				ele.stats = values;
			} else {
				ele.stats = [ 0, 0, 0, 0, 0, 0 ];
			}
		}
		return data;
	};
	castTaskItems_(data) {
		for (let ele of data.items) {
			if (super.checkObject(ele, 'enable'))
				ele.enable = super.getBool(ele.enable);
			if (super.checkObject(ele, 'active'))
				ele.active = super.getBool(ele.active);
			if (super.checkObject(ele, 'runing'))
				ele.runing = super.getBool(ele.runing);
			if (super.checkObject(ele, 'timerlastchange'))
				ele.timerlastchange = super.getBool(ele.timerlastchange);

			if (super.checkObject(ele, 'id'))
				ele.id = ele.id.toString();

			if (super.checkObject(ele, 'periodrun'))
				ele.periodrun = parseInt(ele.periodrun);
			if (super.checkObject(ele, 'duerun'))
				ele.duerun = parseInt(ele.duerun);
			if (super.checkObject(ele, 'dayofweekrun'))
				ele.dayofweekrun = parseInt(ele.dayofweekrun);
			if (super.checkObject(ele, 'lasterror') && super.checkObject(ele.lasterror, 'errorcode'))
				ele.lasterror.errorcode = parseInt(ele.lasterror.errorcode);
		}
		return data;
	};
	defaultSet() {
		this.$(this.eles.stat.target).empty();
		this.$(this.eles.task.target).empty();
		this.eles.stat.table.hide();
		this.eles.task.table.hide();
	};
	refresh() {
	};
	show() {
		this.isopen = true;
		this.box.fadeIn();
		this.btnState_();
	};
	hide() {
		this.isopen = false;
		this.box.fadeOut();
		this.btnState_();
	};
	open() {
		try {
			const this_ = this;
			this.defaultSet();
			this.form.find('input, checkbox, checked').each(function () {
				const ele = $(this);
				if (ele.is(':checked')) {
					const val = ele.prop('id');

					if (!this_.stringEmpty(val)) {
						if (val == this_.eles.stat.id) {
							this_.eles.stat.table.show();
							this_.sendRequest_(this_.clz[1].dataRawUrl(this_.eles.stat.url));
						}
						else if (val == this_.eles.task.id) {
							this_.eles.task.table.show();
							this_.sendRequest_(this_.clz[1].dataRawUrl(this_.eles.task.url));
						}
					}
				}
			});
			this.show();

		} catch (a) { console.log(a); }
	};
	build(dataX) {
		try {
			if (super.checkObject(dataX, 'ProcessStatsXml') &&
				super.checkObject(dataX.ProcessStatsXml, 'items')) {

				let data = { items: [] };
				if (this.$.isArray(dataX.ProcessStatsXml.items))
					data.items = dataX.ProcessStatsXml.items;
				else
					data.items = [dataX.ProcessStatsXml.items];

				if (data.items.length > 0) {
					const tmpl = this.$.templates(this.eles.stat.template);
					this.$(this.eles.stat.target).html(tmpl.render(this.castStatItems_(data), this.TemplateHelpers));
				}
			}
			else if (super.checkObject(dataX, 'SheduleItemsLite') &&
					 super.checkObject(dataX.SheduleItemsLite, 'items')) {

				let data = { items: [] };
				if (this.$.isArray(dataX.SheduleItemsLite.items))
					data.items = dataX.SheduleItemsLite.items;
				else
					data.items = [dataX.SheduleItemsLite.items];

				if (data.items.length > 0) {
					const tmpl = this.$.templates(this.eles.task.template);
					this.$(this.eles.task.target).html(tmpl.render(this.castTaskItems_(data), this.TemplateHelpers));
				}
			}

		} catch (a) { console.log(a); }
	};
};
