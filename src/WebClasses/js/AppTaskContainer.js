class AppTaskContainer {
	constructor() {
		this.cb = $.Callbacks();
		this.data = {};
	};
	get Data() {
		return this.data;
	};
	buildCb(fun) {
		if ((fun === null) || (typeof (fun) !== 'function'))
			return;
		this.cb.add(fun);
	};
	buildsCb(funs) {
		if ((funs === null) || !$.isArray(funs))
			return;
		for (let f of funs)
			this.cb.add(f);
	};
	build(dataX) {
		try {
			this.data = dataX;
			if ("Promise" in window) {
				const this_ = this;
				const p = new window.Promise((resolve, reject) => {
					this.cb.fire(this_.data);
					resolve();
				});
				p.then(() => { });
				p.catch((error) => console.log(error));
			} else {
				AppConnector.showError(AppLanguage.GetLanguage().warnings.msg7);
				this.cb.fire(this.data);
			}
		} catch (a) { console.log(a); }
	};
};
