﻿
class AudioPlayer extends AppUtils {
	constructor(inst, player, arractrl, arrpctrl, clz) {
		super();
		this.$ = inst;
		this.playerjq = this.$(player);
		this.clz = clz;
		this.eles = { tmpld: arrpctrl[8], tmplw: arrpctrl[9], ele: null };
		this.actrl = [];
		this.pctrl = [];
		this.playurl = null;
		this.playname = '';
		this.isplay = false;
		this.isspeech = true;
		this.isinfobox = false;
		const playNextBind = this.playNextTrack.bind(this);
		const playPrevBind = this.playPrevTrack.bind(this);
		const playPauseBind = this.pause.bind(this);
		const playStopBind = this.stop.bind(this);
		const playRepeatOneBind = this.repeatOne.bind(this);
		const playRepeatEventBind = this.repeatEvent.bind(this);
		const playEventBind = this.ctrlEvents.bind(this);
		const playDurationBind = this.ctrlDuration.bind(this);
		const infoBoxBuildBind = this.infoBoxBuild.bind(this);
		const this_ = this;
		for (let i of arractrl) {
			this.actrl.push(this.$(i));
		}
		for (let i of arrpctrl) {
			this.pctrl.push(this.$(i));
		}
		this.eles.ele = this.pctrl[7];
		this.TemplateHelpers = {
			isfilevideo: function (val) {
				return this_.checkFileIsVideo(val);
			},
			isfileaudio: function (val) {
				return this_.checkFileIsAudio(val);
			},
			isint: function (val) {
				return ((typeof (val) === 'number') && (val > -1));
			},
			isstring: function (val) {
				return ((typeof (val) === 'string') && !this_.stringEmpty(val));
			},
			isarray: function (val) {
				return ((this_.$.isArray(val)) && (val.length > 0));
			},
			datestring: function (val) {
				try {
					if (this_.stringEmpty(val))
						return '';

					let formater = AppLanguage.GetDateFormaterWYMDHM();
					if (typeof (formater.format) === 'undefined')
						return val;
					return formater.format(new Date(val));
				} catch (error) {
					console.log(error);
					return (this_.stringEmpty(val)) ? '' : val;
				}
			}
		};
		this.playerjq.on('ended', () => {
			playRepeatEventBind();
		});
		this.playerjq.bind('timeupdate', () => {
			new Promise(resolve => {
				playEventBind(
					isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration),
					isNaN(this_.playerjs.currentTime) ? -1 : parseInt(this_.playerjs.currentTime)
				);
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.playerjq.bind('durationchange progress', () => {
			new Promise(resolve => {
				this_.pctrl[2].text(
					playDurationBind(
						isNaN(this_.playerjs.duration) ? -1 : parseInt(this_.playerjs.duration)));
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.pctrl[10].on('click', (ev) => {
			new Promise(resolve => {
				const [n, p] = this_.getProgressPosition(
					this_.pctrl[10].get(0),
					this_.playerjs.duration,
					ev);
				this_.pctrl[11].css('width', `${p}%`).attr('aria-valuenow', p.toString());
				if (!isNaN(n))
					this_.playerjs.currentTime = n;
			})
			.then(() => { })
			.catch((a) => { console.log(a); });
		});
		this.$(document).on('click', arrpctrl[6], () => {

			if (this.isinfobox) {
				this.infoBoxView(false);
				return;
			} else {
				if (this.playurl === null)
					return;
				let tmplw = this.$.templates(this.eles.tmplw);
				this.eles.ele.html(tmplw.render({}, this.TemplateHelpers));
				this.infoBoxView(true);
				infoBoxBuildBind();
			}
		});
		this.pctrl[4].on('click', () => { playNextBind(); });
		this.pctrl[5].on('click', () => { playPrevBind(); });
		this.actrl[0].on('click', () => { playNextBind(); });
		this.actrl[1].on('click', () => { playPauseBind(); });
		this.actrl[2].on('click', () => { playStopBind(); });
		this.actrl[3].on('click', () => { playStopBind(); });
		this.actrl[4].on('click', () => { playPauseBind(); });
		this.actrl[5].on('click', () => { this.actrl[5].toggleClass('btn-ison'); });
		this.actrl[6].on('click', () => { this.actrl[6].toggleClass('btn-ison'); playRepeatOneBind(); });
		this.actrl[7].on('click', () => { this.IsSpeak = !this.IsSpeak; });
		this.IsSpeak = this.clz[2].Enable;
	};
	get playerjs() {
		return this.playerjq.get(0);
	};
	get Title() {
		return this.playname;
	};
	set Title(s) {
		if ((s === null) || (typeof (s) !== 'string'))
			return;

		let i = s.lastIndexOf('.');
		if (i > 0)
			this.playname = s.substring(0, i);
		else
			this.playname = s;

		if (super.stringEmpty(this.playname))
			this.pctrl[1].empty();
		else
			this.pctrl[1].html(this.playname);
	};
	get IsSpeak() {
		return this.isspeech;
	};
	set IsSpeak(s) {
		this.isspeech = super.getBool(s);
		if (this.isspeech)
			this.actrl[7].addClass('btn-ison');
		else
			this.actrl[7].removeClass('btn-ison');
	};
	ctrlButtonVisible(isvisible) {
		for (let i = 5; i < this.actrl.length; i++) {
			if (isvisible)
				this.actrl[i].removeClass('btndisable');
			else
				this.actrl[i].addClass('btndisable');
		}
	};
	ctrlBoxIcons_(isplays) {
		if (isplays) {
			this.actrl[4].removeClass('bi-pause-circle');
			this.actrl[4].addClass('bi-play-circle');
		} else {
			this.actrl[4].removeClass('bi-play-circle');
			this.actrl[4].addClass('bi-pause-circle');
		}
	};
	ctrlDuration(d) {
		try {
			if (d <= 0)
				return;

			const dd = new Date(d * 1000);
			let str = '';
			if (dd.getUTCHours() > 0)
				str += super.stringHour(dd.getUTCHours()) + ' ';
			if (dd.getUTCMinutes() > 0)
				str += super.stringMinute(dd.getUTCMinutes()) + ' ';
			if (dd.getUTCSeconds() > 0)
				str += super.stringSeconds(dd.getUTCSeconds());
			return str;

		} catch (a) { console.log(a); }
	};
	ctrlEvents(d, c) {
		try {
			if ((d <= -1) || (c <= -1))
				return;

			const p = Math.ceil((c / d) * 100);
			this.pctrl[11].css('width', `${p}%`).attr('aria-valuenow', p.toString());
			this.pctrl[3].text(this.ctrlDuration(c));

		} catch (a) { console.log(a); }
	};
	getEmptyInfoBox_() {
		return {
			title: '',
			desc: '',
			copy: '',
			pub: '',
			tags: '',
			date: '',
			duration: '',
			image: '',
			disk: {
				disk: -1,
				total: -1,
				track: -1,
				enable: false
			},
			albums: [],
			artists: [],
			composers: [],
			genres: []
		};
	};
	infoBoxBuild() {
		try {
			const this_ = this;
			const atc = new AppTaskContainer();
			atc.buildCb((x) => {
				try {

					if (x === null)
						return;

					const d = this_.getEmptyInfoBox_();

					if (this_.checkObject(x, 'title'))
						d.title = x.title;
					if (this_.checkObject(x, 'desc'))
						d.desc = x.desc;
					if (this_.checkObject(x, 'copy'))
						d.copy = x.copy;
					if (this_.checkObject(x, 'pub'))
						d.pub = x.pub;
					if (this_.checkObject(x, 'tags'))
						d.tags = x.tags;
					if (this_.checkObject(x, 'date'))
						d.date = x.date;
					if (this_.checkObject(x, 'duration'))
						d.duration = x.duration;

					if (this_.checkObject(x, 'disk') && (this_.$.isArray(x.disk))) {
						d.disk.disk  = x.disk[0];
						d.disk.total = x.disk[1];
						d.disk.track = x.disk[2];
						d.disk.enable = ((x.disk[0] > -1) || (x.disk[1] > -1) || (x.disk[2] > -1));
					}
					if (this_.checkObject(x, 'albums')) {
						if (this_.$.isArray(x.albums))
							d.albums = x.albums;
						else
							d.albums.push(x.albums);
					}
					if (this_.checkObject(x, 'artists')) {
						if (this_.$.isArray(x.artists))
							d.artists = x.artists;
						else
							d.artists.push(x.artists);
					}
					if (this_.checkObject(x, 'composers')) {
						if (this_.$.isArray(x.composers))
							d.composers = x.composers;
						else
							d.composers.push(x.composers);
					}
					if (this_.checkObject(x, 'genres')) {
						if (this_.$.isArray(x.genres))
							d.genres = x.genres;
						else
							d.genres.push(x.genres);
					}
					const img = this_.clz[0].findImageByUrl(this_.playurl);
					if (img !== null)
						d.image = img;

					let tmpld = this_.$.templates(this_.eles.tmpld);
					this_.eles.ele.html(tmpld.render(d, this_.TemplateHelpers));

					try {
						this_.eles.ele.find('[data-localize]').localize();
					} catch (a) {
						console.log(a);
					}

				} catch (a) { console.log(a); }
			});
			if (this.playurl != null)
				AppConnector.getRequest(
					this.clz[1].dataRawUrl(`data/tag/?src="${encodeURI(decodeURI(this.playurl))}"`), [atc]);

		} catch (a) { console.log(a); }
	};
	eventStartPlay(name) {
		try {
			try {
				let namex = name;
				if (!super.stringEmpty(namex)) {
					const arr = namex.split('/');
					if (arr.length > 0)
						namex = arr[arr.length - 1].trim();
				}
				try {
					if (!super.stringEmpty(namex))
						this.Title = decodeURI(namex);
					else if (this.Title !== name)
						this.Title = name;

				} catch (a) { console.log(a); this.Title = namex; }
			} catch (a) { console.log(a); this.Title = name; }

			this.pctrl[1].html(this.Title);
			this.pctrl[2].text(0);
			this.pctrl[11].width(0);
			this.pctrl[0].show();

		} catch (a) { console.log(a); }
	};
	repeatEvent() {
		try {
			if (!this.actrl[5].hasClass('btn-ison')) {
				this.playname = '';
				this.pctrl[0].hide();
				return;
			}
			this.playNextTrack();
		} catch (a) { console.log(a); }
	};
	repeatOne() {
		try {
			const loop = this.actrl[6].hasClass('btn-ison');
			this.playerjq.attr({
				'loop': loop
			});
		} catch (a) { console.log(a); }
	};
	stop() {
		if (this.isspeech && this.clz[2].Enable)
			this.clz[2].stop();

		this.playerjs.pause();
		this.isplay = false;
		this.playname = '';
		this.playurl = null;
		this.clz[0].index = 0;
		this.infoBoxView(false);
		this.actrl[5].removeClass('btn-ison');
		this.actrl[6].removeClass('btn-ison');
		this.pctrl[1].empty();
		this.pctrl[0].hide();
		this.ctrlBoxIcons_(true);
	}
	pause() {
		if (this.isspeech && this.clz[2].Enable) {
			const state = this.clz[2].pause();
			if (state === this.clz[2].states.Play) {
				this.ctrlBoxIcons_(true);
				return;
			}
			if (state === this.clz[2].states.Pause) {
				this.ctrlBoxIcons_(false);
				return;
			}
		}

		if (this.isplay)
			this.playerjs.pause();
		else
			this.playerjs.play();

		this.isplay = !this.isplay;
		this.ctrlBoxIcons_(this.isplay);
	};
	playNextTrack() {
		const [uri, name] = this.clz[0].getNextTrack();
		this.Title = name;
		this.playTrack(uri);
	};
	playPrevTrack() {
		const [uri, name] = this.clz[0].getPrevTrack();
		this.Title = name;
		this.playTrack(uri);
	};
	playTrack(uri) {
		if (super.stringEmpty(uri))
			return;
		const url = this.clz[1].dataNextUrl(uri, false);
		if (super.stringEmpty(url))
			return;
		this.playTrackext(url, 0);
	};
	playTrackext(url, id) {
		try {
			if (super.stringEmpty(url))
				return;

			if (id > 0) {
				const [uri, name] = this.clz[0].getTrackById(id);
				if (!super.stringEmpty(uri)) {
					if (url.endsWith(uri) && !super.stringEmpty(name))
						this.Title = name;
					else if (super.stringEmpty(this.Title))
						this.Title = uri;
				}
			}
			this.eventStartPlay((super.stringEmpty(this.Title)) ? url : this.Title);

			do {
				if (!this.isspeech || !this.clz[2].Enable)
					break;

				const state = this.clz[2].speak(this.Title, () => {
					if (!this.stringEmpty(this.Title))
						this.playTrackUrl_(url);
				});
				if (state !== this.clz[2].states.Play)
					break;
				return;
			} while (false);

			this.playTrackUrl_(url);

		} catch (a) { console.log(a); }
	};
	playTrackUrl_(url) {
		try {
			const loop = this.actrl[6].hasClass('btn-ison');
			this.playerjq.attr({
				'src': url,
				'poster': '',
				'autoplay': 'autoplay',
				'loop': loop
			});
			this.isplay = true;
			this.playurl = url;

		} catch (a) { console.log(a); this.isplay = false; }
	};
	hide() {
		this.ctrlButtonVisible(false);
		this.stop();
	};
	infoBoxView(b) {
		if (b)
			this.eles.ele.fadeIn();
		else
			this.eles.ele.fadeOut();
		this.isinfobox = b;
	};
};
