﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using PlayListServiceData.Process.Http;
using PlayListServiceData.Process.Http.Utils;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin
{
    public partial class ProxyCheckControlsDialogContent : UserControl
    {
        private const string ProxyUrl = "https://spys.me/proxy.txt";
        private const string LogTag = "Proxy Checker";
        private readonly TextRange TextWriter;
        private readonly StringBuilder ViewText_ = new(string.Empty);
        private readonly HttpClientManager HcMgr;
        private readonly ProxyListConverter Plc;
        private readonly IServiceControls ServiceCtrl;
        private ModernWpf.Controls.ContentDialog dialog;

        public string ViewText
        {
            get { return ViewText_.ToString(); }
            set
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(value))
                        return;
                    ViewText_.AppendLine(value.Trim());
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        try { TextWriter.Text = ViewText_.ToString(); } catch {}
                    });
                }
                catch (Exception e) { ViewText_.AppendLine($"{LogTag} -> {e.Message}"); }
            }
        }

        public ProxyCheckControlsDialogContent(IServiceControls ictrl, string path)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(path))
                    throw new FileFormatException("file name empty");

                ServiceCtrl = ictrl;
                InitializeComponent();
                TextWriter = new TextRange(
                    ViewBox.Document.ContentStart, ViewBox.Document.ContentEnd);
                DataContext = this;

                HcMgr = new HttpClientManager
                {
                    CheckFileOut = new FileInfo(path)
                };
                HcMgr.EventCb += ToLog;
                Plc = new ProxyListConverter(new Uri(ProxyUrl), HcMgr.CheckFileOut);
                Plc.EventCb += ToLog;
            }
            catch (Exception e)
            {
                ViewText = $"{LogTag} -> {e.Message}";
                try { HcMgr.Dispose(); } catch { }
                throw;
            }
        }

        private void ToLog(char c, string s, Exception a)
        {
            if (a == default)
                ViewText = $"[{c}] {s.Trim()}";
            else if (s == default)
                ViewText = $"[{c}] {a?.Message}";
            else
                ViewText = $"[{c}] {s.Trim()} -> {a?.Message}";
        }

        private async Task ProxyAddRunner(List<string> lst)
        {
            bool b = (lst == default);
            if (b)
                _ = await Plc.UrlConvertAsync().ConfigureAwait(false);

            List<string> list = (b) ? File.ReadAllLines(HcMgr.CheckFileOut.FullName).ToList() : lst;

            if (ServiceCtrl.ControlConfig.IsConfigNetHttpProxy)
                await HcMgr.ProxyHttpAddRange(list.Distinct()).ConfigureAwait(false);
            else if (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy)
                await HcMgr.ProxySocks5AddRange(list.Distinct()).ConfigureAwait(false);
            else
                throw new TypeLoadException(Properties.ResourcesSettings.S2);

            ViewText = $"{LogTag} -> {Properties.ResourcesSettings.S5}";

            Application.Current.Dispatcher.Invoke(() =>
            {
                dialog.IsSecondaryButtonEnabled = true;
                dialog.DefaultButton = ModernWpf.Controls.ContentDialogButton.Secondary;
            });
        }

        public async Task<bool> Show(ModernWpf.Controls.ContentDialog d, List<string> list)
        {
            try
            {
                dialog = d;
                if (list != default)
                    dialog.Title = $"{dialog.Title.ToString()}: {list.Count}";
                dialog.PrimaryButtonClick += (async (s, a) =>
                {
                    a.Cancel = true;
                    dialog.IsPrimaryButtonEnabled = false;
                    try { await ProxyAddRunner(list); }
                    catch (Exception e) { ViewText = $"{LogTag} -> {e.Message}"; }
                });
                var r = await dialog.ShowAsync().ConfigureAwait(false);
                return r switch
                {
                    ModernWpf.Controls.ContentDialogResult.Secondary => true,
                    _ => false
                };
            }
            catch (Exception e)
            {
                ViewText = $"{LogTag} -> {e.Message}";
                throw;
            }
            finally
            {
                try { HcMgr.Dispose(); } catch { }
            }
        }
    }
}
