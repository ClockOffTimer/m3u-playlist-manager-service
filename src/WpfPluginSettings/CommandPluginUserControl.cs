﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginUserControl
	{
		public static readonly RoutedUICommand CmdDelete = new RoutedUICommand
			(
				"",
				"CmdDelete",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand CmdClear = new RoutedUICommand
			(
				"",
				"CmdClear",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand CmdRestore = new RoutedUICommand
			(
				"",
				"CmdRestore",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand CmdHelp = new RoutedUICommand
			(
				"",
				"CmdHelp",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
	}
}
