﻿/* Copyright (c) 2021-2022 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfPlugin
{
    public partial class TorrentTrackersDownloadDialogContent : UserControl, INotifyPropertyChanged
    {
        private static readonly string BaseUrl = @"https://raw.githubusercontent.com/ngosang/trackerslist/master/";
        private static readonly string[] Urls = new string[]
        {
            string.Empty,
            "trackers_all_http.txt",
            "trackers_all_https.txt",
            "trackers_all_ip.txt",
            "trackers_all_udp.txt",
            "trackers_best_ip.txt",
            "trackers_best.txt"
        };
        private ModernWpf.Controls.ContentDialog Dialog = default;
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name) =>
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));

        private string ErrorText_ = string.Empty;
        public string ErrorText
        {
            get { return ErrorText_; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    return;
                ErrorText_ = value;
                OnPropertyChanged(nameof(ErrorText));
            }
        }
        private int ControlListIndex_ = 0;
        public int ControlListIndex
        {
            get { return ControlListIndex_; }
            set
            {
                if ((ControlListIndex_ == value) || (value < 0) || (value >= Urls.Length))
                    return;

                if (Dialog != default)
                    Dialog.IsPrimaryButtonEnabled = value > 0;

                ControlListIndex_ = value;
                OnPropertyChanged(nameof(ControlListIndex));
            }
        }
        public Uri GetDownloadUrl
        {
            get => ControlListIndex == 0 ? default :
                new Uri($"{BaseUrl}{Urls[ControlListIndex]}");
        }

        public TorrentTrackersDownloadDialogContent()
        {
            try {
                InitializeComponent();
                DataContext = this;
            }
            catch (Exception e) { ErrorText = e.Message; }
        }

        public async Task<Uri> Show(ModernWpf.Controls.ContentDialog d)
        {
            try
            {
                Dialog = d;
                Dialog.Title = this.Tag;
                Dialog.IsPrimaryButtonEnabled = false;
                Dialog.PrimaryButtonClick += (s, a) => a.Cancel = false;
                var r = await Dialog.ShowAsync().ConfigureAwait(false);
                return r switch
                {
                    ModernWpf.Controls.ContentDialogResult.Primary => GetDownloadUrl,
                    _ => default
                };
            }
            catch (Exception e) { Application.Current.Dispatcher.Invoke(() => ErrorText = e.Message); }
            return default;
        }
    }
}
