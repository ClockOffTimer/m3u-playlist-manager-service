﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;

namespace WpfPlugin.Data
{
    public class PageUserControlData : INotifyPropertyChanged
    {
        private Lazy<Regex> validator = default;
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        #region Get/Set Property
        private string Errors_ = string.Empty;
        public string Errors
        {
            get { return Errors_; }
            set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    Errors_ = value;
                    OnPropertyChanged(nameof(Errors));
                });
            }
        }
        private string SelectedItem_ = string.Empty;
        public string SelectedItem
        {
            get { return SelectedItem_; }
            set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    SelectedItem_ = value;
                    OnPropertyChanged(nameof(SelectedItem));
                });
            }
        }
        private bool IsBtnAllEnable_ = true;
        public bool IsBtnAllEnable
        {
            get { return IsBtnAllEnable_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    IsBtnAllEnable_ = value;
                    OnPropertyChanged(nameof(IsBtnAllEnable));
                });
            }
        }
        private bool IsBtnDownloadEnable_ = false;
        public bool IsBtnDownloadEnable
        {
            get { return IsBtnDownloadEnable_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    IsBtnDownloadEnable_ = value;
                    OnPropertyChanged(nameof(IsBtnDownloadEnable));
                });
            }
        }
        private bool IsEditable_ = true;
        public bool IsEditable
        {
            get { return IsEditable_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    IsEditable_ = value;
                    OnPropertyChanged(nameof(IsEditable));
                });
            }
        }
        private Func<List<string>> FunBtnHelpCb_ = () => { return default; };
        public Func<List<string>> FunBtnHelpCb
        {
            get { return FunBtnHelpCb_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    FunBtnHelpCb_ = value;
                    IsBtnDownloadEnable = true;
                    OnPropertyChanged(nameof(FunBtnHelpCb));
                });
            }
        }
        private Func<List<string>> FunBtnReloadCb_ = () => { return default; };
        public Func<List<string>> FunBtnReloadCb
        {
            get { return FunBtnReloadCb_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    FunBtnReloadCb_ = value;
                    OnPropertyChanged(nameof(FunBtnReloadCb));
                });
            }
        }
        private Func<string,List<string>,string> FunValidCb_ = (s,l) => { return s; };
        public Func<string,List<string>,string> FunValidCb
        {
            get { return FunValidCb_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    FunValidCb_ = value;
                    OnPropertyChanged(nameof(FunValidCb));
                });
            }
        }
        private ObservableCollection<string> Items_ = new();
        public ObservableCollection<string> Items
        {
            get { return Items_; }
            private set
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    Items_ = value;
                    OnPropertyChanged(nameof(Items));
                });
            }
        }
        public List<string> BackupItems { get; private set; } = default;
        #endregion

        public PageUserControlData(bool b = true) { IsEditable = b; }
        public void SetConfig(
            /* button HELP call  */ Func<List<string>> fund,
            /* button RELOAD call*/ Func<List<string>> funb,
            /* VALIDATE call     */ Func<string, List<string>, string> funv,
            /* INIT call         */ Func<List<string>> init = default)
        {
            SetConfig(default, string.Empty, fund, funb, funv, init);
        }
        public void SetConfig(
            /* source collection */ List<string> list,
            /* selected item     */ string selected,
            /* button HELP call  */ Func<List<string>> fund,
            /* button RELOAD call*/ Func<List<string>> funb,
            /* VALIDATE call     */ Func<string, List<string>, string> funv,
            /* INIT call         */ Func<List<string>> init = default)
        {
            try
            {
                SelectedItem = selected;

                if (fund != default)
                    FunBtnHelpCb = fund;
                if (funb != default)
                    FunBtnReloadCb = funb;
                if (funv != default)
                    FunValidCb = funv;
                if (list == default)
                {
                    if (init == default)
                        BackupItems = new List<string>();
                    else
                    {
                        BackupItems = init.Invoke();
                        if (BackupItems == default)
                            BackupItems = new List<string>();
                    }
                }
                else
                    BackupItems = list;
                if (BackupItems.Count > 0)
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        foreach (var s in BackupItems)
                            Items.Add(s);
                        OnPropertyChanged(nameof(Items));
                    });
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(SetConfig)}: {e}");  }
        }
        public List<string> GetDataFromFile(Func<string> fun)
        {
            try
            {
                string path = fun.Invoke();
                if (string.IsNullOrWhiteSpace(path))
                    return default;

                string [] arr = File.ReadAllLines(path);
                if ((arr == default) || (arr.Length == 0))
                {
                    Errors = Properties.ResourcesSettings.S1;
                    return default;
                }
                string[] filter = (from i in arr
                                   where !string.IsNullOrWhiteSpace(i)
                                   select i.Trim()).ToArray();
                if (filter.Length == 0)
                    return default;

                List<string> list = new(filter.Distinct());
                SelectedItem = list[0];
                return list;
            } catch (Exception e) {
                Debug.WriteLine($"{this.GetType().Name} {nameof(GetDataFromFile)}: {e}");
                Application.Current.Dispatcher.Invoke(() => Errors = e.Message);
            }
            return default;
        }
        public List<string> GetDataFromDialog(Func<string> fun)
        {
            try
            {
                string s = fun.Invoke();
                if (string.IsNullOrWhiteSpace(s))
                    return default;
                SelectedItem = s;
                return new List<string>(new string[] { s });
            } catch (Exception e) {
                Debug.WriteLine($"{this.GetType().Name} {nameof(GetDataFromDialog)}: {e}");
                Application.Current.Dispatcher.Invoke(() => Errors = e.Message);
            }
            return default;
        }
        public string ValidateRegex(string s, List<string> list, string rx)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(s) || list.Contains(s))
                    return default;

                if (validator == default)
                    validator = new Lazy<Regex>(() =>
                        new Regex(rx, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Compiled |
                                      RegexOptions.IgnorePatternWhitespace | RegexOptions.CultureInvariant),
                        LazyThreadSafetyMode.PublicationOnly);

                Match m = validator.Value.Match(s);
                if (m.Success)
                    return s.Trim();
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(ValidateRegex)}: {e}"); }
            return default;
        }
        public List<string> RestoreBackup()
        {
            try
            {
                if (BackupItems == default)
                    return default;
                List<string> list = new(BackupItems.ToArray());
                SelectedItem = (list.Count > 0) ? list[0] : string.Empty;
                return list;
            } catch (Exception e) {
                Debug.WriteLine($"{this.GetType().Name} {nameof(RestoreBackup)}: {e}");
                Application.Current.Dispatcher.Invoke(() => Errors = e.Message);
            }
            return default;
        }
        public void SetNewBackup(List<string> list)
        {
            try
            {
                if (BackupItems == default)
                    Application.Current.Dispatcher.Invoke(() => { BackupItems = new List<string>(); });
                else
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        BackupItems.Clear();
                        OnPropertyChanged(nameof(BackupItems));
                    });

                Application.Current.Dispatcher.Invoke(() =>
                {
                    try
                    {
                        Errors = string.Empty;
                        Items.Clear();
                        foreach (var s in list)
                        {
                            Items.Add(s);
                            BackupItems.Add(s);
                        }
                        OnPropertyChanged(nameof(Items));
                        OnPropertyChanged(nameof(BackupItems));
                        SelectedItem = list[0];
                    }
                    catch (Exception e)
                    {
                        Application.Current.Dispatcher.Invoke(() => { Errors = e.Message; });
                        Debug.WriteLine($"{this.GetType().Name} {nameof(SetNewBackup)}: {e}");
                    }
                });
            }
            catch (Exception e)
            {
                Application.Current.Dispatcher.Invoke(() => { Errors = e.Message; });
                Debug.WriteLine($"{this.GetType().Name} {nameof(SetNewBackup)}: {e}");
            }
        }
        public void SetErrors(string s) => Errors = s;
        public void SetSelected(string s) => SelectedItem = s;
        public void SetEditable(bool b) => IsEditable = b;
        public void SetAllBtnEnable(bool b) => IsBtnAllEnable = b;
        public void SetFunCb(Func<List<string>> f) => FunBtnReloadCb = f;
        public void SetItems(ObservableCollection<string> i) => Items = i;
        public List<string> GetList => ((Items == default) || (Items.Count == 0)) ?
                    new List<string>() : new List<string>(Items.ToArray());
        public string GetSelected => SelectedItem;
    }
}
