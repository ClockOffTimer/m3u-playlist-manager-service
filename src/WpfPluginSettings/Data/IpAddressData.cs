﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.ComponentModel;

namespace WpfPlugin.Data
{
    public class IpAddressData : INotifyPropertyChanged
    {
        public string ip { get; private set; } = default;
        public string name { get; private set; } = default;
        public int selected { get; set; } = -1;

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public IpAddressData(string ip_, string name_)
        {
            ip = ip_;
            name = string.IsNullOrEmpty(name_) ? default : "(" + name_ + ")";
            OnPropertyChanged(nameof(ip));
            OnPropertyChanged(nameof(name));
        }
    }
}
