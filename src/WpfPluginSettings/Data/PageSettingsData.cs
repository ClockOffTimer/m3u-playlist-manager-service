﻿/* Copyright (c) 2021-2022 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using PlayListServiceData.Base;
using PlayListServiceData.Certificate;
using PlayListServiceData.Process.Http;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin.Data
{
    public class PageSettingsData : INotifyPropertyChanged
    {
        public ObservableCollection<IpAddressData> ControlNetAddresses { get; private set; }
            = new ObservableCollection<IpAddressData>();
        public ObservableCollection<string> ControlEpgUrls { get; private set; }
            = new ObservableCollection<string>();
        private IpAddressData ControlNetAddress_ = new(default, default);
        private bool IsControlConfigLoaded_ = default,
                     IsControlConfigChange_ = default,
                     IsControlServiceRestart_ = default;
        private int ControlNetAddressIndex_ = -1;
        private CancellationTokenSource ctoken = new();

        private bool CheckControlConfig() => (ServiceCtrl != null) && (ServiceCtrl.ControlConfig != null);
        private IServiceControls ServiceCtrl_ = default;
        public IServiceControls ServiceCtrl
        {
            get { return ServiceCtrl_; }
            private set { ServiceCtrl_ = value; OnPropertyChanged(nameof(ServiceCtrl)); }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public PageSettingsData() { }
        ~PageSettingsData()
        {
            if (ServiceCtrl != null)
                ServiceCtrl.PropertyChanged -= Local_PropertyChanged;
            if (ctoken != null)
            {
                try
                {
                    CancellationTokenSource cts = ctoken;
                    ctoken = default;
                    if (cts != default)
                    {
                        cts.Cancel();
                        Task.Delay(1000);
                        cts.Dispose();
                    }
                }
                catch { }
            }
        }

        public void SetServiceCtrl(IServiceControls controls)
        {
            ServiceCtrl = controls;
            ServiceCtrl.PropertyChanged += Local_PropertyChanged;
            BuildConfig(ctoken.Token);
            if (ServiceCtrl.IsControlConfig)
                ServiceCtrl.ControlConfig.EventCb += (a, b) =>
                {
                    Application.Current.Dispatcher.Invoke(() => {
                        IsControlConfigChange = b;
                        OnPropertyChanged(nameof(IsControlConfigChange));
                    });
                };
        }

        #region Get/Set Control

        public Func<string> GetM3uPathDialog { get; set; } = () => { return default; };
        public Func<string> GetVideoPathDialog { get; set; } = () => { return default; };
        public Func<string> GetAudioPathDialog { get; set; } = () => { return default; };
        public Func<string> GetProxyPathDialog { get; set; } = () => { return default; };
        public Func<string> GetTTrackersFileDialog { get; set; } = () => { return default; };
        public Func<List<string>,string,Task<bool>> GetProxyCheckDialog { get; set; } = async (a,s) => await Task.FromResult(false);
        public Func<Task<Uri>> GetTTrackersDownloadDialog { get; set; } = async () => await Task.FromResult(default(Uri));

        public PageUserControlData Accounts { get; private set; } = new PageUserControlData(true);
        public PageUserControlData Access { get; private set; } = new PageUserControlData(true);
        public PageUserControlData Proxys { get; private set; } = new PageUserControlData(false);
        public PageUserControlData Torrents { get; private set; } = new PageUserControlData(true);
        public PageUserControlData ExcludeM3u { get; private set; } = new PageUserControlData(false);
        public PageUserControlData ExcludeVideo { get; private set; } = new PageUserControlData(false);
        public PageUserControlData ExcludeAudio { get; private set; } = new PageUserControlData(false);

        public bool IsControlConfigLoad
        {
            get { return IsControlConfigLoaded_; }
            set
            {
                if (IsControlConfigLoaded_ == value)
                    return;
                IsControlConfigLoaded_ = value;
                OnPropertyChanged(nameof(IsControlConfigLoad));
            }
        }

        public bool IsControlConfigChange
        {
            get { return IsControlConfigChange_; }
            set
            {
                if (IsControlConfigChange_ == value)
                    return;
                IsControlConfigChange_ = value;
                OnPropertyChanged(nameof(IsControlConfigChange));
                OnPropertyChanged(nameof(IsControlServiceRestart));
                OnPropertyChanged(nameof(IsControlServiceRestartEnable));
            }
        }

        public bool IsControlServiceRestart
        {
            get { return IsControlServiceRestart_; }
            set
            {
                if (IsControlServiceRestart_ == value)
                    return;
                IsControlServiceRestart_ = CheckControlConfig() && ServiceCtrl.IsControlAdminRun && value;
                OnPropertyChanged(nameof(IsControlServiceRestart));
                OnPropertyChanged(nameof(IsControlServiceRestartEnable));
            }
        }

        public bool IsControlServiceRestartEnable
        {
            get { return CheckControlConfig() && ServiceCtrl.IsControlAdminRun && IsControlConfigChange; }
        }

        public int ControlNetAddressIndex
        {
            get { return ControlNetAddressIndex_; }
            set
            {
                if (ControlNetAddressIndex_ == value)
                    return;
                ControlNetAddressIndex_ = value;
                OnPropertyChanged(nameof(ControlNetAddressIndex));
            }
        }

        public int ControlProxyIndex
        {
            get {
                return (!CheckControlConfig()) ? 0 :
                    (ServiceCtrl.ControlConfig.IsConfigNetHttpProxy ? 1 :
                        (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy ? 2 : 0));
            }
            set
            {
                if (!CheckControlConfig())
                    return;

                switch (value)
                {
                    case 0:
                        {
                            ServiceCtrl.ControlConfig.IsConfigNetHttpProxy =
                                ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy = false;
                            Proxys.SetAllBtnEnable(false);
                            break;
                        }
                    case 1:
                        {
                            ServiceCtrl.ControlConfig.IsConfigNetHttpProxy = true;
                            ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy = false;
                            Proxys.SetAllBtnEnable(true);
                            break;
                        }
                    case 2:
                        {
                            ServiceCtrl.ControlConfig.IsConfigNetHttpProxy = false;
                            ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy = true;
                            Proxys.SetAllBtnEnable(true);
                            break;
                        }
                }
                OnPropertyChanged(nameof(ControlProxyIndex));
            }
        }

        public int ControlTorrentsIndex
        {
            get
            {
                return (!CheckControlConfig()) ? 0 :
                    (ServiceCtrl.ControlConfig.IsConfigTorrentEnable ? 1 : 0);
            }
            set
            {
                if (!CheckControlConfig())
                    return;

                ServiceCtrl.ControlConfig.IsConfigTorrentEnable = value switch
                {
                    1 => true,
                    _ => false
                };
                OnPropertyChanged(nameof(ControlTorrentsIndex));
            }
        }

        public IpAddressData ControlNetAddress
        {
            get { return ControlNetAddress_; }
            set
            {
                if (ControlNetAddress_ == value)
                    return;
                ControlNetAddress_ = value;
                if (CheckControlConfig())
                    ServiceCtrl.ControlConfig.ConfigNetAddr = value.ip;
                OnPropertyChanged(nameof(ControlNetAddress));
                OnPropertyChanged(nameof(ControlAdditionalHTTPAddress));
            }
        }

        public string ControlAdditionalHTTPAddress
        {
            get => (!CheckControlConfig()) ? string.Empty :
                $"http://{ServiceCtrl.ControlConfig.ConfigNetAddr}:{ServiceCtrl.ControlConfig.ConfigNetPort - 1}/";
        }

        #endregion

        #region public
        public void ReadConfig()
        {
            if (ServiceCtrl != null)
                ServiceCtrl?.ReadConfig();
        }

        public async void WriteConfig()
        {
            if ((ServiceCtrl == null) || (ServiceCtrl.ControlConfig == null) || (!ServiceCtrl.ControlConfig.IsLoad))
                return;

            try
            {
                ServiceCtrl.ControlConfig.ConfigM3uDirExclude = ExcludeM3u.GetList;
                ServiceCtrl.ControlConfig.ConfigVideoDirExclude = ExcludeVideo.GetList;
                ServiceCtrl.ControlConfig.ConfigAudioDirExclude = ExcludeAudio.GetList;
                ServiceCtrl.ControlConfig.ConfigNetAccess = Access.GetList;
                ServiceCtrl.ControlConfig.FirstConfigure = false;

                if (Proxys.Items.Count > 0)
                {
                    string path = ServiceCtrl.ControlConfig.IsConfigNetHttpProxy ?
                        BasePath.GetHttpProxyListPath() : (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy ?
                            BasePath.GetSocks5ProxyListPath() : string.Empty);

                    if (!string.IsNullOrWhiteSpace(path))
                        try { File.WriteAllLines(path, Proxys.Items.ToArray()); }
                        catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)}: {e}"); }
                }
                if (Torrents.Items.Count > 0)
                {
                    ServiceCtrl.ControlConfig.IsConfigTorrentEnable = true;
                    ServiceCtrl.ControlConfig.ConfigTorrentTrackers = Torrents.Items.ToList();
                }
                else
                {
                    ServiceCtrl.ControlConfig.IsConfigTorrentEnable = false;
                    ServiceCtrl.ControlConfig.ConfigTorrentTrackers = new List<string>();
                }
                if (Accounts.Items.Count > 0)
                {
                    List<string> list = new();
                    foreach (string s in Accounts.GetList)
                    {
                        try
                        {
                            if (string.IsNullOrWhiteSpace(s))
                                continue;

                            string[] arr = s.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                            if ((arr == default) || (arr.Length != 2))
                                continue;

                            string ss = EncryptString.Encrypt($"{arr[0].Trim()}:{arr[1].Trim()}");
                            if (string.IsNullOrWhiteSpace(ss))
                                continue;

                            list.Add(ss);
                        }
                        catch (System.Security.Cryptography.CryptographicException se)
                        {
                            Accounts.SetErrors($"{Properties.ResourcesSettings.S4}: {se.Message}");
                        }
                        catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(WriteConfig)}: {e}"); }
                    }
                    ServiceCtrl.ControlConfig.ConfigUserAuth = list;
                }
                else
                {
                    ServiceCtrl.ControlConfig.ConfigUserAuth = new List<string>();
                }
                if (ServiceCtrl.ControlConfig.IsSSlChange())
                {
                    CertManager cm = new(
                        ServiceCtrl.ControlConfig.ConfigNetSslCertStore,
                        ServiceCtrl.ControlConfig.ConfigNetSslCertThumbprint,
                        ServiceCtrl.ControlConfig.ConfigNetSslDomain,
                        ServiceCtrl.ControlConfig.ConfigNetPort
                        );
                    object obj = cm.Get();
                    if (obj == default)
                        ServiceCtrl.ControlConfig.IsConfigNetSslEnable = false;
                    else
                    {
                        int port = ServiceCtrl.ControlConfig.ConfigNetOldPort;
                        string ip = ServiceCtrl.ControlConfig.ConfigNetAddr;
                        if (string.IsNullOrWhiteSpace(ip))
                            ip = "0.0.0.0";
                        if (port == 0)
                            port = ServiceCtrl.ControlConfig.ConfigNetPort;
                        if (port == 0)
                            port = 8089;

                        await cm.NetshRemove(ip, port).ConfigureAwait(false);
                        await cm.NetshAdd().ConfigureAwait(false);

                        try { ((IDisposable)obj).Dispose(); } catch { }
                    }
                }
                ServiceCtrl.WriteConfig(new Action(() => {
                    IsControlConfigChange = false;
                    OnPropertyChanged(nameof(IsControlConfigChange));
                }));
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(WriteConfig)}: {e}"); }
        }
        #endregion

        #region private
        private async void BuildConfig(CancellationToken token)
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        int count = 256000;
                        while (!CheckControlConfig())
                        {
                            if ((count-- <= 0) || token.IsCancellationRequested)
                            {
#if DEBUG
                                Debug.WriteLine($"Configuration wait false = {count}/{(ServiceCtrl != null)}");
#endif
                                return;
                            }
                            Task.Delay(750);
                            Task.Yield();
                        }

                        if (!ServiceCtrl.IsControlConfig)
                            return;

                        token.ThrowIfCancellationRequested();
                        GetEpgList();

                        token.ThrowIfCancellationRequested();
                        #region ExcludeM3u
                        ExcludeM3u.SetConfig(
                            ServiceCtrl.ControlConfig.ConfigM3uDirExclude,
                            ((ServiceCtrl.ControlConfig.ConfigM3uDirExclude != default) && (ServiceCtrl.ControlConfig.ConfigM3uDirExclude.Count > 0)) ?
                                ServiceCtrl.ControlConfig.ConfigM3uDirExclude[0] : string.Empty, default,
                            () => { return ExcludeM3u.GetDataFromDialog(GetM3uPathDialog); }, default);
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region ExcludeVideo
                        ExcludeVideo.SetConfig(
                            ServiceCtrl.ControlConfig.ConfigVideoDirExclude,
                            ((ServiceCtrl.ControlConfig.ConfigVideoDirExclude != default) && (ServiceCtrl.ControlConfig.ConfigVideoDirExclude.Count > 0)) ?
                                ServiceCtrl.ControlConfig.ConfigVideoDirExclude[0] : string.Empty, default,
                            () => { return ExcludeVideo.GetDataFromDialog(GetVideoPathDialog); }, default);
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region ExcludeAudio
                        ExcludeAudio.SetConfig(
                            ServiceCtrl.ControlConfig.ConfigAudioDirExclude,
                            ((ServiceCtrl.ControlConfig.ConfigAudioDirExclude != default) && (ServiceCtrl.ControlConfig.ConfigAudioDirExclude.Count > 0)) ?
                                ServiceCtrl.ControlConfig.ConfigAudioDirExclude[0] : string.Empty, default,
                            () => { return ExcludeAudio.GetDataFromDialog(GetAudioPathDialog); }, default);
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region Proxys
                        Proxys.SetConfig(
                             /* button HELP call  */ () =>
                             {
                                 try
                                 {
                                     if (!ServiceCtrl.ControlConfig.IsConfigNetHttpProxy &&
                                         !ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy)
                                     {
                                         Proxys.Errors = Properties.ResourcesSettings.S2;
                                         Proxys.SetAllBtnEnable(false);
                                         return default;
                                     }

                                     string path = ServiceCtrl.ControlConfig.IsConfigNetHttpProxy ?
                                         BasePath.GetHttpProxyListPath() : (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy ?
                                             BasePath.GetSocks5ProxyListPath() : string.Empty);

                                     if (string.IsNullOrWhiteSpace(path))
                                     {
                                         Proxys.Errors = Properties.ResourcesSettings.S3;
                                         return default;
                                     }
                                     GetProxyCheckDialog.Invoke(default, path).ContinueWith((t) =>
                                     {
                                         try
                                         {
                                             if (t.Exception != default)
                                             {
                                                 Debug.WriteLine(
                                                     $"{this.GetType().Name} {nameof(Proxys)} Dialog: {t.Exception.Message}");
                                                 return;
                                             }
                                             if (!t.IsCompleted)
                                             {
                                                 Debug.WriteLine(
                                                     $"{this.GetType().Name} {nameof(Proxys)} Dialog: {t.Status}");
                                                 return;
                                             }
                                             if (t.Result)
                                                 Proxys.SetNewBackup(Proxys.GetDataFromFile(() => path));
                                         }
                                         catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)} Dialog: {e}"); }
                                     }).ConfigureAwait(false);
                                 }
                                 catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)}: {e}"); }
                                 return default;
                             },
                            /* button RELOAD call */ () =>
                            {
                                try
                                {
                                    if (!ServiceCtrl.ControlConfig.IsConfigNetHttpProxy &&
                                        !ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy)
                                    {
                                        Proxys.Errors = Properties.ResourcesSettings.S2;
                                        Proxys.SetAllBtnEnable(false);
                                        return default;
                                    }

                                    List<string> list = Proxys.GetDataFromFile(GetProxyPathDialog);
                                    if ((list == default) || (list.Count == 0))
                                    {
                                        Proxys.Errors = Properties.ResourcesSettings.S1;
                                        return default;
                                    }

                                    string path = ServiceCtrl.ControlConfig.IsConfigNetHttpProxy ?
                                        BasePath.GetHttpProxyListPath() : (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy ?
                                            BasePath.GetSocks5ProxyListPath() : string.Empty);

                                    if (string.IsNullOrWhiteSpace(path))
                                    {
                                        Proxys.Errors = Properties.ResourcesSettings.S3;
                                        return default;
                                    }
                                    GetProxyCheckDialog.Invoke(list, path).ContinueWith((t) =>
                                    {
                                        try
                                        {
                                            if (t.Exception != default)
                                            {
                                                Debug.WriteLine(
                                                    $"{this.GetType().Name} {nameof(Proxys)} Dialog: {t.Exception.Message}");
                                                return;
                                            }
                                            if (!t.IsCompleted)
                                            {
                                                Debug.WriteLine(
                                                    $"{this.GetType().Name} {nameof(Proxys)} Dialog: {t.Status}");
                                                return;
                                            }
                                            if (t.Result)
                                                Proxys.SetNewBackup(Proxys.GetDataFromFile(() => path));
                                        }
                                        catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)} Dialog: {e}"); }
                                    }).ConfigureAwait(false);
                                }
                                catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)}: {e}"); }
                                return default;
                            },
                            /* validate call */ (s,l) => Proxys.ValidateRegex(s, l, @"^(?<host>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}):(?<port>\d+)$"),
                            /* init call     */ () =>
                            {
                                try
                                {
                                    if (!ServiceCtrl.ControlConfig.IsConfigNetHttpProxy &&
                                        !ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy)
                                    {
                                        Proxys.Errors = Properties.ResourcesSettings.S2;
                                        Proxys.SetAllBtnEnable(false);
                                        return default;
                                    }

                                    string path = ServiceCtrl.ControlConfig.IsConfigNetHttpProxy ?
                                        BasePath.GetHttpProxyListPath() : (ServiceCtrl.ControlConfig.IsConfigNetSocks5Proxy ?
                                            BasePath.GetSocks5ProxyListPath() : string.Empty);

                                    if (string.IsNullOrWhiteSpace(path))
                                    {
                                        Proxys.Errors = Properties.ResourcesSettings.S3;
                                        return default;
                                    }
                                    return Proxys.GetDataFromFile(() => path);
                                } catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Proxys)}: {e}"); }
                                return default;
                            });
                        OnPropertyChanged(nameof(ControlProxyIndex));
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region Access
                        Access.SetConfig(
                            ServiceCtrl.ControlConfig.ConfigNetAccess,
                            ((ServiceCtrl.ControlConfig.ConfigNetAccess != default) && (ServiceCtrl.ControlConfig.ConfigNetAccess.Count > 0)) ?
                                ServiceCtrl.ControlConfig.ConfigNetAccess[0] : string.Empty,
                            /* button HELP call  */ default,
                            /* button HELP call  */ () => { return Access.RestoreBackup(); },
                            /* validate call     */ (s, l) => Access.ValidateRegex(s, l, @"^(\d{1,3})(:?\.)|(:?[\.\/-])(\d{1,3})"));
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region Accounts
                        Accounts.SetConfig(
                            /* button HELP call  */ default,
                            /* button RELOAD call*/ () => { return Access.RestoreBackup(); },
                            /* validate call */ (s, l) =>
                            {
                                try
                                {
                                    if (string.IsNullOrWhiteSpace(s))
                                        return default;
                                    if (l.Count == 0)
                                        return s;

                                    string[] arr = s.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                    if ((arr == default) || (arr.Length != 2))
                                        return default;

                                    string acc = $"{arr[0]}:";
                                    List<string> list = (from i in l
                                                         where i.StartsWith(acc)
                                                         select i).ToList();
                                    if ((list != default) && (list.Count > 0))
                                    {
                                        Accounts.SetErrors(Properties.ResourcesSettings.S10);
                                        return default;
                                    }
                                    return s;
                                } catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Accounts)}: {e}"); }
                                return default;
                            },
                            /* init call */ () =>
                            {
                                try
                                {
                                    List<string> list = ServiceCtrl.ControlConfig.ConfigUserAuth;
                                    if ((list == default) || (list.Count == 0))
                                        return default;

                                    List<string> listout = new();

                                    foreach (string s in list)
                                    {
                                        try
                                        {
                                            if (string.IsNullOrWhiteSpace(s))
                                                continue;

                                            string ss = EncryptString.Decrypt(s);
                                            if (string.IsNullOrWhiteSpace(ss))
                                                continue;

                                            string[] arr = ss.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                                            if ((arr == default) || (arr.Length == 0))
                                                continue;

                                            if (arr.Length == 2)
                                                listout.Add($"{arr[0]}:{arr[1]}");
                                        }
                                        catch (System.Security.Cryptography.CryptographicException se) { Accounts.SetErrors($"{Properties.ResourcesSettings.S4}: {se.Message}"); }
                                        catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Accounts)}: {e}"); }
                                    }
                                    if (listout.Count > 0)
                                        Accounts.SetSelected(listout[0]);
                                    return listout;
                                }
                                catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Accounts)}: {e}"); }
                                return default;
                            });
                        #endregion

                        token.ThrowIfCancellationRequested();
                        #region Torrents
                        Torrents.SetConfig(
                             /* button HELP call  */
                             () =>
                             {
                                 try
                                 {
                                     if (!ServiceCtrl.ControlConfig.IsConfigTorrentEnable)
                                     {
                                         Torrents.Errors = Properties.ResourcesSettings.S20;
                                         Torrents.SetAllBtnEnable(false);
                                         return default;
                                     }

                                     string path = ServiceCtrl.ControlConfig.ConfigTorrentsPath;

                                     if (string.IsNullOrWhiteSpace(path))
                                     {
                                         Torrents.Errors = Properties.ResourcesSettings.S21;
                                         return default;
                                     }
                                     GetTTrackersDownloadDialog.Invoke().ContinueWith(async (t) =>
                                     {
                                         try
                                         {
                                             if (t.Exception != default)
                                             {
                                                 Debug.WriteLine(
                                                     $"{this.GetType().Name} {nameof(Torrents)} Dialog: {t.Exception.Message}");
                                                 return;
                                             }
                                             if (!t.IsCompleted)
                                             {
                                                 Debug.WriteLine(
                                                     $"{this.GetType().Name} {nameof(Torrents)} Dialog: {t.Status}");
                                                 return;
                                             }
                                             if (t.Result == default)
                                                 return;

                                             string path = $"{BasePath.GetTorrentTrackersListPath()}.download";
                                             if (File.Exists(path))
                                                 try { File.Delete(path); } catch { }

                                             await HttpClientDefault.DownloadFileAsync(t.Result, path);
                                             List<string> list = Torrents.GetDataFromFile(() => path);
                                             if ((list == default) || (list.Count == 0))
                                             {
                                                 Torrents.Errors = Properties.ResourcesSettings.S1;
                                                 try { File.Delete(path); } catch { }
                                                 return;
                                             }
                                             try {
                                                 File.Delete(BasePath.GetTorrentTrackersListPath());
                                                 File.Move(path, BasePath.GetTorrentTrackersListPath());
                                             } catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Torrents)} Delete/Move: {e}"); }
                                             Torrents.SetNewBackup(list);
                                         }
                                         catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Torrents)} Dialog: {e}"); }
                                     }).ConfigureAwait(false);
                                 }
                                 catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Torrents)}: {e}"); }
                                 return default;
                             },
                            /* button RELOAD (load trackers list dialog) call */
                            () =>
                            {
                                try
                                {
                                    if (!ServiceCtrl.ControlConfig.IsConfigTorrentEnable)
                                    {
                                        Torrents.Errors = Properties.ResourcesSettings.S20;
                                        Torrents.SetAllBtnEnable(false);
                                        return default;
                                    }
                                    if (string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigTorrentsPath))
                                    {
                                        Torrents.Errors = Properties.ResourcesSettings.S21;
                                        return default;
                                    }

                                    List<string> list = Torrents.GetDataFromFile(GetTTrackersFileDialog);
                                    if ((list == default) || (list.Count == 0))
                                    {
                                        Torrents.Errors = Properties.ResourcesSettings.S1;
                                        return default;
                                    }
                                    Torrents.SetNewBackup(list);
                                    return default;
                                }
                                catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Torrents)}: {e}"); }
                                return default;
                            },
                            /* validate call */
                            (s, l) => Torrents.ValidateRegex(s, l, @"^(?<scheme>\S{3,5}):\/\/(?<host>\S+):(?<port>\d+)/(?<uri>\S+)$"),
                            /* init call     */
                            () =>
                            {
                                try
                                {
                                    if (!ServiceCtrl.ControlConfig.IsConfigTorrentEnable)
                                    {
                                        Torrents.Errors = Properties.ResourcesSettings.S20;
                                        Torrents.SetAllBtnEnable(false);
                                        return default;
                                    }
                                    if (string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigTorrentsPath))
                                    {
                                        Torrents.Errors = Properties.ResourcesSettings.S21;
                                        return default;
                                    }
                                    if ((ServiceCtrl.ControlConfig.ConfigTorrentTrackers != null) &&
                                        (ServiceCtrl.ControlConfig.ConfigTorrentTrackers.Count > 0))
                                    {
                                        string[] ss = new string[ServiceCtrl.ControlConfig.ConfigTorrentTrackers.Count];
                                        ServiceCtrl.ControlConfig.ConfigTorrentTrackers.CopyTo(ss);
                                        return ss.ToList();
                                    }
                                }
                                catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(Torrents)}: {e}"); }
                                return default;
                            });
                        OnPropertyChanged(nameof(ControlTorrentsIndex));
                        #endregion

                        token.ThrowIfCancellationRequested();

                        if (ServiceCtrl.ControlConfig.ConfigNetPort <= 0)
                            Application.Current.Dispatcher.Invoke(() => ServiceCtrl.ControlConfig.ConfigNetPort = 8089);

                        #region Task Init IP Addresses
                        InitIPAddresses(token).ContinueWith((t) =>
                        {
                            try
                            {
                                if (t.Exception != default)
                                {
                                    Debug.WriteLine(
                                        $"{this.GetType().Name} {nameof(InitIPAddresses)} ContinueWith: {t.Exception.Message}");
                                    return;
                                }
                                if (!t.IsCompleted)
                                {
                                    Debug.WriteLine(
                                        $"{this.GetType().Name} {nameof(InitIPAddresses)} ContinueWith: {t.Status}");
                                    return;
                                }
                                if (!t.Result)
                                    return;

                                string addr = ServiceCtrl.ControlConfig.ConfigNetAddr;
                                if (string.IsNullOrWhiteSpace(addr) || (ControlNetAddresses.Count == 0))
                                    return;

                                for (int i = 0; i < ControlNetAddresses.Count; i++)
                                    if (ControlNetAddresses[i].ip.Equals(addr, StringComparison.OrdinalIgnoreCase))
                                    {
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            ControlNetAddress = ControlNetAddresses[i];
                                            ControlNetAddressIndex = i;
                                            OnPropertyChanged(nameof(ControlNetAddresses));
                                            OnPropertyChanged(nameof(ControlNetAddressIndex));
                                            OnPropertyChanged(nameof(ControlNetAddress));
                                        });
                                        break;
                                    }
                            }
                            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(InitIPAddresses)} ContinueWith: {e}"); }
                        });
                        #endregion
                    }
                    catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(BuildConfig)}: {e}"); }
                    finally
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            IsControlConfigLoaded_ = true;
                            OnPropertyChanged(nameof(IsControlConfigLoad));
                        });
                    }
                });
            } catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(BuildConfig)}: {e}"); }
        }

        #region Init IP Addresses
        private async Task<bool> InitIPAddresses(CancellationToken token)
        {
            return await Task.Run(() =>
            {
                try
                {
                    foreach (NetworkInterface iface in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        token.ThrowIfCancellationRequested();
                        IPInterfaceProperties ips = iface.GetIPProperties();
                        foreach (UnicastIPAddressInformation ip in ips.UnicastAddresses)
                        {
                            token.ThrowIfCancellationRequested();
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                var item = new IpAddressData(ip.Address.ToString(), iface.Name);
                                if (string.IsNullOrEmpty(item.ip))
                                    continue;
                                if ((!string.IsNullOrEmpty(ControlNetAddress.ip)) && item.ip.Equals(ControlNetAddress))
                                    item.selected = ControlNetAddresses.Count;

                                token.ThrowIfCancellationRequested();
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    ControlNetAddresses.Add(item);
                                    if (item.selected != -1)
                                        ControlNetAddressIndex = item.selected;
                                });
                            }
                        }
                    }
                    if (ControlNetAddresses.Count > 0)
                    {
                        Application.Current.Dispatcher.Invoke(() => { OnPropertyChanged(nameof(ControlNetAddresses)); });
                        return true;
                    }
                    return false;
                }
                catch (Exception e)
                {
                    Debug.WriteLine($"{this.GetType().Name} {nameof(InitIPAddresses)}: {e}");
                    return false;
                }
            });
        }
        #endregion

        #region Epg list
        private void GetEpgList()
        {
            if (ServiceCtrl.EpgHostGuiList != default)
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    foreach (var s in ServiceCtrl.EpgHostGuiList)
                        ControlEpgUrls.Add(s);
                });
            }
        }
        #endregion

        #region event control
        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsControlAdminRun":
                    {
                        OnPropertyChanged(nameof(IsControlConfigChange));
                        OnPropertyChanged(nameof(IsControlServiceRestart));
                        break;
                    }
                case "ControlServiceState":
                    {
                        OnPropertyChanged(nameof(IsControlServiceRestart));
                        break;
                    }
                case "ControlConfig":
                    {
                        if ((ServiceCtrl == null) || (ServiceCtrl.ControlConfig == null) || (!ServiceCtrl.ControlConfig.IsLoad))
                            return;

                        if ((ctoken != null) && ctoken.IsCancellationRequested)
                            return;

                        BuildConfig(ctoken.Token);
                        OnPropertyChanged(nameof(ControlAdditionalHTTPAddress));
                        break;
                    }
                case "ControlServicePathM3U":
                    {
                        // TODO: ...
                        break;
                    }
            }
        }
        #endregion

        #endregion

    }
}
