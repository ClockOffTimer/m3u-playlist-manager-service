﻿/* Copyright (c) 2021 WpfPluginsScript, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System.Windows.Input;

namespace WpfPlugin
{
	public static class CommandPluginSettings
	{
		public static readonly RoutedUICommand PathM3U = new RoutedUICommand
			(
				"",
				"PathM3U",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand PathVideo = new RoutedUICommand
			(
				"",
				"PathVideo",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand PathAudio = new RoutedUICommand
			(
				"",
				"PathAudio",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand PathTorrent = new RoutedUICommand
			(
				"",
				"PathTorrent",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand SaveConfig = new RoutedUICommand
			(
				"",
				"SaveConfig",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
		public static readonly RoutedUICommand CertificateView = new RoutedUICommand
			(
				"",
				"CertificateView",
				typeof(CommandPluginSettings),
				new InputGestureCollection()
				{
					new KeyGesture(Key.None) { }
				}
			);
	}
}
