﻿/* Copyright (c) 2021-2022 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using PlayListServiceData.Base;
using PlayListServiceData.Plugins;
using PlayListServiceData.ServiceConfig;
using WpfPlugin.Data;

namespace WpfPlugin
{
    [Export(typeof(IPlugin)),
        ExportMetadata("Id", 1),
        ExportMetadata("pluginType", PluginType.PLUG_PLACE_BTN)]
    public partial class PageSettings : Page, IPlugin
    {
        private IServiceControls ServiceCtrl { get; set; } = default;
        private PageSettingsData PageSettingsData { get; set; } = default;

        public int Id { get; set; } = 0;
        public new string Title => base.WindowTitle;
        public string TitleShort => base.Title;
        public IPlugin GetView => this;

        [ImportingConstructor]
        public PageSettings(IServiceControls icontrols)
        {
            ServiceCtrl = icontrols;
            InitializeComponent();
            PageSettingsData = (PageSettingsData)DataContext;
            PageSettingsData.GetM3uPathDialog   = GetM3uDialog;
            PageSettingsData.GetVideoPathDialog = GetVideoDialog;
            PageSettingsData.GetAudioPathDialog = GetAudioDialog;
            PageSettingsData.GetProxyPathDialog = GetProxyDialog;
            PageSettingsData.GetProxyCheckDialog = ProxyCheckView;
            PageSettingsData.GetTTrackersDownloadDialog = TorrentTrackersDownloadView;
            PageSettingsData.GetTTrackersFileDialog = GetTorrentTrackersDialog;
            PageSettingsData.SetServiceCtrl(icontrols);
        }

        #region private SelectFolder, ServiceRestart, ValidateTextIsNumber
        private string SelectFile(string path, string filter)
        {
            try
            {
                OpenFileDialog ofd = new()
                {
                    Filter = filter,
                    Multiselect = false,
                    RestoreDirectory = true,
                    InitialDirectory = path
                };
                if (ofd.ShowDialog() == true)
                    return ofd.FileName;
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(SelectFile)}: {e}"); }
            return default;
        }
        private string SelectFolder(string path, string filter)
        {
            try
            {
                string s = SelectFile(path, filter);
                if (string.IsNullOrWhiteSpace(s))
                    return default;

                FileInfo file = new(s);
                if ((file == default) || !file.Exists)
                    return default;

                return file.DirectoryName.EndsWith(Path.DirectorySeparatorChar.ToString()) ?
                    file.DirectoryName : $"{file.DirectoryName}{Path.DirectorySeparatorChar}";
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(SelectFolder)}: {e}"); }
            return default;
        }

        private async void ServiceRestart()
        {
            try
            {
                await Task.Run(() =>
                {
                    try
                    {
                        ServiceState state = ServiceCtrl.ControlCommand.GetServiceStatus();

                        if (state == ServiceState.Running)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                ServiceCtrl.ControlServiceState = ServiceState.StopPending;
                            });
                            ServiceCtrl.ControlCommand.StopService();
                        }

                        while ((state = ServiceCtrl.ControlCommand.GetServiceStatus()) != ServiceState.Stopped)
                        {
                            Task.Delay(1000);
                            if ((state == ServiceState.Unknown) ||
                                (state == ServiceState.NotFound))
                                break;
                        }

                        if (state == ServiceState.Stopped)
                        {
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                ServiceCtrl.ControlServiceState = ServiceState.StartPending;
                            });
                            ServiceCtrl.ControlCommand.StartService();
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                    }
                });
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(ServiceRestart)}: {e}"); }
        }

        private void ValidateTextIsNumber(object sender, TextCompositionEventArgs a)
        {
            a.Handled = !uint.TryParse(a.Text, out _);
        }

        #region Proxy Check
        private async Task<bool> ProxyCheckView(List<string> list, string path)
        {
            try
            {
                ProxyCheckControlsDialogContent cdc = new(ServiceCtrl, path);
                ModernWpf.Controls.ContentDialog dialog = new()
                {
                    Title = cdc.Tag.ToString(),
                    IsPrimaryButtonEnabled = true,
                    IsSecondaryButtonEnabled = false,
                    PrimaryButtonText = Properties.ResourcesSettings.S6,
                    SecondaryButtonText = Properties.ResourcesSettings.S7,
                    CloseButtonText = Properties.ResourcesSettings.S8,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Primary,
                    VerticalContentAlignment = VerticalAlignment.Top,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Content = cdc
                };
                return await cdc.Show(dialog, list).ConfigureAwait(false);
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(ProxyCheckView)}: {e}"); }
            return false;
        }
        #endregion

        #region Torrent trackers download
        private async Task<Uri> TorrentTrackersDownloadView()
        {
            try
            {
                TorrentTrackersDownloadDialogContent cdc = new();
                ModernWpf.Controls.ContentDialog dialog = new()
                {
                    Title = cdc.Tag.ToString(),
                    IsPrimaryButtonEnabled = true,
                    IsSecondaryButtonEnabled = false,
                    PrimaryButtonText = Properties.ResourcesSettings.S11,
                    CloseButtonText = Properties.ResourcesSettings.S8,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Close,
                    VerticalContentAlignment = VerticalAlignment.Top,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Content = cdc
                };
                return await cdc.Show(dialog).ConfigureAwait(false);
            }
            catch (Exception e) { Debug.WriteLine($"{this.GetType().Name} {nameof(TorrentTrackersDownloadView)}: {e}"); }
            return default;
        }
        #endregion

        #region Paths
        private string GetProxyDialog()
        {
            string path = Path.Combine(
                Path.GetDirectoryName(
                    Assembly.GetAssembly(typeof(PageSettings)).Location),
                    "config");
            return SelectFile(path, Properties.ResourcesSettings.ProxyDialog);
        }
        private string GetM3uDialog()
        {
            string path = (!string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigM3uPath)) ?
                ServiceCtrl.ControlConfig.ConfigM3uPath :
                Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            return SelectFolder(path, Properties.ResourcesSettings.M3UDialog);
        }
        private string GetVideoDialog()
        {
            string path = (!string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigVideoPath)) ?
                ServiceCtrl.ControlConfig.ConfigVideoPath :
                Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
            return SelectFolder(path, Properties.ResourcesSettings.VideoDialog);
        }
        private string GetAudioDialog()
        {
            string path = (!string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigAudioPath)) ?
                ServiceCtrl.ControlConfig.ConfigAudioPath :
                Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
            return SelectFolder(path, Properties.ResourcesSettings.AudioDialog);
        }
        private string GetTorrentsDialog()
        {
            string path = (!string.IsNullOrWhiteSpace(ServiceCtrl.ControlConfig.ConfigTorrentsPath)) ?
                ServiceCtrl.ControlConfig.ConfigTorrentsPath :
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            return SelectFolder(path, Properties.ResourcesSettings.TorrentDialog);
        }
        private string GetTorrentTrackersDialog()
        {
            return SelectFile(
                Path.GetDirectoryName(BasePath.GetTorrentTrackersListPath()),
                Properties.ResourcesSettings.TorrentTrackersDialog);
        }
        #endregion

        #endregion

        #region Click/Event

        #region Paths
        private void PathM3U_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ServiceCtrl.ControlConfig.ConfigM3uPath = GetM3uDialog();
        }
        private void PathVideo_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ServiceCtrl.ControlConfig.ConfigVideoPath = GetVideoDialog();
        }
        private void PathAudio_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ServiceCtrl.ControlConfig.ConfigAudioPath = GetAudioDialog();
        }
        private void PathTorrent_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            ServiceCtrl.ControlConfig.ConfigTorrentsPath = GetTorrentsDialog();
        }
        #endregion

        #region Save Config
        private void SaveConfig_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PageSettingsData.WriteConfig();
            if (PageSettingsData.IsControlServiceRestart)
                ServiceRestart();
        }
        private void SaveConfig_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = PageSettingsData != default && PageSettingsData.IsControlConfigChange;
        }
        #endregion

        #region Certificate View
        private async void CertificateView_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                CertificateControlsDialogContent cdc = new(ServiceCtrl);
                ModernWpf.Controls.ContentDialog dialog = new()
                {
                    Title = cdc.Tag.ToString(),
                    PrimaryButtonText = ServiceCtrl.ControlLanguage.DialogMessage2,
                    DefaultButton = ModernWpf.Controls.ContentDialogButton.Primary,
                    VerticalContentAlignment = VerticalAlignment.Top,
                    Content = cdc
                };
                cdc.Show();
                var _ = await dialog.ShowAsync();

            } catch { }
        }
        private void CertificateView_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ServiceCtrl.IsControlConfig && ServiceCtrl.ControlConfig.IsConfigNetSslEnable;
        }
        #endregion

        #endregion

    }
}
