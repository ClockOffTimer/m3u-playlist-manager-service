﻿/* Copyright (c) 2021 PlayListServiceSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using PlayListServiceData.Certificate;
using PlayListServiceData.ServiceConfig;

namespace WpfPlugin
{
    public partial class CertificateControlsDialogContent : UserControl
    {
        private const string LogTag = "Cert Manager";
        private readonly string LocaleName;
        private readonly TextRange TextWriter;
        private readonly CertManager CertMgr;
        private readonly StringBuilder ViewText_ = new StringBuilder(string.Empty);

        public string ViewText {
            get { return ViewText_.ToString();  }
            set {
                try
                {
                    if (string.IsNullOrWhiteSpace(value))
                        return;
                    byte[] b0 = Encoding.Default.GetBytes(value);
                    byte[] b1 = Encoding.Convert(Encoding.GetEncoding(LocaleName), Encoding.UTF8, b0);
                    ViewText_.Append(Encoding.UTF8.GetString(b1));
                    TextWriter.Text = ViewText_.ToString();
                }
                catch (Exception e) { ViewText_.Append($"{LogTag} -> {e.Message}"); }
            }
        }

        public CertificateControlsDialogContent(IServiceControls ictrl)
        {
            try
            {
                string locale = Thread.CurrentThread.CurrentUICulture.Name;
                if (locale.StartsWith("ru"))
                    LocaleName = "CP866";
                else
                    LocaleName = "CP437";

                InitializeComponent();
                TextWriter = new TextRange(
                    ViewBox.Document.ContentStart, ViewBox.Document.ContentEnd);
                DataContext = this;
                CertMgr = new CertManager(
                    ictrl.ControlConfig.ConfigNetSslCertStore,
                    ictrl.ControlConfig.ConfigNetSslCertThumbprint,
                    ictrl.ControlConfig.ConfigNetSslDomain,
                    ictrl.ControlConfig.ConfigNetPort
                    );
                CertMgr.EventCb += (s, e) =>
                {
                    ViewText = $"{LogTag} -> {s} -> {e.Message}";
                };
            } catch (Exception e) { ViewText = $"{LogTag} -> {e.Message}"; }
        }

        public async void Show()
        {
            try
            {
                string txt = await CertMgr.NetshCertificateShow().ConfigureAwait(false);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ViewText = txt;
                });

            } catch (Exception e) { ViewText = $"{LogTag} -> {e.Message}"; }
        }
    }
}
