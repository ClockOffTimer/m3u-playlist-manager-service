﻿/* Copyright (c) 2021 WpfPluginSettings, NT, MIT license
   Git: https://gitlab.com/ClockOffTimer/m3u-playlist-manager-service */

using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfPlugin
{
    internal static class DependencyPropertyConstruct
    {
        internal static DependencyProperty Build<T1, T2>(string n, Action<T2, DependencyPropertyChangedEventArgs> act, bool mode = false)
        {
            return DependencyProperty.Register(n, typeof(T1), typeof(T2),
                new FrameworkPropertyMetadata((a, e) => {
                    if (a is T2 cls)
                        act.Invoke(cls, e);
                }) {
                    BindsTwoWayByDefault = mode
                });
        }
    }

    public partial class ConfigListUserControls : UserControl, INotifyPropertyChanged
    {
        #region DependencyProperty

        #region Delete ToolTip Property
        public string DeleteToolTip
        {
            get { return (string)GetValue(DeleteToolTipProperty); }
            set { SetValue(DeleteToolTipProperty, value); }
        }
        public static readonly DependencyProperty DeleteToolTipProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(DeleteToolTip), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(DeleteToolTip)); });
            }, true);
        #endregion

        #region Clear ToolTip Property
        public string ClearToolTip
        {
            get { return (string)GetValue(ClearToolTipProperty); }
            set { SetValue(ClearToolTipProperty, value); }
        }
        public static readonly DependencyProperty ClearToolTipProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(ClearToolTip), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(ClearToolTip)); });
            }, true);
        #endregion

        #region Restore ToolTip Property
        public string RestoreToolTip
        {
            get { return (string)GetValue(RestoreToolTipProperty); }
            set { SetValue(RestoreToolTipProperty, value); }
        }
        public static readonly DependencyProperty RestoreToolTipProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(RestoreToolTip), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(RestoreToolTip)); });
            }, true);
        #endregion

        #region Help ToolTip Property
        public string HelpToolTip
        {
            get { return (string)GetValue(HelpToolTipProperty); }
            set { SetValue(HelpToolTipProperty, value); }
        }
        public static readonly DependencyProperty HelpToolTipProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(HelpToolTip), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(HelpToolTip)); });
            }, true);
        #endregion

        #region Help Uri Property
        public Uri HelpUri
        {
            get { return (Uri)GetValue(HelpUriProperty); }
            set { SetValue(HelpUriProperty, value); }
        }
        public static readonly DependencyProperty HelpUriProperty =
            DependencyPropertyConstruct.Build<Uri, ConfigListUserControls>(nameof(HelpUri), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(HelpUri)); });
            }, false);
        #endregion

        #region SelectedItem Property (string)
        public string SelectedItem
        {
            get { return (string)GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }
        public static readonly DependencyProperty SelectedItemProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(SelectedItem), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(SelectedItem)); });
            }, true);
        #endregion

        #region Errors Property (string)
        public string Errors
        {
            get { return (string)GetValue(ErrorsProperty); }
            set { SetValue(ErrorsProperty, value); }
        }
        public static readonly DependencyProperty ErrorsProperty =
            DependencyPropertyConstruct.Build<string, ConfigListUserControls>(nameof(Errors), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    if (string.IsNullOrWhiteSpace(e.NewValue as string))
                        a.IsErrors = false;
                    else
                        a.IsErrors = true;
                    a.OnPropertyChanged(nameof(Errors));
                });
            }, true);
        #endregion

        #region EditableItem Property (bool)
        public bool EditableItem
        {
            get { return (bool)GetValue(EditableItemProperty); }
            set { SetValue(EditableItemProperty, value); }
        }
        public static readonly DependencyProperty EditableItemProperty =
            DependencyPropertyConstruct.Build<bool, ConfigListUserControls>(nameof(EditableItem), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(EditableItem)); });
            }, false);
        #endregion

        #region BtnAllEnable Property (bool)
        public bool BtnAllEnable
        {
            get { return (bool)GetValue(BtnAllEnableProperty); }
            set { SetValue(BtnAllEnableProperty, value); }
        }
        public static readonly DependencyProperty BtnAllEnableProperty =
            DependencyPropertyConstruct.Build<bool, ConfigListUserControls>(nameof(BtnAllEnable), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(BtnAllEnable)); });
            }, false);
        #endregion

        #region BtnDownloadEnable Property (bool)
        public bool BtnDownloadEnable
        {
            get { return (bool)GetValue(BtnDownloadEnableProperty); }
            set { SetValue(BtnDownloadEnableProperty, value); }
        }
        public static readonly DependencyProperty BtnDownloadEnableProperty =
            DependencyPropertyConstruct.Build<bool, ConfigListUserControls>(nameof(BtnDownloadEnable), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(BtnDownloadEnable)); });
            }, false);
        #endregion

        #region BtnReloadCallBack Property (Restore/Add button <- EditableItem behavoirs)
        public Func<List<string>> BtnReloadCallBack
        {
            get { return (Func<List<string>>)GetValue(BtnCallBackProperty); }
            set { SetValue(BtnCallBackProperty, value); }
        }
        public static readonly DependencyProperty BtnCallBackProperty =
            DependencyPropertyConstruct.Build<Func<List<string>>, ConfigListUserControls>(nameof(BtnReloadCallBack), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(BtnReloadCallBack)); });
            }, false);
        #endregion

        #region BtnDownloadCallBack Property
        public Func<List<string>> BtnDownloadCallBack
        {
            get { return (Func<List<string>>)GetValue(DownloadCallBackProperty); }
            set { SetValue(DownloadCallBackProperty, value); }
        }
        public static readonly DependencyProperty DownloadCallBackProperty =
            DependencyPropertyConstruct.Build<Func<List<string>>, ConfigListUserControls>(nameof(BtnDownloadCallBack), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(BtnDownloadCallBack)); });
            }, false);
        #endregion

        #region ValidateCallBack Property
        public Func<string,List<string>,string> ValidateCallBack
        {
            get { return (Func<string,List<string>,string>)GetValue(ValidateCallBackProperty); }
            set { SetValue(ValidateCallBackProperty, value); }
        }
        public static readonly DependencyProperty ValidateCallBackProperty =
            DependencyPropertyConstruct.Build<Func<string,List<string>,string>, ConfigListUserControls>(nameof(ValidateCallBack), (a, e) =>
            {
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(ValidateCallBack)); });
            }, false);
        #endregion

        #region ItemsSource Property
        public ObservableCollection<string> ItemsSource
        {
            get { return (ObservableCollection<string>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyPropertyConstruct.Build<ObservableCollection<string>, ConfigListUserControls>(nameof(ItemsSource), (a, e) =>
            {
                if (e.NewValue is ObservableCollection<string> oc)
                    a.ComboBoxItems = oc;
                Application.Current.Dispatcher.Invoke(() => { a.OnPropertyChanged(nameof(ItemsSource)); });
            }, true);
        #endregion

        #endregion

        #region PropertyChanged event
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region Get/Set Property
        private ObservableCollection<string> ComboBoxItems_ = new ObservableCollection<string>();
        public ObservableCollection<string> ComboBoxItems
        {
            get { return ComboBoxItems_; }
            set
            {
                if (ComboBoxItems_ == value)
                    return;
                ComboBoxItems_ = value;
                OnPropertyChanged(nameof(ComboBoxItems));
            }
        }

        private int ComboBoxIndex_ = -1;
        public int ComboBoxIndex
        {
            get { return ComboBoxIndex_; }
            set { if (ComboBoxIndex_ == value)
                    return;

                if ((value >= 0) && (value < ComboBoxItems.Count))
                {
                    ComboBoxIndex_ = value;
                    SelectedItem = ComboBoxItems[value];
                    OnPropertyChanged(nameof(SelectedItem));
                }
                else
                    ComboBoxIndex_ = -1;
                OnPropertyChanged(nameof(ComboBoxIndex));
            }
        }
        private bool IsErrors_ = false;
        public bool IsErrors
        {
            get { return IsErrors_; }
            set
            {
                if (IsErrors_ == value)
                    return;
                IsErrors_ = value;
                OnPropertyChanged(nameof(IsErrors));
            }
        }
        public bool IsSearchableItem
        {
            get { return !EditableItem; }
        }
        #endregion

        public ConfigListUserControls()
        {
            InitializeComponent();
            PropertyChanged += Local_PropertyChanged;
            UcRoot.DataContext = this;
        }

        #region private methods
        private void ClearErrors()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Errors = String.Empty;
                IsErrors = false;
                OnPropertyChanged(nameof(Errors));
                OnPropertyChanged(nameof(IsErrors));
            });
        }

        private void RefreshComboBoxItem(int idx = -1)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                ComboBoxIndex = idx;
                OnPropertyChanged(nameof(ComboBoxItems));
                OnPropertyChanged(nameof(ComboBoxIndex));
            });
        }

        private int SelectComboBoxItem(string s)
        {
            if (string.IsNullOrWhiteSpace(s))
                return -1;
            try
            {
                for (int i = 0; i < ComboBoxItems.Count; i++)
                {
                    var a = ComboBoxItems[i];
                    if (s.Equals(ComboBoxItems[i]))
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            ComboBoxIndex = i;
                        });
                        return i;
                    }
                }
            }
            catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(SelectComboBoxItem)}: {ex}"); }
            return -1;
        }

        private void SetItems(List<string> list)
        {
            try
            {
                if ((list == default) || (list?.Count == 0))
                    return;
                if (ComboBoxItems == default)
                    ComboBoxItems = new ObservableCollection<string>();
                if (EditableItem)
                    Application.Current.Dispatcher.Invoke(() => { try { ComboBoxItems.Clear(); } catch { } });

                foreach (string i in list)
                {
                    string s = i;
                    if (!ComboBoxItems.Contains(s))
                    {
                        if (ValidateCallBack != default)
                        {
                            string ss = ValidateCallBack.Invoke(s, list);
                            if (string.IsNullOrWhiteSpace(ss))
                                continue;
                            if (!s.Equals(ss))
                                s = ss;
                        }
                        Application.Current.Dispatcher.Invoke(() => { try { ComboBoxItems.Add(s); } catch { } });
                    }
                }
                if (string.IsNullOrWhiteSpace(SelectedItem) && (ComboBoxItems.Count > 0))
                    Application.Current.Dispatcher.Invoke(() => { ComboBoxIndex = 0; });
            }
            catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(SetItems)}: {ex}"); }
        }
        #endregion

        #region command control
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox c)
                c.IsDropDownOpen = false;
        }

        private void ComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (!BtnAllEnable || !EditableItem || (ComboBoxItems == default))
                {
                    e.Handled = false;
                    return;
                }
                if (e.Key == Key.Return)
                {
                    if (sender is ComboBox c)
                    {
                        do
                        {
                            string s = c.Text;
                            if (string.IsNullOrWhiteSpace(s))
                                break;

                            if ((ComboBoxItems?.Count > 0) &&
                                ((from d in ComboBoxItems
                                  where (d != null) && d.Equals(s)
                                  select d).FirstOrDefault() != default))
                                break;

                            if (ValidateCallBack != default)
                            {
                                string ss = ValidateCallBack.Invoke(s, ComboBoxItems.ToList());
                                if (string.IsNullOrWhiteSpace(ss))
                                    break;
                                if (!s.Equals(ss))
                                    s = ss;
                            }
                            Application.Current.Dispatcher.Invoke(() =>
                            {
                                ComboBoxItems.Add(s);
                                ComboBoxIndex = ComboBoxItems.Count - 1;
                                SelectedItem = s;
                                Errors = string.Empty;
                            });
                            e.Handled = true;
                            return;

                        } while (false);
                        e.Handled = false;
                        Application.Current.Dispatcher.Invoke(() => { ComboBoxIndex = -1; });
                    }
                }
            } catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(ComboBox_KeyDown)}: {ex}"); }
        }

        private void CmdDelete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if ((ComboBoxItems == default) || (ComboBoxItems?.Count == 0) ||
                    (ComboBoxIndex < 0) || (ComboBoxIndex >= ComboBoxItems?.Count))
                    return;
                Application.Current.Dispatcher.Invoke(() => { try { ComboBoxItems?.RemoveAt(ComboBoxIndex); } catch { } });
                RefreshComboBoxItem();
                if (ComboBoxItems?.Count == 0)
                    ClearErrors();
            } catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(CmdClear_Executed)}: {ex}"); }
        }
        private void CmdDelete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BtnAllEnable && ComboBoxItems != default && ((ComboBoxItems?.Count > 0) && (ComboBoxIndex >= 0));
        }

        private void CmdClear_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if ((ComboBoxItems == default) || (ComboBoxItems?.Count == 0))
                    return;
                Application.Current.Dispatcher.Invoke(() => { try { ComboBoxItems?.Clear(); } catch { } });
                RefreshComboBoxItem();
                ClearErrors();
            } catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(CmdClear_Executed)}: {ex}"); }
        }
        private void CmdClear_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BtnAllEnable && ComboBoxItems != default && (ComboBoxItems?.Count > 0);
        }

        private void CmdRestore_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var cmd = BtnReloadCallBack;
                if ((cmd == default) || (ComboBoxItems == default))
                    return;

                List<string> list = cmd.Invoke();
                if ((list == default) || (list?.Count == 0))
                    return;

                SetItems(list);
                RefreshComboBoxItem((ComboBoxItems.Count > 0) ? 0 : -1);
                ClearErrors();
            } catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(CmdRestore_Executed)}: {ex}"); }
        }
        private void CmdRestore_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BtnAllEnable;
        }

        private void CmdHelp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (!BtnDownloadEnable)
            {
                if (HelpUri == default)
                    return;
                try { Process.Start(HelpUri.AbsoluteUri); }
                catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(CmdHelp_Executed)}: {ex}"); }
                return;
            }
            try
            {
                var cmd = BtnDownloadCallBack;
                if ((cmd == default) || (ComboBoxItems == default))
                    return;

                List<string> list = cmd.Invoke();
                if ((list == default) || (list?.Count == 0))
                    return;

                SetItems(list);
                RefreshComboBoxItem((ComboBoxItems.Count > 0) ? 0 : -1);
                ClearErrors();
            }
            catch (Exception ex) { Debug.WriteLine($"{this.GetType().Name} {nameof(CmdHelp_Executed)}: {ex}"); }
        }
        private void CmdHelp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BtnAllEnable && (BtnDownloadEnable || !string.IsNullOrWhiteSpace(HelpToolTip));
        }
        #endregion

        #region event control
        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(SelectedItem):
                case nameof(ItemsSource):
                    {
                        RefreshComboBoxItem(
                            SelectComboBoxItem(SelectedItem));
                        break;
                    }
                case nameof(BtnDownloadCallBack):
                    {
                        OnPropertyChanged(nameof(BtnDownloadEnable));
                        break;
                    }
                case nameof(ComboBoxItems):
                    {
                        break;
                    }
                case nameof(ComboBoxIndex):
                    {
                        break;
                    }
            }
        }
        #endregion
    }
}
